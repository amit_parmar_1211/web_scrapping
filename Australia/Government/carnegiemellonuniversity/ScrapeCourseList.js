const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;

const configs = require('./configs');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'startScrappingFunc ';
    var s=null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var datalist = [];
      await s.setupNewBrowserPage("https://www.australia.cmu.edu/study/public-policy-and-management");
      var mainCategory = [], redirecturl = [];
      //scrape main category
      const maincategoryselector = "//*[@id='mm-0']/div[1]/div/div[2]/div[2]/div[2]/ul/li/a[not(text()='Executive Education')]";
      var category = await s.page.$x(maincategoryselector);
      console.log("Total categories-->" + category.length);
      for (let link of category) {
        var categorystringmain = await s.page.evaluate(el => el.innerText, link);
        mainCategory.push(categorystringmain.replace(/[\r\n\t ]+/g, ' ').trim());

        const selector = "//*[@id='mm-0']/div[1]/div/div[2]/div[2]/div[2]/ul/li/a[contains(text(),'" + categorystringmain.replace(/[\r\n\t ]+/g, ' ').trim() + "')]/following-sibling::div//ul/li/a";
        var title = await s.page.$x(selector);
        for (let t of title) {
          var categorystring = await s.page.evaluate(el => el.innerText, t);
          var categoryurl = await s.page.evaluate(el => el.href, t);
          datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: categorystringmain.replace(/[\r\n\t ]+/g, ' ').trim() });
        }
      }

      var uniquecases = [], uniquedata = [], finalDict = [];
      datalist.forEach(element => {
        if (!uniquecases.includes(element.href)) {
          uniquecases.push(element.href);
          uniquedata.push(element);
        }
      });
      uniquecases.forEach(element => {
        var data = datalist.filter(e => e.href == element);
        var category = [];
        data.forEach(element => {
          if (!category.includes(element.category))
            category.push(element.category);
        });

        data[0].category = (Array.isArray(category)) ? category : [category];
        finalDict.push(data[0]);
      });

      console.log("unique data-->" + uniquecases.length);
      fs.writeFileSync("./output/courselist_uniq.json", JSON.stringify(uniquedata));
      fs.writeFileSync("./output/courselist_original.json", JSON.stringify(datalist));
      console.log(funcName + 'writing courseList to file....');
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(finalDict));
      //fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

}

module.exports = { ScrapeCourseList };
