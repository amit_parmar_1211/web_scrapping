const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_title': {

                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            var ctitle = format_functions.titleCase(title)
                            resJsonData.course_title = ctitle
                            resJsonData.course_discipline = category;
                            resJsonData.program_code = '';
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];

                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; let ieltsNumber = null;
                            var otherLngDict = null;
                            const ieltsMappingDict = await utils.getValueFromHardCodedJsonFile('ielts_mapping');


                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            var penglishList = [];
                            const potherLngDict = ieltsMappingDict[0];
                            if (potherLngDict) {
                                var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                                if (potherLngDict.ielts) {
                                    ieltsScore = await utils.giveMeNumber(potherLngDict.ielts);
                                    console.log("### IELTS data-->" + ieltsScore);
                                }
                                if (potherLngDict.toefl) {
                                    ibtScore = await utils.giveMeNumber(potherLngDict.toefl);
                                }

                                if (ieltsScore != "NA") {
                                    pieltsDict.name = 'ielts academic';
                                    pieltsDict.description = potherLngDict.ielts
                                    pieltsDict.min = 0;
                                    pieltsDict.require = ieltsScore;
                                    pieltsDict.max = 9;
                                    pieltsDict.R = 0;
                                    pieltsDict.W = 0;
                                    pieltsDict.S = 0;
                                    pieltsDict.L = 0;
                                    pieltsDict.O = 0;
                                    englishList.push(pieltsDict);
                                }
                                if (ibtScore != "NA") {
                                    pibtDict.name = 'toefl ibt';
                                    pibtDict.description = potherLngDict.ibt
                                    pibtDict.min = 0;
                                    pibtDict.require = ibtScore;
                                    pibtDict.max = 120;
                                    pibtDict.R = 0;
                                    pibtDict.W = 0;
                                    pibtDict.S = 0;
                                    pibtDict.L = 0;
                                    pibtDict.O = 0;
                                    englishList.push(pibtDict);
                                }


                            }

                            ///progrssbar End
                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }

                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq];
                                }
                            }
                            var filedata = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                            courseAdminReq.academic_requirements_url = filedata["english_more"];
                            courseAdminReq.english_requirements_url = filedata["english_more"];
                            courseAdminReq.entry_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                        }
                        case 'course_url': {
                            var url = await Course.extractValueFromScrappedElement(courseScrappedData.course_url);
                            resJsonData.course_url = url.replace(/[\r\n\t ]+/g, '');

                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText.replace(/[\r\n\t-]+/g, ' ');
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    //courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("filtered_duration_formated", filtered_duration_formated);
                                    console.log("courseDurationDisplayList2", courseDurationDisplayList[0]);
                                    if (resFulltime && resFulltime.length > 0) {

                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston
                            var campLocationText = null;
                            var splitdata = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);

                            const courseKeyVals = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_cricos_code[0].includes('(') || course_cricos_code[0].includes(')')) {
                                course_cricos_code = course_cricos_code[0].replace(/[\r\n\t()]+/g, '')
                                course_cricos_code = course_cricos_code.split(':')[1]
                            }

                            //resJsonData.course_cricos_code = String(course_cricos_code);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                          campLocationText = selList[0];
                                          
                                        }
                                    }
                                }
                            }
                            console.log("##Campus-->" + campLocationText)
                            if (campLocationText && campLocationText.length > 0) {
                                if (campLocationText.includes('and')) {
                                    splitdata = campLocationText.split('and');

                                } else if (campLocationText.includes('&')) {

                                    var sdata = campLocationText.split('&')[0]
                                    splitdata.push(sdata.split('in')[1])


                                }
                                else {
                                    splitdata.push(campLocationText)
                                }
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                var campusedata = [];
                                splitdata.forEach(element => {

                                    if (element.trim() != 'Pittsburgh') {
                                        var cam = format_functions.titleCase(element)
                                        campusedata.push({
                                            "name": cam.trim(),
                                            "code": course_cricos_code.trim()
                                        })
                                    }
                                });

                                resJsonData.course_campus_location = campusedata;//await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');

                                resJsonData.course_study_mode = 'On campus';//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                                // }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};
                            let durationunit=null
                            var filedata = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                            var myfee = "";
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            filedata["fees"].forEach(element => {
                                if (element.name == title) {
                                    myfee = element.fee;
                                }
                            });
                            const feesIntStudent = myfee;
                            console.log("Fee in student--->" + feesIntStudent);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feeYear && feeYear.length > 0) {
                                courseTuitionFee.year = feeYear;
                            }
                            if (feesIntStudent) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                const regEx = /(\d+)/;
                                var duration = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time)
                                 var durations=parseInt(duration.match(regEx), 10)
                                 if(duration.includes('months') | duration.includes('month')){
                                    durationunit='months'
                                 }else if(duration.includes('years') || duration.includes('year') ){
                                    durationunit='years'
                                 }
                                if (feesVal) {
                                    if (feesVal != "NA" && feesVal != "N A") {
                                        const regEx = /\d/g;
                                        let feesValNum = feesVal.match(regEx);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            let feesNumber = null;
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }
                                            console.log(funcName + 'feesNumber = ' + feesNumber);
                                            if (Number(feesNumber)) {
                                                var feesint = {};
                                                feesint.amount = Number(feesNumber);
                                                feesint.duration = durations
                                                feesint.unit = durationunit;
                                                feesint.description = "" + myfee;
                                                feesDict.international_student = feesint;
                                            }
                                        }
                                    }
                                    else {
                                        var feesint = {};
                                        feesint.amount = 0;
                                        feesint.duration = "1";
                                        feesint.unit = "year";
                                        feesint.description = "" + myfee;
                                        feesDict.international_student = feesint;
                                    }
                                }
                            }
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            resJsonData.program_code = String(program_code);
                            break;
                        }
                        case 'course_intake': {
                            const courseIntakeDisplay = {};
                            // existing intake value
                            var courseIntakeStr = null;//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                courseIntakeStr = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var campus = resJsonData.course_campus_location;
                                var intakes = [];
                                console.log("Intake length-->" + courseIntakeStr.length);
                                console.log("Campus length-->" + campus.length);
                                var intakedetail = {};                              
                                var courseIntakeStrarr = [];
                                courseIntakeStr.forEach(element => {
                                    courseIntakeStrarr.push(element.replace(/[\r\n\t ]+/g, ''));
                                });
                                intakedetail.name = campus[0].name;
                                intakedetail.value = courseIntakeStrarr;
                                intakes.push(intakedetail);
                                var intakedata = {};
                                intakedata.intake = await utils.providemyintake(intakes, "mom", "");
                                intakedata.more_details = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake_url);
                                resJsonData.course_intake = intakedata;

                            }
                            break;
                        }
                     
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_overview.length > 0) {
                                resJsonData.course_overview = course_overview;
                            }
                            break;
                        }

                        case 'course_career_outcome': {
                           // const courseKeyVal = courseScrappedData.course_career_outcome;
                           let courseKeyVal=null;
                            let ctitle=resJsonData.course_title;
                            if(ctitle.toLowerCase().includes('msppm')){
                                 courseKeyVal=  courseScrappedData.course_career_outcome_msppm;
                            }else if(ctitle.toLowerCase().includes('msit')){
                                courseKeyVal=courseScrappedData.course_career_outcome;
                            }
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if(course_career_outcome==null){
                                resJsonData.course_career_outcome=[] 
                            }
                            else if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            } else{
                                resJsonData.course_career_outcome=[] 
                            }
                            break;
                        }
                        case 'course_outline': {
                           const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                            const courseKeyVal_major = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline);
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                            let course_outlines = {};
                            if (courseKeyVal_minor != null) {
                                course_outlines.minors = courseKeyVal_minor
                            } else {
                                course_outlines.minors = []
                            }
                            if (courseKeyVal_major != null) {
                                course_outlines.majors = courseKeyVal_major
                            } else {
                                course_outlines.majors = []
                            }
                            if (courseKeyVal != null) {
                                course_outlines.more_details = courseKeyVal
                            } else {
                                course_outlines.more_details = ""
                            }
                            resJsonData.course_outline = course_outlines
                            break;
                        }
                        case 'application_fees':{
                            const courseKeyVal=await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            if(courseKeyVal != null){
                                resJsonData.application_fee=courseKeyVal
                            }else{
                                resJsonData.application_fee=""
                            }
                        }

                        case 'course_study_level': {
                            resJsonData.course_study_level = "Postgraduate";
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        }
                    }
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                }
            }


            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                const basecourseid = location_wise_data.course_id;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid.replace(/[\n :""]+/g, '').replace(/[\/]+/g, '_') + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.category, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };













//             var NEWJSONSTRUCT = {}, structDict = [];
//             console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
//             var locations = resJsonData.course_campus_location;
//             for (let location of locations) {
//                 NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
//                 NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id + "_" + location.replace(/\s/g, '').toLowerCase();
//                 NEWJSONSTRUCT.course_title = resJsonData.course_title;
//                 NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
//                 NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
//                 NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
//                 NEWJSONSTRUCT.course_url = resJsonData.course_url;

//                 console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
//                 NEWJSONSTRUCT.course_campus_location = location;
//                 NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
//                 NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
//                 NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
//                 NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
//                 NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
//                 var intakes = resJsonData.course_intake.intake;
//                 var matchrec = [];
//                 for (let dintake of intakes) {
//                     if (location == dintake.name) {
//                         matchrec = dintake.value;
//                     }
//                 }
//                 if (matchrec.length > 0) {
//                     NEWJSONSTRUCT.course_intake = matchrec[0];
//                 }
//                 else {
//                     NEWJSONSTRUCT.course_intake = "NA";
//                 }
//                 for (let myfees of resJsonData.course_tuition_fee.fees) {
//                     if (myfees.name == location) {
//                         NEWJSONSTRUCT.international_student_all_fees = myfees;
//                     }
//                 }
//                 structDict.push(NEWJSONSTRUCT);
//                 NEWJSONSTRUCT = {};
//             }
//             for (let location_wise_data of structDict) {
//                 resJsonData.course_id = location_wise_data.course_location_id;
//                 //resJsonData.course_cricos_code = location_wise_data.cricos_code;
//                 resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
//                 resJsonData.current_course_campus_location = location_wise_data.course_campus_location;
//                 //resJsonData.current_course_intake = location_wise_data.course_intake;
//                 resJsonData.course_intake_display = location_wise_data.course_intake;
//                 var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
//                 console.log("Write file--->" + filelocation)
//                 fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

//             }
//             return resJsonData;
//         } catch (error) {
//             console.log(funcName + 'try-catch error = ' + error);
//             throw (error);
//         }
//     }

//     // scrape each course details
//     static async scrapeAndFormatCourseDetails(courseDict) {
//         const funcName = 'scrapeAndFormatCourseDetails ';
//         let s = null;
//         try {
//             s = new Scrape();
//             await s.init({ headless: true });
//             await s.setupNewBrowserPage(courseDict.href);

//             Scrape.validateParams([courseDict]);
//             // get course href and innerText from courseDict and ensure it is valid
//             if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
//                 console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
//                 throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
//             }
//             const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
//             console.log(funcName + 'courseTitle = ' + courseTitle);
//             console.log(funcName + 'courseUrl = ' + courseUrl);
//             // output course data and file path
//             let courseScrappedData = null;
//             // create courseId from course title
//             const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
//             // validate generated courseId
//             console.log(funcName + 'courseId = ' + courseId);
//             if (!(courseId && courseId.length > 0)) {
//                 console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
//                 throw (new Error('Invalid courseId, courseId = ' + courseId));
//             }
//             // configure output file path as per UpdateItem configuration
//             const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
//             // decide if need to scrape or read data from local outpurfile
//             if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
//                 console.log(funcName + 'reading scrapped data from local output file...');
//                 const fileData = fs.readFileSync(opCourseFilePath);
//                 if (!fileData) {
//                     console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
//                     throw (new Error('Invalid fileData, fileData = ' + fileData));
//                 }
//                 courseScrappedData = JSON.parse(fileData);
//                 return courseScrappedData;
//             }

//             if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
//                 console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
//                 courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
//             } else {
//                 console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
//                 courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
//             }
//             if (!courseScrappedData) {
//                 throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
//             }

//             const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, await s.page.url(), courseDict.category);
//             const finalScrappedData = await Course.removeKeys(formattedScrappedData);
//             if (s) {
//                 await s.close();
//             }
//             return finalScrappedData;
//         } catch (error) {
//             console.log(funcName + 'try-catch error = ' + error);
//             if (s) {
//                 await s.close();
//             }
//             throw (error);
//         }
//     } // scrapeCourseDetails
// } // class
// module.exports = { ScrapeCourse };
