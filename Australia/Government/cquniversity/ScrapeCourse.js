const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');
class ScrapeCourse extends Course {
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, study_level) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      var demoarray = [];
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'course_discipline': {
              // As of not we do not have way to get this field value
              resJsonData.course_discipline = course_category;
              break;
            }

            case 'course_title': {
              const titles = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
              const ctitle = String(format_functions.titleCase(titles)).trim()
              console.log("Title", ctitle)
              resJsonData.course_title = ctitle.split("-")[0].trim()
              console.log("Title1", resJsonData.course_title)
              
              if (resJsonData.course_title) {
                var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                var parogramcodedata = String(title).split("-");
                var mycode = []
                mycode.push((parogramcodedata.length > 1) ? parogramcodedata[1].trim() : "");
                global.program_code = mycode[0];
                resJsonData.program_code = String(mycode);
                console.log("##program_code" + global.program_code);
              }
              var course_title1 = resJsonData.course_title + "(" +resJsonData.program_code + ")";
              break;
            }
            case 'application_fee': {
              resJsonData.application_fee = "";
            }
            case 'course_outline_majors': {
              var course_career_outline = {};
              let majors = []
              let minors = []
              var courseKeyVal1 = courseScrappedData.course_outline_majors;
              var courseKeyVal2 = courseScrappedData.course_outline_minors;
              console.log("majors------>", JSON.stringify(courseKeyVal1));
              console.log("minors--->", JSON.stringify(courseKeyVal2));

              if (courseKeyVal1 && courseKeyVal1.length > 0) {
                for (const rootEleDict of courseKeyVal1) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selListoutcome1 = ' + JSON.stringify(selList).replace(/[\r\n\t]+/g, ' '));

                      if (Array.isArray(selList) && selList.length > 0) {
                        //  let out_val = String(selList).replace(/[\r\n\t ]+/g, ' ');
                        //var selList1 = String(selList).replace(/[\r\n\t ]+/g, '')
                        // majors.push(selList);
                        majors = 
                         
                           selList,
                        
                        course_career_outline = majors;

                        console.log("course_career_outle--?", course_career_outline);

                      }
                      else {
                        majors = 
                         [],
                         
                        
                        course_career_outline = majors;
                      }
                    }
                  }
                }
              }

              if (courseKeyVal2 && courseKeyVal2.length > 0) {
                console.log("entered-->")
                for (const rootEleDict of courseKeyVal2) {
                  console.log(funcName + '\n\r rootEleDictf = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDictf = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selListoutcome2 = ' + JSON.stringify(selList).replace(/[\r\n\t]+/g, ' '));

                      if (Array.isArray(selList) && selList.length > 0) {
                        //  let out_val = String(selList).replace(/[\r\n\t ]+/g, ' ');
                        //var selList1 = String(selList).replace(/[\r\n\t ]+/g, '')
                        // majors.push(selList);
                        minors = 
                         
                         selList
                        
                        course_career_outline = minors;

                        console.log("course_career_outline--?", course_career_outline);

                      }
                      else {
                        minors = 
                         
                           [],
                        
                        course_career_outline = minors;
                      }
                    }
                  }
                }
              }
             
              

              if (course_career_outline) {
                resJsonData.course_outline = ({
                  "majors": majors,
                  "minors": minors,
                  "more_details": courseUrl
                })
                console.log("resJsonData.course_care--?", resJsonData.course_outline);
              }
              else {
                resJsonData.course_outline = ({
                  "majors": [],
                  "minors": [],
                  "more_details": courseUrl
                }) 

              }
              break;
            }
            case 'course_url': {
              let newUrl = courseScrappedData.course_url;
              let rescourse_url = null;
              if (Array.isArray(newUrl)) {
                for (const rootEleDict of newUrl) {
                  console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));

                      rescourse_url = selList;

                    }
                  }
                }
              }
              if (rescourse_url) {
                resJsonData.course_url = rescourse_url;
              }
              break;

            }
            case 'course_duration_full_time': {
              // display full time
              var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = "";
              var fullTimeText = "";
              const courseKeyVal = courseScrappedData.course_duration_full_time;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        fullTimeText = selList[0];
                      }
                    }
                  }
                }
              }
              if (fullTimeText && fullTimeText.length > 0) {
                const resFulltime = fullTimeText;
                if (resFulltime) {
                  durationFullTime = resFulltime;
                  courseDurationList = durationFullTime;
                  let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                  courseDurationDisplayList.push(tempvar);
                  demoarray = tempvar[0];
                  console.log("demoarray--->", demoarray);
                  console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));

                  let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);

                  if (courseDurationList && courseDurationList.length > 0) {
                    resJsonData.course_duration = courseDurationList;
                  } else {
                    resJsonData.course_duration = "";
                  }
                  if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                    resJsonData.course_duration_display = filtered_duration_formated;
                    var isfulltime = false, isparttime = false;
                    filtered_duration_formated.forEach(element => {
                      if (element.display == "Full-Time") {
                        isfulltime = true;
                      }
                      if (element.display == "Part-Time") {
                        isparttime = true;
                      }
                    });
                    resJsonData.isfulltime = isfulltime;
                    resJsonData.isparttime = isparttime;
                  } else {
                    var coursedurationNotdefine = {};
                    durationFullTime.duration_full_time = "";
                    courseDurationList.push(durationFullTime);
                    if (courseDurationList && courseDurationList.length > 0) {
                      resJsonData.course_duration = courseDurationList;
                    }
                    var durationNa = [];
                    coursedurationNotdefine.unit = "";
                    coursedurationNotdefine.duration = "";
                    coursedurationNotdefine.display = "";
                    durationNa.push(coursedurationNotdefine);
                    courseDurationDisplayList.push(durationNa);

                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);

                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                      resJsonData.course_duration_display = filtered_duration_formated;

                    }
                  }
                }
              }
              break;
            }
            case 'course_tuition_fee':
            case 'course_study_mode':
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_duration_part_time': {
              console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
              // No need to process anything, its already processed in other case.
              break;
            }
            case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};
              // courseTuitionFee.year = configs.propValueNotAvaialble;

              const feesList = [];
              const feesDict = {
                international_student: {}
              };

              console.log("demoarray123 -->", demoarray);
              // feesDict.international_student = configs.propValueNotAvaialble;
              // feesDict.fee_duration_years = configs.propValueNotAvaialble;
              // feesDict.currency = configs.propValueNotAvaialble;
              var myfiledat = {
                "filepath": "../cquniversity/output/course_fee_detail.json",
                "comparekeycolumn": "course_code",
                "value": "global.program_code",
                "returnkey": "total_fee"
              }

              const feesIntStudent = await format_functions.readanyfile(myfiledat);//await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
              console.log("Fee in student--->" + feesIntStudent);
              var isfulltimeflag = false;
              var durationfee = 1;
              var unitfee = "Semester";
              console.log('course duration fee :' + feesIntStudent);
              if (String(feesIntStudent).toLowerCase().indexOf('annual fee') > -1) {
                durationfee = 1;
                unitfee = "Semester";
                if (fullTimeText.indexOf('annual fee') > -1) {
                  isfulltimeflag = true;
                }
              }

              const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
              if (feeYear && feeYear.length > 0) {
                courseTuitionFee.year = feeYear;
              }
              // if we can extract value as Int successfully then replace Or keep as it is
              if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                const arrval = String(feesVal1).split('.');

                const feesVal = String(arrval[0]);

                console.log(funcName + 'feesVal = ' + feesVal);
                if (feesVal) {
                  if (feesVal != "NA" && feesVal != "N A") {
                    const regEx = /\d/g;
                    let feesValNum = feesVal.match(regEx);
                    if (feesValNum) {
                      console.log(funcName + 'feesValNum = ' + feesValNum);
                      feesValNum = feesValNum.join('');
                      console.log(funcName + 'feesValNum = ' + feesValNum);
                      let feesNumber = null;
                      if (feesValNum.includes(',')) {
                        feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                      } else {
                        feesNumber = feesValNum;
                      }
                      console.log(funcName + 'feesNumber = ' + feesNumber);
                      // if (Number(feesNumber)) {
                      //   feesDict.international_student = Number(feesNumber);
                      // }
                      if (Number(feesNumber)) {
                        feesDict.international_student = ({
                          amount: Number(feesNumber),
                          duration: Number(demoarray.duration),
                          unit: demoarray.unit,

                          description: feesIntStudent,

                        });
                      }
                    }
                  }
                  else {
                    //console.log("fee NA")
                    //feesDict.international_student = "";
                    
                    var durationfee = 1;
                    var unitfee = "Year";

                    // if (Number(inetStudentFees)) {
                    if (feesVal == "NA") {
                      feesDict.international_student = ({
                        amount: 0,
                        duration: Number(demoarray.duration),
                        unit: demoarray.unit,

                        description: "",

                      });
                    }

                  }
                }
              } // if (feesIntStudent && feesIntStudent.length > 0)

              // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
              const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
              console.log(funcName + 'cricoseStr = ' + cricoseStr);
              console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
              if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                if (fixedFeesDict) {
                  for (const codeKey in fixedFeesDict) {
                    console.log(funcName + 'codeKey = ' + codeKey);
                    if (fixedFeesDict.hasOwnProperty(codeKey)) {
                      const keyVal = fixedFeesDict[codeKey];
                      console.log(funcName + 'keyVal = ' + keyVal);
                      if (cricoseStr.includes(codeKey)) {
                        const feesDictVal = fixedFeesDict[cricoseStr];
                        console.log(funcName + 'feesDictVal = ' + feesDictVal);
                        if (feesDictVal && feesDictVal.length > 0) {
                          const inetStudentFees = Number(feesDictVal);
                          console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                          if (Number(inetStudentFees)) {
                            feesDict.international_student = Number(inetStudentFees);
                          }
                        }
                      }
                    }
                  } // for
                } // if
              } // if

              if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                console.log("international_student");
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                  var more_fee = [];
                  //more_fee.push("");
                }

                // feesDict.international_student_all_fees = more_fee;
                if (feesDict) {
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                if (courseTuitionFee) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
              }
              break;
            }
            case 'course_campus_location': { // Location Launceston
              let campLocationText
              if (resJsonData.course_title == 'BACHELOR OF PARAMEDIC SCIENCE - CG95') {
                campLocationText = await utils.getValueFromHardCodedJsonFile('campusLocation');
              } else {
                campLocationText = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
              }
              var cricc = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
              console.log("criccos code-->", cricc);

              //const campLocationText = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);

              console.log(funcName + 'campLocationText = ' + campLocationText);
                if(campLocationText == null)
               {
              var myfiledat = {
                "filepath": "../cquniversity/output/course_fee_detail.json",
                "comparekeycolumn": "course_code",
                "value": "global.program_code",
                "returnkey": "availability"
              }
              
              const locasion = await format_functions.readanyfile(myfiledat);
              console.log("myfiledat--->",locasion);
              campLocationText = locasion
            }
              // if(campLocationText == null)
              // {
              //   var locate = cricc
              //   const campp = await format_functions.getLocation(locate)
              // console.log("Study_level97777-->", campp)
              // if (campp.includes(' Brisbane C.Q.U. Campus')) 
              // console.log("enterd-------------000-->")
              //   campLocationText = "Brisbane"
              
              // // resJsonData.course_study_level = studylevel
              // // if (!resJsonData.course_study_level) {
              // //   //throw  new error("Study_level not found")
              // //   throw (new Error('Study_level not found'));
              // // }
              
              // console.log("capmus-->",campLocationText);
              if (campLocationText && campLocationText.length > 0) {
                var campLocationValTrimmed = String(campLocationText).trim();
                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                  var mylocations = await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ''), ',');
                  var mylocations_rem = [];
                  // var mylocations_code = [];
                  if (mylocations.length > 0) {
                    mylocations.forEach(element => {
                      if (element.toLowerCase() != "online" && element.toLowerCase() != "distance") {
                        mylocations_rem.push(element);
                        // mylocations_code.push(element + " - " + resJsonData.course_cricos_code[0]);
                      }

                    });
                  }
                  var campusedata = [];
                  mylocations_rem.forEach(element => {
                    var cam = format_functions.titleCase(element)
                    campusedata.push({
                      "name": cam,
                      "code": cricc,
                    })
                  });
                  // resJsonData.course_cricos_code = mylocations_code;
                  console.log("Avail-->" + campusedata)
                  resJsonData.course_campus_location = campusedata;
                }


                  var studymode = "On campus";
                  resJsonData.course_study_mode = "On campus";

                  //resJsonData.course_campus_location = await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                  // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //

                  console.log("## FInal string-->" + campLocationValTrimmed);
                }
              
              // if (campLocationText && campLocationText.length > 0)
              // else {
              //   var location = ["NA"];
              //   console.log("Avail-->" + location)
              //   resJsonData.course_campus_location = location;
              //   resJsonData.course_study_mode = location;
              // }
              break;
            }
            case 'course_intake_url': {
              break;
            }
            case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
              // Semester 1 (February) and Semester 2 (August)
              // prepare emty JSON struct
              const courseIntakeDisplay = {};
              //let  courseKeyVal;
              // existing intake value
              var courseIntakeStr = null;

              const courseKeyVal = courseScrappedData.course_intake;
              console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        courseIntakeStr = selList;
                      }
                    }
                  }
                }
              }
             
              console.log("intakes" + JSON.stringify(courseIntakeStr));
              var myarry = [];
              if (!courseIntakeStr) {
                console.log("Online Course")
              }

              for (var i = 0; i < courseIntakeStr[0].length; i++) {
                var myinnerarray = [];
                console.log("outer-->" + courseIntakeStr[0][i]);
                //if (courseIntakeStr[0][i].indexOf("2020") == -1) {
                for (var j = 0; j < courseIntakeStr.length; j++) {
                  myinnerarray.push(courseIntakeStr[j][i]);
                }
                // }
                // else {
                //   break;
                // }
                myarry.push(myinnerarray);
              }
              console.log("mydataarray" + JSON.stringify(myarry))

              var myintake_data = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];

              if (myarry && myarry.length > 0) {
                var campus = resJsonData.course_campus_location;
                var intakes = [];
                console.log("Intake length-->" + myarry.length);
                //console.log("Campus length-->" + campus.length);
                for (let loc of campus) {
                  var intakedetail = {};
                  var myintakeArray = [];
                  for (var count = 0; count < myarry.length; count++) {
                    console.log("intake -->" + myarry[count][0].replace(/[\r\n\t]+/g, '').toLowerCase().includes("term 1 - 2020"));

                    if (myarry[count][0].replace(/[\r\n\t]+/g, '').toLowerCase().includes("term 1 - 2020") && myarry[count][1].includes(loc.name)) {
                      console.log("Match--> t1_2020")
                      myintakeArray.push(myintake_data["Term 1_2020"])

                    }
                    if (myarry[count][0].replace(/[\r\n\t]+/g, '').toLowerCase().includes("term 2 - 2020") && myarry[count][1].includes(loc.name)) {
                      console.log("Match--> t2_2020")
                      myintakeArray.push(myintake_data["Term 2_2020"])

                    }
                    if (myarry[count][0].replace(/[\r\n\t]+/g, '').toLowerCase().includes("term 3 - 2020") && myarry[count][1].includes(loc.name)) {
                      console.log("Match--> t3_2020")
                      myintakeArray.push(myintake_data["Term 3_2020"])

                    }
                   
                  }
                  console.log("MYINTAKEArray", JSON.stringify(myintakeArray))
                  intakedetail.name = loc.name;
                  intakedetail.value = myintakeArray;
                  intakes.push(intakedetail);
                }

                console.log('intake display for user account :' + JSON.stringify(intakes));
                let formatIntake = await format_functions.providemyintake(intakes, "mom", "");



                var intakedata = {};
                intakedata.intake = formatIntake;
                intakedata.more_details = resJsonData.course_intake_url;
                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');

              } // if (courseIntakeStr && courseIntakeStr.length > 0)
              else {
                var campus = resJsonData.course_campus_location;
                for (let loc of campus) {
                  var intakedetail = {};
                  var intakes = [];
                  intakedetail.name = loc.name;
                intakedetail.value = [];
                  intakes.push(intakedetail);
                  var intakedata = {};
                intakedata.intake = intakes;
                intakedata.more_details = resJsonData.course_intake_url;
                resJsonData.course_intake = intakedata;
                }
                break;
              }
               
                let formatIntake = await format_functions.providemyintake(intakes, "mom", "");


                var intakedata = {};
                intakedata.intake = formatIntake;
                intakedata.more_details = resJsonData.course_intake_url;
                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
              
              // if (courseIntakeDisplay) {
              //   resJsonData.course_intake_display = courseIntakeDisplay;
              // }
              break;
            }


            case 'univ_name': {
              const courseKeyVal = courseScrappedData.univ_name;
              let resUnivName = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivName = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivName) {
                resJsonData.univ_name = resUnivName;
              }
              break;
            }
            case 'univ_logo': {
              const courseKeyVal = courseScrappedData.univ_logo;
              let resUnivLogo = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivLogo = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivLogo) {
                resJsonData.univ_logo = resUnivLogo;
              }
              break;
            }


            case 'course_study_level': {
              const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
              const Tittle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
              console.log("hi--->",Tittle);
              if(Tittle.includes('BACHELOR OF ARTS'))
              {
                resJsonData.course_study_level = "Bachelor Degree";
                break;
              }

              console.log("CRICOSE@@@@@@", cTitle)
              const studylevel = await format_functions.getMyStudyLevel(cTitle)
              console.log("Study_level", studylevel)
              resJsonData.course_study_level = studylevel
              if (!resJsonData.course_study_level) {
                //throw  new error("Study_level not found")
                throw (new Error('Study_level not found'));
              }


              break;
            }
            case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              const courseAdminReq = {};
              const englishList = [];
              const ieltsDict = {};
              const pibtDict = {};
              const pcaeDict = {};
              const ppteDict = {};
              let othercourses = [];
              let othercourse;
              let ielts, ibt, pte, cae;

              var cStudyLevels = resJsonData.course_study_level
              console.log("StudyLevel!!!" + JSON.stringify(cStudyLevels));
              if (cStudyLevels) {
                othercourse = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                //console.log("OtherCourses-->",JSON.stringify(othercourse))
                othercourse.forEach(element => {
                  console.log("element", element)
                  if (element.key.includes(cStudyLevels)) {
                    console.log("Element->", element)
                    othercourses = element
                    console.log("othercourses-->", othercourses);
                  }
                })
                ielts = othercourses["ielts"]
                ibt = othercourses["toeflibt"]
                pte = othercourses["pte"]
                cae = othercourses["cae"]
                console.log("ielts@@@@", ielts)
                console.log("ibt@@@@", ibt)
              }
              //   if (potherLngDict) {
              var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "", pbtScore = "";
              if (ielts) {
                ieltsScore = await utils.giveMeNumber(ielts);
                console.log("### IELTS data-->" + ieltsScore);
              }
              if (ibt) {
                ibtScore = await utils.giveMeNumber(ibt);
              }
              if (pte) {
                pteScore = await utils.giveMeNumber(pte);
              }

              if (cae) {
                caeScore = await utils.giveMeNumber(cae);
                console.log("### IELTS datas-->" + pbtScore);
              }
              if (ieltsScore != "NA") {
                ieltsDict.name = 'ielts academic';
                ieltsDict.description = ielts;
                ieltsDict.min = 0;
                ieltsDict.require = ieltsScore;
                ieltsDict.max = 9;
                ieltsDict.R = 0;
                ieltsDict.W = 0;
                ieltsDict.S = 0;
                ieltsDict.L = 0;
                ieltsDict.O = 0;
                englishList.push(ieltsDict);
              }
              if (ibtScore != "NA") {
                pibtDict.name = 'toefl ibt';
                pibtDict.description = ibt
                pibtDict.min = 0;
                pibtDict.require = ibtScore;
                pibtDict.max = 120;
                pibtDict.W = 0;
                pibtDict.S = 0;
                pibtDict.L = 0;
                pibtDict.O = 0;
                englishList.push(pibtDict);
              }
              // if (pbtScore != "NA") {
              //   ppbtDict.name = 'toefl pte';
              //   ppbtDict.description = pbt
              //   ppbtDict.min = 310;
              //   ppbtDict.require = pbtScore;
              //   ppbtDict.max = 677;
              //   ppbtDict.W = 0;
              //   ppbtDict.S = 0;
              //   ppbtDict.L = 0;
              //   ppbtDict.O = 0;
              //   englishList.push(ppbtDict);
              // }
              if (pteScore != "NA") {
                ppteDict.name = 'pte';
                ppteDict.description = pte
                ppteDict.min = 0;
                ppteDict.require = pteScore;
                ppteDict.max = 90;
                ppteDict.W = 0;
                ppteDict.S = 0;
                ppteDict.L = 0;
                ppteDict.O = 0;
                englishList.push(ppteDict);
              }
              if (caeScore != "NA") {
                pcaeDict.name = 'cae academic';
                pcaeDict.description = cae
                pcaeDict.min = 0;
                pcaeDict.require = caeScore;
                pcaeDict.max = 90;
                pcaeDict.W = 0;
                pcaeDict.S = 0;
                pcaeDict.L = 0;
                pcaeDict.O = 0;
                englishList.push(pcaeDict);
              }

              if (englishList && englishList.length > 0) {
                courseAdminReq.english = englishList;
              }

              var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
              var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
              var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
              var academicReq = null;
              const courseKeyVal = courseScrappedData.course_academic_requirement;
              console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      academicReq = selList
                    }
                  }
                }
              }
              courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];
              courseAdminReq.english_requirements_url = english_more;
              courseAdminReq.academic_requirements_url = academic_more;
              courseAdminReq.entry_requirements_url = entry_requirements_url;
              resJsonData.course_admission_requirement = courseAdminReq;
              break;
            }
            case 'course_country': {
              const courseKeyVal = courseScrappedData.course_country;
              let course_country = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      //if (Array.isArray(selList) && selList.length > 0) {
                      course_country = selList[0];
                      //}
                    }
                  }
                }
              }
              resJsonData.course_country = course_country;
              break;
            }
            case 'course_overview': {
              const courseKeyVal = courseScrappedData.course_overview;
              console.log("overview---" + JSON.stringify(courseKeyVal));
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_overview = resAccomodationCostJson;
              } else {
                resJsonData.course_overview = ""
              }
              console.log("COURSE_overview", resJsonData.course_overview)
              break;
            }
            case 'course_career_outcome': {
              var course_career_outcome = [];
              let out_val = []
              const courseKeyVal = courseScrappedData.course_career_outcome;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selListoutcome = ' + JSON.stringify(selList).replace(/[\r\n\t]+/g, ' '));

                      if (Array.isArray(selList) && selList.length > 0) {
                        //  let out_val = String(selList).replace(/[\r\n\t ]+/g, ' ');
                        //var selList1 = String(selList).replace(/[\r\n\t ]+/g, '')
                        course_career_outcome = selList;

                        console.log("course_career_outcome--?", course_career_outcome);

                      }
                    }
                  }
                }
              }
              if (course_career_outcome) {
                resJsonData.course_career_outcome = course_career_outcome;
                console.log("resJsonData.course_career_outcome--?", resJsonData.course_career_outcome);
              }
              else {
                resJsonData.course_career_outcome = []

              }
              break;
            }

            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      // console.log('\n\r' + funcName + 'course resJsonData =' + JSON.stringify(resJsonData));
      var NEWJSONSTRUCT = {}, structDict = [];
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = course_title1.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        }
        else {
          NEWJSONSTRUCT.course_intake = [];
        }
        for (let myfees of resJsonData.course_tuition_fee.fees) {
          if (myfees.name == location) {
            // NEWJSONSTRUCT.international_student_all_fees = myfees;
          }
        }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        console.log("location::::" + location_wise_data.course_campus_location);
        resJsonData.course_id = location_wise_data.course_location_id;
        // resJsonData.basecourseid = location_wise_data.course_id;

        // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
        //   if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
        //     resJsonData.course_cricos_code[i].iscurrent = true;
        //   }
        //   else {
        //     resJsonData.course_cricos_code[i].iscurrent = false;
        //   }
        // }

        // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
        //   if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
        //     resJsonData.course_intake.intake[i].iscurrent = true;
        //   }
        //   else {
        //     resJsonData.course_intake.intake[i].iscurrent = false;
        //   }
        // }

        // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
        //   if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
        //     resJsonData.course_tuition_fee.fees[i].iscurrent = true;
        //   }
        //   else {
        //     resJsonData.course_tuition_fee.fees[i].iscurrent = false;
        //   }
        // }

        // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
        //   if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
        //     resJsonData.course_campus_location[i].iscurrent = true;
        //   }
        //   else {
        //     resJsonData.course_campus_location[i].iscurrent = false;
        //   }
        // }
        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

        // try {
        //     if (appConfigs.ISINSERT) {
        //         request.post({
        //             "headers": { "content-type": "application/json" },
        //             "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
        //             "body": JSON.stringify(resJsonData)
        //         }, (error, response, body) => {
        //             if (error) {
        //                 console.log(error);
        //             }
        //             var myresponse = JSON.parse(body);
        //             if (!myresponse.flag) {
        //                 var errorlog = null;
        //                 if (fs.existsSync(configs.opFailedCourseListByAPI)) {
        //                     errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
        //                     errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
        //                 }
        //                 fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
        //             }
        //             console.log("success save data! " + JSON.stringify(body));
        //         });
        //     }
        //     else if (appConfigs.ISREPLACE) {
        //         request.post({
        //             "headers": { "content-type": "application/json" },
        //             "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
        //             "body": JSON.stringify(resJsonData)
        //         }, (error, response, body) => {
        //             if (error) {
        //                 console.log(error);
        //             }
        //             var myresponse = JSON.parse(body);
        //             console.log("MyMessage-->" + JSON.stringify(myresponse))
        //             if (!myresponse.flag) {
        //                 var errorlog = null;
        //                 if (fs.existsSync(configs.opFailedCourseListByAPI)) {
        //                     errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
        //                     errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
        //                 }
        //                 fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
        //             }
        //             console.log("success save data! " + JSON.stringify(body));
        //         });
        //     }
        //     else if (appConfigs.ISUPDATE) {
        //         request.post({
        //             "headers": { "content-type": "application/json" },
        //             "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
        //             "body": JSON.stringify(resJsonData)
        //         }, (error, response, body) => {
        //             if (error) {
        //                 console.log(error);
        //             }
        //             var myresponse = JSON.parse(body);
        //             if (!myresponse.flag) {
        //                 var errorlog = null;
        //                 if (fs.existsSync(configs.opFailedCourseListByAPI)) {
        //                     errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
        //                     errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
        //                 }
        //                 fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
        //             }
        //             console.log("success save data! " + JSON.stringify(body));
        //         });
        //     }
        // }
        // catch (err) {
        //     console.log("#### try-catch error--->" + err);
        // }
        // const res = await awsUtil.putFileInBucket(filelocation);
        //console.log(funcName + 'S3 object location = ' + res);
      }
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = 'scrapeAndFormatCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);

      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }
      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file

      //custom call


      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }



      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.category, courseDict.category, courseDict.study_level);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };


















//       var NEWJSONSTRUCT = {}, structDict = [];
//       console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
//       var locations = resJsonData.course_campus_location;
//       for (let location of locations) {
//         NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
//         NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id + "_" + location.replace(/\s/g, '').toLowerCase();
//         NEWJSONSTRUCT.course_title = resJsonData.course_title;
//         NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
//         NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
//         NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
//         NEWJSONSTRUCT.course_url = resJsonData.course_url;
//         //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
//         console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
//         NEWJSONSTRUCT.course_campus_location = location;
//         NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
//         NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
//         NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
//         NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
//         NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
//         var intakes = resJsonData.course_intake.intake;
//         var matchrec = [];
//         for (let dintake of intakes) {
//           if (location == dintake.name) {
//             matchrec = dintake.value;
//           }
//         }
//         if (matchrec.length > 0) {
//           NEWJSONSTRUCT.course_intake = matchrec[0];
//         }
//         else {
//           NEWJSONSTRUCT.course_intake = "NA";
//         }
//         for (let myfees of resJsonData.course_tuition_fee.fees) {
//           if (myfees.name == location) {
//             NEWJSONSTRUCT.international_student_all_fees = myfees;
//           }
//         }
//         structDict.push(NEWJSONSTRUCT);
//         NEWJSONSTRUCT = {};
//       }
//       for (let location_wise_data of structDict) {
//         resJsonData.course_id = location_wise_data.course_id;
//         //resJsonData.course_cricos_code = location_wise_data.cricos_code;
//         resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
//         resJsonData.current_course_campus_location = location_wise_data.course_campus_location;
//         //resJsonData.current_course_intake = location_wise_data.course_intake;
//         resJsonData.course_intake_display = location_wise_data.course_intake;
//         var filelocation = "./output/" + resJsonData.univ_id + "_" + resJsonData.course_id + "_details.json";
//         console.log("Write file--->" + filelocation)
//         fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
//         try {
//           if (appConfigs.ISINSERT) {
//             request.post({
//               "headers": { "content-type": "application/json" },
//               "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
//               "body": JSON.stringify(resJsonData)
//             }, (error, response, body) => {
//               if (error) {
//                 console.log(error);
//               }
//               var myresponse = JSON.parse(body);
//               if (!myresponse.flag) {
//                 var errorlog = [];
//                 if (fs.existsSync(configs.opFailedCourseListByAPI)) {
//                   errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
//                   errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
//                 }
//                 fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
//               }
//               console.log("success save data! " + JSON.stringify(body));
//             });
//           }
//           else if (appConfigs.ISREPLACE) {
//             request.post({
//               "headers": { "content-type": "application/json" },
//               "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
//               "body": JSON.stringify(resJsonData)
//             }, (error, response, body) => {
//               if (error) {
//                 console.log(error);
//               }
//               var myresponse = JSON.parse(body);
//               console.log("MyMessage-->" + JSON.stringify(myresponse))
//               if (!myresponse.flag) {
//                 var errorlog = [];
//                 if (fs.existsSync(configs.opFailedCourseListByAPI)) {
//                   errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
//                   errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
//                 }
//                 fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
//               }
//               console.log("success save data! " + JSON.stringify(body));
//             });
//           }
//           else if (appConfigs.ISUPDATE) {
//             request.post({
//               "headers": { "content-type": "application/json" },
//               "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
//               "body": JSON.stringify(resJsonData)
//             }, (error, response, body) => {
//               if (error) {
//                 console.log(error);
//               }
//               var myresponse = JSON.parse(body);
//               if (!myresponse.flag) {
//                 var errorlog = [];
//                 if (fs.existsSync(configs.opFailedCourseListByAPI)) {
//                   errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
//                   errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
//                 }
//                 fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
//               }
//               console.log("success save data! " + JSON.stringify(body));
//             });
//           }
//         }
//         catch (err) {
//           console.log("#### try-catch error--->" + err);
//         }
//         // const res = await awsUtil.putFileInBucket(filelocation);
//         // console.log(funcName + 'S3 object location = ' + res);
//       }
//       return resJsonData;
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       throw (error);
//     }
//   }

//   // scrape each course details
//   static async scrapeAndFormatCourseDetails(courseDict) {
//     const funcName = 'scrapeAndFormatCourseDetails ';
//     let s = null;
//     try {
//       s = new Scrape();
//       await s.init({ headless: true });
//       await s.setupNewBrowserPage(courseDict.href);

//       //swithc to international student
//       const switchInternational = await s.page.$x("//*[@id='main']//a[text()='INTERNATIONAL']");
//       for (let link of switchInternational) {
//         console.log('link :' + link);
//         await link.click();
//         await s.page.waitForNavigation();
//       }

//       Scrape.validateParams([courseDict]);
//       // get course href and innerText from courseDict and ensure it is valid
//       if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
//         console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
//         throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
//       }
//       const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
//       console.log(funcName + 'courseTitle = ' + courseTitle);
//       console.log(funcName + 'courseUrl = ' + courseUrl);
//       // output course data and file path
//       let courseScrappedData = null;
//       // create courseId from course title
//       const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
//       // validate generated courseId
//       console.log(funcName + 'courseId = ' + courseId);
//       if (!(courseId && courseId.length > 0)) {
//         console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
//         throw (new Error('Invalid courseId, courseId = ' + courseId));
//       }
//       // configure output file path as per UpdateItem configuration
//       const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
//       // decide if need to scrape or read data from local outpurfile
//       if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
//         console.log(funcName + 'reading scrapped data from local output file...');
//         const fileData = fs.readFileSync(opCourseFilePath);
//         if (!fileData) {
//           console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
//           throw (new Error('Invalid fileData, fileData = ' + fileData));
//         }
//         courseScrappedData = JSON.parse(fileData);
//         return courseScrappedData;
//       } // if (appConfigs.shouldTakeFromOutputFolder)
//       // Scrape course data as per selector file
//       if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
//         console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
//         courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
//       } else {
//         console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
//         courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
//       }
//       if (!courseScrappedData) {
//         throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
//       }
//       const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
//       const finalScrappedData = await Course.removeKeys(formattedScrappedData);
//       if (s) {
//         await s.close();
//       }
//       return finalScrappedData;
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       if (s) {
//         await s.close();
//       }
//       throw (error);
//     }
//   } // scrapeCourseDetails
// } // class
// module.exports = { ScrapeCourse };