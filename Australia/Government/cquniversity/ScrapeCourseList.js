const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');


class ScrapeCourseList extends Scrape {
  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  static async scrapeCourseListForPages(courseItemSelectorList, firstPageSelector, pageURL) {
    const funcName = 'scrapeCourseListForPages ';
    let s = null; let courseList = null;
    try {
      // validate params
      Scrape.validateParams([courseItemSelectorList, firstPageSelector, pageURL]);
      console.log(funcName + 'courseItemSelectorList = ' + JSON.stringify(courseItemSelectorList));
      // create Scrape instance
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(pageURL);
      let nextElementSiblingHandle = null;
      courseList = [];
      const activePaginationItemSel = firstPageSelector;
      let pageCount = 1;

      do {
        await s.page.waitFor(3000);
        // wait for sel
        console.log(funcName + 'waitForSelector sel = ' + activePaginationItemSel);
        await s.page.waitForSelector(activePaginationItemSel, { visible: true });
        // scrape all course list items
        console.log(funcName + '\n\r ********** Scraping for page  ' + pageCount + ' ********** \n\r');
        const resList = [];

        console.log(funcName + 'waitForSelector sel = ' + courseItemSelectorList[0]);
        await s.page.waitForSelector(courseItemSelectorList[0]);

        const anchorEleHandle = await s.page.$(courseItemSelectorList[0]); // anchor element handle
        console.log(funcName + 'anchorEleHandle = ' + anchorEleHandle);

        const hrefList = await s.scrapeAnchorElement(courseItemSelectorList[0], null, '');
        // console.log(funcName + 'hrefList = ' + JSON.stringify(hrefList));
        for (const itemDict of hrefList) {
          const courseDict = {};
          courseDict.href = itemDict.href;
          const itemInnerText = itemDict.innerText;
          const innerTextList = String(itemInnerText).split('\n');
          console.log(funcName + 'innerTextList = ' + JSON.stringify(innerTextList));
          if (innerTextList && innerTextList.length > 0) {
            courseDict.innerText = innerTextList[0];
          }
          console.log(funcName + 'courseDict = ' + JSON.stringify(courseDict));
          resList.push(courseDict);
        } // for

        courseList = courseList.concat(resList);
        // console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
        const eleList = await s.page.$$(activePaginationItemSel);
        if (Array.isArray(eleList) && eleList.length > 0) {
          const activePageButton = eleList[0];
          // reassign next sibling handle
          nextElementSiblingHandle = await activePageButton.getProperty('nextElementSibling');
          console.log(funcName + 'nextElementSiblingHandle = ' + nextElementSiblingHandle);
          if (nextElementSiblingHandle == "JSHandle@node") {
            const propHandle = nextElementSiblingHandle.getProperty('nodeName');
            console.log(funcName + 'propHandle = ' + propHandle);
            let nextSiblingNodeName = null;
            await propHandle.then((res) => {
              console.log('res = ' + res);
              const resJSon = res.jsonValue();
              console.log('resJSon = ' + resJSon);
              resJSon.then((resB) => {
                console.log('resB = ' + resB);
                nextSiblingNodeName = resB;
              });
            });
            console.log(funcName + 'nextSiblingNodeName = ' + nextSiblingNodeName);
            if (String(nextSiblingNodeName) !== 'LI') { // if next is not Anchor element
              console.log(funcName + 'nextElementSiblingHandle includes null....so breaking the loop...');
              break;
            }
            if (nextElementSiblingHandle) {
              await nextElementSiblingHandle.click('middle');
              console.log(funcName + 'Clicked nextElementSiblingHandle..');
            } else {
              throw (new Error('nextElementSiblingHandle invalid'));
            }
          }
          else { break; }
        }

        pageCount += 1;
        console.log(funcName + 'courseList count = ' + courseList.length);
      } while (nextElementSiblingHandle);

      console.log(funcName + 'total pages scrapped = ' + pageCount);
      console.log(funcName + 'courseList count = ' + courseList.length);
      // console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(courseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      if (s) {
        await s.close();
      }
    } catch (error) {
      console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r\n\r');
      console.log(funcName + 'try-catch error = ' + error);
      console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r\n\r');
      if (s) {
        await s.close();
      }
      console.log(funcName + 'writing courseList to tmp file....');
      await fs.writeFileSync(configs.opCourseListTempFilepath, JSON.stringify(courseList));
      console.log(funcName + 'writing courseList to tmp file completed successfully....');
      throw (error);
    }
  }

  // scrape course page list of the univ
  async scrapeCourseListAndPutAtS3(selFilepath) {
    const funcName = 'scrapeCourseListAndPutAtS3 ';
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      const pageUrl = this.selectorJson.url;
      console.log(funcName + 'url = ' + pageUrl);
      const jsonDictElements = this.selectorJson.elements;
      console.log(funcName + 'elements = ' + JSON.stringify(jsonDictElements));
      let courseList = null;
      if (Array.isArray(jsonDictElements)) {
        for (const eleDict of jsonDictElements) {
          const eleType = eleDict.elementType;
          console.log(funcName + 'elementType = ' + eleType);
          // ensure type is matching
          if (eleType === Scrape.ELEMENT_TYPE.COURSE_LIST) {
            const courseItemSelector = eleDict.course_item_selector;
            console.log(funcName + 'course_item_selector = ' + courseItemSelector);
            const pageSelector = eleDict.page_selector;
            console.log(funcName + 'page_selector = ' + pageSelector);
            courseList = await ScrapeCourseList.scrapeCourseListForPages(courseItemSelector, pageSelector, pageUrl);
          } // if
        } // for elements
      } // if
      return courseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  // refers to http://www.utas.edu.au/courses
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // course_level_form_selector key
      // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelList) {
      //   console.log(funcName + 'Invalid formSelList');
      //   throw (new Error('Invalid formSelList'));
      // }
      // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelElementDict) {
      //   console.log(funcName + 'Invalid formSelElementDict');
      //   throw (new Error('Invalid formSelElementDict'));
      // }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }

        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');

      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // scrape course category list wise
  async scrapeCourseListCategorywise() {
    const funcName = 'scrapeCourseListCategorywise ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];
      await s.setupNewBrowserPage("https://www.cqu.edu.au/courses");
      var mainCategory = [], mainCategoryurl = [], studylevelurl = [];

      // // const internation ="//*[@class='flexContainer']//fieldset/ul/li[2]/label";
      // // var clickarea11 = await s.page.$x(internation);
      // await clickarea11[0].click();
       const studyleveltabclick ="//*[@id='main']/div/div/div/nav/ul/li[3]/label";
       var studyclick=await s.page.$x(studyleveltabclick)
       await studyclick[0].click();
       var levelselector="//*[@id='main']/div[1]/div/div/nav/ul/li[3]/div/ul/li/a[not(contains(@href,'https://www.cqu.edu.au/courses/study-information/work-and-study-preparation'))]"
       var categoryselector="//*[@id='main']/div/div/article/div/div/div/div/a"
      // const activeselectors = "//*[@id='main']/div/div/article/div[2]/div[3]/ul/li[@class='pagination-list__item active']";
       var level=await s.page.$x(levelselector)
      for(let j=0;j<level.length;j++){
        var studylevelstring=await s.page.evaluate(el=>el.innerText,level[j])
        var studylevellink=await s.page.evaluate(el=>el.href,level[j])
        studylevelurl.push({href:studylevellink,innerText:studylevelstring})

      }
      console.log("Studylevel@@@",studylevelurl)


      for(let i=0;i<studylevelurl.length;i++){
        await s.page.goto(studylevelurl[i].href, { timeout: 0 });
        var category=await s.page.$x(categoryselector)
        for(let k=0;k<category.length;k++){
          var categorystring=await s.page.evaluate(el=>el.innerText,category[k])
          var categoryurl=await s.page.evaluate(el=>el.href,category[k])
          mainCategory.push({href:categoryurl,innerText:categorystring,study_level:studylevelurl[i].innerText})
        }

      }
      // console.log("maincategory@@@",mainCategory)

      // for (let catc of mainCategory) {
      //   var ispage = true;
      //     console.log("clicked <3 ##" + catc.innerText);
      //     //await s.page.waitForNavigation(),
      //     await s.page.goto(catc.href, { timeout: 0 });
      //     while (ispage) {
      //       const selector = "//*[@id='course_results']/div/div/div[1]/div/a";
      //       const international ="//*[@id='course_results']/div/div/div/ul/li[4]"
      //       var courses = await s.page.$x(international);

      //       var course = await s.page.$x(selector);
      //       for (let link of course) {
      //         var internationalselector=await s.page.evaluate(el => el.innerText, courses[0]);
      //         if(internationalselector=="Domestic and International" || internationalselector=="International" ){
      //           var coursestring = await s.page.evaluate(el => el.innerText, link);
      //           var courseurl = await s.page.evaluate(el => el.href, link);
      //           totalCourseList.push({ href: courseurl, innerText: coursestring, category: catc.innerText, study_level: catc.study_level });
      //         }

      //       }
      //       const active = await s.page.$x(activeselectors);
      //       if (active[0]) {
      //         const next = await s.page.evaluateHandle(el => el.nextElementSibling, active[0]);
      //         if (next) {
      //           console.log(next.toString())
      //         }

      //         if (next && next.toString() != "JSHandle:null") {
      //           var nodename = await (await next.getProperty('nodeName')).jsonValue();
      //           console.log("Next-->" + nodename)
      //           if (nodename == "LI") {
      //            await next.click();

      //             await s.page.waitFor(10000);
      //           }
      //           else {
      //             ispage = false;
      //           }
      //         }
      //         else {
      //           ispage = false;
      //           console.log("null node-->")
      //         }
      //       }
      //       else {
      //         // console.log("no active-->" + active[0].toString())
      //         ispage = false;
      //       }
      //     }
      // }



      // await fs.writeFileSync("./course_list/main_category_courses.json", JSON.stringify(mainCategory))
      // await fs.writeFileSync("./course_list/cquniversity_original_courselist.json", JSON.stringify(totalCourseList));

      // let uniqueUrl = [];
      // //unique url from the courselist file
      // for (let i = 0; i < totalCourseList.length; i++) {
      //   let cnt = 0;
      //   if (uniqueUrl.length > 0) {
      //     for (let j = 0; j < uniqueUrl.length; j++) {
      //       if (totalCourseList[i].href == uniqueUrl[j].href) {
      //         cnt = 0;
      //         break;
      //       } else {
      //         cnt++;
      //       }
      //     }
      //     if (cnt > 0) {
      //       uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [],study_level:totalCourseList[i].study_level });
      //     }
      //   } else {
      //     uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [],study_level:totalCourseList[i].study_level });
      //   }
      // }

      // await fs.writeFileSync("./course_list/cquniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      // //based on unique urls mapping of categories
      // for (let i = 0; i < totalCourseList.length; i++) {
      //   for (let j = 0; j < uniqueUrl.length; j++) {
      //     if (uniqueUrl[j].href == totalCourseList[i].href) {
      //       uniqueUrl[j].category.push(totalCourseList[i].category);
      //     }
      //   }
      // }
      // console.log("totalCourseList -->", uniqueUrl);
      // await fs.writeFileSync("./course_list/cquniversity_courselist.json", JSON.stringify(uniqueUrl));


      const maincategoryselector = "//*[@id='main']/div[1]/div/div/nav/ul/li[2]/div/ul/li/a";
      const studyareatabclick = "//*[@id='main']/div/div/div/nav/ul/li[2]/label";
      const activeselector = "//*[@id='main']/div/div/article/div[2]/div[3]/ul/li[@class='pagination-list__item active']";
      var clickarea = await s.page.$x(studyareatabclick);
      await clickarea[0].click();
      var category = await s.page.$x(maincategoryselector);
      console.log("Total categories-->" + category.length);
      for (let link of category) {
        var categorystringmain = await s.page.evaluate(el => el.innerText, link);
        mainCategory.push(categorystringmain.trim());
        var categorystringmainurl = await s.page.evaluate(el => el.href, link);
        mainCategoryurl.push({ href: categorystringmainurl.trim(), innerText: categorystringmain.trim() });
      }
      for (let catc of mainCategoryurl) {
        var ispage = true;
        console.log("clicked <3 ##" + catc.innerText);
        await s.page.goto(catc.href, { timeout: 0 });
        while (ispage) {
          const selector = "//*[@id='course_results']/div/div/div[1]/div/a";
          var category = await s.page.$x(selector);
          for (let link of category) {
            var categorystring = await s.page.evaluate(el => el.innerText, link);
            var categoryurl = await s.page.evaluate(el => el.href, link);
            totalCourseList.push({ href: categoryurl, innerText: categorystring + "(" + catc.innerText + ")", category: catc.innerText });
          }
          const active = await s.page.$x(activeselector);
          if (active[0]) {
            const next = await s.page.evaluateHandle(el => el.nextElementSibling, active[0]);
            if (next) {
              console.log(next.toString())
            }

            if (next && next.toString() != "JSHandle:null") {
              var nodename = await (await next.getProperty('nodeName')).jsonValue();
              console.log("Next-->" + nodename)
              if (nodename == "LI") {
                await next.click();

                await s.page.waitFor(10000);
              }
              else {
                ispage = false;
              }
            }
            else {
              ispage = false;
              console.log("null node-->")
            }
          }
          else {
            // console.log("no active-->" + active[0].toString())
            ispage = false;
          }
        }
      }
      await fs.writeFileSync("./course_list/main_category_courses.json", JSON.stringify(mainCategory))
      await fs.writeFileSync("./course_list/cquniversity_original_courselist.json", JSON.stringify(totalCourseList));

      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
        }
      }

      await fs.writeFileSync("./course_list/cquniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./course_list/cquniversity_courselist.json", JSON.stringify(uniqueUrl));



      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
