**Australian National University: Total Courses Details As On 26 Feb 2019**
* Total Courses = 286
* Courses without CRICOS code = --
* DDB = 0
* Web portal available courses = --


 <!-- {
            "url": "",
            "elements": [
                {
                    "elementType": "scrape_by_xpath",
                    "selectors": [
                        "//*/p[contains(text(),'CRICOS code')]",
                        "//*/p[contains(text(),'CRICOS code(s)')]",
                        "//*[@id='entry-requirements-2']/div[2]/div[2]/div[2]/ul/li"
                   
                        
                        
                    ]
                }
            ]
        },

         "//*[@id='entry-requirements-2']/div[2]/div[2]/div[2]/ul/li" -->

        

{
        "href": "https://www.monash.edu/study/courses/find-a-course/2020/medicine,-nursing-and-health-sciences-0047?international=true",
        "innerText": "Medicine, Nursing and Health Sciences",
        "category": [
            "Medicine, nursing & health sciences"
        ],
        "reason": "Error: Campus Location not found",
        "index": 243
    },

{
        "href": "https://www.monash.edu/study/courses/find-a-course/2020/occupational-and-environmental-health-m5018?international=true",
        "innerText": "Occupational and Environmental Health",
        "category": [
            "Medicine, nursing & health sciences"
        ],
        "reason": "Error: Campus Location not found",
        "index": 246
    },

