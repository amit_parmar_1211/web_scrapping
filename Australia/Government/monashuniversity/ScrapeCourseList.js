const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');

const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
      const categoryselectorUrl = "#main > div.content-wrapper.bg-white.l--content-rhs > div > div.content-inner__main > div.study-info-extra-courses > div > div > div > div > a";
      const categoryselectorText = "#main > div.content-wrapper.bg-white.l--content-rhs > div > div.content-inner__main > div.study-info-extra-courses > div > div > div > div > a";
      var elementstring = "", elementhref = "", allcategory = [];

      const targetLinksCardsUrls = await page.$$(categoryselectorUrl);
      const targetLinksCardsText = await page.$$(categoryselectorText);

      var targetLinksCardsTotal = [];
      for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        elementstring = await page.evaluate(el => el.innerText, targetLinksCardsText[i]);
        elementhref = await page.evaluate(el => el.href, targetLinksCardsUrls[i]);
        targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
      }
      await fs.writeFileSync("./course_list/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
      console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));
      let linkselector = "#accordion__target-50 > div > div > div > div > div > h2 > a";
      let textselector = "#accordion__target-50 > div > div > div > div > div > h2 > a"
      var totalCourseList = [];
      for (let target of targetLinksCardsTotal) {
        await page.goto(target.href + "&international=true" + "&f.Study+mode%7CcourseMode=On-campus", { timeout: 0 });
       
        let educationLevelCat = "#main > div.bg-light-grey.l--lhs-content > div > div.content.bg-white > div.content-inner__main > div.bottom-tabs.search-tabs > div.search-tabs__wrap.mobile-hidden > ul > li > a:not([alt*='Professional development'])";
        let scrapedCategory = await page.$$(educationLevelCat);
        var educationLevelCatList = [];
        let elementstring1 = "";
        let elementhref1 = "";
        for (let i = 0; i < scrapedCategory.length; i++) {
          elementstring1 = await page.evaluate(el => el.innerText, scrapedCategory[i]);
          elementhref1 = await page.evaluate(el => el.href, scrapedCategory[i]);
          educationLevelCatList.push({ href: elementhref1, innerText: elementstring1 });
        }
        console.log("#### main courses---" + JSON.stringify(educationLevelCatList));

        for (let educationLevel of educationLevelCatList) {
          await page.goto(educationLevel.href, { timeout: 0 });
          const targetLinks = await page.$$(linkselector);
          const targetText = await page.$$(textselector);

          console.log("target.innerText -->", target.innerText);
          console.log("#total link selectors---->" + targetLinks.length);
          for (var j = 0; j < targetLinks.length; j++) {
            var elementstring = await page.evaluate(el => el.innerText, targetText[j]);
            const elementlink = await page.evaluate(el => el.title, targetLinks[j]);
           
            let payload = { href: elementlink, innerText: elementstring, category: target.innerText}
            let res = totalCourseList.some(value => {
              if (value.href.trim() == payload.href.trim() && value.category.trim() == payload.category.trim()) {
                return true;
              } else {
                return false
              }
            });
            if (res == false) {
              totalCourseList.push(payload);
            } else {
              
            }

          }
        }

      }
      console.log("totalCourseList -->", totalCourseList);
      await fs.writeFileSync("./course_list/monashuniversity_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
     

      await fs.writeFileSync("./course_list/monashuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./course_list/monashuniversity_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }


  // refers to https://programsandcourses.anu.edu.au/catalogue
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

     
      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
       
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
    
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
