const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                                console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                                if (!configs.univ_id && configs.univ_id.length > 0) {
                                    console.log(funcName + 'configs.univ_id must have valid value');
                                    throw (new Error('configs.univ_id must have valid value'));
                                }
                                resJsonData.univ_id = configs.univ_id;
                                break;
                            }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = course_category;
                            break;
                        }
                        case 'ielts_require':
                        case 'pbt_require':
                        case 'ibt_require':
                        case 'course_toefl_ielts_score': { 
                            const courseAdminReq = {};
                            const englishList = [];


                            const ielts_require_heading = courseScrappedData.ielts_require_heading;
                            console.log("EntryRequirementielts_require_heading:::" + ielts_require_heading);
                            let resielts_require_headingDetailsJson;
                            if (Array.isArray(ielts_require_heading)) {
                                for (const rootEleDict of ielts_require_heading) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resielts_require_headingDetailsJson = selList;
                                              
                                                console.log("resielts_require_headingDetailsJson:::" + resielts_require_headingDetailsJson);
                                            }
                                        }
                                    }
                                }
                            }

                            const ielts_require = courseScrappedData.ielts_require;
                            console.log("EntryRequirementielts_require:::" + ielts_require);
                            let resielts_requireDetailsJson = null;
                            if (Array.isArray(ielts_require)) {
                                for (const rootEleDict of ielts_require) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resielts_requireDetailsJson = selList;
                                                
                                                console.log("resielts_requireDetailsJson:::" + resielts_requireDetailsJson);
                                            }
                                        }
                                    }
                                }
                            }
                        
                            const pte_require = courseScrappedData.pte_require;
                            console.log("EntryRequirementpte_require:::" + pte_require);
                            let respte_requireDetailsJson = null;
                            if (Array.isArray(pte_require)) {
                                for (const rootEleDict of pte_require) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                respte_requireDetailsJson = selList;
                                                console.log("respte_requireDetailsJson:::" + respte_requireDetailsJson);
                                            }
                                        }
                                    }
                                }
                            }

                            const ibt_require = courseScrappedData.ibt_require;
                            console.log("EntryRequirementibt_require:::" + ibt_require);
                            let resibt_requireDetailsJson = null;
                            if (Array.isArray(ibt_require)) {
                                for (const rootEleDict of ibt_require) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resibt_requireDetailsJson = selList;
                                                console.log("resibt_requireDetailsJson:::" + resibt_requireDetailsJson);
                                            }
                                        }
                                    }
                                }
                            }

                            const penglishList = [];
                            if (resielts_requireDetailsJson != null) {

                                for (let i = 0; i < resielts_requireDetailsJson.length; i++) {
                                    let pieltsDict = {};
                                    console.log("ieltsscore -->", resielts_requireDetailsJson[i]);
                                    if (resielts_requireDetailsJson[i]) {
                                        if (resielts_require_headingDetailsJson[i] != null && resielts_require_headingDetailsJson[i].length > 0) {

                                           // englishList.push({ name: "ielts academic (" + resielts_require_headingDetailsJson[i] + ")", description: resielts_requireDetailsJson[i].trim() });
                                            pieltsDict.name = "ielts academic (" + resielts_require_headingDetailsJson[i] + ")";
                                            pieltsDict.description = resielts_requireDetailsJson[i].trim();
                                            pieltsDict.min = 0;
                                            pieltsDict.require = await utils.giveMeNumber(resielts_requireDetailsJson[i].trim());
                                            pieltsDict.max = 9;
                                            pieltsDict.R = 0;
                                            pieltsDict.W = 0;
                                            pieltsDict.S = 0;
                                            pieltsDict.L = 0;
                                            pieltsDict.O = 0;
                                            englishList.push(pieltsDict);
                                            console.log(" pieltsDict.name", pieltsDict.name);
                                        } else {
                                            throw new Error("HEADING for IELTS not found");
                                        }
                                       
                                    }
                                }
                               // console.log("penglishList -->", penglishList);
                            }

                            if (respte_requireDetailsJson != null) {
                                for (let i = 0; i < respte_requireDetailsJson.length; i++) {
                                    let ppteDict = {};
                                    console.log("ptesscore -->", respte_requireDetailsJson[i]);
                                    if (respte_requireDetailsJson[i]) {
                                        if (resielts_require_headingDetailsJson[i] != null && resielts_require_headingDetailsJson[i].length > 0) {
                                            //englishList.push({ name: "toefl pbt (" + resielts_require_headingDetailsJson[i] + ")", description: respbt_requireDetailsJson[i].trim() });
                                            ppteDict.name = "pte (" + resielts_require_headingDetailsJson[i] + ")";
                                            ppteDict.description=respte_requireDetailsJson[i].trim()
                                            ppteDict.min = 310;
                                            ppteDict.require = await utils.giveMeNumber(respte_requireDetailsJson[i].trim());
                                            ppteDict.max =677;
                                            ppteDict.R = 0;
                                            ppteDict.W = 0;
                                            ppteDict.S = 0;
                                            ppteDict.L = 0;
                                            ppteDict.O = 0;
                                            englishList.push(  ppteDict);
                                            console.log("ppteDict.name", ppteDict.name);
                                        } else {
                                            throw new Error("HEADING for pte not found");
                                        }
                                       
                                    }
                                }
                               //console.log("pbt_penglishList -->", penglishList);
                            }

                            if (resibt_requireDetailsJson != null) {
                                for (let i = 0; i < resibt_requireDetailsJson.length; i++) {
                                    const pibtDict = {};
                                    console.log("pbtsscore -->", resibt_requireDetailsJson[i]);
                                    if (resibt_requireDetailsJson[i]) {
                                        if (resielts_require_headingDetailsJson[i] != null && resielts_require_headingDetailsJson[i].length > 0) {
                                           // englishList.push({ name: "toefl ibt (" + resielts_require_headingDetailsJson[i] + ")", description: resibt_requireDetailsJson[i].trim() });
                                            pibtDict.name = "toefl ibt (" + resielts_require_headingDetailsJson[i] + ")";
                                            pibtDict.description=resibt_requireDetailsJson[i].trim()
                                            pibtDict.min = 0;
                                            pibtDict.require = await utils.giveMeNumber(resibt_requireDetailsJson[i].trim());
                                            pibtDict.max =120;
                                            pibtDict.R = 0;
                                            pibtDict.W = 0;
                                            pibtDict.S = 0;
                                            pibtDict.L = 0;
                                            pibtDict.O = 0;
                                            englishList.push(pibtDict);
                                            console.log("pibtDict.name", pibtDict.name);
                                        } else {
                                            throw new Error("HEADING for toefl ibt not found");
                                        }
                                        
                                    }
                                }
                            

                            }


                            if (englishList && englishList.length > 0) {
                               
                                    
                            let ieltsmax = [];
                            let finalielts =[];
                                    var ielindex =[];
                                    let finaltofel = [];
                                var tofelindex = [];
                                for(let item of englishList){
                                    if(item.description.includes("IELTS") === true){
                                        ielindex.push(englishList.indexOf(item));
                                     console.log("chkin>",ielindex);
                                     finalielts.push(item.description);
                                     }
                                    if(item.description.includes("TOEFL") === true){
                                        tofelindex.push(englishList.indexOf(item));
                                        console.log("chktof>",ielindex);
                                        finaltofel.push(item.description);
                                    }
                                    else{
                                       // englishList = englishList;
                                    }
                                    }

                                     let a; 
                                     let c; let d;
                                     let b;let naindex; let tofindex;
                                     var regEx = /[+-]?\d+(\.\d+)?/g;
                                   
                                            if(ielindex.length > 1){
                                            for(let i= 0; i<= ielindex.length; i++ ){
                                                a = String(finalielts[i]).match(regEx);
                                                b = String(finalielts[i + 1]).match(regEx);
                                             
                                                     if(JSON.stringify(a) > JSON.stringify(b)){
                                                        if(a != null && a!= undefined){
                                                            naindex = finalielts.indexOf(finalielts[i + 1]);
                                                            englishList.splice(naindex, 1);
                                                            console.log("a>",JSON.stringify(a));
                                                            
                                                        }else{
                                                            break;
                                                        }
                                                     }
                                                     else if(JSON.stringify(b) > JSON.stringify(a)){
                                                        
                                                        if(b != null && b!= undefined){
                                                            naindex = finalielts.indexOf(finalielts[i]);
                                                            englishList.splice(naindex, 1);
                                                            console.log("b>",JSON.stringify(b));
                                                          
                                                        }else{
                                                            break;
                                                        }
                                                     }
                                                    }
                                                }
                                                if(tofelindex.length > 1){
                                                    for(let i= 1; i<= tofelindex.length; i++ ){
                                                        c = String(finaltofel[i - 1]).match(regEx);
                                                        d = String(finaltofel[i ]).match(regEx);
                                                    
                                                             if(JSON.stringify(c) > JSON.stringify(d)){
                                                                if(c != null && c!= undefined){
                                                                   
                                                                    tofindex=i + 1;
                                                                    englishList.splice(tofindex, 1);
                                                                    console.log("c>",JSON.stringify(c));
                                                                    
                                                                }else{
                                                                    break;
                                                                }
                                                             }
                                                            else if(JSON.stringify(d) > JSON.stringify(c)){
                                                                
                                                               if(d != null && d!= undefined){
                                                                   tofindex = i;
                                                                   englishList.splice(tofindex, 1);
                                                                    console.log("d>",JSON.stringify(d));
                                                                  
                                                               }else{
                                                                   break;
                                                                }
                                                            }
                                                            }
                                                }
                                                else{
                                                  //  englishList =englishList;
                                                }
                                                    
                                        console.log("finalielts" ,englishList); 
                                        
                                      courseAdminReq.english = englishList;

                        
                               console.log("englishList_penglishList -->", courseAdminReq.english);
                            }
                            else {
                                courseAdminReq.english = [];
                            }
                            console.log("IELST Academic requirment :");
                            if (courseScrappedData.course_academic_requirement) {
                                console.log(funcName + 'matched case course_academic_requirment: ' + key);
                                var flagacademic = false;
                                const courseKeyVal = courseScrappedData.course_academic_requirement;
                                var course_academic_requirment = [];
                                console.log("course_academic_requirement details -->" + JSON.stringify(courseKeyVal));
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict course_academic_requirement = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict course_academic_requirement = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList course_academic_requirement = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    course_academic_requirment = selList;
                                                }
                                            }
                                        }
                                    }
                                } // rootElementDictList
                                // add only if it has valid value
                                if (course_academic_requirment.length > 0) {
                                    flagacademic = false;
                                }
                                else {
                                    flagacademic = true;
                                }
                                if (flagacademic) {
                                    const courseKeyValone = courseScrappedData.course_academic_requirement_one;
                                    console.log('find course academic')
                                    if (Array.isArray(courseKeyValone)) {
                                        for (const rootEleDict of courseKeyValone) {
                                            console.log(funcName + '\n\r rootEleDict course_academic_requirement = ' + JSON.stringify(rootEleDict));
                                            const elementsList = rootEleDict.elements;
                                            for (const eleDict of elementsList) {
                                                console.log(funcName + '\n\r eleDict course_academic_requirement = ' + JSON.stringify(eleDict));
                                                const selectorsList = eleDict.selectors;
                                                for (const selList of selectorsList) {
                                                    console.log(funcName + '\n\r selList course_academic_requirement = ' + JSON.stringify(selList));
                                                    if (Array.isArray(selList) && selList.length > 0) {
                                                        course_academic_requirment = selList;
                                                    }
                                                }
                                            }
                                        }
                                    } // rootElementDictList
                                    // add only if it has valid value
                                }
                                courseAdminReq.academic = course_academic_requirment;
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            } else {
                                resJsonData.course_admission_requirement = [];
                            }
                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.course_entry_more_details;
                            console.log("EntryRequirement:::" + courseKeyVal);
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            // add english requirement 'english_requirements_url'
                            const courseKeyValEnglishReq = courseScrappedData.course_english_more_details;
                            console.log("EnglishRequirement:::" + courseKeyValEnglishReq);
                            let resEnglishReqJson = null;
                            if (Array.isArray(courseKeyValEnglishReq)) {
                                for (const rootEleDict of courseKeyValEnglishReq) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }


                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = resJsonData.course_url;
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = resJsonData.course_url;
                            }

                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_outline': {
              
                            let mgcrs = null;
                          
                         const majorcrs = {
                           majors:[],
                           minors:[],
                           more_details:""
                         };
                         //for major cousrseList
                           var coursemajor =  courseScrappedData.course_outline;
                           if (Array.isArray(coursemajor)) {
                             for (const rootEleDict of coursemajor) {
                                 console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                 const elementsList = rootEleDict.elements;
                                 for (const eleDict of elementsList) {
                                     console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                     const selectorsList = eleDict.selectors;
                                     for (const selList of selectorsList) {
                                         console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                         if (Array.isArray(selList) && selList.length > 0) {
                                           mgcrs = selList;
                                         }
                                     }
                                 }
                             }
                         }
                           if(mgcrs && mgcrs.length >0){
                             majorcrs.majors=mgcrs
                            }
                           else{
                             majorcrs.majors= majorcrs.majors
                           }
                              resJsonData.course_outline =  majorcrs;
                           //for minor courseList
                              var courseminor =  courseScrappedData.course_outline_minor;
                              let mincrs = null;
                              if (Array.isArray(courseminor)) {
                                for (const rootEleDict of courseminor) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                             mincrs = selList;
                                            }
                                        }
                                    }
                                }
                            }
                              if(mincrs && mincrs.length >0){
                                majorcrs.minors=mincrs
                              }
                              else{
                               majorcrs.minors=majorcrs.minors
                             }
                             //for more_details 
                             var more_outline = courseScrappedData.course_outline_more;
                             let mor_outline = [];
                             if (Array.isArray(more_outline)) {
                               for (const rootEleDict of more_outline) {
                                   console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                   const elementsList = rootEleDict.elements;
                                   for (const eleDict of elementsList) {
                                       console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                       const selectorsList = eleDict.selectors;
                                       for (const selList of selectorsList) {
                                           console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                           if (Array.isArray(selList) && selList.length > 0) {
                                            for( const sel of selList){
                                            console.log("more_outttt1"+ sel.href);
                                            mor_outline.push(sel.href);
                                            }
                                           }
                                           else{
                                            mor_outline.push(majorcrs.more_details);
                                           }
                                       }
                                   }
                               }
                           }
                            //for course_outline_more_details 
                            if(mor_outline.length > 0){
                          majorcrs.more_details = mor_outline;
                            }
                            else if(majorcrs.majors.length > 0){
                                majorcrs.more_details ="https://www.monash.edu/study/courses/majors-minors-specialisations?international=true";
                            }
                            else{
                              majorcrs.more_details = majorcrs.more_details;
                            }
                                 resJsonData.course_outline =  majorcrs;
              
                              console.log("major==>", resJsonData.course_outline)
                        
                          break;
                          }
                        case 'course_duration_full_time': {
                            const courseDurationList = [];
                            const courseDurationDisplayList = [];
                            const durationFullTime = {};

                            let anotherArray = [];
                            let durationFullTimeDisplay = null;
                            let duration1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time);

                            console.log("*************start formating Full Time years*************************");
                            
                         
                            console.log("courseScrappedData.course_duration_full_time = ", JSON.stringify(duration1));
                            let finalDuration = duration1;
                            console.log("Duration1 = ", finalDuration.trim());
                            var fromatedduration = String(finalDuration).trim();
                            console.log('Filter duration array :' + fromatedduration);
                            if (fromatedduration.indexOf('year/') > -1) {
                                var durationcheck = "";
                                console.log('Duartion found and replace');
                                durationcheck = fromatedduration.replace(' year/', '-');
                                console.log('duration value for month :' + durationcheck);
                                if (durationcheck.indexOf('years/') > -1) {
                                    durationcheck = durationcheck.replace(' years/', '-');
                                }

                                if (durationcheck.indexOf('years/') > -1) {
                                    durationcheck = durationcheck.replace(' years/', '-');
                                }

                                finalDuration = durationcheck;
                                console.log('Index of duration value :' + finalDuration);
                            }
                            else if (fromatedduration.indexOf('/') > -1) {
                                var durationcheck = "";
                                console.log('Duartion found and replace else if ');
                                durationcheck = fromatedduration.replace(' /', '-');
                                console.log('duration value for month else if:' + durationcheck);
                                if (durationcheck.indexOf('years/') > -1) {
                                    durationcheck = durationcheck.replace(' years/', '-');
                                }

                                if (durationcheck.indexOf('years') > -1) {
                                    durationcheck = durationcheck.replace(' years-', '-');
                                }
                                finalDuration = durationcheck;
                                console.log('Index of duration value else if :' + finalDuration);
                            }
                            else if (fromatedduration.indexOf(' or ') > -1) {
                                var durationcheck = "";
                                console.log('Duartion found and replace else if ');
                                durationcheck = fromatedduration.replace(' or ', '-');
                                console.log('duration value for month else if:' + durationcheck);
                                if (durationcheck.indexOf('years/') > -1) {
                                    durationcheck = durationcheck.replace(' years/', '-');
                                }

                                if (durationcheck.indexOf('years') > -1) {
                                    durationcheck = durationcheck.replace(' years-', '-');
                                }
                                if (durationcheck.indexOf(', ') > -1) {
                                    durationcheck = durationcheck.replace(', ', '-');
                                }

                                finalDuration = durationcheck;
                                console.log('Index of duration value else if :' + finalDuration);
                            }

                            const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration);
                            console.log("full_year_formated_data", full_year_formated_data);
                            ///course duration
                            const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                            console.log("not_full_year_formated_data", not_full_year_formated_data);



                            resJsonData.course_duration = not_full_year_formated_data;
                            try {
                                durationFullTime.duration_full_time = not_full_year_formated_data;
                                courseDurationList.push(durationFullTime);
                                ///END course duration
                                console.log("courseDurationList : ", courseDurationList);

                                courseDurationDisplayList.push(full_year_formated_data);
                                // anotherArray.push(courseDurationDisplayList);
                                console.log("courseDurationDisplayList", courseDurationDisplayList);
                                ///END course duration display
                            } catch (err) {
                                console.log("Problem in Full Time years:", err);
                            }
                            console.log("courseDurationDisplayList1", courseDurationDisplayList);
                            console.log('***************END formating Full Time years**************************')
                            let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                            console.log("filtered_duration_formated", filtered_duration_formated);
                            console.log("courseDurationDisplayList2", courseDurationDisplayList);
                            // if (courseDurationList && courseDurationList.length > 0) {

                            //     resJsonData.course_duration = courseDurationList;
                            // }
                            if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                //  let anotherArray = [];
                                resJsonData.course_duration_display = filtered_duration_formated;
                                var isfulltime = false, isparttime = false;
                                filtered_duration_formated.forEach(element => {
                                    if (element.display == "Full-Time") {
                                        isfulltime = true;
                                    }
                                    if (element.display == "Part-Time") {
                                        isparttime = true;
                                    }
                                });
                                resJsonData.isfulltime = isfulltime;
                                resJsonData.isparttime = isparttime;
                            }
                            break;
                        }
                        case 'course_campus_location': { 
                            
                            const courseKeyValcd = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            var getCricosCode = [];
                            if (Array.isArray(courseKeyValcd)) {
                                for (const rootEleDict of courseKeyValcd) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r cricos_code selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                              
                                                if (selList.length > 0) {
                                                    selList.forEach(element => {
                                                        console.log("selList -->", element);
                                                        if (element.indexOf(':') > -1) {
                                                            let splitCricos = element.split(':');
                                                            let final_cricos;
                                                            if (splitCricos[splitCricos.length - 1].length > 0) {
                                                                final_cricos = splitCricos[splitCricos.length - 1];
                                                                console.log("final_cricos -->", final_cricos);
                                                                getCricosCode.push(final_cricos.trim());
                                                            }
                                                        } else {
                                                            getCricosCode.push(element.trim());
                                                        }
                                                    });
                                                    course_cricos_code = getCricosCode;
                                                }
                                            }
                                        }
                                    }
                                }
                               
                            }
                           
                            if(course_cricos_code && course_cricos_code.length > 0){
                              
                                console.log("course_cricos_code -->", course_cricos_code);
                            }
                            else {
                                throw new Error('Cricos code not found');
                            }
                          
                            // Location Launceston
                            const campLocationText = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            console.log(funcName + 'campLocationText = ' + campLocationText);
                            if (campLocationText != null) {
                                if (campLocationText && campLocationText.length > 0) {
                                    var mylocationdata = await JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];

                                    var campuses = mylocationdata["campuses"];
                                    var location_data = [];
                                    campuses.forEach(element => {
                                        if (campLocationText.toLowerCase().trim().includes(element.toLowerCase())) {
                                            location_data.push(element);
                                        }
                                    });
                                    var campLocationValTrimmed = String(campLocationText).trim();
                                    // console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                    // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                        var campusedata = [];
                                        location_data.forEach(element => {
                                            campusedata.push({
                                                "name": this.titleCase(element),
                                               "code":String(course_cricos_code[0])
                                            })
                                        });
                                    resJsonData.course_campus_location = campusedata;// await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                                    // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //

                                    console.log("#campus lenght yes-->" + resJsonData.course_campus_location.length);
                                   // var study_mode = [];
                                   // study_mode.push('On campus');
                                    resJsonData.course_study_mode = "On campus";//.join(',');
                                    console.log("## FInal string-->" + location_data);


                                    if (location_data.length == 0) {
                                        throw new Error("Campus Location not found");
                                    }
                                    //}

                                } // if (campLocationText && campLocationText.length > 0)
                            }
                            else {
                                const cTitle = (course_cricos_code[0]);
                                console.log("CRICOSE@@@@@@", cTitle)
                              
                                const location = await format_functions.getMyLocation(cTitle)
                                

                                console.log("location", location)
                             
                               // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                        var campusedata = [];
                                       
                                            campusedata.push({
                                                "name": this.titleCase(location).trim(),
                                               "code":String(course_cricos_code[0])
                                            })
                                        
                                    resJsonData.course_campus_location = campusedata;// await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                                    // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //

                                    console.log("#campus lenght yes-->" + resJsonData.course_campus_location.length);
                                   // var study_mode = [];
                                   // study_mode.push('On campus');
                                    resJsonData.course_study_mode = "On campus";//.join(',');
                                    console.log("## FInal string-->" + location_data);


                                        }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {
                               
                            };
                            // feesDict.international_student = configs.propValueNotAvaialble;
                            // feesDict.fee_duration_years = configs.propValueNotAvaialble;
                            // feesDict.currency = configs.propValueNotAvaialble;

                            let feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            // console.log("feesdata",feesIntStudent);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feeYear && feeYear.length > 0) {
                                courseTuitionFee.year = feeYear;
                            }

                            console.log("feesIntStudentBefore -->", feesIntStudent);
                          
                            if (feesIntStudent && feesIntStudent.indexOf(':') > -1) {
                                let splitFees = feesIntStudent.split(':');
                                feesIntStudent = splitFees[1];
                            }
                            if(feesIntStudent && feesIntStudent.indexOf('-') > -1){
                                let splitFees = feesIntStudent.split('-');
                                feesIntStudent = splitFees[1];
                            }
                        
                            console.log("feesIntStudentAfter -->", feesIntStudent);
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    // const regEx = /\d/g;
                                    const regEx = /\d+\.?\d*/g; // /[0-9]/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student={
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: feesIntStudent
                                              
                                            }
                                        }
                                        if (feesNumber == "0") {
                                            console.log("FEesNumber = 0");
                                            feesDict.international_student={
                                                amount: 0,
                                                duration: 1,
                                                unit: "Year",
                                                description: ""
                                              
                                            }
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {


                                // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                                const cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                                const cricoseStr = cricos.replace(/CRICOS code: |\s \n/g, '');
                                console.log(funcName + 'cricoseStr = ' + cricoseStr);
                                console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                                // console.log("ahahahahahah",cricoseStr.length );
                                if (cricoseStr && cricoseStr.length > 0) {
                                    console.log("SDF");
                                    const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                    console.log("tuttion data", fixedFeesDict);
                                    if (fixedFeesDict) {
                                        for (const codeKey in fixedFeesDict) {
                                            console.log(funcName + 'codeKey = ' + codeKey);
                                            if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                                var keyVal = fixedFeesDict[codeKey];
                                                console.log(funcName + 'keyVal = ' + keyVal);
                                                if (cricoseStr.includes(codeKey)) {
                                                    var feesDictVal = fixedFeesDict[cricoseStr];
                                                    console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                    if (feesDictVal && feesDictVal.length > 0) {
                                                        const inetStudentFees = Number(feesDictVal);
                                                        console.log(funcName + 'inetStudentFees = ' + feesDictVal);
                                                        if (feesDictVal) {
                                                            // const regEx = /\d/g;
                                                            const regEx = /\d+\.?\d*/g; // /[0-9]/g;
                                                            let feesValNum = feesDictVal.match(regEx);
                                                            if (feesValNum) {
                                                                console.log(funcName + 'feesValNum = ' + feesValNum);
                                                                feesValNum = feesValNum.join('');
                                                                console.log(funcName + 'feesValNum = ' + feesValNum);
                                                                let feesNumber = null;
                                                                if (feesValNum.includes(',')) {
                                                                    feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                                                } else {
                                                                    feesNumber = feesValNum;
                                                                }
                                                                console.log(funcName + 'feesNumber = ' + feesNumber);
                                                                feesDict.international_student={
                                                                    amount: Number(feesNumber),
                                                                    duration: 1,
                                                                    unit: "Year",
                                                                    description: feesDictVal
                                                                   
                                                                }

                                                            }
                                                        }



                                                    }
                                                }
                                            }
                                        } // for
                                    } // if
                                    else {
                                        feesDict.international_student={
                                            amount: 0,
                                            duration: 1,
                                            unit: "Year",
                                            description: "Not given fee"
                                        }
                                    }
                                }
                            } // if
                            console.log("NEwFEESS", JSON.stringify(feesDict.international_student))
                         //   let international_student_all_fees_array = [];
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                  //  feesDict.international_student_all_fees = international_student_all_fees_array

                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                console.log("(feesIntStudent.length > 0) -->", feesIntStudent);
                                if (feesDict.international_student.length > 0) { // extract only digits
                                    console.log("international_student -->" + feesIntStudent)
                                    const courseKeyVal = courseScrappedData.course_tuition_fees_international_student_more_details;
                                    console.log('value of feee :' + courseKeyVal);
                                    if (Array.isArray(courseKeyVal)) {
                                        for (const rootEleDict of courseKeyVal) {
                                            console.log(funcName + '\n\r rootEleDict fee more= ' + JSON.stringify(rootEleDict));
                                            const elementsList = rootEleDict.elements;
                                            for (const eleDict of elementsList) {
                                                console.log(funcName + '\n\r eleDict fee more= ' + JSON.stringify(eleDict));
                                                const selectorsList = eleDict.selectors;
                                                for (const selList of selectorsList) {
                                                    console.log(funcName + '\n\r selList fee more= ' + JSON.stringify(selList));
                                                    if (Array.isArray(selList) && selList.length > 0) {
                                                        feesDict.international_student_all_fees = selList[0];
                                                        console.log('course fee more details :' + selList[0]);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //feesDict.international_student_all_fees = feesIntStudent;
                                }

                                if (feesDict) {
                                    // feesList.push(feesDict);
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                                if (!feesDict.international_student) {
                                    console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                    console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                    console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                    console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                    return null; // this will add this item into FailedItemList and writes file to local disk
                                }
                            }
                            break;
                        }
                        case 'course_intake': {
                            const courseIntakeDisplay = {};
                            var courseIntakeStr = [];
                            const courseKeyVal = courseScrappedData.course_intake;
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                courseIntakeStr = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("intakes" + JSON.stringify(courseIntakeStr));
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                // var campus = resJsonData.course_campus_location;
                                var locations = resJsonData.course_campus_location;
                                var intakes = [];
                                for (let location of locations) {
                                    var myintakedata = {};
                                    myintakedata.name = location.name;
                                    var myvalues = [];
                                    var myintakestr = courseIntakeStr[0].split(",");
                                    console.log("intake information :" + myintakestr);
                                    for (var s = 0; s < myintakestr.length; s++) {
                                        console.log("Intake duration for loop :" + myintakestr[s]);

                                        var intakeinfo = myintakestr[s];

                                        console.log("Intake information details :" + intakeinfo.toLowerCase().trim());

                                        if (intakeinfo.toLowerCase().trim().indexOf("first semester") != -1) {
                                            console.log("first semester");
                                            var month = intakeinfo.split("(");
                                            myvalues.push(month[1].replace(/[()]+/g, '').trim())
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("second semester") != -1) {
                                            console.log("second semester");
                                            var month = intakeinfo.split("(");
                                            console.log("xyz", month);
                                            myvalues.push(month[1].replace(/[()]+/g, '').trim())
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("third semester") != -1) {
                                            console.log("third semester");
                                            var month = intakeinfo.split("(");
                                            myvalues.push(month[1].replace(/[()]+/g, '').trim())
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("first trimester") != -1) {
                                            console.log("first trimester");
                                            var month = intakeinfo.split("(");
                                            myvalues.push(month[1].replace(/[()]+/g, '').trim())
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("second trimester") != -1) {
                                            console.log("second trimester");
                                            var month = intakeinfo.split("(");
                                            myvalues.push(month[1].replace(/[()]+/g, '').trim())
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("third trimester") != -1) {
                                            console.log("third trimester");
                                            var month = intakeinfo.split("(");
                                            myvalues.push(month[1].replace(/[()]+/g, '').trim())
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("early july") != -1) {
                                            console.log("early july");
                                            myvalues.push("Early Of July");
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("throughout") != -1) {
                                            console.log("Anytime");
                                            myvalues.push("AnyTime");
                                        }

                                        if (intakeinfo.toLowerCase().trim().indexOf("malaysia") != -1) {
                                            console.log("malaysia");
                                            var month = intakeinfo.split("(");
                                            myvalues.push(month[1].replace(/[()]+/g, '').trim())
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("trimester a") != -1) {
                                            console.log("trimester a");
                                            var month = intakeinfo.split("(");
                                            myvalues.push(month[1].replace(/[()]+/g, '').trim())
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("trimester b") != -1) {
                                            console.log("trimester b");
                                            var month = intakeinfo.split("(");
                                            myvalues.push(month[1].replace(/[()]+/g, '').trim())
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("trimester c") != -1) {
                                            console.log("trimester c");
                                            var month = intakeinfo.split("(");
                                            myvalues.push(month[1].replace(/[()]+/g, '').trim())
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("march and august") != -1) {
                                            console.log("march and august");
                                            myvalues.push("March");
                                            myvalues.push("August");
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("not offered") != -1) {
                                            console.log("not offered");
                                            myvalues.push("Not Mention")
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("spring") != -1) {
                                            console.log("spring");
                                            var month = intakeinfo.split("(");

                                            var monthintake = "";
                                            for (var b = 0; b < month.length; b++) {
                                                if (month[b].indexOf('spring') > -1) {
                                                    monthintake = month[b];
                                                }
                                                console.log("month of intake :" + month[b]);
                                            }

                                            myvalues.push(monthintake.replace(/[()]+/g, '').trim())
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("semester one,") != -1) {
                                            // var month = courseIntakeStr.split("(");
                                            console.log("semester one, of intake");
                                            myvalues.push("February");
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("semester two,") != -1) {
                                            console.log("semester two, intake ");
                                            // var month = courseIntakeStr.split("(");
                                            myvalues.push("July");
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("semester one") != -1) {
                                            console.log("semester one intake ");
                                            var month = intakeinfo.split("(");

                                            var monthintake = "";
                                            for (var b = 0; b < month.length; b++) {
                                                if (month[b].toLowerCase().indexOf('semester one') > -1) {
                                                    monthintake = "February";
                                                }
                                                console.log("month of intake :" + month[b]);
                                            }
                                            console.log("name of intake :" + monthintake)
                                            console.log("Month of semester intake one :" + monthintake);

                                            myvalues.push(monthintake.replace(/[()]+/g, '').trim())
                                        }
                                        if (intakeinfo.toLowerCase().trim().indexOf("semester two") != -1) {
                                            console.log("semester two intake ");
                                            var month = intakeinfo.split("(");
                                            var monthintake = "";
                                            for (var b = 0; b < month.length; b++) {
                                                if (month[b].toLowerCase().indexOf('semester two') > -1) {
                                                    monthintake = "July";
                                                }
                                                console.log("month of intake :" + month[b]);
                                            }
                                            console.log("Month of semester intake two :" + monthintake);
                                            myvalues.push(monthintake.replace(/[()]+/g, '').trim())
                                        }
                                    }
                                   
                                    myintakedata.value = myvalues;
                                    intakes.push(myintakedata);
                                }
                                var intakedata = {};
                                let formatedIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                intakedata.intake = formatedIntake;
                                var intakeUrl = [];
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedata.more_details = intakeUrl;
                                resJsonData.course_intake = intakedata
                            }
                            else{
                                var locations = resJsonData.course_campus_location;
                                var intakes = [];
                                for (let location of locations) {
                                    var myintakedata = {};
                                    myintakedata.name = location.name;
                                    var myvalues = [];
                                     
                                       
                                   myvalues.push({
                                    "actualdate": "",
                                    "month": ""
                                  });
                                    myintakedata.value = myvalues;
                                    intakes.push(myintakedata);
                                }
                                var intakedata = {};
                              
                                intakedata.intake = intakes;
                                var intakeUrl = [];
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedata.more_details = intakeUrl;
                                resJsonData.course_intake = intakedata
                            }
                            break;
                        }

                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = "";
                            var getProgramCode = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                selList.forEach(element => {
                                                    let splitProgramcode = element.split(" - ");
                                                    getProgramCode.push(splitProgramcode[splitProgramcode.length - 1]);
                                                });
                                                program_code = getProgramCode;
                                                console.log("program code is:" + program_code);
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            resJsonData.program_code = String(program_code);
                            break;
                        }
                       
                        case 'univ_name':{
                                const courseKeyVal = courseScrappedData.univ_name;
                                let resUnivName = null;
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    resUnivName = selList[0];
                                                }
                                            }
                                        }
                                    }
                                }
                                if (resUnivName) {
                                    resJsonData.univ_name = resUnivName;
                                }
                                break;
                            }
                        case 'univ_logo':{
                                const courseKeyVal = courseScrappedData.univ_logo;
                                let resUnivLogo = null;
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    resUnivLogo = selList[0];
                                                }
                                            }
                                        }
                                    }
                                }
                                if (resUnivLogo) {
                                    resJsonData.univ_logo = resUnivLogo;
                                }
                                break;
                            }
                        case 'course_accomodation_cost':{
                                const courseKeyVal = courseScrappedData.course_accomodation_cost;
                                let resAccomodationCostJson = null;
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    resAccomodationCostJson = selList[0];
                                                }
                                            }
                                        }
                                    }
                                }
                                if (resAccomodationCostJson) {
                                    resJsonData.course_accomodation_cost = resAccomodationCostJson;
                                }
                                break;
                            }

                        

                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let outcm = [];
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                            course_career_outcome = selList[0];
                                              }
                                        }
                                    }
                                }
                            }
                           
                            if (course_career_outcome != null ){
                                resJsonData.course_career_outcome = await utils.giveMeArray(course_career_outcome.replace(/[\r\n\t]+/g, ';'), ";");
                            }
                            else {
                                resJsonData.course_career_outcome = [];
                           }

                            break;
                        }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            if (course_country == null) {
                                resJsonData.course_overview = "";
                            } else {
                                resJsonData.course_overview = course_country;
                            }

                            break;
                        }

                        case 'course_title': {
                                console.log(funcName + 'matched case: ' + key);
                                const rootElementDictList = courseScrappedData[key];
                                console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                                let concatnatedRootElementsStr = null;
                                for (const rootEleDict of rootElementDictList) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    let concatnatedElementsStr = null;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        let concatnatedSelectorsStr = null;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            console.log(funcName + '\n\r selList = ' + selList);
                                            let concatnatedSelStr = null;
                                            for (const selItem of selList) {
                                                if (!concatnatedSelStr) {
                                                    concatnatedSelStr = selItem;
                                                } else {
                                                    concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                                }
                                            } // selList
                                            console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                            if (concatnatedSelStr) {
                                                if (!concatnatedSelectorsStr) {
                                                    concatnatedSelectorsStr = concatnatedSelStr;
                                                } else {
                                                    concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                                }
                                            }
                                        } // selectorsList
                                        console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                        // concat elements
                                        if (concatnatedSelectorsStr) {
                                            if (!concatnatedElementsStr) {
                                                concatnatedElementsStr = concatnatedSelectorsStr;
                                            } else {
                                                concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                            }
                                        }
                                    } // elementsList
                                    console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                    if (concatnatedElementsStr) {
                                        if (!concatnatedRootElementsStr) {
                                            concatnatedRootElementsStr = concatnatedElementsStr;
                                        } else {
                                            concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                        }
                                    }
                                } // rootElementDictList
                                console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
                                // add only if it has valid value
                                const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
                                if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                    if (cTitle != null) {
                                        resJsonData[key] = String(this.titleCase(concatnatedRootElementsStr)).trim() + " (" + this.titleCase(cTitle) + ")";
                                    } else {
                                        resJsonData[key] = String(this.titleCase(concatnatedRootElementsStr)).trim();
                                    }
                                }
                                break;
                            }
                            case 'application_fee': {
                                const courseKeyVal = courseScrappedData.application_fee;
                       
                                let applicationfee = null;
                                  if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                               console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                               const elementsList = rootEleDict.elements;
                               for (const eleDict of elementsList) {
                               console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                  const selectorsList = eleDict.selectors;
                               for (const selList of selectorsList) {
                                   console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                if (Array.isArray(selList) && selList.length > 0) {
                                  applicationfee = selList[0];
                                   }
                                   else{
                                      applicationfee = selList[0];
                                   }
                                }
                               }
                               }
                           }
                          if (applicationfee && applicationfee.length>0) {
                            resJsonData.application_fee = applicationfee;
                           console.log("appfee"+ JSON.stringify(resJsonData.application_fee));
                           }
                           else{
                              resJsonData.application_fee = applicationfee;
                           }
                       
                            break;
                      
                          }
                        case 'course_study_level': {
                                let cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
                                
                                let cStudyLevel = null;
                                console.log(funcName + 'course_title course_study_level = ' + cTitle);
                                if (cTitle) {
                                    resJsonData.course_study_level = cTitle;
                                }else{
                                    throw new Error('Study LEvel Not Found');
                                }
                                    
                                
                                break;
                            }
                        default:
                            {
                                console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                                break;
                            } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            // console.log('\n\r' + funcName + 'course resJsonData =' + JSON.stringify(resJsonData));
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase()));
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                console.log(" NEWJSONSTRUCT.course_id==>", NEWJSONSTRUCT.course_id)
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase() ;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
               
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
               
                var filelocation = "./output/" + resJsonData.univ_id + "_" +location_wise_data.course_location_id.replace(/[\n :""]+/g, '').replace(/[\/]+/g, '_') + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData,courseDict.href,courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails

    static titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    }
} // class
module.exports = { ScrapeCourse };