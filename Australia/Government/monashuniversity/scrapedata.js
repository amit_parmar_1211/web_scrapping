const puppeteer = require('puppeteer');
const fs = require("fs");
async function start() {
    const URL = "https://www.monash.edu/study/courses";
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug

    let page = await browser.newPage();
    await page.setViewport({ width: 1366, height: 900 });
    await page.goto(URL, { timeout: 0 });

    const categoryselectorUrl = "#main > div.content-wrapper.bg-white.l--content-rhs > div > div.content-inner__main > div.study-info-extra-courses > div > div > div > div > a";
    const categoryselectorText = "#main > div.content-wrapper.bg-white.l--content-rhs > div > div.content-inner__main > div.study-info-extra-courses > div > div > div > div > a";
    var elementstring = "", elementhref = "", allcategory = [];

    const targetLinksCardsUrls = await page.$(categoryselectorUrl);
    const targetLinksCardsText = await page.$$(categoryselectorText);

    var targetLinksCardsTotal = [];
    for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        elementstring = await page.evaluate(el => el.innerText, targetLinksCardsText[i]);
        elementhref = await page.evaluate(el => el.href, targetLinksCardsUrls[i]);
        targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
    }
    console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));
    let linkselector = "#accordion__target-50 > div > div > div > div > div > h2 > a";
    let textselector = "#accordion__target-50 > div > div > div > div > div > h2 > a"
    var totalCourseList = [];
    for (let target of targetLinksCardsTotal) {
        await page.goto(target.href + "&international=true" + "&f.Study+mode%7CcourseMode=On-campus", { timeout: 0 });
        //#main > div.bg-light-grey.l--lhs-content > div > div.content.bg-white > div.content-inner__main > div.bottom-tabs.search-tabs > div.search-tabs__wrap.mobile-hidden > ul > li > a:not([alt*='Professional development'])
        let educationLevelCat = "#main > div.bg-light-grey.l--lhs-content > div > div.content.bg-white > div.content-inner__main > div.bottom-tabs.search-tabs > div.search-tabs__wrap.mobile-hidden > ul > li > a:not([alt*='Professional development'])";
        let scrapedCategory = await page.$$(educationLevelCat);
        var educationLevelCatList = [];
        let elementstring1 = "";
        let elementhref1 = "";
        for (let i = 0; i < scrapedCategory.length; i++) {
            elementstring1 = await page.evaluate(el => el.innerText, scrapedCategory[i]);
            elementhref1 = await page.evaluate(el => el.href, scrapedCategory[i]);
            educationLevelCatList.push({ href: elementhref1, innerText: elementstring1 });
        }
        console.log("#### main courses---" + JSON.stringify(educationLevelCatList));

        for (let educationLevel of educationLevelCatList) {
            await page.goto(educationLevel.href, { timeout: 0 });
            const targetLinks = await page.$$(linkselector);
            const targetText = await page.$$(textselector);

            console.log("target.innerText -->", target.innerText);
            console.log("#total link selectors---->" + targetLinks.length);
            for (var j = 0; j < targetLinks.length; j++) {
                var elementstring = await page.evaluate(el => el.innerText, targetText[j]);
                const elementlink = await page.evaluate(el => el.title, targetLinks[j]);
                let payload = { href: elementlink, innerText: elementstring, category: target.innerText }
                let res = totalCourseList.some(value => {
                    if (value.href.trim() == payload.href.trim() && value.category.trim() == payload.category.trim()) {
                        return true;
                    } else {
                        return false
                    }
                });
                if (res == false) {
                    totalCourseList.push(payload);
                } else {
                    // console.log("Record Exists -- >", payload);
                }
                // https://mon3-search.clients.squiz.net/s/redirect?collection=study-monash-courses-meta&url=https%3A%2F%2Fwww.monash.edu%2Fstudy%2Fcourses%2Ffind-a-course%2F2019%2Feducation-and-science-d3005%3Fdomestic%3Dtrue&index_url=https%3A%2F%2Fwww.monash.edu%2Fstudy%2Fcourses%2Ffind-a-course%2F2019%2Feducation-and-science-d3005&auth=R9ABk%2BvQiXi4k1z%2Bnt1LIw&profile=_default&rank=6&query=%21padrenull+%7CcourseStudentType%3A%22%24%2B%2B+domestic+%24%2B%2B%22+%7CcourseInterestAreas%3A%22%24%2B%2B+Education+%24%2B%2B%22+%7CcourseTab%3A%22%24%2B%2B+Undergraduate+%24%2B%2B%22
                // totalCourseList.push({ innerText: elementstring, href: elementlink, category: target.innerText });

            }
        }



        // console.log("scrapedCategory -->", scrapedCategory);
        // for (let categoryL of scrapedCategory) {

        //     //let catelementstring = await page.evaluate(el => el.href, categoryL);
        //     //console.log("catelementstring -->", catelementstring);
        //     //await page.goto(catelementstring, { timeout: 0 });
        //     //  console.log("Category -->", catelementstring);
        //     console.log("before click");

        //     await categoryL.click();
        //     console.log("before click");
        //     await page.waitFor(5000);
        //     console.log("After catelementstring click");
        //     // let check = "#header-utility-links > ul > li.utility-navigation-list__item.student-type > div.student-type__dropdown.student-type__dropdown--active > div.student-type__dropdown__list.student-type__dropdown__list--large > ul > li:nth-child(2) > a";
        //     // await page.waitFor(2000);
        //     // let clickBtn = await page.$$(check);
        //     // console.log("Inside categoryL");
        //     // if (clickBtn.length > 1) {
        //     //     // await page.evaluate((check) => document.querySelector(check).click(), check);
        //     //     await page.waitFor(2000);
        //     //     categoryL.click();
        //     //     console.log("After click categoryL");
        //     // } else {
        //     //     console.log("Button Not found");
        //     // }



        //     //scrape university courselist




        //     // elementhref = await page.evaluate(el => el.href, subCategoryLinks[i]);

        //     // const targetLinks = await page.$$(linkselector);
        //     // const targetText = await page.$$(textselector);

        //     // console.log("target.innerText -->", target.innerText);
        //     // console.log("#total link selectors---->" + targetLinks.length);
        //     // for (var j = 0; j < targetLinks.length; j++) {
        //     //     var elementstring = await page.evaluate(el => el.innerText, targetText[j]);
        //     //     const elementlink = await page.evaluate(el => el.href, targetLinks[j]);
        //     //     let payload = { href: elementlink, innerText: elementstring, category: target.innerText }
        //     //     let res = totalCourseList.some(value => {
        //     //         if (value.href == payload.href && value.category == payload.category) {
        //     //             return true;
        //     //         } else {
        //     //             return false
        //     //         }
        //     //     });
        //     //     if (res == false) {
        //     //         totalCourseList.push(payload);
        //     //     } else {
        //     //         // console.log("Record Exists -- >", payload);
        //     //     }
        //     //     // https://mon3-search.clients.squiz.net/s/redirect?collection=study-monash-courses-meta&url=https%3A%2F%2Fwww.monash.edu%2Fstudy%2Fcourses%2Ffind-a-course%2F2019%2Feducation-and-science-d3005%3Fdomestic%3Dtrue&index_url=https%3A%2F%2Fwww.monash.edu%2Fstudy%2Fcourses%2Ffind-a-course%2F2019%2Feducation-and-science-d3005&auth=R9ABk%2BvQiXi4k1z%2Bnt1LIw&profile=_default&rank=6&query=%21padrenull+%7CcourseStudentType%3A%22%24%2B%2B+domestic+%24%2B%2B%22+%7CcourseInterestAreas%3A%22%24%2B%2B+Education+%24%2B%2B%22+%7CcourseTab%3A%22%24%2B%2B+Undergraduate+%24%2B%2B%22
        //     //     // totalCourseList.push({ innerText: elementstring, href: elementlink, category: target.innerText });

        //     // }

        // }


    }
    console.log("totalCourseList -->", totalCourseList);
    fs.writeFileSync("monashuniversity_category_courselist_bk.json", JSON.stringify(totalCourseList));
    await browser.close();

}
start();
