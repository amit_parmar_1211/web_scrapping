const fs = require('fs');
const configs = require('./configs');
const appConfigs = require('./common/app-config');

const utils = require('./common/utils');
const ScrapeCourseList = require('./ScrapeCourseList').ScrapeCourseList;
const ScrapeCourse = require('./ScrapeCourse').ScrapeCourse;
const ScrapeUniversity = require('./ScrapeUniversity').ScrapeUniversity;
request = require('request');

const START_COURSE_COUNT = 0;
let MAX_COURSE_COUNT = -1; // to scrape for all courses, set to -1
const SCRAPE_COURSE_LIST = false;
const SCRAPE_UNIVERSITY_DETAILS = true;
const SCRAPE_COURSE_DETAILS = false;

async function validateMandatoryConfigs() {
  const funcName = 'validateMandatoryConfigsForUniversity ';
  try {
    // 'univ_name' value is mandatory and this id will be used to put item into DDB
    if (!(configs.univ_name && configs.univ_name.length > 0)) {
      console.log(funcName + 'Invalid value set to \'univ_name\'');
      throw (new Error('Invalid value set to \'univ_name\'. To set this value, open \'config.js\' file of the specific university folder and edit its value. \
      \n\n ########## Warning ########## \n Make sure you set correct value to \'univ_name\'. Because this value will be used to create unique \'univ_id\' value for the university into databse. \
      \n Setting incorrect value may create unexpected result. \n'));
    }
    // 'univ_id' value is mandatory and this id will be used to put items into DDB
    if (!(configs.univ_id && configs.univ_id.length > 0)) {
      console.log(funcName + 'Invalid value set to \'univ_id\'');
      throw (new Error('Invalid value set to \'univ_id\'. To set this value, open \'config.js\' file of the specific university folder and edit its value. \
      \n\n ########## Warning ########## \n Make sure you set correct value to \'univ_id\'. Because this value will be used to make put operations into database. \
      \n Setting incorrect value may overwrite data into database. \n'));
    }
    // validate 'univ_id' rules
    const regEx = /[A-Z\s]/;
    if (String(configs.univ_id).search(regEx) !== -1) { // -1 = not found
      console.log(funcName + '\n \'univ_id\' must NOT contain any white space and must be in lowercase only \n');
      throw (new Error('\'univ_id\' must NOT contain any white space and must be in lowercase only'));
    }
    console.log(funcName + '****************  scrapping for \'univ_id\' = ' + configs.univ_id + ', and \'univ_name\' = ' + configs.univ_name + '  ****************');
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

// eslint-disable-next-line no-unused-vars
async function scrapeUnivDetailsAndPutAtS3() {
  const funcName = 'scrapeUnivDetailsAndPutAtS3 ';
  try {
    await validateMandatoryConfigs();
    const univOverviewJson = [];
    // get existing scrapped files from output folder and do not scrape from scratch
    const su = new ScrapeUniversity();
    if (appConfigs.shouldTakeFromOutputFolder) {
      console.log(funcName + 'reading scrapped data from output file...');
      try {
        if (appConfigs.ISINSERT) {
          request.post({
            "headers": { "content-type": "application/json" },
            "url": appConfigs.API_URL + "api/australia/university/v1/save",
            "body": JSON.stringify(univOverviewJson)
          }, (error, response, body) => {
            if (error) {
              console.log(error);
            }
            console.log("success save data! " + JSON.stringify(body));
          });
        }
      }
      catch (err) {
        console.log("#### try-catch error--->" + err);
      }
      return;
    }
    const univScrappedData = await su.scrapeAndFormatUniversityDetails(appConfigs.selUniversityOverviewSelectorsJsonFilepath);
    univOverviewJson.push(univScrappedData);
    // write to output file
    await fs.writeFileSync(configs.opOverviewFilepath, JSON.stringify(univOverviewJson));
    // put file in S3 bucket
    try {
      if (appConfigs.ISINSERT) {
        request.post({
          "headers": { "content-type": "application/json" },
          "url": appConfigs.API_URL + "api/australia/university/v1/save",
          "body": JSON.stringify(univOverviewJson)
        }, (error, response, body) => {
          if (error) {
            console.log(error);
          }
          console.log("success save data! " + JSON.stringify(body));
        });
      }
    }
    catch (err) {
      console.log("#### try-catch error--->" + err);
    }
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}
// Start scrapping
const startScrapping = async function startScrappingFunc() {
  const funcName = 'startScrappingFunc ';
  // let sc = null;
  try {
    // scrape all courses with its name and link
    let scrappedCourseList = null;
    if (SCRAPE_COURSE_LIST) {
      console.log(funcName + 'Starting to scrape course list.....');
      const scl = new ScrapeCourseList();
      scrappedCourseList = await scl.scrapeOnlyInternationalCourseList_Mapping();
      // console.log(funcName + 'scrappedCourseList = ' + JSON.stringify(scrappedCourseList));
    }

    // scrape Univ Details
    if (SCRAPE_UNIVERSITY_DETAILS) {
      console.log(funcName + 'Starting to scrape univ details.....');
      await scrapeUnivDetailsAndPutAtS3();
    }

    // scrape course details for each course
    if (SCRAPE_COURSE_DETAILS) {
      console.log(funcName + 'Starting to scrape course details.....');
      console.log(funcName + 'Starting to scrape course details.....');
      if (appConfigs.SHOULD_TAKE_FROM_FOLDER) {
        await putCourseInSQL();
      }
      else {
        if (!scrappedCourseList) { // read it from local disk
          const fileData = fs.readFileSync(configs.opCourseListFilepath);
          scrappedCourseList = JSON.parse(fileData);
        }
        // failed course items list
        const failedCourseDictList = [];
        // for (const courseDict of scrappedCourseList) {
        if (MAX_COURSE_COUNT < 0) {
          MAX_COURSE_COUNT = scrappedCourseList.length;
        }
        console.log(funcName + 'START_COURSE_COUNT = ' + START_COURSE_COUNT + ', MAX_COURSE_COUNT = ' + MAX_COURSE_COUNT);
        for (let i = START_COURSE_COUNT; i < MAX_COURSE_COUNT; i += 1) {
          try {
            const courseDict = scrappedCourseList[i];
            console.log('\n\r----------------------------------------------------------------------');
            console.log('----------------------------------------------------------------------\n\r');
            console.log('   Start scraping for Course = ' + courseDict.innerText);
            console.log('   courseCount = ' + i);
            console.log('\n\r----------------------------------------------------------------------');
            console.log('----------------------------------------------------------------------\n\r');
            const finalScrappedDataList = [];
            const finalScrappedData = await ScrapeCourse.scrapeAndFormatCourseDetails(courseDict);
          
            finalScrappedDataList.push(finalScrappedData);
          } catch (error) {
            const failedCourseDict = scrappedCourseList[i];
            console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
            console.log(funcName + 'try-catch error = ' + error);
            console.log('Failed course href = ' + failedCourseDict.href);
            console.log('Failed course innerText = ' + failedCourseDict.innerText);
            failedCourseDict.reason = error.toString();
            failedCourseDict.index = i;
            failedCourseDictList.push(failedCourseDict);
            // write to local disk
            fs.writeFileSync(configs.opFailedCourseList, JSON.stringify(failedCourseDictList));
            console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
          }
        } // for
        console.log('Exiting from program...');
      } // if (SCRAPE_COURSE_DETAILS)
    }
    return true;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
};
startScrapping();


async function readFile() {
  return await fs.readdirSync(appConfigs.READ_FOLDER_PATH)
}
async function putCourseInSQL() {
  try {
    var myfiles = await readFile();

    console.log(JSON.stringify(myfiles))
    for (let filename of myfiles) {
      var resJsonData = JSON.parse(fs.readFileSync(appConfigs.READ_FOLDER_PATH + "/" + filename));

      if (appConfigs.ISINSERT) {
        console.log("start insert for" + filename)
        request.post({
          "headers": { "content-type": "application/json" },
          "url": appConfigs.API_URL + "api/australia/course/v2/savecourse",
          "body": JSON.stringify(resJsonData)
        }, (error, response, body) => {
          if (error) {
            console.log(error);
          }
          var myresponse = JSON.parse(body);
          if (!myresponse.flag) {
            var errorlog = [];
            if (fs.existsSync(configs.opFailedCourseListByAPI)) {
              errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
              errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
            }
            fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
          }
          console.log("success save data! " + JSON.stringify(body));
        });
      }
      else if (appConfigs.ISREPLACE) {
        request.post({
          "headers": { "content-type": "application/json" },
          "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
          "body": JSON.stringify(resJsonData)
        }, (error, response, body) => {
          if (error) {
            console.log(error);
          }
          var myresponse = JSON.parse(body);
          console.log("MyMessage-->" + JSON.stringify(myresponse))
          if (!myresponse.flag) {
            var errorlog = [];
            if (fs.existsSync(configs.opFailedCourseListByAPI)) {
              errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
              errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
            }
            fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
          }
          console.log("success save data! " + JSON.stringify(body));
        });
      }
      else if (appConfigs.ISUPDATE) {
        request.post({
          "headers": { "content-type": "application/json" },
          "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
          "body": JSON.stringify(resJsonData)
        }, (error, response, body) => {
          if (error) {
            console.log(error);
          }
          var myresponse = JSON.parse(body);
          if (!myresponse.flag) {
            var errorlog = [];
            if (fs.existsSync(configs.opFailedCourseListByAPI)) {
              errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
              errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
            }
            fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
          }
          console.log("success save data! " + JSON.stringify(body));
        });
      }
    }
  }
  catch (err) {
    console.log("#### try-catch error--->" + err);
  }
  return await true;
}

async function scrapeCourseDetailsAndPutAtS3() {
  const funcName = 'scrapeCourseDetailsAndPutAtS3 ';
  try {
    const sc = new ScrapeCourse();
    await validateMandatoryConfigs();
    // read course list file
    const fileData = await fs.readFileSync(configs.opCourseListFilepath);
    const courseListJson = JSON.parse(fileData);
    // console.log(funcName + 'courseListJson = ' + JSON.stringify(courseListJson));

    if (Array.isArray(courseListJson)) {
      // for each page url - get course list of that page.
      let courseCount = 1;
      for (const cours of courseListJson) {
        try {
          console.log('\n\r----------------------------------------------------------------------');
          console.log('----------------------------------------------------------------------\n\r');
          console.log('   Start scraping for Course = ' + cours.innerText);
          console.log('\n\r----------------------------------------------------------------------');
          console.log('----------------------------------------------------------------------\n\r');

          const scrappedCourseList = [];
          console.log(funcName + 'cours = ' + JSON.stringify(cours));
          const courseURL = cours.href;
          console.log(funcName + 'courseURL = ' + courseURL);
          let courseScrappedData = null;
          let opCourseFilePath = null;

          const courseId = await removeAllWhiteSpcaesAndLowerCase(cours.innerText);
          console.log(funcName + 'courseId = ' + courseId);
          if (!courseId) {
            console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
            throw (new Error('Invalid courseId, courseId = ' + courseId));
          }
          if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
            opCourseFilePath = configs.outputDirPath + courseId + '_update_coursedetails.json';
            // opCourseFilePath = configs.opUpdateCourseDetailsFilepath;
          } else {
            opCourseFilePath = configs.outputDirPath + courseId + '_coursedetails.json';
            // opCourseFilePath = configs.opCourseDetailsFilepath;
          }
          console.log(funcName + 'opCourseFilePath = ' + opCourseFilePath);

          if (appConfigs.shouldTakeFromOutputFolder) {
            console.log(funcName + 'reading scrapped data from output file...');
           
            return;
          }

          if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
            console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            courseScrappedData = await sc.scrapeCourseDetails(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath, courseURL);
          } else {
            console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
            courseScrappedData = await sc.scrapeCourseDetails(appConfigs.selCourseDetailsSelectorsJsonFilepath, courseURL);
          }
          if (!courseScrappedData) {
            console.log(funcName + 'Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData);
            throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
          }
          const formattedCourseData = await sc.formatOutput(courseScrappedData);
          console.log('formattedCourseData = ' + JSON.stringify(formattedCourseData));
          const finalCourseData = await ScrapeCourse.removeKeys(formattedCourseData);
          // const polishedCourseScrappedData = await Course.placeNAForEachEmptyValue(finalCourseData);
          console.log(funcName + 'finalCourseData = ' + JSON.stringify(finalCourseData));
          if (finalCourseData && finalCourseData.course_cricos_code
            && finalCourseData.course_cricos_code.length > 0
            && (String(finalCourseData.course_cricos_code).includes('N/A') === false)) {
            scrappedCourseList.push(finalCourseData);
            // write file to local disk
            await fs.writeFileSync(opCourseFilePath, JSON.stringify(scrappedCourseList));
           
            courseCount += 1;
          } else {
            console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
            console.log('Invalid course_cricos_code value so not pushing course_title = ' + finalCourseData.course_title);
            console.log('course_cricos_code  = ' + finalCourseData.course_cricos_code);
            console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
          }
          // //// Temp ///////
          if (courseCount >= 1) {
            break;
          }
        } catch (error) { // catch error for the specific course item but contnue scrape loop
          console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
          console.log(funcName + 'try-catch error = ' + error);
          console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
        }
      } // for (const cours of courseListJson)
    }
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

