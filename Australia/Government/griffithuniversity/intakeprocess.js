function providemyintake(oldintake, format, separator) {
    var newintake = [];

    const fullmonths = [{ fullkey: "January", shortkey: "Jan", intake: "Summer" }, { fullkey: "February", shortkey: "Feb", intake: "Summer" },
    { fullkey: "March", shortkey: "Mar", intake: "Autumn" }, { fullkey: "April", shortkey: "Apr", intake: "Autumn" }, { fullkey: "May", shortkey: "May", intake: "Autumn" }, { fullkey: "June", shortkey: "June", intake: "Winter" },
    { fullkey: "July", shortkey: "July", intake: "Winter" }, { fullkey: "August", shortkey: "Aug", intake: "Winter" }, { fullkey: "September", shortkey: "Sept", intake: "Spring" },
    { fullkey: "October", shortkey: "Oct", intake: "Spring" }, { fullkey: "November", shortkey: "Nov", intake: "Spring" }, { fullkey: "December", shortkey: "Dec", intake: "Summer" }];

    oldintake.forEach(element => {
        var newdataafterreplace = {}, replacedata = [];
        element.value.forEach(elementdate => {
            var date = {};
            switch (format) {
                case "MOM": {
                    fullmonths.forEach(elementmon => {
                        if (elementdate.toLowerCase().indexOf(elementmon.fullkey.toLowerCase()) != -1) {
                            date.actualdate = elementdate;
                            date.month = elementmon.fullkey;
                            date.intake = elementmon.intake;
                        }
                    });
                    break;
                }
                case "mom": {
                    fullmonths.forEach(elementmon => {
                        if (elementdate.toLowerCase().indexOf(elementmon.shortkey.toLowerCase()) != -1) {
                            date.actualdate = elementdate;
                            date.month = elementmon.fullkey;
                            date.intake = elementmon.intake;
                        }
                    });
                    break;
                }
                case "ddmmyyyy": {
                    var elementmon = fullmonths[elementdate.split(separator)[1] - 1];
                    date.actualdate = elementdate;
                    date.month = elementmon.fullkey;
                    date.intake = elementmon.intake;
                    break;
                }
                case "mmddyyyy": {
                    var elementmon = fullmonths[elementdate.split(separator)[0] - 1];
                    date.actualdate = elementdate;
                    date.month = elementmon.fullkey;
                    date.intake = elementmon.intake;
                    break;
                }
            }
            replacedata.push(date);
        });
        newdataafterreplace.name = element.name;
        newdataafterreplace.value = replacedata;
        newintake.push(newdataafterreplace);
    });
    return newintake;
}

var intake = [
    {
        "name": "Sydney",
        "value": [
            "18 February",
            "28 October",
            "15 June"
        ]
    },
    {
        "name": "Melbourne",
        "value": [
            "18 February",
            "28 October",
            "15 June"
        ]
    }
];
console.log(JSON.stringify(providemyintake(intake, "MOM", "")));

var intakeshort = [
    {
        "name": "Sydney",
        "value": [
            "18-02-2019",
            "28-08-2019",
            "15-06-2019"
        ]
    },
    {
        "name": "Melbourne",
        "value": [
            "18-02-2019",
            "28-08-2019",
            "15-06-2019"
        ]
    }
];
console.log(JSON.stringify(providemyintake(intakeshort, "ddmmyyyy", "-")));