
const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require("./common/utils");
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json

    static titleCase(str) {
        // You do not need to check if i is larger than splitStr length, as your for does that for you
        // Assign it back to the array
        // Directly return the joined string
        return str.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }
    static async formatOutput(courseScrappedData, courseUrl, course_category, studyLevel) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = course_category;
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            var penglishList = [];
                            var potherLngDict = null, ieltsdic = null;
                            // english requirement
                            let scrappedIELTS = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                            let scrappedPBT = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_pbt);
                            let scrappedIBT = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_ibt);
                            let scrappedCAE = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_cae);
                            let scrappedPTE = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_pte);
                            console.log("scrappedIELTS ->", scrappedIELTS)
                            console.log("scrappedPBT ->", scrappedPBT)
                            console.log("scrappedIBT ->", scrappedIBT)
                            console.log("scrappedCAE ->", scrappedCAE)
                            console.log("scrappedPTE ->", scrappedPTE)
                            if (scrappedIELTS != null && scrappedIELTS.length > 0) {
                                let ieltsDict = {};
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = scrappedIELTS.replace(/OR/, '').trim();
                                englishList.push(ieltsDict);

                                var ieltsScore = await utils.giveMeNumber(scrappedIELTS);
                                if (ieltsScore != []) {
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsScore;
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = ieltsDict;
                                    // englishList.push(ieltsDict);
                                }

                            }

                            // else {
                            //     let ieltsDict = {};
                            //     console.log("gdgdfgdgdfgdfg", scrappedIELTS)
                            //     ieltsDict.min = 0;
                            //     ieltsDict.require = "";
                            //     ieltsDict.max = 9;
                            //     ieltsDict.R = 0;
                            //     ieltsDict.W = 0;
                            //     ieltsDict.S = 0;
                            //     ieltsDict.L = 0;
                            //     ieltsDict.O = 0;
                            // }

                            if (scrappedPBT != null && scrappedPBT.length > 0) {
                                let ieltsDict = {};
                                ieltsDict.name = 'toefl pbt';
                                ieltsDict.description = scrappedPBT.replace(/OR/, '').trim();
                                englishList.push(ieltsDict);

                                var pbtScore = await utils.giveMeNumber(scrappedPBT);
                                if (pbtScore != []) {
                                    ieltsDict.min = 310;
                                    ieltsDict.require = pbtScore;
                                    ieltsDict.max = 677;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = value;
                                    // englishList.push(ieltsDict);
                                }
                            }
                            if (scrappedIBT != null && scrappedIBT.length > 0) {
                                let ieltsDict = {};
                                ieltsDict.name = 'toefl ibt';
                                ieltsDict.description = scrappedIBT.replace(/OR/, '').trim();
                                englishList.push(ieltsDict);

                                var ibtScore = await utils.giveMeNumber(scrappedIBT);
                                if (ibtScore != []) {
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ibtScore;
                                    ieltsDict.max = 120;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = value;
                                    // englishList.push(ieltsDict);
                                }
                            }
                            if (scrappedCAE != null && scrappedCAE.length > 0) {
                                let ieltsDict = {};
                                ieltsDict.name = 'cae';
                                ieltsDict.description = scrappedCAE.replace(/OR/, '').trim();
                                englishList.push(ieltsDict);

                                var caeScore = await utils.giveMeNumber(scrappedCAE);
                                if (caeScore != []) {
                                    ieltsDict.min = 80;
                                    ieltsDict.require = caeScore;
                                    ieltsDict.max = 230;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = value;
                                    // englishList.push(ieltsDict);
                                }
                            }
                            if (scrappedPTE != null && scrappedPTE.length > 0) {
                                let ieltsDict = {};
                                ieltsDict.name = 'pte academic';
                                ieltsDict.description = scrappedPTE.replace(/OR/, '').trim();
                                englishList.push(ieltsDict);

                                var pteScore = await utils.giveMeNumber(scrappedPTE);
                                if (pteScore != []) {
                                    ieltsDict.min = 0;
                                    ieltsDict.require = pteScore;
                                    ieltsDict.max = 90;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = value;
                                    // englishList.push(ieltsDict);
                                }
                            }

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }
                            else {
                                //   throw new Error('IELTS not found');

                                courseAdminReq.english = [];
                            }

                            //                      courseAdminReq.academic = "Academic requirements are provided by country please visit the university to know more.";
                            courseAdminReq.academic// = courseUrl;
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                                var arr = [];
                                const academicReq = courseUrl//await Course.extractValueFromScrappedElement(courseScrappedData.courseUrl);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                arr.push(academicReq)
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq];
                                }
                            }

                            const courseKeyVal = courseScrappedData.course_entry_more_details;
                            console.log("EntryRequirement:::" + courseKeyVal);
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))

                            // add english requirement 'english_more_details' link
                            const courseKeyValAcademic = courseScrappedData.course_admission_academic_more_details;
                            console.log("AcademicRequirement:::" + courseKeyValAcademic);
                            let resAcademicReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyValAcademic)) {
                                for (const rootEleDict of courseKeyValAcademic) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAcademicReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            // add english requirement 'english_requirements_url'
                            const courseKeyValEnglishReq = courseScrappedData.course_english_more_details;
                            console.log("EnglishRequirement:::" + courseKeyValEnglishReq);
                            let resEnglishReqJson = null;
                            if (Array.isArray(courseKeyValEnglishReq)) {
                                for (const rootEleDict of courseKeyValEnglishReq) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = resJsonData.course_url + "#entry-requirements";
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqJson;
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = resJsonData.course_url + "#entry-requirements";
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqJson;
                            }

                            break;
                        }

                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };

                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

                            let resScholarshipJson = null;


                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }




                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }

                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)

                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;


                            break;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                resJsonData.application_fee = ""
                                break;
                            }
                        case 'course_duration_full_time': {
                            const courseDurationList = [];
                            const courseDurationDisplayList = [];
                            const durationFullTime = {};

                            let anotherArray = [];
                            let durationFullTimeDisplay = null;
                            let duration1 = courseScrappedData.course_duration_full_time;

                            console.log("*************start formating Full Time years*************************");
                            console.log("courseScrappedData.course_duration_full_time = ", JSON.stringify(courseScrappedData.course_duration_full_time));
                            let finalDuration = duration1[0].elements[0].selectors[0][0];

                            if (finalDuration.indexOf('/') > -1) {
                                finalDuration = finalDuration.replace(/\//, ' ');
                            }
                            console.log("Duration1 = ", finalDuration.trim());
                            const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration.trim());
                            console.log("full_year_formated_data", full_year_formated_data);
                            ///course duration
                            const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                            console.log("not_full_year_formated_data", not_full_year_formated_data);
                            try {
                                durationFullTime.duration_full_time = not_full_year_formated_data;
                                courseDurationList.push(durationFullTime);
                                ///END course duration
                                console.log("courseDurationList : ", courseDurationList);

                                courseDurationDisplayList.push(full_year_formated_data);
                                // anotherArray.push(courseDurationDisplayList);
                                console.log("courseDurationDisplayList", courseDurationDisplayList);
                                ///END course duration display
                            } catch (err) {
                                console.log("Problem in Full Time years:", err);
                            }
                            console.log("courseDurationDisplayList1", courseDurationDisplayList);
                            console.log('***************END formating Full Time years**************************')
                            let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                            console.log("filtered_duration_formated", filtered_duration_formated);
                            console.log("courseDurationDisplayList2", courseDurationDisplayList);
                            if (courseDurationList && courseDurationList.length > 0) {

                                resJsonData.course_duration = finalDuration;
                            }
                            if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                //  let anotherArray = [];
                                //  anotherArray.push(courseDurationDisplayList);
                                // anotherArray.push(courseDurationDisplayList);
                                resJsonData.course_duration_display = filtered_duration_formated;

                                var isfulltime = false, isparttime = false;
                                filtered_duration_formated.forEach(element => {
                                    if (element.display == "Full-Time") {
                                        isfulltime = true;
                                    }
                                    if (element.display == "Part-Time") {
                                        isparttime = true;
                                    }
                                });
                                resJsonData.isfulltime = isfulltime;
                                resJsonData.isparttime = isparttime;
                            }


                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            console.log("courseScrappedData.program_code = ", courseScrappedData.program_code);
                            var program_code = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            console.log("selList[i]", selList);
                                            for (let i = 0; i < selList.length; i++) {
                                                program_code = selList[i];
                                                console.log("selList[i] --> ", selList[i]);
                                            }
                                            // program_code = selList[0];
                                            // }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            resJsonData.program_code = program_code;
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }

                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {
                                international_student: {},


                            };

                            var feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);

                            if (!feesIntStudent) {
                                feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_other);
                            }


                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                if (feesIntStudent.indexOf('(') > -1) {
                                    feesIntStudent = feesIntStudent.split('(')[0];
                                }
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const arrval = String(feesWithDollorTrimmed).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    console.log('fee value for griff :' + feesVal);
                                    let feeValSplit = feesVal.split('*2020');
                                    console.log('feeevalue :' + feeValSplit[0]);

                                    const regEx = /\d/g;
                                    let feesValNum = feeValSplit[0].match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: feesIntStudent
                                            });

                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: 1,
                                                unit: "Year",
                                                description: ""
                                            });
                                            // throw new Error("Fees not Found");
                                        }

                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                // throw new Error("Fees not Found");
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",
                                    description: ""
                                });
                            }


                            let international_student_all_fees_array = [];
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    //   feesDict.international_student_all_fees = international_student_all_fees_array
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }
                        // case 'course_cricos_code': {
                        //     const courseKeyVal = courseScrappedData.course_cricos_code;
                        //     let course_cricos_code = null;
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                     if (Array.isArray(selList) && selList.length > 0) {
                        //                         course_cricos_code = selList;
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     if (course_cricos_code) {
                        //         global.cricos_code = course_cricos_code[0];
                        //         var locations = resJsonData.course_campus_location;
                        //         var mycodes = [];
                        //         for (let location of locations) {
                        //             mycodes.push({
                        //                 location: location.name, code: course_cricos_code.toString(), iscurrent: false
                        //             })
                        //         }
                        //         resJsonData.course_cricos_code = mycodes;
                        //     }
                        //     //resJsonData.course_cricos_code = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                        //     break;
                        // }
                        case 'course_campus_location': {
                            var campLocationText = [];
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            const Cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);

                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList campuses= ' + JSON.stringify(selList));
                                            let campuses = ['Nathan', 'Gold Coast', 'Mount Gravatt', 'Logan', 'South Bank', 'Mt Gravatt'];
                                            for (let sel of selList) {

                                                for (let i = 0; i < campuses.length; i++) {
                                                    if (sel.toLowerCase().indexOf(campuses[i].toLowerCase()) > -1) {
                                                        sel = this.titleCase(sel);
                                                        campLocationText.push({
                                                            "name": sel,
                                                            "code": Cricos

                                                        });
                                                    }
                                                }
                                            }

                                        }

                                    }
                                    if (campLocationText.length == 0) {
                                        throw new Error("Campus Location not found");
                                    }
                                    console.log("Avail-->" + campLocationText)
                                    resJsonData.course_campus_location = campLocationText;

                                    resJsonData.course_study_mode = "On campus";

                                    break;
                                }
                            }
                        }

                        case 'course_intake': {
                            const courseIntakeDisplay = {};
                            var courseIntakeStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            console.log("Original Intake-->", courseIntakeStr);

                            console.log("intakee-->", courseIntakeStr);
                            console.log(courseIntakeStr.toString());
                            //  splitdata = splitdata.replace(",", "");
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var campus = resJsonData.course_campus_location;
                                var intakes = [];
                                var intakedetail = {};
                                var myintake = [];
                                var myintake_data = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                                var researchintakedates = await utils.getValueFromHardCodedJsonFile('course_intake_research');
                                var tintakedates = await utils.getValueFromHardCodedJsonFile('course_intake_trimester');
                                if (courseIntakeStr.toLowerCase().indexOf('research') > -1) {
                                    if (courseIntakeStr.indexOf('and') > -1) {
                                        courseIntakeStr = courseIntakeStr.replace(/and/, ',');
                                        console.log("After replace courseIntakeStr -->", courseIntakeStr);
                                        let splitIntake = courseIntakeStr.split(',');
                                        console.log("splitIntake --> ", splitIntake);
                                        for (let intake of splitIntake) {
                                            for (let rintake of researchintakedates) {
                                                let splitrintake = rintake.split('-');
                                                if (intake.indexOf(splitrintake[0].trim()) > -1) {
                                                    myintake.push(splitrintake[1].trim());
                                                }
                                            }
                                        }
                                    } else {

                                        for (let rintake of researchintakedates) {
                                            let splitrintake = rintake.split('-');
                                            if (courseIntakeStr.indexOf(splitrintake[0].trim()) > -1) {
                                                myintake.push(splitrintake[1].trim());
                                            }
                                        }

                                    }
                                }
                                else if
                                    (courseIntakeStr.toLowerCase().indexOf('trimester') > -1) {
                                    if (courseIntakeStr.indexOf('and') > -1) {
                                        courseIntakeStr = courseIntakeStr.replace(/and/, ',');
                                        console.log("After replace courseIntakeStr2 -->", courseIntakeStr);
                                        let splitIntake = courseIntakeStr.split(',');
                                        console.log("splitIntake --> ", splitIntake);
                                        for (let intake of splitIntake) {
                                            for (let tintake of tintakedates) {
                                                let splittintake = tintake.split('-');
                                                if (intake.indexOf(splittintake[0].trim()) > -1) {
                                                    myintake.push(splittintake[1].trim());
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        let splitIntake = [];
                                        if (courseIntakeStr.indexOf(",") > -1) {
                                            splitIntake = courseIntakeStr.split(',');
                                        }
                                        else {
                                            splitIntake.push(courseIntakeStr);
                                        }

                                        console.log("splitIntake --> ", splitIntake);
                                        for (let intake of splitIntake) {
                                            for (let tintake of tintakedates) {
                                                let splittintake = tintake.split('-');
                                                if (intake.indexOf(splittintake[0].trim()) > -1) {
                                                    myintake.push(splittintake[1].trim());
                                                }
                                            }
                                        }
                                    }
                                }


                                console.log("myintake ---->", myintake);

                                for (let loc of campus) {
                                    var intakedetail = {};
                                    intakedetail.name = loc.name;
                                    intakedetail.value = myintake;
                                    //  var intakedetail = {};
                                    // intakedetail.name = campus[camcount];
                                    // intakedetail.value = myintake;
                                    intakes.push(intakedetail);
                                }

                                var intakedata = {};
                                let formatedIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                intakedata.intake = formatedIntake;
                                var intakeUrl = "";
                                intakeUrl = (await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedata.more_details = intakeUrl;
                                resJsonData.course_intake = intakedata;
                            }
                            break;
                        }

                        case 'course_scholarship': {
                            const courseKeyVal = courseScrappedData.course_scholarship;
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                resJsonData.course_scholarship = resScholarshipJson;
                            }
                            break;
                        }
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                        case 'course_accomodation_cost': {
                            const courseKeyVal = courseScrappedData.course_accomodation_cost;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_accomodation_cost = resAccomodationCostJson;
                            }
                            break;
                        }

                        case 'course_study_level': {
                            resJsonData.course_study_level = studyLevel;
                            break;
                        }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_country = resAccomodationCostJson;
                            }
                            break;
                        }
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const outcome_data = await Course.extractValueFromScrappedElement(courseScrappedData.course_career_outcome)
                            //console.log("outttttttcome:"+outcome_data);
                            if (outcome_data) {
                                var outcome = await utils.giveMeArray(outcome_data.replace(/[\r\n\t]+/g, ';'), ';');
                                if (outcome.length > 0) {
                                    resJsonData.course_career_outcome = outcome;
                                    console.log("#### Outcome is--->" + resJsonData.course_career_outcome);
                                } else {
                                    resJsonData.course_career_outcome = [];
                                }
                            }
                            else {
                                resJsonData.course_career_outcome = [];
                            }
                            console.log("#### Outcome is--->" + resJsonData.course_career_outcome);
                            break;
                        }
                        // case 'course_career_outcome': {
                        //     console.log(funcName + 'matched case: ' + key);
                        //     const rootElementDictList = courseScrappedData[key];
                        //     console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                        //     let concatnatedRootElementsStr = null;
                        //     for (const rootEleDict of rootElementDictList) {
                        //         console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //         const elementsList = rootEleDict.elements;
                        //         let concatnatedElementsStr = null;
                        //         for (const eleDict of elementsList) {
                        //             console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //             const selectorsList = eleDict.selectors;
                        //             let concatnatedSelectorsStr = null;
                        //             for (const selList of selectorsList) {
                        //                 console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                 console.log(funcName + '\n\r selList = ' + selList);
                        //                 let concatnatedSelStr = null;
                        //                 for (const selItem of selList) {
                        //                     if (!concatnatedSelStr) {
                        //                         concatnatedSelStr = selItem;
                        //                     } else {
                        //                         concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                        //                     }
                        //                 } // selList
                        //                 console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                        //                 if (concatnatedSelStr) {
                        //                     if (!concatnatedSelectorsStr) {
                        //                         concatnatedSelectorsStr = concatnatedSelStr;
                        //                     } else {
                        //                         concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                        //                     }
                        //                 }
                        //             } // selectorsList
                        //             console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                        //             // concat elements
                        //             if (concatnatedSelectorsStr) {
                        //                 if (!concatnatedElementsStr) {
                        //                     concatnatedElementsStr = concatnatedSelectorsStr;
                        //                 } else {
                        //                     concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                        //                 }
                        //             }
                        //         } // elementsList
                        //         console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                        //         if (concatnatedElementsStr) {
                        //             if (!concatnatedRootElementsStr) {
                        //                 concatnatedRootElementsStr = concatnatedElementsStr;
                        //             } else {
                        //                 concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                        //             }
                        //         }
                        //     } // rootElementDictList
                        //     console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
                        //     // add only if it has valid value
                        //     let concatnatedRootElementsStrArray = [];
                        //     if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                        //         concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                        //         resJsonData[key] = concatnatedRootElementsStrArray;
                        //     }
                        //     break;
                        // }
                        case 'course_title': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                concatnatedRootElementsStr = this.titleCase(concatnatedRootElementsStr);
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            } else {
                                throw new Error('Title not found');
                            }
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            ////start genrating new file location wise
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase() + "_" + resJsonData.program_code;
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                console.log("intakadata--->", JSON.stringify(resJsonData.course_intake.intakes));
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "[]";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }

            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            const elmstudy = await utils.checkElementAvailabilty(s.page, '#tab-links > li > a', "", "InnerText", "Study Pattern");
            var promise1 = Promise.resolve(elmstudy);
            promise1.then(async function (value) {
                if (value) {
                    const link1 = await s.page.$('#tab-links > li:nth-child(4) > a');
                    await link1.click();
                    await s.page.waitForSelector('#study_pattern > div > div > p:nth-child(1) > a', s.WAIT_FOR_SEL_OPTIONS);
                    const link = await s.page.$('#study_pattern > div > div > p:nth-child(1) > a');
                    await link.click();
                }
                else console.log('#####--study pattern not found--#####');
            });

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.category, courseDict.studyLevel);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };

    // scrape each course details
//     static async scrapeAndFormatCourseDetails(courseDict) {
//         const funcName = 'scrapeAndFormatCourseDetails ';
//         try {
//             const courseScrappedData = await Course.scrapeCourseDetails(courseDict);
//             s = new Scrape();
//             await s.init({ headless: true });
//             await s.setupNewBrowserPage(courseDict.href);
//             if (courseScrappedData === null) {
//                 return null;
//             }
//             //const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, page.url(), courseDict.category,courseDict.innerText, );
//             const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.category, courseDict.innerText);
//             if (formattedScrappedData === null) {
//                 console.log(funcName + 'formattedScrappedData is null...');
//                 return null;
//             }

//             const finalScrappedData = await Course.removeKeys(formattedScrappedData);
//             return finalScrappedData;
//         } catch (error) {
//             console.log(funcName + 'try-catch error = ' + error);
//             return null;
//         }
//     } // scrapeAndFormatCourseDetails
// } // class
// module.exports = { ScrapeCourse };
