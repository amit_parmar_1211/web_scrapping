const puppeteer = require('puppeteer');
const fs = require("fs");
async function start() {
    const URL = "https://www.griffith.edu.au/study";
    let browser = await puppeteer.launch({ headless: false }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });

    const categoryselectorUrl = "body > main > div.personalised-content.study-study-areas.pers--dynamic.pers--onload.skrollable.skrollable-after > div > div > div > div > a";
    const categoryselectorText = "body > main > div.personalised-content.study-study-areas.pers--dynamic.pers--onload.skrollable.skrollable-after > div > div > div > div > a > span";
    var elementstring = "", elementhref = "", allcategory = [];

    const targetLinksCardsUrls = await page.$$(categoryselectorUrl);

    const targetLinksCardsText = await page.$$(categoryselectorText);
    
    
    var targetLinksCardsTotal = [];
    for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        elementstring = await page.evaluate(el => el.innerText, targetLinksCardsText[i]);
        elementhref = await page.evaluate(el => el.href, targetLinksCardsUrls[i]);
        targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
    }
    // await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
    console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));


    let linkselector = "//div[@class='container results']//div[@class='row']//div[@class='row search-result']/div/h4/a";
    let textselector = "//div[@class='container results']//div[@class='row']//div[@class='row search-result']/div/h4/a"
    var totalCourseList = [];
    for (let target of targetLinksCardsTotal) {
        await page.goto(target.href, { timeout: 0 });
        //scrape university courselist
        // let courseButtonClick = "//a[contains(@title,'Courses')]";
        await page.waitFor(2000);
        var clickbtn = await page.$x("//input[@class='button--search']");
        console.log("Clicked");
        await clickbtn[0].click();
        console.log("After Click");
        // console.log("Clicked");
        // let check = "#filter-graduate";
        // await page.evaluate((check) => document.querySelector(check).click(), check);
        await page.waitFor(8000);
        const targetLinks = await page.$x(linkselector);
        const targetText = await page.$x(textselector);
        console.log("target.innerText -->", target.innerText);
        console.log("#total link selectors---->" + targetLinks.length);
        for (var i = 0; i < targetLinks.length; i++) {
            var elementstring = await page.evaluate(el => el.innerText, targetText[i]);
            const elementlink = await page.evaluate(el => el.href, targetLinks[i]);
            totalCourseList.push({ href: elementlink, innerText: elementstring, category: target.innerText });
        }
    }
    console.log("totalCourseList -->", totalCourseList);
    fs.writeFileSync("griffithuniversity_category_courselist.json", JSON.stringify(totalCourseList));
    await browser.close();

}
start();
