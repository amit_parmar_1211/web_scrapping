const fs = require('fs');
const Scrape = require('../common/scrape').Scrape;
const utils = require('../common/utils');
const configs = require('../configs');

const appConfigs = require('../common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping() {
    var s = null;
    const funcName = 'scrapeCourseCategoryList';
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];
      //var mainCategory = [];
      var studyLevelArray = [];
      // set page to url
      await s.setupNewBrowserPage("https://degrees.griffith.edu.au/");
      const buttonInternational = "//*[@id='degreeApp']/div[5]/div[1]/div[1]/div[1]/div[4]/div/ul/li[2]/label";
      console.log("clicked ! ! ! ! ");
      var buttonInternationalelem = await s.page.$x(buttonInternational);
      await buttonInternationalelem[0].click();
      const maincategoryselector = "//*[@id='degreeApp']/div[5]/div[1]/div[1]/div[1]/div[7]/div/div/div/div/ul/li/label";
      var category = await s.page.$x(maincategoryselector);

      var studyAreabtn = await s.page.$x("//button[contains(text(),'Study area')]");
      await studyAreabtn[0].click();
      const studyLevelselectorName = "//*[@id='degreeApp']/div[5]/div[1]/div[1]/div[1]/div[2]/div/ul//label";
      var studyLevelName = await s.page.$x(studyLevelselectorName);

      await s.page.waitFor(1000);
      for (let studylink of studyLevelName) {

        var studyLevelmain = await s.page.evaluate(el => el.innerText, studylink);
        await studylink.click();
        await s.page.waitFor(1000);
        studyLevelArray.push(studyLevelmain);
        const titleselector = "//article[@class='result degree']//h2";
        var categoryselector = '//*[@id="degreeApp"]/div[5]/div[1]/div[2]/div[2]/article/header';

        var title = await s.page.$x(titleselector);
        var category = await s.page.$x(categoryselector);

        for (let i = 0; i < title.length; i++) {

          var categorystring = await s.page.evaluate(el => el.innerText, title[i]);
          var categorymain = await s.page.evaluate(el => el.innerText, category[i]);
          var categ1 = categorymain.split('Study Area(s):')[1];
         


          var viewDegreeLink = "//article[@class='result degree']//h2[text()='" + categorystring + "']/../following-sibling::div[@class='results-content']/div[@class='tbody']//div[@class='td'][a]/a";
         
          var viewDegree = await s.page.$x(viewDegreeLink);
          
          for (let j = 0; j < viewDegree.length; j++) {
            var dlink = await s.page.evaluate(el => el.href, viewDegree[j]);
       
            const regEx = /\d+\.?\d*/g; 
            let ProgramCode = dlink.match(regEx);
          
            totalCourseList.push({ href: dlink, innerText: categorystring + "(" + ProgramCode + ")", category: categ1, studyLevel: studyLevelmain });
          }
        }
        await studylink.click();
      }

      await fs.writeFileSync("./output/griffithuniversity_original_courselist.json", JSON.stringify(totalCourseList));
      //await fs.writeFileSync("./output/griffithuniversity_main_category_courses.json", JSON.stringify(totalCourseList));

      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, studyLevel: totalCourseList[i].studyLevel, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, studyLevel: totalCourseList[i].studyLevel, category: [] });
        }
      }

      //  console.log("uniquefilegenerated-->");
      await fs.writeFileSync("./output/griffithuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }

          }
        }
      }

      let finalarray = [];
      for (let i = 0; i < uniqueUrl.length; i++) {
        var categ2;
        console.log("uniqueUrl[i].category-->", uniqueUrl[i].category);
        for (let j = 0; j < uniqueUrl[i].category.length; j++) {
          if (uniqueUrl[i].category[j].indexOf(';') > -1) {
            categ2 = uniqueUrl[i].category[j].split(';');
          } else {
            categ2 = [uniqueUrl[i].category[j]];
          }
          console.log("categ22222--->", categ2);
        }
        finalarray.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, studyLevel: totalCourseList[i].studyLevel, category: categ2 })


      }
      await fs.writeFileSync("./output/griffithuniversity_courselist.json", JSON.stringify(finalarray));
      //console.log(funcName + 'writing courseList to file completed successfully....');
      return finalarray;
    } catch (error) {
      console.log(funcName + 'try-catch error ayushi = ' + error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
