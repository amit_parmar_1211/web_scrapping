"course_outline_core": [
        {
            "url": "",
            "elements": [
                {
                    "elementType": "scrape_by_action",
                    "selectors": [
                        {
                            "action": "CLICK",
                            "isxpath": true,
                            "selector": "html/body/div[2]/div[3]/div[1]/div/ul/li[2]/a[contains(text(),'International students')]"
                        },
                        {
                            "action": "WAIT",
                            "istime": true,
                            "time": 2000
                        },
                        {
                            "action": "CLICK",
                            "isxpath": true,
                            "remover": [],
                            "iscomb": false,
                            "selector": "/html/body/div[2]/div[4]/div/div[1]/div[1]/ul/li[3]/a"
                        },
                        {
                            "action": "WAIT",
                            "istime": true,
                            "time": 2000
                        },
                        {
                            "action": "SCRAPE",
                            "isxpath": true,
                            "remover": [],
                            "iscomb": false,
                            "selector": "//*/div/div/div[1]/table/tbody/tr"
                        }
                    ]
                }
            ]
        }
    ],
    "course_outline_major": [
        {
            "url": "",
            "elements": [
                {
                    "elementType": "scrape_by_action",
                    "selectors": [
                        {
                            "action": "CLICK",
                            "isxpath": true,
                            "selector": "html/body/div[2]/div[3]/div[1]/div/ul/li[2]/a[contains(text(),'International students')]"
                        },
                        {
                            "action": "WAIT",
                            "istime": true,
                            "time": 2000
                        },
                        {
                            "action": "CLICK",
                            "isxpath": true,
                            "selector": "/html/body/div[2]/div[4]/div/div[1]/div[1]/ul/li[3]/a"
                        },
                        {
                            "action": "scrape",
                            "isxpath": true,
                            "selectors": [
                                "//*[@id='66946914']/div/div/table"
                            ]
                        }
                    ]
                }
            ]
        }
    ],