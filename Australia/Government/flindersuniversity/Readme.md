**Flinders University: Total Courses Details As On 25 April 2019**
* Total Courses = 271
* Courses without CRICOS code = 
* Total available courses = 
* Repeated = 


**Notes**
* Extremely different format for postgraduate courses.
* Postgraduate courses have single url in which multiple course details are mentioned.
* Two layouts for postgraduate courses.
* Extreme customization in all files.
* Don't use as base for any university.
* Location not given for postgraduate courses. Bedford Park taken as default.
* Academic requirement for undergraduate is static and is scraped for postgraduate courses.
* Made changes in hasAnyValidCricosCode() function.
* special case handled for course index 167 where location is mentioned in delivery mode.
* duration,delivery mode, intake, cricos and fees for postgrauddate courses are fetched from postgraduate_courselist.json file.


**2019 & conditions**
* 2019 is statically added after month in scrapeCourse.js course_intake.
* 2019 and 2020 statically specified in conditions for fees scrapeCourse.js


 {
                    "elementType": "anchor",
                    "selectors": [
                        "div.section>div.component_section.white_container_featured> div.container > div.row > div.col-md-6 a"
                    ]
                }