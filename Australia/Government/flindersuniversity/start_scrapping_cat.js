const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;

const startScrapping = async function startScrappingFunc() {
  const funcName = 'startScrappingFunc ';
  try {
    s = new Scrape();
    await s.init({ headless: true });
    //var datalist = [];
    await s.setupNewBrowserPage("https://www.flinders.edu.au/international");
    var mainCategory = [], redirecturl = [],datalist = [];
    //scrape main category
    const maincategoryselector = "/html/body/div[1]/section/div/div[3]/div/div/div/div/div/div/div/div/div/div/div/div/p/a";
    var category = await s.page.$x(maincategoryselector);
    console.log("Total categories-->" + category.length);
    for (let link of category) {
      var categorystringmain = await s.page.evaluate(el => el.innerText, link);
      var categorystringmainurl = await s.page.evaluate(el => el.href, link);
      mainCategory.push(categorystringmain.trim());
      redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
    }
    console.log("REdirectURL",JSON.stringify(redirecturl))

    //scrape courses
    for (let catc of redirecturl) {
      await s.page.goto(catc.href, { timeout: 0 });
      const selector = "//*/div[@class='course_list_component']//ul/li/a";
      var title = await s.page.$x(selector);
      for (let t of title) {
        var categorystring = await s.page.evaluate(el => el.innerText, t);
        var categoryurl = await s.page.evaluate(el => el.href, t);
        datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: catc.innerText });
      }
    }

    fs.writeFileSync("./output/universityofcanberra_courselist.json", JSON.stringify(datalist))
    fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
    await s.browser.close();
    console.log(funcName + 'browser closed successfully.....');
    return true;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
};
startScrapping();