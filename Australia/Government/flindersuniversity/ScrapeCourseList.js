const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    var mainCategory = [], redirecturl = [], datalist = [];
    const maincategoryselector = "/html/body/div[1]/section/div/div[3]/div/div/div/div/div/div/div/div/div/div/div/div/p/a";
    var category = await page.$x(maincategoryselector);
    let selector, selectors
    console.log("Total categories-->" + category.length);
    for (let link of category) {
      var categorystringmain = await page.evaluate(el => el.innerText, link);
      var categorystringmainurl = await page.evaluate(el => el.href, link);
      mainCategory.push(categorystringmain.trim());
      redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
    }
    console.log("REdirectURL", JSON.stringify(redirecturl))
     for(let i=0;i<redirecturl.length;i++){
      await page.goto(redirecturl[i].href, { timeout: 0 });
      var studylevel="//div[@class='accordion_item']/a[contains(text(),'Undergraduate') or contains(text(),'Graduate')]";
      var level=await page.$x(studylevel)
      var educationLevelCatList = [];
      let elementstring1 = "";
      let elementhref1 = "";
      for (let i = 0; i < level.length; i++) {
        elementstring1 = await page.evaluate(el => el.innerText, level[i]);
        elementhref1 = await page.evaluate(el => el.href, level[i]);
        educationLevelCatList.push({ href: elementhref1, innerText: elementstring1 });
      }
      //console.log("#### Study_level---" + JSON.stringify(educationLevelCatList));
      selector = "//div[@class='accordion_item']/a[contains(text(),'Undergraduate') or contains(text(),'Graduate')]/following-sibling::div//a";
      for( var level of educationLevelCatList){
      
      var title = await page.$x(selector);

      for (let t of title) {
        var categorystring = await page.evaluate(el => el.innerText, t);
        var categoryurl = await page.evaluate(el => el.href, t);
        datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category:redirecturl[i].innerText,studylevel:level.innerText });
      }
    }
  }
  await fs.writeFileSync("./output/flindersuniversity_original_courselist.json", JSON.stringify(datalist));
    console.log("CategoryUndergraduate",datalist)
    //Postgraduate course

  const postgradurlselector = [];
  var postGradCourseList = [];
  let pageUrl
  let pageText
    //layout case 1: 1 to 3 vertical
    postgradurlselector.push({
      name: "//div[contains(@class,'international_content')]/div[@class='container ']/h2[contains(text(),'Course')]/..//div[@class='rte text parbase']//p[1]/span[1]",
      duration: "//div[contains(@class,'international_content')]/div[@class='container ']/h2[contains(text(),'Course')]/..//div[@class='rte text parbase']/p[2]",
      delivery: "//div[contains(@class,'international_content')]/div[@class='container ']/h2[contains(text(),'Course')]/..//div[@class='rte text parbase']/p[3]",
      intake: "//div[contains(@class,'international_content')]/div[@class='container ']/h2[contains(text(),'Course')]/..//div[@class='rte text parbase']/p/b[contains(text(),'Start dates')]/..",
      cricos: "//div[contains(@class,'international_content')]/div[@class='container ']/h2[contains(text(),'Course')]/..//div[@class='rte text parbase']/p/b[contains(text(),'CRICOS')]/..",
      fees: "//div[contains(@class,'international_content')]/div[@class='container ']/h2[contains(text(),'Course')]/..//div[@class='rte text parbase']/p/b[contains(text(),'fees')]/.."
    });
    //layout2: multiple horizontal
    postgradurlselector.push({
      name: "//div[contains(@class,'international_content')]/div[@class='container ']/h2[contains(text(),'Course')]/..//div[@class='accordion-card']/div/a[not(contains(text(),'Learn more'))]",
      duration: "//div[contains(@class,'international_content')]/div[@class='container ']/h2[contains(text(),'Course')]/..//div[@class='accordion-card']/div/a[not(contains(text(),'Learn more'))]/../..//div[@class='rte text parbase']/p[1]",
      delivery: "//div[contains(@class,'international_content')]/div[@class='container ']/h2[contains(text(),'Course')]/..//div[@class='accordion-card']/div/a[not(contains(text(),'Learn more'))]/../..//div[@class='rte text parbase']/p[2]",
      intake: "//div[contains(@class,'international_content')]/div[@class='container ']/h2[contains(text(),'Course')]/..//div[@class='accordion-card']/div/a[not(contains(text(),'Learn more'))]/../..//div[@class='rte text parbase']/p/b[contains(text(),'Start dates')]/..",
      cricos: "//div[contains(@class,'international_content')]/div[@class='container ']/h2[contains(text(),'Course')]/..//div[@class='accordion-card']/div/a[not(contains(text(),'Learn more'))]/../..//div[@class='rte text parbase']/p/b[contains(text(),'CRICOS')]/..",
      fees: "//div[contains(@class,'international_content')]/div[@class='container ']/h2[contains(text(),'Course')]/..//div[@class='accordion-card']/div/a[not(contains(text(),'Learn more'))]/../..//div[@class='rte text parbase']/p/b[contains(text(),'fees')]/.."
    });
    for (let targets of redirecturl) {
      await page.goto(targets.href, { timeout: 0 });
      //await page.goto(targets.href,{ timeout: 0 });
      var pgstudylevel="//div[@class='accordion_item']/a[contains(text(),'Postgraduate ') or contains(text(),'Higher degree by research')]";
   
      var pglevel=await page.$x(pgstudylevel)
      var educationLevelCatList = [];
      let elementstring1 = "";
      let elementhref1 = "";
      for (let i = 0; i < pglevel.length; i++) {
        elementstring1 = await page.evaluate(el => el.innerText, pglevel[i]);
        elementhref1 = await page.evaluate(el => el.href, pglevel[i]);
        
        educationLevelCatList.push({ href: elementhref1, innerText: elementstring1 });
      }
      //console.log("DATA@@@@",educationLevelCatList)
      for(let j=0;j<educationLevelCatList.length;j++){
        var study_level=educationLevelCatList[j].innerText
        //postGradcategoryUrlSelector="//*/div[3]/div[1]/div[1]/div/div/a[contains(text()'" +  study_level + "']/following-sibling::div//a"
       const postGradcategoryUrlSelector="//div[contains(@class,'accordion_item')]/a[(contains(text(),'" +  study_level + "'))]/following-sibling::div//a"
        var postGradcategoryUrlHandles = await page.$x(postGradcategoryUrlSelector);
        var postGradcategoryTextHandles = await page.$x(postGradcategoryUrlSelector);
        var category = targets.innerText;
        console.log("Length", postGradcategoryUrlHandles.length)
        console.log("category", JSON.stringify(category))
        for (let i = 0; i < postGradcategoryUrlHandles.length; i++) {
           pageUrl = await page.evaluate(el => el.href, postGradcategoryUrlHandles[i]);
          pageText = await page.evaluate(el => el.innerText, postGradcategoryTextHandles[i]);
          //let categorys = targets.innerText
          datalist.push({ href: pageUrl, innerText: pageText.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s+)/g, ' ').trim(), category:targets.innerText,studylevel:study_level  })
          console.log("TotalCourseList_PG-->",datalist)
          await fs.writeFileSync("./output/flindersuniversity_original_courselist.json", JSON.stringify(datalist));

          console.log('******************Started ', pageText.trim(), '*****************************');
          let s = null;
          s = new Scrape();
          await s.init({ headless: true });
          await s.setupNewBrowserPage(pageUrl);
          //await page.goto(pageUrl);
  
          let postgradName_H, postgradDuration_H, postgradDelivery_H, postgradIntake_H, postgradCricos_H, postgradFees_H;
          postgradName_H = await s.page.$x(postgradurlselector[0].name);
  
          if (postgradName_H.length > 0) {
            //layout 1 handles
            postgradDuration_H = await s.page.$x(postgradurlselector[0].duration);
            postgradDelivery_H = await s.page.$x(postgradurlselector[0].delivery);
            postgradIntake_H = await s.page.$x(postgradurlselector[0].intake);
            postgradCricos_H = await s.page.$x(postgradurlselector[0].cricos);
            postgradFees_H = await s.page.$x(postgradurlselector[0].fees);
          }
          else {
            //layout 2
            postgradName_H = await s.page.$x(postgradurlselector[1].name);
            postgradDuration_H = await s.page.$x(postgradurlselector[1].duration);
            postgradDelivery_H = await s.page.$x(postgradurlselector[1].delivery);
            postgradIntake_H = await s.page.$x(postgradurlselector[1].intake);
            postgradCricos_H = await s.page.$x(postgradurlselector[1].cricos);
            postgradFees_H = await s.page.$x(postgradurlselector[1].fees);
          }
          for (let i = 0; i < postgradName_H.length; i++) {
            let name, duration, delivery, intake, cricos, fees, categoryss,study_levels;
        
            name = await s.page.evaluate(el => el.innerText, postgradName_H[i]);
            duration = await s.page.evaluate(el => el.innerText, postgradDuration_H[i]);
            delivery = await s.page.evaluate(el => el.innerText, postgradDelivery_H[i]);
            intake = await s.page.evaluate(el => el.innerText, postgradIntake_H[i]);
            categoryss = targets.innerText

            
            if (postgradCricos_H.length > 0 && postgradCricos_H[i]) {
  
              cricos = await s.page.evaluate(el => el.innerText, postgradCricos_H[i]);
            }
            else {
              cricos = "";
            }
            fees = await s.page.evaluate(el => el.innerText, postgradFees_H[i]);
            let exists1 = postGradCourseList.some(value => { return value.name == name.trim() });
            if (exists1 == false) {
              console.log("INEXItS1", targets.innerText)
              postGradCourseList.push({
                key: pageUrl,
                name: name.trim(),
                duration: duration.trim(),
                delivery: delivery.trim(),
                intake: intake.trim(),
                cricos: cricos.trim(),
                fees: fees.trim(),
                category: categoryss,
                
              });
            }
          }
          //totalCourseList.push({ href: pageUrl, innerText: pageText.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s+)/g, ' ').trim(), category:targets.innerText,studylevel:study_level  })
          if (s) {
            await s.close();
          }
          console.log('postGradCourseList = ' + JSON.stringify(postGradCourseList));
        await fs.writeFileSync("./output/flindersuniversity_postgraduate_courselist.json", JSON.stringify(postGradCourseList));
    }
  }
     
}  
let uniqueUrl = [];
//unique url from the courselist file
for (let i = 0; i < datalist.length; i++) {
  let cnt = 0;
  if (uniqueUrl.length > 0) {
    for (let j = 0; j < uniqueUrl.length; j++) {
      if (datalist[i].href == uniqueUrl[j].href) {
        cnt = 0;
        break;
      } else {
        cnt++;
      }
    }
    if (cnt > 0) {
      uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [],studylevel:datalist[i].studylevel});
    }
  } else {
    uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [],studylevel:datalist[i].studylevel});
  }
}

await fs.writeFileSync("./output/flindersuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
//based on unique urls mapping of categories
for (let i = 0; i < datalist.length; i++) {
for (let j = 0; j < uniqueUrl.length; j++) {
  if (uniqueUrl[j].href == datalist[i].href) {
    if (uniqueUrl[j].category.includes(datalist[i].category)) {

    } else {
      uniqueUrl[j].category.push(datalist[i].category);
    }

  }
}
}
console.log("totalCourseList -->", uniqueUrl);
await fs.writeFileSync("./output/flindersuniversity_courselist.json", JSON.stringify(uniqueUrl));
}

  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
   
  }

  // refers to https://futurestudents.csu.edu.au/courses/all
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // course_level_form_selector key
      // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelList) {
      //   console.log(funcName + 'Invalid formSelList');
      //   throw (new Error('Invalid formSelList'));
      // }
      // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelElementDict) {
      //   console.log(funcName + 'Invalid formSelElementDict');
      //   throw (new Error('Invalid formSelElementDict'));
      // }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      // const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      // console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
