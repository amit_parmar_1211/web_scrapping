const fs = require('fs');
const awsUtil = require('./common/aws_utils');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');
//Additional
const postgradCourseList = JSON.parse(fs.readFileSync("./output/flindersuniversity_postgraduate_courselist.json"));


class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    //static async formatOutput(courseScrappedData, courseUrl, course_name) 
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, study_level) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            console.log("course_name",course_name)
            const postGradIndex = postgradCourseList.findIndex(value => value.name == course_name);
            console.log("postGradIndex", postGradIndex);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            let arr = []
                            if (postGradIndex > -1) {
                                let categorys = postgradCourseList[postGradIndex].category;
                                console.log("Category", categorys)
                                var cat = await utils.giveMeArray(categorys.replace(/[\r\n\t]+/g, ''));
                                console.log("CAT!!!", cat)
                                resJsonData.course_discipline = cat
                            } else {

                                resJsonData.course_discipline = course_category;
                            }
                            break;
                        }
                        case 'program_code': {
                            let code = "";
                            // code.push("NA");
                            resJsonData.program_code = code;
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { " ": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {};
                            const pteDict = {};
                            const tofelDict = {};
                            const pbtDict = {};
                            const caeDict = {};
                            const ibtDict = {};
                            let ieltsNumber = null;
                            const regEx = /[+-]?\d+(\.\d+)?/g;

                            //const title = resJsonData.course_title;

                            const title11 = resJsonData.course_title;
                            const title = title11.toLowerCase();
                            console.log("title -->", title);
                            const study_level = resJsonData.course_study_level;
                            console.log("R ielts study_level", study_level, "R ielts title", title);
                            const ieltsMappingDict = JSON.parse(fs.readFileSync(appConfigs.ieltsMapping));
                            //console.log("R ieltsMappingDict", JSON.stringify(ieltsMappingDict[1]));                                

                            if (study_level == "Undergraduate") {
                                if (title.indexOf("arts - enhanced program for high achievers") > -1 || title.indexOf("human nutrition") > -1 || title.indexOf("midwifery") > -1 || title.indexOf("nursing (preregistration)") > -1 || title.indexOf("bachelor of nursing (graduate entry)") > -1 || title.indexOf("bachelor of nutrition and dietetics") > -1 || title.indexOf("bachelor of exercise science") > -1 || title.indexOf("bachelor of psychology (honours)") > -1 || title.indexOf("bachelor of psychological studies (graduate entry)") > -1 || title.indexOf("bachelor of psychological science") > -1) {
                                    //graduate exceptionCase1
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[0].graduate[1].exceptionCase1.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);

                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[0].graduate[1].exceptionCase1.toefl_ibt;;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    // tofelDict.name = 'toefl ibt';
                                    // tofelDict.description = ieltsMappingDict[0].graduate[1].exceptionCase1.toefl_ibt;

                                    //englishList.push(tofelDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[0].graduate[1].exceptionCase1.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    courseAdminReq.english = englishList;
                                }
                                else if (title.indexOf("nursing (post-registration)") > -1) {
                                    //does not exist in course list
                                    //NursingPostRegistration
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[0].graduate[2].NursingPostRegistration.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);


                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[0].graduate[2].NursingPostRegistration.toefl_ibt;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[0].graduate[2].NursingPostRegistration.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    courseAdminReq.english = englishList;
                                }
                                else if (title.indexOf("bachelor of laws and legal practice") > -1 || title.indexOf("bachelor of laws and legal practice (honours)") > -1 || title.indexOf("bachelor of speech pathology") > -1) {
                                    //LawLegalPractice_SpeechPathology
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[0].graduate[3].LawLegalPractice_SpeechPathology.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo;
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);



                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[0].graduate[3].LawLegalPractice_SpeechPathology.toefl_ibt;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[0].graduate[3].LawLegalPractice_SpeechPathology.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    courseAdminReq.english = englishList;

                                }
                                else if (title.indexOf("education") > -1 || title.indexOf("medicine") > -1) {
                                    //educationMedicine
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[0].graduate[4].educationMedicine.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);


                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[0].graduate[4].educationMedicine.toefl_ibt;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[0].graduate[4].educationMedicine.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    courseAdminReq.english = englishList;
                                }
                                else {
                                    //default
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[0].graduate[0].default.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);



                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[0].graduate[0].default.toefl_ibt;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[0].graduate[0].default.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    courseAdminReq.english = englishList;
                                }
                            }
                            else if (study_level == "Postgraduate") {
                                if (title.indexOf("accounting") > -1 || title.indexOf("arts – tesol") > -1 || title.indexOf("audiology") > -1 || title.indexOf("business administration") > -1 || title == "master of business" || title.indexOf("midwifery") > -1 || title.indexOf("nursing") > -1 || title.indexOf("social work") > -1 || title.indexOf("teaching english as a second language") > -1 || title.indexOf("laws (international law and international relations)") > -1) {
                                    //Arts – TESOL does not exist
                                    //exceptionCase1
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[1].postGraduate[1].exceptionCase1.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);


                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[1].postGraduate[1].exceptionCase1.toefl_ibt;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[1].postGraduate[1].exceptionCase1.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    courseAdminReq.english = englishList;
                                }
                                else if (title.indexOf("speech pathology") > -1 || title.indexOf("physiotherapy") > -1 || title.indexOf("nutrition and dietetics") > -1 || title.indexOf("occupational therapy") > -1) {
                                    //Physiotherapy_SpeechPathology_NutritionDietetics_OccupationalTherapy
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[1].postGraduate[2].Physiotherapy_SpeechPathology_NutritionDietetics_OccupationalTherapy.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);



                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[1].postGraduate[2].Physiotherapy_SpeechPathology_NutritionDietetics_OccupationalTherapy.toefl_ibt;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[1].postGraduate[2].Physiotherapy_SpeechPathology_NutritionDietetics_OccupationalTherapy.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);
                                    courseAdminReq.english = englishList;
                                }
                                else if (title.indexOf("medicine") > -1 || title.indexOf("cognitive behaviour therapy") > -1 || title.indexOf("juris doctor") > -1) {
                                    //Medicine_CognitiveBehaviour_JurisDoctor
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[1].postGraduate[3].Medicine_CognitiveBehaviour_JurisDoctor.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);


                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[1].postGraduate[3].Medicine_CognitiveBehaviour_JurisDoctor.toefl_ibt;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[1].postGraduate[3].Medicine_CognitiveBehaviour_JurisDoctor.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    courseAdminReq.english = englishList;
                                }
                                else if (title.indexOf("psychology (clinical)") > -1) {
                                    //Psychology_Clinical
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[1].postGraduate[4].Psychology_Clinical.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);


                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[1].postGraduate[4].Psychology_Clinical.toefl_ibt;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[1].postGraduate[4].Psychology_Clinical.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    courseAdminReq.english = englishList;
                                }
                                else if (title.indexOf("teaching") > -1) {
                                    //Teaching
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[1].postGraduate[5].Teaching.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);


                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[1].postGraduate[5].Teaching.toefl_ibt;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[1].postGraduate[5].Teaching.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    courseAdminReq.english = englishList;
                                }
                                else {
                                    //Teaching
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[1].postGraduate[0].default.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);


                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[1].postGraduate[0].default.toefl_ibt;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[1].postGraduate[0].default.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    courseAdminReq.english = englishList;
                                }
                            }
                            else if (study_level == "Higher degree by research") {
                                if (title.indexOf("clinical psychology") > -1) {
                                    //Teaching
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[2].research[1].exceptionCase1.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);


                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[2].research[1].exceptionCase1.toefl_ibt;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[2].research[1].exceptionCase1.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    courseAdminReq.english = englishList;
                                }
                                else {
                                    //default
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[2].research[0].default.ielts;
                                    let matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                    let ieltsNo = Number(matchedStrList_ielts[0]);
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNo
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);


                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[2].research[0].default.toefl_ibt;
                                    let matchedStrList_ibt = String(ibtDict.description).match(regEx);
                                    let ibtNo = Number(matchedStrList_ibt[0]);
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtNo;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[2].research[0].default.pte;
                                    let matchedStrList_pte = String(pteDict.description).match(regEx);
                                    let pteNo = Number(matchedStrList_pte[0]);
                                    pteDict.min = 0;
                                    pteDict.require = pteNo;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    courseAdminReq.english = englishList;
                                }
                            }

                            // const regEx = /[+-]?\d+(\.\d+)?/g;
                            const matchedStrList = String(englishList[0].description).match(regEx);
                            console.log('course_toefl_ielts_score matchedStrList = ' + JSON.stringify(matchedStrList));
                            if (matchedStrList && matchedStrList.length > 0) {
                                ieltsNumber = Number(matchedStrList[0]);
                                console.log('course_toefl_ielts_score ieltsNumber = ' + ieltsNumber);
                            }


                            // academic requirement
                            if (study_level == "Postgraduate" && courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                let arr = []
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                arr.push(academicReq)
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = arr;
                                }
                            }
                            else if (study_level == "Undergraduate") {
                                let arr = [];

                                arr.push("Flinders University recognises international secondary schooling for direct entry to undergraduate programs.");
                                courseAdminReq.academic = arr;

                            }
                            // if courseAdminReq has any valid value then only assign it
                            // if (courseAdminReq.english || courseAdminReq.academic) {
                            //     resJsonData.course_admission_requirement = courseAdminReq;
                            // }

                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.course_admission_english_more_details;
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))

                            resJsonData.course_admission_requirement = {};
                            if (courseAdminReq.english && courseAdminReq.english.length > 0) {
                                resJsonData.course_admission_requirement.english = courseAdminReq.english;
                            } else {
                                throw (new Error('ielts have some issue'));
                            }
                            if (courseAdminReq.academic && courseAdminReq.academic.length > 0) {
                                resJsonData.course_admission_requirement.academic = courseAdminReq.academic;
                            }
                            // add english requirement 'english_more_details' link
                            const entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            let academic_requirements_url;
                            if (study_level == "Undergraduate") {
                                academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url_undergrad);
                            }
                            else if (study_level == "Postgraduate") {
                                academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url_postgrad);
                            }
                            const english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            if (entry_requirements_url && entry_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            }
                            if (academic_requirements_url && academic_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;
                            }
                            if (english_requirements_url && english_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                            }


                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                    case 'course_duration_full_time': {
                            console.log("*************start formating years*************************");
                            const courseDurationList = [];
                            const courseDurationDisplayList = [];
                            const durationFullTime = {};
                            let durationFullTimeDisplay = null;
                            let fullTimeText;

                            if (postGradIndex > -1) {
                                fullTimeText = postgradCourseList[postGradIndex].duration.split(":")[1].trim();
                            }
                            else {
                                // console.log("courseScrappedData.course_duration_full_time",courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0]);                               
                                if (!courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0] || courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].length == 0) {
                                    console.log("Entered if");
                                    fullTimeText = courseScrappedData.course_duration_full_time[0].elements[0].selectors[1].toString().split(":")[1].replace("\n", "");
                                }
                                else {
                                    console.log("Entered else");
                                    fullTimeText = courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].toString();
                                }
                            }
                            console.log("*************start formating years*************************");
                            let temp1 = await format_functions.validate_course_duration_full_time(fullTimeText);
                            var full_year_formated_data = [{
                                "unit": temp1[0].unit,
                                "duration": temp1[0].duration,
                                "display": "Full-Time"
                            }];
                            //full_year_formated_data[0].display = "Full-Time";
                            console.log("R full_year_formated_data", full_year_formated_data);
                            ///course duration
                            const not_full_year_formated_data = await format_functions.not_formated_course_duration(fullTimeText);
                            durationFullTime.duration_full_time = not_full_year_formated_data.split("deferrable")[0].replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, '');
                            courseDurationList.push(durationFullTime);
                            ///END course duration

                            ///course duration display
                            durationFullTimeDisplay = {};
                            durationFullTimeDisplay = full_year_formated_data;
                            courseDurationDisplayList.push(durationFullTimeDisplay);
                            let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                            console.log("FilterData---->" + JSON.stringify(filtered_duration_formated))

                            ///END course duration display
                            console.log('***************END formating Full Time years**************************');

                            if (courseDurationList && courseDurationList.length > 0) {
                                resJsonData.course_duration =  not_full_year_formated_data.split("deferrable")[0].replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, '');
                            }
                            if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                resJsonData.course_duration_display = filtered_duration_formated;
                                var isfulltime = false, isparttime = false;
                                filtered_duration_formated.forEach(element => {
                                    if (element.display == "Full-Time") {
                                        isfulltime = true;
                                    }
                                    if (element.display == "Part-Time") {
                                        isparttime = true;
                                    }
                                });
                                resJsonData.isfulltime = isfulltime;
                                resJsonData.isparttime = isparttime;
                            }
                            // if (courseDurationDisplayList && courseDurationDisplayList.length > 0) {
                            //     resJsonData.course_duration_display = courseDurationDisplayList;
                            // }
                            console.log("*************END formating years*************************");

                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};
                            var feesIntStudent;
                            let currentText, futureText;
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (postGradIndex > -1) {

                                if (postgradCourseList[postGradIndex].fees.indexOf("To be advised") > -1) {
                                    //    feesDict.international_student.push({
                                    //         amount: 0,
                                    //         duration: 1,
                                    //         unit: "Year",
                                    //         isfulltime: true,
                                    //         description:"",
                                    //         type: ""
                                    //     });
                                    currentText = 0
                                    feesIntStudent = currentText
                                    futureText = ""


                                }
                                else {

                                    console.log("FINAl_FEESS@@", postgradCourseList[postGradIndex].fees.split("\n"))
                                    let fees = postgradCourseList[postGradIndex].fees.split("\n");
                                    console.log("DAta@@@@", fees[1])
                                    if (fees[1].includes("2020")) {
                                        currentText = fees[1].split(":")[1]
                                        futureText = ""
                                        feesIntStudent = currentText

                                    } else {
                                        currentText = fees[2].trim();
                                        //futureText = fees[2].trim();
                                        feesIntStudent = currentText.split(":")[1];
                                    }

                                }
                                console.log("FESSSS@@@@data", JSON.stringify(postgradCourseList[postGradIndex].fees))
                            }
                            else {
                                let fees = [];
                                //let diffLayout = false;
                                let courseKeyVal = courseScrappedData.course_tuition_fees_international_student;
                                console.log("FESSSS", JSON.stringify(courseKeyVal))
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    for (let selItem of selList) {
                                                        if (selItem.indexOf("\n") > -1) {
                                                            //diffLayout = true;
                                                            selItem = selItem.split("\n");
                                                            for (let sel of selItem) {
                                                                if (sel.indexOf("$") > -1) {
                                                                    fees.push(sel);
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            fees.push(selItem);
                                                        }

                                                        console.log("international_student_all_fees_array", fees);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (fees.length == 1) {
                                    console.log("2020only", fees[0])
                                    if (fees[0].indexOf("2020") > -1) {

                                        let currentText = fees[0].trim();
                                        feesIntStudent = currentText.split(":")[1];
                                        futureText = ""

                                    }
                                    else if (fees[0].indexOf("2019") > -1) {
                                        currentText = fees[0].trim();
                                        feesIntStudent = currentText.split(":")[1];
                                    }
                                    console.log("inif@@@");
                                }
                                else {
                                    for (let i = 0; i < fees.length; i++) {
                                        console.log("FEE[i]", JSON.stringify(fees[i]))
                                        if (fees[i].indexOf("2018") > -1) {
                                            currentText = fees[1].trim()
                                            console.log("Current Fees@@@", currentText)
                                            futureText = ""
                                            feesIntStudent = currentText.split(":")[1]
                                        }

                                        if (fees[i].indexOf("2020") > -1) {
                                            currentText = fees[i].trim();
                                            futureText = ""
                                            feesIntStudent = currentText.split(":")[1];
                                        }
                                        // if (fees[i].indexOf("2020") > -1) {
                                        //     futureText = fees[1].trim();
                                        // }
                                    }
                                    // if (diffLayout) {
                                    //     currentText = fees[1].trim();
                                    //     futureText = fees[0].trim();
                                    //     feesIntStudent = currentText.split(":")[1];
                                    // }
                                    // if(fees[0].indexOf("2019")>-1){
                                    //     currentText = fees[0].trim();
                                    //     futureText = fees[1].trim();
                                    //     feesIntStudent = currentText.split(":")[1];
                                    // }
                                }

                            }

                            // if (feeYear && feeYear.length > 0) {
                            //     courseTuitionFee.year = feeYear;
                            // }
                            console.log("Final FEEES", feesIntStudent)

                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal !="To be advised") {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            //feesDict.international_student = Number(feesNumber);
                                            feesDict.international_student={
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",                                               
                                                description: feesIntStudent,
                                               
                                            };

                                        }
                                    }
                                }else{
                                    feesDict.international_student={
                                        amount: 0,
                                        duration: 1,
                                        unit: "Year",                                      
                                        description: ""
                                      
                                    }
                                }
                            } else {
                              
                                if (postgradCourseList[postGradIndex].fees.includes("To be advised")) {
                                    feesDict.international_student.push({
                                        amount: 0,
                                        duration: 1,
                                        unit: "Year",                                      
                                        description: "To be advised"
                                       
                                    });
                                } else {
                                    feesDict.international_student={
                                        amount: 0,
                                        duration: 1,
                                        unit: "Year",                                      
                                        description: ""
                                      
                                    };
                                }

                            }
                            console.log("FADESADFS", feesDict.international_student) // if (feesIntStudent && feesIntStudent.length > 0)
                            let international_student_all_fees_array = [];
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                // const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                              //  feesDict.international_student_all_fees = [];
                                console.log("FFFFFF", futureText)

                                // if (currentText) {
                                //     feesDict.international_student_all_fees.push(currentText.trim());
                                // }
                                // if (futureText) {
                                //     console.log("INIF")
                                //     feesDict.international_student_all_fees.push(futureText.trim());
                                // } else {
                                //     feesDict.international_student_all_fees = international_student_all_fees_array
                                // }


                                // if (feesDuration && feesDuration.length > 0) {
                                //     feesDict.fee_duration_years = feesDuration;
                                // }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }

                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                }
                            }

                            break;
                        }

                        case 'course_campus_location': { // Location Launceston

                           let  course_cricos_code
                            if (postGradIndex > -1) {
                                course_cricos_code = postgradCourseList[postGradIndex].cricos;
                                course_cricos_code = course_cricos_code.split(":")[1].trim();
                                console.log("CRICOSE", course_cricos_code)
                            }
                            else {
                                course_cricos_code = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                                if (course_cricos_code.indexOf(":") > -1) {
                                    course_cricos_code = course_cricos_code.split(":")[1];
                                }
                                //console.log("course_cricos_code",course_cricos_code);
                            }
                            if (course_cricos_code) {
                                if (course_cricos_code == "NA" || course_cricos_code == "External" || course_cricos_code.includes("currently not available to international students") || course_cricos_code.includes("available to international offshore students only") || course_cricos_code.includes("External") || course_cricos_code.includes("This online course is available to international students to study externally (online) from outside Australia.")) {
                                    console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                    console.log(funcName + 'Invalid cricos code so break scrapping function and continue with next course....');
                                    console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                    throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
                                }                         
                                 
                           
                        }
                            if (postGradIndex > -1) {
                                if (postgradCourseList[postGradIndex].delivery.split(":")[1].indexOf("Brooklyn Park") > -1) {
                                    console.log("yessss")
                                    // resJsonData.course_study_mode = [];
                                    // resJsonData.course_study_mode.push("On Campus");
                                    resJsonData.course_campus_location = [
                                        {
                                            "name": "Brooklyn Park",
                                            "code":String(course_cricos_code)
                                        }]
                                    // resJsonData.course_campus_location.push("Brooklyn Park");
                                }
                                else {
                                    console.log("nooo")
                                    resJsonData.course_campus_location = [
                                        {
                                            "name": "Bedford Park",
                                            "code":String(course_cricos_code)
                                        }
                                    ];
                                    //  resJsonData.course_campus_location = ["Bedford Park"];
                                }
                                console.log("Campus_Location---->", resJsonData.course_campus_location)
                            }

                            else {
                                console.log("yessssss_inelse")
                                const campLocationText = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);

                                console.log(funcName + 'campLocationText = ' + campLocationText);
                                if (campLocationText && campLocationText.length > 0) {
                                    var campLocationValTrimmed = String(campLocationText).replace("– ", "").trim();
                                    console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                    if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {

                                        // resJsonData.course_campus_location = await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ''), ',');
                                        // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                        var cam = format_functions.titleCase(campLocationValTrimmed)
                                        resJsonData.course_campus_location = [
                                            {
                                                "name": cam,
                                               "code":String(course_cricos_code)
                                            }]
                                    }
                                }
                                else {
                                    resJsonData.course_campus_location = [
                                        {
                                            "name": "Bedford Park",
                                           "code":String(course_cricos_code)
                                        }]
                                    // resJsonData.course_campus_location = ["Bedford Park"];
                                } // if (campLocationText && campLocationText.length > 0)           
                            }
                            break;
                        }
                        case 'course_intake_url': {
                            const courseKeyVal = courseScrappedData.course_intake_url;
                            var url = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                url = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (url.length > 0) {
                                resJsonData.course_intake_url = url;
                            }
                            break;
                        }
                        case 'course_intake': {
                            var courseIntake = [];
                            if (postGradIndex > -1) {
                                let courseIntakeText = postgradCourseList[postGradIndex].intake.split(":")[1].split("\n");
                                console.log("courseIntakeText", courseIntakeText);
                                for (let date of courseIntakeText) {
                                    if (date && date.length > 0) {
                                        courseIntake.push(date.trim() + ",2020");
                                    }
                                }
                            }
                            else {
                                let courseIntakeText = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                                if (courseIntakeText.indexOf(":") > -1) {
                                    courseIntakeText = courseIntakeText.split(":")[1].split("\n");
                                } else {
                                    courseIntakeText = courseIntakeText.split("–");
                                    console.log("courseIntakeText" + JSON.stringify(courseIntakeText));
                                }
                                for (let date of courseIntakeText) {
                                    if (date && date.length > 0) {
                                        courseIntake.push(date.trim() + ",2020");
                                    }
                                }
                            }
                            var locationArray = resJsonData.course_campus_location;
                            console.log("locationArray", locationArray);
                            console.log("New Intake data@@@", courseIntake)
                            if (courseIntake && courseIntake.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let location of locationArray) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location.name,
                                        "value": courseIntake
                                    });
                                }

                                var newintake = resJsonData.course_intake.intake
                                console.log("New Intake data@@@", newintake)
                                let formatedIntake = await format_functions.providemyintake(newintake, "MOM", "");
                                console.log("NEw Intake Format@@@@@" + JSON.stringify(formatedIntake))
                                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                resJsonData.course_intake.intake = formatedIntake
                                resJsonData.course_intake.more_details = more_details;
                            }
                            break;
                        }
                        case 'course_study_mode': { // Location Launceston                            
                            resJsonData.course_study_mode = "On Campus";                            
                            break;
                        }
                        
                       
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                      
                       
                        case 'course_country':
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                if (postGradIndex > -1) {
                                                    concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                                }
                                                else {
                                                    concatnatedSelStr = String(concatnatedSelStr).concat('. ').concat(selItem).trim();
                                                }
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            console.log(funcName + 'matched case course_career_outcome: ' + key);
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            var course_career_outcome = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            } // rootElementDictList
                            // add only if it has valid value
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            } else {
                                resJsonData.course_career_outcome = []
                            }
                            break;
                        }
                      
                        case 'course_title': {
                            if (postGradIndex > -1) {

                                var title = postgradCourseList[postGradIndex].name;

                                //   var ctitle = format_functions.titleCase(title)
                                title = title.replace('(', '( ').replace('\n', '').replace('/', '/ ');
                                console.log("splitStr@@@2" + title);
                                var ctitle = format_functions.titleCase(title).trim();
                                var ctitle2 = ctitle.replace(' ( ', '(').replace('/ ', '/');
                                console.log("ctitle@@@", ctitle2.trim());
                                resJsonData.course_title = ctitle2
                            }
                            else {
                                var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                                // var ctitle = format_functions.titleCase(title)
                                // resJsonData.course_title = ctitle
                                title = title.replace('(', '( ').replace('\n', '').replace('/', '/ ');
                                console.log("splitStr@@@2" + title);
                                var ctitle = format_functions.titleCase(title).trim();
                                var ctitle2 = ctitle.replace(' ( ', '(').replace('/ ', '/');
                                console.log("ctitle@@@", ctitle2.trim());
                                resJsonData.course_title = ctitle2
                            }


                            break;
                        }
                        case 'course_study_level': {
                            //CUSTOMIZED DON'T COPY
                            //const cTitle = resJsonData.course_title;
                            let cStudyLevel = study_level;
                            console.log(funcName + 'course_title course_study_level = ' + cStudyLevel);

                            if (cStudyLevel) {
                                resJsonData.course_study_level = cStudyLevel;

                            }
                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                            const courseKeyVal_major = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline);
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                            let course_outlines = {};
                            if (courseKeyVal_minor != null) {
                                course_outlines.minors = courseKeyVal_minor
                            } else {
                                course_outlines.minors = []
                            }
                            if (courseKeyVal_major != null) {
                                course_outlines.majors = courseKeyVal_major
                            } else {
                                course_outlines.majors = []
                            }
                            if (courseKeyVal != null) {
                                course_outlines.more_details = courseKeyVal
                            } else {
                                course_outlines.more_details = ""
                            }
                            resJsonData.course_outline = course_outlines
                            break;
                        }
                        case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            if (courseKeyVal != null) {
                                resJsonData.application_fee = courseKeyVal
                            } else {
                                resJsonData.application_fee = ""
                            }
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)            
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                // NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;             
                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;


        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }
    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }
            console.log("courseDict.innerText",courseDict.innerText)
            //const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText);
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category, courseDict.studylevel, s.page.url());
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };