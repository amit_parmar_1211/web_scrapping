**Charles Darwin University: Total Courses Details As On 2 April 2019**
* Total Courses = 108
* Courses without CRICOS code = 
* Total available courses = 
* Repeated = 

**Notes**
* cricos code is fetched from the courselist file by matching course title for vet courses.For other courses cricos is fetched with location.As cricos is different for different locations.
* Code for cricos code is written in case of location.
* in case of course outline for vet courses study_units and for all others subject_Areas.
* Changed code for scrape_list_diff in format function
* Chnaged code for scrape_outline_diff in format function
* Fees, duration, intake and program code fetched with course list using the scrape_list_diff function.
* Custom ielts mapping.
* additional subject list generated for the university for course outline.
* Commented scrapeSelectorFile has any valid cricos code, as cricos code is fetched from courselist.
* Intake is given in semesters so semester is matched with month in hardcode file.

**2019 & conditions**
* ScrapeCourse.js for intake 2019 is statically added.
* Link of academic calendar is specifically for 2019.