const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
// const mapping = require('./output/Mapping/main');
request = require('request');
class ScrapeCourse extends Course {
  static titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      // You do not need to check if i is larger than splitStr length, as your for does that for you
      // Assign it back to the array
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
  }

  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, course_name, category_name) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'program_code': {
              var program_code = [];

              const courseKeyVal = courseScrappedData.program_code;
              console.log("courseScrappedData.program_code = ", courseScrappedData.program_code);
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        program_code.push(selList[0]);
                       
                      }
                    }

                  }
                } 
              }
             
              if (program_code && program_code.length > 0) {
                for (let program_val of program_code) {

                  resJsonData.program_code = program_val;

                }
              }
              else {
                resJsonData.program_code = "";
              }

              break;
            }
            case 'course_discipline': {
              // As of not we do not have way to get this field value
              resJsonData.course_discipline = category_name;
              break;
            }
            case 'course_toefl_ielts_score': {
              //CUSTOMIZED MAPPING CODE DONT COPY            
              const courseAdminReq = {};
              const englishList = [];
              const ieltsDict = {};
              const pteDict = {};
              const tofelDict = {};
              const caeDict = {};
              let ieltsNumber = null;

              // english requirement              
              const study_level = resJsonData.course_study_level;
              var title = resJsonData.course_title;
              console.log("R ielts study_level", study_level, "R ielts title", title);

              if (study_level && study_level.length > 0) {
                const ieltsMappingDict = JSON.parse(fs.readFileSync(appConfigs.ieltsMappingPath));
                //console.log("R ieltsMappingDict", ieltsMappingDict);
                console.log("dgfgfgf", ieltsMappingDict.postgraduate.ibt)


                switch (study_level) {

                  case "UNDERGRADUATE": {
                    if (title.toString().toLowerCase().indexOf("nursing") > -1) {
                      console.log("Nursing!!");
                      ieltsDict.name = 'ielts academic';
                      ieltsDict.description = ieltsMappingDict.nursing.ielts;
                      englishList.push(ieltsDict);

                      pteDict.name = 'pte academic';
                      pteDict.description = ieltsMappingDict.nursing.pte;
                      englishList.push(pteDict);

                      tofelDict.name = 'toefl ibt';
                      tofelDict.description = ieltsMappingDict.nursing.ibt;
                      englishList.push(tofelDict);

                      caeDict.name = 'cae';
                      caeDict.description = ieltsMappingDict.nursing.cae;
                      englishList.push(caeDict);
                      courseAdminReq.english = englishList;
                    }
                    else if (title.toString().toLowerCase().indexOf("midwifery") > -1) {
                      console.log("Midwifery!!");
                      ieltsDict.name = 'ielts academic';
                      ieltsDict.description = ieltsMappingDict.midwifery.ielts;
                      englishList.push(ieltsDict);

                      pteDict.name = 'pte academic';
                      pteDict.description = ieltsMappingDict.midwifery.pte;
                      englishList.push(pteDict);

                      tofelDict.name = 'toefl ibt';
                      tofelDict.description = ieltsMappingDict.midwifery.ibt;
                      englishList.push(tofelDict);

                      caeDict.name = 'cae';
                      caeDict.description = ieltsMappingDict.midwifery.cae;
                      englishList.push(caeDict);
                      courseAdminReq.english = englishList;
                    }
                    else if (title.toString().toLowerCase().indexOf("social work") > -1) {
                      console.log("Social Work!!");
                      ieltsDict.name = 'ielts academic';
                      ieltsDict.description = ieltsMappingDict.social_work.ielts;
                      englishList.push(ieltsDict);

                      pteDict.name = 'pte academic';
                      pteDict.description = ieltsMappingDict.social_work.pte;
                      englishList.push(pteDict);

                      tofelDict.name = 'toefl ibt';
                      tofelDict.description = ieltsMappingDict.social_work.ibt;
                      englishList.push(tofelDict);

                      caeDict.name = 'cae';
                      caeDict.description = ieltsMappingDict.social_work.cae;
                      englishList.push(caeDict);
                      courseAdminReq.english = englishList;
                    }
                    else if (title.toString().toLowerCase().indexOf("pharmacy") > -1) {
                      console.log("Pharmacy!!");
                      ieltsDict.name = 'ielts academic';
                      ieltsDict.description = ieltsMappingDict.pharmacy.ielts;
                      englishList.push(ieltsDict);

                      pteDict.name = 'pte academic';
                      pteDict.description = ieltsMappingDict.pharmacy.pte;
                      englishList.push(pteDict);

                      tofelDict.name = 'toefl ibt';
                      tofelDict.description = ieltsMappingDict.pharmacy.ibt;
                      englishList.push(tofelDict);

                      caeDict.name = 'cae';
                      caeDict.description = ieltsMappingDict.pharmacy.cae;
                      englishList.push(caeDict);
                      courseAdminReq.english = englishList;
                    }
                    else if (title.toString().toLowerCase().indexOf("education (graduate entry)") > -1) {
                      console.log("Education (graduate entry)!!");
                      ieltsDict.name = 'ielts academic';
                      ieltsDict.description = ieltsMappingDict.education_graduation_entry.ielts;
                      englishList.push(ieltsDict);

                      pteDict.name = 'pte academic';
                      pteDict.description = ieltsMappingDict.education_graduation_entry.pte;
                      englishList.push(pteDict);

                      tofelDict.name = 'toefl ibt';
                      tofelDict.description = ieltsMappingDict.education_graduation_entry.ibt;
                      englishList.push(tofelDict);

                      caeDict.name = 'cae';
                      caeDict.description = ieltsMappingDict.education_graduation_entry.cae;
                      englishList.push(caeDict);
                      courseAdminReq.english = englishList;
                    }
                    else {
                      ieltsDict.name = 'ielts academic';
                      ieltsDict.description = ieltsMappingDict.undergraduate.ielts;
                      englishList.push(ieltsDict);

                      pteDict.name = 'pte academic';
                      pteDict.description = ieltsMappingDict.undergraduate.pte;
                      englishList.push(pteDict);

                      tofelDict.name = 'toefl ibt';
                      tofelDict.description = ieltsMappingDict.undergraduate.ibt;
                      englishList.push(tofelDict);

                      caeDict.name = 'cae';
                      caeDict.description = ieltsMappingDict.undergraduate.cae;
                      englishList.push(caeDict);
                      courseAdminReq.english = englishList;
                    }
                    break;
                  }
                  case "RESEARCH":
                  case "POSTGRADUATE RESEARCH": {
                    ieltsDict.name = 'ielts academic';
                    ieltsDict.description = ieltsMappingDict.postgraduate.ielts;
                    englishList.push(ieltsDict);

                    pteDict.name = 'pte academic';
                    pteDict.description = ieltsMappingDict.postgraduate.pte;
                    englishList.push(pteDict);

                    tofelDict.name = 'toefl ibt';
                    tofelDict.description = ieltsMappingDict.postgraduate.ibt;
                    englishList.push(tofelDict);

                    caeDict.name = 'cae';
                    caeDict.description = ieltsMappingDict.postgraduate.cae;
                    englishList.push(caeDict);
                    courseAdminReq.english = englishList;
                    break;
                  }
                  case "POSTGRADUATE": {
                    console.log("INside")

                    ieltsDict.name = 'ielts academic';
                    ieltsDict.description = ieltsMappingDict.postgraduate.ielts;
                    englishList.push(ieltsDict);

                    pteDict.name = 'pte academic';
                    pteDict.description = ieltsMappingDict.postgraduate.pte;
                    englishList.push(pteDict);

                    tofelDict.name = 'toefl ibt';
                    tofelDict.description = ieltsMappingDict.postgraduate.ibt;
                    englishList.push(tofelDict);

                    caeDict.name = 'cae';
                    caeDict.description = ieltsMappingDict.postgraduate.cae;
                    englishList.push(caeDict);
                    courseAdminReq.english = englishList;

                    break;
                  }
                  case "VOCATIONAL EDUCATION & TRAINING": {
                    ieltsDict.name = 'ielts academic';
                    ieltsDict.description = ieltsMappingDict.vet.ielts;
                    englishList.push(ieltsDict);

                    pteDict.name = 'pte academic';
                    pteDict.description = ieltsMappingDict.vet.pte;
                    englishList.push(pteDict);

                    tofelDict.name = 'toefl ibt';
                    tofelDict.description = ieltsMappingDict.vet.ibt;
                    englishList.push(tofelDict);

                    caeDict.name = 'cae';
                    caeDict.description = ieltsMappingDict.vet.cae;
                    englishList.push(caeDict);
                    courseAdminReq.english = englishList;
                    break;
                  }
                }
              }//end switch
              console.log("EnglishList---->", englishList)
              const regEx = /[+-]?\d+(\.\d+)?/g;
              const matchedStrList = String(englishList[0].description).match(regEx);
              console.log('course_toefl_ielts_score matchedStrList = ' + JSON.stringify(matchedStrList));
              if (matchedStrList && matchedStrList.length > 0) {
                ieltsNumber = Number(matchedStrList[0]);
                console.log('course_toefl_ielts_score ieltsNumber = ' + ieltsNumber);
              }
              // academic requirement
              if (courseScrappedData.course_academic_requirement) {
                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                console.log('\n\r');
                console.log(funcName + 'academicReq = ' + academicReq);
                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                console.log('\n\r');
                if (academicReq && String(academicReq).length > 0) {
                  courseAdminReq.academic = academicReq.toString().replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ' ');
                }
              }

              //DYNAMIC PROGRESS BAR //CAN BE COPIED//PBT NOT INCLUDED
              //progress bar start
              let penglishList = [];
              let pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
              // extract exact number from string              
              let matchedStrList_ielts, matchedStrList_pte, matchedStrList_tofel, matchedStrList_cae;
              let ieltsNo, pteNo, ibtNo, caeNo;
              matchedStrList_ielts = String(ieltsDict.description).match(regEx);
              matchedStrList_pte = String(pteDict.description).match(regEx);
              matchedStrList_tofel = String(tofelDict.description).match(regEx);
              matchedStrList_cae = String(caeDict.description).match(regEx);
              if (matchedStrList_ielts && matchedStrList_ielts.length > 0) {
                ieltsNo = Number(matchedStrList_ielts[0]);
              }
              if (matchedStrList_pte && matchedStrList_pte.length > 0) {
                pteNo = Number(matchedStrList_pte[0]);
              }
              if (matchedStrList_tofel && matchedStrList_tofel.length > 0) {
                ibtNo = Number(matchedStrList_tofel[0]);
              }
              if (matchedStrList_cae && matchedStrList_cae.length > 0) {
                caeNo = Number(matchedStrList_cae[0]);
              }
              if (ieltsNumber && ieltsNumber > 0) {
               
                if (ieltsNo) {
                  pieltsDict.name = 'ielts academic';
                  pieltsDict.description = ieltsDict.description;
                  pieltsDict.min = 0;
                  pieltsDict.require = ieltsNo;
                  pieltsDict.max = 9;
                  pieltsDict.R = 0;
                  pieltsDict.W = 0;
                  pieltsDict.S = 0;
                  pieltsDict.L = 0;
                  pieltsDict.O = 0;
                  penglishList.push(pieltsDict);
                }
                if (ibtNo) {
                  pibtDict.name = 'toefl ibt';
                  pibtDict.description = tofelDict.description;
                  pibtDict.min = 0;
                  pibtDict.require = ibtNo;
                  pibtDict.max = 120;
                  pibtDict.R = 0;
                  pibtDict.W = 0;
                  pibtDict.S = 0;
                  pibtDict.L = 0;
                  pibtDict.O = 0;
                  penglishList.push(pibtDict);
                }


                if (pteNo) {
                  ppteDict.name = 'pte academic';
                  ppteDict.description = pteDict.description;
                  ppteDict.min = 0;
                  ppteDict.require = pteNo;
                  ppteDict.max = 90;
                  ppteDict.R = 0;
                  ppteDict.W = 0;
                  ppteDict.S = 0;
                  ppteDict.L = 0;
                  ppteDict.O = 0;
                  penglishList.push(ppteDict);
                }

                
                if (caeNo) {
                  pcaeDict.name = 'cae';
                  pcaeDict.description = caeDict.description;
                  pcaeDict.min = 80;
                  pcaeDict.require = caeNo;
                  pcaeDict.max = 230;
                  pcaeDict.R = 0;
                  pcaeDict.W = 0;
                  pcaeDict.S = 0;
                  pcaeDict.L = 0;
                  pcaeDict.O = 0;
                  penglishList.push(pcaeDict);
                }

              }
              // progrssbar End
              //console.log("penglishList",penglishList);
              resJsonData.course_admission_requirement = {};
              if (courseAdminReq.english && courseAdminReq.english.length > 0) {
                resJsonData.course_admission_requirement.english = courseAdminReq.english;
              } else {

                resJsonData.course_admission_requirement.english = [];
              }
              if (courseAdminReq.academic && courseAdminReq.academic.length > 0) {
                resJsonData.course_admission_requirement.academic = [courseAdminReq.academic];
              } else {
                resJsonData.course_admission_requirement.academic = [];
              }
              // add english requirement 'english_more_details' link
              const entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
              const academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
              const english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
              if (entry_requirements_url && entry_requirements_url.length > 0) {
                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
              }
              if (academic_requirements_url && academic_requirements_url.length > 0) {
                resJsonData.course_admission_requirement.academic_requirements_url = "";
              }
              if (english_requirements_url && english_requirements_url.length > 0) {
                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
              }
              if (penglishList && penglishList.length > 0) {
                courseAdminReq.english = penglishList;
                resJsonData.course_admission_requirement.english = courseAdminReq.english;
              } else {
              //  throw new Error('IELTS not found');
                resJsonData.course_admission_requirement.english = [];
              }

              break;
            }
            case 'course_url': {
              if (courseUrl && courseUrl.length > 0) {
                resJsonData.course_url = courseUrl;
              }
              break;
            }
            case 'course_outline': {
              
              let mgcrs = null;
            
           const majorcrs = {
             majors:[],
             minors:[],
             more_details:""
           };
           //for major cousrseList
             var coursemajor =  courseScrappedData.course_outline;
             if (Array.isArray(coursemajor)) {
               for (const rootEleDict of coursemajor) {
                   console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                   const elementsList = rootEleDict.elements;
                   for (const eleDict of elementsList) {
                       console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                       const selectorsList = eleDict.selectors;
                       for (const selList of selectorsList) {
                           console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                           if (Array.isArray(selList) && selList.length > 0) {
                             mgcrs = selList;
                           }
                       }
                   }
               }
           }
             if(mgcrs && mgcrs.length >0){
               majorcrs.majors=mgcrs
             }
             else{
               majorcrs.majors= majorcrs.majors
             }
                resJsonData.course_outline =  majorcrs;
             //for minor courseList
                var courseminor =  courseScrappedData.course_outline_minor;
                let mincrs = null;
                if (Array.isArray(courseminor)) {
                  for (const rootEleDict of courseminor) {
                      console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                      const elementsList = rootEleDict.elements;
                      for (const eleDict of elementsList) {
                          console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                          const selectorsList = eleDict.selectors;
                          for (const selList of selectorsList) {
                              console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                              if (Array.isArray(selList) && selList.length > 0) {
                               mincrs = selList;
                              }
                          }
                      }
                  }
              }
                if(mincrs && mincrs.length >0){
                  majorcrs.minors=mincrs
                }
                else{
                 majorcrs.minors=majorcrs.minors
               }
                   resJsonData.course_outline =  majorcrs;

                console.log("major==>", resJsonData.course_outline)
          
            break;
            }
            case 'course_duration_full_time': {
              //FULL TIME AND PART TIME IS DONE IN THIS
              //CUSTOMIZED CODE DONOT COPY
              const courseDurationList = [];
              const courseDurationDisplayList = [];
              const durationFullTime = {};

              // let anotherArray = [];
              let duration1 = courseScrappedData.course_duration_full_time;

              console.log("*************start formating Full Time years*************************");
              console.log("courseScrappedData.course_duration_full_time = ", JSON.stringify(courseScrappedData.course_duration_full_time));
              let finalDuration = ""
              if (duration1[0].elements[0].selectors[0][0]) {
                finalDuration = duration1[0].elements[0].selectors[0][0];
              } else {
                finalDuration = duration1[0].elements[1].selectors[0][0];
                duration1[0].elements[0].selectors[0][0] = duration1[0].elements[1].selectors[0][0];
              }
              if (courseDurationList != undefined) {
                finalDuration = finalDuration.replace(/\n\t|\n+/g, ' ').replace(/[\/]+/g, '');
                resJsonData.course_duration = finalDuration;
              }
              else{
                resJsonData.course_duration = finalDuration;
              }

              console.log("finalDuration -->", finalDuration);
              if (finalDuration != undefined) {

                finalDuration = finalDuration.replace(/\n\t|\n+/g, ' ').replace(/[\/]+/g, '');
                console.log("finalDuration -->", finalDuration);

               
                if (finalDuration.toLowerCase().indexOf('full') == -1) {
                  finalDuration += " full time";
                }

                const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration);
                console.log("full_year_formated_data", full_year_formated_data);
                ///course duration
                const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                console.log("not_full_year_formated_data", not_full_year_formated_data);
                try {
                  durationFullTime.duration_full_time = not_full_year_formated_data.replace(/\n\t|\n+/g, ' ').trim();
                  courseDurationList.push(durationFullTime);
                  ///END course duration
                  console.log("courseDurationList : ", courseDurationList);
                  ///course duration display

                  courseDurationDisplayList.push(full_year_formated_data);
                  // anotherArray.push(courseDurationDisplayList);
                  console.log("courseDurationDisplayList", courseDurationDisplayList);
                  ///END course duration display
                } catch (err) {
                  console.log("Problem in Full Time years:", err);
                }
                console.log("courseDurationDisplayList1", courseDurationDisplayList);
                console.log('***************END formating Full Time years**************************')
                let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
               
                if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                  resJsonData.course_duration_display = filtered_duration_formated;
                  var isfulltime = false, isparttime = false;
                  filtered_duration_formated.forEach(element => {
                    if (element.display == "Full-Time") {
                      isfulltime = true;
                    }
                    if (element.display == "Part-Time") {
                      isparttime = true;
                    }
                  });
                  resJsonData.isfulltime = isfulltime;
                  resJsonData.isparttime = isparttime;


                }
              } else {

                throw new Error("Duration not available");
              }

              break;
            }
            case 'course_tuition_fee':
            //case 'course_study_mode':
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_duration_part_time': {
              console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
              // No need to process anything, its already processed in other case.
              break;
            }
            case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};
              // courseTuitionFee.year = configs.propValueNotAvaialble;

              const feesList = [];
              const feesDict = {
                
              };
            

              let feesIntStudent = await Course.extractValueFromScrappedElement
                (courseScrappedData.course_tuition_fees_international_student);
              let feesIntStudent_desc = feesIntStudent;
              const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
             
              console.log("feesIntStudentBefore -->", feesIntStudent);

              if (feesIntStudent && feesIntStudent.indexOf('2020') > -1) {
                let splitFees = feesIntStudent.split('2020');
                feesIntStudent = splitFees[1];
                console.log("feesIntStudentAftefeesIntStudent2r -->", feesIntStudent);

              } else if (feesIntStudent && feesIntStudent.length) {
                let splitFees = feesIntStudent;
                feesIntStudent = splitFees;
                console.log("feesIntStudentAfte@@@@2r -->", feesIntStudent);
              } else {
                throw new Error("Fees new case");
              }
              console.log("feesIntStudentAfter -->", feesIntStudent);
              // if we can extract value as Int successfully then replace Or keep as it is
              if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                console.log(funcName + 'feesVal = ' + feesVal);
                if (feesVal) {
                  // const regEx = /\d/g;
                  const regEx = /\d+\.?\d*/g; // /[0-9]/g;
                  let feesValNum = feesVal.match(regEx);
                  if (feesValNum) {
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    feesValNum = feesValNum.join('');
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    let feesNumber = null;
                    if (feesValNum.includes(',')) {
                      feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                    } else {
                      feesNumber = feesValNum;
                    }
                    console.log(funcName + 'feesNumber = ' + feesNumber);
                    if (Number(feesNumber)) {
                      feesDict.international_student = {
                        amount: Number(feesNumber),
                        duration: 1,
                        unit: "Year",
                        description: feesIntStudent_desc
                        
                      }
                    }
                    if (feesNumber == "0") {
                      console.log("FEesNumber = 0");
                      feesDict.international_student={
                        amount: 0,
                        duration: 1,
                        unit: "Year",
                        description: feesIntStudent_desc
                       
                      }
                    }
                  }
                }
              } // if (feesIntStudent && feesIntStudent.length > 0)
              else {
                feesDict.international_student={
                  amount: 0,
                  duration: 1,
                  unit: "Year",
                  description: ""
                  
                }
              }

              // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
              const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
              console.log(funcName + 'cricoseStr = ' + cricoseStr);
              console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
              if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                if (fixedFeesDict) {
                  for (const codeKey in fixedFeesDict) {
                    console.log(funcName + 'codeKey = ' + codeKey);
                    if (fixedFeesDict.hasOwnProperty(codeKey)) {
                      const keyVal = fixedFeesDict[codeKey];
                      console.log(funcName + 'keyVal = ' + keyVal);
                      if (cricoseStr.includes(codeKey)) {
                        const feesDictVal = fixedFeesDict[cricoseStr];
                        console.log(funcName + 'feesDictVal = ' + feesDictVal);
                        if (feesDictVal && feesDictVal.length > 0) {
                          const inetStudentFees = Number(feesDictVal);
                          console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                          if (Number(inetStudentFees)) {
                            feesDict.international_student.push({
                              amount: Number(inetStudentFees),
                              duration: 1,
                              unit: "Year",
                              description: ""
                             
                            });
                          }
                        }
                      }
                    }
                  } // for
                } // if
              } // if

              if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }
                console.log("(feesIntStudent.length > 0) -->", feesIntStudent);
                if (feesDict.international_student.length > 0) { // extract only digits
                  console.log("international_student -->" + feesIntStudent)
                  const courseKeyVal = courseScrappedData.course_tuition_fees_international_student_more_details;
                }

                if (feesDict) {
                  // feesList.push(feesDict);
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                // take tuition fee value at json top level so will help in DDB for indexing
                if (courseTuitionFee && feesDict.international_student) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
                // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                if (!feesDict.international_student) {
                  console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                  console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                  console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                  console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                  return null; // this will add this item into FailedItemList and writes file to local disk
                }
              } else {

              }
              break;
            }
            case 'course_study_mode': { // Location Launceston

              resJsonData.course_study_mode = "On campus";

              break;
            }

            case 'course_intake':
              {
                let courseIntakeStr = [];
                let tempIntakeArray = [];
                const courseKeyVal = courseScrappedData.course_intake;
                console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                if (Array.isArray(courseKeyVal)) {
                  for (const rootEleDict of courseKeyVal) {
                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                      console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                      const selectorsList = eleDict.selectors;
                      for (const selList of selectorsList) {
                        console.log(funcName + '\n\r selList Intake= ' + JSON.stringify(selList));
                        
                        for (let selItem of selList) {
                          selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, '')
                          console.log("sellitem -->", selItem);;
                          if (selItem.toLowerCase().indexOf('semester 1') > -1) {
                            tempIntakeArray.push('2 March 2020');
                          } else if (selItem.toLowerCase().indexOf('semester 2') > -1) {
                            tempIntakeArray.push('20 July 2020');
                          } else if (selItem.toLowerCase().indexOf('semester 3') > -1) {
                            tempIntakeArray.push('31 August 2020');
                          } else if (selItem.toLowerCase().indexOf('summer') > -1) {
                            tempIntakeArray.push('9 November 2020');
                          }
                        }

                        console.log("tempIntakeArray -->", tempIntakeArray);
                        if (tempIntakeArray && tempIntakeArray.length > 0) {
                          for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                            courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": tempIntakeArray });
                          }
                        } else {
                          for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                          courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": tempIntakeArray });
                          }
                          }
                        }

                      }
                    }
                  }
                
                var intakeUrl = [];
                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                let formatIntake = await format_functions.providemyintake(courseIntakeStr, "MOM", "");
                if (formatIntake && formatIntake.length > 0) {
                  var intakedata = {};
                  intakedata.intake = formatIntake;
                  intakedata.more_details = more_details;
                  resJsonData.course_intake = intakedata;
                  console.log("resJsonData.course_intake -->> ", resJsonData.course_intake);
                }
                break;
              }
              case 'application_fee': {
                const courseKeyVal = courseScrappedData.application_fee;
       
                let applicationfee = null;
                  if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
               console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
               const elementsList = rootEleDict.elements;
               for (const eleDict of elementsList) {
               console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
               for (const selList of selectorsList) {
                   console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                if (Array.isArray(selList) && selList.length > 0) {
                  applicationfee = selList[0];
                   }
                   else{
                      applicationfee = selList[0];
                   }
                }
               }
               }
           }
          if (applicationfee && applicationfee.length>0) {
            resJsonData.application_fee = applicationfee;
           console.log("appfee"+ JSON.stringify(resJsonData.application_fee));
           }
           else{
              resJsonData.application_fee = applicationfee;
           }
       
            break;
       
      
          }
          
            case 'univ_name': {
              const courseKeyVal = courseScrappedData.univ_name;
              let resUnivName = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivName = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivName) {
                resJsonData.univ_name = resUnivName;
              }
              break;
            }
            case 'univ_logo': {
              const courseKeyVal = courseScrappedData.univ_logo;
              let resUnivLogo = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivLogo = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivLogo) {
                resJsonData.univ_logo = resUnivLogo;
              }
              break;
            }
           
            case 'course_country':
            case 'course_overview': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              let contentFlag = false;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    contentFlag = true;
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
              // add only if it has valid value
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0 && contentFlag == false) {
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
              } else {
                resJsonData[key] = "";
              }
              break;
            }
            case 'course_career_outcome': {
              let assigned = false;
              console.log('course_career_outcome matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log('course_career_outcome key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedSelStr = [];
              for (const rootEleDict of rootElementDictList) {
                console.log('course_career_outcome rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                // let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log('course_career_outcome eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  for (const selList of selectorsList) {
                    if (assigned == false) {
                      if (selList && selList.length > 0) {
                        console.log('course_career_outcome selList = ' + JSON.stringify(selList));

                        for (const selItem of selList) {
                          concatnatedSelStr.push(selItem);
                        }
                        assigned = true;
                      }
                      console.log('course_career_outcome concatnatedSelStr = ' + concatnatedSelStr);
                      // if (concatnatedSelStr) {
                      //   concatnatedSelectorsStr = concatnatedSelStr;
                      // }
                    }
                  } // selectorsList                  
                } // elementsList                
              } // rootElementDictList 

              // add only if it has valid value              
              if (concatnatedSelStr && concatnatedSelStr.length > 0) {
                resJsonData.course_career_outcome = concatnatedSelStr;
              } else {
                resJsonData.course_career_outcome = [];
              }

              break;
            }
            case 'course_campus_location': { // Location Launceston 
              let newcricos = null;
              const courseKeyValcd = courseScrappedData.course_cricos_code;
              let course_cricos_code = [];
              if (Array.isArray(courseKeyValcd)) {
                for (const rootEleDict of courseKeyValcd) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (let selList of selectorsList) {
                      console.log(funcName + '\n\r selList cricoscode= ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {

                        if (selList[0] && selList[0].toLowerCase().indexOf('cricos course no.') > -1) {
                          newcricos = selList[0].split('CRICOS Course No.')[1];
                          console.log("newCricos -->", newcricos)

                          if (newcricos.indexOf(')') > -1) {
                            newcricos = newcricos.split(')')[0];
                          }
                          newcricos = newcricos.replace(/[\/]|\(|\)|\.+/g, '').trim();
                          console.log("newCricos After -->", newcricos);
                          if (newcricos.toLowerCase().indexOf('refer') > -1) {
                            let splitAdditionalInfo = newcricos.split('Refer')[0];
                            splitAdditionalInfo = splitAdditionalInfo.replace(/[\/]|\(|\)|\.+/g, '').trim();
                            newcricos = splitAdditionalInfo;
                          }
                          course_cricos_code.push(newcricos);
                        } else {

                          if (newcricos) {

                          } else {
                            if (selList[0].length > 7) {
                              console.log("selList[0].match 7 ", selList[0].match(/.{1,7}/g));
                              selList = selList[0].match(/.{1,7}/g)
                            }
                            let checkDuplicate = [];
                            for (let cricos of selList) {
                              if (!checkDuplicate.includes(cricos)) {
                                checkDuplicate.push(cricos);
                              }
                            }
                            course_cricos_code = checkDuplicate;
                          }

                        }
                      }
                    }
                  }
                }
              }





              let concatnatedSelStr = [];
              console.log('course_campus_location matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedSelectorsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                // let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  for (let selList of selectorsList) {
                    if (selList && selList.length > 0) {
                      console.log('course_campus_location selList = ' + JSON.stringify(selList));


                      for (let sel of selList) {
                        sel = sel.trim();
                        console.log("selCampus --> ", sel);
                        if (sel.indexOf(',') > -1) {
                          let newSelList = [];
                          newSelList = sel.split(',');
                          for (let newSel of newSelList) {
                            if (newSel.toLowerCase().indexOf('tennant') > -1) {
                              concatnatedSelStr.push('Tennant Creek Centre');
                            } else if (newSel.toLowerCase().indexOf('palmerston') > -1) {
                              concatnatedSelStr.push('Palmerston');
                            } else if (newSel.toLowerCase().indexOf('jabiru') > -1) {
                              concatnatedSelStr.push('Jabiru Centre');
                            } else if (newSel.toLowerCase().indexOf('sydney') > -1) {
                              console.log("Inside else if (sel.indexOf('sydney') > -1)");
                              concatnatedSelStr.push('CDU Sydney');
                            } else if (newSel.toLowerCase().indexOf('katherine') > -1) {
                              concatnatedSelStr.push('Katherine');
                            } else if (newSel.toLowerCase().indexOf('waterfront') > -1) {
                              concatnatedSelStr.push('CDU Darwin Waterfront');
                            } else if (newSel.toLowerCase().indexOf('casuarina') > -1) {
                              concatnatedSelStr.push('Casuarina');
                            } else if (newSel.toLowerCase().indexOf('alice') > -1) {
                              concatnatedSelStr.push('Alice Springs');
                            } else if (newSel.toLowerCase().indexOf('melbourne') > -1) {
                              concatnatedSelStr.push('CDU Melbourne');
                            } else if (newSel.toLowerCase().indexOf('yulara') > -1) {
                              concatnatedSelStr.push('Yulara centre');
                            } else if (newSel.toLowerCase().indexOf('batchelor') > -1) {
                              concatnatedSelStr.push('Batchelor');
                            }
                          }

                        } else {
                          if (sel.toLowerCase().indexOf('tennant') > -1) {
                            concatnatedSelStr.push('Tennant Creek centre');
                          } else if (sel.toLowerCase().indexOf('palmerston') > -1) {
                            concatnatedSelStr.push('Palmerston');
                          } else if (sel.toLowerCase().indexOf('jabiru') > -1) {
                            concatnatedSelStr.push('Jabiru');
                          } else if (sel.toLowerCase().indexOf('sydney') > -1) {
                            console.log("Inside else if (sel.indexOf('sydney') > -1)");
                            concatnatedSelStr.push('CDU Sydney');
                          } else if (sel.toLowerCase().indexOf('katherine') > -1) {
                            concatnatedSelStr.push('Katherine');
                          } else if (sel.toLowerCase().indexOf('waterfront') > -1) {
                            concatnatedSelStr.push('CDU Darwin Waterfront');
                          } else if (sel.toLowerCase().indexOf('casuarina') > -1) {
                            concatnatedSelStr.push('Casuarina');
                          } else if (sel.toLowerCase().indexOf('alice') > -1) {
                            concatnatedSelStr.push('Alice Springs');
                          } else if (sel.toLowerCase().indexOf('melbourne') > -1) {
                            concatnatedSelStr.push('CDU Melbourne');
                          } else if (sel.toLowerCase().indexOf('yulara') > -1) {
                            concatnatedSelStr.push('Yulara centre');
                          } else if (sel.toLowerCase().indexOf('batchelor') > -1) {
                            concatnatedSelStr.push('Batchelor');
                          }
                        }

                      }

                      //} // selList
                      console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);
                      if (concatnatedSelStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        throw new Error("Campus not found");
                      }
                    }
                  } // selectorsList 
                } // elementsList 
              } // rootElementDictList 

              

              resJsonData.course_campus_location = concatnatedSelectorsStr;
              // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
              if (concatnatedSelectorsStr && concatnatedSelectorsStr.length > 0) {
                var campusedata = [];
                concatnatedSelectorsStr.forEach(element => {
                  campusedata.push({
                    "name": element,
                    "code": String(course_cricos_code)
                  })
                });
                resJsonData.course_campus_location = campusedata;
                console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

              }
              else {
                throw new Error("No campus location found.");
              }
             
              break;

            }


           
            case 'course_title': {
              let concatnatedElementsStr = null;
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr course title = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr.replace("\n", " ");
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).replace("\n", " ").concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr course title = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr course title = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr course title = ' + concatnatedRootElementsStr);
              // add only if it has valid value
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                concatnatedRootElementsStr = this.titleCase(concatnatedRootElementsStr);
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
              }
              break;
            }
            case 'course_study_level': {
              const cStudyLevel = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
              var split = cStudyLevel.replace('COURSEWORK', '');
              if (split) {
                console.log("cStudyLevel@@@", split);
                resJsonData.course_study_level = split.trim();
              } else {
                throw new Error("Study Level not Found");
              }

              break;
            }
           
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      ////start genrating new file location wise

      var NEWJSONSTRUCT = {}, structDict = [];
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
        console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        }
        else {
          NEWJSONSTRUCT.course_intake = "";
        }
        
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        resJsonData.course_id = location_wise_data.course_location_id;
        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
        

      }
    
      return resJsonData;
      ///end grnrating new file location wise
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = 'scrapeAndFormatCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);
      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }
      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file

      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }

      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
