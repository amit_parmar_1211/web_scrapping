const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');

const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
      const categoryselectorUrl = "//*[@id='block-content']/article/div/div/div/div/div/div/ul/li/a";
      var elementstring = "", elementhref = "", allcategory = [];
      const targetLinksCardsUrls = await page.$x(categoryselectorUrl);
      var targetLinksCardsTotal = [];
      var mainCategory = [];
      for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        elementstring = await page.evaluate(el => el.innerText, targetLinksCardsUrls[i]);
        elementhref = await page.evaluate(el => el.href, targetLinksCardsUrls[i]);
        targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
        mainCategory.push(elementstring);
      }
      await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(mainCategory));
      console.log(funcName + 'writing categories to file completed successfully....');
      console.log("#### main courses---" + targetLinksCardsTotal.length);


      let linkselector = "//*[@id='tab-1']/div[2]/div[2]/div[1]/div/div[@class='course-list__course-name']/following::div[@data-student-type='international' and not(contains(text(),'Not available to international students'))]//../..//a";
      let textselector = "//*[@id='tab-1']/div[2]/div[2]/div[1]/div/div[@class='course-list__course-name']/following::div[@data-student-type='international' and not(contains(text(),'Not available to international students'))]//../..//a"
      var totalCourseList = [];
      for (let target of targetLinksCardsTotal) {
        await page.goto(target.href, { timeout: 0 });
       
        await page.waitFor(5000);
        const targetLinks = await page.$x(linkselector);
        const targetText = await page.$x(textselector);
       // await page.waitFor(5000);
        console.log("target.innerText -->", target.innerText);
        console.log("#total link selectors---->" + targetLinks.length);
        for (var i = 0; i < targetLinks.length; i++) {
          var elementstring = await page.evaluate(el => el.innerText, targetText[i]);
          const elementlink = await page.evaluate(el => el.href, targetLinks[i]);
          let payload = { href: elementlink, innerText: elementstring, category: target.innerText }
          totalCourseList.push(payload);
        }
      }

      await fs.writeFileSync("./output/charlesdarwinuniversity_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
     
      await fs.writeFileSync("./output/charlesdarwinuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }

          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/charlesdarwinuniversity_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  // refers to https://futurestudents.csu.edu.au/courses/all
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');

      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // refers to https:/ / coursefinder.uow.edu.au / undergrad / index.html
  static async scrapeCourseListForPages(courseItemSelectorList, firstPageSelector, pageURL) {
    const funcName = 'scrapeCourseListForPages ';
    let s = null; let courseList = [];
    try {
      // validate params
      Scrape.validateParams([courseItemSelectorList, firstPageSelector, pageURL]);
      console.log(funcName + 'courseItemSelectorList = ' + JSON.stringify(courseItemSelectorList));
      // create Scrape instance
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(pageURL);
      let nextElementSiblingHandle = null;
      const activePaginationItemSel = firstPageSelector;
      let pageCount = 1;
      if (fs.existsSync(configs.opCourseListFilepath)) {
        courseList = JSON.parse(fs.readFileSync(configs.opCourseListFilepath))
      }
      do {
        await s.page.waitFor(3000);
        // wait for sel
        console.log(funcName + 'waitForSelector sel = ' + activePaginationItemSel);
        await s.page.waitForSelector(activePaginationItemSel, { visible: true });
        // scrape all course list items
        console.log(funcName + '\n\r ********** Scraping for page ' + pageCount + ' ********** \n\r');
        const resList = [];

        console.log(funcName + 'waitForSelector sel = ' + courseItemSelectorList[0]);
        await s.page.waitForSelector(courseItemSelectorList[0]);

        const anchorEleHandle = await s.page.$(courseItemSelectorList[0]); // anchor element handle
        console.log(funcName + 'anchorEleHandle = ' + anchorEleHandle);

        const hrefList = await s.scrapeAnchorElement(courseItemSelectorList[0], null, '');
        // console.log(funcName + 'hrefList = ' + JSON.stringify(hrefList));
        for (const itemDict of hrefList) {
          const courseDict = {};
          courseDict.href = itemDict.href;
          const itemInnerText = itemDict.innerText;
          const innerTextList = String(itemInnerText).split('\n');
          console.log(funcName + 'innerTextList = ' + JSON.stringify(innerTextList));
          if (innerTextList && innerTextList.length > 0) {
            courseDict.innerText = innerTextList[0];
          }
          console.log(funcName + 'courseDict = ' + JSON.stringify(courseDict));
          resList.push(courseDict);
        } // for

        courseList = courseList.concat(resList);
        // console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
        const eleList = await s.page.$$(activePaginationItemSel);
        if (Array.isArray(eleList) && eleList.length > 0) {
          const activePageButton = eleList[0];
          // reassign next sibling handle
          nextElementSiblingHandle = await activePageButton.getProperty('nextElementSibling');
          console.log(funcName + 'nextElementSiblingHandle = ' + nextElementSiblingHandle);
          var elemsib = await s.page.evaluateHandle(el => el.nextElementSibling, activePageButton);
          var result = await s.page.evaluate(el => el.innerText, elemsib);
          console.log("## Text---->" + result);
          var numbers = /^[0-9]+$/;
          if (nextElementSiblingHandle == "JSHandle@node" && result.match(numbers)) {
            const propHandle = nextElementSiblingHandle.getProperty('nodeName');

            console.log(funcName + 'propHandle = ' + propHandle);
            let nextSiblingNodeName = null;
            await propHandle.then((res) => {
              console.log('res = ' + res);
              const resJSon = res.jsonValue();
              console.log('resJSon = ' + resJSon);
              resJSon.then((resB) => {
                console.log('resB = ' + resB);
                nextSiblingNodeName = resB;
              });
            });
            console.log(funcName + 'nextSiblingNodeName = ' + nextSiblingNodeName);
            if (String(nextSiblingNodeName) !== 'LI') { // if next is not Anchor element
              console.log(funcName + 'nextElementSiblingHandle includes null....so breaking the loop...');
              break;
            }
            if (nextElementSiblingHandle) {
              await nextElementSiblingHandle.click('middle');
              console.log(funcName + 'Clicked nextElementSiblingHandle..');
            } else {
              throw (new Error('nextElementSiblingHandle invalid'));
            }
          }
          else { break; }
        }

        pageCount += 1;
        console.log(funcName + 'courseList count = ' + courseList.length);
      } while (nextElementSiblingHandle);

      console.log(funcName + 'total pages scrapped = ' + pageCount);
      console.log(funcName + 'courseList count = ' + courseList.length);
      // console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(courseList));
     
      console.log(funcName + 'writing courseList to file completed successfully....');
      if (s) {
        await s.close();
      }
    } catch (error) {
      console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r\n\r');
      console.log(funcName + 'try-catch error = ' + error);
      console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r\n\r');
      if (s) {
        await s.close();
      }
      console.log(funcName + 'writing courseList to tmp file....');
      await fs.writeFileSync(configs.opCourseListTempFilepath, JSON.stringify(courseList));

      console.log(funcName + 'writing courseList to tmp file completed successfully....');
      throw (error);
    }
  }

  // scrape course page list of the univ
  async scrapeCourseListAndPutAtS3(selFilepath) {
    const funcName = 'scrapeCourseListAndPutAtS3 ';
    let courseList = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      const URL = this.selectorJson.url;
      if (fs.existsSync(configs.opCourseListFilepath)) {
        fs.unlinkSync(configs.opCourseListFilepath)
      }
      for (let pageUrl of URL) {
        console.log(funcName + 'url = ' + pageUrl);
        const jsonDictElements = this.selectorJson.elements;
        console.log(funcName + 'elements = ' + JSON.stringify(jsonDictElements));

        if (Array.isArray(jsonDictElements)) {
          for (const eleDict of jsonDictElements) {
            const eleType = eleDict.elementType;
            console.log(funcName + 'elementType = ' + eleType);
            // ensure type is matching
            if (eleType === Scrape.ELEMENT_TYPE.COURSE_LIST) {
              const courseItemSelector = eleDict.course_item_selector;
              console.log(funcName + 'course_item_selector = ' + courseItemSelector);
              const pageSelector = eleDict.page_selector;
              console.log(funcName + 'page_selector = ' + pageSelector);
              courseList = await ScrapeCourseList.scrapeCourseListForPages(courseItemSelector, pageSelector, pageUrl);
            } // if
          } // for elements
        } // if
      }

      return courseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  async scrapeSubjectList(selFilepath) {
    const funcName = 'scrapeSubjectList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);

      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      // course_level_selector key
      const subjectListSel = await utils.getSelectorsListForKey('subject_list_selector', this.selectorJson);
      if (!subjectListSel) {
        console.log(funcName + 'Invalid subjectListSel');
        throw (new Error('Invalid subjectListSel'));
      }
      console.log("R subjectListSel", subjectListSel);
      const elementDictForSubjectListSel = await utils.getElementDictForKey('subject_list_selector', this.selectorJson);
      console.log(funcName + 'elementDictForSubjectListSel = ' + JSON.stringify(elementDictForSubjectListSel));
      if (!elementDictForSubjectListSel) {
        console.log(funcName + 'Invalid elementDictForSubjectListSel');
        throw (new Error('Invalid elementDictForSubjectListSel'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('subject_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));




      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      var totalSubjectList = {};
      totalSubjectList.subject_areas = [];
      let firstPageSelector = this.selectorJson.subject_list_selector[0].elements[1].page_selector;
      const activePaginationItemSel = firstPageSelector;
      let pageCount = 1;
      do {
        var resList = [];
        if (pageCount == 1) {
          //first time
          // anchor list selector of course list          
          resList = await s.scrapeElement(elementDictForSubjectListSel.elementType, subjectListSel, rootEleDictUrl, null);
          console.log(funcName + 'resList = ' + JSON.stringify(resList.subject_areas));
        }
        else {
          await s.page.waitFor(3000);
          // wait for sel
          console.log(funcName + 'waitForSelector sel = ' + activePaginationItemSel);
          await s.page.waitForSelector(activePaginationItemSel, { visible: true });
          // scrape all course list items
          console.log(funcName + '\n\r ********** Scraping for page ' + pageCount + ' ********** \n\r');

          // anchor list selector of course list
          console.log("Before scrape element function, elementDictForSubjectListSel.elementType", elementDictForSubjectListSel.elementType, "subjectListSel[2]", subjectListSel[2]);
          resList = await s.scrapeElement(elementDictForSubjectListSel.elementType, subjectListSel[2], rootEleDictUrl, null);
          console.log(funcName + 'resList = ' + JSON.stringify(resList.subject_areas));
        }

        totalSubjectList.subject_areas = totalSubjectList.subject_areas.concat(resList.subject_areas);
        console.log('R inside do totalSubjectList = ' + JSON.stringify(totalSubjectList));
        //await s.page.click(activePaginationItemSel);
        if (pageCount != 5) {
          const eleList = await s.page.$$(activePaginationItemSel);
          const activePageButton = eleList[pageCount - 1];
          await activePageButton.click();
        }

        pageCount += 1;
        console.log(funcName + 'totalSubjectList count = ' + totalSubjectList.length);
      } while (pageCount <= 5);


      console.log(funcName + 'writing totalSubjectList to file....');
      await fs.writeFileSync(configs.opSubjectListFilepath, JSON.stringify(totalSubjectList));
      console.log(funcName + 'writing subjectList to file completed successfully....');

      if (s) {
        await s.close();
      }
      return totalSubjectList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
