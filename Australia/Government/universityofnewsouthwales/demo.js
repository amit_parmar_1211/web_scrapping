const puppeteer = require('puppeteer');
async function calculatemyfees(ccode) {
    var myfee;
    try {
        const URL = "http://indicativefees.unsw.edu.au/";
        let browser = await puppeteer.launch({ headless: false }); //headless:false so we can debug
        let page = await browser.newPage();
        await page.goto(URL);

        await page.$eval("#commencing-years-inner > ul > li > div > a", el => el.click());
        await page.waitFor(2000);
        await page.type('#search-program', ccode, { delay: 10 })
        await page.waitFor(2000);
        const scrape_element_url = await page.$$("#degrees-autocomplete-container > ul > li");
        for (elm of scrape_element_url) {
            await elm.click()
        }
        await page.$eval("#search-program-submit", el => el.click());
        await page.waitFor(5000);
        //await page.$eval("body > ul:nth-child(6) > li", el => el.click());
        const feeselm = await page.$x("//*[@id='browser-fee-estimate']/div[2]/ul/li[1]/text()");
        for (elm of feeselm) {
            myfee = await page.evaluate(el => el.textContent, elm);
        }
        await browser.close()
    }
    catch (e) {
        console.log("Error--->" + e);
    }
    return myfee.replace(/[$,]/g,"").trim();
}

async function start() {
    console.log(await calculatemyfees("1900"));
}
start();
