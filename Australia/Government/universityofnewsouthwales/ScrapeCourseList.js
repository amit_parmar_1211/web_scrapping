const fs = require("fs");
const Scrape = require("./common/scrape").Scrape;
const utils = require("./common/utils");
const configs = require("./configs");
const appConfigs = require("./common/app-config");

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList() {
    const funcName = "scrapeOnlyInternationalCourseList ";
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage("https://www.international.unsw.edu.au/");

      let totalcourselist = [];

      const url = [
        // "https://www.international.unsw.edu.au/faculty/art-design-undergraduate-degree-programs",
        // "https://www.international.unsw.edu.au/faculty/arts-social-sciences-undergraduate-degree-programs",
        // "https://www.international.unsw.edu.au/faculty/built-environment-undergraduate-degree-programs",
        // "https://www.international.unsw.edu.au/faculty/business-school-undergraduate-degree-programs",
        // "https://www.international.unsw.edu.au/faculty/engineering-undergraduate-degree-programs",
        // "https://www.international.unsw.edu.au/faculty/law-undergraduate-degree-programs",
        // "https://www.international.unsw.edu.au/faculty/medicine-undergraduate-degree-programs",
        // "https://www.international.unsw.edu.au/faculty/science-undergraduate-degree-programs",
        // "https://www.international.unsw.edu.au/faculty/art-design-postgraduate-degree-programs",
        // "https://www.international.unsw.edu.au/faculty/arts-social-sciences-postgraduate-degree-programs",
        // "https://www.international.unsw.edu.au/faculty/built-environment-postgraduate-degree-programs",
        // "https://www.international.unsw.edu.au/faculty/business-school-postgraduate-degree-programs",
        // "https://www.international.unsw.edu.au/faculty/engineering-postgraduate-degree-programs",
        "https://www.international.unsw.edu.au/faculty/medicine-postgraduate-degree-programs",
        //  "https://www.international.unsw.edu.au/faculty/law-postgraduate-degree-programs",
       // "https://www.international.unsw.edu.au/faculty/science-postgraduate-degree-programs"
      ];
      for (let urls of url) {
        let checks;
        await s.page.goto(urls, { timeout: 0 });
        let urlselector = await s.page.$x(
          "//*[@id='postgraduate-options4' or @id='undergraduate-options4']/div/div/div/div/div/div/div/a"
        );
        
        let programSelector = await s.page.$x(
          "//*[@id='undergraduate-options4'or @id='postgraduate-options4']/div/div/div[1]/div[1]/div[1]/div"
        );
        let durationSelector = await s.page.$x(
          "//*[@id='postgraduate-options4' or  @id='undergraduate-options4']/div/div/div/div[2]/div/div[1]/dl[1]/dt[contains(text(),'Minimum years')]/following-sibling::dd[1]"
        );
        let intakeSelector = await s.page.$x(
          "//*[@id='undergraduate-options4' or @id='postgraduate-options4']/div/div/div/div/div/div/dl/dt[contains(text(),'Entry')]/following-sibling::dd[1]"
        );
        let categoryselector = await s.page.$x(
          "//h1[@class='herotitle-title']"
        );
        let studylevel = await s.page.$x(
          "//*[@id='page']/div/div/div/div/div/div/form/span/span[1]/span/span"
        );
        let titleSelector = await s.page.$x(
          "//*[@id='postgraduate-options4' or @id='undergraduate-options4']/div/div/div/div/div/h5"
        );

        // let careerSelector = await s.page.$x(
        //   "//*[@id='postgraduate-options4' or @id='undergraduate-options4']/div/div/div/div/div/div/dl/dt[contains(text(),'Career Opportunities')]/following-sibling::dd/div"
        // );
        // const careerselectors = await s.page.$x(
        //   "//*[@id='postgraduate-options4' or @id='undergraduate-options4']/div/div/div/div/div/div/dl/dt[contains(text(),'Career Opportunities')]/following-sibling::dd"
        // );

        // console.log("YESSSSSSSSSS", careerclick.length);
        for (let i = 0; i < urlselector.length; i++) {
          var elementcareer = "";
          var elementstudylevel = "";
          var elementstring = "";
          var elementurl = "";
          var elementduration = "";
          var elementcategory = "";
          var elementintake = "";
          var elementprogramCode = "";
          var elementTitle = "";
          if (titleSelector[i]) {
            elementTitle = await s.page.evaluate(el => el.innerText, titleSelector[i]);
          }
         
          if (urlselector[i]) {
            elementurl = await s.page.evaluate(el => el.href, urlselector[i]);
          }

          if (durationSelector[i]) {
            elementduration = await s.page.evaluate(
              el => el.innerText,
              durationSelector[i]
            );
          }

          if (intakeSelector[i]) {
            elementintake = await s.page.evaluate(
              el => el.innerText,
              intakeSelector[i]
            );
          }

          if (programSelector[i]) {
            elementprogramCode = await s.page.evaluate( el => el.innerText,programSelector[i]);
          }        
           if (categoryselector[0]) {
            elementcategory = await s.page.evaluate( el => el.innerText,categoryselector[0]);
          }
          if (studylevel[0]) {
            elementstudylevel = await s.page.evaluate( el => el.innerText,studylevel[0] );
          }
          let careerclick = "//*[@id='postgraduate-options4' or @id='undergraduate-options4']/div/div/div/div/div/h5[contains(text(),'"+elementTitle+"')]/following::div[3]/div[1]/div[1]/dl/dt[contains(text(),'Career Opportunities')]/following-sibling::dd/button";
          console.log("careerclick",careerclick)

          let clicks=await s.page.$x(careerclick)
          console.log("careerclick",clicks.length)
          if (clicks && clicks.length > 0) {
            let careerSelector = await s.page.$x(
              "//*[@id='postgraduate-options4' or @id='undergraduate-options4']/div/div/div/div/div/h5[contains(text(),'"+elementTitle+"')]/following::div[3]/div[1]/div[1]/dl/dt[contains(text(),'Career Opportunities')]/following-sibling::dd/div[1]"
            );
            let careerselectors= await s.page.$x(
              "//*[@id='postgraduate-options4' or @id='undergraduate-options4']/div/div/div/div/div/h5[contains(text(),'"+elementTitle+"')]/following::div[3]/div[1]/div[1]/dl/dt[contains(text(),'Career Opportunities')]/following-sibling::dd/div[2]"
            );
            console.log("yessss");
            await clicks[0].click();
            console.log("click...");
            await s.page.waitFor(5000);
           let career = await s.page.evaluate( el => el.innerText,careerSelector[0]);
           let careers=await s.page.evaluate(el=>el.innerText,careerselectors[0]);
            elementcareer=career+careers;
            console.log("elementcareer",elementcareer)
            console.log("inif", elementTitle);
          } else {
            console.log("Else==>", elementTitle);
            let check =
            "//*[@id='postgraduate-options4' or @id='undergraduate-options4']/div/div/div/div/div/h5[contains(text(),'"+elementTitle+"')]/following::div[3]/div[1]/div[1]/dl/dt[contains(text(),'Career Opportunities')]/following-sibling::dd"
            let checked = await s.page.$x(check);
            console.log("checked -->> ", checked.length + " i : ", i);
            if (checked.length >0) {
              elementcareer = await s.page.evaluate(
                el => el.innerText,
                checked[0]
              );
            } else {
              elementcareer = "";
            }
            console.log(elementcareer);
          }

          // console.log(elementcareer);

          // console.log("InIF")
          totalcourselist.push({
            href: elementurl,
            innerText: elementTitle,
            duration: elementduration,
            intake: elementintake,
            programCode: elementprogramCode,
            Title: elementTitle,
            category: elementcategory,
            CareerOutcome: elementcareer,
            study_level: elementstudylevel
          });
        }
      
        console.log("Length-->", totalcourselist);
      
      }
      await fs.writeFileSync(
        configs.opCourseListFilepath,
        JSON.stringify(totalcourselist)
      );

      console.log("yesssss", totalcourselist);
      // destroy scrape resources
      if (s) {
        await s.close();
      }

      // return totalCourseList;
    } catch (error) {
      console.log(funcName + "try-catch error = " + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw error;
    }
  }
} // class

module.exports = { ScrapeCourseList };
