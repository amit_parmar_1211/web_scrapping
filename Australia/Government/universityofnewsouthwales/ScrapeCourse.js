const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_category, cduration, cintake, cprogramCode, coursetitle, cstudy_level, ccareeroutcome) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            var arr = [];
                            arr.push(course_category);
                            console.log("course_category" + JSON.stringify(arr))
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = arr
                            break;
                        }
                        case 'course_title': {

                            var ctitle = format_functions.titleCase(coursetitle)
                            resJsonData.course_title = ctitle;
                            break;
                        }


                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            let programType = null;
                            let category = null;
                            //progress bar start
                            const penglishList = [], pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            const ieltsDict = {}; const ibtDict = {}; const pteDict = {}; const utasDict = {}; let ieltsMapNumber = null; let ieltsNumber = null; const cpeDict = {};
                            const caedisc = {};
                            let categorys = resJsonData.course_discipline
                            const cTitle = resJsonData.course_title
                            console.log("Category", JSON.stringify(categorys))
                            // english requirement
                            if (categorys) {
                                if (categorys == "Business School") {
                                    categorys = "Business"
                                }

                                if (categorys == 'Arts & Social Sciences') {
                                    categorys = "Arts and Social Sciences"
                                }

                                if (categorys == 'Art & Design') {
                                    categorys = 'Art and Design'
                                }

                                console.log("cTitle :" + cTitle);
                                console.log("ieltsScore :" + JSON.stringify(ieltsScore));
                                if (categorys) {
                                    //console.log("hello@-->" + String(ieltsScore).includes(categorys));
                                    if (String(categorys) == 'Science') {
                                        if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || (String(cTitle).toLowerCase().indexOf('diploma') > -1 && String(cTitle).toLowerCase().indexOf('graduate diploma') == -1)) {


                                            programType = "Undergraduate";


                                        } else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1) {

                                            programType = "Postgraduate research";
                                        }
                                        else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1 || String(cTitle).toLowerCase().indexOf('graduate diploma') > -1 || String(cTitle).toLowerCase().indexOf('graduate certificate') > -1) {

                                            programType = "Postgraduate";
                                        }
                                        console.log("hello" + String(cTitle).toLowerCase().indexOf('doctor'));
                                        console.log("hello @@@" + JSON.stringify(programType));
                                    }

                                    else if (String(categorys) == 'Engineering') {
                                        console.log("hello")
                                        if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || (String(cTitle).toLowerCase().indexOf('diploma') > -1 && String(cTitle).toLowerCase().indexOf('graduate diploma') == -1)) {


                                            programType = "Undergraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1 || String(cTitle).toLowerCase().indexOf('graduate diploma') > -1 || String(cTitle).toLowerCase().indexOf('graduate certificate') > -1) {

                                            programType = "Postgraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1) {

                                            programType = "Postgraduate research";
                                        }

                                    }
                                    else if (String(categorys) == 'Medicine') {
                                        console.log("hello")
                                        if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || (String(cTitle).toLowerCase().indexOf('diploma') > -1 && String(cTitle).toLowerCase().indexOf('graduate diploma') == -1)) {


                                            programType = "Undergraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1 || String(cTitle).toLowerCase().indexOf('graduate diploma') > -1 || String(cTitle).toLowerCase().indexOf('graduate certificate') > -1) {

                                            programType = "Postgraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1) {

                                            programType = "Postgraduate research";
                                        }

                                        console.log("category" + JSON.stringify(category));
                                    }

                                    else if (String(categorys) == 'Business') {
                                        if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || (String(cTitle).toLowerCase().indexOf('diploma') > -1 && String(cTitle).toLowerCase().indexOf('graduate diploma') == -1)) {


                                            programType = "Undergraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1 || String(cTitle).toLowerCase().indexOf('graduate diploma') > -1 || String(cTitle).toLowerCase().indexOf('graduate certificate') > -1 || String(cTitle) == "AGSM MBA") {

                                            programType = "Postgraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1) {

                                            programType = "Postgraduate research";
                                        }
                                    } else if (String(categorys).toLowerCase() == 'art and design') {
                                        if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || (String(cTitle).toLowerCase().indexOf('diploma') > -1 && String(cTitle).toLowerCase().indexOf('graduate diploma') == -1)) {


                                            programType = "Undergraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1 || String(cTitle).toLowerCase().indexOf('graduate diploma') > -1 || String(cTitle).toLowerCase().indexOf('graduate certificate') > -1) {

                                            programType = "Postgraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1) {

                                            programType = "Postgraduate research";
                                        }

                                    } else if (String(categorys) == 'Law') {
                                        console.log("hello");
                                        if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || (String(cTitle).toLowerCase().indexOf('diploma') > -1 && String(cTitle).toLowerCase().indexOf('graduate diploma') == -1)) {
                                            console.log("helloo");

                                            programType = "Undergraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1 || String(cTitle).toLowerCase().indexOf('graduate diploma') > -1 || String(cTitle).toLowerCase().indexOf('graduate certificate') > -1) {

                                            programType = "Postgraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1) {

                                            programType = "Postgraduate research";
                                        }
                                    }
                                    else if (String(categorys).toLowerCase() == 'arts and social sciences') {
                                        if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || (String(cTitle).toLowerCase().indexOf('diploma') > -1 && String(cTitle).toLowerCase().indexOf('graduate diploma') == -1)) {


                                            programType = "Undergraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1 || String(cTitle).toLowerCase().indexOf('graduate diploma') > -1 || String(cTitle).toLowerCase().indexOf('graduate certificate') > -1) {

                                            programType = "Postgraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1) {

                                            programType = "Postgraduate research";
                                        }

                                    } else if (String(categorys) == 'Built Environment') {
                                        if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || (String(cTitle).toLowerCase().indexOf('diploma') > -1 && String(cTitle).toLowerCase().indexOf('graduate diploma') == -1)) {


                                            programType = "Undergraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1 || String(cTitle).toLowerCase().indexOf('graduate diploma') > -1 || String(cTitle).toLowerCase().indexOf('graduate certificate') > -1) {

                                            programType = "Postgraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1) {

                                            programType = "Postgraduate research";
                                        }


                                    } else if (String(categorys) == 'UNSW Canberra') {
                                        if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || (String(cTitle).toLowerCase().indexOf('diploma') > -1 && String(cTitle).toLowerCase().indexOf('graduate diploma') == -1)) {


                                            programType = "Undergraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1 || String(cTitle).toLowerCase().indexOf('graduate diploma') > -1 || String(cTitle).toLowerCase().indexOf('graduate certificate') > -1) {

                                            programType = "Postgraduate";
                                        } else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1) {

                                            programType = "Postgraduate research";
                                        }
                                    }




                                }
                            }

                            if (String(categorys) && programType) {
                                // const ieltsMappingProgressDict = await utils.getValueFromHardCodedJsonFile('ielts_progress');
                                const ieltsMappingDict = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                                if (ieltsMappingDict) {
                                    var cat = String(categorys);

                                    console.log("category:" + cat);
                                    console.log("programType:" + programType);

                                    //console.log("----->ieltsMapping###:" + JSON.stringify(ieltsMappingDict));
                                    console.log("----->ieltsMapping###@@@@:" + JSON.stringify(ieltsMappingDict[cat]));

                                    // englishList.push({ name: "ielts academic", description: ieltsMappingDict[cat][programType].ielts });
                                    // englishList.push({ name: "toefl ibt", description: ieltsMappingDict[cat][programType].ibt });

                                    // englishList.push({ name: "pte academic", description: ieltsMappingDict[cat][programType].pte });
                                    // englishList.push({ name: "cae", description: ieltsMappingDict[cat][programType].cae });

                                }
                                const otherLngDict = ieltsMappingDict[cat][programType];
                                const potherLngDict = otherLngDict;
                                if (potherLngDict) {
                                    var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                                    if (potherLngDict.ielts) {
                                        ieltsScore = await utils.giveMeNumber(potherLngDict.ielts);
                                        console.log("### IELTS data-->" + ieltsScore);
                                    }
                                    if (potherLngDict.ibt) {
                                        ibtScore = await utils.giveMeNumber(potherLngDict.ibt);
                                    }
                                    if (potherLngDict.pte) {
                                        pteScore = await utils.giveMeNumber(potherLngDict.pte);
                                    }
                                    if (potherLngDict.cae) {
                                        caeScore = await utils.giveMeNumber(potherLngDict.cae);
                                    }

                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[cat][programType].ielts;
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsScore;
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);

                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ieltsMappingDict[cat][programType].ibt;
                                    ibtDict.min = 0;
                                    ibtDict.require = ibtScore;
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);

                                    pteDict.name = 'pte academic',
                                        pteDict.description = ieltsMappingDict[cat][programType].pte;
                                    pteDict.min = 0;
                                    pteDict.require = pteScore;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);

                                    caedisc.name = 'cae',
                                        caedisc.description = ieltsMappingDict[cat][programType].cae;
                                    caedisc.min = 80;
                                    caedisc.require = caeScore;
                                    caedisc.max = 230;
                                    caedisc.R = 0;
                                    caedisc.W = 0;
                                    caedisc.S = 0;
                                    caedisc.L = 0;
                                    caedisc.O = 0;
                                    englishList.push(caedisc);

                                    // if (ieltsScore != "NA") {
                                    //     pieltsDict.name = 'ielts academic';
                                    //     var value = {};
                                    //     value.min = 0;
                                    //     value.require = ieltsScore;
                                    //     value.max = 9;
                                    //     pieltsDict.value = value;
                                    //     penglishList.push(pieltsDict);
                                    // }
                                    // if (ibtScore != "NA") {
                                    //     pibtDict.name = 'toefl ibt';
                                    //     var value = {};
                                    //     value.min = 0;
                                    //     value.require = ibtScore;
                                    //     value.max = 120;
                                    //     pibtDict.value = value;
                                    //     penglishList.push(pibtDict);
                                    // }
                                    // // if (pbtScore!="NA") {
                                    // //   ppbtDict.name = 'toefl pbt';
                                    // //   ppbtDict.value = pbtScore;
                                    // //   penglishList.push(ppbtDict);
                                    // // }
                                    // if (pteScore != "NA") {
                                    //     ppteDict.name = 'pte academic';
                                    //     var value = {};
                                    //     value.min = 0;
                                    //     value.require = pteScore;
                                    //     value.max = 90;
                                    //     ppteDict.value = value;
                                    //     penglishList.push(ppteDict);
                                    // }
                                    // if (caeScore != "NA") {
                                    //     pcaeDict.name = 'cae';
                                    //     var value = {};
                                    //     value.min = 80;
                                    //     value.require = caeScore;
                                    //     value.max = 230;
                                    //     pcaeDict.value = value;
                                    //     penglishList.push(pcaeDict);
                                    // }
                                    //}
                                }
                            }
                            //console.log("ProgressBar" + JSON.stringify(penglishList));

                            // if (penglishList && penglishList.length > 0) {
                            //     courseAdminReq.englishprogress = penglishList;
                            // }
                            ///progrssbar End
                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            } else {
                                throw new Error("ielts score have issue");
                            }
                            console.log("PROGRESSSS@@@@@", JSON.stringify(courseAdminReq.english))
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                var arr = [];
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                arr.push(academicReq)
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = arr;
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }

                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.entry_requirements_url;
                            console.log("EntryRequirement:::" + courseKeyVal);
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))




                            // add english requirement 'english_requirements_url'
                            const courseKeyValEnglishReq = courseScrappedData.english_requirements_url;
                            console.log("EnglishRequirement:::" + courseKeyValEnglishReq);
                            let resEnglishReqJson = null;
                            if (Array.isArray(courseKeyValEnglishReq)) {
                                for (const rootEleDict of courseKeyValEnglishReq) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            let academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;
                            }
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {

                            // let program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            let duration = []
                            if(!cduration.includes('years')){
                                cduration=cduration+' '+'years'
                            }
                            // if(!cduration.includes('full-time')){
                            //     cduration=cduration+''+'full-time'  
                            // }else{
                            //     cduration=cduration 
                            // }
                           
                            duration.push(cduration);
                            console.log("cduration --> ", cduration);
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];

                                            }
                                        }
                                    }
                                }
                            }
                            if (duration && duration.length > 0) {
                                const resFulltime = duration[0];
                                console.log("resFull" + JSON.stringify(resFulltime));
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("fullTimeText" + JSON.stringify(filtered_duration_formated))
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {

                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }

                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':

                        case 'course_campus_location': {

                            const courseKeyVals = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;
                                                // if(course_cricos_code.includes("^")){
                                                //     console.log("YESS ^")
                                                // }
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_cricos_code) {
                                global.cricos_code = course_cricos_code[0];
                                console.log("Code###", JSON.stringify(course_cricos_code[0]))
                                if (course_cricos_code[0].includes("^")) {
                                    console.log("YESSS")
                                    var splitdata = course_cricos_code[0].split("^")
                                    console.log("YESSS", JSON.stringify(splitdata))
                                    course_cricos_code = splitdata[0]
                                }
                            }


                            var campLocationText = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList[0];
                                            ///}
                                        }
                                    }
                                }
                            }
                            console.log("##Campus-->" + campLocationText)
                            if (campLocationText && campLocationText.length > 0) {
                                // var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                let arr = []
                                arr.push(campLocationText)
                                console.log("ArrayofFess", arr)
                                var campusedata = [];
                                campLocationText.forEach(element => {
                                    var cam = format_functions.titleCase(element)
                                    campusedata.push({
                                        "name": cam,
                                        "code": String(course_cricos_code)
                                    })
                                });
                                console.log("CampusDATA", campusedata)

                                resJsonData.course_campus_location = campusedata;//await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');


                                resJsonData.course_study_mode = 'On campus';//.join(',');

                            } else {
                                throw new Error("Campus Location not found");
                            }
                            break;
                        }

                        case 'course_tuition_fees_international_student': {
                            const pcode = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            console.log(funcName + 'pcode ' + JSON.stringify(pcode));

                            var myfeeFIle = await format_functions.calculatemyfees(pcode);
                            console.log("myfees" + JSON.stringify(myfeeFIle));
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};

                            //for first year only
                            var feesIntStudent = myfeeFIle
                            console.log("FeesData" + JSON.stringify(feesIntStudent));

                            if (feesIntStudent.length > 0) { // extract only digits
                                console.log("international_student" + feesIntStudent)
                                // feesDict.international_student = feesIntStudent;
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal && feesVal != "NA") {
                                    const myval = feesVal.split(".");
                                    const regEx = /\d/g;
                                    let feesValNum = myval[0].match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            //feesDict.international_student = Number(feesNumber);
                                            feesDict.international_student = {
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: feesIntStudent,

                                            };
                                        }
                                    }
                                } else {
                                    //feesDict.international_student = 0
                                    feesDict.international_student = {
                                        amount: 0,
                                        duration: 1,
                                        unit: "Year",
                                        description: ""

                                    }
                                }
                            }

                            console.log("value###" + JSON.stringify(feesDict.international_student))
                            //for all fees

                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);



                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;

                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }
                            break;
                        }

                        case 'course_intake_url': {
                            const courseKeyVal = courseScrappedData.course_intake_url;
                            var url = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                url = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (url.length > 0) {
                                resJsonData.course_intake_url = url;
                            }
                            break;
                        }
                        case 'program_code': {

                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (program_code.length > 0) {
                                resJsonData.program_code = String(program_code);
                            }
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            // Semester 1 (February) and Semester 2 (August)
                            // prepare emty JSON struct
                            const courseIntakeDisplay = {};
                            // existing intake value

                            var courseIntakeStr = null;//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            let courseIntake = cintake
                            if (courseIntake.includes("and")) {
                                let intakeArray = courseIntake.split("and");
                                console.log("INTAKEARRAY" + JSON.stringify(intakeArray));
                                // for (let temp of intakeArray) {
                                //     //temp = temp.trim();
                                //     courseIntakeStr.push(temp + ", 2019");
                                //   }
                                courseIntakeStr = intakeArray;
                                console.log("yes")
                                console.log("INTAKEARRAY@@@" + JSON.stringify(courseIntakeStr));

                            } else {
                                let arr = []
                                arr.push(courseIntake)
                                courseIntakeStr = arr;
                                console.log("No");
                            }
                            //courseIntakeStr = [cintake];


                            if (courseIntakeStr && courseIntakeStr.length > 0) {

                                // const intake = await utils.getValueFromHardCodedJsonFile('course_intake');

                                console.log("InteakeStr@@@" + JSON.stringify(courseIntakeStr));
                                console.log("INTAKE FROM HARDCODE---" + JSON.stringify(String(courseIntakeStr).replace(/[\r\n\t ]+/g, '')));
                                var campus = resJsonData.course_campus_location;
                                var intakes = [];
                                console.log("Intake length-->" + courseIntakeStr.length);
                                console.log("Campus length-->" + JSON.stringify(campus[0].name));
                                var intakedetail = {};
                                //  console.log("Finalintake" + JSON.stringify((myintake)));

                                intakedetail.name = campus[0].name;
                                intakedetail.value = courseIntakeStr;
                                intakes.push(intakedetail);
                                var intakedata = {};
                                var intakeUrl = [];
                                console.log("FinalIntake--" + JSON.stringify(intakes));
                                let formatedIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                console.log("NEw Intake Format@@@@@" + JSON.stringify(formatedIntake))
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedata.intake = formatedIntake;
                                intakedata.more_details = String(intakeUrl);
                                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');

                            } else {
                                var intakedetail = {};
                                var intakes = [];
                                var intakeUrl = [];
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedetail.name = campus[0];
                                intakedetail.value = [];

                                intakes.push(intakedetail);
                                var intakedata = {};
                                intakedata.intake = intakes;
                                intakedata.more_details = intakeUrl;
                                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
                            }
                            break;
                        }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            console.log("overview---" + JSON.stringify(courseKeyVal));
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_overview = resAccomodationCostJson;
                            } else {
                                resJsonData.course_overview = ""
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            // console.log("ccareeroutcome----->",ccareeroutcome)
                            if (String(ccareeroutcome) == []) {
                                console.log("inif")
                                resJsonData.course_career_outcome = []
                            } else {
                                console.log("inelse")
                                resJsonData.course_career_outcome = [ccareeroutcome];
                            }



                            break;
                        }
                        case 'course_study_level': {
                            resJsonData.course_study_level = cstudy_level;
                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                            const courseKeyVal_major =courseScrappedData.course_outline;
                             const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                             console.log("MAjor==>", JSON.stringify(courseKeyVal_major))
                             let course_outlines = {};
                             if (courseKeyVal_minor != null) {
                                 course_outlines.minors = courseKeyVal_minor
                             } else {
                                 course_outlines.minors = []
                             }
                             if (courseKeyVal_major) {
                                let course_career_outcome = null;
                                if (Array.isArray(courseKeyVal_major)) {
                                    for (const rootEleDict of courseKeyVal_major) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    course_career_outcome = selList;
                                                }
                                            }
                                        }
                                    }
                                }
                                if(course_career_outcome!=null){
                                    course_outlines.majors = course_career_outcome
                                }else{
                                    course_outlines.majors = []
                                }
                              
                            }
                             if (courseKeyVal != null) {
                                 course_outlines.more_details = courseKeyVal
                             } else {
                                 course_outlines.more_details = ""
                             }
                             resJsonData.course_outline = course_outlines
                             break;
                         }
                         case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            console.log("Application==>",courseKeyVal)
                            if (courseKeyVal == null || courseKeyVal == "null" ) {
                                resJsonData.application_fee = ""
                            } else {
                                resJsonData.application_fee = courseKeyVal
                            }
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            // console.log('\n\r' + funcName + 'course resJsonData =' + JSON.stringify(resJsonData));
            // var resJsonData = JSON.parse(fs.readFileSync("australiancatholicuniversity_graduatediplomainrehabilitation_coursedetails.json"))[0];
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;

                
                var filelocation = "./output/" + resJsonData.univ_id + "_" +basecourseid.replace(/[\n :""]+/g, '').replace(/[\/]+/g, '_') + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }


            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.category, courseDict.duration, courseDict.intake, courseDict.programCode, courseDict.Title, courseDict.study_level, courseDict.CareerOutcome);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };

























