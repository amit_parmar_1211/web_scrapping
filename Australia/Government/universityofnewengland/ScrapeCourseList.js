const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const configs = require('./configs');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList(path) {
    const funcName = 'startScrappingFunc ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];
      await s.setupNewBrowserPage("https://www.une.edu.au/study/study-options/study-areas");
      var mainCategory = [], redirectUrl = [];
      const maincategoryselector = "//*[@id='content_container_242249']/div/span";
      var category = await s.page.$x(maincategoryselector);
      for (let link of category) {
        var categorystring = await s.page.evaluate(el => el.innerText, link);
        mainCategory.push(categorystring.trim());
        await link.click();
        const selector = "//*[@id='content_container_242249']/div/span[contains(text(),'" + categorystring.trim() + "')]/following-sibling::div//a";
        var category = await s.page.$x(selector);
        for (let link of category) {
          var categorystringinnr = await s.page.evaluate(el => el.innerText, link);
          var categoryurl = await s.page.evaluate(el => el.href, link);
          redirectUrl.push({ href: categoryurl, innerText: categorystringinnr, category: categorystring.trim() });
        }
      }
      for (let data of redirectUrl) {
        console.log("proceed#" + data.innerText);
        await s.page.goto(data.href, { timeout: 0 });
        const selector = "//*/a[contains(@href,'/courses/courses/') and not(contains(@href,'/courses/courses/browse')) and not(contains(text(),'click here'))]";
        var category = await s.page.$x(selector);
        for (let link of category) {
          var categorystring = await s.page.evaluate(el => el.innerText, link);
          var categoryurl = await s.page.evaluate(el => el.href, link);
          totalCourseList.push({ href: categoryurl, innerText: categorystring, category: data.category });
        }
      }
     
     
      fs.writeFileSync("./output/courselist_original.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
      console.log("pathwayyyyyyy------>",path);
      await fs.writeFileSync("./output/universityofnewengland_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/universityofnewengland_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file....');
      fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return uniqueUrl;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

}

module.exports = { ScrapeCourseList };
