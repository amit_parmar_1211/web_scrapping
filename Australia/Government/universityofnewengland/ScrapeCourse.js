const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            const newJSONdata = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_title': {
                            resJsonData.course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            resJsonData.course_discipline = category;
                            break;
                        }
                        case 'course_study_level': {

                            const course_study_level111 = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
                            console.log("coursestudylev-->", course_study_level111);
                            if (course_study_level111) {
                                resJsonData.course_study_level = course_study_level111;
                            }
                        }
                            break;

                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; let ieltsNumber = null;
                            var otherLngDict = null;
                            const ieltsMappingDict = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                            if (ieltsMappingDict) {
                                // let otherLngDict = [];  
                                for (let mapping of ieltsMappingDict) {
                                    console.log("##Mappins" + JSON.stringify(mapping.courses))
                                    const mymappdata = mapping.courses;
                                    if (mymappdata) {
                                        if (mymappdata.includes(resJsonData.course_title)) {
                                            otherLngDict = mapping;
                                            console.log("otherLngDict", otherLngDict);
                                        }
                                    }
                                }
                                console.log("ERRRRR");
                                if (!otherLngDict) {
                                    otherLngDict = ieltsMappingDict[ieltsMappingDict.length - 1];
                                }
                                console.log(funcName + 'otherLngDict = ' + JSON.stringify(otherLngDict));
                                if (otherLngDict) {
                                    if (otherLngDict.ielts) {
                                        ieltsDict.name = 'ielts academic';
                                        ieltsDict.description = otherLngDict.ielts;
                                        englishList.push(ieltsDict);
                                    }
                                    if (otherLngDict.pte) {
                                        pteDict.name = 'pte academic';
                                        pteDict.description = otherLngDict.pte;
                                        englishList.push(pteDict);
                                    }
                                    if (otherLngDict.toefl) {
                                        tofelDict.name = 'toefl ibt';
                                        tofelDict.description = otherLngDict.toefl;
                                        englishList.push(tofelDict);
                                    }
                                    if (otherLngDict.cae) {
                                        caeDict.name = 'cae';
                                        caeDict.description = otherLngDict.cae;
                                        englishList.push(caeDict);
                                    }
                                } else { // if (otherLngDict)
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = configs.propValueNotAvaialble;
                                    englishList.push(ieltsDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = configs.propValueNotAvaialble;
                                    englishList.push(pteDict);

                                    tofelDict.name = 'toefl ibt';
                                    tofelDict.description = configs.propValueNotAvaialble;
                                    englishList.push(tofelDict);

                                    caeDict.name = 'cae';
                                    caeDict.description = configs.propValueNotAvaialble;
                                    englishList.push(caeDict);
                                } // if (otherLngDict)
                            }
                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            var penglishList = [];
                            const potherLngDict = otherLngDict;
                            if (potherLngDict) {
                                var ieltsScore = "NA", ibtScore = "NA", pteScore = "NA", caeScore = "NA", cbtScore = "NA";
                                if (potherLngDict.ielts) {
                                    ieltsScore = await utils.giveMeNumber(potherLngDict.ielts);
                                    console.log("### IELTS data-->" + ieltsScore);
                                }
                                if (potherLngDict.toefl) {
                                    ibtScore = await utils.giveMeNumber(potherLngDict.toefl);
                                }
                                if (potherLngDict.pte) {
                                    pteScore = await utils.giveMeNumber(potherLngDict.pte);
                                }
                                if (potherLngDict.cae) {
                                    caeScore = await utils.giveMeNumber(potherLngDict.cae);
                                }
                                if (ieltsDict.description != null) {
                                    if (ieltsScore != "NA") {
                                        pieltsDict.name = 'ielts academic';
                                        pieltsDict.description = ieltsDict.description;
                                        pieltsDict.min = 0;
                                        pieltsDict.require = ieltsScore;
                                        pieltsDict.max = 9;
                                        pieltsDict.R = 0;
                                        pieltsDict.W = 0;
                                        pieltsDict.S = 0;
                                        pieltsDict.L = 0;
                                        pieltsDict.O = 0;
                                        penglishList.push(pieltsDict);
                                    }
                                }
                                if (tofelDict.description != null) {
                                    if (ibtScore != "NA") {
                                        pibtDict.name = 'toefl ibt';
                                        pibtDict.description = tofelDict.description;
                                        pibtDict.min = 0;
                                        pibtDict.require = ibtScore;
                                        pibtDict.max = 120;
                                        pibtDict.R = 0;
                                        pibtDict.W = 0;
                                        pibtDict.S = 0;
                                        pibtDict.L = 0;
                                        pibtDict.O = 0;
                                        penglishList.push(pibtDict);
                                    }
                                }
                                if (pteDict.description != null) {
                                    if (pteScore != "Na") {
                                        ppteDict.name = 'pte academic';
                                        ppteDict.description = pteDict.description;
                                        ppteDict.min = 0;
                                        ppteDict.require = pteScore;
                                        ppteDict.max = 90;
                                        ppteDict.R = 0;
                                        ppteDict.W = 0;
                                        ppteDict.S = 0;
                                        ppteDict.L = 0;
                                        ppteDict.O = 0;
                                        penglishList.push(ppteDict);
                                    }
                                }
                                if (caeDict.description != null) {
                                    if (caeScore != "NA") {
                                        pcaeDict.name = 'cae';
                                        pcaeDict.description = caeDict.description;
                                        pcaeDict.min = 80;
                                        pcaeDict.require = caeScore;
                                        pcaeDict.max = 230;
                                        pcaeDict.R = 0;
                                        pcaeDict.W = 0;
                                        pcaeDict.S = 0;
                                        pcaeDict.L = 0;
                                        pcaeDict.O = 0;
                                        penglishList.push(pcaeDict);
                                    }
                                }
                                //}
                            }
                            //}
                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }
                            ///progrssbar End
                            // academic requirement
                            var acadStud = resJsonData.course_study_level;
                            console.log("acadstudy-->", acadStud);
                            if (acadStud == 'Undergraduate') {
                                console.log("entered-->>1!!!!!")
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement_UnderGrad);
                                console.log("academicReq--->", academicReq);
                                //var acadStud = resJsonData.course_study_level;

                                //if (courseScrappedData.course_academic_requirement) {
                                // const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                // var acadStud = resJsonData.course_study_level;

                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = await utils.giveMeArray(academicReq.replace(/[\r\n\t]+/g, ';'), ";");
                                }
                            }
                            if (acadStud == 'Postgraduate') {
                                console.log("ente-->>!!!!!")
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement_PostGrad);


                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = await utils.giveMeArray(academicReq.replace(/[\r\n\t]+/g, ';'), ";");
                                }
                            }
                            if (acadStud == 'Research') {
                                console.log("RESEARCHED-->>!!!!!")
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement_Research);
                                console.log("academicReq7--->", academicReq);


                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = await utils.giveMeArray(academicReq.replace(/[\r\n\t]+/g, ';'), ";");
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }

                            var filedata = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                            courseAdminReq.academic_requirements_url = filedata["course_admission_english_more_details"];
                            courseAdminReq.english_requirements_url = filedata["english_more"];
                            courseAdminReq.entry_requirements_url = filedata["entry_requirements"];
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }

                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }

                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, durationPartTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            var partTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            //  const courseKeyValParttime = courseScrappedData.course_duration_part_time
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("filtered_duration_formated", filtered_duration_formated);
                                    console.log("courseDurationDisplayList2", courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {

                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime

                                    }
                                }
                            }

                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_campus_location': { // Location Launceston
                            var campLocationText; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);

                            const cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);

                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            let newcampus = [];
                            for (let campus of campLocationText) {
                                if (!newcampus.includes(campus)) {
                                    newcampus.push(campus);
                                    console.log("##Campuscampus-->" + campus)
                                }
                            }
                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            //   let valfainal =campLocationText.toString();
                            for (let campu_val of campLocationText) {
                                if (campu_val && campu_val.length > 0) {
                                    var campusedata = [];
                                    campu_val.forEach(element => {
                                        campusedata.push({
                                            "name": element,
                                            "code": cricos


                                        })
                                    });
                                    resJsonData.course_campus_location = campusedata;
                                    console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);
                                }
                                else {
                                    throw new Error("No campus location found.");
                                }
                            }
                            break;
                        }


                        // case 'course_cricos_code': {
                        //     const courseKeyVal = courseScrappedData.course_cricos_code;
                        //     let course_cricos_code = null;
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                     if (Array.isArray(selList) && selList.length > 0) {
                        //                         course_cricos_code = selList;
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     if (course_cricos_code) {
                        //         global.cricos_code = course_cricos_code[0];
                        //         var locations = resJsonData.course_campus_location;
                        //         var mycodes = [];
                        //         for (let location of locations) {
                        //             mycodes.push({
                        //                 location: location.name, code: course_cricos_code.
                        //             });
                        //         }
                        //         resJsonData.course_campus_location = mycodes;
                        //     }
                        //     break;
                        // }
                        case 'course_tuition_fees_international_student': {
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};
                            var myfiledat = {
                                "filepath": "../universityofnewengland/output/course_fee_detail.json",
                                "value": "global.cricos_code",
                                "comparekeycolumn": "cricos_code",
                                "returnkey": "year_fee"
                            }
                            var feesIntStudent;
                            if (resJsonData.course_title == "Doctor of Philosophy") {
                                let feesArrayInnovation = [];
                                let feesArrayRural = [];
                                let feesArrayClinical = [];
                                let feesArrayEducation = [];
                                let feesArrayHumanities = [];
                                var campus = resJsonData.course_campus_location;
                                console.log("CAMPUS LOCATION -->", campus)
                                for (let loc of campus) {
                                    feesArrayInnovation.push({
                                        name: loc.name, value: {
                                            international_student: [
                                                {
                                                    amount: 31600,
                                                    duration: "1",
                                                    unit: "year",
                                                    description: "31,600.00"

                                                }
                                            ],
                                            fee_duration_years: "1",
                                            currency: "AUD",
                                            //international_student_all_fees: []
                                        }
                                    });
                                    feesArrayClinical.push({
                                        name: loc.name, value: {
                                            international_student: [
                                                {
                                                    amount: 33856,
                                                    duration: "1",
                                                    unit: "year",
                                                    description: "33,856.00"
                                                }
                                            ],
                                            fee_duration_years: "1",
                                            currency: "AUD",
                                            //international_student_all_fees: []
                                        }
                                    });
                                    feesArrayEducation.push({
                                        name: loc.name, value: {
                                            international_student: [
                                                {
                                                    amount: 31600,
                                                    duration: "1",
                                                    unit: "year",
                                                    description: "31,600.00"

                                                }
                                            ],
                                            fee_duration_years: "1",
                                            currency: "AUD",
                                            // international_student_all_fees: []
                                        }
                                    });
                                    feesArrayHumanities.push({
                                        name: loc.name, value: {
                                            international_student: [
                                                {
                                                    amount: 28216,
                                                    duration: "1",
                                                    unit: "year",
                                                    description: "28,216.00"

                                                }
                                            ],
                                            fee_duration_years: "1",
                                            currency: "AUD",
                                            // international_student_all_fees: []
                                        }
                                    });
                                    feesArrayRural.push({
                                        name: loc.name, value: {
                                            international_student: [
                                                {
                                                    amount: 36120,
                                                    duration: "1",
                                                    unit: "year",

                                                    description: "36,120.00"

                                                }
                                            ],
                                            fee_duration_years: "1",
                                            currency: "AUD",
                                            //international_student_all_fees: []
                                        }
                                    });
                                }

                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Innovation)",
                                    course_discipline: [
                                        "Innovation"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "084916E",
                                    course_tuition_fee: {

                                        fees: feesArrayInnovation
                                    }
                                });
                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Agriculture)",
                                    course_discipline: [
                                        "Agriculture"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "012925M",
                                    course_tuition_fee: {

                                        fees: feesArrayInnovation
                                    }

                                });
                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Health)",
                                    course_discipline: [
                                        "Health"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "012927J",
                                    course_tuition_fee: {

                                        fees: feesArrayInnovation
                                    }

                                });
                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Behavioural Science, Social Studies)",
                                    course_discipline: [
                                        "Behavioural Science, Social Studies"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "066960F",
                                    course_tuition_fee: {

                                        fees: feesArrayInnovation
                                    }

                                });
                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Clinical Psychology)",
                                    course_discipline: [
                                        "Clinical Psychology"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "053917E",
                                    course_tuition_fee: {

                                        fees: feesArrayClinical
                                    }

                                });
                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Computing, Built Environment)",
                                    course_discipline: [
                                        "Computing, Built Environment"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "006338C",
                                    course_tuition_fee: {

                                        fees: feesArrayInnovation
                                    }

                                });
                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Education, Mathematics, Statistics)",
                                    course_discipline: [
                                        "Education, Mathematics, Statistics"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "006330M",
                                    course_tuition_fee: {

                                        fees: feesArrayEducation
                                    }

                                });
                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Humanities)",
                                    course_discipline: [
                                        "Humanities"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "006311C",
                                    course_tuition_fee: {

                                        fees: feesArrayHumanities
                                    }

                                });
                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Law, Accounting, Admin, Economics, Commerce)",
                                    course_discipline: [
                                        "Law, Accounting, Admin, Economics, Commerce"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "049930J",
                                    course_tuition_fee: {

                                        fees: feesArrayInnovation
                                    }

                                });
                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Nursing)",
                                    course_discipline: [
                                        "Nursing"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "014586J",
                                    course_tuition_fee: {

                                        fees: feesArrayInnovation
                                    }

                                });
                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Psychology, Foreign Languages, Performing Arts)",
                                    course_discipline: [
                                        "Psychology, Foreign Languages, Performing Arts"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "066961E",
                                    course_tuition_fee: {

                                        fees: feesArrayInnovation
                                    }

                                });
                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Rural Medicine)",
                                    course_discipline: [
                                        "Rural Medicine"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "066962D",
                                    course_tuition_fee: {

                                        fees: feesArrayRural
                                    }

                                });
                                newJSONdata.push({
                                    course_title: "Doctor of Philosophy (Science)",
                                    course_discipline: [
                                        "Science"
                                    ],
                                    course_study_level: "Research",
                                    course_cricos_code: "000431K",
                                    course_tuition_fee: {

                                        fees: feesArrayInnovation
                                    },
                                    course_campus_location: [
                                        {
                                            name: "ayushi",
                                            code: "000431K"
                                        }
                                    ]

                                });
                                console.log('NEWJSON --->', newJSONdata);
                            } else if (resJsonData.course_title == "Master of Philosophy") {
                                let feesArrayArts = [];
                                let feesArrayBusinessEconomics = [];
                                let feesArrayHealth = [];
                                var campus = resJsonData.course_campus_location;
                                for (let loc of campus) {
                                    feesArrayArts.push({
                                        name: loc.name, value: {
                                            international_student: [
                                                {
                                                    amount: 28216,
                                                    duration: "1",
                                                    unit: "year",

                                                    description: "28,216.00"

                                                }
                                            ],
                                            fee_duration_years: "1",
                                            currency: "AUD",

                                        }
                                    });
                                    feesArrayBusinessEconomics.push({
                                        name: loc, value: {
                                            international_student: [
                                                {
                                                    amount: 30096,
                                                    duration: "1",
                                                    unit: "year",
                                                    description: "30,096.00"

                                                }
                                            ],
                                            fee_duration_years: "1",
                                            currency: "AUD",

                                        }
                                    });
                                    feesArrayHealth.push({
                                        name: loc, value: {
                                            international_student: [
                                                {
                                                    amount: 31600,
                                                    duration: "1",
                                                    unit: "year",

                                                    description: "31,600.00"

                                                }
                                            ],
                                            fee_duration_years: "1",
                                            currency: "AUD",
                                            // international_student_all_fees: []
                                        }
                                    });
                                }
                                newJSONdata.push({
                                    course_title: "Master of Philosophy (Arts)",
                                    course_discipline: [
                                        "Arts"
                                    ],
                                    course_study_level: "Post Graduate",
                                    course_cricos_code: "000456A",
                                    course_tuition_fee: {
                                      
                                        fees: feesArrayArts
                                    }
                                });
                                newJSONdata.push({
                                    course_title: "Master of Philosophy (Business)",
                                    course_discipline: [
                                        "Business"
                                    ],
                                    course_study_level: "Post Graduate",
                                    course_cricos_code: "075533F",
                                    course_tuition_fee: {
                                        
                                        fees: feesArrayBusinessEconomics
                                    }
                                });
                                newJSONdata.push({
                                    course_title: "Master of Philosophy (Economics)",
                                    course_discipline: [
                                        "Economics"
                                    ],
                                    course_study_level: "Post Graduate",
                                    course_cricos_code: "012927J",
                                    course_tuition_fee: {
                                  
                                        fees: feesArrayBusinessEconomics
                                    }
                                });
                                newJSONdata.push({
                                    course_title: "Master of Philosophy (Health)",
                                    course_discipline: [
                                        "Health"
                                    ],
                                    course_study_level: "Post Graduate",
                                    course_cricos_code: "069343G",
                                    course_tuition_fee: {
                                       
                                        fees: feesArrayHealth
                                    }
                                });

                            } else {
                                // feesIntStudent = await format_functions.readanyfile(myfiledat);

                                const fee = JSON.parse(fs.readFileSync(appConfigs.feessssssss));
                                var durations = [];
                                //    const courseKeyVal = resJsonData.course_cricos_code;
                                const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);

                                console.log("pc.sssss------>>>>>>", JSON.stringify(courseKeyVal));
                                for (var durat of fee) {
                                    console.log("feee@@", durat.course_name)
                                    // console.log("durat_@@@@",splitCricos)
                                    if (courseKeyVal.includes(durat.cricos_code) == true) {
                                        console.log("durat.course_name------->", JSON.stringify(durat.cricos_code));
                                        durations.push(durat.year_fee);
                                        console.log("feee------->", JSON.stringify(durations));
                                        feesIntStudent = durations;
                                        console.log("feeeeee------>", feesIntStudent)
                                    }
                                }



                                //   console.log("Fee in student--->" + feesIntStudent);
                                const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                                if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                    const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                    console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                    const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                    const arrval = String(feesVal1).split('.');
                                    const feesVal = String(arrval[0]);
                                    console.log(funcName + 'feesVal = ' + feesVal);
                                    if (feesVal) {
                                        if (feesVal != "NA" && feesVal != "N A") {
                                            const regEx = /\d/g;
                                            let feesValNum = feesVal.match(regEx);
                                            if (feesValNum) {
                                                console.log(funcName + 'feesValNum = ' + feesValNum);
                                                feesValNum = feesValNum.join('');
                                                console.log(funcName + 'feesValNum = ' + feesValNum);
                                                let feesNumber = null;
                                                if (feesValNum.includes(',')) {
                                                    feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                                } else {
                                                    feesNumber = feesValNum;
                                                }
                                                console.log(funcName + 'feesNumber = ' + feesNumber);
                                                if (Number(feesNumber)) {
                                                    var feesint = {};
                                                    feesint.amount = Number(feesNumber);
                                                    feesint.duration = "1";
                                                    feesint.unit = "year";

                                                    feesint.description = String(feesIntStudent);

                                                    feesDict.international_student = feesint;
                                                }
                                            }
                                        }
                                        else {
                                            console.log("fee NA")
                                            var feesint = {};
                                            feesint.amount = 0;
                                            feesint.duration = "1";
                                            feesint.unit = "year";

                                            feesint.description = ""

                                            feesDict.international_student = feesint;
                                        }
                                    }
                                }
                                if (feesDict.international_student) {
                                    console.log("international_student");
                                    const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                    const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                    if (feesDuration && feesDuration.length > 0) {
                                        feesDict.fee_duration_years = feesDuration;
                                    }
                                    if (feesCurrency && feesCurrency.length > 0) {
                                        feesDict.currency = feesCurrency;
                                        var more_fee = [];
                                        //  feesDict.international_student_all_fees = more_fee;
                                    }
                                    if (feesDict) {
                                        var campus = resJsonData.course_campus_location;
                                        for (let loc of campus) {
                                            feesList.push({ name: loc.name, value: feesDict });
                                        }
                                    }
                                    if (feesList && feesList.length > 0) {
                                        courseTuitionFee.fees = feesList;
                                    }
                                    console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                    if (courseTuitionFee) {
                                        resJsonData.course_tuition_fee = courseTuitionFee;
                                    }
                                }
                            }
                            break;
                        }
                        case 'program_code': {
                            resJsonData.program_code = "";
                        }
                        case 'course_study_mode': { // Location Launceston
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }

                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };

                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

                            let resScholarshipJson = null;


                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }




                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }

                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)

                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;


                            break;
                        }

                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                resJsonData.application_fee = ""
                                break;
                            }
                        case 'course_intake': {
                            const courseIntakeDisplay = [];

                            var courseIntakeStr = null;
                            const courseKeyVal = courseScrappedData.course_intake;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList course_intake = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                courseIntakeStr = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var campus = resJsonData.course_campus_location;
                                var intakes = [];
                                console.log("Intake length-->" + courseIntakeStr.length);
                                console.log("Campus length-->" + campus.length);
                                var intakedetail = {};
                                var myintake = [];
                                var courseIntakeStrarr = [];
                                var myintake_data = JSON.parse(fs.readFileSync("./selectors/university_hard_coded_data.json"))[0];
                                courseIntakeStr.forEach(element => {
                                    courseIntakeStrarr.push(element.replace(/[\r\n\t ]+/g, ''));
                                });
                                console.log("courseIntakeStrarr-->" + JSON.stringify(courseIntakeStrarr))
                                if (courseIntakeStrarr.includes("Trimester1")) {
                                    console.log("Match--> t1")
                                    myintake.push(myintake_data["Trimester1"])
                                }
                                if (courseIntakeStrarr.includes("Trimester2")) {
                                    console.log("Match--> t2")
                                    myintake.push(myintake_data["Trimester2"])
                                }
                                if (courseIntakeStrarr.includes("Trimester3")) {
                                    console.log("Match--> t2")
                                    myintake.push(myintake_data["Trimester3"])
                                }
                                if (courseIntakeStrarr.includes("ResearchPeriod1") || courseIntakeStrarr.includes("ResearchPeriod2")) {
                                    myintake.push("anytime")
                                }
                                intakedetail.name = campus[0].name;
                                intakedetail.value = myintake;
                                intakes.push(intakedetail);
                                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                let formatedIntake = await format_functions.providemyintake(intakes, "mom", "");
                                console.log("Intakes --> ", JSON.stringify(formatedIntake));
                                var intakedata = {};
                                intakedata.intake = formatedIntake;
                                let valmonths11 = "";

                                intakedata.more_details = more_details;
                                resJsonData.course_intake = intakedata;
                            }
                            break;
                        }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            course_country = selList[0];
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            resJsonData.course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                                console.log("course_career_outcome--->", course_career_outcome);
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            }
                            break;
                        }


                    }
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)

            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            if (newJSONdata.length > 0) {
                for (let newData of newJSONdata) {
                    var NEWJSONSTRUCT = {}, structDict = [];
                    resJsonData.course_cricos_code = [];
                    // for (let cricosLocation of locations) {
                    //     resJsonData.course_cricos_code.push(cricosLocation + " - " + newData.course_cricos_code);
                    // }
                    for (let location of locations) {
                        NEWJSONSTRUCT.course_id = newData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                        NEWJSONSTRUCT.course_title = newData.course_title;
                        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                        NEWJSONSTRUCT.course_url = resJsonData.course_url;
                        console.log("newData.course_tuition_fee --> ", newData.course_tuition_fee);
                        console.log("AYushi -->", location.name);
                        let course_campus_location = {
                            name: location.name,
                            code: newData.course_cricos_code
                        }
                        NEWJSONSTRUCT.course_campus_location = [course_campus_location];
                        resJsonData.course_tuition_fee = newData.course_tuition_fee;
                        NEWJSONSTRUCT.course_tuition_fee = newData.course_tuition_fee.fees[0].international_student;
                        NEWJSONSTRUCT.currency = newData.course_tuition_fee.fees[0].currency;
                        NEWJSONSTRUCT.fee_duration_years = newData.course_tuition_fee.fees[0].fee_duration_years;
                        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                        resJsonData.course_discipline = newData.course_discipline;
                        var intakes = resJsonData.course_intake.intake;
                        var matchrec = [];
                        for (let dintake of intakes) {
                            if (location == dintake.name) {
                                matchrec = dintake.value;
                            }
                        }
                        if (matchrec.length > 0) {
                            NEWJSONSTRUCT.course_intake = matchrec[0];
                        }
                        else {
                            NEWJSONSTRUCT.course_intake = "";
                        }
                        for (let myfees of newData.course_tuition_fee.fees) {
                            if (myfees.name == location) {
                                NEWJSONSTRUCT.international_student_all_fees = myfees;
                            }
                        }
                        structDict.push(NEWJSONSTRUCT);
                        NEWJSONSTRUCT = {};
                    }
                    for (let location_wise_data of structDict) {
                        resJsonData.course_id = location_wise_data.course_location_id;
                        resJsonData.course_campus_location = location_wise_data.course_campus_location;
                        resJsonData.international_student = location_wise_data.international_student;
                        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                        console.log("Write file--->" + filelocation)
                        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                    }
                }

            }
            else {
                var NEWJSONSTRUCT = {}, structDict = [];
                for (let location of locations) {
                    NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                    NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                    NEWJSONSTRUCT.course_title = resJsonData.course_title;
                    NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                    NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                    NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                    NEWJSONSTRUCT.course_url = resJsonData.course_url;
                    NEWJSONSTRUCT.course_campus_location = location.name;
                    NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                    NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                    NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                    NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                    NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                    var intakes = resJsonData.course_intake.intake;
                    var matchrec = [];
                    for (let dintake of intakes) {
                        if (location == dintake.name) {
                            matchrec = dintake.value;
                        }
                    }
                    if (matchrec.length > 0) {
                        NEWJSONSTRUCT.course_intake = matchrec[0];
                    }
                    else {
                        NEWJSONSTRUCT.course_intake = "";
                    }
                    for (let myfees of resJsonData.course_tuition_fee.fees) {
                        if (myfees.name == location) {
                            NEWJSONSTRUCT.international_student_all_fees = myfees;
                        }
                    }
                    structDict.push(NEWJSONSTRUCT);
                    NEWJSONSTRUCT = {};
                }
                for (let location_wise_data of structDict) {
                    resJsonData.course_id = location_wise_data.course_location_id;

                    var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                    console.log("Write file--->" + filelocation)
                    fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                }
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }
    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, await s.page.url(), courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
