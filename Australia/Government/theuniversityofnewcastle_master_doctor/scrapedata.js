const puppeteer = require('puppeteer');
const fs = require("fs");
async function start() {
    const URL = "https://www.newcastle.edu.au/degrees#filter=intake_international";
    let browser = await puppeteer.launch({ headless: false }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    // let selector = "#modalTitle > button";

    // await page.waitFor(5000);
    // console.log("wait over");
    // var display = await page.$$(selector);
    // console.log("display variable");
    // await display[0].click();




    const categoryselectorUrl = "//*[@id='handbook-filters']/fieldset[7]/ul/li/label/input";
    const categoryselectorText = "//*[@id='handbook-filters']/fieldset[7]/ul/li/label/text()";
    var elementstring = "", elementValue = "", allcategory = [];

    const targetLinksCardsUrls = await page.$x(categoryselectorUrl);
    const targetLinksCardsText = await page.$x(categoryselectorText);

    var targetLinksCardsTotal = [];
    for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        elementstring = await page.evaluate(el => el.textContent, targetLinksCardsText[i]);
        elementValue = await page.evaluate(el => el.value, targetLinksCardsUrls[i]);
        targetLinksCardsTotal.push({ href: elementValue, innerText: elementstring });
    }
    // await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
    console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));


    // let linkselector = "//*[@id='handbook-filters']/fieldset[8]/ul/li/label[@style='opacity: 1;']/text()";
    let textselector = "//*[@id='handbook-filters']/fieldset[8]/ul/li/label[@style='opacity: 1;']/text()"
    var totalCourseList = [];
    for (let target of targetLinksCardsTotal) {
        // console.log("target.innerText -->", target.href);
        let checkBoxClick = '//label/input[@value = "' + target.href + '"]';
        console.log("checkBoxClick -->", checkBoxClick);
        var display = await page.$x(checkBoxClick);
        await display[0].click();

        const targetText = await page.$x(textselector);
        // await page.waitFor(5000);
        console.log("target.innerText -->", target.innerText);
        // console.log("#total link selectors---->" + targetLinks.length);
        for (var i = 0; i < targetText.length; i++) {
            var elementstring = await page.evaluate(el => el.textContent, targetText[i]);
            // const elementlink = await page.evaluate(el => el.href, targetLinks[i]);
            totalCourseList.push({ innerText: elementstring, category: target.innerText });
        }
    }
    console.log("totalCourseList -->", totalCourseList);
    fs.writeFileSync("jamescookuniversity_category_courselist.json", JSON.stringify(totalCourseList));
    await browser.close();

}
start();
