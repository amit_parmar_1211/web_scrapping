const fs = require('fs');
const awsUtil = require('./common/aws_utils');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    static titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    }
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, cricoscode, programcode) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            console.log('predifine value courseUrl :' + courseUrl);
            console.log('predifine value course_name :' + course_name);
            console.log('predifine value cricoscode :' + cricoscode);
            console.log('predifine value programcode :' + programcode);
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            const scrore_file = "./output/theuniversityofnewcastle_master_doctor_category.json";
                            let category_mapping = await JSON.parse(fs.readFileSync(scrore_file));
                            console.log("category_mapping -->", category_mapping);
                            // As of not we do not have way to get this field value
                            const courseKeyVal = courseScrappedData.course_discipline;
                            console.log('duration value for course :' + courseKeyVal);
                            var course_discipline = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict course_discipline = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict course_discipline = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList course_discipline = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_discipline = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("course_discipline -->", course_discipline);
                            if (course_discipline != null) {
                                let count = 0;
                                for (let category of category_mapping) {
                                    if (course_discipline.indexOf(category.faculty) > -1) {
                                        count++;
                                        resJsonData.course_discipline = category.category;
                                        break;
                                    }
                                }
                                if (count == 0) {
                                    for (let category of category_mapping) {
                                        if (category.faculty == 'Other') {
                                            count++;
                                            resJsonData.course_discipline = category.category;
                                            break;
                                        }
                                    }
                                }
                            } else {
                                // throw new Error('Error in course discipline');
                                for (let category of category_mapping) {
                                    if (category.faculty == 'Other') {
                                        count++;
                                        resJsonData.course_discipline = category.category;
                                        break;
                                    }
                                }
                            }

                            break;
                        }
                        case 'course_toefl_ielts_score': {
                            console.log('**************************************start course_toefl_ielts_score***************************************');
                            const courseAdminReq = {};
                            const englishList = [];
                            var ieltsDic = null;
                            const ieltsDict = {}; let ieltsNumber = null;
                            // english requirement
                            if (courseScrappedData.course_toefl_ielts_score) {
                                const ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                                console.log('Ielts score for date :' + ieltsScore);
                                if (ieltsScore && ieltsScore.length > 0) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsScore;
                                    ieltsDic = ieltsDict;
                                    englishList.push(ieltsDict);
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(ieltsScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ieltsNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                    }
                                }
                            }
                            ///progress bar start
                            const pieltsDict = {};
                            var penglishList = [];
                            if (ieltsDict) {
                                var ieltsScore = "";
                                if (ieltsDic.description) {
                                    ieltsScore = await utils.giveMeNumber(ieltsDic.description);
                                    console.log("### IELTS data-->" + ieltsScore);
                                }
                                // if (ieltsScore != "NA") {
                                //     pieltsDict.name = 'ielts academic';
                                //     var value = {};
                                //     value.min = 0;
                                //     value.require = Number(ieltsScore);
                                //     value.max = 9;
                                //     pieltsDict.value = value;
                                //     penglishList.push(pieltsDict);
                                // }
                                if (ieltsScore) {
                                    pieltsDict.name = 'ielts academic';
                                    pieltsDict.description = ieltsDic.description;
                                    pieltsDict.min = 0;
                                    pieltsDict.require = Number(ieltsScore);;
                                    pieltsDict.max = 9;
                                    pieltsDict.R = 0;
                                    pieltsDict.W = 0;
                                    pieltsDict.S = 0;
                                    pieltsDict.L = 0;
                                    pieltsDict.O = 0;
                                    penglishList.push(pieltsDict);
                                }



                            }
                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }
                            ///progrssbar End
                            // if (englishList && englishList.length > 0) {
                            //     courseAdminReq.english = englishList;
                            // }
                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                if (course_name.toLowerCase().indexOf('phd') > -1) {
                                    const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement_doctor);
                                    var academicDetails = [];
                                    if (academicReq && String(academicReq).length > 0) {
                                        academicDetails.push(academicReq);
                                        courseAdminReq.academic = academicDetails;
                                    }
                                } else if (course_name.toLowerCase().indexOf('m philosophy') > -1) {
                                    const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement_master);
                                    var academicDetails = [];
                                    if (academicReq && String(academicReq).length > 0) {
                                        academicDetails.push(academicReq);
                                        courseAdminReq.academic = academicDetails;
                                    }
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }
                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.course_admission_english_more_details;
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            // add english requirement 'english_more_details' link
                            const courseKeyValAcademic = courseScrappedData.course_admission_academic_more_details;
                            let resAcademicReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyValAcademic)) {
                                for (const rootEleDict of courseKeyValAcademic) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAcademicReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            // add english requirement 'english_more_details' link
                            const courseKeyValenglish_url = courseScrappedData.english_requirements_url;
                            let resEnglishReqMoreURLDetailsJson = null;
                            if (Array.isArray(courseKeyValenglish_url)) {
                                for (const rootEleDict of courseKeyValenglish_url) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreURLDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
                            }
                            console.log('**************************************END course_toefl_ielts_score***************************************');
                            break;
                        }
                        // case 'course_url': {
                        //     if (courseUrl && courseUrl.length > 0) {
                        //         resJsonData.course_url = courseUrl;
                        //     }
                        //     break;
                        // }
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));

                                            rescourse_url = selList;

                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;

                        }

                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };

                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

                            let resScholarshipJson = null;


                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }




                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }

                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)

                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;


                            break;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                resJsonData.application_fee = ""
                                break;
                            }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";

                            if (course_name.toLowerCase().indexOf('phd') > -1) {
                                const courseKeyVal = courseScrappedData.course_duration_full_time_doctor;
                                console.log('duration value for course :' + courseKeyVal);
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict course_duration_full_time_doctor = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict course_duration_full_time_doctor = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList course_duration_full_time_doctor = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    fullTimeText = selList[0];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (course_name.toLowerCase().indexOf('m philosophy') > -1) {
                                const courseKeyVal = courseScrappedData.course_duration_full_time_master;
                                console.log('duration value for course :' + courseKeyVal);
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict course_duration_full_time_master = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict course_duration_full_time_master = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList course_duration_full_time_master = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    fullTimeText = selList[0];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("fullTimeText --> ", fullTimeText);

                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);

                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;

                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;



                                    }
                                }
                            }
                            break;
                        }

                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            console.log('******************************Start Tution fee****************************');
                            const courseKeyVal = cricoscode;
                            let course_cricos_code = null;
                            course_cricos_code = cricoscode.toString();
                            console.log('Cricos code :' + course_cricos_code);
                            const cricvalue = course_cricos_code;
                            console.log('****************Program code************************');
                            console.log('Program code :')
                            const rootElementDictProList = programcode;
                            console.log('progrma code :' + JSON.stringify(rootElementDictProList));
                            const progvalue = programcode;
                            console.log('progvalue ' + progvalue);
                            console.log('****************************End Progrma code***************************');

                            var finalfeesInt = '';
                            const cricosecode = cricvalue.toString();
                            const programcodeval = progvalue.toString();
                            const fixedFeesDict = await utils.getValueFromTutionfeeJsonFile();
                            var matchfee = false;
                            for (var i = 0; i < fixedFeesDict.length; i++) {
                                if (fixedFeesDict[i].cricos_code == cricosecode.trim() && fixedFeesDict[i].program_code == programcodeval.trim()) {
                                    console.log('fixedFeesDict saddam :' + fixedFeesDict[i].cricos_code + ' Amount :' + fixedFeesDict[i].course_fee);
                                    finalfeesInt = fixedFeesDict[i].course_fee;
                                    matchfee = true;
                                }
                            }
                            if (!matchfee) {
                                for (var i = 0; i < fixedFeesDict.length; i++) {
                                    if (fixedFeesDict[i].program_code == programcodeval.trim()) {
                                        console.log('fixedFeesDict saddam :' + fixedFeesDict[i].cricos_code + ' Amount :' + fixedFeesDict[i].course_fee);
                                        finalfeesInt = fixedFeesDict[i].course_fee;
                                    }
                                }
                            }

                            const feesIntStudent = finalfeesInt;
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            // if (feeYear && feeYear.length > 0) {
                            //     courseTuitionFee.year = feeYear;
                            // }
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal custome = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        // if (Number(feesNumber)) {
                                        //     feesDict.international_student = Number(feesNumber);
                                        // }


                                        var durationfee = 1;
                                        var unitfee = "Year";
                                        var isfulltimeflag = true;

                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: durationfee,
                                                unit: unitfee,

                                                description: feesIntStudent

                                            });
                                        }
                                    }
                                }
                            }

                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesIntStudent.length > 0) { // extract only digits
                                    console.log("international_student" + feesIntStudent)
                                    const courseKeyVal = courseScrappedData.course_tuition_fees_international_student_more_details;
                                    console.log('value of feee :' + courseKeyVal);
                                    if (Array.isArray(courseKeyVal)) {
                                        for (const rootEleDict of courseKeyVal) {
                                            console.log(funcName + '\n\r rootEleDict fee more= ' + JSON.stringify(rootEleDict));
                                            const elementsList = rootEleDict.elements;
                                            for (const eleDict of elementsList) {
                                                console.log(funcName + '\n\r eleDict fee more= ' + JSON.stringify(eleDict));
                                                const selectorsList = eleDict.selectors;
                                                for (const selList of selectorsList) {
                                                    console.log(funcName + '\n\r selList fee more= ' + JSON.stringify(selList));
                                                    if (Array.isArray(selList) && selList.length > 0) {
                                                        //    feesDict.international_student_all_fees = selList[0];
                                                        console.log('course fee more details :' + selList[0]);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    //  resJsonData.course_tuition_fee_amount = [];
                                }
                                console.log('******************************END Tution fee****************************');
                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }
                        //         case 'course_campus_location': { // Location Launceston
                        //             const campLocationText = await Course.extractLocationValueFromScrappedElement(courseScrappedData.course_campus_location);
                        //             console.log(funcName + 'campLocationText = ' + campLocationText);
                        //             if (campLocationText && campLocationText.length > 0) {
                        //                 var campLocationValTrimmed = String(campLocationText).trim();
                        //                 console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                        //                 if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                        //                     resJsonData.course_campus_location = await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ''), ',');
                        //                     // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                        //                 }
                        //             } // if (campLocationText && campLocationText.length > 0)

                        // if (campLocationText && campLocationText.length > 0) {
                        //     var campusedata = [];

                        //     campLocationText.forEach(element => {
                        //       campusedata.push({
                        //         "name": element,
                        //         "city": "",
                        //         "state": "",
                        //         "country": "",
                        //         "iscurrent": false
                        //       })
                        //     });
                        //     resJsonData.course_campus_location = campusedata;
                        //     console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

                        //   }

                        // else {
                        //   throw new Error("No campus location found.");
                        // }
                        //             break;
                        //         }
                        // case 'course_cricos_code': {
                        //     const courseCodeVal = cricoscode;
                        //     console.log('cricos code :' + courseCodeVal);
                        //     let course_cricos_code = [];
                        //     course_cricos_code.push(courseCodeVal)
                        //     console.log('course_cricos_code cricos code :' + course_cricos_code);
                        //     if (course_cricos_code) {
                        //         global.cricos_code = course_cricos_code[0];
                        //         var locations = resJsonData.course_campus_location;
                        //         var mycodes = [];
                        //         for (let location of locations) {
                        //             //  mycodes.push(location + " - " + course_cricos_code);
                        //             mycodes.push({
                        //                 location: location.name, code: course_cricos_code.toString(), iscurrent: false
                        //             });
                        //         }
                        //         resJsonData.course_cricos_code = mycodes;
                        //     }
                        //     else {
                        //         throw new Error("Cricos code not found");
                        //     }
                        //     break;
                        // }

                        case 'course_campus_location': { // Location Launceston
                            console.log('course_campus_location matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            const Cricos = cricoscode;
                            console.log("Cricosss-------->", Cricos)

                            console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedSelStr = [];
                            for (const rootEleDict of rootElementDictList) {
                                console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                // let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        if (selList && selList.length > 0) {
                                            console.log('course_campus_location selList = ' + JSON.stringify(selList));
                                            for (var selItem of selList) {
                                                selItem = selItem.replace(/[\r\n\t ]+/g, ' ').split(",");
                                                for (let sel of selItem) {
                                                    if (sel != "external" && sel != "External" && sel != "Online" && sel != "online") {
                                                        concatnatedSelStr.push(sel);
                                                    }
                                                }
                                            } // selList
                                            console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);

                                        }
                                    } // selectorsList                  
                                } // elementsList                
                            } // rootElementDictList 

                            // add only if it has valid value    
                            for (var i = 0; i < concatnatedSelStr.length; i++) {

                                // if(concatnatedSelStr.includes('UON')){
                                //   concatnatedSelStr= concatnatedSelStr.replace('UON','');
                                // console.log("UNIK",concatnatedSelStr);
                                // }



                                if (concatnatedSelStr && concatnatedSelStr.length > 0) {
                                    var campusedata = [];

                                    concatnatedSelStr.forEach(element => {
                                        campusedata.push({
                                            "name": element,
                                            "code": Cricos,

                                        })
                                    });
                                    resJsonData.course_campus_location = campusedata;
                                    console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

                                }

                                else {
                                    throw new Error("No campus location found.");
                                }
                                break;
                            }
                        }
                        case 'course_intake_url': {
                            //console.log('************************Start course_intake_url************************');
                            // var courseintakeUrl = [];
                            // var intakeurl = "";
                            // const courseKeyVal = courseScrappedData.course_intake_url;
                            // console.log('duration value for course :' + courseKeyVal);
                            // if (Array.isArray(courseKeyVal)) {
                            //     for (const rootEleDict of courseKeyVal) {
                            //         console.log(funcName + '\n\r rootEleDict course_intake_url = ' + JSON.stringify(rootEleDict));
                            //         const elementsList = rootEleDict.elements;
                            //         for (const eleDict of elementsList) {
                            //             console.log(funcName + '\n\r eleDict course_intake_url = ' + JSON.stringify(eleDict));
                            //             const selectorsList = eleDict.selectors;
                            //             for (const selList of selectorsList) {
                            //                 console.log(funcName + '\n\r selList course_intake_url = ' + JSON.stringify(selList));
                            //                 if (Array.isArray(selList) && selList.length > 0) {
                            //                     intakeurl = selList[0];
                            //                 }
                            //             }
                            //         }
                            //     }
                            // }
                            // console.log('intake url :' + intakeurl);
                            // courseintakeUrl.push(intakeurl);
                            // console.log('intake length :' + courseintakeUrl.length);
                            // if (intakeurl != "" && intakeurl != null) {
                            //     resJsonData.course_intake_url = await utils.giveMe                            // console.log('************************End course_intake_url************************');
                            break;
                        }

                        case 'course_study_mode':
                            { // Location Launceston
                                resJsonData.course_study_mode = "On campus";

                                // const coursestudymodeText = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_mode);
                                // console.log(funcName + 'coursestudymodeText = ' + coursestudymodeText);
                                // if (coursestudymodeText && coursestudymodeText.length > 0) {
                                //     var coursestudymodeValTrimmed = String(coursestudymodeText).trim();
                                //     console.log('***********************************strat course study mode****************************');
                                //     console.log(funcName + 'coursestudymodeValTrimmed = ' + coursestudymodeValTrimmed);
                                //     console.log('***********************************END course study mode********************************');
                                //     coursestudymodeValTrimmed = "On campus";
                                //     if (coursestudymodeValTrimmed && coursestudymodeValTrimmed.length > 0) {
                                //         //resJsonData.course_study_mode = coursestudymodeText;
                                //         resJsonData.course_study_mode = await utils.giveMeArray(coursestudymodeValTrimmed.replace(/[\r\n\t]+/g, ' '), ',');
                                //     }
                                // }
                                break;
                            }
                        case 'course_intake': {
                            console.log('*******************************course_intake start****************************');
                            var courseIntakeStr = [];
                            // existing intake value
                            var intakedate = '';
                            const courseKeyVal = courseScrappedData.course_intake;
                            console.log('course intake display :' + courseKeyVal);
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict course_intake = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict course_intake = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList course_intake = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                intakedate = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            var splitintake = intakedate.split(';');
                            var intakedate = "";
                            for (var s = 0; s < splitintake.length; s++) {
                                //courseIntakeStr.push(splitintake[s]);
                                intakedate += splitintake[s] + ";";
                            }
                            intakedate = intakedate.slice(0, -1);
                            courseIntakeStr.push(intakedate);

                            console.log("intakes" + JSON.stringify(courseIntakeStr));
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var campus = resJsonData.course_campus_location;
                                console.log('Campuse location :' + campus);
                                var intakes = [];
                                console.log("Intake length-->" + courseIntakeStr.length);
                                console.log("Campus length-->" + campus.length);
                                for (var camcount = 0; camcount < campus.length; camcount++) {
                                    for (var count = 0; count < courseIntakeStr.length; count++) {
                                        console.log("intake -->" + count)
                                        var intakedetail = {};
                                        //intakedetail.campus = campus[count];
                                        intakedetail.name = campus[camcount].name;
                                        intakedetail.value = await utils.giveMeArray(courseIntakeStr[count].replace(/[\r\n\t ]+/g, ' '), ";");
                                        intakes.push(intakedetail);
                                    }
                                }
                                let formatIntake = await format_functions.providemyintake(intakes, "mom", "");

                                var intakedata = {};
                                intakedata.intake = formatIntake;
                                intakedata.more_details = await utils.giveMeArray(courseUrl.replace(/[\r\n\t]+/g, ';'), ';');
                                console.log('more_details@@ intake :', intakedata.more_details)
                                resJsonData.course_intake = intakedata;
                                intakedata.more_details = String(intakedata.more_details);

                                //////////////// Start Course Intake ///////////////////////
                                var intakeurl = "";
                                const courseKeyVal = courseScrappedData.course_intake_url;
                                console.log('duration value for course :' + courseKeyVal);
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict course_intake_url = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict course_intake_url = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList course_intake_url = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    intakeurl = selList[0];
                                                }
                                            }
                                        }
                                    }
                                }
                                //////////////// End Course Intake ///////////////////////
                                intakedata.more_details = await utils.giveMeArray(intakeurl.replace(/[\r\n\t]+/g, ';'), ';');
                                console.log('more_details@@ intake :', intakedata.more_details)
                                resJsonData.course_intake = intakedata;
                                intakedata.more_details = String(intakedata.more_details);
                            } // if (courseIntakeStr && courseIntakeStr.length > 0)
                            else {
                                throw new Error('Intake not found');
                                // var campus = resJsonData.course_campus_location;
                                // console.log('Campuse location :' + campus);
                                // var intakes = [];
                                // var Intakenotmention = [];
                                // for (var camcount = 0; camcount < campus.length; camcount++) {
                                //     var intakedetail = {};
                                //     intakedetail.name = campus[camcount].name;
                                //     intakedetail.value = Intakenotmention;
                                //     intakes.push(intakedetail);
                                // }
                                // var intakedata = {};
                                // intakedata.intake = intakes;
                                // intakedata.more_details = await utils.giveMeArray(courseUrl.replace(/[\r\n\t]+/g, ';'), ';');
                                // resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');

                            }
                            console.log('*******************************course_intake end*****************************');
                            break;
                        }
                        case 'course_outline': {
                            console.log('***********************************Start course_outline**********************************');
                            const MAX_SUBJECTS_COUNT = 20;
                            const courseKeyVal = courseScrappedData.course_outline;
                            const resOutlineList = []; // const studyUnitsDict = {};
                            const concatSelectorsList = [];
                            if (Array.isArray(courseKeyVal)) {
                                let concatRootElementsList = [];
                                for (const rootEleDict of courseKeyVal) {
                                    let concatElementsList = [];
                                    const elementsList = rootEleDict.elements;
                                    console.log(funcName + 'elementsList course_outline = ' + JSON.stringify(elementsList));
                                    for (const eleDict of elementsList) {
                                        const selectorsList = eleDict.selectors;
                                        console.log(funcName + 'selectorsList course_outline = ' + JSON.stringify(selectorsList));
                                        for (const selList of selectorsList) {
                                            console.log(funcName + 'selList course_outline = ' + JSON.stringify(selList));
                                            console.log('for loop outline length :' + selList.length);
                                            for (var k = 0; k < selList.length; k++) {
                                                concatSelectorsList.push(selList[k]);
                                            }
                                            console.log('concatSellist outline :' + concatSelectorsList);
                                        } // for (const selList of selectorsList)
                                        concatElementsList = concatElementsList.concat(concatSelectorsList);
                                        console.log(funcName + 'concatElementsList course_outline= ' + JSON.stringify(concatElementsList));
                                    } // for (const eleDict of elementsList)
                                    concatRootElementsList = concatRootElementsList.concat(concatElementsList);
                                } // for
                            } // if (Array.isArray(courseKeyVal))
                            console.log(funcName + 'concatSelectorsList.length  course_outline= ' + concatSelectorsList.length);
                            console.log(funcName + 'concatSelectorsList  course_outline= ' + JSON.stringify(concatSelectorsList));
                            if (concatSelectorsList && concatSelectorsList.length > 0) {
                                let concatnatedSubList = [];
                                for (const selList of concatSelectorsList) {
                                    concatnatedSubList = concatnatedSubList.concat(selList);
                                } // for
                                // keep only unique subjects and remove any duplicates
                                for (const sub of concatnatedSubList) {
                                    if (!resOutlineList.includes(sub)) {
                                        resOutlineList.push(sub);
                                    }
                                }
                                console.log(funcName + 'concatnatedSubList course_outline = ' + JSON.stringify(concatnatedSubList));
                            }
                            console.log(funcName + 'resOutlineList.length course_outline = ' + resOutlineList.length);
                            if (resOutlineList && resOutlineList.length > 0) {
                                resJsonData.course_outline = {};
                                let subjectsList = [];
                                if (resOutlineList.length > MAX_SUBJECTS_COUNT) {
                                    console.log(funcName + 'taking only first ' + MAX_SUBJECTS_COUNT + ' subjects only....');
                                    subjectsList = resOutlineList.slice(0, MAX_SUBJECTS_COUNT);
                                } else {
                                    subjectsList = resOutlineList;
                                }
                                console.log(funcName + 'subjectsList.length course_outline = ' + subjectsList.length);
                                console.log(funcName + 'subjectsList course_outline = ' + JSON.stringify(subjectsList));

                                resJsonData.course_outline.study_units = subjectsList;
                                resJsonData.course_outline.more_details = resJsonData.course_url;
                            }
                            console.log('***********************************END course_outline**********************************');
                            break;
                        }
                        // case 'course_scholarship': {
                        //     console.log('***********************************Start course_scholarship**********************************');
                        //     const courseKeyVal = courseScrappedData.course_scholarship;
                        //     let resScholarshipJson = null;
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                     if (Array.isArray(selList) && selList.length > 0) {
                        //                         resScholarshipJson = selList[0];
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     if (resScholarshipJson) {
                        //         resJsonData.course_scholarship = resScholarshipJson;
                        //     }
                        //     console.log('***********************************END course_scholarship**********************************');
                        //     break;
                        // }
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                        case 'course_accomodation_cost': {
                            const courseKeyVal = courseScrappedData.course_accomodation_cost;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_accomodation_cost = resAccomodationCostJson;
                            }
                            break;
                        }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_country = resAccomodationCostJson;
                            }
                            break;
                        }
                        case 'course_overview': {


                            var courseKeyVal = courseScrappedData.course_overview;
                            var overview = "https://www.newcastle.edu.au/faculty"
                            // console.log("resAccomodationCostJson@@@", courseKeyVal);
                            // let resAccomodationCostJson = null;
                            // if (Array.isArray(courseKeyVal)) {
                            //     for (const rootEleDict of courseKeyVal) {
                            //         console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                            //         const elementsList = rootEleDict.elements;
                            //         for (const eleDict of elementsList) {
                            //             console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                            //             const selectorsList = eleDict.selectors;
                            //             for (const selList of selectorsList) {
                            //                 console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                            //                 if (Array.isArray(selList) && selList.length > 0) {
                            //                     resAccomodationCostJson = selList[0];
                            //                     console.log("resAccomodationCostJson@@@", resAccomodationCostJson);
                            //                 }
                            //             }
                            //         }
                            //     }
                            // }
                            // if (resAccomodationCostJson) {
                            resJsonData.course_overview = overview;
                            //     console.log("resAccomodationCostJson@@@", resAccomodationCostJson);
                            // } else {
                            //     resJsonData.course_overview = "";
                            // }

                            break;
                        }
                        case 'course_career_outcome': {
                            console.log(funcName + 'matched case course_career_outcome: ' + key);
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            var course_career_outcome = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            } // rootElementDictList
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            }
                            else {
                                resJsonData.course_career_outcome = course_career_outcome;
                            }
                            break;
                        }

                        case 'program_code': {
                            console.log(funcName + 'matched case program_code: ' + key);
                            const courseProgramKeyVal = programcode;
                            var program_code = [];
                            program_code.push(courseProgramKeyVal);
                            console.log('program code value :' + program_code);
                            for (let program_val of program_code) {
                                resJsonData.program_code = program_val;
                            }


                            //resJsonData.program_code = program_code;
                            break;
                        }
                        case 'course_title': {
                            console.log(funcName + 'matched case course_title: ' + course_name);
                            course_name = this.titleCase(course_name);
                            resJsonData.course_title = course_name;
                            console.log('course title value :' + resJsonData.course_title);
                            break;
                        }

                        case 'course_study_level':
                            { // Location Launceston
                                resJsonData.course_study_level = "Research";

                                break;
                            }
                        // case 'course_study_level': {
                        //     //const cTitle = resJsonData.course_title;
                        //      let cStudyLevel = null;
                        //     if (cStudyLevel) {
                        //         cStudyLevel = "Research";
                        //     }
                        //     else {
                        //         throw new Error("Study Level not Found");
                        //     }

                        //     // let cStudyLevel = null;
                        //     // console.log(funcName + 'course_title course_study_level = ' + cTitle);
                        //     // if (cTitle) {
                        //     //     if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || String(cTitle).toLowerCase().indexOf('advanced diploma') > -1 || String(cTitle).toLowerCase().indexOf('diploma') > -1) {
                        //     //         cStudyLevel = 'Under Graduate';
                        //     //     }
                        //     //     else if (String(cTitle).toLowerCase().indexOf('master by research') > -1 || String(cTitle).toLowerCase().indexOf('master by researchs') > -1 || String(cTitle).toLowerCase().indexOf('master research') > -1 || String(cTitle).toLowerCase().indexOf('master by researchs') > -1) {
                        //     //         cStudyLevel = 'Research';
                        //     //     }
                        //     //     else if (String(cTitle).toLowerCase().indexOf('graduate') > -1 || String(cTitle).toLowerCase().indexOf('graduates') > -1) {
                        //     //         cStudyLevel = 'Post Graduate';
                        //     //     }
                        //     //     else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1) {
                        //     //         cStudyLevel = 'Post Graduate';
                        //     //     }
                        //     //     else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1 || String(cTitle).toLowerCase().indexOf('m philosophy') > -1) {
                        //     //         cStudyLevel = 'Research';
                        //     //     }
                        //     //     else if (String(cTitle).toLowerCase().indexOf('research') > -1 || String(cTitle).toLowerCase().indexOf('researchs') > -1) {
                        //     //         cStudyLevel = 'Research';
                        //     //     }
                        //     //     else {
                        //     //         cStudyLevel = 'Vocational / Other';
                        //     //     }
                        //     //     if (cStudyLevel) {
                        //     //         resJsonData.course_study_level = cStudyLevel;
                        //     //     }
                        //     // }
                        //     break;
                        // }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            ////start genrating new file location wise
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;

                // resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            Scrape.validateParams([courseDict]);





            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href;
            const courseTitle = courseDict.innerText;
            const courseCricosCode = courseDict.cricoscode;
            const courseProgramCode = courseDict.programcode;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            console.log(funcName + 'courseCricosCode = ' + courseCricosCode);
            console.log(funcName + 'courseProgramCode = ' + courseProgramCode);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.cricoscode, courseDict.programcode);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };