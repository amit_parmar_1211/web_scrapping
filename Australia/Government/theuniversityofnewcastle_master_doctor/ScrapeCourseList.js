const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      const textselector = "//table/tbody/tr/td[2]/a/parent::td/following-sibling::td[not(contains(text(),'* not available to international students'))]/../td[2]/a";
      const linkselector = "//table/tbody/tr/td[2]/a/parent::td/following-sibling::td[not(contains(text(),'* not available to international students'))]/../td[2]/a";
      const cricoscode = "//table/tbody/tr/td[2]/a/parent::td/following-sibling::td[not(contains(text(),'* not available to international students'))]";
      const programcode = "//table/tbody/tr/td[2]/a/parent::td/following-sibling::td[not(contains(text(),'* not available to international students'))]/../td[1]";

      //scrape university courselist
      const targetLinks = await page.$x(linkselector);
      const targetText = await page.$x(textselector);
      const targetCricoscode = await page.$x(cricoscode);
      const targetProgramcode = await page.$x(programcode);

      var totalCourseList = [];
      console.log("#total link selectors---->" + targetLinks.length);
      for (var i = 0; i < targetLinks.length; i++) {
        console.log("#Proceed number of courses---->" + i);
        var elementstring = await page.evaluate(el => el.innerText, targetText[i]);
        const elementlink = await page.evaluate(el => el.href, targetLinks[i]);
        const elementCricoscode = await page.evaluate(el => el.innerText, targetCricoscode[i]);
        const elementProgramcode = await page.evaluate(el => el.innerText, targetProgramcode[i]);
        totalCourseList.push({ href: elementlink, innerText: elementstring, cricoscode: elementCricoscode, programcode: elementProgramcode });
      }

      console.log(funcName + 'writing courseList to file....');
      await fs.writeFileSync("./output/theuniversityofnewcastle_courselist.json", JSON.stringify(totalCourseList));

      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async scrapeCategory(page) {
    const URL = "https://www.newcastle.edu.au/degrees#filter=intake_international";
    await page.goto(URL, { timeout: 0 });

    const categoryselectorUrl = "//*[@id='handbook-filters']/fieldset[7]/ul/li/label/input";
    const categoryselectorText = "//*[@id='handbook-filters']/fieldset[7]/ul/li/label/text()";
    var elementstring = "", elementValue = "", allcategory = [];

    const targetLinksCardsUrls = await page.$x(categoryselectorUrl);
    const targetLinksCardsText = await page.$x(categoryselectorText);

    var targetLinksCardsTotal = [];
    for (let i = 0; i < targetLinksCardsUrls.length; i++) {
      elementstring = await page.evaluate(el => el.textContent, targetLinksCardsText[i]);
      elementValue = await page.evaluate(el => el.value, targetLinksCardsUrls[i]);
      targetLinksCardsTotal.push({ href: elementValue, innerText: elementstring });
    }
    // await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
    console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));


    // let linkselector = "//*[@id='handbook-filters']/fieldset[8]/ul/li/label[@style='opacity: 1;']/text()";
    let textselector = "//*[@id='handbook-filters']/fieldset[8]/ul/li/label[@style='opacity: 1;']/text()"
    var totalCourseList = [];
    for (let target of targetLinksCardsTotal) {
      // console.log("target.innerText -->", target.href);
      let checkBoxClick = '//label/input[@value = "' + target.href + '"]';
      console.log("checkBoxClick -->", checkBoxClick);
      var display = await page.$x(checkBoxClick);
      await display[0].click();
      await page.waitFor(2000);
      const targetText = await page.$x(textselector);
      console.log("target.innerText -->", targetText.length);
      // console.log("#total link selectors---->" + targetLinks.length);
      for (var i = 0; i < targetText.length; i++) {
        var elementstring = await page.evaluate(el => el.textContent, targetText[i]);
        totalCourseList.push({ faculty: target.innerText, category: elementstring });
      }
      await page.waitFor(2000);
      await display[0].click();
    }
    // console.log("totalCourseList -->", totalCourseList);
    // fs.writeFileSync("theuniversityofnewcastle_master_doctor_category_courselist.json", JSON.stringify(totalCourseList));
    let uniqueUrl = [];
    //unique url from the courselist file
    for (let i = 0; i < totalCourseList.length; i++) {
      let cnt = 0;
      if (uniqueUrl.length > 0) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (totalCourseList[i].faculty == uniqueUrl[j].faculty) {
            cnt = 0;
            break;
          } else {
            cnt++;
          }
        }
        if (cnt > 0) {
          uniqueUrl.push({ faculty: totalCourseList[i].faculty, category: [] });
        }
      } else {
        uniqueUrl.push({ faculty: totalCourseList[i].faculty, category: [] });
      }
    }


    // await fs.writeFileSync("./output/theuniversityofnewcastle_master_doctor_unique_courselist.json", JSON.stringify(uniqueUrl));
    //based on unique urls mapping of categories
    for (let i = 0; i < totalCourseList.length; i++) {
      for (let j = 0; j < uniqueUrl.length; j++) {
        if (uniqueUrl[j].faculty == totalCourseList[i].faculty) {
          if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

          } else {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }

        }
      }
    }

    console.log("totalCourseList -->", uniqueUrl);
    await fs.writeFileSync("./output/theuniversityofnewcastle_master_doctor_category.json", JSON.stringify(uniqueUrl));

  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      //await this.scrapeCategory(s.page);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }
      //return totalCourseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // refers to https://futurestudents.csu.edu.au/courses/all
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
