const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {

  async scrapeCourseListCategorywise() {
    const funcName = 'scrapeCourseListCategorywise ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];
      await s.setupNewBrowserPage("https://www.anu.edu.au/study/study-options");
      console.log('scrape course category wise list');




      const categoryselectorUrl = "#node-110393 > div > div > div:nth-child(4) > div > div.panel-pane.pane-custom.pane-5 > div > div.show-rsp3.show-rsp4 > div > a";
      const categoryselectorText = "#node-110393 > div > div > div:nth-child(4) > div > div.panel-pane.pane-custom.pane-5 > div > div.show-rsp3.show-rsp4 > div > a> img";
      var elementstring = "", elementhref = "", allcategory = [];

      const targetLinksCardsUrls = await s.page.$$(categoryselectorUrl);
      const targetLinksCardsText = await s.page.$$(categoryselectorText);

      var targetLinksCardsTotal = [];
      for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        elementstring = await s.page.evaluate(el => el.alt, targetLinksCardsText[i]);
        elementhref = await s.page.evaluate(el => el.href, targetLinksCardsUrls[i]);
        targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
      }
      // await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
      console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));

      let clickCategories = ".degree-builder-by-areas-of-interest.degree-builder-by-areas-of-interest--stage-two.degree-builder-by-areas-of-interest--visible-panel > div.areas-of-interest__choose-topics > div > ul > li > a";
      var totalCourseList = [];
      for (let target of targetLinksCardsTotal) {
        await s.page.goto(target.href, { timeout: 0 });


        const targetLinksCat = await s.page.$$(clickCategories);
        for (let cat of targetLinksCat) {
          await cat.click();
        }
        await s.page.waitFor(5000);
        let linkSelector = "//div[contains(@class,'degree-builder-by-areas-of-interest--visible-panel')]//div[@class='areas-of-interest__matching-degrees-results clearfix']//ul/li/a";
        let textSelector = "//div[contains(@class,'degree-builder-by-areas-of-interest--visible-panel')]//div[@class='areas-of-interest__matching-degrees-results clearfix']//ul/li/a/em";
        const targetText = await s.page.$x(textSelector);
        const targetLinks = await s.page.$x(linkSelector);

        console.log("target.innerText -->", target.innerText);
        console.log("#total link selectors---->" + targetLinks.length);
        for (var i = 0; i < targetLinks.length; i++) {
          var elementstring = await s.page.evaluate(el => el.innerText, targetText[i]);
          const elementlink = await s.page.evaluate(el => el.href, targetLinks[i]);
          totalCourseList.push({ href: elementlink, innerText: elementstring, category: target.innerText });
        }
      }
      console.log("totalCourseList -->", totalCourseList);

      await fs.writeFileSync("./course_list/main_category_courses.json", JSON.stringify(targetLinksCardsTotal))
      await fs.writeFileSync("./course_list/australiannationaluniversity_original_courselist.json", JSON.stringify(totalCourseList));

      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
      await fs.writeFileSync("./course_list/australiannationaluniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./course_list/australiannationaluniversity_courselist.json", JSON.stringify(uniqueUrl));

      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
} // class
module.exports = { ScrapeCourseList };