const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    static async formatOutput(category, courseScrappedData, courseUrl, studyLevel) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = category;
                            break;
                        }
                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            title = title.replace('/', '/ ');
                            console.log("splitStr@@@2" + title);
                            var ctitle = format_functions.titleCase(title).trim();
                            var ctitle2 = ctitle.replace(' / ', '/');
                            console.log("ctitle@@@", ctitle2.trim());
                            resJsonData.course_title = ctitle2
                            break;
                        }

                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };

                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

                            let resScholarshipJson = null;


                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }




                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }

                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)

                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;


                            break;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                resJsonData.application_fee = ""
                                break;
                            }
                        case 'ielts_require':
                        case 'ibt_require':
                        case 'pbt_require':
                        case 'pte_require':
                        case 'cae_require':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            var ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_require);
                            var ibt_req = await Course.extractValueFromScrappedElement(courseScrappedData.ibt_require);
                            var pte_req = await Course.extractValueFromScrappedElement(courseScrappedData.pte_require);
                            var cae_req = await Course.extractValueFromScrappedElement(courseScrappedData.cae_require);
                            console.log("IELTS----->", ieltsScore);
                            console.log("ibt_req---->", ibt_req);
                            console.log("pte_req----->", pte_req);
                            console.log("cae_req------>", cae_req);
                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            var penglishList = [];

                            var ieltsScore = [], ibtScore = [], pteScore = [], caeScore = [], pbtScore = [];
                            if (ielts_req) {
                                ieltsScore = await utils.giveMeNumber(ielts_req);
                            }
                            if (ibt_req) {
                                ibtScore = await utils.giveMeNumber(ibt_req);
                            }
                            if (pte_req) {
                                pteScore = await utils.giveMeNumber(pte_req);
                            }
                            if (cae_req) {
                                caeScore = await utils.giveMeNumber(cae_req);
                            }
                            if (ieltsScore) {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = ielts_req;
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsScore;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);

                            }
                            if (ibtScore) {
                                pibtDict.name = 'toefl ibt';
                                pibtDict.description = ibt_req;
                                pibtDict.min = 0;
                                pibtDict.require = ibtScore;
                                pibtDict.max = 120;
                                pibtDict.R = 0;
                                pibtDict.W = 0;
                                pibtDict.S = 0;
                                pibtDict.L = 0;
                                pibtDict.O = 0;
                                penglishList.push(pibtDict);
                            }
                            if (pteScore) {
                                ppteDict.name = 'pte academic';
                                ppteDict.description = pte_req;
                                ppteDict.min = 0;
                                ppteDict.require = pteScore;
                                ppteDict.max = 90;
                                ppteDict.R = 0;
                                ppteDict.W = 0;
                                ppteDict.S = 0;
                                ppteDict.L = 0;
                                ppteDict.O = 0;
                                penglishList.push(ppteDict);
                            }
                            if (caeScore) {
                                pcaeDict.name = 'cae';
                                pcaeDict.description = cae_req;
                                pcaeDict.min = 80;
                                pcaeDict.require = caeScore;
                                pcaeDict.max = 230;
                                pcaeDict.R = 0;
                                pcaeDict.W = 0;
                                pcaeDict.S = 0;
                                pcaeDict.L = 0;
                                pcaeDict.O = 0;

                                penglishList.push(pcaeDict);
                            }

                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }
                            else {
                                courseAdminReq.english = [];
                            }
                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);

                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = await utils.giveMeArray(academicReq.replace(/[\r\n\t]+/g, ';'), ';');
                                }
                                else {
                                    courseAdminReq.academic = [];
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }

                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.course_admission_english_more_details;
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                            }

                            const academicReq11 = await utils.getValueFromHardCodedJsonFile('english_requirements_url');
                            resJsonData.course_admission_requirement.english_requirements_url = academicReq11;
                            resJsonData.course_admission_requirement.academic_requirements_url = "";
                            //courseAdminReq.english_requirements_url = english_more;
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (var selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                let filtered_duration_formated

                                var resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    if (resFulltime.toLowerCase().indexOf("part") > -1) {
                                        resFulltime = resFulltime.toLowerCase().slice(0, resFulltime.toLowerCase().indexOf("part")) + " dummy text to make perfect output " + resFulltime.toLowerCase().slice(resFulltime.toLowerCase().indexOf("part"))
                                    }
                                    console.log("Fuull string--->" + resFulltime)
                                    if (String(resFulltime).includes("or") && !String(resFulltime).includes("Eligible")) {
                                        if (resFulltime.length > 2) {
                                            var Durationval;
                                            var time = String(resFulltime).split('or');
                                            Durationval = time
                                            selList = String(time);
                                            time = selList.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s|)/g, '').split("  ");
                                            console.log("Durationval----->", Durationval);
                                            var duration1 = await format_functions.validate_course_duration_full_time(Durationval[0])
                                            courseDurationDisplayList.push(duration1)
                                            filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                            console.log("Durationval2----->", Durationval);
                                            var duration2 = await format_functions.validate_course_duration_full_time(Durationval[1])
                                            courseDurationDisplayList.push(duration2)
                                            filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[1]);
                                        }
                                    }
                                    else {
                                        courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                        filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    }

                                    console.log("courseDurationDisplayList----->", courseDurationDisplayList);

                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }


                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':



                        case 'course_campus_location': { // Location Launceston
                            var campLocationText; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            var cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            let arrval;
                            if (cricos.includes(",")) {
                                arrval = String(cricos).split(',');
                                const cricosVal11 = String(arrval)

                                course_cricos_code = String(arrval);
                                arrval = course_cricos_code.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s|)/g, '').split(",");
                                console.log("course_cricos_code11----->", cricosVal11);
                            } else {
                                arrval = cricos
                            }

                            const courseKeyVal = courseScrappedData.course_campus_location;
                            console.log("courseKeyVal@@@@", JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList;

                                            ///}
                                        }
                                    }
                                }
                            }
                            if (String(campLocationText).includes('Not applicable')) {
                                throw new Error("Campuses Location Not applicable");
                            }
                            var campuses = [
                                "Melbourne",
                                "Bbronswick",
                                "Regional",
                                "Overseas",
                                "Bundoora"
                            ];
                            var avilcampus = [];
                            campuses.forEach(element => {
                                if (campLocationText.toString().toLowerCase().trim().includes(element.toLowerCase())) {
                                    avilcampus.push(element);
                                }
                            });
                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = campLocationText.toString();
                                resJsonData.course_campus_location = avilcampus;

                                if (avilcampus && avilcampus.length > 0) {
                                    var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                    //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                    // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                    var campusedata = [];
                                    avilcampus.forEach(element => {
                                        campusedata.push({
                                            "name": element,
                                            "code": arrval
                                        })
                                    });
                                    resJsonData.course_campus_location = campusedata;
                                    resJsonData.course_study_mode = "On campus";//.join(',');
                                    console.log("## FInal string-->" + campLocationValTrimmed);
                                }
                            }
                            else {
                                throw new Error("Campuses Location Error");
                            }

                            break;
                        }
                        case 'course_tuition_fees_international_student_all':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            var fee_all = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal_all = courseScrappedData.course_tuition_fees_international_student_all;
                            if (Array.isArray(courseKeyVal_all)) {
                                for (const rootEleDict of courseKeyVal_all) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            fee_all = selList;
                                        }
                                    }
                                }
                            }
                            const feesList = [];
                            const feesDict = {};

                            var feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            var originaltext = feesIntStudent;
                            console.log("fees-->" + feesIntStudent);
                            if (feesIntStudent) {
                                feesIntStudent = feesIntStudent.replace("2019", "").replace("2018", "").replace("2020", "").replace("2021", "").replace("2022", "").replace("2023", "");
                                var feedata = '';
                                if (feesIntStudent.indexOf("AU") > -1) {
                                    feedata = feesIntStudent.split("AU");
                                    feesIntStudent = feedata[1];
                                }
                            }

                            console.log("Fee in student--->" + feesIntStudent);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                let newFees = feesWithDollorTrimmed.split('BP222ACC');
                                console.log("new feess -->", newFees)
                                const feesVal = String(newFees).replace('$', '');
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    if (feesVal != "NA" && feesVal != "N A" && feesVal != "Not applicable") {
                                        const regEx = /\d/g;
                                        let feesValNum = feesVal.match(regEx);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            let feesNumber = null;
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }
                                            console.log(funcName + 'feesNumber = ' + feesNumber);
                                            if (Number(feesNumber)) {
                                                var feesint = {};
                                                feesint.amount = Number(feesNumber);
                                                feesint.duration = "1";
                                                feesint.unit = "year";

                                                feesint.description = (originaltext) ? originaltext : "";

                                                feesDict.international_student = feesint;
                                            }
                                        }
                                    }
                                    else {
                                        var feesint = {};
                                        feesint.amount = 0;
                                        feesint.duration = "1";
                                        feesint.unit = "year";

                                        feesint.description = (originaltext) ? originaltext : "";

                                        feesDict.international_student = feesint;
                                    }
                                }
                            }


                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                console.log("international_student");
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);

                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                    var more_fee = [];
                                    if (fee_all && fee_all.length > 0) {
                                        // feesDict.international_student_all_fees = fee_all;
                                    }
                                    else {
                                        //feesDict.international_student_all_fees = more_fee;
                                    }

                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;
                        }

                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (program_code && program_code.length > 0) {


                                let code = [];
                                code.push(program_code.split(":")[1]);

                                for (let codeval of code) {
                                    resJsonData.program_code = codeval.trim();
                                }

                            } else {
                                program_code.push("");
                                resJsonData.program_code = program_code;
                            }
                            console.log("resJsonData.program_code----->", resJsonData.program_code);
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            const courseIntakeDisplay = [];

                            var courseIntakeStr = [];
                            let sel;
                            let intake_date;
                            const courseKeyVal = courseScrappedData.course_intake;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            for (let selItem of selList) {
                                                if (selItem.includes('and')) {
                                                    selItem = selItem.replace(/and/g, ',');
                                                }
                                                if (selItem.includes('followed by ongoing enrolments')) {
                                                    selItem = selItem.replace(/followed by ongoing enrolments/g, '');
                                                }
                                                selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s|)/g, '').split(",");
                                                console.log("selItem -->", selItem);
                                                for (sel of selItem) {
                                                    console.log("selItemIntake -->", selItem);
                                                    sel = sel.trim();
                                                    console.log("sel -->", sel);
                                                    if (sel.includes('Continuous (scholarship application closing dates apply)')) {
                                                        console.log("seeel@@@@243243454355 -->", sel);
                                                        intake_date = String(sel).replace('Continuous (scholarship application closing dates apply)', 'anytime');
                                                        console.log("intake_date@@@@43523523523 -->", intake_date);
                                                    } else {
                                                        courseIntakeStr.push(sel);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            let seeel = [];
                            seeel = sel
                            console.log("seeelseeel@@@@ -->", seeel);

                            if (!courseIntakeStr) {
                                courseIntakeStr = [];
                            }
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                console.log("courseIntakeStr -->", courseIntakeStr);
                                var intakes = [];
                                var campus = resJsonData.course_campus_location;
                                for (let loc of campus) {
                                    var intakedetail = {};
                                    courseIntakeStr[0]
                                    intakedetail.name = loc.name;
                                    intakedetail.value = courseIntakeStr;
                                    intakes.push(intakedetail);
                                }
                                // var intakedata = {};
                                // intakedata.intake = intakes;
                                // intakedata.more_details = resJsonData.course_intake_url;
                                // resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
                                let formatedIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                console.log("NEw Intake Format@@@@@" + JSON.stringify(formatedIntake))
                                var intakedata = {};

                                // intakedata.intake = formatedIntake;
                                // var intakedata = {};
                                intakedata.intake = formatedIntake;
                                intakedata.more_details = resJsonData.course_intake_url;
                                resJsonData.course_intake = intakedata;
                                let valmonths11 = "";
                           
                            } else if (intake_date.includes('anytime')) {
                                console.log("intake_date@@@@ -->", intake_date);
                                courseIntakeStr.push({ actualdate: intake_date, month: "" });
                                console.log("courseIntakeStr@@@@ -->", courseIntakeStr);
                                var intakes = [];
                                var campus = resJsonData.course_campus_location;
                                for (let loc of campus) {
                                    var intakedetail = {};
                                    courseIntakeStr[0]
                                    intakedetail.name = loc.name;
                                    intakedetail.value = courseIntakeStr;
                                    intakes.push(intakedetail);
                                }
                                var intakedata = {};
                                intakedata.intake = intakes;
                            }

                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            intakedata.more_details = more_details;
                            resJsonData.course_intake = intakedata;
                            console.log("FinalIntake sddssdds--" + JSON.stringify(resJsonData.course_intake));
                            break;
                        }


                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            resJsonData.course_overview = course_overview;
                            break;
                        }

                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_career_outcome) {
                                resJsonData.course_career_outcome = await utils.giveMeArray(course_career_outcome[0].replace(/[\r\n\t]+/g, ';'), ";");
                            }
                            else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }

                        case 'course_study_level': {
                            resJsonData.course_study_level = studyLevel.replace(/[\r\n\t]+/g, "");
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                // console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                // NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;

                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            //  const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.studyLevel, courseDict.category);
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseDict.category, courseScrappedData, s.page.url(), courseDict.study_level);

            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
