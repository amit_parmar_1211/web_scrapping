const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const configs = require('./configs');

class ScrapeCourseList extends Scrape {
  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'startScrappingFunc ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var datalist = [];
      await s.setupNewBrowserPage("https://www.rmit.edu.au/study-with-us/study-areas");
      var mainCategory = [], redirecturl = [], subcategory = [];
      //scrape main category
      const maincategoryselector = "//*//h2[contains(text(),'studying...')]/following::div[1]//h3";
      const maincategoryselectorurl = "//*//h2[contains(text(),'studying...')]/following::div[1]//h3/..";
      var category = await s.page.$x(maincategoryselector);
      var categoryurl = await s.page.$x(maincategoryselectorurl);
      console.log("Total categories-->" + category.length);
      for (let i = 0; i < category.length; i++) {
        var categorystringmain = await s.page.evaluate(el => el.innerText, category[i]);


        var categorystringmainurl = await s.page.evaluate(el => el.href, categoryurl[i]);
        mainCategory.push(categorystringmain.replace(/[\r\n\t]+/g, ' '));
        redirecturl.push({ innerText: categorystringmain.replace(/[\r\n\t]+/g, ' '), href: categorystringmainurl });
      }

      //scrape courses


      for (let i = 0; i < redirecturl.length; i++) {
        await s.page.goto(redirecturl[i].href, { timeout: 0 });
        var subcatcs = "/html/body/div/div/div/div/div/div/div/div/div/div/div/div/ul/li/div/a[not(contains(text(),'short courses'))][not(contains(text(),'Online'))]"
        var subcatc = await s.page.$x(subcatcs)
        for (let j = 0; j < subcatc.length; j++) {
          var href = await s.page.evaluate(el => el.href, subcatc[j]);
          var text = await s.page.evaluate(el => el.innerText, subcatc[j]);
          subcategory.push({
            href: href, innerText: text, category: redirecturl[i].innerText
          })
        }


      }
      console.log("redirecturl", subcategory)
      for (let k = 0; k < subcategory.length; k++) {
        // for (let href of subcategory) {
        // await s.page.goto(href, { timeout: 0 });
        await s.page.goto(subcategory[k].href, { timeout: 0 });
        console.log("#Proceed1jsgfdjfhg-->" + subcategory[k].category);
        const selector = "//*[@id='rmit-program-list']//tbody/tr/td[3]/a";
        const programcode = "//*[@id='rmit-program-list']//tbody/tr/td[4]"
        const study_level = "//*[@id='rmit-program-list']//tbody/tr/td[2]";
        console.log("#Proceed2-->" + selector);
        var title = await s.page.$x(selector);
        var level = await s.page.$x(study_level)
        var pc = await s.page.$x(programcode)

        //var levels=await s.page.evaluate(el=>el.innerText,level[0])

        for (var i = 0; i < title.length; i++) {
          var categorystring = await s.page.evaluate(el => el.innerText, title[i]);
          var categoryurl = await s.page.evaluate(el => el.href, title[i]);
          var levels = await s.page.evaluate(el => el.innerText, level[i])
          var pc1 = await s.page.evaluate(el => el.innerText, pc[i])

          console.log("categoryurl@@@", categoryurl);
          console.log("categorystring@@@", categorystring);
          console.log("levels@@@", levels);
          console.log("pc1@@@", pc1);
          datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim() + "(" + pc1 + ")", category: subcategory[k].category, study_level: levels });
        }
        //console.log("datalist",datalist)
        //fs.writeFileSync("./output/data.json", JSON.stringify(datalist));

      }




      // for (let catc of redirecturl) {
      //   await s.page.goto(catc.href, { timeout: 0 });
      //   console.log("#Proceed-->" + catc.innerText);
      //   var sub = "//*[@class='columnlinklist__content--box cc_cursor']/ul/li/div//a";

      //   var subcat = await s.page.$x(sub);
      //   var subhref = [];
      //   for (let link of subcat) {
      //     var href = await s.page.evaluate(el => el.href, link);
      //     console.log("#hrefSub-->" + href);
      //     subhref.push(href);
      //   }

      //   for (let href of subhref) {
      //     await s.page.goto(href, { timeout: 0 });
      //     console.log("#Proceed1-->" + href.innerText);
      //     const selector = "//*[@id='rmit-program-list']//tbody/tr/td[3]/a";
      //     const programcode = "//*[@id='rmit-program-list']//tbody/tr/td[4]"
      //     const study_level = "//*[@id='rmit-program-list']//tbody/tr/td[2]";
      //     console.log("#Proceed2-->" + selector);
      //     var title = await s.page.$x(selector);
      //     var level = await s.page.$x(study_level)
      //     var pc = await s.page.$x(programcode)

      //     //var levels=await s.page.evaluate(el=>el.innerText,level[0])

      //     for (var i = 0; i < title.length; i++) {
      //       var categorystring = await s.page.evaluate(el => el.innerText, title[i]);
      //       var categoryurl = await s.page.evaluate(el => el.href, title[i]);
      //       var levels = await s.page.evaluate(el => el.innerText, level[i])
      //       var pc1 = await s.page.evaluate(el => el.innerText, pc[i])

      //       console.log("categoryurl@@@", categoryurl);
      //       console.log("categorystring@@@", categorystring);
      //       console.log("levels@@@", levels);
      //       console.log("pc1@@@", pc1);
      //       datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim() + "(" + pc1 + ")", category: catc.innerText, study_level: levels });
      //     }
      //     //console.log("datalist",datalist)
      //     //fs.writeFileSync("./output/data.json", JSON.stringify(datalist));

      //   }

      // }

      var uniquecases = [], uniquedata = [], finalDict = [];
      datalist.forEach(element => {
        if (!uniquecases.includes(element.href)) {
          uniquecases.push(element.href);
          uniquedata.push(element);
        }
      });
      uniquecases.forEach(element => {
        var data = datalist.filter(e => e.href == element);
        var category = [];
        data.forEach(element => {
          if (!category.includes(element.category))
            category.push(element.category);
        });

        data[0].category = (Array.isArray(category)) ? category : [category];
        finalDict.push(data[0]);
      });

      console.log("unique data-->" + uniquecases.length);
      fs.writeFileSync("./output/courselist_uniq.json", JSON.stringify(uniquedata));
      fs.writeFileSync("./output/courselist_original.json", JSON.stringify(datalist));
      console.log(funcName + 'writing courseList to file....');
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(finalDict));
      fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return finalDict;

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
}

module.exports = { ScrapeCourseList };
