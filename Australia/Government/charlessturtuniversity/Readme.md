**The University Of Sydney: Total Courses Details As On 18 Mar 2019**
* Total Courses = 114
* Courses without CRICOS code = 10
* Total available courses = 114
* Repeted = 20
* fail Course =31

* Not getting Online and non aqf


[
  {
    "href": "https://futurestudents.csu.edu.au/courses/agricultural-wine-sciences/bachelor-agricultural-science",
    "innerText": "Bachelor of Agricultural Science",
    "reason": "not availabel fee 2019"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/allied-health-pharmacy/bachelor-health-rehabilitation-science",
    "innerText": "Bachelor of Health and Rehabilitation Science",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/medical-science/bachelor-medical-radiation-science",
    "innerText": "Bachelor of Medical Radiation Science (with specialisations)",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/allied-health-pharmacy/bachelor-occupational-therapy",
    "innerText": "Bachelor of Occupational Therapy",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/allied-health-pharmacy/bachelor-paramedicine",
    "innerText": "Bachelor of Paramedicine",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/allied-health-pharmacy/bachelor-pharmacy",
    "innerText": "Bachelor of Pharmacy",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/allied-health-pharmacy/bachelor-physiotherapy",
    "innerText": "Bachelor of Physiotherapy",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/allied-health-pharmacy/bachelor-podiatric-medicine",
    "innerText": "Bachelor of Podiatric Medicine",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/allied-health-pharmacy/bachelor-speech-language-pathology",
    "innerText": "Bachelor of Speech and Language Pathology",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/animal-vet-sciences/bachelor-animal-science",
    "innerText": "Bachelor of Animal Science",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/animal-vet-sciences/bachelor-equine-science",
    "innerText": "Bachelor of Equine Science (with specialisation)",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/animal-vet-sciences/bachelor-veterinary-biology-bachelor-veterinary-science",
    "innerText": "Bachelor of Veterinary Biology / Bachelor of Veterinary Science",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/business/agribusiness",
    "innerText": "Bachelor of Agricultural Business Management",
    "reason": "TimeoutError: Navigation Timeout Exceeded: 30000ms exceeded"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/business/doctor-philosophy-business",
    "innerText": "Doctor of Philosophy",
    "reason": "Error: Invalid courseScrappedData, courseScrappedData = null"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/communication-creative/bachelor-communication-advertising",
    "innerText": "Bachelor of Communication (Advertising)",
    "reason": "Error: Invalid courseScrappedData, courseScrappedData = null"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/communication-creative/bachelor-communication-journalism",
    "innerText": "Bachelor of Communication (Journalism)",
    "reason": "TimeoutError: Navigation Timeout Exceeded: 30000ms exceeded"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/communication-creative/bachelor-communication-public-relations",
    "innerText": "Bachelor of Communication (Public Relations)",
    "reason": "TimeoutError: Navigation Timeout Exceeded: 30000ms exceeded"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/environmental-outdoor/bachelor-applied-science-outdoor-recreation-ecotourism",
    "innerText": "Bachelor of Applied Science (Outdoor Recreation and Ecotourism)",
    "reason": "TypeError: Cannot read property 'replace' of undefined"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/environmental-outdoor/bachelor-applied-science-parks-recreation-heritage",
    "innerText": "Bachelor of Applied Science (Parks, Recreation and Heritage)",
    "reason": "TypeError: Cannot read property 'replace' of undefined"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/environmental-outdoor/bachelor-environmental-science-management",
    "innerText": "Bachelor of Environmental Science and Management",
    "reason": "TypeError: Cannot read property 'replace' of undefined"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/exercise-sport-science/bachelor-exercise-science-honours",
    "innerText": "Bachelor of Exercise Science (Honours)",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/teaching-education/bachelor-education-k12",
    "innerText": "Bachelor of Education (K–12)",
    "reason": "Error: Invalid courseScrappedData, courseScrappedData = null"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/library-information-studies/bachelor-information-studies",
    "innerText": "Bachelor of Information Studies (with specialisations)",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/library-information-studies/master-information-studies",
    "innerText": "Master of Information Studies (with specialisations)",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/medical-science/bachelor-dental-science",
    "innerText": "Bachelor of Dental Science",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/medical-science/bachelor-medical-radiation-science",
    "innerText": "Bachelor of Medical Radiation Science (with specialisations)",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/medical-science/bachelor-oral-health-therapy-hygiene",
    "innerText": "Bachelor of Oral Health (Therapy and Hygiene)",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/psychology/bachelor-psychology",
    "innerText": "Bachelor of Psychology",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/teaching-education/bachelor-early-childhood-primary",
    "innerText": "Bachelor of Education (Early Childhood and Primary)",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/teaching-education/bachelor-education-k12",
    "innerText": "Bachelor of Education (K–12)",
    "reason": "Error: Invalid courseScrappedData, courseScrappedData = null"
  },
  {
    "href": "https://futurestudents.csu.edu.au/courses/teaching-education/bachelor-education-technology-applied-studies",
    "innerText": "Bachelor of Education (Technology and Applied Studies)",
    "reason": "Error: Invalid value of parameter, invalidParamsList = [null]"
  }
]

