const fs = require("fs");
async function getmymapping(course_name) {
    console.log("###IELTS SCORE for--->" + course_name);
    var score = "";
    try {
        const mappings = JSON.parse(fs.readFileSync("../charlessturtuniversity/output/Mapping/mapping.js"));
        for (let mapping of mappings[0].mappings) {
            if (mapping.course_name.includes(course_name)) {
                score = mapping.mapping_scrore;
            }
        }
        if (score.length == 0) {
            score = mappings[0].default[0].mapping_scrore;
        }
    }
    catch (e) {
        console.log("Error--->" + e);
    }
    return await score;
}
module.exports = {
    getmymapping
}
