const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
const mapping = require('./output/Mapping/main');
request = require('request');
class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static titleCase(str) {
        console.log("titleeeeee---->>>");
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string

        return splitStr.join(' ');
    }
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, studyLevel) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = course_category;
                            break;
                        }

                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const penglishList = [];

                            const ieltsDict = {}; const ieltsDict1 = {}; const ibtDict = {}; const ibtDict1 = {}; const pteDict = {}; const pteDict1 = {}; const caeDict = {}; const caeDict1 = {}; const cpeDict = {}; let ieltsNumber = null;
                            // english requirement
                            courseScrappedData.course_toefl_ielts_score = await mapping.getmymapping(course_name);
                            console.log("##IELTS SCORE string--->" + courseScrappedData.course_toefl_ielts_score)
                            if (courseScrappedData.course_toefl_ielts_score) {
                                const ieltsScore = courseScrappedData.course_toefl_ielts_score; //await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                                if (ieltsScore && ieltsScore.length > 0) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsScore;
                                    ieltsDict.min = 0;
                                    ieltsDict1.require = await utils.giveMeNumber(ieltsScore.replace(/ /g, ' '));;
                                    ieltsDict.require = Number(ieltsDict1.require);
                                    console.log("ieltsDict1-->", ieltsDict.require);
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);
                                    console.log("fdgfdfjkhdkshdksjdln", englishList);
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(ieltsScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ieltsNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                    }
                                }
                            }
                            // if ieltsNumber is valid then map IBT and PBT accordingly
                            if (ieltsNumber && ieltsNumber > 0) {
                                const ieltsMappingDict = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                                console.log('ieltsMappingDict = ', ieltsMappingDict);
                                if (ieltsMappingDict) {
                                    const otherLngDict = ieltsMappingDict[ieltsNumber];
                                    console.log(funcName + 'otherLngDict = ' + JSON.stringify(otherLngDict));
                                    if (otherLngDict) {
                                        const ibtScore = otherLngDict.ibt;
                                        const pteScore = otherLngDict.pte;
                                        const caeScore = otherLngDict.cae;

                                        console.log(funcName + 'ibtScore = ' + ibtScore + ', pteScore = ' + pteScore + ', caeScore = ' + caeScore);
                                        if (ibtScore) {
                                            ibtDict.name = 'toefl ibt';
                                            ibtDict.description = ibtScore;

                                            ibtDict.min = 0;
                                            ibtDict1.require = await utils.giveMeNumber(ibtScore.replace(/ /g, ' '));;
                                            ibtDict.require = Number(ibtDict1.require);

                                            ibtDict.max = 120;
                                            ibtDict.R = 0;
                                            ibtDict.W = 0;
                                            ibtDict.S = 0;
                                            ibtDict.L = 0;
                                            ibtDict.O = 0;
                                            englishList.push(ibtDict);
                                        }
                                        if (pteScore) {
                                            pteDict.name = 'pte academic';
                                            pteDict.description = pteScore;
                                            pteDict.min = 0;
                                            pteDict1.require = await utils.giveMeNumber(pteScore.replace(/ /g, ' '));;
                                            pteDict.require = Number(pteDict1.require);
                                            pteDict.max = 90;
                                            pteDict.R = 0;
                                            pteDict.W = 0;
                                            pteDict.S = 0;
                                            pteDict.L = 0;
                                            pteDict.O = 0;
                                            englishList.push(pteDict);
                                        }
                                        if (caeScore) {
                                            caeDict.name = 'cae';
                                            caeDict.description = caeScore;
                                            caeDict.min = 80;
                                            caeDict1.require = await utils.giveMeNumber(caeScore.replace(/ /g, ' '));;
                                            caeDict.require = Number(caeDict1.require);
                                            caeDict.max = 230;
                                            caeDict.R = 0;
                                            caeDict.W = 0;
                                            caeDict.S = 0;
                                            caeDict.L = 0;
                                            caeDict.O = 0;
                                            englishList.push(caeDict);
                                        }
                                    } else { // if (otherLngDict)
                                        ibtDict.name = 'toefl ibt';
                                        ibtDict.description = configs.propValueNotAvaialble;
                                        ibtDict.min = 0;
                                        ibtDict.require = await utils.giveMeNumber(ibtScore.replace(/ /g, ' '));;
                                        ibtDict.max = 9;
                                        ibtDict.R = 0;
                                        ibtDict.W = 0;
                                        ibtDict.S = 0;
                                        ibtDict.L = 0;
                                        ibtDict.O = 0;
                                        englishList.push(ibtDict);

                                        pteDict.name = 'pte academic';
                                        pteDict.description = configs.propValueNotAvaialble;
                                        pteDict.min = 0;
                                        pteDict.require = await utils.giveMeNumber(pteScore.replace(/ /g, ' '));;
                                        pteDict.max = 9;
                                        pteDict.R = 0;
                                        pteDict.W = 0;
                                        pteDict.S = 0;
                                        pteDict.L = 0;
                                        pteDict.O = 0;
                                        englishList.push(pteDict);

                                        caeDict.name = 'cae';
                                        caeDict.description = configs.propValueNotAvaialble;
                                        caeDict.min = 0;
                                        caeDict.require = await utils.giveMeNumber(caeScore.replace(/ /g, ' '));;
                                        caeDict.max = 9;
                                        caeDict.R = 0;
                                        caeDict.W = 0;
                                        caeDict.S = 0;
                                        caeDict.L = 0;
                                        caeDict.O = 0;
                                        englishList.push(caeDict);
                                    } // if (otherLngDict)
                                }
                            }
                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }
                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            if (ieltsNumber && ieltsNumber > 0) {
                                const ieltsMappingProgressDict = await utils.getValueFromHardCodedJsonFile('ielts_progress');
                                if (ieltsMappingProgressDict) {
                                    const otherLngDict = ieltsMappingProgressDict[ieltsNumber];
                                    console.log(funcName + 'ieltsMappingProgressDict = ' + JSON.stringify(otherLngDict));
                                    if (otherLngDict) {
                                        const ieltsScore = otherLngDict.ielts;
                                        const ibtScore = otherLngDict.ibt;
                                        const pteScore = otherLngDict.pte;
                                        const caeScore = otherLngDict.cae;
                                        console.log(funcName + 'ibtScore = ' + ibtScore + ', pteScore = ' + pteScore);
                                        if (ieltsScore) {
                                            pieltsDict.name = 'ielts academic';
                                            pieltsDict.value = ieltsScore;
                                            penglishList.push(pieltsDict);
                                        }
                                        if (ibtScore) {
                                            pibtDict.name = 'toefl ibt';
                                            pibtDict.value = ibtScore;
                                            penglishList.push(pibtDict);
                                        }
                                        if (pteScore) {
                                            ppteDict.name = 'pte academic';
                                            ppteDict.value = pteScore;
                                            penglishList.push(ppteDict);
                                        }
                                        if (caeScore) {
                                            pcaeDict.name = 'cae';
                                            pcaeDict.value = caeScore;
                                            penglishList.push(pcaeDict);
                                        }
                                    }
                                }
                            }
                            // if (penglishList && penglishList.length > 0) {
                            //     courseAdminReq.englishprogress = penglishList;
                            //     console.log("##penglishList--->", penglishList)
                            // }
                            ///progrssbar End

                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                let academicReq = [];
                                academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + [academicReq]);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {

                                    courseAdminReq.academic = [String(academicReq).replace(/[\r\n\t]+/g, ' ')];
                                    
                                    console.log('courseAdminReq = ', courseAdminReq.academic);
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            } else {
                                resJsonData.course_admission_requirement = [];
                            }

                            const courseKeyValAcademic = courseScrappedData.course_admission_academic_more_details;
                            let resAcademicReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyValAcademic)) {
                                for (const rootEleDict of courseKeyValAcademic) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAcademicReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            let resEnglishReqMoreDetailsJson = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;

                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;

                            }
                            break;
                        }
                        case 'application_fee': {
                        resJsonData.application_fee = "";
                        }
                        break;
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));

                                            rescourse_url = selList;

                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;

                        }
                        case 'course_duration_full_time': {

                            var courseDurationList = "";
                            const courseDurationDisplayList = [];
                            var durationFullTime = [];

                            let anotherArray = [];
                            let partTimeDuration = "", finalDuration = "";
                            partTimeDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time);
                            var data = await utils.giveMeArray(partTimeDuration.replace(/[\r\n\t]+/g, ";"), ";")
                            for (let i = 0; i < data.length; i++) {
                                var ismatch = false, strdata = "";
                                console.log("data------>", data[i]);
                                console.log("resJsonData.course_title", resJsonData.course_title);
                                if (data[i] && (data[i].trim().toLowerCase() == resJsonData.course_title.toLowerCase().trim())) {
                                    ismatch = true;
                                    if (data[i + 1] && (data[i + 1].trim().toLowerCase().indexOf("full-time") != -1 || data[i + 1].trim().toLowerCase().indexOf("part-time") != -1)) {
                                        strdata += data[i + 1];
                                    }
                                    if (data[i + 2] && (data[i + 2].trim().toLowerCase().indexOf("full-time") != -1 || data[i + 2].trim().toLowerCase().indexOf("part-time") != -1)) {
                                        strdata += " " + data[i + 2];
                                    }
                                }
                                if (ismatch) {
                                    partTimeDuration = strdata.trim();
                                    break;

                                }
                            }

                            console.log("partTimeDuration ----->", partTimeDuration);
                            console.log("*************start formating Full Time years*************************");

                            finalDuration = partTimeDuration;
                            console.log("finalDuration -->", finalDuration);
                            const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration);
                            console.log("full_year_formated_data", full_year_formated_data);

                            const not_full_year_formated_full = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                            console.log("not_full_year_formated_data", not_full_year_formated_full);

                            try {
                                durationFullTime.duration_full_time = finalDuration;

                                courseDurationList = durationFullTime.duration_full_time;
                                ///END course duration
                                console.log("courseDurationList : ", courseDurationList);
                                ///course duration display
                                courseDurationDisplayList.push(full_year_formated_data);
                                // anotherArray.push(courseDurationDisplayList);
                                console.log("courseDurationDisplayList", courseDurationDisplayList);
                                ///END course duration display
                            } catch (err) {
                                console.log("Problem in Full Time years:", err);
                            }
                            console.log("courseDurationDisplayList1", courseDurationDisplayList);
                            console.log('***************END formating Full Time years**************************')
                            let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                            if (courseDurationList && courseDurationList.length > 0) {
                                console.log("fullll--------->",finalDuration);
                                resJsonData.course_duration = finalDuration;
                                console.log("resJsonData.course_duration-=0=-0=0>",resJsonData.course_duration);
                            } else {
                                console.log("fullll--------->",courseDurationList)
                                resJsonData.course_duration;
                               
                            }

                            if (filtered_duration_formated && filtered_duration_formated.length > 0) {

                                resJsonData.course_duration_display = filtered_duration_formated;
                                var isfulltime = false, isparttime = false;
                                filtered_duration_formated.forEach(element => {
                                    if (element.display == "Full-Time") {
                                        isfulltime = true;
                                    }
                                    if (element.display == "Part-Time") {
                                        isparttime = true;
                                    }
                                });
                                resJsonData.isfulltime = isfulltime;
                                resJsonData.isparttime = isparttime;

                            }
                            else {
                                resJsonData.course_duration_display
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            // feesDict.international_student = configs.propValueNotAvaialble;
                            // feesDict.fee_duration_years = configs.propValueNotAvaialble;
                            // feesDict.currency = configs.propValueNotAvaialble;
                            let feesIntStudent = null;
                            let feesScraped = courseScrappedData.course_tuition_fees_international_student;
                            console.log("feesScraped -->", feesScraped);
                            // const feesIntStudent_2019 = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_2019);
                            if (Array.isArray(feesScraped)) {
                                for (const rootEleDict of feesScraped) {
                                    console.log(funcName + '\n\r rootEleDict feesScraped = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict feesScraped = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList feesScraped = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                feesIntStudent = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            //const feesIntStudent_2020 = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_2019);
                            // const feeYear_2019 = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year_2020);

                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: feesWithDollorTrimmed,

                                            });

                                        } else {
                                            throw new Error('Fees not Found');
                                        }
                                    }
                                }
                            } else {
                                throw new Error('Fees not Found');
                            }


                            // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = Number(inetStudentFees);
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if

                            let international_student_all_fees_array = [];
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                   // feesDict.international_student_all_fees = international_student_all_fees_array
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }
                        case 'course_intake_url': {
                            const courseKeyVal = courseScrappedData.course_intake_url;
                            var url = []
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                url = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (url.length > 0) {
                                resJsonData.course_intake_url = url;
                            }
                            break;
                        }
                        case 'course_study_mode': { // Location Launceston


                            //resJsonData.course_study_mode = coursestudymodeText;
                            resJsonData.course_study_mode = "On campus";

                            break;
                        }
                        // case 'course_cricos_code': {
                        //     const courseKeyVal = courseScrappedData.course_cricos_code;
                        //     let course_cricos_code = null;
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList cricos_code= ' + JSON.stringify(selList));
                        //                     if (Array.isArray(selList) && selList.length > 0) {
                        //                         let selList1 = selList[0].trim();
                        //                         var crcode = selList1.toString().split(' ');
                        //                         var cricoscode = [crcode[0]];
                        //                         //cricoscode = crcode[0];
                        //                         course_cricos_code = cricoscode;
                        //                     }


                        //                 }

                        //             }
                        //         }
                        //     }
                        //     let crico = "";
                        //     for (crico of course_cricos_code) {
                        //         course_cricos_code = crico;
                        //         console.log("course_cricos_code----------->", course_cricos_code);
                        //     }
                        //     if (course_cricos_code) {
                        //         global.cricos_code = course_cricos_code[0];

                        //         var locations = resJsonData.course_campus_location;
                        //         var mycodes = [];
                        //         for (let location of locations) {
                        //             mycodes.push({
                        //                 location: location.name, code: course_cricos_code.toString(), iscurrent: false
                        //             })
                        //         }
                        //         console.log("located----------->", mycodes);

                        //         resJsonData.course_cricos_code = mycodes;
                        //     }
                        //     else {
                        //         throw new Error("cricos not found");
                        //     }



                        //     break;
                        // }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""


                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        // case 'course_outline': {
                        //     const courseKeyVal = courseScrappedData.course_outline;
                        //     let res_course_outline = [];
                        //     let majors = [];
                        //     let minors = [];
                        //     // if (Array.isArray(courseKeyVal)) {
                        //     //     for (const rootEleDict of courseKeyVal) {
                        //     //         console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //     //         const elementsList = rootEleDict.elements;
                        //     //         for (const eleDict of elementsList) {
                        //     //             console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //     //             const selectorsList = eleDict.selectors;
                        //     //             for (const selList of selectorsList) {
                        //     //                 //  console.log("####OUTLINE" + JSON.stringify(selList));
                        //     //                 //console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //     //                 //if (Array.isArray(selList) && selList.length > 0) {
                        //     //                 res_course_outline = selList;
                        //     //                 //console.log("####OUTLINE" + JSON.stringify(rescourse_scholarship));
                        //     //                 //}
                        //     //             }
                        //     //         }
                        //     //     }
                        //     // }
                        //    // console.log("##OUtline-->" + JSON.stringify(res_course_outline))
                        //     if (res_course_outline ==[]) {
                        //         console.log("hrjldffxghfgj")
                        //         resJsonData.course_outline.push({
                        //             "majors": [],
                        //             "minors": [],
                        //             })
                        //     }
                          
                        //     break;
                        // }
                        case 'course_campus_location': { // Location Launceston
                            const camlocation1 = await Course.extractLocationValueFromScrappedElement(courseScrappedData.course_campus_location);
                            console.log("camlocation1-->",camlocation1);
                            if(camlocation1.includes('No on-campus study'))
                            {
                                throw new Error("Not available to On-Campus study !!!!!");
                            }
                            var cricc = await Course.extractLocationValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            var cricc1 = cricc.split('(')[0].trim();
                            console.log("cricc-->", cricc1);
                            let campLocationText = camlocation1.trim();

                            console.log(funcName + 'campLocationText = ' + campLocationText);
                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = String(campLocationText).trim();
                                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    let camlocation = campLocationValTrimmed.trim();
                                    let splitCampus = await utils.giveMeArray(camlocation.replace(/[\r\n\t]+/g, '').trim(), ',');
                                    let finalCampusVariable = [];
                                    for (let campus of splitCampus) {
                                        if (campus.toLowerCase().indexOf('uni wide') > -1) {
                                            //if campus is uniwide means it is applicable for all campus
                                            finalCampusVariable.push("Albury-Wodonga");
                                            finalCampusVariable.push("Bathurst");
                                            finalCampusVariable.push("Canberra");
                                            finalCampusVariable.push("CSU Study Centre Melbourne");
                                            finalCampusVariable.push("CSU Study Centre Sydney");
                                            finalCampusVariable.push("CSU Study Centre Brisbane");
                                            finalCampusVariable.push("Dubbo");
                                            finalCampusVariable.push("Orange");
                                            finalCampusVariable.push("Parramatta");
                                            finalCampusVariable.push("Port Macquarie");
                                            finalCampusVariable.push("Wagga Wagga");



                                        } else {
                                            finalCampusVariable.push(campus.trim());
                                        }



                                    }
                                    let newCampus = [];
                                    // var cric = mycodes;
                                    console.log("cric--->", cricc1);
                                    for (let campus of finalCampusVariable) {
                                        campus = this.titleCase(campus);
                                        newCampus.push({
                                            "name": campus,
                                            "code": cricc1

                                        });
                                    }

                                    resJsonData.course_campus_location = newCampus;
                                    console.log("finalCampusVariable777777777777777777", newCampus);
                                    // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //

                                }
                                else {
                                    throw new Error("location not found");
                                }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_intake': {
                            var courseIntakeStr = [];
                            //existing intake value
                            const courseIntakeStr1 = await Course.extractdurationValueFromScrappedElement(courseScrappedData.course_intake);
                            let intake_date = [
                                {
                                    key: "March 2,2020",
                                    value: "2020-03-02"
                                },
                                {
                                    key: "July 13,2020",
                                    value: "2020-07-13"
                                },
                                {
                                    key: "November 16,2020",
                                    value: "2020-11-16"
                                }
                            ]
                            if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                                resJsonData.course_intake = courseIntakeStr1;
                                console.log('changes intake date :' + courseIntakeStr1);
                                const intakeStrList = String(courseIntakeStr1);
                                console.log(funcName + 'intakeStrList course_intake = ' + JSON.stringify(intakeStrList));
                                courseIntakeStr = JSON.parse(courseIntakeStr1);
                            }
                            console.log("intakes date for course" + JSON.stringify(courseIntakeStr));
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var campus = resJsonData.course_campus_location;
                                console.log('Campuse location :' + campus);
                                const course_semesterText = await Course.extractOtherValueListFromScrappedElement(courseScrappedData.course_semester);
                                console.log('course semester length :' + course_semesterText.length);
                                var semestercount = null;
                                if (course_semesterText && course_semesterText.length > 0) {
                                    const coursesemesterVal = String(course_semesterText).split(';');
                                    semestercount = coursesemesterVal.length;
                                }
                                var intakesArray = [];
                                var array_data = courseIntakeStr.toString().replace(/\s/g, ' ').split(';');
                                console.log('array intake length :' + array_data.length);
                                var durationArr = '';
                                for (var k = 0; k < semestercount; k++) {
                                    console.log('array of :' + array_data[k]);
                                    durationArr += array_data[k] + ";";
                                }
                                durationArr = durationArr.slice(0, -1);
                                intakesArray.push(durationArr);

                                var intakes = [];
                                console.log("Intake length-->" + intakesArray.length);
                                console.log("Campus length-->" + campus.length);

                                for (var camcount = 0; camcount < campus.length; camcount++) {
                                    for (var count = 0; count < intakesArray.length; count++) {
                                        console.log("intake -->" + count)
                                        var intakedetail = {};
                                        intakedetail.campus = campus[camcount];
                                        intakedetail.name = campus[camcount].name;
                                        intakedetail.value = await utils.giveMeArray(intakesArray[count].replace(/[\r\n\t ]+/g, ' '), ";");
                                        intakes.push(intakedetail);
                                    }
                                }
                                console.log("Intakes --> ", intakes);
                                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                let formatedIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                console.log("Intakes --> ", JSON.stringify(formatedIntake));
                                var intakedata = {};
                                intakedata.intake = formatedIntake;
                                intakedata.more_details = more_details;

                                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
                            } // if (courseIntakeStr && courseIntakeStr.length > 0)
                            break;
                        }


                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                        case 'course_accomodation_cost': {
                            const courseKeyVal = courseScrappedData.course_accomodation_cost;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_accomodation_cost = resAccomodationCostJson;
                            }
                            break;
                        }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_country = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_country) {
                                resJsonData.course_country = course_country;
                            }
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            console.log(funcName + 'matched case course_career_outcome: ' + key);
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            var course_career_outcome = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            } // rootElementDictList
                            // add only if it has valid value
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            } else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }

                        case 'program_code': {
                            console.log(funcName + 'matched case program_code: ' + key);
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                            else {
                                                program_code = "";
                                            }
                                        }
                                    }
                                }
                            }
                            resJsonData.program_code = program_code;
                            break;
                        }
                        case 'course_title': {
                            var title1  = course_name;
                            var ctitle = format_functions.titleCase(title1).trim();
                            resJsonData.course_title = ctitle;
                        }
                        // case 'course_title': {
                        //     let concatnatedElementsStr = null;
                        //     console.log(funcName + 'matched case course_title : ' + key);
                        //     const rootElementDictList = courseScrappedData[key];
                        //     console.log(funcName + 'key-val is course_title' + JSON.stringify(rootElementDictList));
                        //     let concatnatedRootElementsStr = null;
                        //     for (const rootEleDict of rootElementDictList) {
                        //         console.log(funcName + '\n\r rootEleDict course_title = ' + JSON.stringify(rootEleDict));
                        //         const elementsList = rootEleDict.elements;

                        //         for (const eleDict of elementsList) {
                        //             console.log(funcName + '\n\r eleDict course_titleaaa= ' + JSON.stringify(eleDict));
                        //             const selectorsList = eleDict.selectors;
                        //             let concatnatedSelectorsStr = null;
                        //             var alreadyexist = true;
                        //             for (const selList of selectorsList) {
                        //                 let concatnatedSelStr = null;
                        //                 if (alreadyexist) {
                        //                     console.log(funcName + '\n\r selList course_title 1 = ' + JSON.stringify(selList));
                        //                     console.log(funcName + '\n\r selList course_title 2 = ' + selList);
                        //                     alreadyexist = false;
                        //                     for (const selItem of selList) {
                        //                         if (!concatnatedSelStr) {
                        //                             concatnatedSelStr = selItem;
                        //                         } else {
                        //                             concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                        //                         }
                        //                     } // selList
                        //                 }
                        //                 console.log(funcName + 'concatnatedSelStr course_title = ' + concatnatedSelStr);
                        //                 if (concatnatedSelStr) {
                        //                     if (!concatnatedSelectorsStr) {
                        //                         concatnatedSelectorsStr = concatnatedSelStr.replace("\n", " ");
                        //                     } else {
                        //                         concatnatedSelectorsStr = String(concatnatedSelectorsStr).replace("\n", " ").concat(' ').concat(concatnatedSelStr).trim();
                        //                     }
                        //                 }
                        //             } // selectorsList
                        //             console.log(funcName + 'concatnatedSelectorsStr course_title = ' + concatnatedSelectorsStr);
                        //             // concat elements
                        //             if (concatnatedSelectorsStr) {
                        //                 if (!concatnatedElementsStr) {
                        //                     concatnatedElementsStr = concatnatedSelectorsStr;
                        //                 } else {
                        //                     concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                        //                 }
                        //             }
                        //         } // elementsList
                        //         console.log(funcName + 'concatnatedElementsStr course_title = ' + concatnatedElementsStr);
                        //         if (concatnatedElementsStr) {
                        //             if (!concatnatedRootElementsStr) {
                        //                 concatnatedRootElementsStr = concatnatedElementsStr;
                        //             } else {
                        //                 concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                        //             }
                        //         }
                        //     } // rootElementDictList
                        //     console.log(funcName + 'concatnatedRootElementsStr course_title = ' + concatnatedRootElementsStr);
                        //     // add only if it has valid value
                        //     if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                        //         concatnatedElementsStr = this.titleCase(concatnatedElementsStr);
                        //         resJsonData[key] = String(concatnatedRootElementsStr).trim();
                        //     }
                        //     break;
                        // }
                        case 'course_study_level': {
                            resJsonData.course_study_level = studyLevel;
                            break;
                        }
                        // case 'course_study_level': {
                        //     const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                        //     let cStudyLevel = null;
                        //     console.log(funcName + 'course_title course_study_level = ' + cTitle);
                        //     if (cTitle) {
                        //         if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || String(cTitle).toLowerCase().indexOf('advanced diploma') > -1 || String(cTitle).toLowerCase().indexOf('diploma') > -1) {
                        //             cStudyLevel = 'Under Graduate';
                        //         }
                        //         else if (String(cTitle).toLowerCase().indexOf('master by research') > -1 || String(cTitle).toLowerCase().indexOf('master by researchs') > -1 || String(cTitle).toLowerCase().indexOf('master research') > -1 || String(cTitle).toLowerCase().indexOf('master by researchs') > -1) {
                        //             cStudyLevel = 'Research';
                        //         }
                        //         else if (String(cTitle).toLowerCase().indexOf('graduate') > -1 || String(cTitle).toLowerCase().indexOf('graduates') > -1) {
                        //             cStudyLevel = 'Post Graduate';
                        //         }
                        //         else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1) {
                        //             cStudyLevel = 'Post Graduate';
                        //         }
                        //         else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1) {
                        //             cStudyLevel = 'Research';
                        //         }
                        //         else if (String(cTitle).toLowerCase().indexOf('research') > -1 || String(cTitle).toLowerCase().indexOf('researchs') > -1) {
                        //             cStudyLevel = 'Research';
                        //         }
                        //         else {
                        //             cStudyLevel = 'Vocational / Other';
                        //         }
                        //         if (cStudyLevel) {
                        //             resJsonData.course_study_level = cStudyLevel;
                        //         }
                        //     }
                        //     break;
                        // }
                        // default: {
                        //     console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                        //     break;
                        // } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            console.log("resjson---Location---->", resJsonData.course_title);
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                console.log('course location :' + location);

                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                      //  NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                // resJsonData.basecourseid = location_wise_data.course_id;
                // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                //     if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                //         resJsonData.course_cricos_code[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_cricos_code[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                //     if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_intake.intake[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_intake.intake[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                //     if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                //     if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_campus_location[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_campus_location[i].iscurrent = false;
                //     }
                // }
                //resJsonData.course_cricos_code = location_wise_data.cricos_code;
                // resJsonData.current_course_campus_location = location_wise_data.course_campus_location;
                //resJsonData.current_course_intake = location_wise_data.course_intake;
                //resJsonData.course_intake_display = location_wise_data.course_intake;
                // resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }
    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.studyLevel);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
