[
    {
        "mappings": [{
            "course_name": ["Associate Degree in Policing Practice"],
            "mapping_scrore": "A minimum overall Academic IELTS score of 7.0 with no score below a 7.0 in each of the individual skill areas or a qualification deemed equivalent."
        },
        {
            "course_name": ["Master of Professional Accounting"],
            "mapping_scrore": "A minimum overall Academic IELTS score of 6.0 with no score below a 5.5 in each of the individual skill areas (Reading, Speaking, Listening) and no score below 6.0 in Writing, or a qualification deemed equivalent."
        },
        {
            "course_name": ["Bachelor of Medical Radiation Science"],
            "mapping_scrore": "A minimum overall Academic IELTS score of 6.5 with no score below a 6.0 in each of the individual skill areas or a qualification deemed equivalent."
        },
        {
            "course_name": ["Bachelor of Health and Rehabilitation Science",
                "Bachelor of Occupational Therapy",
                "Bachelor of Physiotherapy",
                "Bachelor of Podiatric Medicine"],
            "mapping_scrore": "A minimum overall Academic IELTS score IELTS of 6.5 with no score below a 6.5 in each of the individual skill areas or a qualification deemed equivalent."
        },
        {
            "course_name": ["Bachelor of Paramedicine",
                "Bachelor of Dental Science",
                "Bachelor of Pharmacy",
                "Bachelor of Oral Health (Therapy and Hygiene)",
                "Bachelor of Speech and Language Pathology",
                "Master of Speech Pathology"],
            "mapping_scrore": "A minimum overall Academic IELTS score of 7.0 with no score below a 6.5 in each of the individual skill areas or a qualification deemed equivalent."
        },
        {
            "course_name": ["Master of Medical Radiation Science"],
            "mapping_scrore": "A minimum overall Academic IELTS score of 7 and with no score below 7 in each of the individual skill areas or a qualification deemed equivalent."
        },
        {
            "course_name": ["Bachelor of Applied Science (Outdoor Recreation and Ecotourism)",
                "Bachelor of Education (Birth to Five Years)",
                "Bachelor of Education (Early Childhood and Primary)",
                "Bachelor of Education (Health and Physical Education)",
                "Bachelor of Education (K-12)",
                "Bachelor of Education (Secondary) - Industry Entry",
                "Bachelor of Education (Technology and Applied Studies)",
                "Bachelor of Outdoor Education",
                "Bachelor of Teaching (Primary)",
                "Bachelor of Teaching (Secondary)",
                "Master of Teaching (Primary)",
                "Master of Teaching (Secondary)"],
            "mapping_scrore": "A minimum overall Academic IELTS score of 7.5 (with no score below 7 in reading and writing, and a score of no less than 8 in speaking and listening) or a qualification deemed equivalent. Testing results must be obtained within two years from the date of your application for admission."
        },
        {
            "course_name": ["Bachelor of Nursing",
                "Bachelor of Nursing - Graduate Diploma of Clinical Practice (Paramedic)",
                "Bachelor of Veterinary Biology / Bachelor of Veterinary Science"],
            "mapping_scrore": "An Academic IELTS with a minimum overall score of 7 and with no score below 7 in each of the individual skill areas or a qualification deemed equivalent."
        }],
        "default": [{
            "mapping_scrore": "Academic IELTS with a minimum overall score of 6 with no individual score below 5.5"
        }]
    }
]