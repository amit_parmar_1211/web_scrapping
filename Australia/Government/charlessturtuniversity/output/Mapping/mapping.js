[
    {
        "mappings": [{
            "course_name": ["associate degree in policing practice"],
            "mapping_scrore": "A minimum overall Academic IELTS score of 7.0 with no score below a 7.0 in each of the individual skill areas or a qualification deemed equivalent."
        },
        {
            "course_name": ["master of professional accounting"],
            "mapping_scrore": "A minimum overall Academic IELTS score of 6.0 with no score below a 5.5 in each of the individual skill areas (Reading, Speaking, Listening) and no score below 6.0 in Writing, or a qualification deemed equivalent."
        },
        {
            "course_name": ["bachelor of medical radiation science"],
            "mapping_scrore": "A minimum overall Academic IELTS score of 6.5 with no score below a 6.0 in each of the individual skill areas or a qualification deemed equivalent."
        },
        {
            "course_name": ["bachelor of health and rehabilitation science",
                "bachelor of Occupational Therapy",
                "bachelor of Physiotherapy",
                "bachelor of Podiatric Medicine"],
            "mapping_scrore": "A minimum overall Academic IELTS score IELTS of 6.5 with no score below a 6.5 in each of the individual skill areas or a qualification deemed equivalent."
        },
        {
            "course_name": ["bachelor of paramedicine",
                "bachelor of dental science",
                "bachelor of pharmacy",
                "bachelor of oral health (therapy and hygiene)",
                "bachelor of speech and language pathology",
                "master of speech pathology"],
            "mapping_scrore": "A minimum overall Academic IELTS score of 7.0 with no score below a 6.5 in each of the individual skill areas or a qualification deemed equivalent."
        },
        {
            "course_name": ["master of medical radiation science"],
            "mapping_scrore": "A minimum overall Academic IELTS score of 7 and with no score below 7 in each of the individual skill areas or a qualification deemed equivalent."
        },
        {
            "course_name": ["bachelor of Applied science (Outdoor Recreation and Ecotourism)",
                "bachelor of Education (Birth to Five Years)",
                "bachelor of Education (Early Childhood and Primary)",
                "bachelor of Education (Health and Physical Education)",
                "bachelor of Education (K-12)",
                "bachelor of Education (Secondary) - Industry Entry",
                "bachelor of Education (Technology and Applied Studies)",
                "bachelor of Outdoor Education",
                "bachelor of Teaching (Primary)",
                "bachelor of Teaching (Secondary)",
                "master of Teaching (Primary)",
                "master of Teaching (Secondary)"],
            "mapping_scrore": "A minimum overall Academic IELTS score of 7.5 (with no score below 7 in reading and writing, and a score of no less than 8 in speaking and listening) or a qualification deemed equivalent. Testing results must be obtained within two years from the date of your application for admission."
        },
        {
            "course_name": ["bachelor of Nursing",
                "bachelor of Nursing - Graduate Diploma of Clinical Practice (Paramedic)",
                "bachelor of Veterinary Biology / bachelor of Veterinary science"],
            "mapping_scrore": "An Academic IELTS with a minimum overall score of 7 and with no score below 7 in each of the individual skill areas or a qualification deemed equivalent."
        }],
        "default": [{
            "mapping_scrore": "Academic IELTS with a minimum overall score of 6 with no individual score below 5.5"
        }]
    }
]