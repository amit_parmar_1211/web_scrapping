
const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to https://futurestudents.csu.edu.au/courses/all
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
      var subjectAreasArray = [];
      var codeList = [];
      var datalist = [];
      var click = await page.$x("//*[@id='modal-international']");
      await page.waitFor(1000);
      if (click[0]) {
        await click[0].click();
        //console.log("clicked-->")
      }
      await page.waitFor(1000);
      let selector = "#intoncampus";
      await page.evaluate((selector) => document.querySelector(selector).click(), selector);
      const subjects = await page.$x("//*[@id='calp-list']/option[not(contains(text(),'Career Area'))]");
      for (let country of subjects) {
        let name = await page.evaluate(el => el.innerText, country);
        let code = await page.evaluate(el => el.value, country);
        subjectAreasArray.push({ name: name, code: code });
      }
      //console.log("country length-->" + subjectAreasArray.length);



      await page.waitFor(1000);

      for (let countrycode of subjectAreasArray) {
        let studyLevelSel = await page.$x('//div[@aria-label="Filter courses by course level"]//label');

        await page.select("#calp-list", countrycode.code);
        for (let studyLeveli of studyLevelSel) {


          await studyLeveli.click();



          await page.waitFor(1000);

          const title = await page.$x("//*[@id='all-courses-list']/ul/li[not(@style='display: none;')]/a");
          const description = await page.$x("//*[@id='all-courses-list']/ul/li[not(@style='display: none;')]/a");
          //console.log("studyleveli--->", studyLeveli);
          // const studyLeveli = await page.$x('//div[@aria-label="Filter courses by course level"]//label/text()')
          for (let i = 0; i < title.length; i++) {
            let titletext = await page.evaluate(el => el.innerText, title[i]);
            let descriptiontext = await page.evaluate(el => el.href, description[i]);
            let studyLevelText = await page.evaluate(el => el.innerText, studyLeveli);
           // console.log("studyleveltext-->", studyLevelText);
            datalist.push({ innerText: titletext, href: descriptiontext, studyLevel: studyLevelText, category: countrycode.name.replace(/[\r\n\t]+/g, '') });
          }

          //codeList.push({ name: name, code: code, data: values });
        }



      }
      fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(datalist));
      // fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(titletext))
      await fs.writeFileSync("./output/charlessturtuniversity_original_courselist.json", JSON.stringify(datalist));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < datalist.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (datalist[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, studyLevel: datalist[i].studyLevel, category: [] });
          }
        } else {
          uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, studyLevel: datalist[i].studyLevel, category: [] });
        }
      }


      await fs.writeFileSync("./output/charlessturtuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories

      for (let i = 0; i < datalist.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == datalist[i].href) {
            if (uniqueUrl[j].category.includes(datalist[i].category)) {

            } else {
              uniqueUrl[j].category.push(datalist[i].category);
            }

          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/charlessturtuniversity_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }


  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  // refers to https://futurestudents.csu.edu.au/courses/all
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // course_level_form_selector key
      // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelList) {
      //   console.log(funcName + 'Invalid formSelList');
      //   throw (new Error('Invalid formSelList'));
      // }
      // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelElementDict) {
      //   console.log(funcName + 'Invalid formSelElementDict');
      //   throw (new Error('Invalid formSelElementDict'));
      // }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');

      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
