const puppeteer = require('puppeteer');

testText();
async function testText(inputText) {
    const URL = "https://futurestudents.csu.edu.au/courses/agricultural-wine-sciences/graduate-diploma-sustainable-agriculture";
    let browser = await puppeteer.launch({ headless: true });
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    const title = await page.$x("//*[@id='cYear-duration']/ul/li");
    let text = await page.evaluate(el => el.textContent, title[0]);
    var data = await giveMeArray(text.replace(/[\r\n\t]+/g, ";"), ";")
    console.log(text)
    console.log(JSON.stringify(data));
    var courseName = "Graduate Diploma of Sustainable Agriculture";
    for (let i = 0; i < data.length; i++) {
        var ismatch = false, strdata = "";
        if (data[i].trim().toLowerCase() == courseName.toLowerCase().trim()) {
            console.log("Match");
            ismatch=true;
            if (data[i + 1].trim().toLowerCase().indexOf("full-time") != -1 || data[i + 1].trim().toLowerCase().indexOf("part-time") != -1) {
                strdata += data[i + 1];
            }
            if (data[i + 2].trim().toLowerCase().indexOf("full-time") != -1 || data[i + 2].trim().toLowerCase().indexOf("part-time") != -1) {
                strdata +=" "+ data[i + 2];
            }
        }
        if (ismatch) {
            console.log("data-->" + strdata)
            break;
        }
    }
    await browser.close()
}
async function giveMeArray(rootstring, seperator) {
    console.log("Called array-->" + rootstring)
    var array_data = rootstring.replace(/\s/g, ' ').split(seperator);
    return await array_data;
}