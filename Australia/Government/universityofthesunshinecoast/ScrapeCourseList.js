const fs = require("fs");
const Scrape = require("./common/scrape").Scrape;
const configs = require("./configs");
class ScrapeCourseList extends Scrape {
  async scrapeCourseListAndPutAtS3() {
    const funcName = "startScrappingFunc ";
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var datalist = [];
      await s.setupNewBrowserPage("https://www.usc.edu.au/");

      var URLS = ["https://www.usc.edu.au/time-to-shine/find-your-program"];

      var mainCategory = [];
      for (let url of URLS) {
        //scrape main category
        await s.page.goto(url, { timeout: 0 });
        const maincategoryselector = "//*[@id='content']/div/div[1]/ul/li/a";
        const text = "//*[@id='content']/div/div[1]/ul/li/a/span";
        //const study_level="//*[@id='top']/div[1]/div[1]/div/div/h3"
        var category = await s.page.$x(maincategoryselector);
        var categorytext = await s.page.$x(text);
        for (let i = 0; i < category.length; i++) {
          const href = await s.page.evaluate(el => el.href, category[i]);
          const innertexts = await s.page.evaluate(
            el => el.innerText,
            categorytext[i]
          );

          mainCategory.push({ href: href, innertext: innertexts });
          console.log("mainCategory", mainCategory);
        }

        for (let i = 0; i < mainCategory.length; i++) {
          await s.page.goto(mainCategory[i].href);
          const category = mainCategory[i].innertext;

          const study_level = "//*[@id='exploreYourStudyOptions']/div/h5";
          const levels = await s.page.$x(study_level);

          for (let j = 0; j < levels.length; j++) {
            let catc;
            const studylevel = await s.page.evaluate(
              el => el.innerText,
              levels[j]
            );

            let finalstudylevel = studylevel.trim();
            if (String(finalstudylevel).includes(" ")) {
              let spites = String(finalstudylevel).split(" ")[0];
              // console.log("catc", String(finalstudylevel));
              let convert = spites.toLowerCase();
              let con = convert.charAt(0).toUpperCase();
              catc = con.concat(spites.toLowerCase().slice(1));

              //console.log("catc", catc);
            } else {
              let convert = finalstudylevel.toLowerCase();
              let con = convert.charAt(0).toUpperCase();
              catc = con.concat(finalstudylevel.toLowerCase().slice(1));
              //console.log(catc);
            }

            const courseurl =
              "//*[@id='exploreYourStudyOptions']/div/h5[contains(.,'" +
              catc +
              "')]/following::ul[1]/li/a";
            console.log("studylevel", courseurl);
            const url = await s.page.$x(courseurl);
            for (let i = 0; i < url.length; i++) {
              const urlstring = await s.page.evaluate(el => el.href, url[i]);
              const innertext = await s.page.evaluate(
                el => el.innerText,
                url[i]
              );
              datalist.push({
                href: urlstring,
                innerText: innertext,
                study_level: studylevel,
                category: category
              });
              console.log("mainCategory", datalist);
            }
          }
        }

        //scrape courses
      }

      fs.writeFileSync(
        "./output/universityofthesunshinecoast_original_courselist.json",
        JSON.stringify(datalist)
      );
      fs.writeFileSync(
        "./output/maincategorylist.json",
        JSON.stringify(mainCategory)
      );
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < datalist.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (datalist[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({
              href: datalist[i].href,
              innerText: datalist[i].innerText,
              category: [],
              study_level: datalist[i].study_level
            });
          }
        } else {
          uniqueUrl.push({
            href: datalist[i].href,
            innerText: datalist[i].innerText,
            category: [],
            study_level: datalist[i].study_level
          });
        }
      }
      //

      await fs.writeFileSync(
        "./output/universityofthesunshinecoast_unique_courselist.json",
        JSON.stringify(uniqueUrl)
      );

      //based on unique urls mapping of categories
      for (let i = 0; i < datalist.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == datalist[i].href) {
            if (uniqueUrl[j].category.includes(datalist[i].category)) {
            } else {
              uniqueUrl[j].category.push(datalist[i].category);
            }
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync(
        "./output/universityofthesunshinecoast_courselist.json",
        JSON.stringify(uniqueUrl)
      );
      console.log(
        funcName + "writing courseList to file completed successfully...."
      );
      await s.browser.close();
      console.log(funcName + "browser closed successfully.....");
      return true;
    } catch (error) {
      console.log(funcName + "try-catch error = " + error);
      throw error;
    }
  }
}
module.exports = { ScrapeCourseList };
