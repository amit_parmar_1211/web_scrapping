const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_category, study_level) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = course_category;
                            break;
                        }
                        case 'course_title': {
                            const title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            const ctitle = format_functions.titleCase(title)
                            resJsonData.course_title = ctitle
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            let programType = null;
                            let category = null;
                            let cStudyLevels = null;
                            let newcourse = null;
                            var titles = null;
                            var ietlsss = null;
                            //progress bar start
                            const penglishList = [], pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            const ieltsDict = {}; const ibtDict = {}; const pteDict = {}; const utasDict = {}; let ieltsMapNumber = null; let ieltsNumber = null; const cpeDict = {};

                            // english requirement
                            if (courseScrappedData.course_toefl_ielts_score) {
                                const title = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                                const ielts = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                                // const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                                console.log("cTitle :" + JSON.stringify(title));
                                console.log("ieltsScore :" + JSON.stringify(ielts));





                                var course = [];
                                ielts.forEach(element => {
                                    if (element['Courses']) {

                                        course.push(element['Courses']);

                                    }

                                });
                                const coursearray = String(course).split(",");

                                console.log("arrcourse a1-->" + course);
                                console.log("coursearray-->" + JSON.stringify(coursearray));
                                console.log("arrcourse a2-->" + course.length);
                                console.log("coursearray1-->" + JSON.stringify(coursearray.length));

                                // console.log("@@@coursearr:"+coursearray.indexOf(title));
                                var arr = coursearray.includes(title);
                                console.log("@@@coursearr1:" + JSON.stringify(arr));

                                if (coursearray.includes(title)) {
                                    for (var i = 0; i < ielts.length; i++) {
                                        var newtitle = ielts[i].Courses;


                                        //console.log("print titles :"+newtitle);
                                        var titles = newtitle.indexOf(title);
                                        // console.log("print titles :"+ JSON.stringify(titles));

                                        if (newtitle.includes(title)) {
                                            titles = i;
                                            console.log("print titles :" + titles);
                                            ietlsss = ielts[titles].ielts;
                                            var ibt = ielts[titles].ibt;
                                            var pbt = ielts[titles].pbt;
                                            var pte = ielts[titles].pte;
                                            var cae = ielts[titles].cae;

                                        }

                                    }
                                    /*  console.log("print title :"+titles);
                                    var a = ielts[7].ielts;
                                    var b = ielts.length;
                                  
                                   
                                    console.log("print title :"+ JSON.stringify(a));
                                    console.log("print title b :"+ JSON.stringify(b));
                                    console.log("print title c :"+ c);
                                   // var categorys=ielts[title].ielts;*/



                                    // englishList.push({ name: "ielts academic", description: ietlsss });
                                    // englishList.push({ name: "toefl ibt", description: ibt });
                                    //englishList.push({ name: "toefl pbt", description: pbt });
                                    //englishList.push({ name: "pte academic", description: pte });
                                    //englishList.push({ name: "cae", description: cae });
                                    console.log("new course" + JSON.stringify(englishList));


                                    var pieltlsss1 = ietlsss;
                                    var pibt1 = ibt;
                                    var ppbt1 = pbt;
                                    var ppte1 = pte;
                                    var pcae1 = cae;
                                    // console.log("print title :"+ JSON.stringify(potherLngDicts));
                                    var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "", pbtScore = "";
                                    if (pieltlsss1) {
                                        ieltsScore = await utils.giveMeNumber(pieltlsss1);
                                        console.log("### IELTS data-->" + ieltsScore);
                                    }
                                    if (pibt1) {
                                        ibtScore = await utils.giveMeNumber(pibt1);
                                    }
                                    if (ppbt1) {
                                        pbtScore = await utils.giveMeNumber(ppbt1);
                                    }else{
                                        pbtScore="NA"
                                    }
                                    if (pcae1) {
                                        caeScore = await utils.giveMeNumber(pcae1);
                                    }
                                    if (ppte1) {
                                        pteScore = await utils.giveMeNumber(ppte1);
                                        console.log("### IELTS datas-->" + pteScore);
                                    }
                                    if (ieltsScore != "NA") {
                                        pieltsDict.name = 'ielts academic';
                                        pieltsDict.description = ietlsss
                                        pieltsDict.min = 0;
                                        pieltsDict.require = ieltsScore;
                                        pieltsDict.max = 9;
                                        pieltsDict.R = 0;
                                        pieltsDict.W = 0;
                                        pieltsDict.S = 0;
                                        pieltsDict.L = 0;
                                        pieltsDict.O = 0;
                                        englishList.push(pieltsDict);
                                    }
                                    if (ibtScore != "NA") {
                                        pibtDict.name = 'toefl ibt';
                                        pibtDict.description = ibt
                                        pibtDict.min = 0;
                                        pibtDict.require = ibtScore;
                                        pibtDict.max = 120;
                                        pibtDict.R = 0;
                                        pibtDict.W = 0;
                                        pibtDict.S = 0;
                                        pibtDict.L = 0;
                                        pibtDict.O = 0;
                                        englishList.push(pibtDict);
                                    }
                                    if (pbtScore != "NA") {
                                        ppbtDict.name = 'toefl pbt';
                                        ppbtDict.description = pbt
                                        ppbtDict.min = 310;
                                        ppbtDict.require = pbtScore;
                                        ppbtDict.max = 677;
                                        ppbtDict.R = 0;
                                        ppbtDict.W = 0;
                                        ppbtDict.S = 0;
                                        ppbtDict.L = 0;
                                        ppbtDict.O = 0;
                                        englishList.push(ppbtDict);
                                    }
                                    if (pteScore != "NA") {
                                        ppteDict.name = 'pte academic';
                                        ppteDict.description = pte
                                        ppteDict.min = 0;
                                        ppteDict.require = pteScore;
                                        ppteDict.max = 90;
                                        ppteDict.R = 0;
                                        ppteDict.W = 0;
                                        ppteDict.S = 0;
                                        ppteDict.L = 0;
                                        ppteDict.O = 0;
                                        englishList.push(ppteDict);
                                    }
                                    if (caeScore != "NA") {
                                        pcaeDict.name = 'cae';
                                        pcaeDict.description = cae
                                        pcaeDict.min = 80;
                                        pcaeDict.require = caeScore;
                                        pcaeDict.max = 230;
                                        pcaeDict.R = 0;
                                        pcaeDict.W = 0;
                                        pcaeDict.S = 0;
                                        pcaeDict.L = 0;
                                        pcaeDict.O = 0;
                                        englishList.push(pcaeDict);
                                    }
                                } else {
                                    const cStudyLevels = resJsonData.course_study_level

                                    // if (String(ieltsScores).toLowerCase().indexOf('bachelor') > -1 || String(ieltsScores).toLowerCase().indexOf('bachelors') > -1 || String(ieltsScores).toLowerCase().indexOf('advanced diploma') > -1 || String(ieltsScores).toLowerCase().indexOf('associate') > -1) {
                                    //     cStudyLevels = 'UNDERGRADUATE';
                                    // }
                                    // else if (String(ieltsScores).toLowerCase().indexOf('master by research') > -1 || String(ieltsScores).toLowerCase().indexOf('master by researchs') > -1 || String(ieltsScores).toLowerCase().indexOf('master research') > -1 || String(ieltsScores).toLowerCase().indexOf('master by researchs') > -1) {
                                    //     cStudyLevels = 'RESEARCH';
                                    // }
                                    // else if (String(ieltsScores).toLowerCase().indexOf('research') > -1 || String(ieltsScores).toLowerCase().indexOf('researchs') > -1 || String(ieltsScores).toLowerCase().indexOf('doctor') > -1
                                    //     || String(ieltsScores).toLowerCase().indexOf('doctors') > -1) {
                                    //     cStudyLevels = 'RESEARCH';
                                    // }
                                    // else if (String(ieltsScores).toLowerCase().indexOf('graduate') > -1 || String(ieltsScores).toLowerCase().indexOf('graduates') > -1) {
                                    //     cStudyLevels = 'POSTGRADUATE';
                                    // }
                                    // else if (String(ieltsScores).toLowerCase().indexOf('master') > -1 || String(ieltsScores).toLowerCase().indexOf('masters') > -1) {
                                    //     cStudyLevels = 'POSTGRADUATE';
                                    // }
                                    console.log("StudyLevel!!!" + JSON.stringify(cStudyLevels));
                                    if (cStudyLevels) {
                                        const othercourse = await utils.getValueFromHardCodedJsonFile('otherCourses');
                                        console.log("othercourses@@@" + JSON.stringify(othercourse[cStudyLevels]));

                                        // englishList.push({ name: "ielts academic", description: othercourse[cStudyLevels].ielts });
                                        //englishList.push({ name: "toefl ibt", description: othercourse[cStudyLevels].ibt });
                                        //englishList.push({ name: "toefl pbt", description: othercourse[cStudyLevels].pbt });
                                        //englishList.push({ name: "pte academic", description: othercourse[cStudyLevels].pte });
                                        // englishList.push({ name: "cae", description: othercourse[cStudyLevels].cae });
                                        const otherLngDict = othercourse[cStudyLevels];
                                        const potherLngDict = otherLngDict;
                                        if (potherLngDict) {
                                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "", pbtScore = "";
                                            if (potherLngDict.ielts) {
                                                ieltsScore = await utils.giveMeNumber(potherLngDict.ielts);
                                                console.log("### IELTS data-->" + ieltsScore);
                                            }
                                            if (potherLngDict.ibt) {
                                                ibtScore = await utils.giveMeNumber(potherLngDict.ibt);
                                            }
                                            if (potherLngDict.pte) {
                                                pteScore = await utils.giveMeNumber(potherLngDict.pte);
                                            }
                                            if (potherLngDict.cae) {
                                                caeScore = await utils.giveMeNumber(potherLngDict.cae);
                                            }
                                            if (potherLngDict.pbt) {
                                                pbtScore = await utils.giveMeNumber(potherLngDict.pbt);
                                                console.log("### IELTS datas-->" + pbtScore);
                                            }else{
                                                pbtScore="NA"
                                            }
                                            if (ieltsScore != "NA") {
                                                pieltsDict.name = 'ielts academic';
                                                pieltsDict.description = othercourse[cStudyLevels].ielts
                                                pieltsDict.min = 0;
                                                pieltsDict.require = ieltsScore;
                                                pieltsDict.max = 9;
                                                pieltsDict.R = 0;
                                                pieltsDict.W = 0;
                                                pieltsDict.S = 0;
                                                pieltsDict.L = 0;
                                                pieltsDict.O = 0;
                                                englishList.push(pieltsDict);
                                            }
                                            if (ibtScore != "NA") {
                                                pibtDict.name = 'toefl ibt';
                                                pibtDict.description = othercourse[cStudyLevels].ibt
                                                pibtDict.min = 0;
                                                pibtDict.require = ibtScore;
                                                pibtDict.max = 120;
                                                pibtDict.R = 0;
                                                pibtDict.W = 0;
                                                pibtDict.S = 0;
                                                pibtDict.L = 0;
                                                pibtDict.O = 0;
                                                englishList.push(pibtDict);
                                            }
                                            if (pbtScore != "NA") {
                                                ppbtDict.name = 'toefl pbt';
                                                ppbtDict.description = othercourse[cStudyLevels].pbt;
                                                ppbtDict.min = 310;
                                                ppbtDict.require = pbtScore;
                                                ppbtDict.max = 677;
                                                ppbtDict.R = 0;
                                                ppbtDict.W = 0;
                                                ppbtDict.S = 0;
                                                ppbtDict.L = 0;
                                                ppbtDict.O = 0;
                                                englishList.push(ppbtDict);

                                            }
                                            if (pteScore != "NA") {
                                                ppteDict.name = 'pte academic';
                                                ppteDict.description = othercourse[cStudyLevels].pte;
                                                ppteDict.min = 0;
                                                ppteDict.require = pteScore;
                                                ppteDict.max = 90;
                                                ppteDict.R = 0;
                                                ppteDict.W = 0;
                                                ppteDict.S = 0;
                                                ppteDict.L = 0;
                                                ppteDict.O = 0;
                                                englishList.push(ppteDict);
                                            }
                                            if (caeScore != "NA") {
                                                pcaeDict.name = 'cae';
                                                pcaeDict.description = othercourse[cStudyLevels].cae;
                                                pcaeDict.min = 80;
                                                pcaeDict.require = caeScore;
                                                pcaeDict.max = 230;
                                                pcaeDict.R = 0;
                                                pcaeDict.W = 0;
                                                pcaeDict.S = 0;
                                                pcaeDict.L = 0;
                                                pcaeDict.O = 0;
                                                englishList.push(pcaeDict);
                                            }
                                            //}
                                        }
                                    }
                                }
                            }
                            console.log("English--->", englishList)

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            } else {
                                courseAdminReq.english = []
                            }

                            if (courseScrappedData.course_academic_requirement) {
                                var arr = [];
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                arr.push(academicReq)
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = arr;
                                }else{
                                    courseAdminReq.academic=["To check the academic requirement for your degree program, please see this link https://www.usc.edu.au/study/international-students/how-to-apply/step-2-check-entry-requirements/undergraduate-program-admission-requirements"]
                                    
                                }
                                if(academicReq==null){
                                    courseAdminReq.academic=["To check the academic requirement for your degree program, please see this link https://www.usc.edu.au/study/international-students/how-to-apply/step-2-check-entry-requirements/undergraduate-program-admission-requirements"]
                                }

                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }

                            // add entry requirement 'entry_more_details' link
                            const courseKeyVal = courseScrappedData.entry_requirements_url;
                            console.log("EntryRequirement:::" + courseKeyVal);
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            //english requirement 'english_more_details'
                            const courseKeyValEnglishReq = courseScrappedData.english_requirements_url;
                            console.log("EnglishRequirement:::" + courseKeyValEnglishReq);
                            let resEnglishReqJson = null;
                            if (Array.isArray(courseKeyValEnglishReq)) {
                                for (const rootEleDict of courseKeyValEnglishReq) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            const courseKeyValAcademic = courseScrappedData.academic_requirements_url;
                            console.log("AcademicRequirement:::" + courseKeyValAcademic);
                            let resAcademicReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyValAcademic)) {
                                for (const rootEleDict of courseKeyValAcademic) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAcademicReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqJson;
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqJson;
                            }
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                if(fullTimeText=="1.5 years; 1 year accelerated"){
                                    fullTimeText="1.5 years"
                                }
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("FilterData---->" + JSON.stringify(filtered_duration_formated))
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {

                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':

                        case 'course_campus_location': { // Location Launceston

                            const courseKeyVals = courseScrappedData.course_cricos_code;
                            let course_cricos_code = [];
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                for (let sel of selList) {
                                                    if (sel.indexOf(';') > -1) {
                                                        let splitCricos = sel.split(';');
                                                        for (let cricos of splitCricos) {
                                                            if (cricos.toLowerCase().indexOf('online') > -1) {

                                                            } else {
                                                                course_cricos_code.push(cricos);
                                                            }
                                                        }
                                                    } else if (sel.length > 0) {
                                                        course_cricos_code.push(sel);
                                                    }

                                                }

                                            }
                                        }
                                    }
                                }
                            }
                           if(!course_cricos_code){
                            throw new Error("CRICOS CODE not found");
                           }
                            var campLocationText = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            var campusss = [];
                            var campuss = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selLists = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {

                                                campLocationText = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            //console.log("##Campus-->" + campLocationText.trim())
                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = String(campLocationText).trim();
                                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);

                                if (campLocationText && campLocationText.length > 0) {
                                   
                                    // var campuss = await utils.giveMeArray(campLocationValTrimmed, '(/[\r\n\t ]+/g,'')');
                                    //var campuss = await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                                    campLocationText.forEach(element => {
                                        if(element=="Sunshine Coast"){
                                           element= "Sippy Downs"
                                        }
                                        campuss.push(element.replace('^', '').trim())
                                    })

                                    console.log("Campuses@@@" + JSON.stringify(campuss))
                                    //  let campuss = ['Sippy Downs', 'Fraser Coast', 'Gympie', 'Caboolture', 'SouthBank', 'Moreton Bay']
                                    if (campuss.length > 0) {
                                        let campuses = ['Sippy Downs', 'Fraser Coast', 'Gympie', 'Caboolture', 'SouthBank', 'Moreton Bay']
                                        campuss.forEach(element => {
                                            if (campuses.includes(element)) {
                                                console.log("YESSSS location is there")
                                                for (var i = 0; i < campuses.length; i++) {
                                                    if (campuses[i] == element) {
                                                        campusss.push(campuses[i])
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                                var campusedata = [];
                                campusss.forEach(element => {
                                    var cam = format_functions.titleCase(element)
                                    campusedata.push({
                                        "name": cam,
                                       "code":String(course_cricos_code)
                                    })
                                });




                                console.log("Campuses@@@" + JSON.stringify(campusedata))                            
                                resJsonData.course_campus_location = campusedata;//await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                                resJsonData.course_study_mode = 'On campus';//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                                // }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }

                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {};

                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            const feesIntStudents = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_first_year)
                            // const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            console.log("All fees-->", JSON.stringify(feesIntStudents))
                          
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const arrval = String(feesWithDollorTrimmed).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student={
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: feesIntStudent,
                                               
                                            };

                                        }
                                    }
                                }
                            } else {
                                feesDict.international_student={
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",
                                    description: feesIntStudent,
                                   
                                };
                            }
                           

                          
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }

                        case 'course_intake_url': {
                            const courseKeyVal = courseScrappedData.course_intake_url;
                            var url = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                url = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (url.length > 0) {
                                resJsonData.course_intake_url = url;
                            }
                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            // if (program_code.length > 0) {
                            resJsonData.program_code = String(program_code);
                            //}
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            // Semester 1 (February) and Semester 2 (August)
                            // prepare emty JSON struct
                            const courseIntakeDisplay = {};
                            // existing intake value
                            var courseIntakeStr = null;//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                courseIntakeStr = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("NEWIntake@data-->", JSON.stringify(courseIntakeStr))
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var arr = []
                                // const intake = await utils.getValueFromHardCodedJsonFile('course_intake');
                                //console.log("INTAKE FROM HARDCODE---" + JSON.stringify(courseIntakeStr.indexOf('&')));
                                if (String(courseIntakeStr).includes("or")) {

                                    var splitdata = String(courseIntakeStr).split("or ");
                                    console.log("SplitData" + JSON.stringify(splitdata))
                                    arr = splitdata
                                }
                                else if (String(courseIntakeStr).includes("and")) {
                                    var splitdata = String(courseIntakeStr).split("and");
                                    console.log("SplitData" + JSON.stringify(splitdata))
                                    arr = splitdata
                                } else {
                                    splitdata = courseIntakeStr
                                }
                                console.log("IntakeArr--->" + JSON.stringify(arr))
                                var campus = resJsonData.course_campus_location;
                                var intakes = [];
                                console.log("Intake length-->" + courseIntakeStr);
                                console.log("Campus length-->" + campus);
                                var intakedetail = {};
                                var myintake = [];
                                var courseIntakeStrarr = [];
                                var myintake_data = JSON.parse(fs.readFileSync("./selectors/university_hard_coded_data.json"))[0];
                                splitdata.forEach(element => {
                                    if (element.toLowerCase().trim().includes("semester 1")) {
                                        console.log("Match--> s1")
                                        myintake.push(myintake_data["Semester1"]);
                                    }
                                    console.log("INCLUDES OR NOT" + JSON.stringify(element.indexOf("Semester 2")))

                                    if (element.includes("Semester 2") || element.includes("Semester 2") || element.includes("2^")) {
                                        console.log("Match--> s2")
                                        myintake.push(myintake_data["Semester2"]);
                                    }
                                    if (element.toLowerCase().trim().includes("trimester 3")) {
                                        console.log("Match--> t3")
                                        myintake.push(myintake_data["Trimester3"]);
                                    }
                                    if(element.includes("Session 1")){
                                        myintake.push(myintake_data["Session 1"]);
                                    }
                                    if(element.includes("Session 2")){
                                        myintake.push(myintake_data["Session 2"]);
                                    }
                                    if(element.includes("Session 3")){
                                        myintake.push(myintake_data["Session 3"]);
                                    }
                                    if(element.includes("Session 4")){
                                        myintake.push(myintake_data["Session 4"]);
                                    }
                                    if(element.includes("Session 5")){
                                        myintake.push(myintake_data["Session 5"]);
                                    }
                                    if(element.includes("Session 6")){
                                        myintake.push(myintake_data["Session 6"]);
                                    }
                                    if(element.includes("Session 7")){
                                        myintake.push(myintake_data["Session 7"]);

                                    }
                                    if(element.includes("Session 8")){
                                        myintake.push(myintake_data["Session 8"]);
                                    }
                                });

                                console.log("Finalintake" + JSON.stringify((myintake)));
                                for (var camcount = 0; camcount < campus.length; camcount++) {
                                    // console.log("intake -->" + count)

                                    var intakedetail = {};
                                    if (String(myintake) == []) {
                                        console.log("myintakemyintake -->" + myintake)
                                        intakedetail.name = campus[camcount].name;
                                        intakedetail.value = ["anytime"]
                                    } else {
                                        intakedetail.name = campus[camcount].name;
                                        intakedetail.value = myintake;
                                    }
                                    //intakedetail.campus = campus[count];


                                    intakes.push(intakedetail);
                                }
                                var intakedata = {};
                                let formatedIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                console.log("NEw Intake Format@@@@@" + JSON.stringify(formatedIntake))
                                intakedata.intake = formatedIntake;
                                var intakeUrl = [];
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                resJsonData.course_intake = intakedata
                            }
                            else {
                                var intakes = [];
                                for (var camcount = 0; camcount < campus.length; camcount++) {
                                    // console.log("intake -->" + count)
                                    var intakedetail = {};
                                    //intakedetail.campus = campus[count];
                                    intakedetail.name = campus[camcount].name;
                                    intakedetail.value = [];
                                    intakes.push(intakedetail);
                                }
                                var intakedata = {};
                               // var intakeUrl = [];
                                //intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedata.intake = intakes;
                                console.log("intakedata.intake  -->" + intakedata.intake)
                                intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
                            }
                            break;
                        }

                     
                        
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            console.log("overview---" + JSON.stringify(courseKeyVal));
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_overview = resAccomodationCostJson;
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            let outcome_data=null
                            const courseKeyVal =courseScrappedData.course_career_outcome
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                outcome_data = selList;
                                            }
                                        }
                                    }
                                }
                            }

                            if (outcome_data) {
                                //var outcome = await utils.giveMeArray(outcome_data.replace(/[\r\n\t]+/g, ';'), ';');
                                if (outcome_data.length > 0) {
                                    resJsonData.course_career_outcome = outcome_data;
                                    console.log("#### Outcome is--->" + resJsonData.course_career_outcome);
                                } else {
                                    resJsonData.course_career_outcome = [];
                                }
                            }
                            else {
                                resJsonData.course_career_outcome = [];
                            }
                            console.log("#### Outcome is--->" + resJsonData.course_career_outcome);
                            break;
                        }


                        case 'course_study_level': {
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            const level = study_level;
                            var splitdata
                            if (level.includes('(undergraduate programs)')) {
                                splitdata = level.split('(undergraduate programs)')[0]
                            } else {
                                splitdata = level
                            }
                            resJsonData.course_study_level = splitdata.trim();


                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                             const courseKeyVal_major = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline);
                             const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                             let course_outlines = {};
                             if (courseKeyVal_minor != null) {
                                 let arr=[];
                                 arr.push(courseKeyVal_minor)
                                 course_outlines.minors = arr
                             } else {
                                 course_outlines.minors = []
                             }
                             if (courseKeyVal_major != null) {
                                let arr=[];
                                arr.push(courseKeyVal_major)
                                 course_outlines.majors = arr
                             } else {
                                 course_outlines.majors = []
                             } 
                             if (courseKeyVal != null) {
                                 course_outlines.more_details = resJsonData.course_url
                             } else {
                                 course_outlines.more_details =  resJsonData.course_url
                             }
                             resJsonData.course_outline = course_outlines
                             break;
                         }
                         case 'application_fees':{
                             const courseKeyVal=await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                             if(courseKeyVal != null){
                                 resJsonData.application_fee=courseKeyVal
                             }else{
                                 resJsonData.application_fee=""
                             }
                         }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)

            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;        
                var filelocation = "./output/" + resJsonData.univ_id + "_" +basecourseid.replace(/[\n :""]+/g, '').replace(/[\/]+/g, '_') + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.category, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };





