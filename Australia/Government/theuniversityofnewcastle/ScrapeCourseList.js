const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');

const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to https://www.newcastle.edu.au/degrees
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      // course_level_selector key
      const courseListSel = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!courseListSel) {
        console.log(funcName + 'Invalid courseListSel');
        throw (new Error('Invalid courseListSel'));
      }
      const elementDictForCourseListSel = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCourseListSel) {
        console.log(funcName + 'Invalid elementDictForCourseListSel');
        throw (new Error('Invalid elementDictForCourseListSel'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      // console.log('******************Click on check box button*****************************');
      // const checkboxinternational = await s.page.$('#handbook-filters > fieldset:nth-child(6) > ul > li:nth-child(2) > label > input[type="checkbox"]');
      // await checkboxinternational.click();
      // console.log('******************END click*****************************************');
      // anchor list selector of course list
      const totalCourseList = await s.scrapeElement(elementDictForCourseListSel.elementType, courseListSel, rootEleDictUrl, null);
      console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));

      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
     
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return totalCourseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // refers to https://www.newcastle.edu.au/degrees
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // course_level_form_selector key
      // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelList) {
      //   console.log(funcName + 'Invalid formSelList');
      //   throw (new Error('Invalid formSelList'));
      // }
      // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelElementDict) {
      //   console.log(funcName + 'Invalid formSelElementDict');
      //   throw (new Error('Invalid formSelElementDict'));
      // }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
     
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  async scrapeCourseListCategorywise() {
    const funcName = 'scrapeCourseListCategorywise ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];
      await s.setupNewBrowserPage("https://www.newcastle.edu.au/degrees#filter=intake_international");
      console.log('scrape course category wise list');

      var mainCategory = []
      const maincategoryselector = "//*[@id='handbook-filters']/fieldset/legend[contains(text(),'Study Area')]/following-sibling::ul/li/label";
      var category = await s.page.$x(maincategoryselector);
      var categorystringmain = "", categorymainlink = "";
      var targetLinksCardsTotal = [];

      for (let link of category) {
        categorystringmain = await s.page.evaluate(el => el.innerText, link);

        var categoryNameArr = categorystringmain.toString().split('\n');
        var catogeryname = categoryNameArr[0].toString().trim();
        //mainCategory.push(catogeryname);

        const maincategoryselector = "//*[@id='handbook-filters']/fieldset/legend[contains(text(),'Study Area')]/following-sibling::ul/li/label[contains(text(),'" + catogeryname + "')]";
        console.log('Category url :' + maincategoryselector);

        var categorylink = await s.page.$x(maincategoryselector);
        console.log('course listing file university :' + categorylink);

        await categorylink[0].click();
        await s.page.waitFor(2000);

        categorymainlink = await s.page.url();
        targetLinksCardsTotal.push({ href: categorymainlink, innerText: catogeryname });

        console.log("url for final :" + categorymainlink);
        const selector = ".handbook-degree-listing > tbody > tr:not([style*='display: none']) > td.title > a";
        var category = await s.page.$$(selector);
        for (let link of category) {
          var categorystring = await s.page.evaluate(el => el.innerText, link);
          var categoryurl = await s.page.evaluate(el => el.href, link);
          totalCourseList.push({ href: categoryurl, innerText: categorystring, category: catogeryname });
        }

        await categorylink[0].click();
        await s.page.waitFor(2000);
      }
      await fs.writeFileSync("./course_list/main_category_courses.json", JSON.stringify(targetLinksCardsTotal))
      await fs.writeFileSync("./course_list/theuniversityofnewcastle_original_courselist.json", JSON.stringify(totalCourseList));

      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }

      await fs.writeFileSync("./course_list/theuniversityofnewcastle_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./course_list/theuniversityofnewcastle_courselist.json", JSON.stringify(uniqueUrl));

      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
