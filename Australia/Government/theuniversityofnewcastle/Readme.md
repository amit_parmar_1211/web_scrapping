**The University of Newcastle Australia (UON) : Total Courses Details As On 20 Mar 2019**
* Total Courses = 185
* Courses without CRICOS code = 19
* Total available courses = 185
* Record Uploaded = all record

[
    {
        "href": "https://www.newcastle.edu.au/degrees/master-business-administration-mba", //not given information
        "innerText": "Master of Business Administration (MBA)"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/master-business-research",  //not availabe cricos code
        "innerText": "Master of Business Research"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/graduate-certificate-clinical-nursing", //not availabe cricos code
        "innerText": "Graduate Certificate in Clinical Nursing"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/master-cyber-security", //not availabe cricos code
        "innerText": "Master of Cyber Security"
    },
	{
        "href": "https://www.newcastle.edu.au/degrees/graduate-certificate-data-analytics", //not availabe cricos code
        "innerText": "Graduate Certificate in Data Analytics"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/master-data-science", //not availabe cricos code
        "innerText": "Master of Data Science"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/bachelor-of-chemical-engineering-honours-science", //not availabe cricos code
        "innerText": "Bachelor of Chemical Engineering (Honours) / Bachelor of Science"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/bachelor-of-computer-systems-engineering-honours-science", //not availabe cricos code
        "innerText": "Bachelor of Computer Systems Engineering (Honours) / Bachelor of Science"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/bachelor-of-electrical-and-electronic-engineering-honours-science", //not availabe cricos code
        "innerText": "Bachelor of Electrical and Electronic Engineering (Honours) / Bachelor of Science"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/bachelor-of-environmental-engineering-honours-science", //not availabe cricos code
        "innerText": "Bachelor of Environmental Engineering (Honours) / Bachelor of Science"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/bachelor-of-environmental-science-and-management",  //not given information
        "innerText": "Bachelor of Environmental Science and Management"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/bachelor-of-mechanical-engineering-honours-science", //not availabe cricos code
        "innerText": "Bachelor of Mechanical Engineering (Honours) / Bachelor of Science"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/bachelor-of-mechatronics-engineering-honours-science", //not availabe cricos code
        "innerText": "Bachelor of Mechatronics Engineering (Honours) / Bachelor of Science"
    },
	{
        "href": "https://www.newcastle.edu.au/degrees/master-professional-engineering", // not given any information
        "innerText": "Master of Professional Engineering"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/master-professional-engineering-software", // not given tution fee
        "innerText": "Master of Professional Engineering (Software)"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/bachelor-of-laws-honours", //not availabe cricos code
        "innerText": "Bachelor of Laws (Honours) Combined"
    },
    {
        "href": "https://www.newcastle.edu.au/degrees/bachelor-of-medical-science-doctor-of-medicine-joint-medical-program", //not given international course
        "innerText": "Bachelor of Medical Science / Doctor of Medicine (Joint Medical Program)"
    }
]