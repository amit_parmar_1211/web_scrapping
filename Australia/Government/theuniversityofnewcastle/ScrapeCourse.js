const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, page) {

    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'course_discipline': {
              // As of not we do not have way to get this field value
              resJsonData.course_discipline = course_category;
              break;
            }
            case 'course_url': {
              let newUrl = courseScrappedData.course_url;
              let rescourse_url = null;
              if (Array.isArray(newUrl)) {
                for (const rootEleDict of newUrl) {
                  console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));

                      rescourse_url = selList;

                    }
                  }
                }
              }
              if (rescourse_url) {
                resJsonData.course_url = rescourse_url;
              }
              break;

            }
            case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              const courseAdminReq = {};
              const englishList = [];
              var otherLngDict = null, ieltsDic = null;
              const ieltsDict = {}; const ibtDict = {}; const pteDict = {}; const utasDict = {}; let ieltsNumber = null;
              // english requirement
              if (courseScrappedData.course_toefl_ielts_score) {
                const ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                if (ieltsScore && ieltsScore.length > 0) {
                  const ieltsmoreScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_ielts_more_score);

                  ieltsDict.name = 'ielts academic';
                  ieltsDict.description = ieltsScore + ", " + ieltsmoreScore;
                  ieltsDic = ieltsDict;
                  englishList.push(ieltsDict);
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(ieltsScore).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    ieltsNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                  }
                }
              }
              // if ieltsNumber is valid then map IBT and PBT accordingly
              if (ieltsNumber && ieltsNumber > 0) {
                const ieltsMappingDict = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                if (ieltsMappingDict) {
                  otherLngDict = ieltsMappingDict[ieltsNumber];
                  console.log(funcName + 'otherLngDict = ' + JSON.stringify(otherLngDict));
                  if (otherLngDict) {
                    const ibtScore = otherLngDict.ibt;
                    const pteScore = otherLngDict.pte;
                    //                    const utasScore = otherLngDict.utasaccess;
                    console.log(funcName + 'ibtScore = ' + ibtScore + ', pteScore = ' + pteScore);
                    if (ibtScore) {
                      ibtDict.name = 'toefl ibt';
                      ibtDict.description = ibtScore;
                      englishList.push(ibtDict);
                    }
                    if (pteScore) {
                      pteDict.name = 'pte academic';
                      pteDict.description = pteScore;
                      englishList.push(pteDict);
                    }
                  } else { // if (otherLngDict)
                    ibtDict.name = 'toefl ibt';
                    ibtDict.description = configs.propValueNotAvaialble;
                    englishList.push(ibtDict);

                    pteDict.name = 'pte academic';
                    pteDict.description = configs.propValueNotAvaialble;
                    englishList.push(pteDict);
                  } // if (otherLngDict)
                }
              }

              ///progress bar start
              const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
              var penglishList = [];
              const potherLngDict = otherLngDict;

              if (potherLngDict) {
                var ieltsScore = "NA", ibtScore = "NA", pteScore = "NA";
                if (ieltsDic.description) {
                  ieltsScore = await utils.giveMeNumber(ieltsDic.description);
                  console.log("### IELTS data-->" + ieltsScore);
                }
                if (potherLngDict.ibt) {
                  ibtScore = await utils.giveMeNumber(potherLngDict.ibt);
                }
                if (potherLngDict.pte) {
                  pteScore = await utils.giveMeNumber(potherLngDict.pte);
                }

                // if (ieltsScore != "NA") {
                //   pieltsDict.name = 'ielts academic';
                //   var value = {};
                //   value.min = 0;
                //   value.require = Number(ieltsScore);
                //   value.max = 9;
                //   pieltsDict.value = value;
                //   penglishList.push(pieltsDict);
                // }

                // if (ieltsScore && ieltsScore > 0) {
                if (ieltsScore) {
                  pieltsDict.name = 'ielts academic';
                  pieltsDict.description = ieltsDic.description;
                  pieltsDict.min = 0;
                  pieltsDict.require = Number(ieltsScore);;
                  pieltsDict.max = 9;
                  pieltsDict.R = 0;
                  pieltsDict.W = 0;
                  pieltsDict.S = 0;
                  pieltsDict.L = 0;
                  pieltsDict.O = 0;
                  penglishList.push(pieltsDict);
                }

                // if (ibtScore != "NA") {
                //   pibtDict.name = 'toefl ibt';
                //   var value = {};
                //   value.min = 0;
                //   value.require = Number(ibtScore);
                //   value.max = 120;
                //   pibtDict.value = value;
                //   penglishList.push(pibtDict);
                // }

                if (ibtScore) {
                  pibtDict.name = 'toefl ibt';
                  pibtDict.description = potherLngDict.ibt;
                  pibtDict.min = 0;
                  pibtDict.require = Number(ibtScore);;
                  pibtDict.max = 120;
                  pibtDict.R = 0;
                  pibtDict.W = 0;
                  pibtDict.S = 0;
                  pibtDict.L = 0;
                  pibtDict.O = 0;
                  penglishList.push(pibtDict);
                }


                // if (pteScore != "NA") {
                //   ppteDict.name = 'pte academic';
                //   var value = {};
                //   value.min = 0;
                //   value.require = Number(pteScore);
                //   value.max = 90;
                //   ppteDict.value = value;
                //   penglishList.push(ppteDict);
                // }
                if (pteScore) {
                  ppteDict.name = 'pte academic';
                  ppteDict.description = potherLngDict.pte;
                  ppteDict.min = 0;
                  ppteDict.require = Number(pteScore);;
                  ppteDict.max = 90;
                  ppteDict.R = 0;
                  ppteDict.W = 0;
                  ppteDict.S = 0;
                  ppteDict.L = 0;
                  ppteDict.O = 0;
                  penglishList.push(ppteDict);
                }
              }
              if (penglishList && penglishList.length > 0) {
                courseAdminReq.english = penglishList;
              }
              ///progrssbar End
              // if (englishList && englishList.length > 0) {
              //   courseAdminReq.english = englishList;
              // }
              // academic requirement
              if (courseScrappedData.course_academic_requirement) {
                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                var academicDetails = [];

                console.log('\n\r');
                console.log(funcName + 'academicReq = ' + academicReq);
                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                console.log('\n\r');
                if (academicReq && String(academicReq).length > 0) {
                  academicDetails.push(academicReq);
                  courseAdminReq.academic = academicDetails;
                }
              }
              // if courseAdminReq has any valid value then only assign it
              if (courseAdminReq.english || courseAdminReq.academic) {
                resJsonData.course_admission_requirement = courseAdminReq;
              }
              // add english requirement 'english_more_details' link
              const courseKeyVal = courseScrappedData.course_admission_english_more_details;
              let resEnglishReqMoreDetailsJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resEnglishReqMoreDetailsJson = selList[0];
                      }
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))
              // add english requirement 'english_more_details' link
              const courseKeyValAcademic = courseScrappedData.course_admission_academic_more_details;
              let resAcademicReqMoreDetailsJson = null;
              if (Array.isArray(courseKeyValAcademic)) {
                for (const rootEleDict of courseKeyValAcademic) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAcademicReqMoreDetailsJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                resJsonData.course_admission_requirement.entry_requirements_url = "";
                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqMoreDetailsJson;
                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
              } else if (resEnglishReqMoreDetailsJson) {
                resJsonData.course_admission_requirement = {};
                resJsonData.course_admission_requirement.entry_requirements_url = "";
                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqMoreDetailsJson;
                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
              }
              break;
            }

            case 'course_duration_full_time': {
              // display full time
              var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
              var fullTimeText = "";
              const courseKeyVal = courseScrappedData.course_duration_full_time;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        fullTimeText = selList[0];
                      }
                    }
                  }
                }
              }
              if (fullTimeText && fullTimeText.length > 0) {
                const resFulltime = fullTimeText;
                if (resFulltime) {
                  durationFullTime.duration_full_time = resFulltime;
                  courseDurationList.push(durationFullTime);
                  courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                  console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                  let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                  if (courseDurationList && courseDurationList.length > 0) {
                    resJsonData.course_duration = resFulltime;
                  }
                  if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                    resJsonData.course_duration_display = filtered_duration_formated;
                    var isfulltime = false, isparttime = false;
                    filtered_duration_formated.forEach(element => {
                      if (element.display == "Full-Time") {
                        isfulltime = true;
                      }
                      if (element.display == "Part-Time") {
                        isparttime = true;
                      }
                    });
                    resJsonData.isfulltime = isfulltime;
                    resJsonData.isparttime = isparttime;

                  }
                }
              }
              break;
            }
            case 'course_tuition_fee':
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_duration_part_time': {
              console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
              break;
            }
            case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};
              const feesList = [];
              const feesDict = {
                international_student: {}
              };
              const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
              const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
              // if (feeYear && feeYear.length > 0) {
              //   courseTuitionFee.year = feeYear;
              // }
              console.log('Fees for international student :' + feesIntStudent);

              // if we can extract value as Int successfully then replace Or keep as it is
              if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                var isfulltimeflag = false;
                var durationfee = "";
                var unitfee = "";
                ///Start
                var fullTimeText = "";
                const courseKeyVal = courseScrappedData.course_duration_full_time;
                if (Array.isArray(courseKeyVal)) {
                  for (const rootEleDict of courseKeyVal) {
                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                      console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                      const selectorsList = eleDict.selectors;
                      for (const selList of selectorsList) {
                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        if (Array.isArray(selList) && selList.length > 0) {
                          fullTimeText = selList[0];
                        }
                      }
                    }
                  }
                }
                console.log("course duration display :" + fullTimeText);
                ////END

                if (String(feesIntStudent).toLowerCase().indexOf('indicative annual fee') > -1) {
                  durationfee = 1;
                  unitfee = "Year";
                  if (fullTimeText.indexOf('1 year full-time') > -1) {
                    isfulltimeflag = true;
                  }
                }
                const feesSplit = feesWithDollorTrimmed.split('(');

                console.log('Total fees:' + feesSplit[0]);

                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                const feesVal1 = String(feesSplit[0]).replace('$', '');

                const arrval = String(feesVal1).split('.');

                const feesVal = String(arrval[0]);

                console.log(funcName + 'feesVal = ' + feesVal);
                if (feesVal) {
                  const regEx = /\d/g;
                  let feesValNum = feesVal.match(regEx);
                  if (feesValNum) {
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    feesValNum = feesValNum.join('');
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    let feesNumber = null;
                    if (feesValNum.includes(',')) {
                      feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                    } else {
                      feesNumber = feesValNum;
                    }
                    console.log(funcName + 'feesNumber = ' + feesNumber);
                    if (Number(feesNumber)) {
                      feesDict.international_student = ({
                        amount: Number(feesNumber),
                        duration: durationfee,
                        unit: unitfee,

                        description: feesIntStudent

                      });

                    } else {
                      feesDict.international_student = ({
                        amount: 0,
                        duration: durationfee,
                        unit: unitfee,

                        description: feesIntStudent

                      });
                    }
                  }
                }
              } // if (feesIntStudent && feesIntStudent.length > 0)
              // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
              const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
              console.log(funcName + 'cricoseStr = ' + cricoseStr);
              console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
              if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                if (fixedFeesDict) {
                  for (const codeKey in fixedFeesDict) {
                    console.log(funcName + 'codeKey = ' + codeKey);
                    if (fixedFeesDict.hasOwnProperty(codeKey)) {
                      const keyVal = fixedFeesDict[codeKey];
                      console.log(funcName + 'keyVal = ' + keyVal);
                      if (cricoseStr.includes(codeKey)) {
                        const feesDictVal = fixedFeesDict[cricoseStr];
                        console.log(funcName + 'feesDictVal = ' + feesDictVal);
                        if (feesDictVal && feesDictVal.length > 0) {
                          const inetStudentFees = Number(feesDictVal);
                          console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                          if (Number(inetStudentFees)) {
                            feesDict.international_student = Number(inetStudentFees);
                          }
                        }
                      }
                    }
                  } // for
                } // if
              } // if
              let international_student_all_fees_array = [];
              if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                  //   feesDict.international_student_all_fees = international_student_all_fees_array
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }
                if (feesDict) {
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                if (courseTuitionFee) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
                // take tuition fee value at json top level so will help in DDB for indexing
                if (courseTuitionFee && feesDict.international_student) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                  // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                }
              }
              console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
              // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
              if (!feesDict.international_student) {
                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                return null; // this will add this item into FailedItemList and writes file to local disk
              }

              break;
            }
            case 'course_outline_core': {
              // "//*//h4[contains(text(),'Program requirements')]/following-sibling::ul/li[not(strong/a)]"
              resJsonData.course_outline = {
                subject_areas: {
                  core: [],
                  major: [],
                  minor: []
                },
                more_details: ""
              };
              const courseKeyVal = courseScrappedData.course_outline_core;
              let res_course_outline_core = [];
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict course_outline_core= ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict course_outline_core= ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log("SELLIST -->>>> ", selList.subject_areas);
                      if (selList.subject_areas && selList.subject_areas.length > 0) {
                        console.log("SELLIST -->>>>111 ", selList.subject_areas);
                        res_course_outline_core = selList.subject_areas;
                      }
                      console.log("SELLIST -->>>>222 ");
                      if (selList.study_units && selList.study_units.length > 0) {
                        res_course_outline_core = selList.study_units;
                      }
                      resJsonData.course_outline.more_details = selList.more_details;
                    }
                  }
                }
              }
              console.log("SELLIST -->>>>333 ");
              if (res_course_outline_core) {
                console.log("SELLIST -->>>>444 ");
                resJsonData.course_outline.subject_areas.core = res_course_outline_core;
              }
              const courseKeyVal_major = courseScrappedData.course_outline_major;
              let res_course_outline_major = [];
              if (Array.isArray(courseKeyVal_major)) {
                for (const rootEleDict of courseKeyVal_major) {
                  console.log(funcName + '\n\r rootEleDict course_outline_major= ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict course_outline_major= ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log("SELLIST -->>>>555 ", selList.subject_areas);
                      if (selList.subject_areas && selList.subject_areas.length > 0) {
                        console.log("SELLIST -->>>>7777 ");
                        res_course_outline_major = selList.subject_areas;
                      }
                      console.log("SELLIST -->>>>888 ");
                      if (selList.study_units && selList.study_units.length > 0) {
                        console.log("SELLIST -->>>>666 ");
                        res_course_outline_major = selList.study_units;
                      }
                      resJsonData.course_outline.more_details = selList.more_details;
                    }
                  }
                }
              }
              console.log("SELLIST -->>>>999 ");
              if (res_course_outline_core) {
                console.log("SELLIST -->>>>1010 ");
                resJsonData.course_outline.subject_areas.major = res_course_outline_major;
                console.log("SELLIST -->>>>11 11 ");
              }
              break;
            }


            // case 'course_cricos_code': {
            //   const courseKeyVal = courseScrappedData.course_cricos_code;
            //   let course_cricos_code = null;
            //   if (Array.isArray(courseKeyVal)) {
            //     for (const rootEleDict of courseKeyVal) {
            //       console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
            //       const elementsList = rootEleDict.elements;
            //       for (const eleDict of elementsList) {
            //         console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
            //         const selectorsList = eleDict.selectors;
            //         for (const selList of selectorsList) {
            //           console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
            //           if (Array.isArray(selList) && selList.length > 0) {
            //             course_cricos_code = selList;
            //           }
            //         }
            //       }
            //     }
            //   }
            //   //  if (course_cricos_code) {
            //   if (course_cricos_code && course_cricos_code.length > 0) {
            //     global.cricos_code = course_cricos_code[0];
            //     var locations = resJsonData.course_campus_location;
            //     var mycodes = [];
            //     for (let location of locations) {
            //       mycodes.push({
            //         location: location.name, code: course_cricos_code.toString(), iscurrent: false
            //       });
            //       //  mycodes.push(location.name + " - " + course_cricos_code);
            //     }
            //     resJsonData.course_cricos_code = mycodes;
            //   }
            //   else {
            //     throw new Error("Cricos code not found");
            //   }
            //   break;
            // }


            case 'course_campus_location': { // Location Launceston
              console.log('course_campus_location matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));

              const Cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);

              let split_campus2, replace_val;
              let concatnatedSelStr = [];
              for (const rootEleDict of rootElementDictList) {
                console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                // let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  for (const selList of selectorsList) {
                    if (selList && selList.length > 0) {
                      console.log('course_campus_location selList = ' + JSON.stringify(selList));
                      for (var selItem of selList) {
                        selItem = selItem.replace(/[\r\n\t ]+/g, ' ').split(",");
                        for (let sel of selItem) {
                          if (sel != "external" && sel != "External" && sel != "Online" && sel != "online") {
                            concatnatedSelStr.push(sel.replace('UON', '').replace('BCA', '').trim());
                          }
                          else {
                            concatnatedSelStr = sel
                            console.log("itis in else111", concatnatedSelStr)
                          }
                        }
                      } // selList
                      console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);

                    }
                  }
                }
              }

              for (var i = 0; i < concatnatedSelStr.length; i++) {
                if (concatnatedSelStr.includes('BCA')) {
                  concatnatedSelStr = concatnatedSelStr.replace('BCA', '');
                }
                else {
                  concatnatedSelStr = concatnatedSelStr
                  console.log("itis in else", concatnatedSelStr)
                }
                var finallocation = [];
                var loca = String(concatnatedSelStr).split(',');
                console.log("loca------>", loca)

                loca.forEach(element => {
                  if (element.includes('City')) {
                    finallocation.push(element.replace('City', ''))
                  }
                  else {
                    finallocation.push(element)
                    console.log("finallocation------->", finallocation)
                  }
                })
                var campusss = [];

                if (finallocation.length > 0) {
                  console.log("locasfsdf------>", finallocation);
                  let campuses = ['Newcastle', 'Central Coast', 'Sydney CBD']
                  console.log("@@campuses------->", campuses);

                  for (let i = 0; i < finallocation.length; i++) {
                    campuses.forEach(element => {
                      console.log(element.includes(finallocation[i].trim()))
                      if (element.includes(finallocation[i].trim())) {

                        campusss.push(element)

                      }
                    })
                  }
                  console.log("@@campuses------->", campusss)
                  // let newcampus = [];
                  // for (let campus of concatnatedSelStr) {
                  //   if (!newcampus.includes(campus)) {
                  //     replace_val = String(campus).replace(/Vic/g, '').trim();
                  //     if (!replace_val == '') {
                  //       console.log("##123456-->" + replace_val)
                  //       const location_hardcode = await utils.getValueFromHardCodedJsonFile('campuses');
                  //       console.log("location_hardcode-------->>>>", location_hardcode);
                  //       let tmpvar123 = location_hardcode.filter(val => {
                  //         return val.name.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase() == replace_val.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase()
                  //       });
                  //       console.log("##tmpvar123-->" + JSON.stringify(tmpvar123[0].Location))
                  //       let loc_val = tmpvar123[0].Location;
                  //       console.log("##Campuscampus-->" + loc_val)
                  //       newcampus.push(loc_val);
                  //     }
                  //   }
                  // }
                  if (campusss && campusss.length > 0) {
                    var campusedata = [];
                    campusss.forEach(element => {
                      campusedata.push({
                        "name": this.titleCase(element),
                        "code": Cricos

                      })
                    });
                    resJsonData.course_campus_location = campusedata;
                    console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

                  }

                  else {
                    throw new Error("No campus location found.");

                  }
                }
              }
              break;

            }
            // case 'course_campus_location': { // Location Launceston
            //   const campLocationText = await Course.extractLocationValueFromScrappedElement(courseScrappedData.course_campus_location);
            //   console.log(funcName + 'campLocationText = ' + campLocationText);
            //   if (campLocationText && campLocationText.length > 0) {
            //     var campLocationValTrimmed = String(campLocationText).trim();
            //     console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
            //     if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
            //       resJsonData.course_campus_location = await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ''), ',');
            //     }
            //   } 

            //   break;
            // }
            case 'course_intake_url': {
              // if (courseUrl && courseUrl.length > 0) {
              //   resJsonData.course_intake_url = await utils.giveMeArray(courseUrl.replace(/[\r\n\t]+/g, ';'), ';');
              // }
              break;
            }
            case 'course_study_mode': { // Location Launceston

              resJsonData.course_study_mode = "On campus";

              break;
            }
            // case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
            //   var courseIntakeStr = [];//= await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
            //   // existing intake value
            //   const courseIntakeStr1 = await Course.extractdurationValueFromScrappedElement(courseScrappedData.course_intake);
            //  console.log("courseIntakeStr1",courseIntakeStr1);
            //   if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
            //     resJsonData.course_intake = courseIntakeStr1;
            //     const intakeStrList = String(courseIntakeStr1).split('Semester');
            //     console.log(funcName + 'intakeStrList course_intake = ' + JSON.stringify(intakeStrList));
            //     courseIntakeStr = intakeStrList;
            //   }
            //   console.log("intakes" + JSON.stringify(courseIntakeStr));
            //   if (courseIntakeStr && courseIntakeStr.length > 0) {
            //     var campus = resJsonData.course_campus_location;
            //     console.log('Campuse location :' + campus);
            //     var intakes = [];
            //     console.log("Intake length-->" + courseIntakeStr.length);
            //     console.log("Campus length-->" + campus.length);
            //     for (var camcount = 0; camcount < campus.length; camcount++) {
            //       for (var count = 0; count < courseIntakeStr.length; count++) {
            //         console.log("intake -->" + count)
            //         var intakedetail = {};
            //         intakedetail.name = campus[camcount].name;
            //         console.log('vales career :' + courseIntakeStr[count]);
            //         var intakesplite = courseIntakeStr[count].split(';');
            //         var finalintake = "";
            //         for (var i = 0; i < intakesplite.length; i++) {
            //           finalintake += intakesplite[i].trim() + ";";
            //         }
            //         finalintake = finalintake.slice(0, -1);
            //         console.log('final intake :' + finalintake)
            //         intakedetail.value = await utils.giveMeArray(finalintake.replace(/[\r\n\t ]+/g, ' '), ";");
            //         intakes.push(intakedetail);
            //       }
            //     }
            //     //  let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
            //     let formatIntake = await format_functions.providemyintake(intakes, "mom", "");
            //     console.log('****************************Formated Intake date Start************************');
            //     console.log('formated intake date :' + JSON.stringify(formatIntake));
            //     formatedIntake.forEach(element => {
            //                     element.value.forEach(elementdate => {
            //                         if (elementdate.actualdate == 'First intake in 2020') {
            //                           elementdate.filterdate = "2020-01-03"

            //                           }
            //                           // else if (elementdate.actualdate == '22 Jul, 2019') {
            //                         //     elementdate.filterdate = "2019-07-22"
            //                         // }
            //                         //elementdate.push({filterdate:myintake_data["term 1_date"]})
            //                         console.log("DATA--->", elementdate)
            //                     })
            //                 });
            //     console.log('****************************Formated Intake date End************************');
            //     var intakedata = {};
            //     intakedata.intake = formatIntake;
            //     intakedata.more_details = await utils.giveMeArray(courseUrl.replace(/[\r\n\t]+/g, ';'), ';');
            //     console.log('more_details@@ intake :', intakedata.more_details)
            //     resJsonData.course_intake = intakedata;
            //     intakedata.more_details = String(intakedata.more_details);
            //   }
            //   break;
            // }
            case 'course_outline': {
              console.log("course_outline--->")
              const outline = {
                majors: [],
                minors: [],
                more_details: ""
              };

              var major, minor
              var intakedata1 = {};
              const courseKeyVal = courseScrappedData.course_outline_major;
              console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

              let resScholarshipJson = null;


              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList;
                        console.log("resScholarshipJson", resScholarshipJson)
                      }
                    }
                  }
                }
              }
              if (resScholarshipJson) {
                major = resScholarshipJson
              } else {
                major = []
              }




              const courseKeyVal1 = courseScrappedData.course_outline_minors;
              let resScholarshipJson1 = null;
              if (Array.isArray(courseKeyVal1)) {
                for (const rootEleDict of courseKeyVal1) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson1 = selList;

                      }
                    }
                  }
                }
              }
              if (resScholarshipJson1) {
                minor = resScholarshipJson1
              } else {
                minor = []
              }

              let m_d = resJsonData.course_url;
              console.log("md--------->", m_d)

              var intakedata1 = {};
              intakedata1.majors = major;
              intakedata1.minors = minor;
              intakedata1.more_details = m_d;
              resJsonData.course_outline = intakedata1;


              break;
            }

            case 'application_fee':
              {
                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                resJsonData.application_fee = ""
                break;
              }
            case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
              var courseIntakeStr = [];//= await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
              // existing intake value
              const courseIntakeStr1 = await Course.extractdurationValueFromScrappedElement(courseScrappedData.course_intake);
              if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                resJsonData.course_intake = courseIntakeStr1;
                const intakeStrList = String(courseIntakeStr1).split('Semester');
                console.log(funcName + 'intakeStrList course_intake = ' + JSON.stringify(intakeStrList));
                courseIntakeStr = intakeStrList;
              }
              console.log("intakes" + JSON.stringify(courseIntakeStr));
              if (courseIntakeStr && courseIntakeStr.length > 0) {
                var campus = resJsonData.course_campus_location;
                console.log('Campuse location :' + campus);
                var intakes = [];
                console.log("Intake length-->" + courseIntakeStr.length);
                console.log("Campus length-->" + campus.length);
                for (var camcount = 0; camcount < campus.length; camcount++) {
                  for (var count = 0; count < courseIntakeStr.length; count++) {
                    console.log("intake -->" + count)
                    var intakedetail = {};
                    intakedetail.name = campus[camcount].name;
                    console.log('vales career :' + courseIntakeStr[count]);
                    var intakesplite = courseIntakeStr[count].split(';');
                    var finalintake = "";
                    for (var i = 0; i < intakesplite.length; i++) {
                      finalintake += intakesplite[i].trim() + ";";
                    }
                    finalintake = finalintake.slice(0, -1);
                    console.log('final intake :' + finalintake)
                    intakedetail.value = await utils.giveMeArray(finalintake.replace(/[\r\n\t ]+/g, ' '), ";");
                    intakes.push(intakedetail);
                  }
                }
                let formatIntake = await format_functions.providemyintake(intakes, "mom", "");
                console.log('****************************Formated Intake date Start************************');
                console.log('formated intake date :' + JSON.stringify(formatIntake));
                console.log('****************************Formated Intake date End************************');
                var intakedata = {};
                intakedata.intake = formatIntake;
                // intakedata.more_details = await utils.giveMeArray(courseUrl.replace(/[\r\n\t]+/g, ';'), ';');

                intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');

                resJsonData.course_intake = intakedata;
              }
              break;
            }
            case 'course_outline': {
              const MAX_SUBJECTS_COUNT = 20;
              const courseKeyVal = courseScrappedData.course_outline;
              const resOutlineList = []; // const studyUnitsDict = {};
              const concatSelectorsList = []; let moreSubjectsUrl = null;
              if (Array.isArray(courseKeyVal)) {
                let concatRootElementsList = [];
                for (const rootEleDict of courseKeyVal) {
                  let concatElementsList = [];
                  const elementsList = rootEleDict.elements;
                  console.log(funcName + 'elementsList course_outline = ' + JSON.stringify(elementsList));
                  for (const eleDict of elementsList) {
                    const selectorsList = eleDict.selectors;
                    console.log(funcName + 'selectorsList course_outline = ' + JSON.stringify(selectorsList));
                    for (const selList of selectorsList) {
                      let concatSelList = [];
                      console.log(funcName + 'selList course_outline = ' + JSON.stringify(selList));
                      if (selList.list) {
                        const resList = selList.list;
                        resList.forEach(elementArr => {
                          concatSelList.push(elementArr.innerText)
                        });
                        concatSelectorsList.push(concatSelList);
                      } // if (selList.list)
                      if (selList.dict) {
                        const urlDict = selList.dict;
                        console.log(funcName + 'urlDict course_outline = ' + JSON.stringify(urlDict));
                        if (urlDict.url && urlDict.url.length > 0) {
                          moreSubjectsUrl = urlDict.url;
                        }
                        console.log(funcName + 'moreSubjectsUrl  course_outline = ' + moreSubjectsUrl);
                      } // if (selList.dict)
                    } // for (const selList of selectorsList)
                    concatElementsList = concatElementsList.concat(concatSelectorsList);
                    console.log(funcName + 'concatElementsList course_outline= ' + JSON.stringify(concatElementsList));
                  } // for (const eleDict of elementsList)
                  concatRootElementsList = concatRootElementsList.concat(concatElementsList);
                } // for
              } // if (Array.isArray(courseKeyVal))
              console.log(funcName + 'concatSelectorsList.length  course_outline= ' + concatSelectorsList.length);
              console.log(funcName + 'concatSelectorsList  course_outline= ' + JSON.stringify(concatSelectorsList));
              if (concatSelectorsList && concatSelectorsList.length > 0) {
                let concatnatedSubList = [];
                for (const selList of concatSelectorsList) {
                  concatnatedSubList = concatnatedSubList.concat(selList);
                } // for
                // keep only unique subjects and remove any duplicates
                for (const sub of concatnatedSubList) {
                  if (!resOutlineList.includes(sub)) {
                    resOutlineList.push(sub);
                  }
                }
                console.log(funcName + 'concatnatedSubList course_outline = ' + JSON.stringify(concatnatedSubList));
              }
              console.log(funcName + 'resOutlineList.length course_outline = ' + resOutlineList.length);
              if (resOutlineList && resOutlineList.length > 0) {
                resJsonData.course_outline = {};
                // get only first 25 subjects
                let subjectsList = [];
                if (resOutlineList.length > MAX_SUBJECTS_COUNT) {
                  console.log(funcName + 'taking only first ' + MAX_SUBJECTS_COUNT + ' subjects only....');
                  subjectsList = resOutlineList.slice(0, MAX_SUBJECTS_COUNT);
                } else {
                  subjectsList = resOutlineList;
                }
                console.log(funcName + 'subjectsList.length course_outline = ' + subjectsList.length);
                console.log(funcName + 'subjectsList course_outline = ' + JSON.stringify(subjectsList));
                resJsonData.course_outline.study_units = subjectsList;
                console.log(funcName + 'moreSubjectsUrl = ' + moreSubjectsUrl);
                if (moreSubjectsUrl && moreSubjectsUrl.length > 0) {
                  resJsonData.course_outline.more_details = moreSubjectsUrl;
                }
              }
              break;
            }
            case 'course_scholarship': {
              const courseKeyVal = courseScrappedData.course_scholarship;
              let resScholarshipJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resScholarshipJson) {
                resJsonData.course_scholarship = resScholarshipJson;
              }
              break;
            }
            case 'univ_name': {
              const courseKeyVal = courseScrappedData.univ_name;
              let resUnivName = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivName = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivName) {
                resJsonData.univ_name = resUnivName;
              }
              break;
            }
            case 'univ_logo': {
              const courseKeyVal = courseScrappedData.univ_logo;
              let resUnivLogo = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivLogo = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivLogo) {
                resJsonData.univ_logo = resUnivLogo;
              }
              break;
            }
            case 'course_accomodation_cost': {
              const courseKeyVal = courseScrappedData.course_accomodation_cost;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_accomodation_cost = resAccomodationCostJson;
              }
              break;
            }
            case 'course_country':
            case 'course_overview': {
              console.log(funcName + 'matched case : ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr course_career_outcome= ' + concatnatedRootElementsStr);
              // add only if it has valid value
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
              }
              break;
            }
            case 'course_career_outcome': {
              const programClick = await page.$x('//a[text()="About this program"]');
              for (let link of programClick) {
                console.log('link career outcome :' + link);
                await link.click();
                await page.waitForNavigation();
              }
              await page.waitFor(2000);

              const selector = "#career-opportunities > ul > li";
              var category = await page.$$(selector);
              console.log('career outcome :' + category);
              if (category != '') {
                var course_career_outcome_found = []
                for (let link of category) {
                  var categorystring = await page.evaluate(el => el.innerText, link);
                  console.log("Category string for career outcome :" + categorystring);
                  course_career_outcome_found.push(categorystring);
                }
                resJsonData.course_career_outcome = course_career_outcome_found;
              }
              else {
                const courseKeyVal = courseScrappedData.course_career_outcome;
                var course_career_outcome = []
                console.log("course_career_outcome-->" + JSON.stringify(courseKeyVal));
                if (Array.isArray(courseKeyVal)) {
                  for (const rootEleDict of courseKeyVal) {
                    console.log(funcName + '\n\r rootEleDict course_career_outcome= ' + JSON.stringify(rootEleDict));
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                      console.log(funcName + '\n\r eleDict course_career_outcome= ' + JSON.stringify(eleDict));
                      const selectorsList = eleDict.selectors;
                      for (const selList of selectorsList) {
                        console.log(funcName + '\n\r selList course_career_outcome= ' + JSON.stringify(selList));
                        if (Array.isArray(selList) && selList.length > 0) {
                          course_career_outcome = selList;
                        }
                      }
                    }
                  }
                } // rootElementDictList
                // add only if it has valid value
                if (course_career_outcome.length > 0) {
                  resJsonData.course_career_outcome = course_career_outcome;
                }
              }
              break;
            }


            case 'program_code': {
              console.log(funcName + 'matched case program_code: ' + key);
              const courseKeyVal = courseScrappedData.program_code;
              var program_code = []
              console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        program_code = selList;
                      }
                    }
                  }
                }
              }
              for (let program_val of program_code) {
                resJsonData.program_code = program_val;
              }
              break;
            }

            case 'course_title': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr course title = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr.replace("\n", " ");
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).replace("\n", " ").concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr course title = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr course title = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr course title = ' + concatnatedRootElementsStr);
              // add only if it has valid value
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                resJsonData[key] = this.titleCase(concatnatedRootElementsStr);
              }
              break;
            }

            case 'course_study_level': {
              //  const level = courseScrappedData.course_study_level;
              // resJsonData.course_study_level = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
              const cStudyLevel = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
              if (cStudyLevel) {

                const study_l = cStudyLevel.split('Level');

                console.log('study_l' + study_l);
                const study_l1 = String(study_l).replace(/[0-9]/g, "");

                const arrval1 = String(study_l1).split('(Coursework)');
                console.log('arrval11' + arrval1);
                //  if (arrval1.includes(',') > -1) {

                var arrval12 = String(arrval1).replace(/,/g, '', -1).trim();


                resJsonData.course_study_level = String(arrval12);
                //   }
              }
              else {
                const code = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                console.log("Cricodecbhhfofdhdess---", code);
                let study_l = await format_functions.getMyStudyLevel(code);
                console.log("NEw Intake Formagfdt@@@@@" + JSON.stringify(study_l))
                // for (let study_l of code) {
                resJsonData.course_study_level = study_l;

              }





              //   const study_l =  resJsonData.course_study_level.split('Level');
              //   const study_l1 = String(study_l).replace('$', '');

              //   level.push(study_l1);
              //  // console.log('study_l' + study_l[0]);
              //   console.log("studyLevel@@",study_l1);

              break;
            }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)

      ////start genrating new file location wise
      var NEWJSONSTRUCT = {}, structDict = [];
      if (fs.existsSync("./output/new_structure_data.json")) {
        structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
      }
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
        console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
        console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        // var intakes = resJsonData.course_intake.intake;
        // var matchrec = [];
        // for (let dintake of intakes) {
        //   if (location == dintake.name) {
        //     matchrec = dintake.value;
        //   }
        // }
        // if (matchrec.length > 0) {
        //   NEWJSONSTRUCT.course_intake = matchrec[0];
        // }
        // else {
        //   NEWJSONSTRUCT.course_intake = "NA";
        // }
        for (let myfees of resJsonData.course_tuition_fee.fees) {
          if (myfees.name == location) {
            NEWJSONSTRUCT.international_student_all_fees = myfees;
          }
        }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        resJsonData.course_id = location_wise_data.course_location_id;



        resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
        // try {
        //   if (appConfigs.ISINSERT) {
        //     request.post({
        //       "headers": { "content-type": "application/json" },
        //       "url": appConfigs.API_URL + "api/australia/course/v2/savecourse",
        //       "body": JSON.stringify(resJsonData)
        //     }, (error, response, body) => {
        //       if (error) {
        //         console.log(error);
        //       }
        //       var myresponse = JSON.parse(body);
        //       if (!myresponse.flag) {
        //         var errorlog = null;
        //         if (fs.existsSync(configs.opFailedCourseListByAPI)) {
        //           errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
        //           errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
        //         }
        //         fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
        //       }
        //       console.log("success save data! " + JSON.stringify(body));
        //     });
        //   }
        //   else if (appConfigs.ISREPLACE) {
        //     request.post({
        //       "headers": { "content-type": "application/json" },
        //       "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
        //       "body": JSON.stringify(resJsonData)
        //     }, (error, response, body) => {
        //       if (error) {
        //         console.log(error);
        //       }
        //       var myresponse = JSON.parse(body);
        //       console.log("MyMessage-->" + JSON.stringify(myresponse))
        //       if (!myresponse.flag) {
        //         var errorlog = null;
        //         if (fs.existsSync(configs.opFailedCourseListByAPI)) {
        //           errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
        //           errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
        //         }
        //         fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
        //       }
        //       console.log("success save data! " + JSON.stringify(body));
        //     });
        //   }
        //   else if (appConfigs.ISUPDATE) {
        //     request.post({
        //       "headers": { "content-type": "application/json" },
        //       "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
        //       "body": JSON.stringify(resJsonData)
        //     }, (error, response, body) => {
        //       if (error) {
        //         console.log(error);
        //       }
        //       var myresponse = JSON.parse(body);
        //       if (!myresponse.flag) {
        //         var errorlog = null;
        //         if (fs.existsSync(configs.opFailedCourseListByAPI)) {
        //           errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
        //           errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
        //         }
        //         fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
        //       }
        //       console.log("success save data! " + JSON.stringify(body));
        //     });
        //   }
        // }
        // catch (err) {
        //   console.log("#### try-catch error--->" + err);
        // }
      }
      return resJsonData;
      ///end grnrating new file location wise
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = 'scrapeAndFormatCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);
      //swithc to international student
      const link = await s.page.$('#panel > div > div > div > span > span:not([style*="display: none"])[data-region*="australian"]');
      if (link != null) {
        await link.click();
      }
      //click on your programm handbook button
      const programClick = await s.page.$x('//a[text()="Your program handbook"]');
      for (let link of programClick) {
        console.log('link :' + link);
        await link.click();
        await s.page.waitForNavigation();
      }
      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }
      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      } const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, s.page);
      // const formattedScrappedData = await ScrapeCourse.formatOutput(courseDict.category, courseScrappedData, s.page.url(), courseDict.study_level);

      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails

  static titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      // You do not need to check if i is larger than splitStr length, as your for does that for you
      // Assign it back to the array
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
  }

} // class
module.exports = { ScrapeCourse };
