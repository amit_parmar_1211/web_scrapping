// const fs = require('fs');
// const Scrape = require('./common/scrape').Scrape;

// const startScrapping = async function startScrappingFunc() {
//   const funcName = 'startScrappingFunc ';
//   try {
//     s = new Scrape();
//     await s.init({ headless: true });
//     var datalist = [];
//     await s.setupNewBrowserPage("https://www.qut.edu.au/study");
//     var mainCategory = [], redirecturl = [];
//     //scrape main category
//     const maincategoryselector = "//*[@id='study-menu']/div/div/div[1]/div[4]/div/div/div[1]/div[2]/ul/li/a";
//     var category = await s.page.$x(maincategoryselector);
//     console.log("Total categories-->" + category.length);
//     for (let link of category) {
//       var categorystringmain = await s.page.evaluate(el => el.innerText, link);
//       var categorystringmainurl = await s.page.evaluate(el => el.href, link);
//       mainCategory.push(categorystringmain.trim());
//       redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
//     }

//     //scrape courses
//     for (let catc of redirecturl) {
//       await s.page.goto(catc.href, { timeout: 0 });
//       const selector = "//*/div[@id='course-category-listing-panel-content']/div//a";
//       var title = await s.page.$x(selector);
//       for (let t of title) {
//         var categorystring = await s.page.evaluate(el => el.innerText, t);
//         var categoryurl = await s.page.evaluate(el => el.href, t);
//         datalist.push({ href: categoryurl, innerText: categorystring, category: catc.innerText });
//       }
//     }

//     fs.writeFileSync("./output/queenslanduniversityoftechnology_original_courselist.json", JSON.stringify(datalist))
//     fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
//     // await fs.writeFileSync("./output/swinburneuniversityoftechnology_original_courselist.json", JSON.stringify(totalCourseList));
//     let uniqueUrl = [];
//     //unique url from the courselist file
//     for (let i = 0; i < datalist.length; i++) {
//       let cnt = 0;
//       if (uniqueUrl.length > 0) {
//         for (let j = 0; j < uniqueUrl.length; j++) {
//           if (datalist[i].href == uniqueUrl[j].href) {
//             cnt = 0;
//             break;
//           } else {
//             cnt++;
//           }
//         }
//         if (cnt > 0) {
//           uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [] });
//         }
//       } else {
//         uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [] });
//       }
//     }

//     await fs.writeFileSync("./output/queenslanduniversityoftechnology_unique_courselist.json", JSON.stringify(uniqueUrl));
//     //based on unique urls mapping of categories
//     // for (let i = 0; i < totalCourseList.length; i++) {
//     //   for (let j = 0; j < uniqueUrl.length; j++) {
//     //     if (uniqueUrl[j].href == totalCourseList[i].href) {
//     //       uniqueUrl[j].category.push(totalCourseList[i].category);
//     //     }
//     //   }
//     // }
//     //based on unique urls mapping of categories
//     for (let i = 0; i < datalist.length; i++) {
//       for (let j = 0; j < uniqueUrl.length; j++) {
//         if (uniqueUrl[j].href == datalist[i].href) {
//           if (uniqueUrl[j].category.includes(datalist[i].category)) {

//           } else {
//             uniqueUrl[j].category.push(datalist[i].category);
//           }

//         }
//       }
//     }
//     console.log("totalCourseList -->", uniqueUrl);
//     await fs.writeFileSync("./output/queenslanduniversityoftechnology_courselist.json", JSON.stringify(uniqueUrl));
//     console.log(funcName + 'writing courseList to file completed successfully....');
//     await s.browser.close();
//     console.log(funcName + 'browser closed successfully.....');
//     return true;
//   } catch (error) {
//     console.log(funcName + 'try-catch error = ' + error);
//     throw (error);
//   }
// };
// startScrapping();