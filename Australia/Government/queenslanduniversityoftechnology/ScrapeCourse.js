
const fs = require('fs');
const awsUtil = require('./common/aws_utils');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, studyLevel) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = course_category;
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}

                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {};
                            const pteDict = {};
                            const ibtDict = {};
                            const pbtDict = {};
                            const caeDict = {};
                            let ieltsNumber = null;
                            let title = resJsonData.course_title.toLowerCase();


                            if (title == "master of business (management)") {
                                const ieltsMappingDict = await utils.getValueFromHardCodedJsonFile("ielts_mapping");

                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ieltsMappingDict.masterOfBusiness_management.ielts;
                                let require_val = await utils.giveMeNumber(description);
                                ieltsDict.min = 0;
                                ieltsDict.require = Number(require_val);
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);

                                ibtDict.name = 'toefl ibt';
                                ibtDict.description = ieltsMappingDict.masterOfBusiness_management.tofel_ibt;
                                require_val = await utils.giveMeNumber(description);
                                ieltsDict.min = 0;
                                ieltsDict.require = Number(description);
                                ibtDict.max = 120;
                                ibtDict.R = 0;
                                ibtDict.W = 0;
                                ibtDict.S = 0;
                                ibtDict.L = 0;
                                ibtDict.O = 0;
                                englishList.push(ibtDict);

                                pteDict.name = 'pte academic';
                                pteDict.description = ieltsMappingDict.masterOfBusiness_management.pte;
                                require_val = await utils.giveMeNumber(description);
                                pteDict.min = 0;
                                pteDict.require = Number(description);
                                pteDict.max = 90;
                                pteDict.R = 0;
                                pteDict.W = 0;
                                pteDict.S = 0;
                                pteDict.L = 0;
                                pteDict.O = 0;
                                englishList.push(pteDict);

                                caeDict.name = 'cae';
                                caeDict.description = ieltsMappingDict.masterOfBusiness_management.cae;
                                require_val = await utils.giveMeNumber(description);
                                caeDict.min = 80;
                                caeDict.require = Number(description);
                                caeDict.max = 230;
                                caeDict.R = 0;
                                caeDict.W = 0;
                                caeDict.S = 0;
                                caeDict.L = 0;
                                caeDict.O = 0;
                                englishList.push(caeDict);

                                courseAdminReq.english = englishList;
                            }
                            else if (title == "graduate diploma in applied science (medical physics)") {
                                const ieltsMappingDict = await utils.getValueFromHardCodedJsonFile("ielts_mapping");

                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ieltsMappingDict.graduateDiplomainAppliedScience_medicalPhysics.ielts;
                                let require_val = await utils.giveMeNumber(description);
                                ieltsDict.min = 0;
                                ieltsDict.require = Number(require_val);
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);

                                ibtDict.name = 'toefl ibt';
                                ibtDict.description = ieltsMappingDict.graduateDiplomainAppliedScience_medicalPhysics.tofel_ibt;
                                //  let require_val =await utils.giveMeNumber(description);
                                ibtDict.min = 0;
                                ibtDict.require = Number(require_val);
                                ibtDict.max = 120;
                                ibtDict.R = 0;
                                ibtDict.W = 0;
                                ibtDict.S = 0;
                                ibtDict.L = 0;
                                ibtDict.O = 0;
                                englishList.push(ibtDict);

                                pteDict.name = 'pte academic';
                                pteDict.description = ieltsMappingDict.graduateDiplomainAppliedScience_medicalPhysics.pte;
                                // let require_val =await utils.giveMeNumber(description);
                                pteDict.min = 0;
                                pteDict.require = Number(require_val);
                                pteDict.max = 90;
                                pteDict.R = 0;
                                pteDict.W = 0;
                                pteDict.S = 0;
                                pteDict.L = 0;
                                pteDict.O = 0;
                                englishList.push(pteDict);

                                caeDict.name = 'cae';
                                caeDict.description = ieltsMappingDict.graduateDiplomainAppliedScience_medicalPhysics.cae;
                                // let require_val =await utils.giveMeNumber(description);
                                caeDict.min = 80;
                                caeDict.require = Number(require_val);
                                caeDict.max = 230;
                                caeDict.R = 0;
                                caeDict.W = 0;
                                caeDict.S = 0;
                                caeDict.L = 0;
                                caeDict.O = 0;
                                englishList.push(caeDict);

                                courseAdminReq.english = englishList;
                            }
                            else {
                                const ieltsMappingDict = [];
                                if (courseScrappedData.course_toefl_ielts_score) {
                                    const courseKeyVal = courseScrappedData.course_toefl_ielts_score;
                                    console.log("IELTS@@@NEW", JSON.stringify(courseKeyVal))
                                    if (Array.isArray(courseKeyVal)) {
                                        for (const rootEleDict of courseKeyVal) {
                                            console.log('course_toefl_ielts_score rootEleDict = ' + JSON.stringify(rootEleDict));
                                            const elementsList = rootEleDict.elements;
                                            for (const eleDict of elementsList) {
                                                console.log('course_toefl_ielts_score eleDict = ' + JSON.stringify(eleDict));
                                                const selectorsList = eleDict.selectors;
                                                for (const selList of selectorsList) {
                                                    console.log('course_toefl_ielts_score selList = ' + JSON.stringify(selList));
                                                    if (Array.isArray(selList) && selList.length > 0) {
                                                        for (let selItem of selList) {
                                                            selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s+)/g, ' ').trim()
                                                            console.log('course_toefl_ielts_score selItem = ' + selItem);
                                                            const regEx = /[+-]?\d+(\.\d+)?/g;
                                                            let score = String(selItem).match(regEx);
                                                            console.log("after splitting", score);
                                                            // let obj = {
                                                            //     name: selItem.split(".")[0],
                                                            //     overall: score[1].trim(),
                                                            //     listening: score[2].trim(),
                                                            //     reading: score[3].trim(),
                                                            //     writing: score[4].trim()
                                                            // }
                                                            let obj = {
                                                                name: selItem.split(".")[0],
                                                                overall: score[1],
                                                                listening: score[2],
                                                                reading: score[3],
                                                                writing: score[4],
                                                                speaking: score[5]
                                                            }
                                                            ieltsMappingDict.push(obj);
                                                            console.log("Speaking@@@", JSON.stringify(obj))

                                                            // selItem = selItem.split(".");
                                                            // let temp = selItem.length - 1;
                                                            // console.log("after splitting", matchedStrList);
                                                            // let score = selItem[temp].replace(/\s+/g, " ").trim();
                                                            // score = score.replace(/\s/g, ",");
                                                            // score = score.split(",");
                                                            // console.log("score", score);   
                                                        }
                                                    }
                                                    
                                                }
                                            }
                                        }
                                    }
                                }

                                console.log("ieltsMappingDict", ieltsMappingDict);

                                if (ieltsMappingDict && ieltsMappingDict.length > 0) {
                                    let ieltsIndex = ieltsMappingDict.findIndex(value => { return value.name.indexOf("IELTS Academic") > -1 });
                                    let ibtIndex = ieltsMappingDict.findIndex(value => { return value.name.indexOf("TOEFL iBT") > -1 });
                                    let pteIndex = ieltsMappingDict.findIndex(value => { return value.name.indexOf("Pearson PTE (Academic)") > -1 });
                                    let caeIndex = ieltsMappingDict.findIndex(value => { return value.name.indexOf("Cambridge Advanced English") > -1 || value.name.indexOf('Cambridge English Score') > -1 });
                            
                                    if (ieltsMappingDict[ieltsIndex].name.indexOf("IELTS Academic") > -1) {

                                        console.log("Speaking---->", ieltsMappingDict[ieltsIndex].speaking)
                                        let description = "Overall score: " + ieltsMappingDict[ieltsIndex].overall + " Listening: " + ieltsMappingDict[ieltsIndex].listening + "; Reading: " + ieltsMappingDict[ieltsIndex].reading + ";Writing: " + ieltsMappingDict[ieltsIndex].writing + ";Speaking: " + ieltsMappingDict[ieltsIndex].speaking;
                                        let require_val = await utils.giveMeNumber(description);
                                        ieltsDict.name = 'ielts academic';
                                        ieltsDict.description = description;
                                        ieltsDict.min = 0;
                                        ieltsDict.require = Number(require_val);
                                        ieltsDict.max = 9;
                                        ieltsDict.R = 0;
                                        ieltsDict.W = 0;
                                        ieltsDict.S = 0;
                                        ieltsDict.L = 0;
                                        ieltsDict.O = 0;
                                        englishList.push(ieltsDict);
                                    }
                                    if (ieltsMappingDict[ibtIndex].name.indexOf("TOEFL iBT") > -1) {
                                        let description = "Overall score: " + ieltsMappingDict[ibtIndex].overall + " Listening: " + ieltsMappingDict[ibtIndex].listening + "; Reading: " + ieltsMappingDict[ibtIndex].reading + ";Writing: " + ieltsMappingDict[ibtIndex].writing + ";Speaking: " + ieltsMappingDict[ibtIndex].speaking;
                                        let require_val = await utils.giveMeNumber(description);
                                        ibtDict.name = 'toefl ibt';
                                        ibtDict.description = description;
                                        ibtDict.min = 0;
                                        ibtDict.require = Number(require_val);
                                        ibtDict.max = 120;
                                        ibtDict.R = 0;
                                        ibtDict.W = 0;
                                        ibtDict.S = 0;
                                        ibtDict.L = 0;
                                        ibtDict.O = 0;
                                        englishList.push(ibtDict);
                                    }
                                    if (ieltsMappingDict[pteIndex].name.indexOf("Pearson PTE (Academic)") > -1) {
                                        let description = "Overall score: " + ieltsMappingDict[pteIndex].overall + " Listening: " + ieltsMappingDict[pteIndex].listening + "; Reading: " + ieltsMappingDict[pteIndex].reading + ";Writing: " + ieltsMappingDict[pteIndex].writing + ";Speaking: " + ieltsMappingDict[pteIndex].speaking;
                                        let require_val = await utils.giveMeNumber(description);
                                        pteDict.name = 'pte academic';
                                        pteDict.description = description;
                                        pteDict.min = 0;
                                        pteDict.require = Number(require_val);
                                        pteDict.max = 90;
                                        pteDict.R = 0;
                                        pteDict.W = 0;
                                        pteDict.S = 0;
                                        pteDict.L = 0;
                                        pteDict.O = 0;
                                        englishList.push(pteDict);
                                    }
                                    if (ieltsMappingDict[caeIndex].name.indexOf("Cambridge Advanced English") > -1 || ieltsMappingDict[caeIndex].name.indexOf("Cambridge English Score") > -1) {
                                        let description = "Overall score: " + ieltsMappingDict[caeIndex].overall + " Listening: " + ieltsMappingDict[caeIndex].listening + "; Reading: " + ieltsMappingDict[caeIndex].reading + ";Writing: " + ieltsMappingDict[caeIndex].writing + ";Speaking: " + ieltsMappingDict[caeIndex].speaking;
                                        let require_val = await utils.giveMeNumber(description);
                                        caeDict.name = 'cae';
                                        caeDict.description = description;
                                        caeDict.min = 80;
                                        caeDict.require = Number(require_val);
                                        caeDict.max = 230;
                                        caeDict.R = 0;
                                        caeDict.W = 0;
                                        caeDict.S = 0;
                                        caeDict.L = 0;
                                        caeDict.O = 0;
                                        englishList.push(caeDict);
                                    }
                                    courseAdminReq.english = englishList;
                                }
                                else
                                                    {
                                                        courseAdminReq.english=[]
                                                       // throw new Error('IELTS not GIVEN !!!');
                                                      }//else{
                                //     courseAdminReq.english=[]

                                // }
                            }

                            console.log("ieltsMappingDict--->", courseAdminReq.english);

                            const regEx = /[+-]?\d+(\.\d+)?/g;
                            if (englishList.length > 0) {


                                const matchedStrList = String(englishList[0].description).match(regEx);
                                console.log('course_toefl_ielts_score matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    ieltsNumber = Number(matchedStrList[0]);
                                    console.log('course_toefl_ielts_score ieltsNumber = ' + ieltsNumber);
                                }

                            }
                            // academic requirement                           
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                let arr = [];
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                arr.push(academicReq.toString().replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ' '))
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = arr;
                                }
                                else {
                                    courseAdminReq.academic = [];
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            // if (courseAdminReq.english || courseAdminReq.academic) {
                            //     resJsonData.course_admission_requirement = courseAdminReq;
                            //     // resJsonData.course_admission_requirement.academic_more_details = resAcademicReqMoreDetailsJson;
                            // }

                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.course_admission_english_more_details;
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))


                            //DYNAMIC PROGRESS BAR //CAN BE COPIED//PBT NOT INCLUDED
                            //progress bar start
                            let penglishList = [];
                            let pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            // extract exact number from string              
                            let matchedStrList_ielts, matchedStrList_pte, matchedStrList_ibt, matchedStrList_pbt, matchedStrList_cae;
                            let ieltsNo, pteNo, ibtNo, caeNo, pbtNo;
                            if (ieltsDict) {
                                matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                            }
                            if (pteDict) {
                                matchedStrList_pte = String(pteDict.description).match(regEx);
                            }
                            if (ibtDict) {
                                matchedStrList_ibt = String(ibtDict.description).match(regEx);
                            }
                            if (pbtDict) {
                                matchedStrList_pbt = String(pbtDict.description).match(regEx);
                            }
                            if (caeDict) {
                                matchedStrList_cae = String(caeDict.description).match(regEx);
                            }

                            if (matchedStrList_ielts && matchedStrList_ielts.length > 0) {
                                ieltsNo = Number(matchedStrList_ielts[0]);
                            }
                            if (matchedStrList_pte && matchedStrList_pte.length > 0) {
                                pteNo = Number(matchedStrList_pte[0]);
                            }
                            if (matchedStrList_ibt && matchedStrList_ibt.length > 0) {
                                ibtNo = Number(matchedStrList_ibt[0]);
                            }
                            if (matchedStrList_pbt && matchedStrList_pbt.length > 0) {
                                pbtNo = Number(matchedStrList_pbt[0]);
                            }
                            if (matchedStrList_cae && matchedStrList_cae.length > 0) {
                                caeNo = Number(matchedStrList_cae[0]);
                            }
                            if (ieltsNumber && ieltsNumber > 0) {
                                if (ieltsNo) {
                                    pieltsDict.name = 'ielts academic';
                                    pieltsDict.value = {};
                                    pieltsDict.value.min = 0;
                                    pieltsDict.value.require = ieltsNo;
                                    pieltsDict.value.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    penglishList.push(pieltsDict);
                                }
                                if (ibtNo) {
                                    pibtDict.name = 'toefl ibt';
                                    pibtDict.value = {};
                                    pibtDict.value.min = 0;
                                    pibtDict.value.require = ibtNo;
                                    pibtDict.value.max = 120;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    penglishList.push(pibtDict);
                                }
                                if (pbtNo) {
                                    ppbtDict.name = 'toefl pbt';
                                    ppbtDict.value = {};
                                    ppbtDict.value.min = 310;
                                    ppbtDict.value.require = pbtNo;
                                    ppbtDict.value.max = 677;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    penglishList.push(ppbtDict);
                                }
                                if (pteNo) {
                                    ppteDict.name = 'pte academic';
                                    ppteDict.value = {};
                                    ppteDict.value.min = 0;
                                    ppteDict.value.require = pteNo;
                                    ppteDict.value.max = 90;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    penglishList.push(ppteDict);
                                }
                                if (caeNo) {
                                    pcaeDict.name = 'cae';
                                    pcaeDict.value = {};
                                    pcaeDict.value.min = 80;
                                    pcaeDict.value.require = caeNo;
                                    pcaeDict.value.max = 230;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    penglishList.push(pcaeDict);
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            resJsonData.course_admission_requirement = {};
                            if (courseAdminReq.english && courseAdminReq.english.length > 0) {
                                resJsonData.course_admission_requirement.english = courseAdminReq.english;
                            } else {
                                resJsonData.course_admission_requirement.english = []
                            }
                            if (courseAdminReq.academic && courseAdminReq.academic.length > 0) {
                                resJsonData.course_admission_requirement.academic = courseAdminReq.academic;
                            }
                            // add english requirement 'english_more_details' link
                            const entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            const academic_requirements_url = resJsonData.course_url;
                            const english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);

                            if (entry_requirements_url && entry_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            }
                            if (academic_requirements_url && academic_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;
                            }
                            if (english_requirements_url && english_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                            }
                           
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            let courseDurationList;
                            let courseDurationDisplayList = [];
                            let durationFullTime = {};

                            let durationFullTimeDisplay = null;

                            console.log("*************start formating years*************************");
                            console.log("R full-time", JSON.stringify(courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0]));

                            if (courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0]) {
                                if (courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0] && courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].length > 0) {
                                    console.log("*************start formating Full Time years*************************");
                                    let temp = courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0];

                                    let temp1 = await format_functions.validate_course_duration_full_time(courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].toString());
                                    var full_year_formated_data = {
                                        "unit": temp1[0].unit,
                                        "duration": temp1[0].duration,
                                        "display": "Full-Time"
                                    };
                                    //full_year_formated_data[0].display = "Full-Time";

                                    console.log("R full_year_formated_data", JSON.stringify(courseDurationDisplayList));
                                    ///course duration
                                    const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                                    durationFullTime = not_full_year_formated_data;
                                    courseDurationList=durationFullTime;
                                    ///END course duration

                                    ///course duration display
                                    courseDurationDisplayList.push(full_year_formated_data)
                                    durationFullTimeDisplay = {};
                                    durationFullTimeDisplay = full_year_formated_data;
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList);
                                    console.log("fullTimeText" + JSON.stringify(filtered_duration_formated))
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {

                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                    // courseDurationDisplayList.push(durationFullTimeDisplay);

                                    ///END course duration display
                                    console.log('***************END formating Full Time years**************************')
                                }
                            }

                            if (courseScrappedData.course_duration_part_time[0].elements[0].selectors[0][0]) {
                                if (courseScrappedData.course_duration_part_time && courseScrappedData.course_duration_part_time.length > 0) {
                                    console.log("*************start formating Part Time years*************************");
                                    console.log("R part-time", JSON.stringify(courseScrappedData.course_duration_part_time[0].elements[0]));
                                    let durationPartTimeDisplay = null;
                                    const durationPartTime = {};
                                    let temp2 = await format_functions.validate_course_duration_full_time(courseScrappedData.course_duration_part_time[0].elements[0].selectors[0][0]);
                                    var partyear_formated_data = {
                                        "unit": temp2[0].unit,
                                        "duration": temp2[0].duration,
                                        "display": "Part-Time"
                                    };
                                    courseDurationDisplayList.push(partyear_formated_data)
                                    console.log("R full_year_formated_data", JSON.stringify(partyear_formated_data));

                                    //partyear_formated_data[0].display = "Part-Time";
                                    var parttimeduration = JSON.stringify(partyear_formated_data);
                                    // if (parttimeduration != '[]') {

                                    //     ///parttime course duration
                                    //     const not_parttime_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_part_time);
                                    //     durationPartTime.duration_part_time = not_parttime_year_formated_data;
                                    //     if (courseDurationList[0] && courseDurationList[0].length > 0) {
                                    //         console.log("INIFFFF")
                                    //         courseDurationList[0].duration_full_time += ", " + durationPartTime.duration_part_time;
                                    //     }
                                    //     else {
                                    //         console.log("INElse")
                                    //         courseDurationList.push({
                                    //             duration_full_time: durationPartTime.duration_part_time
                                    //         });
                                    //     }
                                    //     ///End Parttime course duration

                                    //     ///partitme course duration display
                                    //     durationPartTimeDisplay = {};
                                    //     durationPartTimeDisplay = partyear_formated_data[0];
                                    //     ///END parttime course duration display
                                    //     console.log("IFERROR");
                                    //     if (durationPartTimeDisplay) {
                                    //         if (courseDurationList[0] && courseDurationList[0].length > 0) {
                                    //             courseDurationDisplayList[0].push(durationPartTimeDisplay);
                                    //         }
                                    //         else{
                                    //             courseDurationDisplayList.push([durationPartTimeDisplay]);
                                    //         }
                                    //     }
                                    // }
                                    if (parttimeduration != '[]') {

                                        ///parttime course duration
                                        const not_parttime_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_part_time);
                                        durationPartTime.duration_part_time = not_parttime_year_formated_data;
                                        var isfulltime = false, isparttime = false;
                                        // filtered_duration_formated.forEach(element => {
                                        //     if (element.display == "Full-Time") {
                                        //         isfulltime = true;
                                        //     }
                                        //     if (element.display == "Part-Time") {
                                        //         isparttime = true;
                                        //     }
                                        // });
                                        // resJsonData.isfulltime = isfulltime;
                                        // resJsonData.isparttime = isparttime;
                                        // console.log("DisplayList",JSON.stringify(courseDurationList[0].length))
                                        if (courseDurationList[0]) {
                                            console.log("inif", JSON.stringify(courseDurationList))
                                            console.log(" courseDurationList[0].duration_full_time ", durationPartTime.duration_part_time)
                                            courseDurationList=courseDurationList += " " + durationPartTime.duration_part_time;
                                        }
                                        else {
                                            courseDurationList=durationPartTime.duration_part_time
                                            
                                        }
                                        ///End Parttime course duration

                                        ///partitme course duration display
                                        durationPartTimeDisplay = {};
                                        durationPartTimeDisplay = partyear_formated_data[0];
                                        ///END parttime course duration display
                                        console.log("IFERROR");
                                        if (durationPartTimeDisplay) {
                                            if (courseDurationList[0] && courseDurationList[0].length > 0) {
                                                courseDurationDisplayList[0].push(durationPartTimeDisplay);
                                            }
                                            else {
                                                courseDurationDisplayList.push([durationPartTimeDisplay]);
                                            }
                                        }
                                    }
                                    console.log('***************END formating Part Time years**************************');
                                    let filtered_duration_formatedd = await format_functions.getfilterduration(courseDurationDisplayList);
                                    console.log("fullTimeText" + JSON.stringify(filtered_duration_formatedd))
                                    if (filtered_duration_formatedd && filtered_duration_formatedd.length > 0) {

                                        resJsonData.course_duration_display = filtered_duration_formatedd;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formatedd.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                    console.log('***************END formating Part Time years**************************');
                                }
                            }

                            if (courseDurationList && courseDurationList.length > 0) {
                                resJsonData.course_duration = courseDurationList;
                            }
                            if (courseDurationDisplayList && courseDurationDisplayList.length > 0) {
                                resJsonData.course_duration_display = courseDurationDisplayList;
                            }
                            console.log("*************END formating years*************************");
                            break;
                        }
                        
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        
                        
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            //CUSTOMIZED CODE DON'T COPY    
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};
                            var futureFees;
                            var currentFees;
                            // var international_student_all_fees = [];
                            var feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            console.log("!fees_2020",feesIntStudent)
                            if(feesIntStudent!=null || feesIntStudent!="null"||feesIntStudent!='null'||String(feesIntStudent).includes('2020: To Be Advised')){
                                if (feesIntStudent.indexOf("Either ") > -1) {
                                    
                                    feesIntStudent = feesIntStudent.replace(" 2019", ";2019", " 2020", ";2020").split(";");
                                    console.log("feesIntStudent_1", feesIntStudent);
                                    futureFees = feesIntStudent[0];   //future fees 
                                    currentFees = feesIntStudent[1];
                                    console.log("feesIntStudent_1_2", feesIntStudent);
                                    if (feesIntStudent[1].indexOf("(") > -1) {
                                        //currentFees = feesIntStudent[1].split("(")[0];
                                        feesIntStudent = feesIntStudent[1].split(":")[1].split("(")[0]; //current fees
    
                                        if (feesIntStudent[0].indexOf("- $") > -1) {
                                            feesIntStudent = feesIntStudent[0].split("-")[0].split("(")[0];
                                        }
                                    }
                                }
                                else if (feesIntStudent.indexOf("+") > -1) {
                                   
                                    let arr = [];
                                    var data
                                    var feesIntStudent = feesIntStudent.split(":");
                                    feesIntStudent.forEach(element => {
                                        if (element.includes("per")) {
    
                                            feesIntStudent = element.split("per")
    
                                            console.log("SplitData", JSON.stringify(feesIntStudent))
                                            data = String(feesIntStudent[0]).replace(/\,/g, "")
                                            console.log("SplitData", JSON.stringify(data))
    
    
                                            arr.push(data.trim())
    
    
                                        }
                                    });
    
                                    currentFees = arr[0]
                                    futureFees = arr[1]
                                    feesIntStudent = currentFees
    
                                    //feesIntStudent=String(data).split("+")
                                    // console.log("SplitData",JSON.stringify(feesIntStudent))
                                    console.log("feesIntStudent_3_1", arr);
                                    console.log("YESSSSS", feesIntStudent);
                                }
                                else if (feesIntStudent.indexOf(")") > -1) {
                                     
                                    if (String(feesIntStudent).includes("2020") && !String(feesIntStudent).includes("2019")) {
                                        feesIntStudent = feesIntStudent.split(")");
                                        console.log("feesIntStudent_2_1", feesIntStudent);
                                        console.log("YESSSSS", feesIntStudent);
                                        currentFees = 0
                                        var futureFees = feesIntStudent[0]
                                        console.log("FutureFees", JSON.stringify(String(feesIntStudent[0]).split("(")[0]))
                                        if (String(feesIntStudent[0]).indexOf("(") > -1) {
                                            var splitdata = String(feesIntStudent[0]).split("(")[0];
                                            console.log("FutureFees", JSON.stringify(splitdata))
                                            futureFees = String(splitdata).split(":")[1] //current fees
    
                                            console.log("FutureFees", JSON.stringify(futureFees))
                                            if (feesIntStudent.indexOf("- $") > -1) {
                                                feesIntStudent = feesIntStudent.split("-")[1];
                                            }
                                        }
                                    }
                                    else {
                                        //else {
                                        feesIntStudent = feesIntStudent.split(")");
                                        console.log("feesIntStudent_else", feesIntStudent);
                                        futureFees = feesIntStudent[0];   //future fees 
                                        if (feesIntStudent[1].indexOf("(") > -1) {
                                            currentFees = feesIntStudent[1].split("(")[0];
                                            feesIntStudent = feesIntStudent[1].split(":")[1].split("(")[0]; //current fees
    
                                            if (feesIntStudent.indexOf("- $") > -1) {
                                                feesIntStudent = feesIntStudent.split("-")[1];
                                            }
                                        }
                                        else {
                                            futureFees = feesIntStudent[0];
                                            feesIntStudent = feesIntStudent[0].split(":")[1].split("(")[0]; //current fees
    
                                            if (feesIntStudent.indexOf("- $") > -1) {
                                                feesIntStudent = feesIntStudent.split("-")[1];
                                            }
                                        }
                                        //}
                                        console.log("feesIntStudent1", feesIntStudent);
                                    }
                                }
                                else {
                                   
                                    feesIntStudent = feesIntStudent.split("time");
                                    console.log("feesIntStudent_3_1", feesIntStudent);
                                    if(feesIntStudent.includes('per study period')){
                                        feesIntStudent=feesIntStudent.split('per')[0]
                                        console.log("per study",feesIntStudent)
                                    }
                                    //currentFees = feesIntStudent[1].split("(")[0].split("- $")[1];
    
                                    //console.log("feesIntStudent++++++>", currentFees);   //future fees  
    
                                   // futureFees = feesIntStudent[0].split("(")[0].split("- $")[1];
                                    //console.log("futureFees.,.,.,...", futureFees);
                                    console.log("feesIntStudent_3_2",feesIntStudent)
                                    if(!feesIntStudent.includes('(') ){
                                        feesIntStudent=feesIntStudent[0].split(":")
                                        console.log("feesIntStudent_3_3",feesIntStudent)
                                    }else{
                                        feesIntStudent = feesIntStudent[1].split(":")[1].split("(")[0];
                                    }

                                    
    
                                    //current fees
                                    if (feesIntStudent.indexOf("- $") > -1) {
                                        feesIntStudent = feesIntStudent.split("-")[1];
                                        // feesIntStudent = currentFees;
                                        console.log("IntStudent-=-=-=-=-=-=->", feesIntStudent);
                                    }
                                    if (feesIntStudent.indexOf(" $") > -1) {
                                        feesIntStudent = feesIntStudent.split(":")[0];
                                        // feesIntStudent = currentFees;
                                        console.log("feesIntStudent-=-=-=-=-=-=->", feesIntStudent);
                                    }
    
                                }
                            }
                            else{
                                console.log("inn else")
                                feesIntStudent=0
                            }
                            
                            console.log("FutureFees", futureFees)
                            console.log("CurrentFees", currentFees)
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                           

                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) {
                                // extract only digits  
                                if (String(feesIntStudent).includes("2019") && !String(feesIntStudent).includes("2020")) {
                                    console.log("YESSSSS", feesIntStudent);
                                    // feesIntStudent = currentFees

                                }

                                console.log("feesIntStudent3", feesIntStudent);
                                const regEx = /\d/g;
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                let feesVal1 = String(feesWithDollorTrimmed).replace('$', '');

                                if (feesVal1.indexOf('(') > -1) {
                                    feesVal1 = feesVal1.split('(')[0];
                                }
                                if (feesVal1.indexOf(':') > -1) {
                                    feesVal1 = feesVal1.split(':')[1];
                                }
                                let feesVal = feesVal1;

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        const feesval2 = String(feesWithDollorTrimmed).replace('$', '');
                                        console.log("feesval2--+++++>", feesval2);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        console.log('feesNumber =@@ ', Number(feesNumber))
                                        if (futureFees) {
                                            let feesValNum = futureFees.match(regEx);
                                            feesValNum = feesValNum.join('');
                                            let feesNumber1;
                                            if (feesValNum.includes(',')) {
                                                feesNumber1 = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber1 = feesValNum;
                                            }
                                            console.log("Regex fotr future fees ", feesNumber1);
                                            feesDict.international_student={
                                                amount: Number(feesNumber1),
                                                duration: 1,
                                                unit: "Year",                                                
                                                description: feesNumber1
                                               
                                            };
                                        }
                                        else if (Number(feesIntStudent)) {
                                            console.log("YESSS Number")
                                            feesDict.international_student={
                                                amount: Number(feesIntStudent),
                                                duration: 1,
                                                unit: "Year",                                               
                                                description: feesIntStudent
                                               
                                            };
                                            // feesDict.international_student = Number(feesNumber);
                                            console.log("Fees Array_inif", feesDict.international_student)
                                        } else {
                                            feesDict.international_student={
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",                                                
                                                description: "",
                                               
                                            };
                                            console.log("Fees Array_inelse", feesDict.international_student)
                                        }
                                    }
                                }
                            }
                            console.log("Fees Array_final", feesDict.international_student)

                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                               
                                console.log("Future@@@@", futureFees)
                                //  Futurefees = futureFees.split('- $');
                                // feesDict.international_student_all_fees = Futurefees;
                                // console.log("Futurefiiii--->", futureFees);                                
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }

                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let i = 0; i < campus.length; i++) {
                                        feesList.push({ name: campus[i].name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;

                                }
                            }
                            else {
                                feesDict.international_student={
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",                                                
                                    description: "",
                                   
                                };
                                
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let i = 0; i < campus.length; i++) {
                                        feesList.push({ name: campus[i].name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                if (courseTuitionFee) {
                                   // resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;
                        }

                        case 'course_study_mode': { // Location Launceston

                            resJsonData.course_study_mode = "On campus";
              
                            break;
                          }
                        case 'course_campus_location': {

                            const courseKeyVals = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                var crcode = selList.toString().split(' ');
                                                var cricoscode = [crcode[0]];
                                                //cricoscode = crcode[0];
                                                course_cricos_code = cricoscode;
                                            }
                                        }
                                    }
                                }
                            }

                            var campLocationText = []; // Location Launceston
                            console.log('course_campus_location matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedSelStr = [];
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                concatnatedSelStr = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if(concatnatedSelStr=[]){
                                concatnatedSelStr.push('Kelvin Grove')
                            }

                            // add only if it has valid value              
                            if (concatnatedSelStr && concatnatedSelStr.length > 0) {

                                for (let sel of concatnatedSelStr) {
                                    console.log('course_campus_location sel = ' + JSON.stringify(sel));
                                    let campuss = ['Gardens Point', 'Kelvin Grove']
                                    // for (let sel of selList) {
                                    console.log("Ayushi -->", sel);
                                    if (campuss.includes(sel)) {
                                        campLocationText.push({
                                            "name": sel,
                                            "code":String(course_cricos_code)
                                        });
                                    }


                                    //concatnatedSelStr.push(sel.trim());
                                }
                                resJsonData.course_campus_location = campLocationText;
                                console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);
                            }
                            break;
                        }
                        case 'course_intake_url': {
                            const courseKeyVal = courseScrappedData.course_intake_url;
                            var url = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                url = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (url.length > 0) {
                                resJsonData.course_intake_url = url;
                            }
                            break;
                        }
                        case 'course_intake': {
                            var courseIntake = [];
                           
                            const IntakeStr = await utils.getValueFromHardCodedJsonFile('course_intake');
                            var courseIntakeStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            console.log("NEwIntakeDATA-->", JSON.stringify(courseScrappedData.course_intake))
                            if (courseIntakeStr) {
                                if (courseIntakeStr.indexOf("and") > -1) {
                                    courseIntakeStr = courseIntakeStr.split("and");
                                    console.log("courseIntakeStr@@@and", courseIntakeStr);
                                }
                                if (Array.isArray(courseIntakeStr)) {
                                    for (let intake of courseIntakeStr) {
                                        if (intake.indexOf(",") > -1) {
                                            intake = intake.split(",");
                                            for (let month of intake) {
                                                courseIntake.push(month.trim());
                                            }
                                        }
                                        else {
                                            courseIntake.push(intake.trim());
                                        }
                                    }
                                }
                                else if (courseIntakeStr.indexOf("any time") > -1) {
                                    console.log("yesss anytime")
                                    if (String(courseIntakeStr).includes("Start")) {
                                        var splitdata = String(courseIntakeStr).split("Start");
                                        console.log("SplitData", JSON.stringify(splitdata))
                                        courseIntake.push(splitdata[1].trim());
                                    } else {
                                        courseIntake.push(courseIntakeStr.trim());
                                    }


                                }
                                else {
                                    courseIntake.push(courseIntakeStr.trim());
                                }

                            }

                            //console.log("R courseIntake",courseIntake);
                            var locationArray = resJsonData.course_campus_location;
                            console.log("locationArray", locationArray);

                            if (courseIntake && courseIntake.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let i = 0; i < locationArray.length; i++) {
                                    resJsonData.course_intake.intake.push({
                                        "name": locationArray[i].name,
                                        "value": courseIntake
                                    });
                                }
                                var newintake = resJsonData.course_intake.intake
                                console.log("New Intake data@@@", newintake)
                                let formatedIntake = await format_functions.providemyintake(newintake, "MOM", "");
                                console.log("NEw Intake Format@@@@@" + JSON.stringify(formatedIntake))
                                
                                resJsonData.course_intake.intake = formatedIntake
                                resJsonData.course_intake.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                
                               
                            }
                            break;
                        }                  
                                        
                        case 'course_country':
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            console.log(funcName + 'matched case course_career_outcome: ' + key);
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            var course_career_outcome = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            } // rootElementDictList
                            // add only if it has valid value
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            } else {
                                resJsonData.course_career_outcome = []
                            }
                            break;
                        }
                        
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            console.log("courseScrappedData.program_code = ", courseScrappedData.program_code);
                            var program_code = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList[0].trim();
                                            }
                                            else {
                                                program_code = "";
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            resJsonData.program_code = program_code;
                            break;
                        }


                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            if (title.includes(' (')) {
                              const title_text = title.replace('(', '( ');
                              console.log("splitStr@@@2" + title_text);
                              title = title_text;
                              console.log("splitStr@@@2" + title);
                      
                            }
                            var ctitle = format_functions.titleCase(title).trim();
                           // var ctitle1 = ctitle.replace('(','').replace(')','').trim();
                          var ctitle2 = ctitle.replace(' ( ','(');
                            console.log("ctitle@@@", ctitle2.trim());
                            resJsonData.course_title = ctitle2
                            break;
                        }
                        // 
                        case 'course_study_level': {
                            resJsonData.course_study_level = studyLevel;
                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                             const courseKeyVal_major = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline);
                             const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                             let course_outlines = {};
                             if (courseKeyVal_minor != null) {
                                 course_outlines.minors = courseKeyVal_minor
                             } else {
                                 course_outlines.minors = []
                             }
                             if (courseKeyVal_major != null) {
                                 course_outlines.majors = courseKeyVal_major
                             } else {
                                 course_outlines.majors = []
                             }
                             if (courseKeyVal != null) {
                                 course_outlines.more_details = courseKeyVal
                             } else {
                                 course_outlines.more_details = ""
                             }
                             resJsonData.course_outline = course_outlines
                             break;
                         }
                         case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            console.log("Application==>",courseKeyVal)
                            if (courseKeyVal == null || courseKeyVal == "null" ) {
                                resJsonData.application_fees = ""
                            } else {
                                resJsonData.application_fees = courseKeyVal
                            }
                            break;
                        }
                        // 
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)            
            var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace("/", "_").toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                // console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                // NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                // NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                // NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "NA";
                }
                // for (let myfees of resJsonData.course_tuition_fee.fees) {
                //     if (myfees.name == location) {
                //         NEWJSONSTRUCT.international_student_all_fees = [];
                //     }
                // }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                //resJsonData.basecourseid = location_wise_data.course_id;
                
                //resJsonData.course_cricos_code = location_wise_data.cricos_code;
                // resJsonData.current_course_campus_location = location_wise_data.course_campus_location;
                //resJsonData.current_course_intake = location_wise_data.course_intake;
                //  resJsonData.course_intake_display = location_wise_data.course_intake;
                resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                //const res = await awsUtil.putFileInBucket(filelocation); 
                //console.log(funcName + 'S3 object location = ' + res);
                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
            }
            return resJsonData;

        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }
    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            //const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText);
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.studyLevel);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
