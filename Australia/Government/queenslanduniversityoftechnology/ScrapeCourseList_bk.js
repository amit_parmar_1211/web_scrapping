const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');

const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {

  // async scrapeOnlyInternationalCourseList_Mapping(page) {
  //   const funcName = 'scrapeCourseCategoryList ';
  //   // generate category list
  //   const categoryUrl = this.selectorJson.course_list_selector[0].url;
  //   console.log("categoryUrl", categoryUrl);
  //   const categoryUrlSelector = this.selectorJson.course_list_selector[0].elements[0].selectors[0];
  //   const categoryUrlHandles = await page.$$(categoryUrlSelector);
  //   const categoryTextHandles = categoryUrlHandles;
  //   var totalCategoryList = [];
  //   for (let i = 0; i < categoryUrlHandles.length; i++) {
  //     let elementText = await page.evaluate(el => el.innerText, categoryTextHandles[i]);
  //     let elementUrl = await page.evaluate(el => el.href, categoryUrlHandles[i]);
  //     totalCategoryList.push({ href: elementUrl, innerText: elementText });
  //   }
  //   console.log(funcName + 'totalCategoryList = ' + JSON.stringify(totalCategoryList));
  //   await fs.writeFileSync("./selectors/main_categoryList.json", JSON.stringify(totalCategoryList));
  //   console.log(funcName + 'writing category list to file completed successfully....');

  //   //generating course list
  //   const fileData = JSON.parse(await fs.readFileSync("./selectors/main_categoryList.json"));
  //   const courseUrlSelector = "//div[@class='course-list']/div[@role='list-item']/div[1]//a";
  //   const courseTextSelector = courseUrlSelector;
  //   var totalCourseList = [];
  //   //iterate through category list
  //   for (let target of fileData) {
  //     console.log('******************Started Scrapping ', target.innerText, '*****************************');
  //     await page.goto(target.href);
  //     //console.log('******************Click on anchor*****************************');
  //     let click1 = "div.col-xs-10.col-md-9.col-lg-7 > div > ul > li.d-none.d-lg-inline-block.hidden-md-down > a";
  //     let click2 = "main > div > div.row.no-gutters > div.btn-group.col-md-10.col-lg-8.col-xl-7 > div > div:nth-child(5)";
  //     await page.evaluate((click1) => document.querySelector(click1).click(), click1);
  //     //await page.waitForSelector(click2);      
  //     await page.evaluate((click2) => document.querySelector(click2).click(), click2);
  //     //console.log('******************END click on anchor*****************************************');
  //     //start scrapping
  //     if (target.innerText == "English and pathway programs") {
  //       //because we only need to select pathways from this category.
  //       let courseUrlSelector = "//*[@id='course-category-listing-panel-content']/div[@id='Pathway-programs-tab']//div[@class='course-list']/div[@role='list-item']/div[1]//a";
  //       let courseTextSelector = courseUrlSelector;
  //       let courseUrlHandles = await page.$x(courseUrlSelector);
  //       let courseTextHandles = await page.$x(courseTextSelector);
  //       for (let i = 0; i < courseUrlHandles.length && i < courseTextHandles.length; i++) {
  //         let elementUrl = await page.evaluate(el => el.href, courseUrlHandles[i]);
  //         let elementText = await page.evaluate(el => el.innerText, courseTextHandles[i]);
  //         //check for duplicates
  //         let exists = totalCourseList.some(value => { return value.href == elementUrl });
  //         if (exists == false) {
  //           totalCourseList.push({ href: elementUrl, innerText: elementText });
  //         }
  //       }
  //     }
  //     else {
  //       let courseUrlHandles = await page.$x(courseUrlSelector);
  //       let courseTextHandles = await page.$x(courseTextSelector);
  //       for (let i = 0; i < courseUrlHandles.length && i < courseTextHandles.length; i++) {
  //         let elementUrl = await page.evaluate(el => el.href, courseUrlHandles[i]);
  //         let elementText = await page.evaluate(el => el.innerText, courseTextHandles[i]);
  //        //check for duplicates
  //        let exists = totalCourseList.some(value => { return value.href == elementUrl });
  //        if (exists == false) {
  //          totalCourseList.push({ href: elementUrl, innerText: elementText });
  //        }
  //       }
  //     }
  //     console.log('******************Finished Scrapping ', target.innerText, '*****************************');
  //   }
  //   console.log(funcName + 'writing course list to file....');
  //   console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
  //   await fs.writeFileSync("./output/queenslanduniversityoftechnology_courselist.json", JSON.stringify(totalCourseList));
  // }
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
      var mainCategory = [], redirecturl = [];
      var datalist = [];
      //scrape main category
      const maincategoryselector = "//*[@id='study-menu']/div/div/div[1]/div[4]/div/div/div[1]/div[2]/ul/li/a";
      var category = await page.$x(maincategoryselector);
      console.log("Total categories-->" + category.length);
      for (let link of category) {
        var categorystringmain = await page.evaluate(el => el.innerText, link);
        var categorystringmainurl = await page.evaluate(el => el.href, link);
        mainCategory.push(categorystringmain.trim());
        redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
      }
  
      //scrape courses
      for (let catc of redirecturl) {
        await page.goto(catc.href, { timeout: 0 });
        const selector = "//*/div[@id='course-category-listing-panel-content']/div//a";
        var title = await page.$x(selector);
        for (let t of title) {
          var categorystring = await page.evaluate(el => el.innerText, t);
          var categoryurl = await page.evaluate(el => el.href, t);
          datalist.push({ href: categoryurl, innerText: categorystring, category: catc.innerText });
        }
      }
  
      fs.writeFileSync("./output/queenslanduniversityoftechnology_original_courselist.json", JSON.stringify(datalist))
      fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
      // await fs.writeFileSync("./output/swinburneuniversityoftechnology_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < datalist.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (datalist[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [] });
        }
      }

      await fs.writeFileSync("./output/queenslanduniversityoftechnology_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      // for (let i = 0; i < totalCourseList.length; i++) {
      //   for (let j = 0; j < uniqueUrl.length; j++) {
      //     if (uniqueUrl[j].href == totalCourseList[i].href) {
      //       uniqueUrl[j].category.push(totalCourseList[i].category);
      //     }
      //   }
      // }
      //based on unique urls mapping of categories
      for (let i = 0; i < datalist.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == datalist[i].href) {
            if (uniqueUrl[j].category.includes(datalist[i].category)) {

            } else {
              uniqueUrl[j].category.push(datalist[i].category);
            }

          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/queenslanduniversityoftechnology_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }

  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load filez
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });

      const courseListSel = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!courseListSel) {
        console.log(funcName + 'Invalid courseListSel');
        throw (new Error('Invalid courseListSel'));
      }
      const elementDictForCourseListSel = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCourseListSel) {
        console.log(funcName + 'Invalid elementDictForCourseListSel');
        throw (new Error('Invalid elementDictForCourseListSel'));
      }
      console.log("courseListSel", courseListSel);
      console.log("elementDictForCourseListSel", elementDictForCourseListSel);
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      //set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
     
      if (s) {
        await s.close();
      }
      //return totalCourseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // refers to https://futurestudents.csu.edu.au/courses/all
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // course_level_form_selector key
      // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelList) {
      //   console.log(funcName + 'Invalid formSelList');
      //   throw (new Error('Invalid formSelList'));
      // }
      // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelElementDict) {
      //   console.log(funcName + 'Invalid formSelElementDict');
      //   throw (new Error('Invalid formSelElementDict'));
      // }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
     
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
