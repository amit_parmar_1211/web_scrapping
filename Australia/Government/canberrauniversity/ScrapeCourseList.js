const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');

class ScrapeCourseList extends Scrape {

  async scrapeCourseListAndPutAtS3() {
    var s = null;
    const funcName = 'startScrappingFunc ';
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var datalist = [];
      var studylevel = [];
      await s.setupNewBrowserPage("https://www.canberra.edu.au/");

      var activeall = "//*[@id='browse_courses']";
      var button = await s.page.$x(activeall);
      await button[0].click();
      await s.page.waitFor(6000);
      var levelselector = "//*[@id='uc-facets']/div/ul/li/a[not(contains(text(),'All'))  and not(contains(text(),'Majors & Minors'))]";
      const activeSelector = "//*/ul/li/a[contains(text(),'Next')]";
      var levels = await s.page.$x(levelselector);
      console.log("Length", levels.length)
      for (let level of levels) {

        const levelurl = await s.page.evaluate(el => el.href, level)

        const levelstring = await s.page.evaluate(el => el.innerText, level);
        studylevel.push({ href: levelurl.trim(), innerText: levelstring.trim() })

      }
      console.log("Studylevel", studylevel)

      for (let i = 0; i < studylevel.length; i++) {
        await s.page.goto(studylevel[i].href, { timeout: 0 });
       

        console.log("Processed...!" + studylevel[i])
        const selector = "//*/table/tbody/tr/td[1]/small/a";
        var ispage = true;
        while (ispage) {
          var title = await s.page.$x(selector);
          for (let t of title) {
            var categorystring = await s.page.evaluate(el => el.innerText, t);
            var categoryurl = await s.page.evaluate(el => el.href, t);
            datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), study_level: studylevel[i].innerText });
          }
          var active = await s.page.$x(activeSelector);
          if (active[0]) {
            await active[0].click();
            await s.page.waitFor(5000);
            console.log("page next");
          }
          else {
            ispage = false;
          }
          console.log("Data list::" + datalist.length);
        }
       // fs.writeFileSync("./output/universityofcanberra_courselist.json", JSON.stringify(datalist))

        fs.writeFileSync("./output/universityofcanberra_original_courselist.json", JSON.stringify(datalist))
      //  fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
  
        let uniqueUrl = [];
        //unique url from the courselist file
        for (let i = 0; i < datalist.length; i++) {
          let cnt = 0;
          if (uniqueUrl.length > 0) {
            for (let j = 0; j < uniqueUrl.length; j++) {
              if (datalist[i].href == uniqueUrl[j].href) {
                cnt = 0;
                break;
              } else {
                cnt++;
              }
            }
            if (cnt > 0) {
              uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, study_level: datalist[i].study_level });
            }
          } else {
            uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, study_level: datalist[i].study_level });
          }
        }
        await fs.writeFileSync("./output/universityofcanberra_unique_courselist.json", JSON.stringify(uniqueUrl));
       
        await fs.writeFileSync("./output/universityofcanberra_courselist.json", JSON.stringify(uniqueUrl));
        console.log(funcName + 'writing course list to file....');
        console.log(funcName + 'totalCourseList = ' + JSON.stringify(datalist));

      }

       await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

} // class

module.exports = { ScrapeCourseList };
