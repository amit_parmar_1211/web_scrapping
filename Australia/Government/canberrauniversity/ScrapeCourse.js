const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');
class ScrapeCourse extends Course {
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl,study_level) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'course_discipline': {
              const courseKeyVal = courseScrappedData.course_discipline;
              var course_discipline = [];
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        course_discipline = selList;
                      }
                    }
                  }
                }
              }
              resJsonData.course_discipline = course_discipline;
              break;
            }
            case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              const englishList = [];
              var courseAdminReq = {};
              var ieltsScore = "";
              var penglishList = [];
              const pieltsDict = {};
              if (courseScrappedData.course_toefl_ielts_score) {
                ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                var ielts = {};
                if (ieltsScore) {
                  ielts.name = 'ielts academic';
                  ielts.description = ieltsScore;
                  ielts.min = 0;
                  ielts.require = await utils.giveMeNumber(ieltsScore);
                  ielts.max = 9;
                  ielts.R = 0;
                  ielts.W = 0;
                  ielts.S = 0;
                  ielts.L = 0;
                  ielts.O = 0;
                  englishList.push(ielts);
                } 
                else {
                  ielts.name = 'ielts academic';
                  ielts.description = configs.propValueNotAvaialble;
                  ielts.min = 0;
                  ielts.require = await utils.giveMeNumber(ieltsScore);
                  ielts.max = 9;
                  ielts.R = 0;
                  ielts.W = 0;
                  ielts.S = 0;
                  ielts.L = 0;
                  ielts.O = 0;
                  englishList.push(ielts);
                }
              }
              if (englishList && englishList.length > 0) {
                courseAdminReq.english = englishList;
              }
              if (courseScrappedData.course_academic_requirement) {
                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                if (academicReq && String(academicReq).length > 0) {
                  courseAdminReq.academic = [academicReq];
                }
              }
              var filedata = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
              courseAdminReq.entry_requirements_url = filedata["entry_requirements"];
              courseAdminReq.academic_requirements_url = filedata["course_admission_english_more_details"];
              courseAdminReq.english_requirements_url = filedata["english_more"];

              resJsonData.course_admission_requirement = courseAdminReq;
              break;
            }
            case 'course_url': {
              if (courseUrl && courseUrl.length > 0) {
                resJsonData.course_url = courseUrl;
              }
              break;
            }
            case 'course_duration_full_time': {
              // display full time
              var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
              var fullTimeText = "";
              const courseKeyVal = courseScrappedData.course_duration_full_time;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        fullTimeText = selList[0];
                      }
                    }
                  }
                }
              }
              if (fullTimeText && fullTimeText.length > 0) {
                const resFulltime = fullTimeText;
                  resJsonData.course_duration = fullTimeText;
               
                if (resFulltime) {

                  console.log("resfulltime>>",resFulltime);
                
                  durationFullTime.duration_full_time = resFulltime;
                  courseDurationList.push(durationFullTime);

                  courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                  let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                  console.log("filtered_duration_formated", filtered_duration_formated);
                  console.log("courseDurationDisplayList2", courseDurationDisplayList);
                 
                  if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                    resJsonData.course_duration_display = filtered_duration_formated;
                    var isfulltime = false, isparttime = false;
                    filtered_duration_formated.forEach(element => {
                      if (element.display == "Full-Time") {
                        isfulltime = true;
                      }
                      if (element.display == "Part-Time") {
                        isparttime = true;
                      }
                    });
                    resJsonData.isfulltime = isfulltime;
                    resJsonData.isparttime = isparttime
                  }
                }
              }
                else{
                  resJsonData.course_duration = "";
                }
              
              break;
            }
            case 'course_tuition_fee':
            case 'course_study_mode':
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_tuition_fees_international_student_first_year': {
              const feesDict = {};
              const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_first_year);
              if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                console.log("international_student" + feesIntStudent)
                feesDict.international_student = feesIntStudent;
                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                console.log(funcName + 'feesVal = ' + feesVal);
                if (feesVal) {
                  const myval = feesVal.split(".");
                  const regEx = /\d/g;
                  let feesValNum = myval[0].match(regEx);
                  if (feesValNum) {
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    feesValNum = feesValNum.join('');
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    let feesNumber = null;
                    if (feesValNum.includes(',')) {
                      feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                    } else {
                      feesNumber = feesValNum;
                    }
                    console.log(funcName + 'feesNumber = ' + feesNumber);
                    if (Number(feesNumber)) {
                      feesDict.international_student_first_year = Number(feesNumber);
                    }
                  }
                }
              }
              else {
                feesDict.international_student_first_year = "NA";
              }
              break;
            }
            case 'course_tuition_fees_international_student': {
              const courseTuitionFee = {};
              const feesList = [];
              const feesDict = {};
              
              //for first year only
              const feesIntStudent_first_year = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_first_year);
              if (feesIntStudent_first_year && feesIntStudent_first_year.length > 0) { // extract only digits
              console.log("international_student" + feesIntStudent_first_year)
              // feesDict.international_student = feesIntStudent;
              const feesWithDollorTrimmed = String(feesIntStudent_first_year).trim();
              console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
              const feesVal = String(feesWithDollorTrimmed).replace('$', '');
              console.log(funcName + 'feesVal = ' + feesVal);
              if (feesVal) {
              const myval = feesVal.split(".");
              const regEx = /\d/g;
              let feesValNum = myval[0].match(regEx);
              if (feesValNum) {
              console.log(funcName + 'feesValNum = ' + feesValNum);
              feesValNum = feesValNum.join('');
              console.log(funcName + 'feesValNum = ' + feesValNum);
              let feesNumber = null;
              if (feesValNum.includes(',')) {
              feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
              } else {
              feesNumber = feesValNum;
              }
              console.log(funcName + 'feesNumber = ' + feesNumber);
              if (Number(feesNumber)) {
              var feesint = {};
              feesint.amount = Number(feesNumber);
              feesint.duration = "1";
              feesint.unit = "year";
              feesint.description = feesIntStudent_first_year;
              feesDict.international_student = feesint;
              }
              }
              }
              }
              else {
              var feesint = {};
              feesint.amount = 0;
              feesint.duration = "1";
              feesint.unit = "year";
              feesint.description = feesIntStudent_first_year;
              feesDict.international_student = feesint;
              }
              
              const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
              const courseKeyVal = courseScrappedData.course_tuition_fees_international_student;
              let feesIntStudent = null;
              if (Array.isArray(courseKeyVal)) {
              for (const rootEleDict of courseKeyVal) {
              console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
              const elementsList = rootEleDict.elements;
              for (const eleDict of elementsList) {
              console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
              const selectorsList = eleDict.selectors;
              for (const selList of selectorsList) {
              console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
              if (Array.isArray(selList) && selList.length > 0) {
              feesIntStudent = selList;
              }
              }
              }
              }
              }
              // if (feeYear && feeYear.length > 0) {
              // courseTuitionFee.year = feeYear;
              // }
              
              if (feesDict.international_student) { // if we have fees value then add following supporting attributes
              const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
              const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
              if (feesDuration && feesDuration.length > 0) {
              feesDict.fee_duration_years = feesDuration;
              }
              if (feesCurrency && feesCurrency.length > 0) {
              feesDict.currency = feesCurrency;
              }
              
              if (feesDict) {
              var campus = resJsonData.course_campus_location;
              for (let loc of campus) {
              feesList.push({ name: loc.name, value: feesDict });
              }
              }
              if (feesList && feesList.length > 0) {
              courseTuitionFee.fees = feesList;
              }
              console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
              if (courseTuitionFee) {
              resJsonData.course_tuition_fee = courseTuitionFee;
              }
              }
              break;
              }
            case 'course_campus_location': { // Location Launceston


              const courseKeyValcd = courseScrappedData.course_cricos_code;
              let course_cricos_code = "";
              if (Array.isArray(courseKeyValcd)) {
                  for (const rootEleDict of courseKeyValcd) {
                      console.log(funcName + '\n\r rootEleDictcric = ' + JSON.stringify(rootEleDict));
                      const elementsList = rootEleDict.elements;
                      for (const eleDict of elementsList) {
                          console.log(funcName + '\n\r eleDictcric = ' + JSON.stringify(eleDict));
                          const selectorsList = eleDict.selectors;
                          for (const selList of selectorsList) {
                              console.log(funcName + '\n\r selListcric = ' + JSON.stringify(selList));
                              if (Array.isArray(selList) && selList.length > 0) {
                                  course_cricos_code = selList;
                              }
                              else{
                                course_cricos_code = selList;
                              }
                          }
                      }
                  }
              }
              const campLocationText = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
              console.log(funcName + 'campLocationText = ' + campLocationText);
              if (campLocationText && campLocationText.length > 0) {
                var campLocationValTrimmed = String(campLocationText).trim();
                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                  var mylocations = await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                  var mylocations_rem = [];
                  mylocations.forEach(element => {
                    if (element.toLowerCase() != "online" && element.toLowerCase() != "distance") {
                      mylocations_rem.push(element);
                    }

                  });

                  if (mylocations_rem && mylocations_rem.length > 0) {

                    // if (location_val && location_val.length > 0) {
                    var campusedata = [];
                    mylocations_rem.forEach(element => {
                      campusedata.push({
                        "name": element,
                        "code":String(course_cricos_code)
                      })
                    });
                    //contains only australia campuses
                    console.log("cmpdata", campusedata);
                    var naindex;
                   for(let item of campusedata){
                     if(item.name.includes("UCI") === true){
                     naindex = campusedata.indexOf(item);
                      console.log("chkin>",naindex);
                     if (naindex > -1) {
                      campusedata.splice(naindex, 1);
                      }
                    }else{
                      campusedata=campusedata;
                    }
                    }
                    console.log("chkin>",campusedata);
                    resJsonData.course_campus_location = campusedata;
                    resJsonData.course_study_mode = "On campus";
                    console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

                  }
                  else {
                    throw new Error("No campus location found.");
                  }
                 
                }
              } // if (campLocationText && campLocationText.length > 0)
              break;
            }
            case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
              var courseIntakeStr = [];//= await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
              if (resJsonData.course_title == "Master of Information Technology - 846AA" || resJsonData.course_title == "Master of Information Technology and Systems - 973AA") {
                console.log("Fix intake-->")
                var myintake_data = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                var campus = resJsonData.course_campus_location;
                var intakes = [];
                for (var count = 0; count < campus.length; count++) {
                  var intakedetail = {};
                  intakedetail.name = campus[count].trim();
                  var intake_value = [];
                  intake_value.push(myintake_data["fix_intake"]);
                  intakedetail.value = intake_value;
                  intakes.push(intakedetail);
                }
                console.log("DATA--->", intakes)
                // var intakedata = {};
                // intakedata.intake = await utils.providemyintake(intakes, "MOM", "");
                let formatedIntake = await utils.providemyintake(intakes, "MOM", "");
                console.log("FormatIntake", JSON.stringify(formatedIntake))
                formatedIntake.forEach(element => {
                  element.value.forEach(elementdate => {
                    if (elementdate.actualdate == '11 February 2019') {
                      elementdate.filterdate = "2019-02-11"
                    } else if (elementdate.actualdate == '5 August 2019') {
                      elementdate.filterdate = "2019-08-05"
                    }
                    console.log("DATA--->", elementdate)
                  })
                });
                //intakedata.intake = await utils.providemyintake(intakes, "MOM", "");
                var intakedata = {};
                intakedata.intake = formatedIntake;
                intakedata.more_details = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake_url);;
                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
              }
              else {
                const courseKeyVal = courseScrappedData.course_intake;
                console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                if (Array.isArray(courseKeyVal)) {
                  for (const rootEleDict of courseKeyVal) {
                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                      console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                      const selectorsList = eleDict.selectors;
                      for (const selList of selectorsList) {
                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        if (Array.isArray(selList) && selList.length > 0) {
                          courseIntakeStr = selList;
                        }
                      }
                    }
                  }
                }
                console.log("intakes" + JSON.stringify(courseIntakeStr));
                var myarry = [];
                for (var i = 0; i < courseIntakeStr[0].length; i++) {
                  var myinnerarray = [];
                  console.log("outer-->" + courseIntakeStr[0][i]);
                  if (courseIntakeStr[0][i] != 2021 ) {
                    for (var j = 0; j < courseIntakeStr.length; j++) {
                      myinnerarray.push(courseIntakeStr[j][i]);
                    }
                  }
                  else {
                    break;
                  }
                  myarry.push(myinnerarray);
                }
                console.log("mydataarray" + JSON.stringify(myarry))

                var myintake_data = JSON.parse(fs.readFileSync("./selectors/university_hard_coded_data.json"))[0];

                if (myarry && myarry.length > 0) {
                  var campus = resJsonData.course_campus_location;
                  var intakes = [];
                  console.log("Intake length-->" + myarry.length);
                  //console.log("Campus length-->" + campus.length);
                  for (var count = 0; count < myarry.length; count++) {
                    console.log("intake -->" + count)

                    var intakedetail = {};
                    intakedetail.name = myarry[count][1].replace(/[\r\n\t ]+/g, ' ');
                    console.log("intakedetail.name----->>", intakedetail.name);
                    var myintakeArray = [];
                    if (myarry[count][3].replace(/[\r\n\t ]+/g, ' ').includes("Semester 1")) {
                      console.log("Match--> t1")
                      myintakeArray.push(myintake_data["Semester 1"])
                    }
                    if (myarry[count][3].replace(/[\r\n\t ]+/g, ' ').includes("Semester 2")) {
                      console.log("Match--> t2")
                      myintakeArray.push(myintake_data["Semester 2"])
                    }
                    intakedetail.value = myintakeArray;
                    intakes.push(intakedetail);

                  }
                  console.log("DATA--->", intakes)
                  let formatedIntake = await utils.providemyintake(intakes, "MOM", "");
                  console.log("FormatIntake", JSON.stringify(formatedIntake))
                  formatedIntake.forEach(element => {
                    element.value.forEach(elementdate => {
                      if (elementdate.actualdate == '11 February 2019') {
                        elementdate.filterdate = "2019-02-11"
                      } else if (elementdate.actualdate == '5 August 2019') {
                        elementdate.filterdate = "2019-08-05"
                      }
                      console.log("DATA--->", elementdate)
                    })
                  });
                  //intakedata.intake = await utils.providemyintake(intakes, "MOM", "");
                  var intakedata = {};
                  intakedata.intake = formatedIntake;
                  intakedata.more_details = resJsonData.course_intake_url;
                  resJsonData.course_intake = intakedata; 
                }
              }
              break;
            }
            
            case 'application_fee': {
              const courseKeyVal = courseScrappedData.application_fee;
     
              let applicationfee = null;
                if (Array.isArray(courseKeyVal)) {
              for (const rootEleDict of courseKeyVal) {
             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
             const elementsList = rootEleDict.elements;
             for (const eleDict of elementsList) {
             console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                const selectorsList = eleDict.selectors;
             for (const selList of selectorsList) {
                 console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
              if (Array.isArray(selList) && selList.length > 0) {
                applicationfee = selList[0];
                 }
                 else{
                    applicationfee = selList[0];
                 }
              }
             }
             }
         }
        if (applicationfee && applicationfee.length>0) {
          resJsonData.application_fee = applicationfee;
         console.log("appfee"+ JSON.stringify(resJsonData.application_fee));
         }
         else{
            resJsonData.application_fee = applicationfee;
         }
     
          break;
    
        }
        case 'course_outline': {
              
          let mgcrs = null;
        
       const majorcrs = {
         majors:[],
         minors:[],
         more_details:""
       };
       //for major cousrseList
       var more_outline = courseScrappedData.course_outline;
       console.log('"more_outline1", ' + JSON.stringify(more_outline));
       let mor_outline = [];
       if (Array.isArray(more_outline)) {
         for (const rootEleDict of more_outline) {
             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
             const elementsList = rootEleDict.elements;
             for (const eleDict of elementsList) {
                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                 const selectorsList = eleDict.selectors;
                 for (const selList of selectorsList) {
                     console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                     if (Array.isArray(selList) && selList.length > 0) {
                      for( const sel of selList){
                      // console.log("more_outttt1"+ sel);
                       mor_outline.push(sel);
                       
                       
                      }
                     }
                     else{
                     // mor_outline.push(majorcrs.majors);
                     }
                 }
             }
         }
     }
      //for course_outline_more_details 
      if(mor_outline.length > 0){
    majorcrs.majors = mor_outline;
      }
      else{
        majorcrs.majors = majorcrs.majors;
      }
      //for minor courseList
      var courseminor =  courseScrappedData.course_outline_minor;
      let mincrs = null;
      if (Array.isArray(courseminor)) {
        for (const rootEleDict of courseminor) {
            console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
            const elementsList = rootEleDict.elements;
            for (const eleDict of elementsList) {
                console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                const selectorsList = eleDict.selectors;
                for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    if (Array.isArray(selList) && selList.length > 0) {
                     mincrs = selList;
                    }
                }
            }
        }
    }
      if(mincrs && mincrs.length >0){
        majorcrs.minors=mincrs
      }
      else{
       majorcrs.minors=majorcrs.minors
     }
    
            resJsonData.course_outline =  majorcrs;
            break;
        }
            case 'course_study_level': {
              console.log(funcName + 'course_title course_study_level = ' + study_level);
              resJsonData.course_study_level = study_level.trim();
              break;
            }
            case 'course_country': {
              const courseKeyVal = courseScrappedData.course_country;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_country = resAccomodationCostJson;
              }
              break;
            }

            case 'course_overview': {
              const courseKeyVal = courseScrappedData.course_overview;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson && resAccomodationCostJson != null) {
                resJsonData.course_overview = resAccomodationCostJson;
              }
              else{
                resJsonData.course_overview = "";
              }
              break;
            }
            case 'course_career_outcome': {
              var fullTimeText = "";
              const courseKeyVal = courseScrappedData.course_career_outcome;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        fullTimeText = selList;
                      }
                    }
                  }
                }
              }
              if (fullTimeText[0]) {
                resJsonData.course_career_outcome = await utils.giveMeArray(fullTimeText[0].replace(/(?:[\n\t\r])/g, ';'), ";");
              }
              else {
                var mydata = [];
                resJsonData.course_career_outcome = mydata;
              }

              break;
            }
            case 'course_title': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
              // add only if it has valid value
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
              }
              if (resJsonData.course_title) {
                var title = resJsonData.course_title;
                var parogramcodedata = title.split("-");
                var mycode = [];
                mycode.push((parogramcodedata.length > 1) ? parogramcodedata[1].trim() : "NA");
                for (let program_val of mycode) {
                  resJsonData.program_code = program_val;
                }
                //resJsonData.program_code = mycode;
              }
              break;
            }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      var NEWJSONSTRUCT = {}, structDict = [];
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
       // console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name || dintake.name.includes(location)) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        }
        else {
          NEWJSONSTRUCT.course_intake = "NA";
        }
        // for (let myfees of resJsonData.course_tuition_fee.fees) {
        //   if (myfees.name == location) {
        //     NEWJSONSTRUCT.international_student_all_fees = myfees;
        //   }
        // }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        resJsonData.course_id = location_wise_data.course_location_id;
       
        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
       
      }
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }


  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = 'scrapeAndFormatCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);
      const elmstudy = await utils.checkElementAvailabilty(s.page, '#tab-links > li > a', "", "InnerText", "Study Pattern");
      var promise1 = Promise.resolve(elmstudy);
      promise1.then(async function (value) {
        if (value) {
          const link1 = await s.page.$('#tab-links > li:nth-child(4) > a');
          await link1.click();
          await s.page.waitForSelector('#study_pattern > div > div > p:nth-child(1) > a', s.WAIT_FOR_SEL_OPTIONS);
          const link = await s.page.$('#study_pattern > div > div > p:nth-child(1) > a');
          await link.click();
        }
        else console.log('#####--study pattern not found--#####');
      });

      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }
      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file
      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }
      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, await s.page.url(), courseDict.study_level);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
