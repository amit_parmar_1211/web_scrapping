const puppeteer = require('puppeteer');
const utils = require('./common/utils');
const fs = require("fs");
async function start() {
    const URL = "https://study.uwa.edu.au/courses%20and%20careers/find%20a%20course?level=";
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
    let page = await browser.newPage();

    await page.setViewport({ width: 1366, height: 900 });
    await page.goto(URL, { timeout: 0 });

    await page.$eval("#international-only", el => el.click());

    const btnclick = "div.checkbox-select.checkbox-select--interests > button";
    let scrappedBtn = await page.$$(btnclick);
    await scrappedBtn[0].click();
    const categoryselectorUrl = "//*[@id='00001']/div[1]/div/input[not(@id='all') and not(@id='Unsorted')]";
    const categoryselectorText = "//*[@id='00001']/div[1]/div/label[not(@for='all') and not(@for='Unsorted')]";
    var elementstring = "", elementhref = "", allcategory = [];

    const targetLinksCardsUrls = await page.$x(categoryselectorUrl);
    const targetLinksCardsText = await page.$x(categoryselectorText);
    var targetLinksCardsTotal = [];
    var totalCourseList = [];
    for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        await page.waitFor(2000);
        const btnclick = "div.checkbox-select.checkbox-select--interests > button";
        let scrappedBtn = await page.$$(btnclick);
        await scrappedBtn[0].click();
        elementstring = await page.evaluate(el => el.innerText, targetLinksCardsUrls[i]);
        let categoryType = await page.evaluate(el => el.innerText, targetLinksCardsText[i]);
        console.log("elementstring -->", categoryType);
        await page.waitFor(2000);
        targetLinksCardsUrls[i].click();
        await page.waitFor(2000);
        let clickSearch = await page.$$("div > div.module-wrapper.alt-bg > div > form > button");
        await page.waitFor(2000);
        await clickSearch[0].click();

        console.log('---------------------------call selector-------------------------------')
        await utils.checkandclick(page, "div.results-action > button.results-action-load-more");


        let linkselector = "div > div:nth-child(2) > div > div > div:nth-child(3) > div.results-list > a";
        let textselector = "div > div:nth-child(2) > div > div > div:nth-child(3) > div.results-list > a > div > h4"
        

        await page.waitFor(5000);
        const targetLinks = await page.$$(linkselector);
        const targetText = await page.$$(textselector);
        await page.waitFor(5000);
        console.log("#total link selectors---->" + targetLinks.length);
        for (var j = 0; j < targetLinks.length; j++) {
            var elementstring = await page.evaluate(el => el.innerText, targetText[j]);
            const elementlink = await page.evaluate(el => el.href, targetLinks[j]);
            totalCourseList.push({ href: elementlink, innerText: elementstring, category: categoryType });
        }
        const btnclick1 = "div.checkbox-select.checkbox-select--interests > button";
        let scrappedBtn1 = await page.$$(btnclick1);
        await scrappedBtn1[0].click();
        // let elementstring1 = await page.evaluate(el => el.innerText, targetLinksCardsUrls[i]);
        await page.waitFor(2000);
        targetLinksCardsUrls[i].click();
    }
    
    fs.writeFileSync("theuniversityofwesternaustralia_category_courselist.json", JSON.stringify(totalCourseList));
    await browser.close();
}
start();
