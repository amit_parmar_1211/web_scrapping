const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses

  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
      var subjectAreasArray = [];
      var codeList = [];
      let selector = "#intoncampus";

      await page.$eval("#international-only", el => el.click());

      // const btnclick = "div.checkbox-select.checkbox-select--interests > button";
      // let scrappedBtn = await page.$$(btnclick);
      //await scrappedBtn[0].click();
      const categoryselectorUrl = "//*[@id='00001']/div[1]/div/input[not(@id='all') and not(@id='Unsorted')]";
      const categoryselectorText = "//*[@id='00001']/div[1]/div/label[not(@for='all') and not(@for='Unsorted')]";
      var elementstring = "", elementhref = "", allcategory = [];

      const targetLinksCardsUrls = await page.$x(categoryselectorUrl);
      const targetLinksCardsText = await page.$x(categoryselectorText);
      var targetLinksCardsTotal = [];
      var totalCourseList = [];
      for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        await page.waitFor(2000);
        const btnclick = "div.checkbox-select.checkbox-select--interests > button";
        let scrappedBtn = await page.$$(btnclick);
        await scrappedBtn[0].click();
        elementstring = await page.evaluate(el => el.innerText, targetLinksCardsUrls[i]);
        let categoryType = await page.evaluate(el => el.innerText, targetLinksCardsText[i]);
        console.log("elementstring -->", categoryType);
        await page.waitFor(2000);
        targetLinksCardsUrls[i].click();
        await page.waitFor(5000);
        let clickSearch = await page.$$("div > div.module-wrapper.alt-bg > div > form > button");
        await page.waitFor(2000);
        await clickSearch[0].click();

        console.log('---------------------------call selector-------------------------------')
        await utils.checkandclick(page, "div.results-action > button.results-action-load-more");

        let linkselector = "div > div:nth-child(2) > div > div > div:nth-child(3) > div.results-list > a";
        let textselector = "div > div:nth-child(2) > div > div > div:nth-child(3) > div.results-list > a > div > h4";
        let studylevelselector = "div > div:nth-child(2) > div > div > div:nth-child(3) > div.results-list > a> div > ul > li:nth-child(1) > span"

        await page.waitFor(5000);
        const targetLinks = await page.$$(linkselector);
        const targetText = await page.$$(textselector);
        const studylevel = await page.$$(studylevelselector);
        await page.waitFor(5000);
        console.log("#total link selectors---->" + targetLinks.length);
        for (var j = 0; j < targetLinks.length; j++) {
          var elementstring = await page.evaluate(el => el.innerText, targetText[j]);
          const elementlink = await page.evaluate(el => el.href, targetLinks[j]);
          const study_level = await page.evaluate(el => el.innerText, studylevel[j])
          console.log("Study_level", study_level)
          totalCourseList.push({ href: elementlink, innerText: elementstring, category: categoryType, study_level: study_level });
        }
        const btnclick1 = "div.checkbox-select.checkbox-select--interests > button";
        let scrappedBtn1 = await page.$$(btnclick1);
        await scrappedBtn1[0].click();
        // let elementstring1 = await page.evaluate(el => el.innerText, targetLinksCardsUrls[i]);
        await page.waitFor(2000);
        targetLinksCardsUrls[i].click();
      }

      fs.writeFileSync("theuniversityofwesternaustralia_category_courselist.json", JSON.stringify(totalCourseList));
      // fs.writeFileSync("charlessturtuniversity_category_courselist.json", JSON.stringify(values));
      // fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinks))
      await fs.writeFileSync("./output/theuniversityofwesternaustralia_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
        }
      }
      await fs.writeFileSync("./output/theuniversityofwesternaustralia_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/theuniversityofwesternaustralia_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }


  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // refers to https://programsandcourses.anu.edu.au/catalogue
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({
        headless: true
      });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // course_level_form_selector key
      // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelList) {
      //   console.log(funcName + 'Invalid formSelList');
      //   throw (new Error('Invalid formSelList'));
      // }
      // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelElementDict) {
      //   console.log(funcName + 'Invalid formSelElementDict');
      //   throw (new Error('Invalid formSelElementDict'));
      // }

      // for each course-category, scrape all courses
      let totalCourseList = [];
      let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = {
  ScrapeCourseList
};