const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category, study_level) {
        const funcName = 'formatOutput ';
        const newJSONdata = [];
        var demoarray = [];
        let feesNumber = null;
        const feesDict = {
            international_student: {}
        };
        const feesList = [];
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id':
                            {
                                console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                                if (!configs.univ_id && configs.univ_id.length > 0) {
                                    console.log(funcName + 'configs.univ_id must have valid value');
                                    throw (new Error('configs.univ_id must have valid value'));
                                }
                                resJsonData.univ_id = configs.univ_id;
                                break;
                            }
                        case 'course_discipline': {
                            resJsonData.course_discipline = category;
                            console.log("fgsfghdfsjgf=====>", category)
                            // As of not we do not have way to get this field value
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            var myList = [];
                            let programType = null;
                            //progress bar start
                            const penglishList = [], pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            const ieltsDict = {}; const ibtDict = {}; const pteDict = {}; const utasDict = {}; let ieltsMapNumber = null; let ieltsNumber = null; const cpeDict = {};

                            // english requirement

                            programType = study_level
                            //const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            //console.log("cTitle :" + cTitle);
                            console.log("programType :" + programType);

                            if (programType) {
                                // const ieltsMappingProgressDict = await utils.getValueFromHardCodedJsonFile('ielts_progress');
                                const ieltsMappingDict = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                                // if (ieltsMappingDict) {
                                //     console.log("ptype:" + programType);
                                //     console.log("-----?ieltsMapping###:" + JSON.stringify(ieltsMappingDict));
                                //     englishList.push({ name: "ielts academic", description: ieltsMappingDict[programType].ielts });
                                //     englishList.push({ name: "toefl ibt", description: ieltsMappingDict[programType].ibt });
                                //     englishList.push({ name: "toefl pbt", description: ieltsMappingDict[programType].pbt });
                                //     englishList.push({ name: "pte academic", description: ieltsMappingDict[programType].pte });
                                //     englishList.push({ name: "cae", description: ieltsMappingDict[programType].cae });
                                //     myList = ieltsMappingDict[programType];
                                // }
                                // console.log("listttttt",myList.ielts);
                                if (ieltsMappingDict) {
                                    const potherLngDict = ieltsMappingDict[programType];
                                    console.log(funcName + 'ieltsMappingProgressDict = ' + JSON.stringify(potherLngDict));

                                    if (potherLngDict) {
                                        var ieltsScore = "",
                                            ibtScore = "",
                                            pbtScore = "",
                                            pteScore = "",
                                            caeScore = "";
                                        if (potherLngDict.ielts) {
                                            ieltsScore = await utils.giveMeNumber(potherLngDict.ielts);
                                            console.log("### IELTS data-->" + ieltsScore);
                                        }
                                        if (potherLngDict.ibt) {
                                            ibtScore = await utils.giveMeNumber(potherLngDict.ibt);
                                        }
                                        if (potherLngDict.pbt) {
                                            pbtScore = await utils.giveMeNumber(potherLngDict.pbt);
                                        }
                                        if (potherLngDict.pte) {
                                            pteScore = await utils.giveMeNumber(potherLngDict.pte);
                                        }
                                        if (potherLngDict.cae) {
                                            caeScore = await utils.giveMeNumber(potherLngDict.cae);
                                        }
                                        if (ieltsScore != "NA") {
                                            pieltsDict.name = 'ielts academic';
                                            pieltsDict.description = potherLngDict.ielts
                                            pieltsDict.min = 0;
                                            pieltsDict.require = ieltsScore;
                                            pieltsDict.max = 9;
                                            pieltsDict.R = 0;
                                            pieltsDict.W = 0;
                                            pieltsDict.S = 0;
                                            pieltsDict.L = 0;
                                            pieltsDict.O = 0;
                                            englishList.push(pieltsDict);
                                        }
                                        if (ibtScore != "NA") {
                                            pibtDict.name = 'toefl ibt';
                                            pibtDict.description = potherLngDict.ibt
                                            pibtDict.min = 0;
                                            pibtDict.require = ibtScore;
                                            pibtDict.max = 120;
                                            pibtDict.R = 0;
                                            pibtDict.W = 0;
                                            pibtDict.S = 0;
                                            pibtDict.L = 0;
                                            pibtDict.O = 0;
                                            englishList.push(pibtDict);
                                        }
                                        if (pbtScore != "NA") {
                                            ppbtDict.name = 'toefl pbt';
                                            ppbtDict.description = potherLngDict.pbt
                                            ppbtDict.min = 310;
                                            ppbtDict.require = pbtScore;
                                            ppbtDict.max = 677;
                                            ppbtDict.R = 0;
                                            ppbtDict.W = 0;
                                            ppbtDict.S = 0;
                                            ppbtDict.L = 0;
                                            ppbtDict.O = 0;
                                            englishList.push(ppbtDict);
                                        }
                                        if (pteScore != "NA") {
                                            ppteDict.name = 'pte academic';
                                            ppteDict.description = potherLngDict.pte
                                            ppteDict.min = 0;
                                            ppteDict.require = pteScore;
                                            ppteDict.max = 90;
                                            ppteDict.R = 0;
                                            ppteDict.W = 0;
                                            ppteDict.S = 0;
                                            ppteDict.L = 0;
                                            ppteDict.O = 0;
                                            englishList.push(ppteDict);
                                        }
                                        if (caeScore != "NA") {
                                            pcaeDict.name = 'cae';
                                            pcaeDict.description = potherLngDict.cae
                                            pcaeDict.min = 80;
                                            pcaeDict.require = caeScore;
                                            pcaeDict.max = 230;
                                            pcaeDict.R = 0;
                                            pcaeDict.W = 0;
                                            pcaeDict.S = 0;
                                            pcaeDict.L = 0;
                                            pcaeDict.O = 0;
                                            englishList.push(pcaeDict);
                                        }
                                        // }
                                    }

                                }
                            }

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            } else {
                                courseAdminReq.english = [];
                            }

                            // academic requirement
                            if (courseScrappedData.academic) {
                                let academicReq = [];
                                academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.academic);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + [academicReq]);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {

                                    courseAdminReq.academic = [academicReq];
                                    console.log('courseAdminReq = ', courseAdminReq.academic);
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }

                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.course_admission_english_more_details;
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            var entry_requirements_url = "";
                            let resEnglishReqMoreDetailsJson11 = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            // let academic = await Course.extractValueFromScrappedElement(courseScrappedData.academic);
                            // let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson11;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                //resJsonData.course_admission_requirement.academic = academic;
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            } else if (resEnglishReqMoreDetailsJson) {

                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson11;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                // resJsonData.course_admission_requirement.academic = academic;
                            }
                            break;
                        }
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));

                                            rescourse_url = selList;

                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;

                        }
                        // case 'course_duration_full_time': {
                        //     // display full time
                        //     var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                        //     var fullTimeText = "";
                        //     let arr = []
                        //     let courseKeyVal;
                        //     var course_title = resJsonData.course_title;
                        //     console.log("titleeee-=--->", course_title);
                        //     if (course_title = "Master Of International Development") {
                        //         console.log("gfggfgfgfffgfg89999---->");
                        //         const courseKeyVal = "1.5 years"
                        //         arr.push(courseKeyVal);
                        //         fullTimeText = arr[0]
                        //         console.log("INIF")


                        //     }
                        //     if (course_title = "Master Of Engineering In Oil And Gas") {
                        //         console.log("gfggfgfgfffgfg7675---->");
                        //         const courseKeyVal = "2 years"
                        //         arr.push(courseKeyVal);
                        //         console.log("arr-=-=-=--=>", arr);
                        //         fullTimeText = arr[1]
                        //         console.log("INIF")


                        //     }
                        //     if (course_title = "Graduate Certificate In Asian Studies") {
                        //         console.log("gfggfgfgfffgfg-0-000---->");
                        //         const courseKeyVal = "1 year"
                        //         arr.push(courseKeyVal);
                        //         console.log("arr-=-=-=--=>", arr);
                        //         fullTimeText = arr[2]
                        //         console.log("INIF")


                        //     }
                        //     else {
                        //         courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time);

                        //        // courseKeyVal = courseScrappedData.course_duration_full_time;
                        //     }
                        //     console.log("courseKeyVal_duration@@@@", courseKeyVal)

                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                     if (Array.isArray(selList) && selList.length > 0) {
                        //                         fullTimeText = selList[0];
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     if (fullTimeText.includes('1 to 2 years of full-time study or the equivalent in part-time study depending on previous engineering discipline completed')) {

                        //         fullTimeText = "1 to 2 years";
                        //         console.log("fullTimeText---->>>>", fullTimeText);
                        //     }
                        //     if (fullTimeText.includes('1 year part-')) {
                        //         let duration_val = fullTimeText.split('part-')[0];
                        //         fullTimeText = duration_val.trim();
                        //         console.log("fullTimeText---->>>>", fullTimeText);
                        //     }
                        //     console.log("FullTime---->", JSON.stringify(fullTimeText))
                        //     if (fullTimeText.includes('-')) {
                        //         let duration_val = fullTimeText.split('-')[1];
                        //         fullTimeText = duration_val.trim();
                        //         console.log("fullTimeText---->>>>", fullTimeText);
                        //     }

                        //     if (fullTimeText && fullTimeText.length > 0) {
                        //         const resFulltime = fullTimeText;
                        //         if (resFulltime) {
                        //             durationFullTime.duration_full_time = resFulltime;
                        //             courseDurationList.push(durationFullTime);
                        //             let tempvar = await format_functions.validate_course_duration_full_time(resFulltime);
                        //             courseDurationDisplayList.push(tempvar);
                        //             demoarray = tempvar[0];
                        //             console.log("demoarray--->", demoarray);
                        //             console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                        //             let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                        //             console.log("##filtered_duration_formated--->" + JSON.stringify(filtered_duration_formated));
                        //             if (courseDurationList && courseDurationList.length > 0) {
                        //                 resJsonData.course_duration = resFulltime;
                        //                 console.log("resJsonData.course_duration", resJsonData.course_duration)
                        //             }
                        //             if (filtered_duration_formated && filtered_duration_formated.length > 0) {

                        //                 resJsonData.course_duration_display = [demoarray];
                        //                 var isfulltime = false, isparttime = false;
                        //                 filtered_duration_formated.forEach(element => {
                        //                     if (element.display == "Full-Time") {
                        //                         isfulltime = true;
                        //                     }
                        //                     if (element.display == "Part-Time") {
                        //                         isparttime = true;
                        //                     }
                        //                 });
                        //                 resJsonData.isfulltime = isfulltime;
                        //                 resJsonData.isparttime = isparttime;
                        //             }
                        //             else {
                        //                 resJsonData.course_duration_display = [];
                        //             }
                        //         }
                        //     }
                        //     break;
                        // }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            let arr = []
                            var courseKeyVal;

                            var course_title = resJsonData.course_title;
                            console.log("titleeee-=--->", course_title);
                            if (course_title.includes("Master Of International Development")) {
                                console.log("gfggfgfgfffgfg89999---->");
                                const courseKeyVal = "1.5 years"
                                arr.push(courseKeyVal);
                                fullTimeText = arr[0]
                                console.log("INIF")


                            }
                            else if (course_title.includes("Master Of Engineering In Oil And Gas")) {
                                console.log("gfggfgfgfffgfg7675---->");
                                const courseKeyVal = "2 years"
                                arr.push(courseKeyVal);
                                console.log("arr-=-=-=--=>", arr);
                                fullTimeText = arr[0]
                                console.log("INIF")


                            }
                            else if (course_title.includes("Graduate Certificate In Asian Studies")) {
                                console.log("gfggfgfgfffgfg-0-000---->");
                                const courseKeyVal = "1 year"
                                arr.push(courseKeyVal);
                                console.log("arr-=-=-=--=>", arr);
                                fullTimeText = arr[0]
                                console.log("INIF")


                            }
                            else {
                                courseKeyVal = courseScrappedData.course_duration_full_time;
                                console.log("courseKeyVal-=-=-=--=>", JSON.stringify(courseKeyVal));

                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    fullTimeText = selList[0];
                                                    console.log("##fullTimeText--->" + JSON.stringify(fullTimeText));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // if (fullTimeText.includes('1 year part-')) {
                            //     let duration_val = fullTimeText.split('part-')[0];
                            //     fullTimeText = duration_val.trim();
                            //     console.log("fullTimeText---->>>>", fullTimeText);
                            // }
                            // console.log("FullTime---->", JSON.stringify(fullTimeText))
                            // if (fullTimeText.includes('-')) {
                            //     let duration_val = fullTimeText.split('-')[1];
                            //     fullTimeText = duration_val.trim();
                            //     console.log("fullTimeText---->>>>", fullTimeText);
                            // }

                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    // durationFullTime.duration_full_time = resFulltime;
                                    // courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    console.log("tempvar--->", tempvar);
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("##filtered_duration_formated--->" + JSON.stringify(filtered_duration_formated));
                                    //if (courseDurationList && courseDurationList.length > 0) {
                                    resJsonData.course_duration = resFulltime;
                                    // }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;

                        }
                        case 'course_tuition_fee':

                        case 'course_study_mode': { // Location Launceston
                            //resJsonData.course_study_mode = coursestudymodeText;
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'page_url': {
                            //resJsonData.page_url = courseUrl;
                            break;
                        }
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':

                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;



                            console.log("demoarray123 -->", demoarray.duration);
                            // feesDict.international_student = configs.propValueNotAvaialble;
                            // feesDict.fee_duration_years = configs.propValueNotAvaialble;
                            // feesDict.currency = configs.propValueNotAvaialble;
                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feeYear && feeYear.length > 0) {
                                courseTuitionFee.year = feeYear;
                            }
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                                const arrval = String(feesVal1).split('.');

                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);

                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            const regEx = /\d/g;
                                            var durations = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time)
                                            console.log("DUrations-->", JSON.stringify(durations))
                                            console.log("Data@@@", JSON.stringify(durations))
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,
                                                description: feesWithDollorTrimmed

                                            });

                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,
                                                description: ""

                                            });
                                        }
                                    }
                                }
                            } else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: Number(demoarray.duration),
                                    unit: demoarray.unit,
                                    description: ""
                                });
                            }
                            // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = Number(inetStudentFees);
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if

                            let international_student_all_fees_array = [];
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    //  feesDict.international_student_all_fees = international_student_all_fees_array
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }


                        case 'course_campus_location': { // Location Launceston
                            const campLocationText = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            let staticCampuses = await utils.getValueFromHardCodedJsonFile('staticCampuses');
                            const Cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code)
                            const durations = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time)
                            var campusedata = [];
                            let cat = category;
                            let mainCampuses = [];
                            let feesDict = {}
                            console.log(funcName + 'feesVal===========>>>>>>' + JSON.stringify(durations));
                            console.log("cat------>", Cricos.length)
                            if (Cricos.length == 7) {
                                console.log("ItsinIf----->", Cricos.length)

                                console.log(funcName + 'campLocationText = ' + campLocationText);
                                if (campLocationText && campLocationText.length > 0) {
                                    var campLocationValTrimmed = String(campLocationText).trim();
                                    console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);

                                    if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                        var locatioarray = await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t ]+/g, ';'), ';');
                                        console.log("locatioarray -->", locatioarray);
                                        for (let campus of staticCampuses) {
                                            for (let location1 of locatioarray) {
                                                if (location1.indexOf(campus) > -1) {
                                                    mainCampuses.push(campus);
                                                }
                                            }

                                        }

                                        mainCampuses.forEach(element => {
                                            campusedata.push({
                                                "name": element,
                                                "code": Cricos

                                            })
                                        });
                                        console.log("mainCampuses -->", campusedata);
                                        // let uniqueNames = (locatioarray) => locatioarray.filter((v, i) => locatioarray.indexOf(v) === i)
                                        // console.log("location---?" + JSON.stringify(uniqueNames));

                                        if (mainCampuses.length > 0) {
                                            resJsonData.course_campus_location = campusedata;
                                        } else {
                                            throw new Error("Campus not found");
                                        }


                                    }
                                }
                            }
                            else {
                                console.log("itiselse======>")



                                //   mainCampuses.forEach(element => {
                                campusedata.push({
                                    "name": "Nedlands"
                                    //"code": Cricos

                                })
                                // });
                                console.log("mainCamdgfdgpuses -->", campusedata);

                                let splitCricos, study_title, study_cri, loc, fin_loc
                                let arr = []
                                let fee = feesList
                                console.log("fesss---->", fee)
                                const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                                const crcos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                                // let loca = await utils.getValueFromHardCodedJsonFile('staticCampuses');
                                //console.log("loca----->", loca)
                                if (cTitle.includes(",")) {

                                    splitCricos = cTitle.split(',');
                                    console.log("splitCricosghdfgjdgf=======>", splitCricos)

                                }

                                let MasterofPhilosophy = [];
                                let f;

                                console.log("demoarray1234567889 -->", demoarray);
                                const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                                const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                                if (feeYear && feeYear.length > 0) {
                                    courseTuitionFee.year = feeYear;
                                }
                                // if (feesIntStudent == null) {
                                //     const feesss = await utils.getValueFromHardCodedJsonFile('fees_mapping');
                                //     console.log("feesIntStudentfeesIntStudent -->", feesss);
                                // }
                                // if we can extract value as Int successfully then replace Or keep as it is
                                if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                    const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                    console.log(funcName + 'feesWithDollorTrimmed===========>>>>>>' + feesWithDollorTrimmed);
                                    const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                    const arrval = String(feesVal1).split('.');
                                    const feesVal = String(arrval[0]);

                                    const regEx = /\d/g;
                                    //let duration= resJsonData.course_duration
                                    let fduration = durations.match(regEx);
                                    console.log(funcName + 'fduration===========>>>>>>' + fduration);

                                    if (feesVal) {
                                        const regEx = /\d/g;
                                        let feesValNum = feesVal.match(regEx);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum===========>>>>>>' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum===========>>>>>>' + feesValNum);
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }
                                            console.log(funcName + 'feesNumber===========>>>>>>' + feesNumber);
                                            if (Number(feesNumber)) {
                                                const regEx = /\d/g;
                                                feesDict.international_student = ({
                                                    amount: Number(feesNumber),
                                                    duration: String(fduration),
                                                    unit: "year",
                                                    description: feesIntStudent
                                                });
                                                console.log("feesDict.international_student", feesDict.international_student)

                                            } else {
                                                feesDict.international_student = ({
                                                    amount: 0,
                                                    duration: fduration,
                                                    unit: "year",
                                                    description: ""
                                                });
                                            }
                                        }
                                    }
                                } else {
                                    feesDict.international_student = ({
                                        amount: 0,
                                        duration: Number(demoarray.duration),
                                        unit: "year",
                                        description: ""
                                    });
                                }
                                console.log("MasterofPhilosophy=======>", feesDict.international_student)
                                console.log("feesDict=======>", feesDict)

                                for (let i = 0; i < splitCricos.length; i++) {
                                    let course_tuition_fee = {}
                                    let MasterofPhilosophy = []
                                    let finalfes = feesDict.international_student
                                    console.log("splitCricos=======>", splitCricos)
                                    study_title = await format_functions.getMyTitle(splitCricos[i].trim());
                                    study_cri = await format_functions.getMyCricos(splitCricos[i].trim());
                                    loc = await format_functions.getMyLocation(splitCricos[i].trim());
                                    if (loc.includes("WA - The University of Western Australia - Location owned and operated by provider")) {
                                        fin_loc = loc.replace("WA - The University of Western Australia - Location owned and operated by provider", "Western Australia")
                                        console.log("fin_loc---->", fin_loc)
                                    }
                                    let newArray = {
                                        name: "Nedlands", value: {
                                            international_student: {},
                                        },
                                        fee_duration_years: "1",
                                        currency: "AUD",
                                    }
                                    newArray.value.international_student = finalfes;
                                    MasterofPhilosophy.push(newArray);

                                    console.log("study_val=======>", study_title)
                                    console.log("study_cri=======>", study_cri)
                                    console.log("MasterofPhilosophy=======>", MasterofPhilosophy)
                                    newJSONdata.push(
                                        {
                                            course_title: study_title,
                                            course_discipline: category,
                                            course_campus_location: [{
                                                //campusedata.name
                                                "name": "Nedlands",
                                                "code": study_cri
                                            }],
                                            course_tuition_fee: {

                                                fees: MasterofPhilosophy
                                            }

                                            // course_tuition_fee: {
                                            //     "fee": [{
                                            //         "name": "Nedlands",
                                            //         "value": {
                                            //             "international_student": {
                                            //                 finalfes
                                            //             },
                                            //             "fee_duration_years": "1",
                                            //             "currency": "AUD"
                                            //         }
                                            //     }
                                            //     ]
                                            // }
                                        })
                                }
                                // MasterofPhilosophy.push({
                                //     name: "Western Australia", value: {
                                //         international_student: [
                                //             {
                                //                 amount: 92800,
                                //                 duration: "1",
                                //                 unit: "year",

                                //                 description: "$92,800.00"

                                //             }
                                //         ],
                                //         fee_duration_years: "1",
                                //         currency: "AUD",
                                //         // international_student_all_fees: []
                                //     }
                                // });

                                // console.log("newJSONdata------->", JSON.stringify(course_tuition_fee))
                                console.log("newJSONdataallllll------->", newJSONdata)
                                resJsonData.course_campus_location = campusedata;
                            }

                            break;
                        }

                        case 'course_intake_url': {
                            const courseKeyVal = courseScrappedData.course_intake_url;
                            var url = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                url = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (url.length > 0) {
                                resJsonData.course_intake_url = url;
                            }
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            var courseIntakeStr = [];//= await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            // existing intake value
                            const courseIntakeStr1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);


                            if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                                resJsonData.course_intake = courseIntakeStr1;
                                const intakeStrList = String(courseIntakeStr1).split('2020');
                                console.log(funcName + 'intakeStrList course_intake = ' + JSON.stringify(intakeStrList));
                                courseIntakeStr = intakeStrList[0];
                            }

                            console.log("intakes" + JSON.stringify(courseIntakeStr));
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var campus = resJsonData.course_campus_location;

                                // var campus = await utils.giveMeArray(cam.replace(/[\r\n\t]+/g, ';'), ';');
                                console.log('Campuse location :' + campus);
                                var intakes = [];
                                console.log("Intake length-->" + courseIntakeStr.length);
                                console.log("Campus length-->" + campus.length);
                                var intakevalue = [];
                                var myintake_data = JSON.parse(fs.readFileSync("./selectors/university_hard_coded_data.json"))[0];


                                if (courseIntakeStr.toLowerCase().trim().includes("any time")) {
                                    intakevalue.push("Any time Except December");
                                }
                                if (courseIntakeStr.toLowerCase().trim().includes("semester 1") || courseIntakeStr.toLowerCase().trim().includes("february")) {
                                    intakevalue.push(myintake_data["term1"]);
                                }
                                if (courseIntakeStr.toLowerCase().trim().includes("semester 2")) {
                                    // intakevalue.push("July 29");
                                    intakevalue.push(myintake_data["term2"]);
                                }
                                if (courseIntakeStr.toLowerCase().trim().includes("january")) {
                                    intakevalue.push(myintake_data["term3"]);
                                }
                                if (courseIntakeStr.toLowerCase().trim().includes("year")) {
                                    intakevalue.push(myintake_data["term1"]);
                                    intakevalue.push(myintake_data["term2"]);
                                }

                                for (var camcount = 0; camcount < campus.length; camcount++) {
                                    var intakedetail = {};
                                    intakedetail.name = campus[camcount].name;
                                    intakedetail.value = intakevalue;//await utils.giveMeArray(courseIntakeStr[count].replace(/[\r\n\t ]+/g, ' '), ";");
                                    intakes.push(intakedetail);
                                }
                                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                let formatedIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                console.log("Intakes --> ", JSON.stringify(formatedIntake));

                                var intakedata = {};
                                intakedata.intake = formatedIntake;
                                intakedata.more_details = more_details;
                                resJsonData.course_intake = intakedata;
                            }

                            break;
                        }
                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };

                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

                            let resScholarshipJson = null;


                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }




                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }

                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)

                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;


                            break;
                        }

                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                resJsonData.application_fee = ""
                                break;
                            }
                        // case 'course_outline':
                        //     {
                        //         const courseKeyVal = courseScrappedData.course_outline;
                        //         let course_outline = [];
                        //         if (Array.isArray(courseKeyVal)) {
                        //             for (const rootEleDict of courseKeyVal) {
                        //                 console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //                 const elementsList = rootEleDict.elements;
                        //                 for (const eleDict of elementsList) {
                        //                     console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                     const selectorsList = eleDict.selectors;
                        //                     for (const selList of selectorsList) {
                        //                         console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                         // if (Array.isArray(selList) && selList.length > 0) {
                        //                         course_outline = selList;
                        //                         //  }
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //         resJsonData.course_outline = course_outline;
                        //         break;
                        //     }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = "";
                            var getProgramCode = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                /*selList.forEach(element => {
                                                  getProgramCode.push(element);
                                                });*/
                                                program_code = selList;
                                                console.log("program code is:" + program_code);
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            if (program_code.length > 0) {
                                resJsonData.program_code = String(program_code);
                            } else {
                                resJsonData.program_code = "";
                            }
                            break;
                        }

                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            course_career_outcome = selList;
                                            //  }
                                        }
                                    }
                                }
                            }
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            } else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }

                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome':
                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            if (title.includes(' (')) {
                                const title_text = title.replace('(', ' (');
                                console.log("splitStr@@@2" + title_text);
                                title = title_text;
                                console.log("splitStr@@@1" + title);

                            }
                            var ctitle = format_functions.titleCase(title).trim();
                            // var ctitle1 = ctitle.replace('(','').replace(')','').trim();
                            var ctitle2 = ctitle.replace(' (', '(');
                            var ctitle3 = ctitle2.replace(' (', '(').trim();
                            console.log("ctitle@@@", ctitle3.trim());
                            resJsonData.course_title = ctitle3
                            break;



                        }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            course_country = selList[0];
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_study_level':
                            {
                                //const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
                                let cStudyLevel = study_level;
                                console.log(funcName + 'course_title course_study_level = ' + cStudyLevel);

                                if (cStudyLevel) {
                                    resJsonData.course_study_level = cStudyLevel;
                                } else {
                                    throw new Error("study_level undefined");
                                }

                                break;
                            }
                        default:
                            {
                                console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                                break;
                            } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            if (newJSONdata.length > 0) {
                console.log("newJSONdata.length----->", newJSONdata.length)
                console.log("newJSONdatafgfh----->", newJSONdata)

                for (let newData of newJSONdata) {
                    var NEWJSONSTRUCT = {}, structDict = [];
                    //resJsonData.course_cricos_code = [];
                    // for (let cricosLocation of locations) {
                    //     resJsonData.course_cricos_code.push(cricosLocation + " - " + newData.course_cricos_code);
                    // }
                    for (let location of locations) {
                        NEWJSONSTRUCT.course_id = newData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                        NEWJSONSTRUCT.course_title = newData.course_title;
                        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                        NEWJSONSTRUCT.course_url = resJsonData.course_url;
                        //  console.log("newData.course_tuition_fee --> ", newData.course_tuition_fee);
                        NEWJSONSTRUCT.course_campus_location = location.name;
                        resJsonData.course_tuition_fee = newData.course_tuition_fee;
                        NEWJSONSTRUCT.course_tuition_fee = newData.course_tuition_fee.fees[0].international_student;
                        NEWJSONSTRUCT.currency = newData.course_tuition_fee.fees[0].currency;
                        NEWJSONSTRUCT.fee_duration_years = newData.course_tuition_fee.fees[0].fee_duration_years;
                        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                        NEWJSONSTRUCT.course_campus_location = newData.course_campus_location;
                        resJsonData.course_discipline = newData.course_discipline;
                        var intakes = resJsonData.course_intake.intake;
                        var matchrec = [];
                        for (let dintake of intakes) {
                            if (location == dintake.name) {
                                matchrec = dintake.value;
                            }
                        }
                        if (matchrec.length > 0) {
                            NEWJSONSTRUCT.course_intake = matchrec[0];
                        }
                        else {
                            NEWJSONSTRUCT.course_intake = "";
                        }
                        for (let myfees of newData.course_tuition_fee.fees) {
                            if (myfees.name == location) {
                                NEWJSONSTRUCT.international_student_all_fees = myfees;
                            }
                        }
                        structDict.push(NEWJSONSTRUCT);
                        NEWJSONSTRUCT = {};
                    }
                    for (let location_wise_data of structDict) {
                        resJsonData.course_id = location_wise_data.course_location_id;

                        resJsonData.international_student = location_wise_data.international_student;
                        resJsonData.course_campus_location = location_wise_data.course_campus_location;
                        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                        console.log("Write file--->" + filelocation)
                        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                    }
                }

            }
            else {
                var NEWJSONSTRUCT = {}, structDict = [];
                console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
                var locations = resJsonData.course_campus_location;
                for (let location of locations) {
                    NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                    NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                    NEWJSONSTRUCT.course_title = resJsonData.course_title;
                    NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                    NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                    NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                    NEWJSONSTRUCT.course_url = resJsonData.course_url;
                    //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                    NEWJSONSTRUCT.course_campus_location = location.name;
                    NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                    NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                    NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                    NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                    NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                    var intakes = resJsonData.course_intake.intake;
                    var matchrec = [];
                    for (let dintake of intakes) {
                        if (location == dintake.name) {
                            matchrec = dintake.value;
                        }
                    }
                    if (matchrec.length > 0) {
                        NEWJSONSTRUCT.course_intake = matchrec[0];
                    }
                    else {
                        NEWJSONSTRUCT.course_intake = [];
                    }
                    for (let myfees of resJsonData.course_tuition_fee.fees) {
                        if (myfees.name == location) {
                            NEWJSONSTRUCT.international_student_all_fees = myfees;
                        }
                    }
                    structDict.push(NEWJSONSTRUCT);
                    NEWJSONSTRUCT = {};
                }
                for (let location_wise_data of structDict) {
                    console.log("location::::" + location_wise_data.course_campus_location);
                    resJsonData.course_id = location_wise_data.course_location_id;

                    //   var filelocation = "./output/" + resJsonData.univ_id + "_" + resJsonData.basecourseid.replace(/[\n :""]+/g, '').replace(/[\/]+/g, '_') + "_details.json";
                    var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";

                    console.log("Write file--->" + filelocation)
                    fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


                }
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            //const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData,courseDict.href,courseDict.category,courseDict.study_level);
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };







