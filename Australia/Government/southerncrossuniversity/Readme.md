**Southern Cross University: Total Courses Details As On 25 Mar 2019**
* Total Courses = 92
* Courses without CRICOS code = 0
* Total available courses = 92
* Repeted = 0
**IELTS**
* Mapping is done from course Mapping file and ielts is scrraped from course details
* based on the ieltsscore it is mapped from the file
* seprate condition is given for the ielts score = 7 as there are 2 mappings of 7 in mapping file
**CourseIntake**
* fileIntake.split(' - '); condition added
* 2019 is not appended
**course_tuition_fee_amount**
* This will not have this field as there are multiple fees
**ToaddAdditionalCourses in course_list.json**
