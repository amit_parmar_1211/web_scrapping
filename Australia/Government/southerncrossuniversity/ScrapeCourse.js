const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
// const mapping = require('./output/Mapping/main');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, study_level) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            console.log("courseScrappedData.program_code = ", courseScrappedData.program_code);
                            var program_code = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList[0].split(": ");
                                                program_code = program_code[1];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            let program_code_array = [];
                            if (program_code == '' || program_code == undefined) {
                                for (let program_val of program_code) {

                                    resJsonData.program_code = program_val;

                                }
                            } else {
                                program_code_array.push(program_code);
                                for (let program_val of program_code_array) {

                                    resJsonData.program_code = program_val;

                                }
                            }


                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = course_category;
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            var ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_require);
                            var ibt_req = await Course.extractValueFromScrappedElement(courseScrappedData.ibt_require);
                            var pbt_req = await Course.extractValueFromScrappedElement(courseScrappedData.pbt_require);
                            var pte_req = await Course.extractValueFromScrappedElement(courseScrappedData.pte_require);
                            var cae_req = await Course.extractValueFromScrappedElement(courseScrappedData.cae_require);

                            var myscrore = '';
                            let ieltsNumber = null; let pbtNumber = null; let pteNumber = null; let caeNumber = null; let ibtNumber = null
                            var ieltsfinaloutput = null;
                            var tofelfinaloutput = null;
                            var ptefinaloutput = null;
                            const pieltsDict1 = {};
                            const penglishList = [];
                            var ieltsScore;
                            var caefinaloutput = null;
                            // english requirement
                            if (courseScrappedData.course_toefl_ielts_score) {
                                console.log('Course english language requirment :' + JSON.stringify(courseScrappedData.course_toefl_ielts_score));
                                ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                                ieltsfinaloutput = ieltsScore.toString();

                                if (ieltsScore && ieltsScore.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(ieltsScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ieltsNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                    }
                                }

                            }

                            if (ieltsNumber == 8) {
                                console.log("finalData1 -->", ieltsNumber);

                                if (ieltsScore) {
                                    pieltsDict1.name = 'ielts academic';
                                    pieltsDict1.description = ieltsNumber + "";
                                    pieltsDict1.min = 0;
                                    pieltsDict1.require = ieltsScore;
                                    pieltsDict1.max = 9;
                                    pieltsDict1.R = 0;
                                    pieltsDict1.W = 0;
                                    pieltsDict1.S = 0;
                                    pieltsDict1.L = 0;
                                    pieltsDict1.O = 0;
                                    penglishList.push(pieltsDict1);
                                }

                            }
                            else {


                                let flagForIelts7 = false;
                                let finalData = courseScrappedData.course_toefl_ielts_score[0].elements[0].selectors[0];
                                let ieltsString = '';
                                console.log("finalData -->", finalData.length);
                                if (finalData.length == 5) {
                                    ieltsString = "Overall " + finalData[0] + ", Reading " + finalData[1] + ", Writing " + finalData[2] + ", Listening " + finalData[3] + ", Speaking " + finalData[4];
                                } else {
                                    ieltsString = ieltsNumber;
                                }

                                if (ieltsNumber == 7) {
                                    for (let score of finalData) {
                                        console.log("score -->", score);
                                        if (score < 7) {
                                            flagForIelts7 = true;
                                        }
                                    }
                                }
                                let keyStringForIelts = '';
                                if (ieltsNumber == 7 && flagForIelts7 == true) {
                                    keyStringForIelts = "7.0 with no band less than 6.5"
                                } else if (ieltsNumber == 7 && flagForIelts7 == false) {
                                    keyStringForIelts = "7.0 with no band less than 7.0"
                                } else {
                                    keyStringForIelts = ieltsNumber;
                                }
                                console.log("keyStringForIelts -->", keyStringForIelts);
                                myscrore = await utils.getMappingScore(keyStringForIelts);
                                console.log("myscrore -->", myscrore);
                                // if ieltsNumber is valid then map IBT and PBT accordingly
                                var ibtDict = {}, catDict = {}, pteDict = {}, ieltsDict = {}, pbtDict = {}, oetDict = {}, jcuceapDict = {};
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ieltsString;
                                englishList.push(ieltsDict);
                                ibtDict.name = 'toefl ibt';
                                ibtDict.description = myscrore.ibt;
                                englishList.push(ibtDict);
                                pbtDict.name = 'toefl pbt';
                                pbtDict.description = myscrore.pbt;
                                englishList.push(pbtDict);
                                pteDict.name = 'pte academic';
                                pteDict.description = myscrore.pte;
                                englishList.push(pteDict);


                                if (ieltsNumber && ieltsNumber > 0) {
                                    //tofel score
                                    console.log("myscrore.pbt -->", myscrore.pbt);
                                    let tempTOFEL = [
                                        {
                                            "elements":
                                                [
                                                    {
                                                        "selectors":
                                                            [
                                                                [myscrore.pbt]
                                                            ]
                                                    }
                                                ]
                                        }
                                    ]
                                    const tofelScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempTOFEL);
                                    if (tofelScore && tofelScore.length > 0) {
                                        // tofelfinaloutput = tofelScore.toString();
                                        // extract exact number from string
                                        const regEx = /[+-]?\d+(\.\d+)?/g;
                                        const matchedStrList = String(tofelScore).match(regEx);
                                        console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                        if (matchedStrList && matchedStrList.length > 0) {
                                            pbtNumber = Number(matchedStrList[0]);
                                            console.log(funcName + 'pbtNumber = ' + pbtNumber);
                                        }
                                    }
                                    //toefl ibt
                                    let tempIBT = [
                                        {
                                            "elements":
                                                [
                                                    {
                                                        "selectors":
                                                            [
                                                                [myscrore.ibt]
                                                            ]
                                                    }
                                                ]
                                        }
                                    ]
                                    const tofelScoreibt = await Course.extractValueEnglishlanguareFromScrappedElement(tempIBT);
                                    if (tofelScoreibt && tofelScoreibt.length > 0) {
                                        // tofelfinaloutput = tofelScore.toString();
                                        // extract exact number from string
                                        const regEx = /[+-]?\d+(\.\d+)?/g;
                                        const matchedStrList = String(tofelScoreibt).match(regEx);
                                        console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                        if (matchedStrList && matchedStrList.length > 0) {
                                            ibtNumber = Number(matchedStrList[0]);
                                            console.log(funcName + 'ibtNumber = ' + ibtNumber);
                                        }
                                    }
                                    //pte score
                                    console.log("myscrore.PTE -->", myscrore.pte);
                                    let tempPTE = [
                                        {
                                            "elements":
                                                [
                                                    {
                                                        "selectors":
                                                            [
                                                                [myscrore.pte]
                                                            ]
                                                    }
                                                ]
                                        }
                                    ]
                                    const pteAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempPTE);
                                    if (pteAScore && pteAScore.length > 0) {
                                        // ptefinaloutput = pteAScore.toString();
                                        // extract exact number from string
                                        const regEx = /[+-]?\d+(\.\d+)?/g;
                                        const matchedStrList = String(pteAScore).match(regEx);
                                        console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                        if (matchedStrList && matchedStrList.length > 0) {
                                            pteNumber = Number(matchedStrList[0]);
                                            console.log(funcName + 'pteNumber = ' + pteNumber);
                                        }
                                    }

                                }

                                ///progress bar start
                                const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};


                                let course_title = resJsonData.course_title;

                                if (course_title == "Bachelor of Speech Pathology") {

                                    if (ibtNumber != []) {
                                        var IELTSJSON = {
                                            "name": 'ielts academic',
                                            "description": ieltsDict.description + "",
                                            "min": 0,
                                            "require": ieltsNumber,
                                            "max": 9,
                                            "R": 0,
                                            "W": 0,
                                            "S": 0,
                                            "L": 0,
                                            "O": 0,
                                        }
                                        penglishList.push(IELTSJSON);

                                    }

                                }
                                else {
                                    if (ibtNumber != []) {
                                        var IELTSJSON = {
                                            "name": 'ielts academic',
                                            "description": ieltsDict.description + "",
                                            "min": 0,
                                            "require": ieltsNumber,
                                            "max": 9,
                                            "R": 0,
                                            "W": 0,
                                            "S": 0,
                                            "L": 0,
                                            "O": 0,
                                        }
                                        penglishList.push(IELTSJSON);
                                        // if(ibtNumber){
                                        //     
                                        // }


                                    }

                                    if (ibtNumber != []) {
                                        var IBTJSON = {
                                            "name": 'toefl ibt',
                                            "description": ibtDict.description + "",
                                            "min": 0,
                                            "require": ibtNumber,
                                            "max": 120,
                                            "R": 0,
                                            "W": 0,
                                            "S": 0,
                                            "L": 0,
                                            "O": 0,
                                        }
                                        penglishList.push(IBTJSON);

                                    }

                                    if (pbtNumber != []) {
                                        var PBTJSON = {
                                            "name": 'toefl pbt',
                                            "description": pbtDict.description + "",
                                            "min": 310,
                                            "require": pbtNumber,
                                            "max": 677,
                                            "R": 0,
                                            "W": 0,
                                            "S": 0,
                                            "L": 0,
                                            "O": 0,
                                        }
                                        penglishList.push(PBTJSON);

                                    }

                                    if (pteNumber != []) {
                                        var PTEJSON = {
                                            "name": 'pte academic',
                                            "description": pteDict.description + "",
                                            "min": 0,
                                            "require": pteNumber,
                                            "max": 90,
                                            "R": 0,
                                            "W": 0,
                                            "S": 0,
                                            "L": 0,
                                            "O": 0,
                                        }
                                        penglishList.push(PTEJSON);

                                    }
                                }
                            }
                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }
                            ///progrssbar End




                            // if (englishList && englishList.length > 0) {
                            //     courseAdminReq.english = englishList;
                            // }

                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq];
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }

                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }

                            let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            console.log("entry_requirements_url -->", entry_requirements_url);
                            // let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            if (entry_requirements_url && resJsonData.course_admission_requirement) {
                                console.log("entry_requirements_url -->", entry_requirements_url);
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                resJsonData.course_admission_requirement.academic_requirements_url = "";
                                // resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            } else if (entry_requirements_url) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                resJsonData.course_admission_requirement.academic_requirements_url = "";
                                // resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            }
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            const courseDurationList = [];
                            const courseDurationDisplayList = [];
                            const durationFullTime = {};
                            // let anotherArray = [];
                            let duration1 = courseScrappedData.course_duration_full_time;

                            console.log("*************start formating Full Time years*************************");
                            console.log("courseScrappedData.course_duration_full_time = ", JSON.stringify(courseScrappedData.course_duration_full_time));
                            let finalDuration = duration1[0].elements[0].selectors[0][0];
                            const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration);
                            console.log("full_year_formated_data", full_year_formated_data);
                            ///course duration
                            const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                            console.log("not_full_year_formated_data", not_full_year_formated_data);
                            try {
                                durationFullTime.duration_full_time = not_full_year_formated_data;
                                courseDurationList.push(durationFullTime);
                                ///END course duration
                                console.log("courseDurationList : ", courseDurationList);
                                ///course duration display

                                courseDurationDisplayList.push(full_year_formated_data);
                                // anotherArray.push(courseDurationDisplayList);
                                console.log("courseDurationDisplayList", courseDurationDisplayList);
                                ///END course duration display
                            } catch (err) {
                                console.log("Problem in Full Time years:", err);
                            }
                            console.log("courseDurationDisplayList1", courseDurationDisplayList);
                            let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                            console.log("filtered_duration_formated", filtered_duration_formated);
                            console.log('***************END formating Full Time years**************************')

                            if (courseDurationList && courseDurationList.length > 0) {
                                resJsonData.course_duration = not_full_year_formated_data ;
                            }
                            if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                resJsonData.course_duration_display = filtered_duration_formated;
                                var isfulltime = false, isparttime = false;
                                filtered_duration_formated.forEach(element => {
                                    if (element.display == "Full-Time") {
                                        isfulltime = true;
                                    }
                                    if (element.display == "Part-Time") {
                                        isparttime = true;
                                    }
                                });
                                resJsonData.isfulltime = isfulltime;
                                resJsonData.isparttime = isparttime;

                            }
                            break;
                        }
                        case 'course_tuition_fee':

                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesDict = {};
                            const feesList = [];

                            // feesDict.international_student = configs.propValueNotAvaialble;
                            // feesDict.fee_duration_years = configs.propValueNotAvaialble;
                            // feesDict.currency = configs.propValueNotAvaialble;
                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            // if (feeYear && feeYear.length > 0) {
                            //     courseTuitionFee.year = feeYear;
                            // }
                            
                                console.log("feesIntStudent[i]",feesIntStudent)
                            
                            let multipleFeesSplit = feesIntStudent.split(')');
                            console.log("multipleFeesSplit -->", multipleFeesSplit);
                            let additionalInfoFees = [];
                            let internationalFeesArray = [];
                            for (let feesDetail of multipleFeesSplit) {
                                if (feesDetail && feesDetail.length > 0) { // extract only digits
                                    const feesWithDollorTrimmed = String(feesDetail).trim();
                                    console.log("feesDetail -->", feesDetail);

                                    let finalFee = feesDetail.split('(');
                                    console.log(funcName + 'feesWithDollorTrimmed = ' + finalFee);

                                    const feesVal1 = String(finalFee[0]).replace('$', '');

                                    additionalInfoFees.push(finalFee[1]);
                                    // const arrval = String(finalFee).split('.');

                                    const feesVal = String(feesVal1);

                                    console.log(funcName + 'feesVal = ' + feesVal);
                                    if (feesVal) {
                                        const regEx = /\d/g;
                                        let feesValNum = feesVal.match(regEx);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            let feesNumber = null;
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }
                                            console.log(funcName + 'feesNumber = ' + feesNumber);
                                            if (Number(feesNumber)) {
                                                internationalFeesArray.push(Number(feesNumber));
                                            }
                                        }
                                    }
                                } // if (feesIntStudent && feesIntStudent.length > 0)


                            }
                            console.log("feesDict -->", internationalFeesArray);
                            // if we can extract value as Int successfully then replace Or keep as it is


                            for (let i = 0; i < internationalFeesArray.length; i++) {
                                let tempDict = { international_student: [] };
                                console.log("internationalFeesArray[i] -->", internationalFeesArray[i]);
                                tempDict.international_student={
                                    amount: internationalFeesArray[i],
                                    duration: 1,
                                    unit: "Year",                                  
                                    description: internationalFeesArray[i] + " " + additionalInfoFees[i],
                                   
                                };

                                if (tempDict.international_student) { // if we have fees value then add following supporting attributes
                                    const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                    const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);                                 
                                  
                                    if (feesDuration && feesDuration.length > 0) {
                                        tempDict.fee_duration_years = feesDuration;
                                    }
                                    if (feesCurrency && feesCurrency.length > 0) {
                                        tempDict.currency = feesCurrency;
                                    }
                                    if (tempDict) {
                                        // feesList.push(feesDict);
                                        var campus = resJsonData.course_campus_location;

                                        // console.log("feesDict -->", feesDict);
                                        feesList.push({ name: campus[i].name, value: tempDict });


                                    }
                                    console.log("feesList -->", feesList);

                                }
                            }
                            //added
                            if (feesList && feesList.length > 0) {
                                courseTuitionFee.fees = feesList;
                            }
                            console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                            if (courseTuitionFee) {
                                resJsonData.course_tuition_fee = courseTuitionFee;
                            }

                            // // take tuition fee value at json top level so will help in DDB for indexing
                            if (courseTuitionFee) {
                                resJsonData.course_tuition_fee = courseTuitionFee;
                                // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                            }
                            break;
                        }
                        case 'course_campus_location': { // Location Launceston
                          
                            const courseKeyVals = courseScrappedData.course_cricos_code;
                            let course_cricos_code=null
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            console.log("campus_cricose==>",course_cricos_code)
                            var campLocationText; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            let newcampus = [];

                            for (let campus of campLocationText) {
                                if (!newcampus.includes(campus)) {
                                    newcampus.push(campus);
                                    console.log("##Campuscampus-->" + campus)
                                }
                            }
                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            //   let valfainal =campLocationText.toString();

                            if (newcampus && newcampus.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                // var ctitle= format_functions.titleCase1(newcampus)
                                // console.log("newcampus@@@",ctitle)
                                // resJsonData.course_title=ctitle


                                var campusedata = [];
                                var flocation=[]
                                let campuses=["Gold Coast","Lismore","Melbourne","Perth","Sydney","Coffs Harbour"]
                                for(let i=0;i<campuses.length;i++){
                                    newcampus.forEach(element => {
                                        if(element.includes(campuses[i])){
                                            flocation.push(campuses[i])

                                        }
                                    })
                                }

                                for(let i=0;i<flocation.length;i++){
                                    campusedata.push({name:flocation[i],code:course_cricos_code[i]})
                                }
                                // flocation.forEach(element => {
                                //     campusedata.push({
                                //         "name": element,
                                //         "code":String(course_cricos_code)
                                //     })
                                // });
                                console.log("gfdkfgbdjhbfgdghfbh@@@", campusedata)
                                resJsonData.course_campus_location = campusedata;
                                resJsonData.course_study_mode = "On campus"
                                console.log("## FInal string-->" + campLocationValTrimmed);
                                // }
                            } // if (campLocationText && campLocationText.length > 0)

                            break;
                        }                   
                        


                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            // Semester 1 (February) and Semester 2 (August)
                            // prepare emty JSON struct
                            var courseIntakeStr = [];//= await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;
                            let anotherArray = [];
                            let formatedIntake
                            var intakedetail = {};


                            const courseIntakeDList = await utils.getValueFromHardCodedJsonFile('course_intake');

                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList =112 ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {

                                            for (let intake of selList) {
                                                let splitIntake = intake.split(', ');
                                                let anotherArray1 = [];
                                                for (let scrapeIntake of splitIntake) {

                                                    for (let fileIntake of courseIntakeDList) {
                                                        let filrSplitIntake = fileIntake.split(' - ');

                                                        if (filrSplitIntake[0] == scrapeIntake) {
                                                            anotherArray1.push(filrSplitIntake[1]);
                                                        }
                                                    }
                                                }
                                                anotherArray.push(anotherArray1);
                                            }
                                            for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                                                //   courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": anotherArray });
                                                console.log("selList[i] -->", anotherArray);
                                                courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": anotherArray[j] });
                                                console.log("courseIntakeStr -->", courseIntakeStr);

                                            }
                                        }

                                    }
                                }
                            }

                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            let formatIntake = await format_functions.providemyintake(courseIntakeStr, "MOM", "");
                            // if (formatIntake && formatIntake.length > 0) {
                            var intakedata = {};

                            if (formatIntake && formatIntake.length > 0) {
                                var intakedata = {};
                                intakedata.intake = formatIntake;
                                console.log("FormatIntake", JSON.stringify(formatIntake))
                               
                                // }
                                // intakedata.intake = formatIntake;
                                intakedata.more_details = more_details;
                                resJsonData.course_intake = intakedata;
                                console.log("resJsonData.course_intake -->> ", resJsonData.course_intake);
                            }

                          
                            break;
                        }
                       
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_country = resAccomodationCostJson;
                            }
                            break;

                        }
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                console.log("Inside If Condition");
                                concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = concatnatedRootElementsStrArray;
                            } else {
                                resJsonData[key] = [];
                            }
                            break;
                        }
                        
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                            const courseKeyVal_major =courseScrappedData.course_outline;
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                            let course_outlines = {};
                            console.log("MAjor==>", JSON.stringify(courseKeyVal_major))
                            if (courseKeyVal_minor != null) {
                                course_outlines.minors = courseKeyVal_minor
                            } else {
                                course_outlines.minors = []
                            }
                            if (courseKeyVal_major) {
                                let course_career_outcome = [];
                                if (Array.isArray(courseKeyVal_major)) {
                                    for (const rootEleDict of courseKeyVal_major) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    console.log("course_career_outcome_selList==>",JSON.stringify(selList))
                                                    selList.forEach(element=>{
                                                        var value=element.replace(/[\/\n\t]+/g, '')
                                                        course_career_outcome.push(value.trim())
                                                    })
                                                    //course_career_outcome = selList;
                                                }
                                            }
                                        }
                                    }
                                }
                                if(course_career_outcome!=null){
                                    
                                    
                                    course_outlines.majors = course_career_outcome
                                }else{
                                    course_outlines.majors = []
                                }
                              
                            }
                            if (courseKeyVal != null) {
                                course_outlines.more_details = courseKeyVal
                            } else {
                                course_outlines.more_details = ""
                            }
                            resJsonData.course_outline = course_outlines
                            break;
                        }
                        case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            if (courseKeyVal != null) {
                                resJsonData.application_fee = courseKeyVal
                            } else {
                                resJsonData.application_fee = ""
                            }
                            break;
                        }
                        case 'course_title': {
                            const title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            var ctitle = format_functions.titleCase(title)
                            console.log("ctitle@@@", ctitle)
                            resJsonData.course_title = ctitle
                            break;
                        }
                        
                        case 'course_study_level': {
                            // console.log(funcName + 'course_title course_study_level = ' + study_level);
                            resJsonData.course_study_level = study_level.trim();
                            break;


                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            console.log("Course_id", JSON.stringify(resJsonData.course_title))
            for (let location of locations) {

                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();

                //    NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id + "_" + location.replace(/\s/g, '').toLowerCase();
                console.log("Course_id", JSON.stringify(NEWJSONSTRUCT.course_location_id))
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location_wise_data------->", location_wise_data);
                resJsonData.course_id = location_wise_data.course_id;
                let basecourseid = location_wise_data.course_id;               
                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }
    //resJsonData.course_cricos_code = location_wise_data.cricos_code;
    // resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
    // resJsonData.current_course_campus_location = location_wise_data.course_campus_location;
    //resJsonData.current_course_intake = location_wise_data.course_intake;
    //  resJsonData.course_intake_display = location_wise_data.course_intake;



    //             var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
    //             console.log("Write file--->" + filelocation)
    //             fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
    //             try {
    //                 if (appConfigs.ISINSERT) {
    //                     request.post({
    //                         "headers": { "content-type": "application/json" },
    //                         "url": appConfigs.API_URL + "api/australia/course/v2/savecourse",
    //                         "body": JSON.stringify(resJsonData)
    //                     }, (error, response, body) => {
    //                         if (error) {
    //                             console.log(error);
    //                         }
    //                         var myresponse = JSON.parse(body);
    //                         if (!myresponse.flag) {
    //                             var errorlog = [];
    //                             if (fs.existsSync(configs.opFailedCourseListByAPI)) {
    //                                 errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
    //                                 errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
    //                             }
    //                             fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
    //                         }
    //                         console.log("success save data! " + JSON.stringify(body));
    //                     });
    //                 }
    //                 else if (appConfigs.ISREPLACE) {
    //                     request.post({
    //                         "headers": { "content-type": "application/json" },
    //                         "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
    //                         "body": JSON.stringify(resJsonData)
    //                     }, (error, response, body) => {
    //                         if (error) {
    //                             console.log(error);
    //                         }
    //                         var myresponse = JSON.parse(body);
    //                         console.log("MyMessage-->" + JSON.stringify(myresponse))
    //                         if (!myresponse.flag) {
    //                             var errorlog = [];
    //                             if (fs.existsSync(configs.opFailedCourseListByAPI)) {
    //                                 errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
    //                                 errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
    //                             }
    //                             fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
    //                         }
    //                         console.log("success save data! " + JSON.stringify(body));
    //                     });
    //                 }
    //                 else if (appConfigs.ISUPDATE) {
    //                     request.post({
    //                         "headers": { "content-type": "application/json" },
    //                         "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
    //                         "body": JSON.stringify(resJsonData)
    //                     }, (error, response, body) => {
    //                         if (error) {
    //                             console.log(error);
    //                         }
    //                         var myresponse = JSON.parse(body);
    //                         if (!myresponse.flag) {
    //                             var errorlog = [];
    //                             if (fs.existsSync(configs.opFailedCourseListByAPI)) {
    //                                 errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
    //                                 errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
    //                             }
    //                             fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
    //                         }
    //                         console.log("success save data! " + JSON.stringify(body));
    //                     });
    //                 }
    //             }
    //             catch (err) {
    //                 console.log("#### try-catch error--->" + err);
    //             }
    //             // const res = await awsUtil.putFileInBucket(filelocation);
    //             // console.log(funcName + 'S3 object location = ' + res);
    //         }
    //         // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
    //         return resJsonData;
    //     } catch (error) {
    //         console.log(funcName + 'try-catch error = ' + error);
    //         throw (error);
    //     }
    // }
    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };