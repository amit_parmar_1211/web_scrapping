const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const appConfigs = require('./common/app-config');
class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      var totalCourseList = [];
      var mainCategory = [];
      const maincategoryselectorurl = "//*//div[@class='btn-link col-sm-4']//a[not(contains(@href,'pathways'))]"
      const maincategoryselectortext = "//*//div[@class='btn-link col-sm-4']//a//h2[not(contains(text(),'Pathway Programs'))]"
      var categorylinkurl = await page.$x(maincategoryselectorurl);
      var categorytext = await page.$x(maincategoryselectortext);
      for (let ii = 0; ii < categorylinkurl.length; ii++) {
        var categorylink = await page.evaluate(el => el.href, categorylinkurl[ii]);
        var categorystring = await page.evaluate(el => el.innerText, categorytext[ii])
        console.log("categorystring@@@@", categorylink)
        mainCategory.push({ href: categorylink, innertext: categorystring });
      }
      console.log("mainCategory@@@@", mainCategory)
      fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(mainCategory))
      //undergraduate courses
      for (let data of mainCategory) {
        await page.goto(data.href, { timeout: 0 })
        const studylevelselector = "//*[@id='main']/div/div/div/div/div/ul/li/a[contains(text(),'Undergraduate')]"
        const selector = "//*[@class='tab-pane active in']/ul/li/a";
        var level = await page.$x(studylevelselector)
        var study_levellink = await page.evaluate(el => el.href, level[0])
        var study_levelstring = await page.evaluate(el => el.innerText, level[0])
        await level[0].click();
        var category = await page.$x(selector);
        for (let link of category) {
          var categorystring = await page.evaluate(el => el.innerText, link);
          var categoryurl = await page.evaluate(el => el.href, link);
          console.log("Href@@@@", categorystring)
          totalCourseList.push({ href: categoryurl, innerText: categorystring, category: data.innertext, study_level: study_levelstring });
        }
      }
      //postgraduate courses
      for (let data of mainCategory) {
        await page.goto(data.href, { timeout: 0 })
        const studylevelselector = "//*[@id='main']/div/div/div/div/div/ul/li/a[contains(text(),'Postgraduate')]"
        const selector = "//*[@class='tab-pane active in']/ul/li/a";
        var level = await page.$x(studylevelselector)
        var study_levellink = await page.evaluate(el => el.href, level[0])
        var study_levelstring = await page.evaluate(el => el.innerText, level[0])
        await level[0].click();
        var category = await page.$x(selector);
        for (let link of category) {
          var categorystring = await page.evaluate(el => el.innerText, link);
          var categoryurl = await page.evaluate(el => el.href, link);
          totalCourseList.push({ href: categoryurl, innerText: categorystring, category: data.innertext, study_level: study_levelstring });
        }
      }
      await fs.writeFileSync("./output/southerncrossuniversity_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
        }
      }
      await fs.writeFileSync("./output/southerncrossuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/southerncrossuniversity_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  // scrape course page list of the univ
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));
      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        count += 1;
      }
      console.log(funcName + 'writing courseList to file....');
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class
module.exports = { ScrapeCourseList };