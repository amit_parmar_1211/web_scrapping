const puppeteer = require('puppeteer');
const fs = require("fs");
async function start() {
    const URL = "http://handbook.murdoch.edu.au/courses/?year=2020";
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    var subjectAreasArray = [];
    var subjectAreasArray11 = [];
    let finallist = [];
    let url_title = "//*//div[@class='no-more-tables']/table/tbody/tr/td/a"
    //let progtam_code = "//*//div[@class='no-more-tables']/table/tbody/tr/td[1]"
    let url_title111 = await page.$x(url_title);
  
    for (let link of url_title111) {
        var categorystringmainurl = await page.evaluate(el => el.href, link);
        subjectAreasArray.push(categorystringmainurl.trim());
    }
    for (let sub_page of subjectAreasArray) {
        await page.goto(sub_page, { timeout: 0 });
        let programcode = "//*//div[@class='outline']//b[contains(text(),'Course Code')]/following::td[1]";
        let ieltsSelector = "//*//div[@class='outline']//b[contains(text(),'Admission Requirements: Offshore course offerings')]/following::td[1]/text()[contains(.,'IELTS')]";
        let elements = await page.$x(ieltsSelector); // if no match value found then returns empty []
        let programvalu = await page.$x(programcode);
        let programtext = "";
        if (programvalu && programvalu.length > 0) {
            programtext = await page.evaluate(el => el.innerText, programvalu[0]);
        }
        console.log('length -->????? ', programtext);
        if (elements.length > 0) {
            for (let i = 0; i < elements.length; i++) {
                let list = [];
                let text = await page.evaluate(h1 => h1.textContent, elements[i]);
                if (text) {
                    let splitNewLine = text;
                    splitNewLine = text.split('\n');
                    for (let data of splitNewLine) {

                        if (data.toLowerCase().indexOf('ielts') > -1) {
                            //  list.push({ programcode: programtext, IELTS: data });
                            list.push(data);
                            //console.log("IeltsTmp  ", finallist);
                        }
                    }
                    finallist.push({ programcode: programtext, ielts: list });
                }
            }
        }
        else {
           // finallist.push({ programcode: programtext, ielts: ['IELTS not found'] });
            // console.log("IeltsTmp 111111--> ", finallist);
        }
        //   console.log("IeltsTmp --> ", finallist);
    }
    await fs.writeFileSync("./output/murdochuniversity_IELTS.json", JSON.stringify(finallist));
    //console.log("name-->", JSON.stringify(subjectAreasArray));
    await browser.close();
}
start();
