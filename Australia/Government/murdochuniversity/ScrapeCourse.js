const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const utils = require('./common/utils');
const format_functions = require('./common/format_functions');
request = require('request');
class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, category, studyLevel) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};




            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                //program_code = selList;
                                                program_code.push(selList[0]);
                                            }
                                        }
                                    }
                                }
                            }
                            // if (program_code.length > 0) {
                            resJsonData.program_code = String(program_code);
                            //}
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            let englishList = [];
                            let ieltsDict = {}
                            var course_admission_requirement = {};
                            var course_title=resJsonData.course_title
                            var study_level=resJsonData.course_study_level
                            var staticIelts = await utils.getValueFromHardCodedJsonFile('ieltsmapping');
                            const title11 = resJsonData.course_title;
                            const title = title11.toLowerCase();
                            console.log("title -->", title);
                            const intakefees = JSON.parse(fs.readFileSync(appConfigs.Ielts_scrap));
                            console.log("intakefees-------->>>>", intakefees);
                            let profram_text = resJsonData.program_code
                            let tmpvar = intakefees.filter(val => {
                                return val.programcode.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase() == profram_text.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase()
                            });
                            let ielts_txt;
                            console.log("tmpvar------->>>>", tmpvar);
                            if (tmpvar.length > 0) {
                                console.log("programcode------->>>>", tmpvar[0].ielts);
                                ielts_txt = tmpvar[0].ielts;
                            }

                            if (courseScrappedData.course_toefl_ielts_score) {
                                const courseKeyValiel = courseScrappedData.course_toefl_ielts_score;
                                let resEnglishReqMoreDetailsielts = null;
                                if (Array.isArray(courseKeyValiel)) {
                                    for (const rootEleDict of courseKeyValiel) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            resEnglishReqMoreDetailsielts = selectorsList[0];
                                        }
                                    }
                                }
                                console.log("ieltsScore :" + JSON.stringify(resEnglishReqMoreDetailsielts));
                                var ieltsScore = resEnglishReqMoreDetailsielts.study_units[0];
                                console.log("##ieltsScore-->" + ieltsScore);

                                if (ieltsScore && ieltsScore.length > 0) {
                                    ieltsDict.name = 'ielts academic',
                                        ieltsDict.description = ieltsScore;
                                    ieltsDict.min = 0;
                                    ieltsDict.require = await utils.giveMeNumber(ieltsScore);
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);
                                }
                                else if (ieltsScore == undefined) {
                                    if (ielts_txt) {
                                        let firstvalue = ielts_txt
                                        let string_ielts = String(firstvalue[0]);
                                        console.log("##string_ielts-->" + string_ielts);
                                        ieltsDict.name = 'ielts academic',
                                            ieltsDict.description = string_ielts;
                                        ieltsDict.min = 0;
                                        ieltsDict.require = await utils.giveMeNumber(string_ielts);
                                        ieltsDict.max = 9;
                                        ieltsDict.R = 0;
                                        ieltsDict.W = 0;
                                        ieltsDict.S = 0;
                                        ieltsDict.L = 0;
                                        ieltsDict.O = 0;
                                        englishList.push(ieltsDict);
                                    }
                                }
                            }
                             let myscrore = await utils.getMappingScore(course_title,study_level.trim());
                             console.log("myscore==>",JSON.stringify(study_level.trim()))
                             if(myscrore.ielts){
                                ieltsDict.name = 'ielts academic',
                                ieltsDict.description = myscrore.ielts;
                            ieltsDict.min = 0;
                            ieltsDict.require = await utils.giveMeNumber(myscrore.ielts);
                            ieltsDict.max = 9;
                            ieltsDict.R = 0;
                            ieltsDict.W = 0;
                            ieltsDict.S = 0;
                            ieltsDict.L = 0;
                            ieltsDict.O = 0;
                            englishList.push(ieltsDict);
                             }
                             if(myscrore.pte){
                               let pteDict={}
                                 console.log("myscrore.pte",myscrore.pte)
                                 pteDict.name = 'pte academic',
                                 pteDict.description = myscrore.pte;
                                 pteDict.min = 0;
                                 pteDict.require = await utils.giveMeNumber(myscrore.pte);
                                 pteDict.max = 90;
                                 pteDict.R = 0;
                                 pteDict.W = 0;
                                 pteDict.S = 0;
                                 pteDict.L = 0;
                                 pteDict.O = 0;
                                        englishList.push(pteDict);

                             }
                             if(myscrore.ibt){
                                 let ibtDict={}
                                 console.log("myscrore.pte",myscrore.ibt)
                                 ibtDict.name = 'toefl ibt',
                                 ibtDict.description = myscrore.ibt;
                                 ibtDict.min = 0;
                                 ibtDict.require = await utils.giveMeNumber(myscrore.ibt);
                                 ibtDict.max = 120;
                                 ibtDict.R = 0;
                                 ibtDict.W = 0;
                                 ibtDict.S = 0;
                                 ibtDict.L = 0;
                                 ibtDict.O = 0;
                                englishList.push(ibtDict);

                             }
                             if(myscrore.cae){
                                 let caeDict={}
                                 console.log("myscrore.pte",myscrore.cae)
                                 caeDict.name = 'cae',
                                 caeDict.description = myscrore.cae;
                                 caeDict.min = 80;
                                 caeDict.require = await utils.giveMeNumber(myscrore.cae);
                                 caeDict.max = 230;
                                 caeDict.R = 0;
                                 caeDict.W = 0;
                                 caeDict.S = 0;
                                 caeDict.L = 0;
                                 caeDict.O = 0;
                                englishList.push(caeDict);

                             }
                            
                            if (englishList) {
                                course_admission_requirement.english = englishList;
                                console.log("##course_admission_requirement.english-->" + course_admission_requirement.english);
                            }
                            else {
                                course_admission_requirement.english = [];
                                // throw new Error("No english found.");
                            }
                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    course_admission_requirement.academic = ["To check the academic requirement in the degree program, please see this link: "+resJsonData.course_url];
                                }
                            }
                            var filedata = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                            course_admission_requirement.academic_requirements_url = resJsonData.course_url;
                            course_admission_requirement.english_requirements_url = filedata["english_more"];
                            course_admission_requirement.entry_requirements_url = "";
                            resJsonData.course_admission_requirement = course_admission_requirement;
                            break;
                        }
                        case 'course_url': {
                            const courseKeyVal = courseScrappedData.course_url;
                            var course_url = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            //program_code = selList;
                                            course_url = selList;
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_url = course_url;
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                            if(fullTimeText=="12 months in accelerated mode or 18 months (with study over three semesters) or part-time equivalent."||
                                            fullTimeText=="1 year in accelerated mode or 18 months (with study over three semesters) or part-time equivalent."){
                                                fullTimeText="18 months  full-time or part-time equivalent."
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("filtered_duration_formated", filtered_duration_formated);
                                    console.log("courseDurationDisplayList2", courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {

                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_tuition_fees_international_student': {
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};
                            let fduration
                            let units
                            let dur=await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time)
                            let result = await format_functions.getMyFees(global.cricos_code)
                            console.log("Fees_result", JSON.stringify(dur))
                            if(dur=="12 months in accelerated mode or 18 months (with study over three semesters) or part-time equivalent."||
                            dur=="1 year in accelerated mode or 18 months (with study over three semesters) or part-time equivalent."){
                                dur="18 months  full-time or part-time equivalent."
                            }
                            if(String(dur).includes("months")){
                                let num=await utils.giveMeNumber(dur);
                                fduration=num
                                console.log("@@@num",fduration)
                                units="Months"
                            }else{
                                units="Years"
                                let num=await utils.giveMeNumber(dur);
                                fduration=num
                            }
                           console.log("Units===>",units)
                           // const result = myfees.filter(fees => fees.CRICOS_CODE == global.cricos_code);
                            const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                            const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                            let feesNumber
                            if (result.length > 0) {
                                console.log("Fee result-->" + JSON.stringify(result));
                                feesDict.fee_duration_years = feesDuration;
                                feesDict.currency = feesCurrency;
                                console.log("result[0].fy2020",result[0].fy2020)
                                if (result.includes(',')) {
                                    feesNumber = parseInt(result.replace(/,/g, ''), 10);
                                } else {
                                    feesNumber = result/fduration;
                                }
                                var feesint = {};
                                feesint.amount = Number(feesNumber);
                                feesint.duration = fduration;
                                feesint.unit = units;                               
                                feesint.description = result
                                
                                feesDict.international_student = feesint;

                                
                                
                            }
                            else {
                                feesDict.fee_duration_years = feesDuration;
                                feesDict.currency = feesCurrency;
                                var feesint = {};
                                feesint.amount = 0;
                                feesint.duration = "1";
                                feesint.unit = "year";                               
                                feesint.description = "";
                               
                                feesDict.international_student = feesint;
                               // feesDict.international_student_all_fees = [];
                            }
                            var campus = resJsonData.course_campus_location;
                            for (let loc of campus) {
                                feesList.push({ name: loc.name, value: feesDict });
                            }
                            //const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            // courseTuitionFee.year = feeYear;
                            courseTuitionFee.fees = feesList;
                            resJsonData.course_tuition_fee = courseTuitionFee;
                            break;
                        }
                        case 'course_campus_location': { // Location Launceston
                            const courseKeyVal = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            global.cricos_code=String(course_cricos_code)
                            const campLocationText = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            console.log(funcName + 'campLocationText = ' + campLocationText);
                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = String(campLocationText).trim();
                                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    var location = [];
                                    var camp = [
                                        "Perth",
                                        "Mandurah",
                                        "Rockingham"                                     
                                        
                                    ];
                                    camp.forEach(element => {
                                        if (campLocationValTrimmed.replace(/[\r\n\t]+/g, ' ').toLowerCase().trim().includes(element.toLowerCase())) {
                                            location.push(element);
                                        }
                                    });
                                    var campusedata = [];
                                    location.forEach(element => {
                                        campusedata.push({
                                            "name": format_functions.titleCase(element),                                              
                                            "code":String(course_cricos_code)
                                        })
                                    });
                                    resJsonData.course_campus_location = campusedata;
                                    resJsonData.course_study_mode = 'On campus';
                                    console.log("## FInal string-->" + campLocationValTrimmed);
                                }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }

                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = [];
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code.push(selList);
                                            }
                                        }
                                    }
                                }
                            }
                            if (program_code.length > 0) {
                                resJsonData.program_code = String(program_code);
                            }
                            break;
                        }
                        case 'course_intake': {
                            const courseIntakeDisplay = {};
                            // existing intake value
                            const courseIntakeStr1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseIntakeStr = await utils.giveMeArray(courseIntakeStr1, ',');
                            console.log(funcName + 'courseIntakeStr = ' + JSON.stringify(courseIntakeStr));
                            if (courseIntakeStr && courseIntakeStr.length > 0) {

                                var campus_location = resJsonData.course_campus_location;
                                console.log('Campuse location :' + campus_location);
                                var intakes = [];
                                console.log("Intake length-->" + courseIntakeStr.length);
                                //console.log("Campus length-->" + campus.length);
                                for (var camcount = 0; camcount < campus_location.length; camcount++) {
                                    var intakedetail = {};
                                    //intakedetail.campus = campus[count];
                                    intakedetail.name = campus_location[camcount].name;
                                    var intakesubdetailarr = [];
                                    for (var count = 0; count < courseIntakeStr.length; count++) {
                                        var intakesubdetail = {};
                                        //console.log("intake -->" + count)
                                        intakesubdetailarr.push(courseIntakeStr[count]);
                                    }
                                    intakedetail.value = intakesubdetailarr;
                                    intakes.push(intakedetail);
                                }
                                console.log(funcName + 'intakeStrList = ' + JSON.stringify(intakes));
                                //courseIntakeDisplay.intake = intakeList;
                                courseIntakeDisplay.intake = await utils.providemyintake(intakes, "MOM", "");;
                                courseIntakeDisplay.more_details = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake_url);
                                console.log(funcName + 'courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));
                            } // if (courseIntakeStr && courseIntakeStr.length > 0)                            
                            if (courseIntakeDisplay) {
                                resJsonData.course_intake = courseIntakeDisplay;
                            }
                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                             const courseKeyVal_major = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline);
                             const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                             let course_outlines = {};
                             if (courseKeyVal_minor != null) {
                                 course_outlines.minors = courseKeyVal_minor
                             } else {
                                 course_outlines.minors = []
                             }
                             if (courseKeyVal_major != null) {
                                 course_outlines.majors = courseKeyVal_major
                             } else {
                                 course_outlines.majors = []
                             }
                             if (courseKeyVal != null) {
                                 course_outlines.more_details = courseKeyVal
                             } else {
                                 course_outlines.more_details = resJsonData.course_url
                             }
                             resJsonData.course_outline = course_outlines
                             break;
                         }
                         case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            console.log("Application==>",courseKeyVal)
                            if (courseKeyVal == null || courseKeyVal == "null" ) {
                                resJsonData.application_fee = ""
                            } else {
                                resJsonData.application_fee = courseKeyVal
                            }
                            break;
                        }

                                                case 'course_study_level': {

                            resJsonData.course_study_level = studyLevel;
                            break;
                        }
                        case 'course_country': {
                            resJsonData.course_country = await Course.extractValueFromScrappedElement(courseScrappedData.course_country);
                            break;
                        }
                        case 'course_overview': {
                            resJsonData.course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_career_outcome) {
                                resJsonData.course_career_outcome = course_career_outcome
                            }

                            else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }


                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            title = title.replace('(', '( ').replace('\n', '');
                            var ctitle = format_functions.titleCase(title)
                            var ctitle2 = ctitle.replace(' ( ', '(').trim();
                            console.log("ctitle@@@", ctitle2)
                            resJsonData.course_title = ctitle2
                            resJsonData.course_discipline = category;
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        }
                    }
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                }
            }

            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;

                

                
                var filelocation = "./output/" + resJsonData.univ_id + "_" +basecourseid.replace(/[\n :""]+/g, '').replace(/[\/]+/g, '_') + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.category, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };