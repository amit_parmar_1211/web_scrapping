const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const configs = require('./configs');
const puppeteer = require('puppeteer');
class ScrapeCourseList extends Scrape {

  async scrapeCourseListAndPutAtS3(selFilepath) {
    const funcName = 'startScrappingFunc ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var datalist = [];

      


      await s.setupNewBrowserPage("https://search.murdoch.edu.au/s/search.html?collection=mu-course-search&f.Student+type%7CstudentType=International");
      var mainCategory = [], redirecturl = [];
      //scrape main category
      const maincategoryselector = "//*[@id='search-facets']/div[5]/div[2]/ul/li/span[1]/a";
     // const study_level = "//*[@class='container course-attributes']//div/div[1]"
      var category = await s.page.$x(maincategoryselector);
      
      console.log("Total categories-->" + category.length);

      for (let link of category) {
        var categorystringmain = await s.page.evaluate(el => el.innerText, link);
        var categorystringmainurl = await s.page.evaluate(el => el.href, link);
        mainCategory.push(categorystringmain.trim());
        redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
      }

      // let elements11 = await page.$x(programcode);
        // let text_program = await page.evaluate(h1 => h1.textContent, elements11);
        // console.log("IeltsTmp --> ", text_program);
      console.log("RedirectUrl",redirecturl)
      for (let link of redirecturl) {
        console.log("start--> " + link.innerText);
        await s.page.goto(link.href, { timeout: 0 })
        var ispage = true;
       // var level=await s.page.$x( study_level)
        const title = "//*[@id='main']/div/div/div[3]/section/div/div[1]/div[2]/ol/li/div/div[1]/h4/a";
        const studylevel="//*[@id='main']/div/div/div[3]/section/div/div[1]/div[2]/ol/li/div/div/div/div/div/div/div[1]/text()"
        const nextpage = "//*[@id='main']/div/div/div[3]/section/div/div[1]/div[2]/div[3]/ul/li/a[span[contains(text(),'Next')]]";
        while (ispage) {
          var titlevalue = await s.page.$x(title);
          var levelvalue=await s.page.$x(studylevel);
          for (let i = 0; i < titlevalue.length; i++) {
            var categorystring = await s.page.evaluate(el => el.innerText, titlevalue[i]);
            var categoryurl = await s.page.evaluate(el => el.href, titlevalue[i]);
            var levels=await s.page.evaluate(el=>el.textContent,levelvalue[i])
            //console.log("Study_level",levels)
            datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: link.innerText,study_level:levels });
          }


          var next = await s.page.$x(nextpage);
          if (next[0]) {
            await next[0].click();
            await s.page.waitFor(5000);
            console.log("Clicked next " + datalist.length);
          }
          else {
            console.log("No more pages" + datalist.length);
            ispage = false;
          }
        }

      }
     // console.log("Datalist",datalist)

      var uniquecases = [], uniquedata = [], finalDict = [];
      datalist.forEach(element => {
        if (!uniquecases.includes(element.href)) {
          uniquecases.push(element.href);
          uniquedata.push(element);
          //uniquecases.push(element.study_level)
        }
      });
      uniquecases.forEach(element => {
        var data = datalist.filter(e => e.href == element);
        var category = [];
        data.forEach(element => {
          if (!category.includes(element.category))
            category.push(element.category);
        });

        data[0].category = (Array.isArray(category)) ? category : [category];
        finalDict.push(data[0]);
      });

      console.log("unique data-->" + uniquecases.length);
      fs.writeFileSync("./output/courselist_uniq.json", JSON.stringify(uniquedata));
      fs.writeFileSync("./output/courselist_original.json", JSON.stringify(datalist));
      console.log(funcName + 'writing courseList to file....');
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(finalDict));
      //fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))




    



      await s.browser.close();
      await this.start();
      console.log(funcName + 'browser closed successfully.....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  async  start() {
    const URL = "http://handbook.murdoch.edu.au/courses/?year=2020";
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    var subjectAreasArray = [];
    var subjectAreasArray11 = [];
    let finallist = [];
    let url_title = "//*//div[@class='no-more-tables']/table/tbody/tr/td/a"
    //let progtam_code = "//*//div[@class='no-more-tables']/table/tbody/tr/td[1]"
    let url_title111 = await page.$x(url_title);
  
    for (let link of url_title111) {
        var categorystringmainurl = await page.evaluate(el => el.href, link);
        subjectAreasArray.push(categorystringmainurl.trim());
    }
    for (let sub_page of subjectAreasArray) {
        await page.goto(sub_page, { timeout: 0 });
        let programcode = "//*//div[@class='outline']//b[contains(text(),'Course Code')]/following::td[1]";
        let ieltsSelector = "//*//div[@class='outline']//b[contains(text(),'Admission Requirements: Offshore course offerings')]/following::td[1]/text()[contains(.,'IELTS')]";
        let elements = await page.$x(ieltsSelector); // if no match value found then returns empty []
        let programvalu = await page.$x(programcode);
        let programtext = "";
        if (programvalu && programvalu.length > 0) {
            programtext = await page.evaluate(el => el.innerText, programvalu[0]);
        }
        console.log('length -->????? ', programtext);
        if (elements.length > 0) {
            for (let i = 0; i < elements.length; i++) {
                let list = [];
                let text = await page.evaluate(h1 => h1.textContent, elements[i]);
                if (text) {
                    let splitNewLine = text;
                    splitNewLine = text.split('\n');
                    for (let data of splitNewLine) {

                        if (data.toLowerCase().indexOf('ielts') > -1) {
                            //  list.push({ programcode: programtext, IELTS: data });
                            list.push(data);
                            //console.log("IeltsTmp  ", finallist);
                        }
                    }
                    finallist.push({ programcode: programtext, ielts: list });
                }
            }
        }
        else {
           // finallist.push({ programcode: programtext, ielts: ['IELTS not found'] });
            // console.log("IeltsTmp 111111--> ", finallist);
        }
        //   console.log("IeltsTmp --> ", finallist);
    }
    await fs.writeFileSync("./output/murdochuniversity_IELTS.json", JSON.stringify(finallist));
    //console.log("name-->", JSON.stringify(subjectAreasArray));
    await browser.close();
}
}

module.exports = { ScrapeCourseList };
