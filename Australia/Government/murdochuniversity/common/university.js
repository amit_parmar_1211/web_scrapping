// const fs = require('fs');
const Scrape = require('./scrape').Scrape;
// eslint-disable-next-line import/no-unresolved
// const configs = require('./../configs');

// const MAX_PAGES = 1;

class University extends Scrape {
  /*
  // scrape course page list of the univ
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      const pageUrl = this.selectorJson.url;
      console.log(funcName + 'url = ' + pageUrl);
      const jsonDictElements = this.selectorJson.elements;
      console.log(funcName + 'elements = ' + JSON.stringify(jsonDictElements));
      if (Array.isArray(jsonDictElements)) {
        for (const eleDict of jsonDictElements) {
          const eleType = eleDict.elementType;
          console.log(funcName + 'elementType = ' + eleType);
          // ensure type is matching
          if (eleType === Scrape.ELEMENT_TYPE.COURSE_LIST) {
            const courseItemSelector = eleDict.course_item_selector;
            console.log(funcName + 'course_item_selector = ' + courseItemSelector);
            const pageSelector = eleDict.page_selector;
            console.log(funcName + 'page_selector = ' + pageSelector);
            // const nextPageSelector = eleDict.next_page_selector;
            // console.log(funcName + 'next_page_selector = ' + nextPageSelector);
            // const prevPageSelector = eleDict.previous_page_selector;
            // console.log(funcName + 'previous_page_selector = ' + prevPageSelector);
            // start scrapping course list - name and url
            const courseList = await University.scrapeCourseListForPages(courseItemSelector, pageSelector, pageUrl);
            return courseList;
          } // if
        } // for elements
      } // if
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  */

  /*
  // scrape course list of the univ
  static async scrapeCourseListForPages(courseItemSelector, firstPageSelector, pageURL) {
    const funcName = 'scrapeCourseListForPages ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([courseItemSelector, firstPageSelector, pageURL]);
      // create Scrape instance
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(pageURL);
      let nextElementSiblingHandle = null; let courseList = []; const activePaginationItemSel = firstPageSelector;
      let pageCount = 1;
      
      do {
        await s.page.waitFor(5000);
        // wait for sel
        console.log(funcName + 'waitForSelector sel = ' + activePaginationItemSel);
        await s.page.waitForSelector(activePaginationItemSel, { visible: true });
        // scrape all course list items
        console.log(funcName + '\n\r ********** Scraping for page  ' + pageCount + ' ********** \n\r');
        const resList = await s.scrapeAnchorElement(courseItemSelector);
        courseList = courseList.concat(resList);
        // console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
        const eleList = await s.page.$$(activePaginationItemSel);
        if (Array.isArray(eleList) && eleList.length > 0) {
          const activePageButton = eleList[0];
          // reassign next sibling handle
          nextElementSiblingHandle = await activePageButton.getProperty('nextElementSibling');
          console.log(funcName + 'nextElementSiblingHandle = ' + nextElementSiblingHandle);
          if (String(nextElementSiblingHandle).includes('null')) {
            console.log(funcName + 'nextElementSiblingHandle includes null....so breaking the loop...');
            break;
          }
          if (nextElementSiblingHandle) {
            await nextElementSiblingHandle.click();
            console.log(funcName + 'Clicked nextElementSiblingHandle..');
          } else {
            throw (new Error('nextElementSiblingHandle invalid'));
          }
        }
        pageCount += 1;
      } while (nextElementSiblingHandle);

      console.log(funcName + 'total pages scrapped = ' + pageCount);
      console.log(funcName + 'courseList count = ' + courseList.length);
      console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
      if (s) {
        await s.close();
      }
      return courseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  */

  /*
  static async extractValueFromScrappedElement(rootElementDictList) {
    const funcName = 'extractValueFromScrappedElement ';
    try {
      console.log(funcName + 'rootElementDictList =' + JSON.stringify(rootElementDictList));
      console.log(funcName + 'rootElementDictList = ' + rootElementDictList);
      let extractedVal = null;
      let concatnatedRootElementsStr = null;
      if (Array.isArray(rootElementDictList)) {
        for (const rootEleDict of rootElementDictList) {
          console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
          const elementsList = rootEleDict.elements;
          let concatnatedElementsStr = null;
          for (const eleDict of elementsList) {
            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
            const selectorsList = eleDict.selectors;
            let concatnatedSelectorsStr = null;
            for (const selList of selectorsList) {
              console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
              console.log(funcName + '\n\r selList = ' + selList);
              let concatnatedSelStr = null;
              for (const selItem of selList) {
                if (!concatnatedSelStr) {
                  concatnatedSelStr = selItem;
                } else {
                  concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                }
              } // selList
              console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
              if (concatnatedSelStr) {
                if (!concatnatedSelectorsStr) {
                  concatnatedSelectorsStr = concatnatedSelStr;
                } else {
                  concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                }
              }
            } // selectorsList
            console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
            // concat elements
            if (concatnatedSelectorsStr) {
              if (!concatnatedElementsStr) {
                concatnatedElementsStr = concatnatedSelectorsStr;
              } else {
                concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
              }
            }
          } // elementsList
          console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
          if (concatnatedElementsStr) {
            if (!concatnatedRootElementsStr) {
              concatnatedRootElementsStr = concatnatedElementsStr;
            } else {
              concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
            }
          }
        } // rootElementDictList
      } else {
        concatnatedRootElementsStr = configs.placeNAForEachEmptyValue;
      }
      extractedVal = String(concatnatedRootElementsStr).trim();
      console.log(funcName + 'extractedVal = ' + extractedVal);
      return extractedVal;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
*/

  /*
  // format output json
  async formatUnivOutput(univScrappedData) {
    const funcName = 'formatUnivOutput ';
    try {
      Scrape.validateParams([univScrappedData]);
      const resJsonData = {};
      for (const key in univScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (univScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_name': {
              console.log(funcName + 'configs.univ_name = ' + configs.univ_name);
              if (!configs.univ_name && configs.univ_name.length > 0) {
                console.log(funcName + 'configs.univ_name must have valid value');
                throw (new Error('configs.univ_name must have valid value'));
              }
              resJsonData.univ_name = configs.univ_name;
              break;
            }
            case 'univ_url': {
              console.log(funcName + 'this.selectorJson.page_url = ' + this.selectorJson.page_url);
              let univUrl = '';
              if (this.selectorJson && this.selectorJson.page_url) {
                univUrl = this.selectorJson.page_url;
              }
              resJsonData.univ_url = univUrl;
              break;
            }
            case 'univ_videos': {
              const courseKeyVal = univScrappedData.univ_videos;
              let resScholarshipJson = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList[0];
                      }  
                    }
                  }
                }
                resJsonData.univ_videos = resScholarshipJson;
              }
              break;
            }
            case 'univ_images': {
              const courseKeyVal = univScrappedData.univ_images;
              let resScholarshipJson = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList[0];
                      }  
                    }
                  }
                }
                resJsonData.univ_images = resScholarshipJson;
              }
              break;
            }
            case 'univ_scholarship': {
              const scholarshipDict = {};
              scholarshipDict.scholarship = configs.placeNAForEachEmptyValue;
              scholarshipDict.more_details = configs.placeNAForEachEmptyValue;

              const scholarshipText = await University.extractValueFromScrappedElement(univScrappedData.univ_scholarship);
              if (scholarshipText && scholarshipText.length > 0) {
                scholarshipDict.scholarship = scholarshipText;
              }
              if (this.selectorJson && this.selectorJson.univ_scholarship && this.selectorJson.univ_scholarship.length > 0) {
                const rootEleDict = this.selectorJson.univ_scholarship[0];
                if (rootEleDict && rootEleDict.url) {
                  scholarshipDict.more_details = rootEleDict.url;
                }
              }
              resJsonData.univ_scholarship = scholarshipDict;
              break;
            }
            case 'univ_accomodation': {
              const courseKeyVal = univScrappedData.univ_accomodation;
              let resAccomodationCostJson = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }  
                    }
                  }
                }
                resJsonData.univ_accomodation = resAccomodationCostJson;
              }
              break;
            }
            case 'univ_rankings': {
              const courseKeyVal = univScrappedData.univ_rankings;
              let resAccomodationCostJson = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }  
                    }
                  }
                }
                resJsonData.univ_rankings = resAccomodationCostJson;
              }
              break;
            }
            case 'univ_country':
            case 'univ_logo':
            case 'univ_about': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = univScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
              resJsonData[key] = String(concatnatedRootElementsStr).trim();
              // console.log(funcName + 'resJsonData[' + key + ']= ' + JSON.stringify(resJsonData[key]));
              break;
            }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default...');
              break;
            }
          } // switch
        }
      } // for
      console.log(funcName + 'univ resJsonData =' + JSON.stringify(resJsonData));
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  */

  /*
  static async placeNAForEachEmptyValue(univScrappedData) {
    const funcName = 'placeNAForEachEmptyValue ';
    try {
      Scrape.validateParams([univScrappedData]);
      const resJsonData = univScrappedData;
      for (const key in univScrappedData) {
        console.log(funcName + 'key = ' + key);
        if (univScrappedData.hasOwnProperty(key)) {
          const keyVal = univScrappedData[key];
          console.log(funcName + 'keyVal = ' + keyVal);
          if (typeof keyVal === 'string') {
            if (keyVal && keyVal.length <= 0) {
              console.log(funcName + key + ' has empty string value so replacing with NA...');
              resJsonData[key] = configs.propValueNotAvaialble;
            }
          }
        }
      } // for
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  */

  /*
  // scrape everything as per selector file
  async scrapeUniversityDetails(selectorFilepath) {
    const funcName = 'scrapeUniversityDetails ';
    let s = null;
    try {
      Scrape.validateParams([selectorFilepath]);
      console.log(funcName + 'selectorFilepath = ' + selectorFilepath);
      const fileData = fs.readFileSync(selectorFilepath);
      const selectorJson = JSON.parse(fileData);
      this.selectorJson = selectorJson;
      s = new Scrape();
      await s.init({ headless: true });

      const univScrappedData = {};
      // validate that page_url must exist
      Scrape.validateParams([this.selectorJson.page_url]);
      // set new browser page with page url
      await s.setupNewBrowserPage(this.selectorJson.page_url);

      // for each json key in slector file
      for (const jsonKey in this.selectorJson) {
        // console.log(funcName + 'jsonKey = ' + jsonKey);
        if (this.selectorJson.hasOwnProperty(jsonKey)) {
          console.log(funcName + '\n\r ###########################      Scraping for key = ' + jsonKey + '     ###########################');
          const jsonVal = this.selectorJson[jsonKey];
          // for each element in json-array, will be always 1 item in array
          if (Array.isArray(jsonVal)) {
            const allRootElemetDictScrappedResList = [];
            for (const elementDict of jsonVal) {
              const outRootElementDict = {};
              const elementUrl = elementDict.url;
              // console.log(funcName + '    elementDict url = ' + elementUrl);
              // for each elementDict, set url or page-url
              let url = null;
              if (elementUrl && elementUrl.length > 0) {
                console.log(funcName + 'overwriting page-url with elementDict url...');
                url = elementUrl;
              }
              // else {
              //   console.log(funcName + 'setting defaulr this.selectorJson.page_url...');
              //   url = this.selectorJson.page_url;
              // }
              const elements = elementDict.elements;
              console.log(funcName + '    elementDict elements = ' + JSON.stringify(elements));
              // for each element of the json-key-dict
              if (Array.isArray(elements)) {
                const allElementsScrappedResList = [];
                for (const eleDict of elements) {
                  const outElementDict = {};
                  const eleType = eleDict.elementType;
                  // console.log(funcName + '      elementType = ' + eleType);
                  let eleOptions = null;
                  if (eleDict.option) {
                    eleOptions = eleDict.option;
                  }
                  if (eleDict.value) {
                    eleOptions = eleDict.value;
                  }
                  const eleSelctors = eleDict.selectors;
                  // for each selector of the element
                  if (Array.isArray(eleSelctors)) {
                    const allSelectorsScrappedResList = [];
                    for (const sel of eleSelctors) {
                      const scrapeElementRes = await s.scrapeElement(eleType, sel, url, eleOptions);
                      console.log(funcName + 'scrapeElementRes = ' + scrapeElementRes);
                      if (scrapeElementRes) {
                        allSelectorsScrappedResList.push(scrapeElementRes);
                      }
                    } // for each sel in selectors[]
                    console.log('**');
                    console.log(funcName + 'All selectors scrapped for the element, allSelectorsJson length = ' + allSelectorsScrappedResList.length);
                    console.log(funcName + 'allSelectorsJson = ' + JSON.stringify(allSelectorsScrappedResList));
                    console.log('**');
                    // allElementsScrappedResList.push(allSelectorsScrappedResList);
                    outElementDict.selectors = allSelectorsScrappedResList;
                    allElementsScrappedResList.push(outElementDict);
                  } // if (Array.isArray(eleSelctors)) {
                } // for each element in elements[]
                console.log('***');
                console.log(funcName + 'All elements scrapped for the key, allElementsJson length = ' + allElementsScrappedResList.length);
                console.log(funcName + 'allElementsJson = ' + JSON.stringify(allElementsScrappedResList));
                console.log('***');
                // allRootElemetDictScrappedResList.push(allElementsScrappedResList);
                outRootElementDict.elements = allElementsScrappedResList;
                allRootElemetDictScrappedResList.push(outRootElementDict);
              } // if (Array.isArray(elements)) {
            } // for (const elementDict of jsonVal)
            univScrappedData[jsonKey] = allRootElemetDictScrappedResList;
          } else { // if (Array.isArray(jsonVal)) {
            univScrappedData[jsonKey] = [];
          }
        } // if (this.selectorJson.hasOwnProperty(jsonKey)
        console.log('\n\r**********');
        console.log(funcName + 'univScrappedData[' + jsonKey + '] = ' + JSON.stringify(univScrappedData[jsonKey]));
        console.log('**********\n\r');
      } // for (const jsonKey in this.selectorJson)
      const formattedUnivData = await this.formatUnivOutput(univScrappedData);
      // const polishedUnivData = await University.placeNAForEachEmptyValue(formattedUnivData);
      // const finalCourseData = await Course.removeKeys(formattedCourseData);
      console.log(funcName + 'final scrapped json = ' + JSON.stringify(formattedUnivData));
      if (s) {
        await s.close();
      }
      // return final scrapped data
      return formattedUnivData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  */
}
// University.KEYS_TO_FORMAT = ['univ_url', 'univ_name', 'univ_scholarship'];

module.exports = { University };
