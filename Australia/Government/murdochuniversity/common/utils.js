const fs = require('fs');
const appConfigs = require('./app-config');
// eslint-disable-next-line import/no-unresolved
const configs = require('./../configs');

// validate function params
async function validateParams(paramsList) {
  const funcName = 'validateParams ';
  try {
    if (!paramsList) {
      console.log(funcName + 'Invalid value of paramsList');
      throw (new Error('Invalid value of paramsList'));
    }
    const invalidParamsList = [];
    paramsList.forEach((param) => {
      if (!param) {
        invalidParamsList.push(param);
      } // if
    });
    if (invalidParamsList.length > 0) {
      console.log(funcName + 'Invalid value of parameter, invalidParamsList = ' + JSON.stringify(invalidParamsList));
      throw (new Error('Invalid value of parameter, invalidParamsList = ' + JSON.stringify(invalidParamsList)));
    }
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

// check if string has valid cricos code
async function hasValidCricosode(scrappedCricosCodeStr) {
  const funcName = 'hasValidCricosode ';
  try {
    if (scrappedCricosCodeStr && scrappedCricosCodeStr.length > 0
      && String(scrappedCricosCodeStr).includes('N/A') === false) {
      return true;
    }
    return false;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

async function getSelectorsListForKey(rootEleKeyName, selectorJson) {
  const funcName = 'getSelectorsListForKey ';
  try {
    // get first root elementDict
    validateParams([rootEleKeyName, selectorJson]);
    const rootElementDictList = selectorJson[rootEleKeyName];
    console.log(funcName + 'rootElementDictList = ' + JSON.stringify(rootElementDictList));
    if (!(rootElementDictList && rootElementDictList.length > 0)) {
      console.log(funcName + 'Invalid or null course_list_selector key');
      throw (new Error('Invalid or null course_list_selector key'));
    }
    const rootElementDict = rootElementDictList[0];
    console.log(funcName + 'rootElementDict = ' + JSON.stringify(rootElementDict));
    // get url and elements list of this element dict
    const url = rootElementDict.url;
    console.log(funcName + 'url = ' + url);
    const elementsList = rootElementDict.elements;
    if (!(elementsList && elementsList.length > 0)) {
      console.log(funcName + 'Invalid or null elements key');
      throw (new Error('Invalid or null elements key'));
    }
    // get first elementDict
    const elementDict = elementsList[0];
    console.log(funcName + 'elementDict = ' + JSON.stringify(elementDict));
    const selectorsList = elementDict.selectors;
    if (!(selectorsList && selectorsList.length > 0)) {
      console.log(funcName + 'Invalid or null selectors key');
      return null;
    }
    return selectorsList;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

// get elementDict
async function getElementDictForKey(rootEleKeyName, selectorJson) {
  const funcName = 'getElementDictForKey ';
  try {
    // get first root elementDict
    validateParams([rootEleKeyName, selectorJson]);
    const rootElementDictList = selectorJson[rootEleKeyName];
    console.log(funcName + 'rootElementDictList = ' + JSON.stringify(rootElementDictList));
    if (!(rootElementDictList && rootElementDictList.length > 0)) {
      console.log(funcName + 'Invalid or null course_list_selector key');
      throw (new Error('Invalid or null course_list_selector key'));
    }
    const rootElementDict = rootElementDictList[0];
    console.log(funcName + 'rootElementDict = ' + JSON.stringify(rootElementDict));
    // get url and elements list of this element dict
    const url = rootElementDict.url;
    console.log(funcName + 'url = ' + url);
    const elementsList = rootElementDict.elements;
    if (!(elementsList && elementsList.length > 0)) {
      console.log(funcName + 'Invalid or null elements key');
      throw (new Error('Invalid or null elements key'));
    }
    // get first elementDict
    const elementDict = elementsList[0];
    if (elementDict) {
      return elementDict;
    }
    return null;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

// get root element 
async function getRooElementDictUrl(rootEleKeyName, selectorJson) {
  const funcName = 'getRooElementDictUrl ';
  try {
    // get first root elementDict
    validateParams([rootEleKeyName, selectorJson]);
    const rootElementDictList = selectorJson[rootEleKeyName];
    console.log(funcName + 'rootElementDictList = ' + JSON.stringify(rootElementDictList));
    if (!(rootElementDictList && rootElementDictList.length > 0)) {
      console.log(funcName + 'Invalid or null course_list_selector key');
      throw (new Error('Invalid or null course_list_selector key'));
    }
    const rootElementDict = rootElementDictList[0];
    console.log(funcName + 'rootElementDict = ' + JSON.stringify(rootElementDict));
    // get url and elements list of this element dict
    const url = rootElementDict.url;
    console.log(funcName + 'url = ' + url);
    if (url) {
      return url;
    }
    return null;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

// create course id from course title
// Trim and remove all white spcaes and conver to lower case
async function removeAllWhiteSpcaesAndLowerCase(val) {
  const funcName = 'removeAllWhiteSpcaesAndLowerCase ';
  try {
    if (!(val && val.length > 0)) {
      console.log(funcName + 'Invalid prama, val = ' + val);
      throw (new Error('Invalid prama, val = ' + val));
    }
    // console.log(funcName + 'val  before operation = ' + val);
    const trimmedVal = String(val).trim();
    // console.log(funcName + 'trimmedVal = ' + trimmedVal);
    //const regex = /\s/g;
    const regex = /[,\/ ]/g;
    const wsRemovedVal = String(trimmedVal).replace(regex, '');
    const ucVal = String(wsRemovedVal).toLowerCase();
    // console.log(funcName + 'val  after operation = ' + ucVal);
    return ucVal;
  } catch (error) {
    console.log(funcName + 'try-catch error = ', error);
    throw (error);
  }
}

// generate opCourseFilePath
async function generateOutputCourseFilePath(courseDict,program_code) {
  const funcName = 'generateOutputCourseFilePath ';
  try {
    let opCourseFilePath = null;
    if (!courseDict) {
      console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
      throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
    }
    // create courseId from course title
    var courseId = await removeAllWhiteSpcaesAndLowerCase(courseDict.innerText);
    courseId = courseId + "_" + program_code;
    // validate generated courseId
    console.log(funcName + 'courseId = ' + courseId);
    if (!(courseId && courseId.length > 0)) {
      console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
      throw (new Error('Invalid courseId, courseId = ' + courseId));
    }
    if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
      opCourseFilePath = configs.outputDirPath + configs.univ_id + '_' + courseId + '_update_coursedetails.json';
    } else {
      opCourseFilePath = configs.outputDirPath + configs.univ_id + '_' + courseId + '_coursedetails.json';
    }
    console.log(funcName + 'opCourseFilePath = ' + opCourseFilePath);
    return opCourseFilePath;
  } catch (error) {
    console.log(funcName + 'try-catch error = ', error);
    throw (error);
  }
}

// get values from hard-coded json file
async function getValueFromHardCodedJsonFile(jsonKeyName) {
  const funcName = 'getValueFromHardCodedJsonFile ';
  try {
    validateParams([jsonKeyName]);
    const fileData = fs.readFileSync(appConfigs.hardCodedJsonDataFilepath);
    if (!fileData) {
      throw (new Error('Invalid file data, fileData = ' + fileData));
    }
    const dataJSon = JSON.parse(fileData)[0];
    if (!dataJSon) {
      throw (new Error('Invalid json data, dataJSon = ' + JSON.stringify(dataJSon)));
    }
    const keyValData = dataJSon[jsonKeyName];
    return keyValData;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

//for remove course list
async function validateMyCourseList(page, courselistfile, list, selectorgroup, text) {
  var missingdata = [], founddata = [];
  //var min_value = 0;
  //var max_value = -1; //set -1 if requires no limit
  let elemfound = false, elementstring = "";
  try {
    //const fileData = JSON.parse(fs.readFileSync(courselistfile));
    const fileData = list;
    //max_value = (fileData.length > max_value && max_value!=-1) ? max_value : fileData.length;
    console.log("### Total urls ### " + fileData.length);
    for (var count = 0; count < fileData.length; count++) {
      await page.goto(fileData[count].href);
      const targetLinks = await page.$$(selectorgroup);
      for (let link of targetLinks) {
        elementstring = await page.evaluate(el => el.innerText, link);
        if (elementstring === text) {
          elemfound = true;
        }
      }
      if (elemfound) {
        founddata.push(fileData[count]);
      }
      else {
        missingdata.push(fileData[count]);
        console.log("##### NOT Found element #####  --" + (count + 1));
        console.log("---URL----" + fileData[count].href)
      }
      elemfound = false;
    }
    fs.writeFileSync("./output/skip_course.json", JSON.stringify(missingdata));
    fs.writeFileSync(courselistfile, JSON.stringify(founddata));
    console.log("##### Total Course #####  -- " + fileData.length);
    console.log("##### Total Course with cricos code #####  -- " + founddata.length);
    console.log("##### Total Missing cricose code #####  -- " + missingdata.length)
  }
  catch (ex) {
    console.log("Error: " + ex)
  }
}
//for check whethe element is present or not
async function checkElementAvailabilty(page, selectorgroup, pageurl, compareWith, compareUsing) {
  const targetLinks = await page.$$(selectorgroup);
  var match = false, elem = null, elementstring = "";
  try {
    if (page.url() != pageurl && pageurl != "" && pageurl != null) {
      await page.goto(pageurl);
    }
    for (let link of targetLinks) {
      switch (compareWith) {
        case "InnerText":
          elementstring = await page.evaluate(el => el.innerText, link);
        case "InnerHTML":
          elementstring = await page.evaluate(el => el.innerHTML, link);
      }
      if (elementstring === compareUsing) {
        elem = link;
        return true;
      }
    }
  }
  catch (exp) {
    return exp;
  }
  return false;
}
async function checkandclick(page, cssSelector) {
  try {
    await page.waitForSelector(cssSelector, {
      timeout: 2000
    });
    const clickelmclick = await page.$(cssSelector);
    await clickelmclick.click();
  } catch (err) {
    return;
  }
  return checkandclick(page, cssSelector);
}

async function giveMeArray(rootstring, seperator) {
  console.log("Called array-->" + rootstring)
  var array_data = rootstring.replace(/\s/g, ' ').split(seperator);
  return await array_data;
}
async function giveMeNumber(myString) {
  var myarray = myString.split(" ");
  console.log("### split data-->" + JSON.stringify(myarray));
  for (let element of myarray) {
    if (/\d/.test(element)) {
      console.log("##match" + element)
      return await parseFloat(element.replace(/[\r\n\t:,]+/g, ''))
    }
  }
  return await "NA";
}
async function providemyintake(oldintake, format, separator) {
  var newintake = [];

  const fullmonths = [{ fullkey: "January", shortkey: "Jan", intake: "Summer" }, { fullkey: "February", shortkey: "Feb", intake: "Summer" },
  { fullkey: "March", shortkey: "Mar", intake: "Autumn" }, { fullkey: "April", shortkey: "Apr", intake: "Autumn" }, { fullkey: "May", shortkey: "May", intake: "Autumn" }, { fullkey: "June", shortkey: "June", intake: "Winter" },
  { fullkey: "July", shortkey: "July", intake: "Winter" }, { fullkey: "August", shortkey: "Aug", intake: "Winter" }, { fullkey: "September", shortkey: "Sept", intake: "Spring" },
  { fullkey: "October", shortkey: "Oct", intake: "Spring" }, { fullkey: "November", shortkey: "Nov", intake: "Spring" }, { fullkey: "December", shortkey: "Dec", intake: "Summer" }];

  oldintake.forEach(element => {
    var newdataafterreplace = {}, replacedata = [];
    element.value.forEach(elementdate => {
      var date = {};
      switch (format) {
        case "MOM": {
          fullmonths.forEach(elementmon => {
            if (elementdate.toLowerCase().indexOf(elementmon.fullkey.toLowerCase()) != -1) {
              date.actualdate = elementdate;
              date.month = elementmon.fullkey;
              //date.intake = elementmon.intake;
            }
          });
          break;
        }
        case "mom": {
          fullmonths.forEach(elementmon => {
            if (elementdate.toLowerCase().indexOf(elementmon.shortkey.toLowerCase()) != -1) {
              date.actualdate = elementdate;
              date.month = elementmon.fullkey;
              //date.intake = elementmon.intake;
            }
          });
          break;
        }
        case "ddmmyyyy": {
          var elementmon = fullmonths[elementdate.split(separator)[1] - 1];
          date.actualdate = elementdate;
          date.month = elementmon.fullkey;
          //date.intake = elementmon.intake;
          break;
        }
        case "mmddyyyy": {
          var elementmon = fullmonths[elementdate.split(separator)[0] - 1];
          date.actualdate = elementdate;
          date.month = elementmon.fullkey;
          //date.intake = elementmon.intake;
          break;
        }
      }
      replacedata.push(date);
    });
    newdataafterreplace.name = element.name;
    newdataafterreplace.value = replacedata;
    newintake.push(newdataafterreplace);
  });
  return newintake;
}
async function getMappingScore(coursename,studylevel) {
  try {
    const scrore_file = appConfigs.ielts_mapping;

    const fileData = JSON.parse(fs.readFileSync(scrore_file));
    if (!fileData) {
      throw (new Error('Invalid file data, fileData = ' + fileData));
    }
    var returnDict = {};


    console.log("coursename --->", coursename);
    if(coursename.toLowerCase().indexOf('master of education')>-1){
      for(let data of fileData[1]){
        // console.log("####let data of Nursing Match data for--->" + coursename);
         //returnDict.IELTS = data.IELTS;
         returnDict.pte = data.pte;
         returnDict.ibt = data.ibt;
         returnDict.ielts=data.ielts
         //returnDict.cae=data.cae
       }
    }
    else if(coursename.toLowerCase().indexOf('master of wildlife health and conservation')>-1 || coursename.toLowerCase
    ().indexOf('master of food security') >-1 ||coursename.toLowerCase().indexOf('master of veterinary studies in conservation medicine')> -1){
      for(let data of fileData[2]){
        // console.log("####let data of Nursing Match data for--->" + coursename);
         //returnDict.IELTS = data.IELTS;
         returnDict.pte = data.pte;
         returnDict.ibt = data.ibt;
         returnDict.cae=data.cae
       }
    }
    else if(coursename.toLowerCase().indexOf('master of veterinary clinical studies')> -1){
      for(let data of fileData[3]){
        // console.log("####let data of Nursing Match data for--->" + coursename);
         //returnDict.IELTS = data.IELTS;
         returnDict.pte = data.pte;
         returnDict.ibt = data.ibt;
         returnDict.cae=data.cae
       }
    }
    else if(coursename.toLowerCase().indexOf('graduate diploma in migration law and practice')> -1){
      for(let data of fileData[4]){
        // console.log("####let data of Nursing Match data for--->" + coursename);
         //returnDict.IELTS = data.IELTS;
         //returnDict.pte = data.pte;
         returnDict.ibt = data.ibt;
         //returnDict.cae=data.cae
       }
    }
    
    else if(coursename.toLowerCase().indexOf('criminology') > -1 ||coursename.toLowerCase().indexOf('law') > -1 ){
      console.log("coursename --->", coursename);
      for(let data of fileData[8]){
       // console.log("####let data of Nursing Match data for--->" + coursename);
        //returnDict.IELTS = data.IELTS;
        returnDict.pte = data.pte;
        returnDict.ibt = data.ibt;
        returnDict.cae=data.cae
      }

    }
    else if (coursename.toLowerCase().indexOf('nursing') > -1) {
      for (let data of fileData[9]) {
        console.log("####let data of Nursing Match data for--->" + coursename);
        returnDict.pte = data.pte;
        returnDict.ibt = data.ibt;
        returnDict.cae=data.cae
      }
    } else if (coursename.toLowerCase().indexOf('veterinary science') > -1) {
      console.log("coursename --->", coursename);
      for (let data of fileData[10]) {
        console.log("####let data of veterinary science Match data for--->" + coursename);
        returnDict.pte = data.pte;
        returnDict.ibt = data.ibt;
        returnDict.cae=data.cae
      }
    } else if (coursename.toLowerCase().indexOf('education') > -1) {
      console.log("coursename --->", coursename);
      for (let data of fileData[11]) {
        console.log("####let data of education Match data for--->" + coursename);
        returnDict.pte = data.pte;
        returnDict.ibt = data.ibt;
        returnDict.cae=data.cae
        returnDict.ielts=data.ielts
      }
    }
    else if(studylevel.toLowerCase()=="postgraduate")  {
      
      for (let data of fileData[0]) {     
       
          returnDict.pte = data.pte;
          returnDict.ibt = data.ibt;
          returnDict.cae=data.cae
             
      }
    }
    else if(studylevel.toLowerCase()=="undergraduate"){
      for (let data of fileData[7]) {     
       
        returnDict.pte = data.pte;
        returnDict.ibt = data.ibt;
        returnDict.cae=data.cae  
            
        
    
         }
    }

    if (returnDict.IELTS == undefined) {
      for (let data of fileData[0]) {
        console.log("let data of fileData[0]");
        returnDict.IELTS = data.IELTS;
        returnDict.PTE = data.PTE;
        returnDict.TOFEL = data.TOFEL;
      }
    }
    return await returnDict;
  } catch (error) {
    console.log('try-catch error = ' + error);
    throw (error);
  }
}
module.exports = {
  getSelectorsListForKey,
  getElementDictForKey,
  getRooElementDictUrl,
  removeAllWhiteSpcaesAndLowerCase,
  generateOutputCourseFilePath,
  getValueFromHardCodedJsonFile,
  hasValidCricosode,
  validateMyCourseList,
  checkElementAvailabilty,
  checkandclick,
  giveMeArray,
  giveMeNumber,
  providemyintake,
  getMappingScore
};
