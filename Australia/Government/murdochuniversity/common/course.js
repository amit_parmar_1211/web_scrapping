// const fs = require('fs');
const path = require('path');
const Scrape = require('./scrape').Scrape;
// eslint-disable-next-line import/no-unresolved
const appConfigs = require('./../common/app-config');
const configs = require('./../configs');

class Course extends Scrape {
  // remove unwanted keys
  static async removeKeys(scrappedDataJson) {
    const funcName = 'removeKeys ';
    try {
      const resJson = scrappedDataJson;
      Scrape.validateParams([scrappedDataJson]);
      for (const key of Course.KEYS_TO_REMOVE) {
        console.log(funcName + 'removing key = ' + key);
        delete resJson[key];
      }
      return resJson;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  /*
  // init
  async init(puputrOptions) {
    const funcName = 'init ';
    try {
      console.log(funcName + 'puputrOptions = ' + JSON.stringify(puputrOptions));
      await super.init(puputrOptions);
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      super.close();
      throw (error);
    }
  }
  */
  /*
  static async placeNAForEachEmptyValue(courseScrappedData) {
    const funcName = 'placeNAForEachEmptyValue ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = courseScrappedData;
      for (const key in courseScrappedData) {
        console.log(funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          const keyVal = courseScrappedData[key];
          console.log(funcName + 'keyVal = ' + keyVal);
          if (typeof keyVal === 'string') {
            if (keyVal && keyVal.length <= 0) {
              console.log(funcName + key + ' has empty string value so replacing with NA...');
              resJsonData[key] = configs.propValueNotAvaialble;
            }
          }
        }
      } // for
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  */
  
  
  /*
  static async extractValueFromScrappedElement(rootElementDictList) {
    const funcName = 'extractValueFromScrappedElement ';
    try {
      console.log(funcName + 'rootElementDictList =' + JSON.stringify(rootElementDictList));
      console.log(funcName + 'rootElementDictList = ' + rootElementDictList);
      let extractedVal = null;
      let concatnatedRootElementsStr = null;
      if (Array.isArray(rootElementDictList)) {
        for (const rootEleDict of rootElementDictList) {
          // console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
          const elementsList = rootEleDict.elements;
          let concatnatedElementsStr = null;
          for (const eleDict of elementsList) {
            // console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
            const selectorsList = eleDict.selectors;
            let concatnatedSelectorsStr = null;
            for (const selList of selectorsList) {
              console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
              console.log(funcName + '\n\r selList = ' + selList);
              let concatnatedSelStr = null;
              for (const selItem of selList) {
                if (!concatnatedSelStr) {
                  concatnatedSelStr = selItem;
                } else {
                  concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                }
              } // selList
              console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
              if (concatnatedSelStr) {
                if (!concatnatedSelectorsStr) {
                  concatnatedSelectorsStr = concatnatedSelStr;
                } else {
                  concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                }
              }
            } // selectorsList
            console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
            // concat elements
            if (concatnatedSelectorsStr) {
              if (!concatnatedElementsStr) {
                concatnatedElementsStr = concatnatedSelectorsStr;
              } else {
                concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
              }
            }
          } // elementsList
          console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
          if (concatnatedElementsStr) {
            if (!concatnatedRootElementsStr) {
              concatnatedRootElementsStr = concatnatedElementsStr;
            } else {
              concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
            }
          }
        } // rootElementDictList
      }
      extractedVal = String(concatnatedRootElementsStr).trim();
      console.log(funcName + 'extractedVal = ' + extractedVal);
      return extractedVal;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  */

  // format output json
  /*
  async formatOutput(courseScrappedData) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'course_study_level': {
              const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
              let cStudyLevel = null;
              console.log(funcName + 'course_title = ' + cTitle);
              if (cTitle) {
                if (String(cTitle).includes('bachelor') || String(cTitle).includes('Bachelor')) {
                  cStudyLevel = 'Bachelor';
                } else if (String(cTitle).includes('master') || String(cTitle).includes('Master')) {
                  cStudyLevel = 'Master';
                } else if (String(cTitle).includes('postgraduate') || String(cTitle).includes('Postgraduate')) {
                  cStudyLevel = 'Postgraduate';
                }
                if (cStudyLevel) {
                  resJsonData.course_study_level = cStudyLevel;
                }
              }
              break;
            }
            case 'course_discipline': {
              // As of not we do not have way to get this field value
              break;
            }
            case 'course_admission_requirement': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              const courseAdminReq = {};
              const englishList = [];
              
              const ieltsDict = {}; const ibtDict = {}; const pbtDict = {};
              // english requirement
              const ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
              if (ieltsScore && ieltsScore.length > 0) {
                ieltsDict.name = 'ielts';
                ieltsDict.description = ieltsScore;
                englishList.push(ieltsDict);
              }
              const ibtScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ibt_indicator);
              if (ibtScore && ibtScore.length > 0) {
                ibtDict.name = 'toefl ibt';
                ibtDict.description = ibtScore;
                englishList.push(ibtDict);
              }
              const pbtScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_toefl_pbt_score);
              if (pbtScore && pbtScore.length > 0) {
                pbtDict.name = 'toefl pbt';
                pbtDict.description = pbtScore;
                englishList.push(pbtDict);
              }
              if (englishList && englishList.length > 0) {
                courseAdminReq.english = englishList;
              }
              
              // academic requirement
              const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
              console.log('\n\r');
              console.log(funcName + 'academicReq = ' + academicReq);
              console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
              console.log('\n\r');
              if (academicReq && String(academicReq).length > 0) {
                courseAdminReq.academic = academicReq;
              }
              // if courseAdminReq has any valid value then only assign it
              if (courseAdminReq.english || courseAdminReq.academic) {
                resJsonData.course_admission_requirement = courseAdminReq;
              }

              // add english requirement 'english_more_details' link
              const courseKeyVal = courseScrappedData.course_admission_english_more_details;
              let resEnglishReqMoreDetailsJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resEnglishReqMoreDetailsJson = selList[0];
                      }  
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))
              if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                resJsonData.course_admission_requirement.english_more_details = resEnglishReqMoreDetailsJson;
              } else if (resEnglishReqMoreDetailsJson) {
                resJsonData.course_admission_requirement = {};
                resJsonData.course_admission_requirement.english_more_details = resEnglishReqMoreDetailsJson;
              }
              break;
            }
            case 'course_url': {
              // console.log(funcName + 'this.selectorJson.page_url = ' + this.selectorJson.page_url);
              if (this.selectorJson && this.selectorJson.page_url) {
                resJsonData.course_url = this.selectorJson.page_url;
              }
              break;
            }
            case 'course_cricos_code': { // "CRICOS code: 087981E", split by : and only set code-value
              // resJsonData.course_cricos_code = configs.propValueNotAvaialble;
              const cricosCodeText = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
              if (cricosCodeText && cricosCodeText.length > 0) {
                let resCricosCode = null;
                // split by :
                const textList = String(cricosCodeText).split(':');
                console.log(funcName + 'textList = ' + JSON.stringify(textList));
                if (textList && textList.length > 1) {
                  resCricosCode = textList[1];
                  if (resCricosCode) {
                    const trimmedStr = String(resCricosCode).trim();
                    resJsonData.course_cricos_code = trimmedStr;
                    console.log(funcName + 'course_cricos_code, trimmedStr = ' + trimmedStr);
                  }
                } // if (textList && textList.length > 1)
              }
              break;
            }
            case 'course_duration_full_time': {
              // "course_duration_display": [ {"duration_full_time": "1", "unit": "year" }, { "duration_part_time": "2", "unit": "year" }],
              // "course_duration":         [ {"duration_full_time": "1 year full time for Domestic and International students"},
              // {"duration_part_time": "2 years part time for Domestic students / Not available to International students"}],
              // prepare empty JSON struct
              const courseDurationList = []; const courseDurationDisplayList = [];
              const durationFullTime = {};

              // display full time
              const fullTimeText = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time);
              // split by :, Full time
              if (fullTimeText && fullTimeText.length > 0) {
                let resFulltime = null;
                const textList = String(fullTimeText).split(':');
                console.log(funcName + 'textList = ' + JSON.stringify(textList));
                if (textList && textList.length > 1) {
                  resFulltime = textList[1];
                }
                if (resFulltime) {
                  durationFullTime.duration_full_time = resFulltime;
                  courseDurationList.push(durationFullTime);
                  // fetch value for 'duration' as number
                  const regEx = /[0-9]/g;
                  const yearNumList = String(resFulltime).match(regEx);
                  console.log(funcName + 'yearNumList = ' + JSON.stringify(yearNumList));
                  let durationFullTimeDisplay = null;
                  if (yearNumList && Array.isArray(yearNumList) && yearNumList.length > 0) {
                    durationFullTimeDisplay = {};
                    durationFullTimeDisplay.duration_full_time = yearNumList[0];
                    // fetch value for 'unit'
                    for (const item of Course.COURSE_DURATION_UNIT_TYPES_LIST) {
                      if (String(resFulltime).includes(item)) {
                        durationFullTimeDisplay.unit = item;
                      }
                    }
                    courseDurationDisplayList.push(durationFullTimeDisplay);
                  }
                }
              } // if (fullTimeText && fullTimeText.length > 0)
              
              //  PART TIME DURATION
              const durationPartTime = {};
              const partTimeText = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_part_time);
              // split by :, Part time
              if (partTimeText && partTimeText.length > 0) {
                let resParttime = null;
                const textList = String(partTimeText).split(':');
                console.log(funcName + 'textList = ' + JSON.stringify(textList));
                if (textList && textList.length > 1) {
                  resParttime = textList[1];
                }
                if (resParttime) {
                  durationPartTime.duration_part_time = resParttime;
                  courseDurationList.push(durationPartTime);
                  // fetch value for 'duration' as number
                  const regEx = /[0-9]/g;
                  console.log(funcName + 'resParttime = ' + resParttime);
                  const yearNumList = String(resParttime).match(regEx);
                  console.log(funcName + 'yearNumList = ' + JSON.stringify(yearNumList));
                  let durationPartTimeDisplay = null;
                  if (yearNumList && Array.isArray(yearNumList) && yearNumList.length > 0) {
                    durationPartTimeDisplay = {};
                    durationPartTimeDisplay.duration_part_time = yearNumList[0];
                    // fetch value for 'unit'
                    for (const item of Course.COURSE_DURATION_UNIT_TYPES_LIST) {
                      if (String(resParttime).includes(item)) {
                        durationPartTimeDisplay.unit = item;
                      }
                    } // for
                  }
                  if (durationPartTimeDisplay) {
                    courseDurationDisplayList.push(durationPartTimeDisplay);
                  }
                } 
              } // if (partTimeText && partTimeText.length > 0)

              if (courseDurationList && courseDurationList.length > 0) {
                resJsonData.course_duration = courseDurationList;
              }
              if (courseDurationDisplayList && courseDurationDisplayList.length > 0) {
                resJsonData.course_duration_display = courseDurationDisplayList;
              }
              break;
            }
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'course_toefl_ielts_score':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_tuition_fees_international_student':
            case 'course_duration_part_time': {
              console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
              // No need to process anything, its already processed in other case.
              break;
            }
            case 'course_tuition_fee': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};
              // courseTuitionFee.year = configs.propValueNotAvaialble;

              const feesList = [];
              const feesDict = {};
              // feesDict.international_student = configs.propValueNotAvaialble;
              // feesDict.fee_duration_years = configs.propValueNotAvaialble;
              // feesDict.currency = configs.propValueNotAvaialble;
              
              const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
              const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
              if (feeYear && feeYear.length > 0) {
                courseTuitionFee.year = feeYear;
              }
              // if we can extract value as Int successfully then replace Or keep as it is
              if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                let textList = [];
                textList = String(feesIntStudent).split(':');
                console.log(funcName + 'textList = ' + JSON.stringify(textList));
                if (textList && textList.length > 1) {
                  const feesWithDollor = textList[1];
                  const feesWithDollorTrimmed = String(feesWithDollor).trim();
                  console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                  const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                  console.log(funcName + 'feesVal = ' + feesVal);
                  if (feesVal) {
                    const feesNumber = parseInt(feesVal.replace(/,/g, ''), 10);
                    console.log(funcName + 'Number(), feesNumber = ' + feesNumber);
                    feesDict.international_student = feesNumber;
                  }
                }
              } // if (feesIntStudent && feesIntStudent.length > 0)
              const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
              const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
              if (feesDuration && feesDuration.length > 0) {
                feesDict.fee_duration_years = feesDuration;
              }
              if (feesCurrency && feesCurrency.length > 0) {
                feesDict.currency = feesCurrency;
              }
              if (feesDict) {
                feesList.push(feesDict);
              }
              if (feesList && feesList.length > 0) {
                courseTuitionFee.fees = feesList;
              }
              console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
              if (courseTuitionFee) {
                resJsonData.course_tuition_fee = courseTuitionFee;
              }
              break;
            }
            case 'course_study_mode': { // "Study mode: On-campus day and evening", split by :
              const studyModeText = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_mode);
              if (studyModeText && studyModeText.length > 0) {
                let studyModeVal = null;
                // split by :
                const textList = String(studyModeText).split(':');
                console.log(funcName + 'textList = ' + JSON.stringify(textList));
                if (textList && textList.length > 1) {
                  studyModeVal = textList[1];
                }
                if (studyModeVal) {
                  const studyModeValTrimmed = String(studyModeVal).trim();
                  resJsonData.course_study_mode = studyModeValTrimmed;
                }
              } // if (studyModeText && studyModeText.length > 0)
              break;
            }
            case 'course_campus_location': { // "Location: Camperdown/Darlington campus", split by :
              const campLocationText = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
              if (campLocationText && campLocationText.length > 0) {
                let campLocationVal = null;
                // split by :
                const textList = String(campLocationText).split(':');
                console.log(funcName + 'textList = ' + JSON.stringify(textList));
                if (textList && textList.length > 1) {
                  campLocationVal = textList[1];
                }
                if (campLocationVal) {
                  const campLocationValTrimmed = String(campLocationVal).trim();
                  resJsonData.course_campus_location = campLocationValTrimmed;
                }
              } // if (campLocationText && campLocationText.length > 0)
              break;
            }
            case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
              // Semester 1 (February) and Semester 2 (August)
              // prepare emty JSON struct
              if (courseScrappedData.course_intake) {
                resJsonData.course_intake = courseScrappedData.course_intake;
              }
              const courseIntakeDisplay = {};
              // existing intake value
              const courseIntakeStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
              if (courseIntakeStr && courseIntakeStr.length > 0) {
                const intakeStrList = String(courseIntakeStr).split(' ');
                console.log(funcName + 'intakeStrList = ' + JSON.stringify(intakeStrList));
                const regEx = /[1-9]/g; const semList = []; const semMonthList = [];
                for (const item of intakeStrList) {
                  const trimmedItem = String(item).trim();
                  const charIndex = String(trimmedItem).search(regEx);
                  if (charIndex >= 0) {
                    console.log(funcName + 'charIndex = ' + charIndex);
                    const semCharVal = String(trimmedItem).charAt(charIndex);
                    if (semCharVal && semCharVal.length > 0) {
                      console.log(funcName + 'semCharVal = ' + semCharVal);
                      const semKey = 'semester' + String(semCharVal).trim();
                      console.log(funcName + 'semKey = ' + semKey);
                      semList.push(semKey);
                      console.log(funcName + 'semList = ' + JSON.stringify(semList));
                    } // if (semCharVal && semCharVal.length > 0) {
                  } // if (charIndex >= 0) {
                  // seperate months
                  for (const m of Course.COURSE_MONTHS_LIST) {
                    if (String(trimmedItem).includes(m)) {
                      semMonthList.push(m + ' ' + new Date().getUTCFullYear());
                      console.log(funcName + 'semMonthList = ' + JSON.stringify(semMonthList));
                    }
                  } // for
                } // for
                // now create json dict from semList and mothList
                if (semList.length === semMonthList.length) {
                  let index = 0;
                  for (const semNum of semList) {
                    courseIntakeDisplay[semNum] = semMonthList[index];
                    index += 1;
                  }
                }
                resJsonData.course_intake = courseIntakeStr; // if we failed to parse then we may use this for display
              } // if (courseIntakeStr && courseIntakeStr.length > 0)

              if (courseIntakeDisplay) {
                resJsonData.course_intake_display = courseIntakeDisplay;
              }
              break;
            }
            case 'course_outline': {
              let courseKeyVal = courseScrappedData.course_outline;
              const resOutlineList = []; // const studyUnitsDict = {};

              if (Array.isArray(courseKeyVal)) {
                let concatRootElementsList = [];
                for (const rootEleDict of courseKeyVal) {
                  let concatElementsList = [];
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    let concatSelectorsList = [];
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      let concatSelList = [];
                      if (Array.isArray(selList)) {
                        concatSelList = concatSelList.concat(selList);
                      }  
                      concatSelectorsList = concatSelectorsList.concat(concatSelList);
                    }
                    concatElementsList = concatElementsList.concat(concatSelectorsList);
                  }
                  concatRootElementsList = concatRootElementsList.concat(concatElementsList);
                  if (concatRootElementsList && concatRootElementsList.length > 0) {
                    // studyUnitsDict.study_units = concatRootElementsList;
                    resOutlineList.push(concatRootElementsList);
                  }
                }
              } // if (Array.isArray(courseKeyVal))

              // check if we have honours available then append it
              let subjectAreasList = null;
              if (courseScrappedData.course_outline_honours && Array.isArray(courseScrappedData.course_outline_honours) && courseScrappedData.course_outline_honours.length > 0) {
                // const honoursSubList = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_honours);
                courseKeyVal = courseScrappedData.course_outline_honours;
                if (Array.isArray(courseKeyVal)) {
                  // concatRootElementsList = [];
                  for (const rootEleDict of courseKeyVal) {
                    // let concatElementsList = [];
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                      // let concatSelectorsList = [];
                      const selectorsList = eleDict.selectors;
                      for (const selList of selectorsList) {
                        // let concatSelList = [];
                        if (Array.isArray(selList)) {
                          subjectAreasList = [];
                          for (const subDict of selList) {
                            // console.log(funcName + 'honoursList  subDict = ' + JSON.stringify(subDict));
                            subjectAreasList.push(subDict);
                          } // for of
                        } // if
                        // concatSelectorsList = concatSelectorsList.concat(concatSelList);
                      } // for selectorsList
                      // concatElementsList = concatElementsList.concat(concatSelectorsList);
                    } //  for elementsList
                    // concatRootElementsList = concatRootElementsList.concat(concatElementsList);
                    // studyUnitsDict.study_units = concatRootElementsList;
                    // resOutlineList.push(studyUnitsDict);
                  } //  for rootEleDict
                } // if (Array.isArray(courseKeyVal))
              } // if
              console.log(funcName + 'subjectAreasList = ' + JSON.stringify(subjectAreasList));
              if (resOutlineList && resOutlineList.length > 0) {
                resJsonData.course_outline = {};
                resJsonData.course_outline.study_units = resOutlineList[0];
              }
              if (subjectAreasList && subjectAreasList.length > 0) {
                if (resJsonData.course_outline) {
                  resJsonData.course_outline.subject_areas = subjectAreasList;
                } else {
                  const courseOutlineDict = {};
                  courseOutlineDict.subject_areas = subjectAreasList;
                  resJsonData.course_outline = courseOutlineDict;
                }
              }
              break;
            }
            case 'course_scholarship': {
              const courseKeyVal = courseScrappedData.course_scholarship;
              let resScholarshipJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList[0];
                      }  
                    }
                  }
                }
              }
              if (resScholarshipJson) {
                resJsonData.course_scholarship = resScholarshipJson;
              }
              break;
            }
            case 'univ_name': {
              const courseKeyVal = courseScrappedData.univ_name;
              let resUnivName = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivName = selList[0];
                      }  
                    }
                  }
                }
              }
              if (resUnivName) {
                resJsonData.univ_name = resUnivName;
              }
              break;
            }
            case 'univ_logo': {
              const courseKeyVal = courseScrappedData.univ_logo;
              let resUnivLogo = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivLogo = selList[0];
                      }  
                    }
                  }
                }
              }
              if (resUnivLogo) {
                resJsonData.univ_logo = resUnivLogo;
              }
              break;
            }
            case 'course_accomodation_cost': {
              const courseKeyVal = courseScrappedData.course_accomodation_cost;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }  
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_accomodation_cost = resAccomodationCostJson;
              }
              break;
            }
            case 'course_country':
            case 'course_overview':
            case 'course_career_outcome':
            case 'course_title': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
              // add only if it has valid value
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
              }
              break;
            }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      // console.log('\n\r' + funcName + 'course resJsonData =' + JSON.stringify(resJsonData));
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  */

  // scrape everything as per selector file
  /*
  async scrapeCourseDetails(selectorFilepath, courseURL) {
    const funcName = 'scrapeCourseDetails ';
    let s = null;
    try {
      Scrape.validateParams([selectorFilepath, courseURL]);
      console.log(funcName + 'selectorFilepath = ' + selectorFilepath);
      const fileData = fs.readFileSync(selectorFilepath);
      const selectorJson = JSON.parse(fileData);
      this.selectorJson = selectorJson;
      s = new Scrape();
      await s.init({ headless: true });
      // set course URL value to selector json page-url
      this.selectorJson.page_url = courseURL;
      // set new browser page with course URL
      await s.setupNewBrowserPage(this.selectorJson.page_url);
      const courseScrappedData = {};
      // for each json key in slector file
      for (const jsonKey in this.selectorJson) {
        // console.log(funcName + 'jsonKey = ' + jsonKey);
        if (this.selectorJson.hasOwnProperty(jsonKey)) {
          console.log(funcName + '\n\r ###########################      Scraping for key = ' + jsonKey + '     ###########################');
          const jsonVal = this.selectorJson[jsonKey];
          // for each element in json-array, will be always 1 item in array
          if (Array.isArray(jsonVal)) {
            const allRootElemetDictScrappedResList = [];
            for (const elementDict of jsonVal) {
              const outRootElementDict = {};
              const elementUrl = elementDict.url;
              // console.log(funcName + '    elementDict url = ' + elementUrl);
              // for each elementDict, set url or page-url
              let url = null;
              if (elementUrl && elementUrl.length > 0) {
                console.log(funcName + 'overwriting page-url with elementDict url...');
                url = elementUrl;
              }
              // else {
              //   console.log(funcName + 'setting defaulr this.selectorJson.page_url...');
              //   url = this.selectorJson.page_url;
              // }
              const elements = elementDict.elements;
              console.log(funcName + '    elementDict elements = ' + JSON.stringify(elements));
              // for each element of the json-key-dict
              if (Array.isArray(elements)) {
                const allElementsScrappedResList = [];
                for (const eleDict of elements) {
                  const outElementDict = {};
                  const eleType = eleDict.elementType;
                  // console.log(funcName + '      elementType = ' + eleType);
                  let eleOptions = null;
                  if (eleDict.option) {
                    eleOptions = eleDict.option;
                  }
                  if (eleDict.value) {
                    eleOptions = eleDict.value;
                  }
                  const eleSelctors = eleDict.selectors;
                  // for each selector of the element
                  if (Array.isArray(eleSelctors)) {
                    const allSelectorsScrappedResList = [];
                    for (const sel of eleSelctors) {
                      const scrapeElementRes = await s.scrapeElement(eleType, sel, url, eleOptions);
                      if (jsonKey === 'course_cricos_code') {
                        // check if valid code else do not continue
                        if ((appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION === false) 
                          && (await Course.hasValidCricosode(scrapeElementRes) === false)) {
                          console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                          console.log(funcName + 'Invalid cricos code so break scrapping function and continue with next course....');
                          console.log(funcName + 'scrapeElementRes = ' + scrapeElementRes);
                          console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                          return null;
                        }
                      }
                      console.log(funcName + 'scrapeElementRes = ' + scrapeElementRes);
                      if (scrapeElementRes) {
                        allSelectorsScrappedResList.push(scrapeElementRes);
                      }
                    } // for each sel in selectors[]
                    console.log('**');
                    console.log(funcName + 'All selectors scrapped for the element, allSelectorsJson length = ' + allSelectorsScrappedResList.length);
                    console.log(funcName + 'allSelectorsJson = ' + JSON.stringify(allSelectorsScrappedResList));
                    console.log('**');
                    // allElementsScrappedResList.push(allSelectorsScrappedResList);
                    outElementDict.selectors = allSelectorsScrappedResList;
                    allElementsScrappedResList.push(outElementDict);
                  } // if (Array.isArray(eleSelctors)) {
                } // for each element in elements[]
                console.log('***');
                console.log(funcName + 'All elements scrapped for the key, allElementsJson length = ' + allElementsScrappedResList.length);
                console.log(funcName + 'allElementsJson = ' + JSON.stringify(allElementsScrappedResList));
                console.log('***');
                // allRootElemetDictScrappedResList.push(allElementsScrappedResList);
                outRootElementDict.elements = allElementsScrappedResList;
                allRootElemetDictScrappedResList.push(outRootElementDict);
              } // if (Array.isArray(elements)) {
            } // for (const elementDict of jsonVal)
            courseScrappedData[jsonKey] = allRootElemetDictScrappedResList;
          } else { // if (Array.isArray(jsonVal)) {
            courseScrappedData[jsonKey] = [];
          }
        } // if (this.selectorJson.hasOwnProperty(jsonKey)
        console.log('\n\r**********');
        console.log(funcName + 'courseScrappedData[' + jsonKey + '] = ' + JSON.stringify(courseScrappedData[jsonKey]));
        console.log('**********\n\r');
      } // for (const jsonKey in this.selectorJson)
      console.log('\n\r**********');
      console.log(funcName + 'All scrapped data,  courseScrappedData = ' + JSON.stringify(courseScrappedData));
      console.log('**********\n\r');
      if (s) {
        await s.close();
      }
      // return final scrapped data
      // return polishedCourseScrappedData;
      return courseScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  */

 static async scrapeCourseDetails(courseDict) {
  const funcName = 'scrapeCourseDetails ';
  let s = null;
  try {
    s = new Scrape();
    await s.init({ headless: true });
    await s.setupNewBrowserPage(courseDict.href);

    Scrape.validateParams([courseDict]);
    // get course href and innerText from courseDict and ensure it is valid
    if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
      console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
      throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
    }
    const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
    console.log(funcName + 'courseTitle = ' + courseTitle);
    console.log(funcName + 'courseUrl = ' + courseUrl);
    // output course data and file path
    let courseScrappedData = null,retDict={};
    // create courseId from course title
    const courseId = await Course.removeAllWhiteSpcaesAndLowerCase(courseTitle);
    // validate generated courseId
    console.log(funcName + 'courseId = ' + courseId);
    if (!(courseId && courseId.length > 0)) {
      console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
      throw (new Error('Invalid courseId, courseId = ' + courseId));
    }
    // configure output file path as per UpdateItem configuration
    const opCourseFilePath = await Course.generateOutputCourseFilePath(courseDict);
    // decide if need to scrape or read data from local outpurfile
    if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
      console.log(funcName + 'reading scrapped data from local output file...');
      const fileData = fs.readFileSync(opCourseFilePath);
      if (!fileData) {
        console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
        throw (new Error('Invalid fileData, fileData = ' + fileData));
      }
      courseScrappedData = JSON.parse(fileData);
      return courseScrappedData;
    } // if (appConfigs.shouldTakeFromOutputFolder)
    // Scrape course data as per selector file
    if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
      console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
    } else {
      console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
      courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
    }
    if (!courseScrappedData) {
      throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
    }
    retDict.courseDict=courseScrappedData;
    retDict.href=await s.page.url();
    if (s) {
      await s.close();
    }
    return retDict;
    // return finalScrappedData;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    if (s) {
      await s.close();
    }
    return null;
    // throw (error);
  }
}

static async removeAllWhiteSpcaesAndLowerCase(val) {
  const funcName = 'removeAllWhiteSpcaesAndLowerCase ';
  try {
    if (!(val && val.length > 0)) {
      console.log(funcName + 'Invalid prama, val = ' + val);
      throw (new Error('Invalid prama, val = ' + val));
    }
    // console.log(funcName + 'val  before operation = ' + val);
    const trimmedVal = String(val).trim();
    // console.log(funcName + 'trimmedVal = ' + trimmedVal);
    const regex = /\s/g;
    const wsRemovedVal = String(trimmedVal).replace(regex, '');
    let ucVal = String(wsRemovedVal).toLowerCase();
    // remove all / slassh from file name else it wiil be considered as dir seperator
    while (String(ucVal).includes('/')) {
      ucVal = String(ucVal).replace('/', '');
      console.log(funcName + 'ucVal = ' + ucVal);
    }
    // console.log(funcName + 'val  after operation = ' + ucVal);
    return ucVal;
  } catch (error) {
    console.log(funcName + 'try-catch error = ', error);
    throw (error);
  }
}

// generate opCourseFilePath
static async generateOutputCourseFilePath(courseDict) {
  const funcName = 'generateOutputCourseFilePath ';
  try {
    let opCourseFilePath = null;
    if (!courseDict) {
      console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
      throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
    }
    // create courseId from course title
    const courseId = await Course.removeAllWhiteSpcaesAndLowerCase(courseDict.innerText);
    // validate generated courseId
    console.log(funcName + 'courseId = ' + courseId);
    if (!(courseId && courseId.length > 0)) {
      console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
      throw (new Error('Invalid courseId, courseId = ' + courseId));
    }
    // eslint-disable-next-line no-path-concat
    console.log(funcName + '__dirname = ' + __dirname);
    console.log(funcName + 'path.dirname = ' + path.dirname(__dirname));
    if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
      // opCourseFilePath = configs.outputDirPath + configs.univ_id + '_' + courseId + '_update_coursedetails.json';
      opCourseFilePath = path.join(path.dirname(__dirname), configs.outputDirPath + configs.univ_id + '_' + courseId + '_update_coursedetails.json');
    } else {
      // opCourseFilePath = configs.outputDirPath + configs.univ_id + '_' + courseId + '_coursedetails.json';
      opCourseFilePath = path.join(path.dirname(__dirname), configs.outputDirPath + configs.univ_id + '_' + courseId + '_coursedetails.json');
    }
    // console.log(funcName + 'opCourseFilePath = ' + opCourseFilePath);
    // const normalizedFilePath = path.normalize(opCourseFilePath);
    // console.log(funcName + 'normalizedFilePath = ' + normalizedFilePath);

    // const fileName = path.basename(opCourseFilePath);
    // console.log(funcName + 'fileName = ' + fileName);
    // const normFilePath = path.normalize(fileName);
    // console.log(funcName + 'normFilePath = ' + normFilePath);

    // const escapedFilePath = escape(fileName);
    // console.log(funcName + 'escapedFilePath = ' + escapedFilePath);

    return opCourseFilePath;
  } catch (error) {
    console.log(funcName + 'try-catch error = ', error);
    throw (error);
  }
}

}

// Course.KEYS_TO_FORMAT = ['univ_id', 'course_study_level', 'course_tuition_fee', 'course_discipline', 'course_admission_requirement', 
//   'course_url', 'course_cricos_code', 'course_study_mode', 'course_campus_location', 'course_duration_full_time', 'course_intake'];
  
Course.KEYS_TO_REMOVE = ['select_study_year_and_citizenship_type', 'select_english_as_ibt', 'select_english_as_ielts', 'select_english_as_pbt', 
  'course_academic_requirement', 'course_duration_full_time', 'course_duration_part_time', 'course_tuition_fees_year', 'course_tuition_fees_international_student',
  'course_tuition_fee_duration_years', 'course_tuition_fees_currency', 'select_course_tuition_fees_international_student', 'course_toefl_ibt_indicator',
  'course_toefl_ielts_score', 'course_toefl_toefl_pbt_score', 'page_url'];

// Course.COURSE_ELEMENT_TYPE = {
//   page_url: 'page_url',
//   course_study_level: 'course_study_level',
// };

Course.COURSE_DURATION_UNIT_TYPES_LIST = ['year', 'years', 'month', 'months'];
Course.COURSE_MONTHS_LIST = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

module.exports = { Course };
