const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const configs = require('./configs');


class ScrapeCourseList extends Scrape {
  // scrape course list of the univ
  static async scrapeCourseListForPages(courseItemSelectorList, firstPageSelector, pageURL) {
    const funcName = 'scrapeCourseListForPages ';
    let s = null; let courseList = null;
    try {
      // validate params
      Scrape.validateParams([courseItemSelectorList, firstPageSelector, pageURL]);
      console.log(funcName + 'courseItemSelectorList = ' + JSON.stringify(courseItemSelectorList));
      // create Scrape instance
      s = new Scrape();
      await s.init({ headless: false });
      await s.setupNewBrowserPage(pageURL);

      const intrationalbuttonclick = await s.page.$('#root > div > div.src-components-StudentNav-___studentNav__student-nav-container___1skxv > div > div > div:nth-child(1) > div.src-components-NationalitySelector-___nationalitySelector__nationality-wrapper___1i0e4 > section > div.src-components-NationalitySelector-___nationalitySelector__buttons___znX-m > button:nth-child(2)');
      await intrationalbuttonclick.click();
      await s.page.waitFor(5000);
      const intrationalbuttonclick1 = await s.page.$('#root > div > div.src-components-StudentNav-___studentNav__student-nav-container___1skxv > div > div > div:nth-child(1) > div.src-components-YearSelector-___yearSelector__year-wrapper___X53lj > button');
      await intrationalbuttonclick1.click();
      await s.page.waitFor(5000);
      const intrationalbuttonclick2 = await s.page.$('#root > div > div.src-components-StudentNav-___studentNav__student-nav-container___1skxv > div > div > div:nth-child(1) > div.src-components-YearSelector-___yearSelector__year-wrapper___X53lj > section > div.src-components-YearSelector-___yearSelector__buttons___2xoyX > button:nth-child(1)');
      await intrationalbuttonclick2.click();
      courseList = [];
      const activePaginationItemSel = firstPageSelector;
      let pageCount = 1;
      let nextSiblingNodeName = true;
      do {
        await s.page.waitFor(3000);
        // wait for sel
        console.log(funcName + 'waitForSelector sel = ' + activePaginationItemSel);
        await s.page.waitForSelector(activePaginationItemSel, { visible: true });
        // scrape all course list items
        console.log(funcName + '\n\r ********** Scraping for page  ' + pageCount + ' ********** \n\r');
        const resList = [];

        console.log(funcName + 'waitForSelector sel = ' + courseItemSelectorList[0]);
        await s.page.waitForSelector(courseItemSelectorList[0]);

        const anchorEleHandle = await s.page.$(courseItemSelectorList[0]); // anchor element handle
        console.log(funcName + 'anchorEleHandle = ' + anchorEleHandle);

        const hrefList = await s.scrapeAnchorElement(courseItemSelectorList[0], null, '');
        // console.log(funcName + 'hrefList = ' + JSON.stringify(hrefList));
        for (const itemDict of hrefList) {
          const courseDict = {};
          courseDict.href = itemDict.href;
          const itemInnerText = itemDict.innerText;
          const innerTextList = String(itemInnerText).replace('\n', ' ');
          console.log(funcName + 'innerTextList = ' + JSON.stringify(innerTextList));
          // if (innerTextList && innerTextList.length > 0) {
          courseDict.innerText = innerTextList;
          // }
          console.log(funcName + 'courseDict = ' + JSON.stringify(courseDict));
          resList.push(courseDict);
        } // for

        courseList = courseList.concat(resList);
        pageCount += 1;
        console.log(funcName + 'courseList count = ' + courseList.length);
        const link = await s.page.$('#root > div > div.container > div > div > div > div.main-content > div > div > div:nth-child(4) > div.src-components-SearchListing-___searchListing__pagination___W8NJN > ul > li.ais-Pagination__item.ais-Pagination__itemNext > a');
        if (link != null) {
          await link.click();
        }
        else {
          nextSiblingNodeName = false;
        }
      } while (nextSiblingNodeName);

      console.log(funcName + 'total pages scrapped = ' + pageCount);
      console.log(funcName + 'courseList count = ' + courseList.length);
      // console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
      if (s) {
        await s.close();
      }
      return courseList;
    } catch (error) {
      console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r\n\r');
      console.log(funcName + 'try-catch error = ' + error);
      console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r\n\r');
      if (s) {
        await s.close();
      }
      console.log(funcName + 'writing courseList to tmp file....');
      await fs.writeFileSync(configs.opCourseListTempFilepath, JSON.stringify(courseList));
      console.log(funcName + 'writing courseList to tmp file completed successfully....');
      throw (error);
    }
  }

  // scrape course page list of the univ
  async scrapeCourseListAndPutAtS3(selFilepath) {
    const funcName = 'scrapeCourseListAndPutAtS3 ';
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      const pageUrl = this.selectorJson.url;
      console.log(funcName + 'url = ' + pageUrl);
      const jsonDictElements = this.selectorJson.elements;
      console.log(funcName + 'elements = ' + JSON.stringify(jsonDictElements));
      let courseList = null;
      if (Array.isArray(jsonDictElements)) {
        for (const eleDict of jsonDictElements) {
          const eleType = eleDict.elementType;
          console.log(funcName + 'elementType = ' + eleType);
          // ensure type is matching
          if (eleType === Scrape.ELEMENT_TYPE.COURSE_LIST) {
            const courseItemSelector = eleDict.course_item_selector;
            console.log(funcName + 'course_item_selector = ' + courseItemSelector);
            const pageSelector = eleDict.page_selector;
            console.log(funcName + 'page_selector = ' + pageSelector);
            courseList = await ScrapeCourseList.scrapeCourseListForPages(courseItemSelector, pageSelector, pageUrl);
          } // if
        } // for elements
      } // if
      console.log(funcName + 'writing courseList to file....');
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(courseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
    
      return courseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  // scrape course category list wise
  async scrapeCourseListCategorywise() {
    const funcName = 'scrapeCourseListCategorywise ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];
      await s.setupNewBrowserPage("https://www.mq.edu.au/");
      console.log('scrape course category wise list');


    const categoryselectorUrl = "body > main > section.courses > div > div > div > div.col-7.md-col-9.sm-col-12 > ul > li > a";
    const categoryselectorText = "body > main > section.courses > div > div > div > div.col-7.md-col-9.sm-col-12 > ul > li > a";
    var elementstring = "", elementhref = "", allcategory = [];

    const targetLinksCardsUrls = await s.page.$$(categoryselectorUrl);
    const targetLinksCardsText = await s.page.$$(categoryselectorText);

    var targetLinksCardsTotal = [];
    for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        elementstring = await s.page.evaluate(el => el.innerText, targetLinksCardsText[i]);
        elementhref = await s.page.evaluate(el => el.href, targetLinksCardsUrls[i]);
        targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
    }
    // await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
    console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));

    let linkselector = "body > main > section.bg-sand.aoi > div > div > ul.boxes-white > li > div > div > div > div > div > table > tbody > tr > td > a";
    let textselector = "body > main > section.bg-sand.aoi > div > div > ul.boxes-white > li > div > div > div > div > div > table > tbody > tr > td > a"
    var totalCourseList = [];
    for (let target of targetLinksCardsTotal) {
        await s.page.goto(target.href, { timeout: 0 });
        //scrape university courselist
        await s.page.waitFor(2000);
        let check = "#international-student";
        await s.page.evaluate((check) => document.querySelector(check).click(), check);
        // let check = "#filter-graduate";
        // await page.evaluate((check) => document.querySelector(check).click(), check);
        let subCategoryLinks = "body > main > section.bg-sand.aoi > div:nth-child(2) > div > ul.boxes-white > li > a";
        const subCategoryLinksScrapped = await s.page.$$(subCategoryLinks);
        for (let i = 0; i < subCategoryLinksScrapped.length; i++) {
            // elementhref = await page.evaluate(el => el.href, subCategoryLinks[i]);
            await subCategoryLinksScrapped[i].click();
            await s.page.waitFor(5000);
            let studylevelselector="/html/body/main/section/div/div/ul/li/div/div/div/div/div/table/tbody/tr/th[1]"
            const targets= await s.page.$x(studylevelselector)
            for(let k=0;k<targets.length;k++){
              var studylevelstring = await s.page.evaluate(el => el.innerText, targets[k]);
              console.log("Study_level@@@",studylevelstring)
              var courseselector="/html/body/main/section/div/div/ul/li/div/div/div/div/div/table/tbody/tr/th[contains(text(),'" + studylevelstring + "')]/./../following-sibling::tr/td/a"
              const targetLinks = await s.page.$x(courseselector);
              for (var j = 0; j < targetLinks.length; j++) {
                var elementstring = await s.page.evaluate(el => el.innerText, targetLinks[j]);
                const elementlink = await s.page.evaluate(el => el.href, targetLinks[j]);
                let payload = { href: elementlink, innerText: elementstring, category: target.innerText, study_level:studylevelstring }
                console.log("PAyLoad",payload)
                let res = totalCourseList.some(value => {
                    if (value.href == payload.href && value.category == payload.category) {
                        return true;
                    } else {
                        return false
                    }
                });
                if (res == false) {
                    totalCourseList.push(payload);
                } else {
                    console.log("Record Exists -- >", payload);
                }
            }
            }
          }
        }
            
            
     
    console.log("totalCourseList -->", totalCourseList);
    await fs.writeFileSync("./course_list/main_category_courses.json", JSON.stringify(targetLinksCardsTotal))
    await fs.writeFileSync("./course_list/macquarieuniversity_original_courselist.json", JSON.stringify(totalCourseList));

    let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [],study_level:totalCourseList[i].study_level });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [],study_level:totalCourseList[i].study_level});
        }
      }

      await fs.writeFileSync("./course_list/macquarieuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./course_list/macquarieuniversity_courselist.json", JSON.stringify(uniqueUrl));

      
    

      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
