const fs = require('fs');

const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, study_level) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = course_category;
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const penglishList = [];
                            const ieltsScore1 = "";
                            let ibtScore = "";
                            let pteScore = "";
                            let caeScore = "";
                         
                            let ieltsScore;
                            const ieltsDict = {}; const ibtDict = {}; const pteDict = {}; const caeDict = {}; const cpeDict = {}; let ieltsNumber = null;
                            // english requirement
                            let myscore = '';
                            console.log('tofel ielts course :' + courseScrappedData.course_toefl_ielts_score);
                            if (courseScrappedData.course_toefl_ielts_score) {
                                ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                                console.log("ieltsScore---->>>>", ieltsScore);

                                const ieltsMappingDict = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                                console.log('IELTS ieltsMappingDict :' + JSON.stringify(ieltsMappingDict));

                                if (ieltsScore && ieltsScore.length > 0) {

                                    if (ieltsScore.includes('5.5 overall with minimum 5.0')) {
                                        let ielts_one = ieltsMappingDict[0]

                                        console.log("ielts_one--->>>", ielts_one);
                                    }
                                    if (ieltsScore.includes('5.0 overall with minimum 5.0')) {

                                    }
                                    if (ieltsScore.includes('6.0 overall with minimum 6.0')) {

                                    }
                                    if (ieltsScore.includes('6.5 overall with minimum 6.0')) {
                                        // ieltsScore="Academic IELTS of 5.5 overall with minimum 5.0 in each band, or equivalent."
                                        // let ielts_4 = ieltsMappingDict.includes(ieltsScore);
                                        // console.log("ielts_4--->>>", ielts_4);
                                        // if (ielts_4) {
                                        //     ieltsScore1 = ielts_4.ielts;
                                        //     ibtScore = ielts_4.ibt;
                                        //     pteScore = ielts_4.pte;
                                        //     caeScore = ielts_4.cae;
                                        //     cpeScore = ielts_4.cpe;
                                        // }
                                        // console.log("ielts_444--->>>", ielts_4 + ieltsScore1 + ibtScore + pteScore + caeScore + cpeScore);
                                    }
                                    if (ieltsScore.includes('7.0 overall with minimum 6.0')) {

                                    }
                                    if (ieltsScore.includes('7.0 overall with minimum 6.5')) {

                                    }
                                    if (ieltsScore.includes('7.5 overall')) {

                                    }
                                    if (ieltsScore.includes('7.0 overall with minimum 7.0')) {

                                    }
                                    console.log('IELTS Score :' + ieltsScore);
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(ieltsScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ieltsNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                    }
                                }
                            }
                            // if ieltsNumber is valid then map IBT and PBT accordingly
                            if (ieltsNumber && ieltsNumber > 0) {

                                myscore = await utils.getMappingScore(ieltsScore.trim());
                                console.log("myscore : ", myscore);
                                if (myscore) {
                                    ieltsScore = myscore.ielts;
                                     ibtScore = myscore.ibt;
                                     pteScore = myscore.pte;
                                     caeScore = myscore.cae;
                                   
                                }

                               
                                            if (ieltsScore) {
                                                ieltsDict.name = 'ielts academic';
                                                ieltsDict.description = ieltsScore;
                                                ieltsDict.min = 0;
                                                ieltsDict.require = Number(await utils.giveMeNumber(ieltsScore));
                                                ieltsDict.max = 9;
                                                ieltsDict.R = 0;
                                                ieltsDict.W = 0;
                                                ieltsDict.S = 0;
                                                ieltsDict.L = 0;
                                                ieltsDict.O = 0;
                                                englishList.push(ieltsDict);
                                            }
                                            if (ibtScore) {
                                                ibtDict.name = 'toefl ibt';
                                                ibtDict.description = ibtScore;
                                                ibtDict.min = 0;
                                                ibtDict.require = Number(await utils.giveMeNumber(ibtScore));
                                                ibtDict.max = 120;
                                                ibtDict.R = 0;
                                                ibtDict.W = 0;
                                                ibtDict.S = 0;
                                                ibtDict.L = 0;
                                                ibtDict.O = 0;
                                                englishList.push(ibtDict);
                                            }
                                            if (pteScore) {
                                                pteDict.name = 'pte academic';
                                                pteDict.description = pteScore;
                                                pteDict.min = 0;
                                                pteDict.require = Number(await utils.giveMeNumber(pteScore));
                                                pteDict.max = 90;
                                                pteDict.R = 0;
                                                pteDict.W = 0;
                                                pteDict.S = 0;
                                                pteDict.L = 0;
                                                pteDict.O = 0;
                                                englishList.push(pteDict);
                                            }
                                            if (caeScore) {
                                                caeDict.name = 'cae';
                                                caeDict.description = caeScore;
                                                caeDict.min = 80;
                                                caeDict.require = Number(await utils.giveMeNumber(caeScore));;
                                                caeDict.max = 230;
                                                caeDict.R = 0;
                                                caeDict.W = 0;
                                                caeDict.S = 0;
                                                caeDict.L = 0;
                                                caeDict.O = 0;
                                                englishList.push(caeDict);
                                            }
                                            
                               
                            }


                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }
                            else {
                                courseAdminReq.english = [];
                            }
                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                // const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                // console.log('\n\r');
                                // console.log(funcName + 'academicReq = ' + academicReq);
                                // console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                // console.log('\n\r');
                                // if (academicReq && String(academicReq).length > 0) {
                                //     courseAdminReq.academic = academicReq;
                                // }
                                console.log(funcName + 'matched case course_academic_requirment: ' + key);
                                const courseKeyVal = courseScrappedData.course_academic_requirement;
                                var course_academic_requirment = []
                                console.log("course_academic_requirement details -->" + JSON.stringify(courseKeyVal));
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict course_academic_requirement = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict course_academic_requirement = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList course_academic_requirement = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    course_academic_requirment = selList;
                                                }
                                            }
                                        }
                                    }
                                } // rootElementDictList
                                // add only if it has valid value
                                if (course_academic_requirment.length > 0) {
                                    courseAdminReq.academic = course_academic_requirment;
                                }
                            }
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }

                            const courseKeyValEntryReq = courseScrappedData.course_entry_requirements_url_more_details;
                            var resEntryReqMoreDetailsJson = "";
                            if (Array.isArray(courseKeyValEntryReq)) {
                                for (const rootEleDict of courseKeyValEntryReq) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEntryReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))

                            const courseKeyVal = courseScrappedData.course_admission_english_more_details;
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))

                            // add english requirement 'english_more_details' link
                            const courseKeyValAcademic = courseScrappedData.course_admission_academic_more_details;
                            if (courseKeyValAcademic != null && courseKeyValAcademic != "") {
                                var resAcademicReqMoreDetailsJson = "";
                                if (Array.isArray(courseKeyValAcademic)) {
                                    for (const rootEleDict of courseKeyValAcademic) {
                                        console.log(funcName + '\n\r rootEleDict courseKeyValAcademic= ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict courseKeyValAcademic= ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList courseKeyValAcademic= ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    resAcademicReqMoreDetailsJson = selList[0];
                                                    console.log('course url academic :' + resAcademicReqMoreDetailsJson);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                const courseKeyValAcademic_not_found = courseScrappedData.course_admission_academic_more_details;
                                var resAcademicReqMoreDetailsJson = "";
                                if (Array.isArray(courseKeyValAcademic_not_found)) {
                                    for (const rootEleDict of courseKeyValAcademic_not_found) {
                                        console.log(funcName + '\n\r rootEleDict courseKeyValAcademic_not_found= ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict courseKeyValAcademic_not_found= ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList courseKeyValAcademic_not_found= ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    resAcademicReqMoreDetailsJson = selList[0];
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // add english requirement 'english_more_details' link
                            const courseKeyValenglish_url = courseScrappedData.english_requirements_url;
                            let resEnglishReqMoreURLDetailsJson = null;
                            if (Array.isArray(courseKeyValenglish_url)) {
                                for (const rootEleDict of courseKeyValenglish_url) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreURLDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.entry_requirements_url = resEntryReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqMoreURLDetailsJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;

                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};

                                resJsonData.course_admission_requirement.entry_requirements_url = resEntryReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqMoreURLDetailsJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
                            }
                            break;
                        }
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));

                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);

                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student_first_year': {
                            const feesDict = {};
                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_first_year);
                            console.log('consol first year tution fee' + feesIntStudent);
                            if (feesIntStudent != null) {
                                if (feesIntStudent.length > 0) { // extract only digits
                                    console.log("international_student" + feesIntStudent)
                                    feesDict.international_student = feesIntStudent;
                                    const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                    console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                    const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                                    console.log(funcName + 'feesVal = ' + feesVal);
                                    if (feesVal) {
                                        const myval = feesVal.split(".");
                                        const regEx = /\d/g;
                                        let feesValNum = myval[0].match(regEx);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum course_tuition_fees_international_student_first_year= ' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum course_tuition_fees_international_student_first_year= ' + feesValNum);
                                            let feesNumber = null;
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }
                                            console.log(funcName + 'feesNumber course_tuition_fees_international_student_first_year= ' + feesNumber);
                                            if (Number(feesNumber)) {
                                                feesDict.international_student_first_year = Number(feesNumber);
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                                const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                                console.log(funcName + 'cricoseStr tution fee international tution = ' + cricoseStr);
                                console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                                if ((!feesDict.international_student_first_year) && cricoseStr && cricoseStr.length > 0) {
                                    const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                    if (fixedFeesDict) {
                                        for (const codeKey in fixedFeesDict) {
                                            console.log(funcName + 'codeKey = ' + codeKey);
                                            if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                                const keyVal = fixedFeesDict[codeKey];
                                                console.log(funcName + 'keyVal course_tuition_fees_international_student_first_year= ' + keyVal);
                                                if (cricoseStr.includes(codeKey)) {
                                                    const feesDictVal = fixedFeesDict[cricoseStr];
                                                    console.log(funcName + 'feesDictVal course_tuition_fees_international_student_first_year= ' + feesDictVal);
                                                    if (feesDictVal && feesDictVal.length > 0) {
                                                        const inetStudentFees = Number(feesDictVal);
                                                        console.log(funcName + 'inetStudentFees course_tuition_fees_international_student_first_year= ' + inetStudentFees);
                                                        if (Number(inetStudentFees)) {
                                                            feesDict.international_student_first_year = Number(inetStudentFees);
                                                        }
                                                    }
                                                }
                                            }
                                        } // for
                                    } // if
                                } // if
                            }
                            break;
                        }
                        case 'course_tuition_fees_international_student': {
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {
                              
                            };
                            //for first year only
                            console.log('***********************tution fee**************************');
                            const feesIntStudent_first_year = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_first_year);
                            console.log('feesIntStudent_first_year :' + feesIntStudent_first_year);
                            console.log('macquery conditon put fee not found');
                            if (feesIntStudent_first_year != null) {
                                if (feesIntStudent_first_year.length > 0) { // extract only digits
                                    console.log("course tuition fees international student" + feesIntStudent_first_year)
                                    const feesWithDollorTrimmed = String(feesIntStudent_first_year).trim();

                                    var isfulltimeflag = false;
                                    var durationfee = 1;
                                    var unitfee = "Year";
                                    console.log('course duration fee :' + feesIntStudent_first_year);
                                    if (String(feesIntStudent_first_year).toLowerCase().indexOf('annual fee') > -1) {
                                        durationfee = 1;
                                        unitfee = "Year";
                                        if (fullTimeText.indexOf('annual fee') > -1) {
                                            isfulltimeflag = true;
                                        }
                                    }

                                    console.log(funcName + 'feesWithDollorTrimmed course tuition fees international student= ' + feesWithDollorTrimmed);
                                    const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                                    console.log(funcName + 'feesVal course tuition fees international student= ' + feesVal);
                                    if (feesVal) {
                                        const myval = feesVal.split(".");
                                        const regEx = /\d/g;
                                        let feesValNum = myval[0].match(regEx);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum course tuition fees international student= ' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum course tuition fees international student= ' + feesValNum);
                                            let feesNumber = null;
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }

                                            if (Number(feesNumber)) {
                                                feesDict.international_student={
                                                    amount: Number(feesNumber),
                                                    duration: durationfee,
                                                    unit: unitfee,                                                   
                                                    description: feesIntStudent_first_year,
                                                   
                                                };

                                            }                                           
                                        }
                                    }
                                }

                                console.log('course tuition fees international student value');
                                // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                                const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                                console.log(funcName + 'cricoseStr tution fee = ' + cricoseStr);
                                console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                                if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                    const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                    if (fixedFeesDict) {
                                        for (const codeKey in fixedFeesDict) {
                                            console.log(funcName + 'codeKey = ' + codeKey);
                                            if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                                const keyVal = fixedFeesDict[codeKey];
                                                console.log(funcName + 'keyVal = ' + keyVal);
                                                if (cricoseStr.includes(codeKey)) {
                                                    const feesDictVal = fixedFeesDict[cricoseStr];
                                                    console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                    if (feesDictVal && feesDictVal.length > 0) {
                                                        const inetStudentFees = Number(feesDictVal);
                                                        console.log(funcName + 'inetStudentFees = ' + inetStudentFees);

                                                        if (Number(inetStudentFees)) {
                                                            feesDict.international_student={
                                                                amount: Number(inetStudentFees),
                                                                duration: 1,
                                                                unit: "Year",                                                              
                                                                description: inetStudentFees,
                                                              
                                                            };
                                                        }                                                     
                                                    }
                                                }
                                            }
                                        } // for
                                    } // if
                                } // if

                                //for all fees
                                const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                                const courseKeyVal = courseScrappedData.course_tuition_fees_international_student;
                                let feesIntStudent = null;
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    feesIntStudent = selList;
                                                }
                                            }
                                        }
                                    }
                                }
                                // if (feeYear && feeYear.length > 0) {
                                //     courseTuitionFee.year = feeYear;
                                // }
                                if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                    const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                    const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                    if (feesDuration && feesDuration.length > 0) {
                                        feesDict.fee_duration_years = feesDuration;
                                    }
                                    if (feesCurrency && feesCurrency.length > 0) {
                                        feesDict.currency = feesCurrency;
                                    }
                                    // if (feesIntStudent.length > 0) { // extract only digits
                                    //     console.log("international_student" + feesIntStudent)
                                    //     feesDict.international_student_all_fees = feesIntStudent;
                                    // }
                                    if (feesDict) {
                                        var campus = resJsonData.course_campus_location;
                                        for (let loc of campus) {
                                            feesList.push({ name: loc.name, value: feesDict });
                                        }
                                    }
                                    if (feesList && feesList.length > 0) {
                                        courseTuitionFee.fees = feesList;
                                    }
                                    console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                    if (courseTuitionFee) {
                                        resJsonData.course_tuition_fee = courseTuitionFee;
                                        //resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                    }
                                }
                                else {
                                    feesDict.international_student = "";
                                    
                                    // feesDict.international_student_all_fees.push("");
                                    if (feesDict) {
                                        var campus = resJsonData.course_campus_location;
                                        for (let loc of campus) {
                                            feesList.push({ name: loc.name, value: feesDict });
                                        }
                                    }
                                    if (feesList && feesList.length > 0) {
                                        courseTuitionFee.fees = feesList;
                                    }
                                    if (courseTuitionFee) {
                                        //resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                        resJsonData.course_tuition_fee = courseTuitionFee;
                                    }
                                }
                            }
                            else {
                                const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                                console.log(funcName + 'cricoseStr tution fee international tution = ' + cricoseStr);
                                console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));

                                if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                    const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                    if (fixedFeesDict) {
                                        for (const codeKey in fixedFeesDict) {
                                            console.log(funcName + 'codeKey = ' + codeKey);
                                            if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                                const keyVal = fixedFeesDict[codeKey];
                                                console.log(funcName + 'keyVal course_tuition_fees_international_student_first_year= ' + keyVal);
                                                if (cricoseStr.includes(codeKey)) {
                                                    const feesDictVal = fixedFeesDict[cricoseStr];
                                                    console.log(funcName + 'feesDictVal course_tuition_fees_international_student_first_year= ' + feesDictVal);
                                                    if (feesDictVal && feesDictVal.length > 0) {
                                                        const inetStudentFees = Number(feesDictVal);
                                                        console.log(funcName + 'inetStudentFees course_tuition_fees_international_student_first_year= ' + inetStudentFees);
                                                        // if (Number(inetStudentFees)) {
                                                        //     feesDict.international_student = Number(inetStudentFees);
                                                        // }
                                                        if (Number(inetStudentFees)) {
                                                            feesDict.international_student={
                                                                amount: Number(inetStudentFees),
                                                                duration: 1,
                                                                unit: "Year",                                                                
                                                                description: inetStudentFees
                                                               
                                                            };
                                                        }
                                                    }
                                                }
                                            }
                                        } // for
                                    } // if
                                } // if
                                let tutionfeeAll = [];
                                if (cricoseStr && cricoseStr.length > 0) {
                                    const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed_more_details');
                                    if (fixedFeesDict) {
                                        for (const codeKey in fixedFeesDict) {
                                            console.log(funcName + 'codeKey more details = ' + codeKey);
                                            if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                                const keyVal = fixedFeesDict[codeKey];
                                                console.log(funcName + 'keyVal more details =' + keyVal);
                                                if (cricoseStr.includes(codeKey)) {
                                                    const feesDictVal = fixedFeesDict[cricoseStr];
                                                    console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                    if (feesDictVal && feesDictVal.length > 0) {
                                                        const inetStudentFees = feesDictVal;
                                                        console.log(funcName + 'internation tution fee more details = ' + inetStudentFees);
                                                        tutionfeeAll.push(inetStudentFees);
                                                    }
                                                }
                                            }
                                        } // for
                                    } // if
                                } // if

                                //for all fees
                                const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                                // if (feeYear && feeYear.length > 0) {
                                //     courseTuitionFee.year = feeYear;
                                // }
                                if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                    const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                    const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                    if (feesDuration && feesDuration.length > 0) {
                                        feesDict.fee_duration_years = feesDuration;
                                    }
                                    if (feesCurrency && feesCurrency.length > 0) {
                                        feesDict.currency = feesCurrency;
                                    }
                                   
                                    if (feesDict) {
                                        var campus = resJsonData.course_campus_location;
                                        for (let loc of campus) {
                                            feesList.push({ name: loc, value: feesDict });
                                        }
                                    }
                                    if (feesList && feesList.length > 0) {
                                        courseTuitionFee.fees = feesList;
                                    }
                                    console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                    if (courseTuitionFee) {
                                        resJsonData.course_tuition_fee = courseTuitionFee;
                                        //resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                    }
                                }

                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                if (courseTuitionFee) {
                                    //resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;
                        }

                        case 'course_study_mode': { // Location Launceston                          
                            resJsonData.course_study_mode = "On campus";
                            // resJsonData.course_study_mode = await utils.giveMeArray(coursestudymodeValTrimmed.replace(/[\r\n\t]+/g, ' '), ',');                             

                            break;
                        }
                        case 'course_campus_location': { // Location Launceston
                            const courseKeyVal = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            const campLocationText = await Course.extractLocationValueFromScrappedElement(courseScrappedData.course_campus_location);
                            console.log(funcName + 'campLocationText = ' + campLocationText);
                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = String(campLocationText).trim();
                                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                if (campLocationValTrimmed == "Sydney, CBD") {
                                    campLocationValTrimmed = "Sydney CBD"
                                    console.log("YESSSS", campLocationValTrimmed)
                                }
                                if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    var campuses = await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ''), ',');
                                    var campusedata = [];
                                    campuses.forEach(element => {
                                        campusedata.push({
                                            "name": element,
                                            "code": String(course_cricos_code)
                                        })
                                    });
                                    resJsonData.course_campus_location = campusedata;
                                    // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //

                                }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_intake': {
                            var courseIntakeStr = [];
                            //existing intake value
                            let courseIntakeStr1 = courseScrappedData.course_intake
                            console.log("courseIntakeStr1 -->    >", courseIntakeStr1);
                            if (Array.isArray(courseIntakeStr1)) {
                                for (const rootEleDict of courseIntakeStr1) {
                                    console.log(funcName + '\n\r rootEleDict course_intake = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = courseIntakeStr1' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                for (let sel of selList) {
                                                    if (sel.indexOf('(') > -1) {
                                                        let intakeVal = sel.split('(')[1].replace(/[\r\n\t()]+/g, '');
                                                        courseIntakeStr.push(intakeVal);
                                                    }
                                                }
                                                // courseIntakeStr1 = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log('changes intake date :' + courseIntakeStr);


                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                let intakes = [];
                                for (let campus of resJsonData.course_campus_location) {
                                    var intakedetail = {};
                                    intakedetail.name = campus.name;
                                    intakedetail.value = courseIntakeStr;
                                    intakes.push(intakedetail);
                                }

                                let formatIntake = await format_functions.providemyintake(intakes, "mom", "");

                                var intakedata = {};
                                intakedata.intake = formatIntake;
                                intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url')
                                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
                            } // if (courseIntakeStr && courseIntakeStr.length > 0)
                            break;
                        }

                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }

                        case 'course_country':
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            console.log(funcName + 'matched case course_career_outcome: ' + key);
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            var course_career_outcome = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            } // rootElementDictList
                            // add only if it has valid value
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            }
                            else {
                                resJsonData.course_career_outcome = course_career_outcome;
                            }
                            break;
                        }

                        case 'program_code': {
                            console.log(funcName + 'matched case program_code: ' + key);
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = []
                            var pcode
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("program_code===>",program_code)
                            if(program_code){
                                if(String(program_code).includes(",")){
                                    var splitdata=String(program_code).split(",")[0]
                                    console.log("program_code===>",splitdata)
                                    if(splitdata.includes('(')){
                                        pcode=splitdata.split('(')[0]
                                    }else{
                                        pcode=String(splitdata)
                                    }

                                }else{
                                    pcode= String(program_code)
                                }
                            }
                            resJsonData.program_code = pcode
                            break;
                        }
                        case 'course_title': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr course title = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr.replace("\n", " ");
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).replace("\n", " ").concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr course title = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr course title = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr course title = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            }
                            break;
                        }
                        case 'course_study_level': {
                            // const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            let cStudyLevel = study_level;
                            console.log(funcName + 'course_title course_study_level = ' + cStudyLevel);

                            if (cStudyLevel) {
                                resJsonData.course_study_level = cStudyLevel;
                            }

                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                            const courseKeyVal_major =courseScrappedData.course_outline;
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                            let course_outlines = {};
                            console.log("MAjor==>", JSON.stringify(courseKeyVal_major))
                            if (courseKeyVal_minor != null) {
                                course_outlines.minors = courseKeyVal_minor
                            } else {
                                course_outlines.minors = []
                            }
                            if (courseKeyVal_major) {
                                let course_career_outcome = null;
                                if (Array.isArray(courseKeyVal_major)) {
                                    for (const rootEleDict of courseKeyVal_major) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    course_career_outcome = selList;
                                                }
                                            }
                                        }
                                    }
                                }
                                if(course_career_outcome!=null){
                                    course_outlines.majors = course_career_outcome
                                }else{
                                    course_outlines.majors = []
                                }
                              
                            }
                            if (courseKeyVal != null) {
                                course_outlines.more_details = courseKeyVal
                            } else {
                                course_outlines.more_details = ""
                            }
                            resJsonData.course_outline = course_outlines
                            break;
                        }
                        case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            if (courseKeyVal != null) {
                                resJsonData.application_fee = courseKeyVal
                            } else {
                                resJsonData.application_fee = ""
                            }
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)

            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_title))
            
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
               
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
             
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;


                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid.replace(/[\n :""]+/g, '').replace(/[\/]+/g, '_') + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };









