const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');

const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to https://www.deakin.edu.au/courses/find-a-course
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });


      // course_level_selector key
      const courseListSel = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!courseListSel) {
        console.log(funcName + 'Invalid courseListSel');
        throw (new Error('Invalid courseListSel'));
      }
      const elementDictForCourseListSel = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCourseListSel) {
        console.log(funcName + 'Invalid elementDictForCourseListSel');
        throw (new Error('Invalid elementDictForCourseListSel'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);

      //await s.page.waitForSelector('input[value="International"]', { timeout: 10000, visible: true })

      const rdcheck = await s.page.$('input[value="International"]');
      await rdcheck.click();
      console.log('*****************************Wait for selector********************');
      await s.page.waitFor(7000);
      console.log('*****************************Wait for End selector*******************');
      const totalCourseList = await s.scrapeElement(elementDictForCourseListSel.elementType, courseListSel, rootEleDictUrl, null);
      console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));

      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');

      if (s) {
        await s.close();
      }
      return totalCourseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // refers to https://www.deakin.edu.au/courses/find-a-course
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }


      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');

      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // scrape course category list wise
  async scrapeCourseListCategorywise() {
    const funcName = 'scrapeCourseListCategorywise ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];

      await s.setupNewBrowserPage("https://www.deakin.edu.au/courses/find-a-course");
      var mainCategory = [], redirecturl = [];
      const internation = "//*[@class='flexContainer']//fieldset/ul/li[2]/label";
      var clickarea11 = await s.page.$x(internation);
      await clickarea11[0].click();
      console.log("internations-->");
      //scrape main category
      const maincategoryselector = "//*[@id='lhs']//fieldset//li/label";

      var category = await s.page.$x(maincategoryselector);
      console.log("Total categories-->" + category.length);
      for (let link of category) {

        var ispage_ug = true;
        var ispage_pg = true;
        var ispage_rs = true;
        var categorystringmain = await s.page.evaluate(el => el.innerText, link);
        mainCategory.push(categorystringmain.replace(/[\r\n\t ]+/g, ' ').trim());
        console.log("---------------" + categorystringmain + "-----------------");

        await link.click();
        await s.page.waitFor(2000);

        // UnderGraduate
        const UnderGraduateBtnSel = "//*//ul[@class='course-type-nav']//li/label[text()='Undergraduate']";
        var underGradBtn = await s.page.$x(UnderGraduateBtnSel);
        await underGradBtn[0].click();
        while (ispage_ug) {

          const selector = "//*/div[@id='undergrad_course_resultset_wrapper']//tbody/tr/td[1]/a";
          // const study_lavel_val_ug = "//*//ul[@class='course-type-nav']//li/label[text()='Undergraduate']";

          var title = await s.page.$x(selector);
          var titles = await s.page.$x(UnderGraduateBtnSel)

          for (let t of title) {
            var categorystring = await s.page.evaluate(el => el.innerText, t);
            var categoryurl = await s.page.evaluate(el => el.href, t);
            var studylevel = await s.page.evaluate(el => el.innerText, titles[0])
            totalCourseList.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: categorystringmain.replace(/[\r\n\t ]+/g, ' ').trim(), study_level: studylevel });
          }
          const activeselector = "//*[@id='undergrad_course_resultset_paginate']/span/a[@class='paginate_button current']";
          const active = await s.page.$x(activeselector);
          if (active[0]) {
            const next = await s.page.evaluateHandle(el => el.nextElementSibling, active[0]);
            if (next) {
              console.log(next.toString())
            }

            if (next && next.toString() != "JSHandle:null") {
              var nodename = await (await next.getProperty('nodeName')).jsonValue();
              console.log("Next-->" + nodename)
              if (nodename == "A") {
                await next.click();
                await s.page.waitFor(5000);
              }
              else {
                ispage_ug = false;
              }
            }
            else {
              ispage_ug = false;
              console.log("null node-->")
            }
          }
          else {
            // console.log("no active-->" + active[0].toString())
            ispage_ug = false;
          }
        }

        // PostGraduate
        const postGraduateBtnSel = "//*//ul[@class='course-type-nav']//li/label[text()='Postgraduate']";
        var postGradBtn = await s.page.$x(postGraduateBtnSel);
        await postGradBtn[0].click();
        while (ispage_pg) {

          const selector = "//*//div[@id='postgrad_course_resultset_wrapper']//tbody/tr/td[1]/a";
          var title = await s.page.$x(selector);
          var titles = await s.page.$x(postGraduateBtnSel)

          for (let t of title) {
            var categorystring = await s.page.evaluate(el => el.innerText, t);
            var categoryurl = await s.page.evaluate(el => el.href, t);
            var studylevel = await s.page.evaluate(el => el.innerText, titles[0]);
            totalCourseList.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: categorystringmain.replace(/[\r\n\t ]+/g, ' ').trim(), study_level: studylevel });
          }
          const activeselector = "//*[@id='postgrad_course_resultset_paginate']/span/a[@class='paginate_button current']";
          const active = await s.page.$x(activeselector);
          if (active[0]) {
            const next = await s.page.evaluateHandle(el => el.nextElementSibling, active[0]);
            if (next) {
              console.log(next.toString())
            }

            if (next && next.toString() != "JSHandle:null") {
              var nodename = await (await next.getProperty('nodeName')).jsonValue();
              console.log("Next-->" + nodename)
              if (nodename == "A") {
                await next.click();
                await s.page.waitFor(5000);
              }
              else {
                ispage_pg = false;
              }
            }
            else {
              ispage_pg = false;
              console.log("null node-->")
            }
          }
          else {
            // console.log("no active-->" + active[0].toString())
            ispage_pg = false;
          }
        }


        // Research
        const ResearchBtnSel = "//*//ul[@class='course-type-nav']//li/label[text()='Research']";
        var ResearchBtn = await s.page.$x(ResearchBtnSel);
        await ResearchBtn[0].click();
        while (ispage_rs) {

          const selector = "//*//div[@id='research_course_resultset_wrapper']//tbody/tr/td[1]/a";

          var title = await s.page.$x(selector);
          var titles = await s.page.$x(ResearchBtnSel)

          for (let t of title) {
            var categorystring = await s.page.evaluate(el => el.innerText, t);
            var categoryurl = await s.page.evaluate(el => el.href, t);
            var studylevel = await s.page.evaluate(el => el.innerText, titles[0])
            totalCourseList.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: categorystringmain.replace(/[\r\n\t ]+/g, ' ').trim(), study_level: studylevel });
          }
          const activeselector = "//*[@id='research_course_resultset_paginate']/span/a[@class='paginate_button current']";
          const active = await s.page.$x(activeselector);
          if (active[0]) {
            const next = await s.page.evaluateHandle(el => el.nextElementSibling, active[0]);
            if (next) {
              console.log(next.toString())
            }

            if (next && next.toString() != "JSHandle:null") {
              var nodename = await (await next.getProperty('nodeName')).jsonValue();
              console.log("Next-->" + nodename)
              if (nodename == "A") {
                await next.click();
                await s.page.waitFor(5000);
              }
              else {
                ispage_rs = false;
              }
            }
            else {
              ispage_rs = false;
              console.log("null node-->")
            }
          }
          else {
            // console.log("no active-->" + active[0].toString())
            ispage_rs = false;
          }
        }
        await link.click();
      }

      await fs.writeFileSync("./course_list/main_category_courses.json", JSON.stringify(mainCategory))
      await fs.writeFileSync("./course_list/deakinuniversity_original_courselist.json", JSON.stringify(totalCourseList));


      // fs.writeFileSync("./output/universityofcanberra_courselist.json", JSON.stringify(datalist))
      // fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))

      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
        }
      }

      await fs.writeFileSync("./course_list/deakinuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./course_list/deakinuniversity_courselist.json", JSON.stringify(uniqueUrl));

      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
