const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, study_level) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'course_discipline': {
              // As of not we do not have way to get this field value
              resJsonData.course_discipline = course_category;
              break;
            }
            case 'course_outline': {
              const outline = {
                majors: [],
                minors: [],
                more_details: courseUrl


              };
              console.log("outline------>", outline)
              resJsonData.course_outline = outline;
            }
            case 'application_fee': {
              resJsonData.application_fee = "55";
            }
            case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              const courseAdminReq = {};
              const englishList = [];
              var otherLngDict = null, ieltsDic = null;
              const ieltsDict = {}; const ibtDict = {}; const pteDict = {}; const utasDict = {}; let ieltsNumber = null;
              // english requirement
              if (courseScrappedData.course_toefl_ielts_score) {
                var ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                console.log("Ielts not found :" + ieltsScore);
                if (ieltsScore != "null") {
                  if (ieltsScore && ieltsScore.length > 0) {
                    const ieltsmoreScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_ielts_more_score);
                    console.log("IELTS Course :" + ieltsmoreScore);
                    var ieltsscorenew = String(ieltsScore).trim();
                    var splitIeltsscore = ieltsscorenew.split('More information is available');
                    if (splitIeltsscore.length > 1) {
                      ieltsScore = String(splitIeltsscore[0]).trim();
                    }
                    ieltsDict.name = 'ielts academic';
                    ieltsDict.description = ieltsScore;
                    ieltsDic = ieltsDict;
                    englishList.push(ieltsDict);
                    // extract exact number from string
                    const regEx = /[+-]?\d+(\.\d+)?/g;
                    const matchedStrList = String(ieltsScore).match(regEx);
                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                    if (matchedStrList && matchedStrList.length > 0) {
                      ieltsNumber = Number(matchedStrList[0]);
                      console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                    }
                  }
                }
              }
              ///progress bar start
              const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
              var penglishList = [];
              const potherLngDict = otherLngDict;
              if (ieltsNumber && ieltsNumber > 0) {
                var ieltsScore = "NA", ibtScore = "NA", pteScore = "NA";
                if (ieltsDic.description) {
                  ieltsScore = await utils.giveMeNumber(ieltsDic.description);
                  console.log("### IELTS data-->" + ieltsScore);
                }
                //  else{
                //ieltsScore = await utils.giveMeNumber(ieltsDic.description.split("of")[1]);
                console.log("### IELTS datdgdfga-->" + ieltsScore);
                //  }
                if (ieltsScore != "NA") {
                  pieltsDict.name = 'ielts academic';
                  pieltsDict.description = ieltsDict.description;
                  pieltsDict.min = 0;
                  pieltsDict.require = Number(ieltsScore);
                  pieltsDict.max = 9;
                  pieltsDict.R = 0;
                  pieltsDict.W = 0;
                  pieltsDict.S = 0;
                  pieltsDict.L = 0;
                  pieltsDict.O = 0;
                  penglishList.push(pieltsDict);
                }
              }
              if (penglishList && penglishList.length > 0) {
                courseAdminReq.english = penglishList;
              }
              else {
                courseAdminReq.english = [];
              }
              if (courseScrappedData.course_academic_requirement) {
                console.log(funcName + 'matched case course_academic_requirment: ' + key);
                var flagacademic = false;
                const courseKeyVal = courseScrappedData.course_academic_requirement;
                var course_academic_requirment = [];
                console.log("course_academic_requirement details -->" + JSON.stringify(courseKeyVal));
                if (Array.isArray(courseKeyVal)) {
                  for (const rootEleDict of courseKeyVal) {
                    console.log(funcName + '\n\r rootEleDict course_academic_requirement = ' + JSON.stringify(rootEleDict));
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                      console.log(funcName + '\n\r eleDict course_academic_requirement = ' + JSON.stringify(eleDict));
                      const selectorsList = eleDict.selectors;
                      for (const selList of selectorsList) {
                        console.log(funcName + '\n\r selList course_academic_requirement = ' + JSON.stringify(selList));
                        if (Array.isArray(selList) && selList.length > 0) {
                          course_academic_requirment.push(selList);
                        }
                      }
                    }
                  }
                } // rootElementDictList
                // add only if it has valid value
                var academiArray = [];
                console.log('Array of academic requirment :' + course_academic_requirment.length);
                for (var i = 0; i < course_academic_requirment.length; i++) {
                  console.log('inner academic requirment :' + course_academic_requirment[i].length);
                  for (var j = 0; j < course_academic_requirment[i].length; j++) {
                    academiArray.push(course_academic_requirment[i][j]);
                  }
                }
                if (academiArray.length > 0) {
                  flagacademic = false;
                }
                else {
                  flagacademic = true;
                }
                if (flagacademic) {
                  const courseKeyValone = courseScrappedData.course_academic_requirement_one;
                  var course_academic_requirment_one = [];

                  if (Array.isArray(courseKeyValone)) {
                    for (const rootEleDict of courseKeyValone) {
                      console.log(funcName + '\n\r rootEleDict course_academic_requirement = ' + JSON.stringify(rootEleDict));
                      const elementsList = rootEleDict.elements;
                      for (const eleDict of elementsList) {
                        console.log(funcName + '\n\r eleDict course_academic_requirement = ' + JSON.stringify(eleDict));
                        const selectorsList = eleDict.selectors;
                        for (const selList of selectorsList) {
                          console.log(funcName + '\n\r selList course_academic_requirement = ' + JSON.stringify(selList));
                          if (Array.isArray(selList) && selList.length > 0) {
                            course_academic_requirment_one.push(selList);
                          }
                        }
                      }
                    }
                  } // rootElementDictList
                  // add only if it has valid value
                  console.log('Array of academic requirment :' + course_academic_requirment_one.length);
                  for (var i = 0; i < course_academic_requirment_one.length; i++) {
                    academiArray.push(course_academic_requirment_one[i]);
                  }
                }
                let newAcademicArr = [];
                for (let academic_val of academiArray) {
                  if (newAcademicArr.length > 0) {
                    if (!newAcademicArr.includes(academic_val)) {
                      newAcademicArr.push(academic_val);
                    }
                  } else {
                    newAcademicArr.push(academic_val);
                  }
                }
                courseAdminReq.academic = newAcademicArr;
              }
              // if courseAdminReq has any valid value then only assign it
              if (courseAdminReq.english || courseAdminReq.academic) {
                resJsonData.course_admission_requirement = courseAdminReq;
              }
              const courseKeyValEntryReq = courseScrappedData.course_entry_requirements_url_more_details;
              var resEntryReqMoreDetailsJson = "";
              if (Array.isArray(courseKeyValEntryReq)) {
                for (const rootEleDict of courseKeyValEntryReq) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resEntryReqMoreDetailsJson = selList[0];
                      }
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))
              const courseKeyVal = courseScrappedData.course_admission_english_more_details;
              var resEnglishReqMoreDetailsJson = "";
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resEnglishReqMoreDetailsJson = selList[0];
                      }
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))
              // add english requirement 'english_more_details' link
              const courseKeyValAcademic = courseScrappedData.course_admission_academic_more_details;
              var resAcademicReqMoreDetailsJson = "";
              if (Array.isArray(courseKeyValAcademic)) {
                for (const rootEleDict of courseKeyValAcademic) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAcademicReqMoreDetailsJson = selList[0];
                      }
                    }
                  }
                }
              }
              // add english requirement 'english_more_details' link
              const courseKeyValenglish_url = courseScrappedData.english_requirements_url;
              var resEnglishReqMoreURLDetailsJson = "";
              if (Array.isArray(courseKeyValenglish_url)) {
                for (const rootEleDict of courseKeyValenglish_url) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resEnglishReqMoreURLDetailsJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                resJsonData.course_admission_requirement.entry_requirements_url = resEntryReqMoreDetailsJson;
                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqMoreURLDetailsJson;
                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;

              } else if (resEnglishReqMoreDetailsJson) {
                resJsonData.course_admission_requirement = {};
                resJsonData.course_admission_requirement.entry_requirements_url = resEntryReqMoreDetailsJson;
                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqMoreURLDetailsJson;
                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
              }
              break;
            }
            case 'course_url': {
              let newUrl = courseScrappedData.course_url;
              let rescourse_url = null;
              if (Array.isArray(newUrl)) {
                for (const rootEleDict of newUrl) {
                  console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));

                      rescourse_url = selList;

                    }
                  }
                }
              }
              if (rescourse_url) {
                resJsonData.course_url = rescourse_url;
              }
              break;
            }
            case 'course_duration_full_time': {
              // display full time
              var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = "";
              let duration_value = await utils.getValueFromHardCodedJsonFile('duration_mapping');
              var fullTimeText = "";
              const courseKeyVal = courseScrappedData.course_duration_full_time;
              console.log('Duartion values :' + courseKeyVal);
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        fullTimeText = selList[0];
                      }
                    }
                  }
                }
              }
              console.log("duration values :" + fullTimeText);
              if (fullTimeText && fullTimeText.length > 0) {
                const resFulltime = fullTimeText;
                if (resFulltime) {
                  durationFullTime = resFulltime;
                  courseDurationList = durationFullTime;
                  courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                  console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                  let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                  if (courseDurationList && courseDurationList.length > 0) {
                    resJsonData.course_duration = courseDurationList;
                  }
                  if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                    resJsonData.course_duration_display = filtered_duration_formated;
                    var isfulltime = false, isparttime = false;
                    filtered_duration_formated.forEach(element => {
                      if (element.display == "Full-Time") {
                        isfulltime = true;
                      }
                      if (element.display == "Part-Time") {
                        isparttime = true;
                      }
                    });
                    resJsonData.isfulltime = isfulltime;
                    resJsonData.isparttime = isparttime;
                  }
                }
              }
              else {
                resJsonData.course_duration_display = [];
                resJsonData.course_duration = [];
              }
              break;
            }
            case 'course_tuition_fee':
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_duration_part_time': {
              console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
              // No need to process anything, its already processed in other case.
              break;
            }
            case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};
              const feesList = [];
              const feesDict = {
                international_student: {}
              };
              var feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
              console.log('tution fee amount :' + feesIntStudent);
              // if (feesIntStudent == null)
              // {
              //   feesDict.international_student = ({
              //     amount: 0,
              //     duration: 1,
              //     unit: "Year",
              //     description: "",
              //   });
              //   console.log("ENTERED-->");
              // }
              if (feesIntStudent != null) {

                // if we can extract value as Int successfully then replace Or keep as it is
                if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                  const feesWithDollorTrimmed = String(feesIntStudent).trim();
                  console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                  var isfulltimeflag = false;
                  var durationfee = 1;
                  var unitfee = "Year";
                  console.log('course duration fee :' + feesIntStudent);
                  if (String(feesIntStudent).toLowerCase().indexOf('annual fee') > -1) {
                    durationfee = 1;
                    unitfee = "Year";
                    if (fullTimeText.indexOf('annual fee') > -1) {
                      isfulltimeflag = true;
                    }
                  }
                  const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                  const arrval = String(feesVal1).split('.');
                  const feesVal = String(arrval[0]);
                  console.log(funcName + 'feesVal = ' + feesVal);
                  var feesvalArr = feesVal.toString().split('for');
                  if (feesvalArr[0]) {
                    const regEx = /\d/g;
                    let feesValNum = feesvalArr[0].match(regEx);
                    if (feesValNum) {
                      console.log(funcName + 'feesValNum = ' + feesValNum);
                      feesValNum = feesValNum.join('');
                      console.log(funcName + 'feesValNum = ' + feesValNum);
                      let feesNumber = null;
                      if (feesValNum.includes(',')) {
                        feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                      } else {
                        feesNumber = feesValNum;
                      }
                      console.log(funcName + 'feesNumber = ' + feesNumber);
                      if (Number(feesNumber)) {
                        feesDict.international_student = ({
                          amount: Number(feesNumber),
                          duration: durationfee,
                          unit: unitfee,
                          description: feesIntStudent,
                        });
                      }
                    } else {
                      feesDict.international_student = ({
                        amount: 0,
                        duration: 1,
                        unit: "Year",
                        description: "",
                      });
                    }
                  }
                }// if (feesIntStudent && feesIntStudent.length > 0)
                // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                console.log(funcName + 'cricoseStr = ' + cricoseStr);
                console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                if ((feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                  const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                  if (fixedFeesDict) {
                    for (const codeKey in fixedFeesDict) {
                      console.log(funcName + 'codeKey = ' + codeKey);
                      if (fixedFeesDict.hasOwnProperty(codeKey)) {
                        const keyVal = fixedFeesDict[codeKey];
                        console.log(funcName + 'keyVal = ' + keyVal);
                        if (cricoseStr.includes(codeKey)) {
                          const feesDictVal = fixedFeesDict[cricoseStr];
                          console.log(funcName + 'feesDictVal = ' + feesDictVal);
                          if (feesDictVal && feesDictVal.length > 0) {
                            const inetStudentFees = Number(feesDictVal);
                            console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                            var isfulltimeflag = false;
                            var durationfee = 1;
                            var unitfee = "Year";
                            if (Number(inetStudentFees)) {
                              feesDict.international_student = ({
                                amount: Number(inetStudentFees),
                                duration: durationfee,
                                unit: unitfee,
                                description: inetStudentFees,
                              });
                            }
                          }
                          else {
                            feesDict.international_student = ({
                              amount: 0,
                              duration: 1,
                              unit: "Year",

                              description: "",

                            });
                          }
                        }
                      }
                    } // for
                  } // if
                } else {
                  feesDict.international_student = ({
                    amount: 0,
                    duration: 1,
                    unit: "Year",
                    description: "",
                  });
                }
                if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                  const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                  const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                  if (feesDuration && feesDuration.length > 0) {
                    feesDict.fee_duration_years = feesDuration;
                  }
                  if (feesCurrency && feesCurrency.length > 0) {
                    feesDict.currency = feesCurrency;
                  }
                  if (feesIntStudent.length > 0) { // extract only digits
                    console.log("international_student" + feesIntStudent)
                    const courseKeyVal = courseScrappedData.course_tuition_fees_international_student_more_details;
                    console.log('value of feee :' + courseKeyVal);
                    if (Array.isArray(courseKeyVal)) {
                      for (const rootEleDict of courseKeyVal) {
                        console.log(funcName + '\n\r rootEleDict fee more= ' + JSON.stringify(rootEleDict));
                        const elementsList = rootEleDict.elements;
                        for (const eleDict of elementsList) {
                          console.log(funcName + '\n\r eleDict fee more= ' + JSON.stringify(eleDict));
                          const selectorsList = eleDict.selectors;
                          for (const selList of selectorsList) {
                            console.log(funcName + '\n\r selList fee more= ' + JSON.stringify(selList));
                            if (Array.isArray(selList) && selList.length > 0) {
                              // feesDict.international_student_all_fees = selList[0];
                              console.log('course fee more details :' + selList[0]);
                            }
                          }
                        }
                      }
                    }
                  }
                  if (feesDict) {
                    var campus = resJsonData.course_campus_location;
                    for (let loc of campus) {
                      feesList.push({ name: loc.name, value: feesDict });
                    }
                  }
                  if (feesList && feesList.length > 0) {
                    courseTuitionFee.fees = feesList;
                  }
                  console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                  if (courseTuitionFee) {
                    resJsonData.course_tuition_fee = courseTuitionFee;
                  }
                  // take tuition fee value at json top level so will help in DDB for indexing
                  if (courseTuitionFee && feesDict.international_student) {
                    resJsonData.course_tuition_fee = courseTuitionFee;
                    //resJsonData.course_tuition_fee_amount = feesDict.international_student;
                  }
                }
              }
              else {
                feesDict.international_student = ({
                  amount: 0,
                  duration: 1,
                  unit: "Year",
                  description: "",
                });
                const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                if (feeYear && feeYear.length > 0) {
                  courseTuitionFee.year = feeYear;
                }
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }
                feesDict.international_student = ({
                  amount: 0,
                  duration: 1,
                  unit: "Year",
                  description: "",
                });
                //feesDict.international_student_all_fees = [];
                if (feesDict) {
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                if (courseTuitionFee) {
                  //resJsonData.course_tuition_fee_amount = feesDict.international_student;
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
              }
              console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
              // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
              // if (feesDict.international_student.length == 0) {
              //   console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
              //   console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
              //   console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
              //   console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
              //   return null; // this will add this item into FailedItemList and writes file to local disk
              // }
              break;
            }
            case 'course_campus_location': { // Location Launceston
              var campLocationText = await Course.extractLocationValueFromScrappedElement(courseScrappedData.course_campus_location);
              console.log(funcName + 'campLocationText = ' + campLocationText);
              if (campLocationText == null) {
                console.log("entering---->")
                throw new Error("This course is available online only !!!!")
              }
              const cricc = await Course.extractLocationValueFromScrappedElement(courseScrappedData.course_cricos_code);
              console.log("cricc-->", cricc);

              if (campLocationText != null) {
                if (campLocationText && campLocationText.length > 0) {
                  var campLocationValTrimmed = String(campLocationText).trim();
                  console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                  // if(campLocationValTrimmed.includes('Campus'))
                  // {
                  //   // if(campLocationValTrimmed.includes(',')){
                  //   //   location_val = String(campLocationValTrimmed).split(',');
                  //   // }

                  //    location_val = [String(campLocationValTrimmed).replace('Campus','').replace('Campus','').trim()];
                  //    console.log("new location-->",location_val);
                  if (campLocationValTrimmed.includes(',')) {
                    location_val = campLocationValTrimmed.split(',');
                    console.log("location_val--->", location_val);

                    var campusedata = [];
                    location_val.forEach(element => {
                      campusedata.push({
                        "name": this.titleCase(element),
                        "code": cricc
                      })
                    });

                    resJsonData.course_campus_location = campusedata;
                    break;
                  }
                  if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                    var location_val = await utils.giveMeArray(campLocationValTrimmed.replace('*', '').replace(/[\r\n\t]+/g, ''), ',');

                    if (location_val.includes(',')) {
                      location_val3 = String(location_val).split(',');
                      console.log("location34333--->", location_val3);
                    }


                    // resJsonData.course_campus_location = await utils.giveMeArray(campLocationValTrimmed.replace('*', '').replace(/[\r\n\t]+/g, ''), ',');

                    if (location_val && location_val.length > 0) {
                      var campusedata = [];
                      location_val.forEach(element => {
                        campusedata.push({
                          "name": this.titleCase(element),
                          "code": cricc
                        })
                      });
                      resJsonData.course_campus_location = campusedata;
                      console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);
                    }
                    else {
                      throw new Error("No campus location found.");
                    }
                    var studymode = "On Campus";
                    resJsonData.course_study_mode = studymode;
                  }
                } // if (campLocationText && campLocationText.length > 0)
              }
              else {
                const courseKeyVal = courseScrappedData.course_campus_location_other;
                console.log('Course validation for user :' + courseKeyVal);
                let campLocationOtherText = null;
                if (Array.isArray(courseKeyVal)) {
                  for (const rootEleDict of courseKeyVal) {
                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                      console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                      const selectorsList = eleDict.selectors;
                      for (const selList of selectorsList) {
                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        if (Array.isArray(selList) && selList.length > 0) {
                          campLocationOtherText = selList[0];
                        }
                      }
                    }
                  }
                }
                // const campLocationOtherText = await Course.extractLocationValueFromScrappedElement(courseScrappedData.course_campus_location_other);
                console.log(funcName + 'campLocationOtherText = ' + campLocationOtherText);
                if (campLocationOtherText && campLocationOtherText.length > 0) {
                  console.log('campuse location not found :' + campLocationOtherText)
                  var spliteLocation = String(campLocationOtherText).split('studies at');

                  if (spliteLocation.length > 1) {
                    console.log('IF length of split location :' + spliteLocation.length);
                    var campLocationValTrimmed = String(spliteLocation[1]).trim();
                    console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                    if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                      // resJsonData.course_campus_location = await utils.giveMeArray(campLocationValTrimmed.replace('*', '').replace(/[\r\n\t]+/g, ''), ',');
                      const location_val = await utils.giveMeArray(campLocationValTrimmed.replace('.', '').replace('*', '').replace(/[\r\n\t]+/g, ''), ',');

                      if (location_val && location_val.length > 0) {
                        var campusedata = [];
                        location_val.forEach(element => {
                          campusedata.push({
                            "name": this.titleCase(element),
                            "code": cricc
                          })
                        });
                        resJsonData.course_campus_location = campusedata;
                        console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);
                      }
                      else {
                        throw new Error("No campus location found.");
                      }
                      var studymode = "On campus";
                      resJsonData.course_study_mode = await utils.giveMeArray(studymode.replace(/[\r\n\t]+/g, ' '), ',');
                    }
                  }
                  else {
                    console.log('************************start location not found********************');

                    console.log('Else length of split location :' + spliteLocation.length);
                    var campLocationValTrimmed = String(spliteLocation[0]).trim();
                    console.log(funcName + 'campLocationValTrimmed else = ' + campLocationValTrimmed);
                    if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                      const location_val = await utils.giveMeArray(campLocationValTrimmed.replace('.', '').replace('*', '').replace(/[\r\n\t]+/g, ''), ',');
                      if (location_val && location_val.length > 0) {
                        var campusedata = [];
                        location_val.forEach(element => {
                          campusedata.push({
                            "name": this.titleCase(element),
                            "code": cricc
                          })
                        });
                        resJsonData.course_campus_location = campusedata;
                        console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);
                      }
                      else {
                        throw new Error("No campus location found.");
                      }
                      var studymode = "On campus";
                      resJsonData.course_study_mode = await utils.giveMeArray(studymode.replace(/[\r\n\t]+/g, ' '), ',');
                    }
                    console.log('************************end location not found********************');
                  }
                }
              }
              break;
            }
            case 'course_intake_url': {
              break;
            }
            case 'course_intake': {
              var campus = resJsonData.course_campus_location;
              let intakeVal = await utils.getValueFromHardCodedJsonFile('intake_dates');
              let intake_url = await utils.getValueFromHardCodedJsonFile('intake_url');
              let multiIntake = intakeVal.split(';');
              let newValIntake = [];
              var courseIntakeStr = [];

              // existing intake value
              const courseIntakeStr1 = await Course.extractdurationValueFromScrappedElement(courseScrappedData.course_intake);
              console.log('Course intake json :' + JSON.stringify(courseIntakeStr1));
              console.log('Course intake :' + courseIntakeStr1);
              if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                resJsonData.course_intake = courseIntakeStr1;
                const intakeStrList = String(courseIntakeStr1).split('Start date:');
                console.log(funcName + 'intakeStrList course_intake = ' + JSON.stringify(intakeStrList));
                courseIntakeStr = intakeStrList;

                ///process intake 
                var intakeprocess = [];
                var intakestring = '';
                for (var x = 0; x < courseIntakeStr.length; x++) {
                  if (courseIntakeStr[x] != '') {
                    intakestring += courseIntakeStr[x].trim() + ";";
                  }
                }
                intakestring = intakestring.slice(0, -1);
                console.log('String intake :' + intakestring)
                intakeprocess.push(intakestring);
                console.log('String intake Array :' + intakeprocess.length)
                console.log("intakes" + JSON.stringify(courseIntakeStr));
                if (intakeprocess && intakeprocess.length > 0) {

                  console.log('Campuse location :' + campus);
                  var intakes = [];
                  console.log("Intake length-->" + intakeprocess.length);
                  console.log("Campus length-->" + campus.length);
                  var processdate = intakeprocess.toString().split(';');
                  console.log('process date :' + processdate.length);
                  for (var camcount = 0; camcount < campus.length; camcount++) {
                    console.log('process campus :' + camcount + JSON.stringify(campus[camcount]));
                    var nameofintake = '';
                    for (var i_count = 0; i_count < processdate.length; i_count++) {

                      var c_date = processdate[i_count].toString().split('Available at:');
                      if (c_date == '') {
                        c_date = '';
                      }
                      var splitlocation = c_date[1].toString().split(' ');
                      for (var s_count = 0; s_count < splitlocation.length; s_count++) {
                        if (splitlocation[s_count].toString().trim().indexOf('(') > -1) {
                          console.log('splite location array :' + splitlocation[s_count].toString().replace('(', '').replace(')', ''));
                          var locationname = splitlocation[s_count].toString().replace('(', '').replace(')', '');
                          if (campus[camcount].name.indexOf(locationname) > -1) {

                            nameofintake += c_date[0].toString().trim() + ";";
                          }
                        }
                      }
                      console.log('*******************end intake*******************')
                    }
                    nameofintake = nameofintake.slice(0, -1);
                    newValIntake.push(nameofintake);
                    // var intakedetail = {};
                    //intakedetail.campus = campus[count];
                    if (nameofintake && nameofintake.length > 0) {
                      var intakedetail = {};
                      console.log("CampusNAme@@@:-", campus[camcount])
                      if (nameofintake != "") {
                        console.log("nameofintake@@@:-", nameofintake)
                        intakedetail.name = campus[camcount].name;
                        intakedetail.value = await utils.giveMeArray(nameofintake.replace(/[\r\n\t ]+/g, ' '), ";");
                        console.log("intakedetail.name====>>>", intakedetail.name);
                      }
                      else{
                        console.log("nameofintake@@@:-", nameofintake)
                        intakedetail.name = campus[camcount].name;
                        intakedetail.value = []   
                      }
                      intakes.push(intakedetail);
                    }
                  }
                  var intakedata = {};
                  let formatIntake = await format_functions.providemyintake(intakes, "mom", "");
                  for (let date1 of formatIntake) {
                    for (let val of date1.value) {
                      for (let intake1 of multiIntake) {
                        if (intake1.toLowerCase().indexOf(val.actualdate.toLowerCase()) > -1) {
                          val.actualdate = (intake1.split('-')[1]).trim();
                        }
                      }

                    }
                  }

                  intakedata.intake = formatIntake;
                  intakedata.more_details = intake_url;
                  resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
                } // if (courseIntakeStr && courseIntakeStr.length > 0)
               }
              // else {
              //   var intakedata = {};
              //   for (var camcount = 0; camcount < campus.length; camcount++) {
              //     console.log('process campus :' + camcount + JSON.stringify(campus[camcount]));
              //     var intakedetail = {};
              //     intakedetail.name = campus[camc].name;
              //     intakedetail.value = [];
              //   }
              //   intakedata.intake = [intakedetail];
              //   intakedata.more_details = intake_url;
              //   resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
              // }

              break;
            }


            case 'univ_name': {
              const courseKeyVal = courseScrappedData.univ_name;
              let resUnivName = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivName = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivName) {
                resJsonData.univ_name = resUnivName;
              }
              break;
            }
            case 'univ_logo': {
              const courseKeyVal = courseScrappedData.univ_logo;
              let resUnivLogo = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivLogo = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivLogo) {
                resJsonData.univ_logo = resUnivLogo;
              }
              break;
            }

            case 'course_country':
            case 'course_overview': {
              console.log(funcName + 'matched case : ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr course_career_outcome= ' + concatnatedRootElementsStr);
              // add only if it has valid value
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
              }
              break;
            }
            case 'course_career_outcome': {
              console.log(funcName + 'matched case course_career_outcome: ' + key);
              const courseKeyVal = courseScrappedData.course_career_outcome;
              var course_career_outcome = []
              console.log("course_career_outcome-->" + JSON.stringify(courseKeyVal));
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        course_career_outcome = selList;
                      }
                    }
                  }
                }
              } // rootElementDictList
              // add only if it has valid value
              resJsonData.course_career_outcome = course_career_outcome;
              break;
            }
            // case 'course_cricos_code': {
            //   var courseKeyVal = courseScrappedData.course_cricos_code;
            //   console.log("criccbuzz-->",courseKeyVal);
            //   let course_cricos_code = null;
            //   if (Array.isArray(courseKeyVal)) {
            //     for (const rootEleDict of courseKeyVal) {
            //       console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
            //       const elementsList = rootEleDict.elements;
            //       for (const eleDict of elementsList) {
            //         console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
            //         const selectorsList = eleDict.selectors;
            //         for (const selList of selectorsList) {
            //           console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
            //           if (Array.isArray(selList) && selList.length > 0) {
            //             course_cricos_code = selList;
            //           }
            //         }
            //       }
            //     }
            //   }
            //   var locations = resJsonData.course_campus_location;
            //   var mycodes = [];
            //   if (course_cricos_code) {
            //     global.cricos_code = course_cricos_code[0];

            //     for (let location of locations) {
            //       console.log("location------->>", location);
            //       mycodes.push({
            //         location: location.name, code: course_cricos_code.toString(), iscurrent: false
            //       });
            //     }
            //     resJsonData.course_cricos_code = mycodes;
            //   }
            //   else {
            //     throw new Error("Cricos code not found");
            //   }
            //   break;
            // }
            case 'program_code': {
              console.log(funcName + 'matched case program_code: ' + key);
              const courseKeyVal = courseScrappedData.program_code;
              var program_code = []
              console.log("program_code-->" + JSON.stringify(courseKeyVal));
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        program_code = selList;
                      }
                    }
                  }
                }
              }
              for (let program_val of program_code) {
                resJsonData.program_code = program_val;
              }
              break;
            }
            case 'course_title': {
              var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)

              console.log("splitStr@@@2" + title);
              var ctitle = format_functions.titleCase(title).trim();
              // var ctitle2 = ctitle.replace(' ( ', '(');
              console.log("ctitle@@@", ctitle.trim());
              resJsonData.course_title = ctitle
              break;
            }
            // case 'course_title': {
            //   console.log(funcName + 'matched case: ' + key);
            //   const rootElementDictList = courseScrappedData[key];
            //   console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
            //   let concatnatedRootElementsStr = null;
            //   for (const rootEleDict of rootElementDictList) {
            //     console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
            //     const elementsList = rootEleDict.elements;
            //     let concatnatedElementsStr = null;
            //     for (const eleDict of elementsList) {
            //       console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
            //       const selectorsList = eleDict.selectors;
            //       let concatnatedSelectorsStr = null;
            //       for (const selList of selectorsList) {
            //         console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
            //         console.log(funcName + '\n\r selList = ' + selList);
            //         let concatnatedSelStr = null;
            //         for (const selItem of selList) {
            //           if (!concatnatedSelStr) {
            //             concatnatedSelStr = selItem;
            //           } else {
            //             concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
            //           }
            //         } // selList
            //         console.log(funcName + 'concatnatedSelStr course title = ' + concatnatedSelStr);
            //         if (concatnatedSelStr) {
            //           if (!concatnatedSelectorsStr) {
            //             concatnatedSelectorsStr = concatnatedSelStr.replace("\n", " ");
            //           } else {
            //             concatnatedSelectorsStr = String(concatnatedSelectorsStr).replace("\n", " ").concat(' ').concat(concatnatedSelStr).trim();
            //           }
            //         }
            //       } // selectorsList
            //       console.log(funcName + 'concatnatedSelectorsStr course title = ' + concatnatedSelectorsStr);
            //       // concat elements
            //       if (concatnatedSelectorsStr) {
            //         if (!concatnatedElementsStr) {
            //           concatnatedElementsStr = concatnatedSelectorsStr;
            //         } else {
            //           concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
            //         }
            //       }
            //     } // elementsList
            //     console.log(funcName + 'concatnatedElementsStr course title = ' + concatnatedElementsStr);
            //     if (concatnatedElementsStr) {
            //       if (!concatnatedRootElementsStr) {
            //         concatnatedRootElementsStr = concatnatedElementsStr;
            //       } else {
            //         concatnatedRootElementsStr = this.titleCase(String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim());
            //       }
            //     }
            //   } // rootElementDictList
            //   console.log(funcName + 'concatnatedRootElementsStr course title = ' + concatnatedRootElementsStr);
            //   // add only if it has valid value
            //   if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
            //     resJsonData[key] = String(concatnatedRootElementsStr).trim();
            //   }
            //   break;
            // }
            case 'course_study_level': {
              console.log(funcName + 'course_title course_study_level = ' + study_level);
              resJsonData.course_study_level = study_level.trim();
              break;
            }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      ////start genrating new file location wise
      var NEWJSONSTRUCT = {}, structDict = [];
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
        console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        }
        else {
          NEWJSONSTRUCT.course_intake = "";
        }
        for (let myfees of resJsonData.course_tuition_fee.fees) {
          if (myfees.name == location) {
            // NEWJSONSTRUCT.international_student_all_fees = myfees;
          }
        }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        resJsonData.course_id = location_wise_data.course_location_id;
        // resJsonData.basecourseid = location_wise_data.course_id;
        // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
        //   if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
        //     resJsonData.course_cricos_code[i].iscurrent = true;
        //   }
        //   else {
        //     resJsonData.course_cricos_code[i].iscurrent = false;
        //   }
        // }
        // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
        //   if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
        //     resJsonData.course_intake.intake[i].iscurrent = true;
        //   }
        //   else {
        //     resJsonData.course_intake.intake[i].iscurrent = false;
        //   }
        // }
        // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
        //   if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
        //     resJsonData.course_tuition_fee.fees[i].iscurrent = true;
        //   }
        //   else {
        //     resJsonData.course_tuition_fee.fees[i].iscurrent = false;
        //   }
        // }
        // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
        //   if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
        //     resJsonData.course_campus_location[i].iscurrent = true;
        //   }
        //   else {
        //     resJsonData.course_campus_location[i].iscurrent = false;
        //   }
        // }
        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
      }
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = 'scrapeAndFormatCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);
      //swithc to international student
      const switchInternational = await s.page.$x("//*[@id='main-content']//a[text()='Switch to international student information']");
      for (let link of switchInternational) {
        console.log('link :' + link);
        await link.click();
        await s.page.waitForNavigation();
      }
      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }
      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file
      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData (Information not given) = ' + courseScrappedData));
      }
      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.study_level);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails
  static titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      // You do not need to check if i is larger than splitStr length, as your for does that for you
      // Assign it back to the array
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
  }
} // class
module.exports = { ScrapeCourse };
