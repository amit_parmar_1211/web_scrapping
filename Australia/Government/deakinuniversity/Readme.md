**deakinuniversity : Total Courses Details As On 17 May 2019**
* Total Courses = 185
* Courses without CRICOS code = 53
* Total available courses = 185
* fail courses = 53

* Online Course not taken in selector 


[
    {
        "href": "https://www.deakin.edu.au/course/bachelor-exercise-and-sport-science-bachelor-nutrition-science-international",
        "innerText": "Bachelor of Exercise and Sport Science/Bachelor of Nutrition Science",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 47
    },
    {
        "href": "https://www.deakin.edu.au/course/bachelor-laws-honours-international",
        "innerText": "Bachelor of Laws (Honours)",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 64
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-agricultural-health-and-medicine-international",
        "innerText": "Graduate Certificate of Agricultural Health and Medicine",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 91
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-business-arts-and-cultural-management-international",
        "innerText": "Graduate Certificate of Business (Arts and Cultural Management)",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 92
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-business-sport-management-international",
        "innerText": "Graduate Certificate of Business (Sport Management)",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 93
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-cyber-security-international",
        "innerText": "Graduate Certificate of Cyber Security (Online)",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 98
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-data-analytics-international",
        "innerText": "Graduate Certificate of Data Analytics",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 99
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-data-science-international",
        "innerText": "Graduate Certificate of Data Science",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 100
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-development-humanitarian-action-international",
        "innerText": "Graduate Certificate of Development and Humanitarian Action",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 101
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-diabetes-education-international",
        "innerText": "Graduate Certificate of Diabetes Education",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 102
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-digital-learning-leadership-international",
        "innerText": "Graduate Certificate of Digital Learning Leadership",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 103
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-education-international",
        "innerText": "Graduate Certificate of Education",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 104
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-education-business-leadership-international",
        "innerText": "Graduate Certificate of Education Business Leadership",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 105
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-education-research-international",
        "innerText": "Graduate Certificate of Education Research",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 106
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-entrepreneurship-international",
        "innerText": "Graduate Certificate of Entrepreneurship",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 107
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-financial-planning-international",
        "innerText": "Graduate Certificate of Financial Planning",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 108
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-health-research-practice-international",
        "innerText": "Graduate Certificate of Health Research Practice",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 109
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-human-nutrition-international",
        "innerText": "Graduate Certificate of Human Nutrition",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 110
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-human-resource-management-international",
        "innerText": "Graduate Certificate of Human Resource Management",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 111
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-humanitarian-assistance-international",
        "innerText": "Graduate Certificate of Humanitarian Assistance",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 112
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-humanitarian-health-international",
        "innerText": "Graduate Certificate of Humanitarian Health",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 113
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-information-technology-future-learn-international",
        "innerText": "Graduate Certificate of Information Technology (Online)",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 116
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-information-technology-leadership-international",
        "innerText": "Graduate Certificate of Information Technology Leadership",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 117
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-international-and-community-development-international",
        "innerText": "Graduate Certificate of International and Community Development",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 120
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-leadership-international",
        "innerText": "Graduate Certificate of Leadership",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 121
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-museum-studies-international",
        "innerText": "Graduate Certificate of Museum Studies",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 123
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-property-international",
        "innerText": "Graduate Certificate of Property",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 126
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-public-health-nutrition-international",
        "innerText": "Graduate Certificate of Public Health Nutrition",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 127
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-certificate-sustainable-regional-development-international",
        "innerText": "Graduate Certificate of Sustainable Regional Development",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 128
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-diploma-business-arts-and-cultural-management-international",
        "innerText": "Graduate Diploma of Business (Arts and Cultural Management)",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 131
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-diploma-childrens-literature-international",
        "innerText": "Graduate Diploma of Children's Literature",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 134
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-diploma-financial-planning-international",
        "innerText": "Graduate Diploma of Financial Planning",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 140
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-diploma-human-nutrition-international",
        "innerText": "Graduate Diploma of Human Nutrition",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 142
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-diploma-journalism-international",
        "innerText": "Graduate Diploma of Journalism",
        "reason": "TypeError: locations is not iterable",
        "index": 148
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-diploma-literary-studies-international",
        "innerText": "Graduate Diploma of Literary Studies",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 149
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-diploma-museum-studies-international",
        "innerText": "Graduate Diploma of Museum Studies",
        "reason": "TypeError: locations is not iterable",
        "index": 151
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-diploma-property-international",
        "innerText": "Graduate Diploma of Property",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 155
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-diploma-public-relations-international",
        "innerText": "Graduate Diploma of Public Relations",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 157
    },
    {
        "href": "https://www.deakin.edu.au/course/graduate-diploma-visual-communication-design-international",
        "innerText": "Graduate Diploma of Visual Communication Design",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 159
    },
    {
        "href": "https://www.deakin.edu.au/course/master-business-arts-and-cultural-management-international",
        "innerText": "Master of Business (Arts and Cultural Management)",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 164
    },
    {
        "href": "https://www.deakin.edu.au/course/master-cyber-security-future-learn-international",
        "innerText": "Master of Cyber Security (Online)",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 176
    },
    {
        "href": "https://www.deakin.edu.au/course/master-development-and-humanitarian-action-international",
        "innerText": "Master of Development and Humanitarian Action",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 179
    },
    {
        "href": "https://www.deakin.edu.au/course/master-digital-learning-leadership-international",
        "innerText": "Master of Digital Learning Leadership",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 181
    },
    {
        "href": "https://www.deakin.edu.au/course/master-education-leadership-and-management-international",
        "innerText": "Master of Education (Leadership and Management)",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 183
    },
    {
        "href": "https://www.deakin.edu.au/course/master-human-nutrition-international",
        "innerText": "Master of Human Nutrition",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 188
    },
    {
        "href": "https://www.deakin.edu.au/course/master-human-resource-management-international",
        "innerText": "Master of Human Resource Management",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 189
    },
    {
        "href": "https://www.deakin.edu.au/course/master-information-technology-leadership-international",
        "innerText": "Master of Information Technology Leadership",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 195
    },
    {
        "href": "https://www.deakin.edu.au/course/master-international-and-community-development-international",
        "innerText": "Master of International and Community Development",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 198
    },
    {
        "href": "https://www.deakin.edu.au/course/master-leadership-international",
        "innerText": "Master of Leadership",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 200
    },
    {
        "href": "https://www.deakin.edu.au/course/master-nursing-practice-international",
        "innerText": "Master of Nursing Practice",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 202
    },
    {
        "href": "https://www.deakin.edu.au/course/master-politics-and-policy-international",
        "innerText": "Master of Politics and Policy",
        "reason": "Error: Invalid courseScrappedData, courseScrappedData = null",
        "index": 204
    },
    {
        "href": "https://www.deakin.edu.au/course/overseas-university-exchange-enrolment-arts-and-education-international",
        "innerText": "Overseas University Exchange Enrolment (Arts and Education)",
        "reason": "TypeError: locations is not iterable",
        "index": 253
    },
    {
        "href": "https://www.deakin.edu.au/course/study-abroad-program-business-and-law-international",
        "innerText": "Study Abroad Program (Business and Law)",
        "reason": "TypeError: locations is not iterable",
        "index": 255
    }
]

