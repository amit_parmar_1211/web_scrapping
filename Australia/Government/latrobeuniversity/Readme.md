**La Trobe University: Total Courses Details As On 13 May 2019**
* Total Courses = 485
* Courses without CRICOS code = 200
* DDB = 0
* Web portal available courses = 0



in Scrape course.js -> course_intake -> "2019" is use for split the intake data.
in university_hard_coded -> fees_more_details -> NA

Master of Laws (Research) ->course ouline not avaiable so it display "NA"









{
    "course_list_selector": [
       
        {
           
          "url": [
               
               "https://www.latrobe.edu.au/courses/study-arts-social-sciences-and-communications",
                "https://www.latrobe.edu.au/courses/study-business-and-commerce",
                "https://www.latrobe.edu.au/courses/study-education-teaching",
                "https://www.latrobe.edu.au/courses/study-health",
                "https://www.latrobe.edu.au/courses/study-it-and-engineering",
                "https://www.latrobe.edu.au/courses/study-law-criminology",
                "https://www.latrobe.edu.au/courses/study-science"
            ],
            "elements": [{
                "elementType": "scrape_list_diff",
                "selectors": [{
                        "action": "CLICK",
                        "isxpath": false,
                        "iscomb": false,
                        "remover": [],
                        "selector": "#overlay-content > ul > li:nth-child(2) button"
                    },
                    {
                        "action": "CLICK",
                        "isxpath": false,
                        "iscomb": false,
                        "remover": [],
                        "selector": "#discipline"
                    },
                    {
                        "action": "SCRAPE",
                        "isxpath": false,
                        "remover": [],
                        "iscomb": false,
                        "issame": true,
                        "selector": [{
                            "urlselector": "#ajax-course-list > article > h3 > a",
                            "textselector": "#ajax-course-list > article > h3 > a"
                        }]
                    }
                ]
            }]
        }
    ]
}

//*[@id="overview"]/div[3]/div/div/div/div/table/tbody/tr/th/a
 "//*[@id='ltu-content']/h4[contains(text(),'Current')]"