const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
    //   let select=await page.$x("/html/body/div[@id='saved-student-type']")
    // await select[0].click()
    // await page.waitFor(5000)
    let clicked=await page.$x("//*[@id='overlay-int']")
    await clicked[0].click()
    await page.waitFor(5000)
    const categoryselectorUrl = "#bodyContent > section.our-courses > div > div > ol > li:not([class*='need-help']) > a";
    const categoryselectorText = "#bodyContent > section.our-courses > div > div > ol > li:not([class*='need-help']) > a";


    // for (let i = 0; i < scrapedCategory.length; i++) {
    //   elementstring1 = await page.evaluate(el => el.innerText, scrapedCategory[i]);
    //   elementhref1 = await page.evaluate(el => el.href, scrapedCategory[i]);
    //   educationLevelCatList.push({ href: elementhref1, innerText: elementstring1 });
    // }

    var elementstring = "", elementhref = "", allcategory = [];

    const targetLinksCardsUrls = await page.$$(categoryselectorUrl);
    const targetLinksCardsText = await page.$$(categoryselectorText);


    var targetLinksCardsTotal = [];
    for (let i = 0; i < targetLinksCardsUrls.length; i++) {

      elementstring = await page.evaluate(el => el.innerText, targetLinksCardsText[i]);
      elementhref = await page.evaluate(el => el.href, targetLinksCardsUrls[i]);
      targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
    }
    // await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
    console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));


    let linkselector = "#ajax-course-list > article > h3 > a";
    let textselector = "#ajax-course-list > article > h3 > a"
    var totalCourseList = [];
    for (let target of targetLinksCardsTotal) {
      await page.goto(target.href, { timeout: 0 });
      let courseButtonClick = "//*[@id='btn-container']/a[1]";
      var clickbtn = await page.$x(courseButtonClick);
    
      console.log("Button");
      await clickbtn[0].click();

      console.log("Clicked");
      console.log("YESSSSSSS")
      await page.waitFor(15000)
      let educationLevelCat = "//*[@id='ug-button' or @id='pg-button' or @id='pgr-button']";
      let scrapedCategory = await page.$x(educationLevelCat);

      var educationLevelCatList = [];
      let elementstring1 = "";
      let elementhref1 = "";
      for (let i = 0; i < scrapedCategory.length; i++) {
        elementstring1 = await page.evaluate(el => el.innerText, scrapedCategory[i]);
        elementhref1 = await page.evaluate(el => el.href, scrapedCategory[i]);
        educationLevelCatList.push(elementstring1);
      }
       console.log("#### main courses---" + JSON.stringify(educationLevelCatList));




      // scrape university courselist study levelwise
      // let courseButtonClicks = "//a[contains(@title,'Courses')]";
      // var clickbtn = await page.$x(courseButtonClicks);
      // console.log("Button");
      // await clickbtn[0].click();
      // console.log("Clicked");
      // let check = "#filter-graduate";
      // await page.evaluate((check) => document.querySelector(check).click(), check);

      var links = await page.$x("//*[@id='ug-button']");
      // var level=await page.evaluate(el=>el.innerText,links)
      await links[0].click();
      // console.log("@@@@",links[0].innerText)
      console.log("Clicked")
      let ugselector = "//*[@id='ajax-course-list']/article[contains(@data-filters,'UG')]/h3/a";
      let ugtextselecor = "//*[@id='ajax-course-list']/article[contains(@data-filters,'UG')]/h3/a";

      await page.waitFor(5000);
      const ugtargetlinks = await page.$x(ugselector);
      const ugtargettext = await page.$x(ugtextselecor);
      for (var i = 0; i < ugtargetlinks.length; i++) {
        var ugelementstring = await page.evaluate(el => el.innerText, ugtargettext[i])
        const ugelementlink = await page.evaluate(el => el.href, ugtargetlinks[i])
        totalCourseList.push({ href: ugelementlink, innerText: ugelementstring, category: target.innerText, study_level: educationLevelCatList[0] });
      }

      var pglinks = await page.$x("//*[@id='pg-button']")
      await pglinks[0].click();
      console.log("Clicked pg")
      let pgselector = "//*[@id='ajax-course-list']/article[contains(@data-filters,'PG') and  not(contains(@data-filters,'PGR'))]/h3/a";
      let pgtextselecor = "//*[@id='ajax-course-list']/article[contains(@data-filters,'PG') and  not(contains(@data-filters,'PGR'))]/h3/a";
      await page.waitFor(5000);
      const pgtargetlinks = await page.$x(pgselector);
      const pgtargettext = await page.$x(pgtextselecor);

      for (var i = 0; i < pgtargetlinks.length; i++) {
        var pgelementstring = await page.evaluate(el => el.innerText, pgtargettext[i])
        const pgelementlink = await page.evaluate(el => el.href, pgtargetlinks[i])
        totalCourseList.push({ href: pgelementlink, innerText: pgelementstring, category: target.innerText, study_level: educationLevelCatList[1] });
      }

      var pgrlinks = await page.$x("//*[@id='pgr-button']")
      await pgrlinks[0].click();
      console.log("Clicked pgr")
      let pgrselector = "//*[@id='ajax-course-list']/article[contains(@data-filters,'PGR')]/h3/a";
      let pgrtextselecor = "//*[@id='ajax-course-list']/article[contains(@data-filters,'PGR')]/h3/a";
      await page.waitFor(5000);
      const pgrtargetlinks = await page.$x(pgrselector);
      const pgrtargettext = await page.$x(pgrtextselecor);

      for (var i = 0; i < pgrtargetlinks.length; i++) {
        var pgrelementstring = await page.evaluate(el => el.innerText, pgrtargettext[i])
        const pgrelementlink = await page.evaluate(el => el.href, pgrtargetlinks[i])
        totalCourseList.push({ href: pgrelementlink, innerText: pgrelementstring, category: target.innerText, study_level: educationLevelCatList[2] });
      }

      await fs.writeFileSync("./output/latrobeuniversity_data.json", JSON.stringify(totalCourseList));
      await fs.writeFileSync("./output/latrobeuniversity_original_courselist.json", JSON.stringify(totalCourseList));
      // await page.waitFor(5000);


      // const targetLinks = await page.$$(linkselector);
      // const targetText = await page.$$(textselector);
      // await page.waitFor(5000);
      // console.log("target.innerText -->", target.innerText);
      // console.log("#total link selectors---->" + targetLinks.length);
      // for (var i = 0; i < targetLinks.length; i++) {
      //     var elementstring = await page.evaluate(el => el.innerText, targetText[i]);
      //     const elementlink = await page.evaluate(el => el.href, targetLinks[i]);
      //     totalCourseList.push({ href: elementlink, innerText: elementstring, category: target.innerText });
      // }
    }
    console.log("totalCourseList -->", totalCourseList);

    //await fs.writeFileSync("./output/latrobeuniversity_original_courselist.json", JSON.stringify(totalCourseList));
    let uniqueUrl = [];
    //unique url from the courselist file
    for (let i = 0; i < totalCourseList.length; i++) {
      let cnt = 0;
      if (uniqueUrl.length > 0) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (totalCourseList[i].href == uniqueUrl[j].href) {
            cnt = 0;
            break;
          } else {
            cnt++;
          }
        }
        if (cnt > 0) {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
        }
      } else {
        uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
      }
    }
    // var flags = [], uniqueUrl = [], l = totalCourseList.length, i;
    // for (i = 0; i < l; i++) {
    //   if (flags[totalCourseList[i].href]) continue;
    //   flags[totalCourseList[i].href] = true;
    //   uniqueUrl.push(totalCourseList[i]);
    // }
    // console.log("NAME@@@@@"+JSON.stringify(uniqueUrl))

    await fs.writeFileSync("./output/latrobeuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
    //based on unique urls mapping of categories
    // for (let i = 0; i < totalCourseList.length; i++) {
    //   for (let j = 0; j < uniqueUrl.length; j++) {
    //     if (uniqueUrl[j].href == totalCourseList[i].href) {
    //       uniqueUrl[j].category.push(totalCourseList[i].category);
    //     }
    //   }
    // }
    for (let i = 0; i < totalCourseList.length; i++) {
      for (let j = 0; j < uniqueUrl.length; j++) {
        if (uniqueUrl[j].href == totalCourseList[i].href) {
          if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

          } else {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }

        }
      }
    }
    console.log("totalCourseList -->", uniqueUrl);
    await fs.writeFileSync("./output/latrobeuniversity_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }

  
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  static async scrapeCourseListForPages(courseItemSelectorList, firstPageSelector, pageURL) {
    const funcName = 'scrapeCourseListForPages ';
    let s = null; let courseList = null;
    try {
      // validate params
      Scrape.validateParams([courseItemSelectorList, firstPageSelector, pageURL]);
      console.log(funcName + 'courseItemSelectorList = ' + JSON.stringify(courseItemSelectorList));
      // create Scrape instance
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(pageURL);
      let nextElementSiblingHandle = null;
      courseList = [];
      const activePaginationItemSel = firstPageSelector;
      let pageCount = 1;

      do {
        await s.page.waitFor(3000);
        // wait for sel
        console.log(funcName + 'waitForSelector sel = ' + activePaginationItemSel);
        await s.page.waitForSelector(activePaginationItemSel, { visible: true });
        // scrape all course list items
        console.log(funcName + '\n\r ********** Scraping for page  ' + pageCount + ' ********** \n\r');
        const resList = [];

        console.log(funcName + 'waitForSelector sel = ' + courseItemSelectorList[0]);
        await s.page.waitForSelector(courseItemSelectorList[0]);

        const anchorEleHandle = await s.page.$(courseItemSelectorList[0]); // anchor element handle
        console.log(funcName + 'anchorEleHandle = ' + anchorEleHandle);

        const hrefList = await s.scrapeAnchorElement(courseItemSelectorList[0], null, '');
        // console.log(funcName + 'hrefList = ' + JSON.stringify(hrefList));
        for (const itemDict of hrefList) {
          const courseDict = {};
          courseDict.href = itemDict.href;
          const itemInnerText = itemDict.innerText;
          const innerTextList = String(itemInnerText).split('\n');
          console.log(funcName + 'innerTextList = ' + JSON.stringify(innerTextList));
          if (innerTextList && innerTextList.length > 0) {
            courseDict.innerText = innerTextList[0];
          }
          console.log(funcName + 'courseDict = ' + JSON.stringify(courseDict));
          resList.push(courseDict);
        } // for

        courseList = courseList.concat(resList);
        // console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
        const eleList = await s.page.$$(activePaginationItemSel);
        if (Array.isArray(eleList) && eleList.length > 0) {
          const activePageButton = eleList[0];
          // reassign next sibling handle
          nextElementSiblingHandle = await activePageButton.getProperty('nextElementSibling');
          console.log(funcName + 'nextElementSiblingHandle = ' + nextElementSiblingHandle);
          if (nextElementSiblingHandle == "JSHandle@node") {
            const propHandle = nextElementSiblingHandle.getProperty('nodeName');
            console.log(funcName + 'propHandle = ' + propHandle);
            let nextSiblingNodeName = null;
            await propHandle.then((res) => {
              console.log('res = ' + res);
              const resJSon = res.jsonValue();
              console.log('resJSon = ' + resJSon);
              resJSon.then((resB) => {
                console.log('resB = ' + resB);
                nextSiblingNodeName = resB;
              });
            });
            console.log(funcName + 'nextSiblingNodeName = ' + nextSiblingNodeName);
            if (String(nextSiblingNodeName) !== 'LI') { // if next is not Anchor element
              console.log(funcName + 'nextElementSiblingHandle includes null....so breaking the loop...');
              break;
            }
            if (nextElementSiblingHandle) {
              await nextElementSiblingHandle.click('middle');
              console.log(funcName + 'Clicked nextElementSiblingHandle..');
            } else {
              throw (new Error('nextElementSiblingHandle invalid'));
            }
          }
          else { break; }
        }

        pageCount += 1;
        console.log(funcName + 'courseList count = ' + courseList.length);
      } while (nextElementSiblingHandle);

      console.log(funcName + 'total pages scrapped = ' + pageCount);
      console.log(funcName + 'courseList count = ' + courseList.length);
      // console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(courseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      if (s) {
        await s.close();
      }
    } catch (error) {
      console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r\n\r');
      console.log(funcName + 'try-catch error = ' + error);
      console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r\n\r');
      if (s) {
        await s.close();
      }
      console.log(funcName + 'writing courseList to tmp file....');
      await fs.writeFileSync(configs.opCourseListTempFilepath, JSON.stringify(courseList));
      console.log(funcName + 'writing courseList to tmp file completed successfully....');
      throw (error);
    }
  }

  // scrape course page list of the univ
  async scrapeCourseListAndPutAtS3(selFilepath) {
    const funcName = 'scrapeCourseListAndPutAtS3 ';
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      const pageUrl = this.selectorJson.url;
      console.log(funcName + 'url = ' + pageUrl);
      const jsonDictElements = this.selectorJson.elements;
      console.log(funcName + 'elements = ' + JSON.stringify(jsonDictElements));
      let courseList = null;
      if (Array.isArray(jsonDictElements)) {
        for (const eleDict of jsonDictElements) {
          const eleType = eleDict.elementType;
          console.log(funcName + 'elementType = ' + eleType);
          // ensure type is matching
          if (eleType === Scrape.ELEMENT_TYPE.COURSE_LIST) {
            const courseItemSelector = eleDict.course_item_selector;
            console.log(funcName + 'course_item_selector = ' + courseItemSelector);
            const pageSelector = eleDict.page_selector;
            console.log(funcName + 'page_selector = ' + pageSelector);
            courseList = await ScrapeCourseList.scrapeCourseListForPages(courseItemSelector, pageSelector, pageUrl);
          } // if
        } // for elements
      } // if
      // console.log(funcName + 'writing courseList to file....');
      // const selector = "#main > div.center-col.content > table > tbody > tr > th";
      // const innerText = "CRICOS Code:";
      // await utils.validateMyCourseList(this.s.page,configs.opCourseListFilepath,courseList,selector,innerText);
      // //await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(courseList));
      // console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      // const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      //console.log(funcName + 'object location = ' + res);
      return courseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  // refers to http://www.utas.edu.au/courses
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // course_level_form_selector key
      // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelList) {
      //   console.log(funcName + 'Invalid formSelList');
      //   throw (new Error('Invalid formSelList'));
      // }
      // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelElementDict) {
      //   console.log(funcName + 'Invalid formSelElementDict');
      //   throw (new Error('Invalid formSelElementDict'));
      // }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
