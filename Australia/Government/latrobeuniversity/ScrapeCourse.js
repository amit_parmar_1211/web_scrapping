const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');
class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_category, study_level) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            console.log("course_category" + JSON.stringify(course_category))
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = course_category;
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const penglishList = []; const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            const ieltsDict = {};
                            let ieltsNumber = null;
                            if (courseScrappedData.course_toefl_ielts_score) {
                                const ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                                if (ieltsScore && ieltsScore.length > 0) {

                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(ieltsScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ieltsNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                    }
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsScore;
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNumber
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;

                                    englishList.push(ieltsDict);
                                }
                                else {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = "";
                                    ieltsDict.min = 0;
                                    ieltsDict.require = 0
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;

                                    englishList.push(ieltsDict);
                                    console.log("not founfded", englishList)
                                }
                                if (englishList && englishList.length > 0) {
                                    courseAdminReq.english = englishList;
                                }
                            }
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                let arr = [];
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                arr.push(academicReq)
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = arr;
                                } else {
                                    courseAdminReq.academic = []
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }
                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.entry_requirements_url;
                            console.log("EntryRequirement:::" + courseKeyVal);
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))

                            // add english requirement 'english_more_details' link
                            const courseKeyValAcademic = courseScrappedData.academic_requirements_url;
                            console.log("AcademicRequirement:::" + courseKeyValAcademic);
                            let resAcademicReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyValAcademic)) {
                                for (const rootEleDict of courseKeyValAcademic) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAcademicReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            // add english requirement 'english_requirements_url'
                            const courseKeyValEnglishReq = courseScrappedData.english_requirements_url;
                            console.log("EnglishRequirement:::" + courseKeyValEnglishReq);
                            let resEnglishReqJson = null;
                            if (Array.isArray(courseKeyValEnglishReq)) {
                                for (const rootEleDict of courseKeyValEnglishReq) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqJson;
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqJson;
                            }
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            console.log("courseKeyVal: " + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    // durationFullTime.duration_full_time = resFulltime;
                                    // courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("FilterData---->" + JSON.stringify(filtered_duration_formated))

                                    // if (courseDurationList && courseDurationList.length > 0) {
                                    resJsonData.course_duration = resFulltime;
                                    //  }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }

                                }
                            }
                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            // const courseKeyVal =await Course.extractValueFromScrappedElement(courseScrappedData.program_code);

                            console.log("courseKey_pcode: " + JSON.stringify(courseKeyVal));
                            var program_code = [];
                            var pcode = []
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {

                                                program_code.push(selList);
                                                console.log("pro:::" + JSON.stringify(String(program_code).split(",")));
                                                // if (String(program_code).includes(",")) {
                                                //     var pcode = String(program_code).split(", ");
                                                //     //pcode.trim();
                                                // } else {
                                                //     var pcode = program_code
                                                // }
                                            }
                                        }
                                    }
                                }
                            }
                            if (program_code && program_code.length > 0) {

                                if (program_code.length == 2) {
                                    var splitdata = String(program_code).split(",")
                                    console.log("Program_code@@@" + JSON.stringify(splitdata))
                                    splitdata.forEach(element => {
                                        var datas = element.split(":")
                                        if (datas[1].length > 0) {
                                            console.log("NewData@@@@", JSON.stringify(datas[1]))
                                            pcode.push(datas[1].trim())

                                        }
                                    })
                                }
                                else {
                                    console.log("InELSE")
                                    if (String(program_code).includes(",")) {
                                        var arr = []

                                        var splitdatas = String(program_code).split(",");

                                        console.log("NEWSplitData@@@" + JSON.stringify(splitdatas))
                                        for (var i = 0; i < splitdatas.length; i++) {
                                            if (splitdatas[i].includes(":")) {
                                                var data = splitdatas[i].split(":")
                                                pcode.push(data[1].trim())
                                            } else {
                                                pcode.push(splitdatas[i].trim())
                                            }
                                        }
                                        console.log("INcluded Data@@@", JSON.stringify(pcode))
                                    } else {
                                        var datas = String(program_code).split(":")
                                        pcode.push(datas[1])
                                    }
                                }
                            } else {
                                pcode = ""
                            } if (pcode.length > 0) {
                                resJsonData.program_code = String(pcode).trim();
                            } else {
                                resJsonData.program_code = ""
                            }

                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            let feesVal;
                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feeYear && feeYear.length > 0) {
                                courseTuitionFee.year = feeYear;
                            }
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const arrval = String(feesWithDollorTrimmed).split('.');
                                console.log("ArrayValue", JSON.stringify(arrval))
                                if (String(arrval[0]).includes("Note:")) {
                                    var splitdata = String(arrval[0]).split("Note:")
                                    console.log("SplitData", JSON.stringify(splitdata[0]))
                                    feesVal = splitdata[0]
                                } else {
                                    feesVal = String(arrval[0]);
                                }
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: feesIntStudent,
                                            });
                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: 1,
                                                unit: demoarray.unit,
                                                description: "not available fee",
                                            });
                                        }
                                        console.log("feesDictinternational_student-->", feesDict.international_student);
                                    }
                                }
                            }

                            // let international_student_all_fees_array = [];
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    //  feesDict.international_student_all_fees = international_student_all_fees_array
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: resJsonData.course_url
                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                if (application_fee.length > 0) {
                                    resJsonData.application_fee = application_fee;
                                } else {
                                    resJsonData.application_fee = '';
                                }

                                break;
                            }
                        case 'course_campus_location': { // Location Launceston
                            const campLocationText = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            console.log("All Campus@@@" + JSON.stringify(campLocationText))
                            //console.log(funcName + 'campLocationTextArr = ' + campLocationTextArr);
                            //const campLocationText = campLocationTextArr.split(" ");
                            console.log(funcName + 'campLocationText = ' + campLocationText);
                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = String(campLocationText).trim();
                                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    var mylocations = await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t ]+/g, ';'), ';');
                                    var mylocations_rem = [];
                                    // var mylocations_code = [];
                                    if (mylocations.length > 0) {
                                        let campuses = ['Melbourne', 'Albury-Wodonga', 'Bendigo', 'City', 'Mildura', 'Shepparton', 'Sydney']
                                        mylocations.forEach(element => {
                                            if (campuses.includes(element)) {
                                                console.log("YESSSS location perfact")
                                                for (var i = 0; i < campuses.length; i++) {
                                                    if (campuses[i] == element) {
                                                        mylocations_rem.push(campuses[i])
                                                    }
                                                }
                                            }
                                        });
                                    }

                                    const courseKeyVal1111 = courseScrappedData.course_cricos_code;
                                    let course_cricos_code = null;
                                    if (Array.isArray(courseKeyVal1111)) {
                                        for (const rootEleDict of courseKeyVal1111) {
                                            console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                            const elementsList = rootEleDict.elements;
                                            for (const eleDict of elementsList) {
                                                console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                                const selectorsList = eleDict.selectors;
                                                for (const selList of selectorsList) {
                                                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                    if (Array.isArray(selList) && selList.length > 0) {
                                                        course_cricos_code = selList;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    var campusedata = [];
                                    mylocations_rem.forEach(element => {
                                        var cam = format_functions.titleCase(element)
                                        campusedata.push({
                                            "name": cam,
                                            "code": course_cricos_code.toString()
                                        })
                                    });
                                    console.log("Avail-->" + mylocations_rem)
                                    resJsonData.course_campus_location = campusedata;
                                    resJsonData.course_study_mode = "On campus";//.join(',');
                                    console.log("## FInal string-->" + campLocationValTrimmed);
                                }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            var courseIntakeStr = [];//= await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            // existing intake value
                            const courseIntakeStr1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);


                            if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                                resJsonData.course_intake = courseIntakeStr1;
                                const intakeStrList = String(courseIntakeStr1).split('2020 ');
                                console.log("@@@hello" + JSON.stringify(intakeStrList));
                                console.log("1@@@hello" + JSON.stringify(String(courseIntakeStr1).includes(",")));
                                console.log(funcName + 'intakeStrList course_intake = ' + JSON.stringify(intakeStrList));
                                courseIntakeStr = intakeStrList;
                            }

                            console.log("intakes" + JSON.stringify(courseIntakeStr));
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var campus = resJsonData.course_campus_location;
                                // var campus = await utils.giveMeArray(String(cam).replace(/[\r\n\t ]+/g, ';'), ';');
                                console.log('Campuse location :' + campus);
                                var intakes = [];
                                console.log("Intake length-->" + courseIntakeStr.length);
                                console.log("Campus length-->" + campus.length);
                                for (var camcount = 0; camcount < campus.length; camcount++) {
                                    //for (var count = 0; count < courseIntakeStr.length; count++) {
                                    // console.log("intake -->" + count)
                                    var intakedetail = {};
                                    //intakedetail.campus = campus[count];
                                    intakedetail.name = campus[camcount].name;
                                    var array1 = [];

                                    var nt;
                                    if (String(courseIntakeStr).includes("February") || String(courseIntakeStr).includes("January")) {

                                        if (!String(courseIntakeStr).includes("Semester 1")) {
                                            let arr = []
                                            if (String(courseIntakeStr).includes('.')) {
                                                var data = String(courseIntakeStr).split('.')
                                                console.log("element->", data)
                                                var intake = data[1].replace("Course commences", '')
                                                arr.push(intake.trim())
                                                intakedetail.value = arr;
                                                intakes.push(intakedetail);

                                            }
                                            else if (String(courseIntakeStr).includes(",")) {
                                                var splitdata = String(courseIntakeStr).split(",")

                                                var trimvalue
                                                var newarr = []
                                                splitdata.forEach(element => {
                                                    arr.push(element)
                                                })
                                                console.log("@splitdata@@", arr)
                                                arr.forEach(element => {
                                                    if (element.includes("(Melbourne)") || element.includes("(Regional campuses) 2020")) {
                                                        var value = element.replace("(Melbourne)", '').replace("(Regional campuses) 2020", '');
                                                        trimvalue = value.trim();
                                                        newarr.push(trimvalue)
                                                        console.log("@@trimvalue", trimvalue)
                                                    }
                                                })


                                                intakedetail.value = newarr;
                                                intakes.push(intakedetail);

                                            } else {
                                                console.log("yesss")
                                                if (String(courseIntakeStr).includes("(compulsory preparatory subject)")) {
                                                    var data = String(courseIntakeStr).replace("(compulsory preparatory subject) 2020", '')
                                                    arr.push(data.trim() + ",2020")
                                                    console.log("arr", arr)
                                                    intakedetail.value = arr;
                                                    intakes.push(intakedetail);
                                                } else if (String(courseIntakeStr).includes("and")) {
                                                    var data = String(courseIntakeStr).split("and")
                                                    data.forEach(element => {
                                                        if (element.includes("Winter Semester") || element.includes("Summer Semester 2"))
                                                            var dd = element.replace("Winter Semester", '').replace("Summer Semester 2", '').replace(/[\r\n\t() ]+/g, ' ').replace("2020", '')
                                                        console.log("Element-->", dd)
                                                        arr.push(dd.trim() + ",2020")
                                                    })
                                                    intakedetail.value = arr;
                                                    intakes.push(intakedetail);

                                                }
                                                else {
                                                    intakedetail.value = courseIntakeStr;
                                                    intakes.push(intakedetail);
                                                }

                                            }



                                        } else {
                                            var myarray = await utils.giveMeArray(courseIntakeStr[0].replace(/[\r\n\t ]+/g, ' '), ",");
                                            //console.log("HELLO-->"+JSON.stringify(myarray));
                                            var myvalue = [];
                                            var remover = ["semester 1", "semester 2", "semester 3", "semester 4", "Melbourne and regional campuses", "Melbourne Campus only"];
                                            myarray.forEach(element => {
                                                remover.forEach(elementrm => {
                                                    element = element.toLowerCase().replace(elementrm, "");
                                                });
                                                myvalue.push(element.trim());
                                            });
                                            var val = [];

                                            if (courseIntakeStr[0].toLowerCase().trim().indexOf("anytime") == -1) {

                                                var mytextpre = courseIntakeStr[0].split("(");
                                                if (mytextpre.length > 1) {
                                                    var mytextsuf = mytextpre[1].split(")");
                                                    val = await utils.giveMeArray(mytextsuf[0], "and");
                                                }
                                                else {
                                                    val = courseIntakeStr;

                                                }
                                            } else {

                                                val = courseIntakeStr;
                                            }

                                            intakedetail.value = val;
                                            intakes.push(intakedetail);
                                        }

                                    }

                                    else if (String(courseIntakeStr).includes(",")) {

                                        console.log("intakesDATw" + JSON.stringify(!String(courseIntakeStr).includes("Semester 1")));
                                        //console.log("CourseINtakeSTr-->",JSON.stringify(courseIntakeStr))
                                        if (!String(courseIntakeStr).includes("Semester 1")) {
                                            var intake1 = String(courseIntakeStr).replace("2020", '').trim();
                                            var array = String(intake1).split(",");
                                            var splitdata = String(array[0]).split("and");
                                            var arr = splitdata.concat(array[1]).concat(array[2]);
                                            arr.forEach(element => {
                                                if (element.includes("(all campuses)") || element.includes("(Melbourne Campus only)") || element.includes('(Sydney Campus only)')) {
                                                    var value = element.replace("(all campuses)", '').replace("(Melbourne Campus only)", '').replace("(Sydney Campus only)", '');
                                                    var trimvalue = value.trim();
                                                }

                                                else {
                                                    var trimvalue = element.trim();
                                                }
                                                array1.push(trimvalue)

                                            })
                                            intakedetail.value = array1;
                                            intakes.push(intakedetail);
                                            console.log("NOOOO" + JSON.stringify(array))
                                        } else {
                                            var myarray = await utils.giveMeArray(courseIntakeStr[0].replace(/[\r\n\t ]+/g, ' '), ",");
                                            //console.log("HELLO-->"+JSON.stringify(myarray));
                                            var myvalue = [];
                                            var remover = ["semester 1", "semester 2", "semester 3", "semester 4", "Melbourne and regional campuses", "Melbourne Campus only", "Semester 1",
                                                "Semester 2", "Summer 1", "Melbourne"];
                                            myarray.forEach(element => {
                                                remover.forEach(elementrm => {
                                                    if (element.includes("(")) {
                                                        element = element.replace(/[\r\n\t()]+/g, "")
                                                    }
                                                    if (element.toLowerCase().includes("summer 1 november 2020") || element.toLowerCase().includes("(Regional campuses) 2020")) {
                                                        element = element.toLowerCase().replace("summer 1 november 2020", "november").replace("(Regional campuses) 2020", '')
                                                    }
                                                    element = element.toLowerCase().replace(elementrm, "");
                                                });
                                                myvalue.push(element.trim());
                                            });
                                            Console.log("MYVAlue@@@", myvalue)
                                            //intakedetail.value=
                                            var val = [];
                                            if (courseIntakeStr[0].toLowerCase().trim().indexOf("anytime") == -1) {
                                                if (myvalue.length > 0) {
                                                    myvalue.forEach(element => {
                                                        val.push(element + ",2020")
                                                    })
                                                    console.log("MyValue-->", val)
                                                } else {
                                                    var mytextpre = courseIntakeStr[0].split("(");
                                                    console.log("mytextpre@@@", JSON.stringify(mytextpre))
                                                    if (mytextpre.length > 1) {
                                                        var mytextsuf = mytextpre[1].split(")");
                                                        val = await utils.giveMeArray(mytextsuf[0], "and");
                                                    }
                                                    else {
                                                        val = courseIntakeStr;
                                                    }
                                                }
                                            }
                                            else {
                                                val = courseIntakeStr;
                                            }
                                            console.log("MyValue-->", val)
                                            intakedetail.value = val;
                                            intakes.push(intakedetail);
                                        }
                                    }
                                    else if (String(courseIntakeStr).includes("(Melbourne and regional campuses)")) {
                                        var array = String(courseIntakeStr).split(") and ");
                                        console.log("YESSSS" + JSON.stringify(array));

                                        array.forEach(element => {

                                            nt = element.replace(/\(|/g, '').replace('Melbourne and regional campuses', '').replace('Melbourne Campus only) 2020', '');
                                            //console.log("-->####HELLO-->"+JSON.stringify(nt));
                                            array1.push(nt.trim());

                                        });
                                        console.log("####HELLO-->" + JSON.stringify(array1));
                                        intakedetail.value = array1;
                                        intakes.push(intakedetail);

                                    } else {
                                        var myarray = await utils.giveMeArray(courseIntakeStr[0].replace(/[\r\n\t ]+/g, ' '), ",");
                                        console.log("HELLO-->" + JSON.stringify(myarray));

                                        var myvalue = [];
                                        var remover = ["semester 1", "semester 2", "semester 3", "semester 4", "Semester 1"];
                                        myarray.forEach(element => {
                                            remover.forEach(elementrm => {
                                                element = element.toLowerCase().replace(elementrm, "");
                                            });
                                            myvalue.push(element.trim());
                                        });
                                        var val = [];
                                        if (courseIntakeStr[0].toLowerCase().trim().indexOf("anytime") == -1) {
                                            console.log("-->val123@@@" + val)
                                            var mytextpre = courseIntakeStr[0].split("(");
                                            if (mytextpre.length > 1) {
                                                var mytextsuf = mytextpre[1].split(")");
                                                val = await utils.giveMeArray(mytextsuf[0], " and ");
                                                console.log("-->val33@@@" + JSON.stringify(val))
                                            }
                                            else {
                                                val = courseIntakeStr;
                                                console.log("-->val22@@@" + JSON.stringify(val))
                                            }
                                        } else {
                                            val = courseIntakeStr;
                                            console.log("-->val11@@@" + JSON.stringify(val))
                                        }
                                        console.log("-->NEWINtakeDATA@@@" + JSON.stringify(val))
                                        intakedetail.value = val;
                                        intakes.push(intakedetail);
                                        console.log("-->intakes@@@intakes" + JSON.stringify(intakes))
                                    }
                                }
                                var intakedata = {};
                                var intakeUrl = [];
                                console.log("FinalIntake--" + JSON.stringify(intakes));
                                let formatedIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                console.log("NEw Intake Format@@@@@" + JSON.stringify(formatedIntake))
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedata.intake = formatedIntake;
                                intakedata.more_details = String(intakeUrl);
                                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
                            } // if (courseIntakeStr && courseIntakeStr.length > 0)
                            break;
                        }

                        case 'course_study_level': {
                            //const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            let cStudyLevel = null;
                            console.log(funcName + 'course_title course_study_level = ' + study_level);
                            resJsonData.course_study_level = study_level;
                            // if (cTitle) {
                            //     if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || String(cTitle).toLowerCase().indexOf('advanced diploma') > -1 || (String(cTitle).toLowerCase().indexOf('diploma') > -1 && String(cTitle).toLowerCase().indexOf('graduate diploma') == -1)) {
                            //         cStudyLevel = 'Under Graduate';
                            //     }
                            //     else if (String(cTitle).toLowerCase().indexOf('master by research') > -1 || String(cTitle).toLowerCase().indexOf('master by researchs') > -1 || String(cTitle).toLowerCase().indexOf('master research') > -1 || String(cTitle).toLowerCase().indexOf('master  researchs') > -1 || String(cTitle).toLowerCase().indexOf('research') > -1 || String(cTitle).toLowerCase().indexOf('researchs') > -1) {
                            //         cStudyLevel = 'Research';
                            //     }
                            //     else if (String(cTitle).toLowerCase().indexOf('graduate') > -1 || String(cTitle).toLowerCase().indexOf('graduates') > -1) {
                            //         cStudyLevel = 'Post Graduate';
                            //     }
                            //     else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1) {
                            //         cStudyLevel = 'Post Graduate';
                            //     }
                            //     else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1) {
                            //         cStudyLevel = 'Research';
                            //     }
                            //     else {
                            //         cStudyLevel = 'Vocational / Other';
                            //     }
                            //     if (cStudyLevel) {
                            //         resJsonData.course_study_level = cStudyLevel;
                            //     }
                            // }
                            break;
                        }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview)
                            //const courseKeyVal = courseScrappedData.course_overview;
                            let resAccomodationCostJson = courseKeyVal;
                            // if (Array.isArray(courseKeyVal)) {
                            //     for (const rootEleDict of courseKeyVal) {
                            //         console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                            //         const elementsList = rootEleDict.elements;
                            //         for (const eleDict of elementsList) {
                            //             console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                            //             const selectorsList = eleDict.selectors;
                            //             for (const selList of selectorsList) {
                            //                 console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                            //                 if (Array.isArray(selList) && selList.length > 0) {
                            //                     resAccomodationCostJson = selList[0];
                            //                 }
                            //             }
                            //         }
                            //     }
                            // }
                            if (resAccomodationCostJson) {
                                resJsonData.course_overview = resAccomodationCostJson;
                            } else {
                                resJsonData.course_overview = ""
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const outcome_data = courseScrappedData.course_career_outcome
                            let outcome_data111;
                            if (Array.isArray(outcome_data)) {
                                for (const rootEleDict of outcome_data) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                outcome_data111 = selList;
                                                console.log("#### Outcome1243--->" + outcome_data111);
                                            }
                                        }
                                    }
                                }
                            }
                            // console.log("outttttttcome:"+outcome_data);
                            if (outcome_data111) {
                                var outcome = outcome_data111;
                                if (outcome.length > 0) {
                                    resJsonData.course_career_outcome = [String(outcome).trim()];
                                    console.log("#### Outcome is--->" + resJsonData.course_career_outcome);
                                } else {
                                    resJsonData.course_career_outcome = []
                                }
                            } else {
                                resJsonData.course_career_outcome = []
                            }

                            console.log("#### Outcome is--->" + resJsonData.course_career_outcome);
                            break;
                        }
                        case 'course_title': {
                            const title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            const ctitle = format_functions.titleCase(title)
                            resJsonData.course_title = ctitle
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            // console.log('\n\r' + funcName + 'course resJsonData =' + JSON.stringify(resJsonData));
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;

                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid.replace(/[\n :""]+/g, '').replace(/[\/]+/g, '_') + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
            } // if (appConfigs.shouldTakeFromOutputFolder)
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.category, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };