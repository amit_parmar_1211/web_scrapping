const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {

            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = course_category;
                            break;
                        }

                        case 'course_program_code': {
                            const courseKeyVal = courseScrappedData.course_program_code;
                            console.log("courseScrappedData.program_code = ", courseScrappedData.course_program_code);
                            var program_code = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selListprcode = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                                console.log("program_code---->", program_code);
                                            }
                                           
                                        }
                                    }
                                }
                            }
                            if (program_code.length == 0) {
                                resJsonData.program_code = "";
                            }
                            else{
                            for (let program_val of program_code) {
                                resJsonData.program_code = program_val;
                            }
                        }
                            break;
                        }
                        case 'application_fee': {
                            const courseKeyVal = courseScrappedData.application_fee;
                   
                            let applicationfee = null;
                              if (Array.isArray(courseKeyVal)) {
                            for (const rootEleDict of courseKeyVal) {
                           console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                           const elementsList = rootEleDict.elements;
                           for (const eleDict of elementsList) {
                           console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                              const selectorsList = eleDict.selectors;
                           for (const selList of selectorsList) {
                               console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                            if (Array.isArray(selList) && selList.length > 0) {
                              applicationfee = selList[0];
                               }
                               else{
                                  applicationfee = selList[0];
                               }
                            }
                           }
                           }
                       }
                      if (applicationfee && applicationfee.length>0) {
                        resJsonData.application_fee = applicationfee;
                       console.log("appfee"+ JSON.stringify(resJsonData.application_fee));
                       }
                       else{
                          resJsonData.application_fee = applicationfee;
                       }
                   
                        break;
                   
                  
                      }
                        case 'course_toefl_ielts_score': {
                            
                            const courseAdminReq = {};
                            const englishList = [];
                            const penglishList = [];
                            var myscrore = '';
                            let course_cricos_code = [];
                            let staticIelts = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                            let extractCricos = courseScrappedData.course_cricos_code;
                            if (Array.isArray(extractCricos)) {
                                for (const rootEleDict of extractCricos) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                for (let sel of selList) {
                                                    if (sel.indexOf(',') > -1) {
                                                        let splitCricos = sel.split(',');
                                                        for (let cricos of splitCricos) {
                                                            cricos = cricos.trim();
                                                            if (cricos == "TBC" || cricos == "DE" || cricos == "Distance Education" || cricos.indexOf("NSc") > -1) {

                                                            } else {
                                                                course_cricos_code.push(cricos);
                                                            }
                                                        }

                                                    } else {
                                                        course_cricos_code.push(sel);
                                                    }
                                                }


                                            }
                                           
                                        }
                                    }
                                }
                              
                            }
                            if(course_cricos_code && course_cricos_code.length >0){
                           course_cricos_code= course_cricos_code;
                      
                            }
                            else{
                                throw new Error("Cricos code not found");
                            }
                            console.log("staticIelts --> ", staticIelts);
                         
                             let ieltsStr = '';
                            for (let record of staticIelts) {
                               
                                let splitCricos = record.split(' - ');
                                console.log("splitCricos1 --> ", splitCricos);
                                console.log("splitCricos2 --> ", splitCricos[1]);
                                    console.log("splitCricos3 --> ", record);
                                    console.log("splitCricos4 --> ",course_cricos_code);
                               if((course_cricos_code).indexOf(splitCricos[0]) > -1){
                                ieltsStr = splitCricos[1];
                                console.log("splitCricos5 --> ", record);
                               }
                            }
                           
                            if (ieltsStr == '') {
                                let score1 = courseScrappedData.course_toefl_ielts_score;
                                console.log('Course info :' + score1);
                                myscrore = await utils.getMappingScore(score1, course_name);

                                console.log('IELTS Score :' + myscrore.IELTS);
                                var ibtDict = {}, catDict = {}, pteDict = {}, ieltsDict = {}, islprDict = {}, oetDict = {}, jcuceapDict = {}; let ieltsNumber = null;
                                if (myscrore.IELTS) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = myscrore.IELTS;
                                    ieltsDict.min = 0;
                                    ieltsDict.require = await utils.giveMeNumber(myscrore.IELTS);
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);
                                }

                                if (myscrore.TOFEL) {
                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = myscrore.TOFEL;
                                    ibtDict.min = 0;
                                    ibtDict.require = await utils.giveMeNumber(myscrore.TOFEL);
                                    ibtDict.max = 120;
                                    ibtDict.R = 0;
                                    ibtDict.W = 0;
                                    ibtDict.S = 0;
                                    ibtDict.L = 0;
                                    ibtDict.O = 0;
                                    englishList.push(ibtDict);
                                }

                                if (myscrore.PTE) {
                                    pteDict.name = 'pte academic';
                                    pteDict.description = myscrore.PTE;
                                    pteDict.min = 0;
                                    pteDict.require = await utils.giveMeNumber(myscrore.PTE);
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);
                                }
                                if (myscrore.CAE) {
                                    catDict.name = 'cae';
                                    catDict.description = myscrore.CAE;
                                    catDict.min = 80
                                    catDict.require = await utils.giveMeNumber(myscrore.CAE);
                                    catDict.max = 230
                                    catDict.R = 0;
                                    catDict.W = 0;
                                    catDict.S = 0;
                                    catDict.L = 0;
                                    catDict.O = 0;
                                    englishList.push(catDict);
                                }

                                if (englishList && englishList.length > 0) {
                                    courseAdminReq.english = englishList;
                                } else {
                                    throw new Error("IELTS NOT FOUND !!");
                                }

                                let ieltsScore = myscrore.IELTS;
                                let ibtScore = myscrore.TOFEL;
                                let pteScore = myscrore.PTE;
                                let caeScore = myscrore.CAE;
                                console.log("ieltsScore -->", ieltsScore);
                                if (ieltsScore && ieltsScore.length > 0) {
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(ieltsScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ieltsNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                    }
                                }
                                if (ibtScore && ibtScore.length > 0) {
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(ibtScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ibtScore = Number(matchedStrList[0]);
                                        console.log(funcName + 'ibtScore = ' + ibtScore);
                                    }
                                }
                                if (pteScore && pteScore.length > 0) {
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(pteScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pteScore = Number(matchedStrList[0]);
                                        console.log(funcName + 'pteScore = ' + pteScore);
                                    }
                                }
                                if (caeScore && caeScore.length > 0) {
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(caeScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        caeScore = Number(matchedStrList[0]);
                                        console.log(funcName + 'caeScore = ' + caeScore);
                                    }
                                }
                                const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};

                                if (ieltsNumber && ieltsNumber > 0) {

                                    console.log(funcName + 'ibtScore = ' + ibtScore + ', pteScore = ' + pteScore);

                                    if (ieltsNumber && ieltsNumber > 0) {
                                        var IELTSJSON = {
                                            "min": 0,
                                            "require": ieltsNumber,
                                            "max": 9
                                        };
                                        if (ieltsNumber) {
                                            pieltsDict.name = 'ielts academic';
                                            pieltsDict.value = IELTSJSON;
                                            penglishList.push(pieltsDict);
                                        }
                                    }
                                    if (ibtScore && ibtScore > 0) {
                                        var IBTJSON = {
                                            "min": 0,
                                            "require": ibtScore,
                                            "max": 120
                                        };
                                        if (ibtScore) {
                                            pibtDict.name = 'toefl ibt';
                                            pibtDict.value = IBTJSON;
                                            penglishList.push(pibtDict);
                                        }
                                    }
                                    if (pteScore && pteScore > 0) {
                                        var PTEJSON = {
                                            "min": 0,
                                            "require": pteScore,
                                            "max": 90
                                        };
                                        if (pteScore) {
                                            ppteDict.name = 'pte academic';
                                            ppteDict.value = PTEJSON;
                                            penglishList.push(ppteDict);
                                        }
                                    }
                                    if (caeScore && caeScore > 0) {
                                        var CAEJSON = {
                                            "min": 80,
                                            "require": caeScore,
                                            "max": 230
                                        };
                                        if (caeScore) {
                                            pcaeDict.name = 'cae';
                                            pcaeDict.value = CAEJSON;
                                            penglishList.push(pcaeDict);
                                        }
                                    }
                                }

                            } else {
                                const pieltsDict = {};
                                var ibtDict = {}, catDict = {}, pteDict = {}, ieltsDict = {}; let ieltsNumber = null;
                                if (ieltsStr.length > 0) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsStr;
                                    ieltsDict.min = 0;
                                    ieltsDict.require = await utils.giveMeNumber(ieltsStr);
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);
                                }


                                if (englishList && englishList.length > 0) {
                                    courseAdminReq.english = englishList;
                                }
                            }

                            if (courseScrappedData.course_academic_requirement) {
                                var arr = [];
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                arr.push(academicReq)
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = arr;
                                }
                            }
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }

                            let resEnglishReqMoreDetailsJson = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            }
                            console.log("resJsonData.course_admission_requirement.english_requirements_url -->", resJsonData.course_admission_requirement.english_requirements_url);

                            break;
                        }

                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            let courseDurationList = [];
                            const courseDurationDisplayList = [];
                            const durationFullTime = {};

                            let durationFullTimeDisplay = null;

                            console.log("*************start formating years*************************");
                            console.log("R full-time", JSON.stringify(courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0]));

                            if (courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0]) {
                                if (courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0] && courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].length > 0) {
                                    console.log("*************start formating Full Time years*************************");
                                    let temp = courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0];
                                    if(temp.includes('\n')){
                                       var newtmp= temp.replace('\n',' '); 
                                       resJsonData.course_duration = newtmp.trim();
                                    }
                                    else{
                                        resJsonData.course_duration = temp;
                                    }
                                   
                                    let temp1 = await format_functions.validate_course_duration_full_time(courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].toString());
                                    
                                    var full_year_formated_data = {
                                        "unit": temp1[0].unit,
                                        "duration": temp1[0].duration,
                                        "display": "Full-Time"
                                    };
                                   
                                    console.log("R full_year_formated_data", JSON.stringify(courseDurationDisplayList));
                                    const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                                  
                                    durationFullTime.duration_full_time = not_full_year_formated_data;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(full_year_formated_data)
                                    durationFullTimeDisplay = {};
                                    durationFullTimeDisplay = full_year_formated_data;
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList);
                                
                                    console.log("fullTimeText" + JSON.stringify(filtered_duration_formated))
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {

                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                    console.log('***************END formating Full Time years**************************')
                                }
                            }


                            // if (courseDurationList && courseDurationList.length > 0) {
                            //     resJsonData.course_duration = courseDurationList;
                            // }
                            if (courseDurationDisplayList && courseDurationDisplayList.length > 0) {
                                resJsonData.course_duration_display = courseDurationDisplayList;
                            }
                            console.log("*************END formating years*************************");
                            break;
                        }

                        case 'course_tuition_fee':

                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            break;
                        }
                        case 'course_tuition_fees_international_student': {
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};
                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student={
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: feesIntStudent
                                             
                                            };

                                        }
                                    }
                                }
                            }
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = Number(inetStudentFees);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            let international_student_all_fees_array = [];
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                   // feesDict.international_student_all_fees = international_student_all_fees_array
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }

                        case 'course_campus_location': {
                            
                            let course_cricos_code = [];
                            let courseKeyValcd = courseScrappedData.course_cricos_code;
                            if (Array.isArray(courseKeyValcd)) {
                                for (const rootEleDict of courseKeyValcd) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                console.log("JSON.stringify(selList) course_cricos_code -->", JSON.stringify(selList));
                                                for (let sel of selList) {
                                                    if (sel.indexOf(',') > -1) {
                                                        let splitCricos = sel.split(',');
                                                        for (let cricos of splitCricos) {
                                                            cricos = cricos.trim();
                                                            if (cricos == "TBC" || cricos == "DE" || cricos == "Distance Education" || cricos.indexOf("NSc") > -1) {

                                                            } else {
                                                                course_cricos_code.push(cricos);
                                                            }
                                                        }

                                                    } else {
                                                        course_cricos_code.push(sel);
                                                    }
                                                }


                                            }
                                        }
                                    }
                                }
                              
                            }
                            if(course_cricos_code && course_cricos_code.length >0){
                                course_cricos_code=course_cricos_code;
                              }
                              else{
                                throw (new Error('Cricose code Not found!!'));
                              }
                            // Location Launceston 
                            let staticCampuses = await utils.getValueFromHardCodedJsonFile('campuses');
                            console.log('course_campus_location matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedSelectorsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                // let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (let selList of selectorsList) {
                                        if (selList && selList.length > 0) {
                                            console.log('course_campus_location selList = ' + JSON.stringify(selList));
                                            let concatnatedSelStr = [];
                                            if (selList[0].indexOf('\n')) {
                                                console.log("inside");
                                                selList = selList[0].split('\n')[0];
                                            }
                                            console.log("selList campus -->", selList);
                                            let selItem;
                                            if (selList.includes(',')) {
                                                selItem = selList.split(",");
                                            } else {
                                                selItem = selList.split(";");
                                            }
                                            console.log("selItem campus -->", selItem);
                                            for (let sel of selItem) {
                                                console.log("selCampus --> ", sel);
                                                sel = sel.trim();
                                                var campusedata = [];
                                                for (let campus of staticCampuses) {
                                                    if (campus.indexOf(sel) > -1) {
                                                        concatnatedSelStr.push({
                                                            "name": this.titleCase(campus),
                                                            "code": String(course_cricos_code)
                                                        })

                                                        resJsonData.course_campus_location = campusedata;
                                                    }
                                                }
                                                

                                            }
                                            console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);
                                            if (concatnatedSelStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            }
                                        }
                                    }
                                }
                            }
                          
                            if (concatnatedSelectorsStr && concatnatedSelectorsStr.length > 0) {
                                console.log("cmpdata", campusedata);
                                var naindex;
                               for(let item of concatnatedSelectorsStr){
                                 if(item.name.includes("Singapore") === true){
                                 naindex = concatnatedSelectorsStr.indexOf(item);
                                  console.log("chkin>",naindex);
                                 if (naindex > -1) {
                                    concatnatedSelectorsStr.splice(naindex, 1);
                                  }
                                }else{
                                    concatnatedSelectorsStr=concatnatedSelectorsStr;
                                }
                                }
    
                                resJsonData.course_campus_location = concatnatedSelectorsStr;
                               
                            } else {
                                throw new Error("Location Not Found !!");
                            }

                            break;

                        }
                        case 'course_study_mode': { // Location Launceston
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }

                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            let courseIntakeDisplay = [];
                            // existing intake value
                            // const courseIntakeStrElem = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            let intake_months = [
                                {
                                    key: "January",
                                    value: "2020-01-31"
                                },
                                {
                                    key: "February",
                                    value: "2020-02-28"
                                },
                                {
                                    key: "March",
                                    value: "2020-03-31"
                                },
                                {
                                    key: "April",
                                    value: "2020-04-30"
                                },
                                {
                                    key: "May",
                                    value: "2020-05-31"
                                },
                                {
                                    key: "June",
                                    value: "2020-06-30"
                                },
                                {
                                    key: "July",
                                    value: "2020-07-31"
                                },
                                {
                                    key: "August",
                                    value: "2020-08-31"
                                },
                                {
                                    key: "September",
                                    value: "2020-09-30"
                                },
                                {
                                    key: "October",
                                    value: "2020-10-31"
                                },
                                {
                                    key: "November",
                                    value: "2020-11-30"
                                },
                                {
                                    key: "December",
                                    value: "2020-12-31"
                                }
                            ]
                            let courseKeyVal_intake = courseScrappedData.course_intake;
                            let courseintake;
                            let courseIntakeDisplay1;
                            if (Array.isArray(courseKeyVal_intake)) {
                                for (const rootEleDict of courseKeyVal_intake) {
                                    console.log(funcName + '\n\r rootEleDict ieltsDict= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict ieltsDict= ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList ieltsDict= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList)) {
                                                for (let sel of selList) {
                                                    if (courseintake != sel) {
                                                        courseintake = sel;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }


                            console.log("R locationArray", campus);
                            console.log('************************Start course_intake******************************');
                            console.log('courseIntakeStr : ' + courseintake);
                            //console.log("R courseIntakeStrFile", courseIntakeStrFile);
                            // const intakeSplit =[];
                            if (courseintake && courseintake.length > 0) {
                                resJsonData.course_intake = courseintake;
                                //resJsonData.course_intake_display = courseIntakeStr;
                                if (String(courseintake).includes("and")) {

                                    courseIntakeDisplay1 = String(courseintake).replace("and", ',').replace(".", '').trim()
                                    console.log("gfdshgdshgfdsfgy", courseIntakeDisplay1)
                                    courseintake = [courseIntakeDisplay1]
                                    console.log("courseIntakeDisplaycourseIntakeDisplay", courseintake)
                                }


                                const intakeStrList = String(courseintake).split('\n');
                                const intakeSplit = String(intakeStrList).split(',');
                                console.log('course_intake intakeStrList = ' + JSON.stringify(intakeSplit));
                                //const regEx = /[ ]/g; var semList = [];
                                if (intakeSplit && intakeSplit.length > 0) {
                                    for (var part of intakeSplit) {
                                        // part = part.split("(");
                                        // part = part[1].split(")")[0];
                                        console.log("R part", part);
                                        part = part.trim();
                                        courseIntakeDisplay.push(part);
                                    } // for
                                }

                                console.log('course_intake courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));

                            }

                            var campus = resJsonData.course_campus_location;

                            if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];

                                for (let location11 of campus) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location11.name.trim(),
                                        "value": courseIntakeDisplay
                                    });
                                }
                            }
                            console.log("intakes123 -->", resJsonData.course_intake.intake);
                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            let formatedIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");
                            // console.log(JSON.stringify(providemyintake(intake, "MOM", "")));
                            console.log("Intakes --> ", JSON.stringify(formatedIntake));
                            var intakedata = {};
                            intakedata.intake = formatedIntake;
                            let valmonths11 = "";
                            // for (let intakeval of formatedIntake) {
                            //     console.log("intakeval--->", intakeval);
                            //     for (let month of intakeval.value) {
                            //         console.log("month--->", month);
                            //         for (valmonths11 of intake_months) {
                            //             console.log("valmonths11--->", valmonths11);
                            //             if (valmonths11.key == month.month) {
                            //                 console.log("month.key--->", valmonths11.key);
                            //                 month.filterdate = valmonths11.value;

                            //                 // formatedIntake.filterdate.push(valmonths11.value);
                            //                 //formatedIntake.push(intakedata.filterdate );
                            //                 console.log("intake_months--->", month.filterdate);
                            //                 break;
                            //             }
                            //         }
                            //     }
                            // }
                            intakedata.more_details = more_details;
                            resJsonData.course_intake = intakedata;
                            break;

                        }
                        

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_country = resAccomodationCostJson;
                            }
                            break;
                        }
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        }
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    }
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                }
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            }
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            let careerOutcomeArray = [];
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        }
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    }
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);

                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                }
                                console.log(funcName + 'concatnatedElementsStr  = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            }
                            console.log(funcName + 'concatnatedRootElementsStr career = ' + concatnatedRootElementsStr);
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                careerOutcomeArray.push(String(concatnatedRootElementsStr).trim())
                                resJsonData[key] = careerOutcomeArray;
                            }
                            else {
                                resJsonData[key] = [];
                            }
                            break;
                        }
                        case 'course_outline': {
              
                            let mgcrs = null;
                          
                         const majorcrs = {
                           majors:[],
                           minors:[],
                           more_details:""
                         };
                         //for major cousrseList
                           var coursemajor =  courseScrappedData.course_outline;
                           if (Array.isArray(coursemajor)) {
                             for (const rootEleDict of coursemajor) {
                                 console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                 const elementsList = rootEleDict.elements;
                                 for (const eleDict of elementsList) {
                                     console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                     const selectorsList = eleDict.selectors;
                                     for (const selList of selectorsList) {
                                         console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                         if (Array.isArray(selList) && selList.length > 0) {
                                           mgcrs = selList;
                                         }
                                     }
                                 }
                             }
                         }
                           if(mgcrs && mgcrs.length >0){
                             majorcrs.majors=mgcrs
                           }
                           else{
                             majorcrs.majors= majorcrs.majors
                           }
                              resJsonData.course_outline =  majorcrs;
                           //for minor courseList
                              var courseminor =  courseScrappedData.course_outline_minor;
                              let mincrs = null;
                              if (Array.isArray(courseminor)) {
                                for (const rootEleDict of courseminor) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                             mincrs = selList;
                                            }
                                        }
                                    }
                                }
                            }
                              if(mincrs && mincrs.length >0){
                                majorcrs.minors=mincrs
                              }
                              else{
                               majorcrs.minors=majorcrs.minors
                             }
                                 resJsonData.course_outline =  majorcrs;
              
                              console.log("major==>", resJsonData.course_outline)
                        
                          break;
                          }
                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                           
                            if (title.includes(' (')) {
                                const title_text = title.replace('(', ' (');
                                console.log("splitStr@@@2" + title_text);
                                title = title_text;
                                console.log("splitStr@@@1" + title);

                            }
                            else{
                                title=title;
                                console.log("splitStr@@@3" + title);
                            }
                            var ctitle = format_functions.titleCase(title).trim();
                            var ctitle2 = ctitle.replace(' (', '(');
                            var ctitle3 = ctitle2.replace(' (', '(').trim();
                            console.log("ctitle@@@", ctitle3.trim());
                            resJsonData.course_title = ctitle3
                            break;
                        }

                        case 'course_study_level':
                            {
                                let course_cricos_code = [];
                            let courseKeyValcd = courseScrappedData.course_cricos_code;
                            if (Array.isArray(courseKeyValcd)) {
                                for (const rootEleDict of courseKeyValcd) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                console.log("JSON.stringify(selList) course_cricos_code -->", JSON.stringify(selList));
                                                for (let sel of selList) {
                                                    if (sel.indexOf(',') > -1) {
                                                        let splitCricos = sel.split(',');
                                                        for (let cricos of splitCricos) {
                                                            cricos = cricos.trim();
                                                            if (cricos == "TBC" || cricos == "DE" || cricos == "Distance Education" || cricos.indexOf("NSc") > -1) {

                                                            } else {
                                                                course_cricos_code.push(cricos);
                                                            }
                                                        }

                                                    } else {
                                                        course_cricos_code.push(sel);
                                                    }
                                                }


                                            }
                                        }
                                    }
                                }
                              
                            }
                            if(course_cricos_code && course_cricos_code.length >0){
                               
                             
                                const cTitle =course_cricos_code ;
                                console.log("studylevelllll----->", cTitle)
                                let cStudyLevel = await format_functions.getMyStudyLevel(cTitle);
                                console.log("course_study_level--------", cStudyLevel);
                                resJsonData.course_study_level = cStudyLevel
                            }
                            else{
                                resJsonData.course_study_level =""
                            }
                            }
                    }
                }
                console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
            }


            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                NEWJSONSTRUCT.course_study_level = resJsonData.course_study_level;
                NEWJSONSTRUCT.course_program_code = resJsonData.course_program_code;
               // console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                // for (let myfees of resJsonData.course_tuition_fee.fees) {
                //     if (myfees.name == location) {
                //         NEWJSONSTRUCT.international_student_all_fees = myfees;
                //     }
                // }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
               // resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }

            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            const link = await s.page.$('#main > div.insidebodywrapper.colour-theme- > div.insidebody > div.insidebody__content > div.insidebody__contentmain > div > div.coursebox.box > div.box__heading > ul > li:nth-child(2) > a');
            const btnlink = await s.page.$('#main > div.insidebodywrapper.colour-theme- > div.insidebody > div.coursetitle_wrapper > div.coursebox__toggle-information.is-international.is-domestic > div > label > span.onoffswitch-switch');
            await s.page.waitFor(10000);
            if (link != null) {
                console.log("STOP STOP");

                await link.click();
                await s.page.waitFor(7000);

            }
            if(btnlink != null) {
                try {
                    console.log("CLICK CLICK CLICK!!");
                    await btnlink.click();
                    await s.page.waitFor(10000);
                } catch (e) {
                    console.log("Error International button selector Not found");
                }
            }
           if(link == null && btnlink == null){
            console.log("No CLICK only take");
                await s.page.waitFor(7000);
            }

            Scrape.validateParams([courseDict]);
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            let courseScrappedData = null;
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            }
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    }
    static titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(' ');
    }
}
module.exports = { ScrapeCourse };
