**The University Of Adelaide: Total Courses Details As On 25 Mar 2019**
* Total Courses = 415
* Courses without CRICOS code = 0
* Total available courses = 415
* Repeted = 0
* Failed = 70
**course_toefl_ielts_score**
* Everything scraped from course details and not from hardcode
**Course Intake**
* In some case intake is not given so NA is pushed
* 2019 is manually added
**Fees**
* feesWithDollorTrimmed --> conditions are added for \n
**Intakes**
* (subject to availability) --> replaced and after that it has been split