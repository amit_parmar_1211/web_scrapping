const puppeteer = require('puppeteer');
const fs = require("fs");
async function start() {
    const URL = "https://www.adelaide.edu.au/degree-finder?v__s=&m=view&dsn=program.source_program&adv_avail_comm=1&adv_acad_career=0&adv_degree_type=0&adv_atar=0&year=2019&adv_subject=0&adv_career=0&adv_campus=0&adv_mid_year_entry=0#ugrad-brse-tab";
    var subjectAreasArray = [];
    var codeList = [];
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
    let page = await browser.newPage();


    var mainCategory = [], redirecturl = [];
    //scrape main category
    const maincategoryselector = "//*/div/section/div/div[2]/h4/a[not(contains(text(),'Study at Adelaide'))]";
    var category = await s.page.$x(maincategoryselector);
    console.log("Total categories-->" + category.length);
    for (let link of category) {
      var categorystringmain = await s.page.evaluate(el => el.innerText, link);
      var categorystringmainurl = await s.page.evaluate(el => el.href, link);
      mainCategory.push(categorystringmain.trim());
      redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
    }

    //scrape courses
    for (let catc of redirecturl) {
      await s.page.goto(catc.href, { timeout: 0 });
      const titleselector = "//*/tbody/tr//a";
      var title = await s.page.$x(titleselector);
      for (let i = 0; i < title.length; i++) {
        var categorystring = await s.page.evaluate(el => el.innerText, title[i]);
        var categoryurl = await s.page.evaluate(el => el.href, title[i]);
        datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: catc.innerText });
      }
    }

    fs.writeFileSync("./output/theuniversityofadelaide_courselist.json", JSON.stringify(datalist));
    fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(mainCategory))


    fs.writeFileSync("main_category_courselist.json", JSON.stringify(values));
    await browser.close()
}
start();
