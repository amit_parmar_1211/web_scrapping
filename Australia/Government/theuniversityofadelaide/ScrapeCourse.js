const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
// const mapping = require('./output/Mapping/main');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var mycodes = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'program_code': {
                            // const courseKeyVal = courseScrappedData.program_code;
                            // console.log("courseScrappedData.program_code = ", courseScrappedData.program_code);
                            // var program_code ;
                            // if (Array.isArray(courseKeyVal)) {
                            //     for (const rootEleDict of courseKeyVal) {
                            //         console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                            //         const elementsList = rootEleDict.elements;
                            //         for (const eleDict of elementsList) {
                            //             console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                            //             const selectorsList = eleDict.selectors;
                            //             for (const selList of selectorsList) {
                            //                 console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                            //                 if (Array.isArray(selList) && selList.length > 0) {
                            //                     program_code = selList[0].split(": ");
                            //                     program_code = program_code[1];
                            //                 }
                            //             }
                            //         }
                            //     }
                            // } // if (Array.isArray(courseKeyVal))
                            let program_code_array = "";
                            // if (program_code == '' || program_code == undefined) {
                            //     program_code ;
                            // }

                            resJsonData.program_code = program_code_array;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = course_category;
                            // As of not we do not have way to get this field value
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const penglishList = [];

                            const ieltsDict = {}; const ibtDict = {}; const pteDict = {}; const caeDict = {}; let ieltsNumber = null; let pbtNumber = null; let pteNumber = null; let caeNumber = null;
                            var ieltsfinaloutput = null;
                            var tofelfinaloutput = null;
                            var ptefinaloutput = null;
                            var caefinaloutput = null;

                            // english requirement
                            if (courseScrappedData.course_toefl_ielts_score) {
                                console.log('Course english language requirment :' + JSON.stringify(courseScrappedData.course_toefl_ielts_score));
                                const ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                                ieltsfinaloutput = ieltsScore.toString().replace(/[\r\n\t ]+/g, ' ').trim();

                                if (ieltsScore && ieltsScore.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(ieltsScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ieltsNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                    }
                                }
                            }
                            // if ieltsNumber is valid then map IBT and PBT accordingly
                            if (ieltsNumber && ieltsNumber > 0) {
                                //tofel score
                                const tofelScore = await Course.extractValueEnglishlanguareFromScrappedElement(courseScrappedData.course_toefl_score);
                                if (tofelScore && tofelScore.length > 0) {
                                    tofelfinaloutput = tofelScore.toString().replace(/[\r\n\t ]+/g, ' ').trim();
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(tofelScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pbtNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'pbtNumber = ' + pbtNumber);
                                    }
                                }
                                //pte score
                                const pteAScore = await Course.extractValueEnglishlanguareFromScrappedElement(courseScrappedData.course_pte_score);
                                if (pteAScore && pteAScore.length > 0) {
                                    ptefinaloutput = pteAScore.toString().replace(/[\r\n\t ]+/g, ' ').trim();
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(pteAScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pteNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'pteNumber = ' + pteNumber);
                                    }
                                }
                                //cae score
                                const caeAScore = await Course.extractValueEnglishlanguareFromScrappedElement(courseScrappedData.course_cae_score);
                                if (caeAScore && caeAScore.length > 0) {
                                    caefinaloutput = caeAScore.toString().replace(/[\r\n\t ]+/g, ' ').replace('C1 Advanced', 'CAE Advanced').trim();

                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(caefinaloutput).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        caeNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'caeNumber = ' + caeNumber);
                                    }
                                }
                                const ieltsScore = ieltsfinaloutput.replace(/[\r\n\t ]+/g, ' ').trim();
                                const ibtScore = tofelfinaloutput.replace(/[\r\n\t ]+/g, ' ').trim();
                                const pteScore = ptefinaloutput.replace(/[\r\n\t ]+/g, ' ').trim();
                                const caeScore = caefinaloutput.replace(/[\r\n\t ]+/g, ' ').replace('C1 Advanced', 'CAE Advanced').trim();
                                if (ieltsScore) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsScore;
                                    englishList.push(ieltsDict);
                                }
                                else {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = configs.propValueNotAvaialble;
                                    englishList.push(ieltsDict);
                                }

                                if (ibtScore) {
                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = ibtScore;
                                    englishList.push(ibtDict);
                                }
                                else {
                                    ibtDict.name = 'toefl ibt';
                                    ibtDict.description = configs.propValueNotAvaialble;
                                    englishList.push(ibtDict);
                                }

                                if (pteScore) {
                                    pteDict.name = 'pte academic';
                                    pteDict.description = pteScore;
                                    englishList.push(pteDict);
                                }
                                else {
                                    pteDict.name = 'pte academic';
                                    pteDict.description = configs.propValueNotAvaialble;
                                    englishList.push(pteDict);
                                }

                                if (caeScore) {
                                    caeDict.name = 'cae';
                                    caeDict.description = caeScore;
                                    englishList.push(caeDict);
                                }
                                else {
                                    caeDict.name = 'cae';
                                    caeDict.description = configs.propValueNotAvaialble;
                                    englishList.push(caeDict);
                                }
                            }

                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};

                            if (ieltsNumber && ieltsNumber > 0) {
                                var IELTSJSON = {
                                    "name": "ielts academic",
                                    "description": ieltsDict.description,
                                    "min": 0,
                                    "require": ieltsNumber,
                                    "max": 9,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (ieltsNumber) {
                                    // pieltsDict.name = 'ielts academic';
                                    // pieltsDict.value = IELTSJSON;
                                    penglishList.push(IELTSJSON);
                                }
                            }

                            if (pbtNumber && pbtNumber > 0) {
                                var IBTJSON = {
                                    "name": "toefl ibt",
                                    "description": ibtDict.description,
                                    "min": 0,
                                    "require": pbtNumber,
                                    "max": 120,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (pbtNumber) {
                                    // pibtDict.name = 'toefl ibt';
                                    // pibtDict.value = IBTJSON;
                                    penglishList.push(IBTJSON);
                                }
                            }

                            if (pteNumber && pteNumber > 0) {
                                var PTEJSON = {
                                    "name": "pte academic",
                                    "description": pteDict.description,
                                    "min": 0,
                                    "require": pteNumber,
                                    "max": 90,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (pteNumber) {
                                    // ppteDict.name = 'pte academic';
                                    // ppteDict.value = PTEJSON;
                                    penglishList.push(PTEJSON);
                                }
                            }

                            if (caeNumber && caeNumber > 0) {
                                var CAEJSON = {
                                    "name": "cae",
                                    "description": caeDict.description,
                                    "min": 80,
                                    "require": caeNumber,
                                    "max": 230,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (caeNumber) {
                                    // pcaeDict.name = 'cae';
                                    // pcaeDict.value = CAEJSON;
                                    penglishList.push(CAEJSON);
                                }
                            }

                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            } else {
                                throw new Error("course_toefl_ielts_score undefined");
                            }
                            ///progrssbar End

                            // if (englishList && englishList.length > 0) {
                            //     courseAdminReq.english = englishList;
                            // }

                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = academicReq;
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }

                            var academicdata = [];
                            // let academic =  await utils.getValueFromHardCodedJsonFile('academic');

                            // academicdata.academic = [];

                            let resEnglishReqMoreDetailsJson = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            let academic11 = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.academic = [academic11];
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;

                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.academic = [];
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;

                            }
                            break;
                        }
                        // case 'academic': {
                        //     var academicdata = {};
                        //     let academic =  await utils.getValueFromHardCodedJsonFile('academic');
                        //     academicdata.more_details = [academic];
                        //     resJsonData.course_discipline = academic;
                        //     // As of not we do not have way to get this field value
                        //     break;
                        // }
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));

                                            rescourse_url = selList;

                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;

                        }
                        case 'course_duration_full_time': {
                            //FULL TIME AND PART TIME IS DONE IN THIS
                            //CUSTOMIZED CODE DONOT COPY
                            const courseDurationList = [];
                            const courseDurationDisplayList = [];
                            const durationFullTime = {};

                            let anotherArray = [];
                            let duration1 = courseScrappedData.course_duration_full_time;

                            console.log("*************start formating Full Time years*************************");
                            console.log("courseScrappedData.course_duration_full_time = ", JSON.stringify(courseScrappedData.course_duration_full_time));
                            let finalDuration = duration1[0].elements[0].selectors[0][0];
                            if (finalDuration.toLowerCase().indexOf('full-time') == -1 && finalDuration.toLowerCase().indexOf('full time') == -1) {
                                let splitBracket = finalDuration.split('(');
                                finalDuration = splitBracket[0] + " full-time " + splitBracket[1];
                            }
                            console.log("finalDuration -->", finalDuration);
                            const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration);
                            console.log("full_year_formated_data", full_year_formated_data);
                            ///course duration
                            const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                            console.log("not_full_year_formated_data", not_full_year_formated_data);
                            try {
                                // durationFullTime.duration_full_time = not_full_year_formated_data;
                                // courseDurationList.push(durationFullTime);
                                ///END course duration
                                console.log("courseDurationList : ", courseDurationList);
                                ///course duration display

                                courseDurationDisplayList.push(full_year_formated_data);
                                // anotherArray.push(courseDurationDisplayList);
                                console.log("courseDurationDisplayList", courseDurationDisplayList);
                                ///END course duration display
                            } catch (err) {
                                console.log("Problem in Full Time years:", err);
                            }
                            console.log("courseDurationDisplayList1", courseDurationDisplayList);
                            console.log('***************END formating Full Time years**************************')
                            let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                            //    if (courseDurationList && courseDurationList.length > 0) {
                            resJsonData.course_duration = not_full_year_formated_data;
                            //  }
                            if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                resJsonData.course_duration_display = filtered_duration_formated;
                                var isfulltime = false, isparttime = false;
                                filtered_duration_formated.forEach(element => {
                                    if (element.display == "Full-Time") {
                                        isfulltime = true;
                                    }
                                    if (element.display == "Part-Time") {
                                        isparttime = true;
                                    }
                                });
                                resJsonData.isfulltime = isfulltime;
                                resJsonData.isparttime = isparttime;
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'academic':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            // feesDict.international_student = configs.propValueNotAvaialble;
                            // feesDict.fee_duration_years = configs.propValueNotAvaialble;
                            // feesDict.currency = configs.propValueNotAvaialble;
                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feeYear && feeYear.length > 0) {
                                courseTuitionFee.year = feeYear;
                            }
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');

                                const arrval11 = String(feesVal1).split('\n')[0];
                                const arrval = String(arrval11).split('.');


                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: arrval11,
                                            });

                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: 1,
                                                unit: "Year",
                                                description: "not available fee",
                                            });
                                        }
                                    }
                                }
                            } else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",
                                    description: "not available fee",
                                });
                            }
                            // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = Number(inetStudentFees);
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if

                            //      let international_student_all_fees_array = [];
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    // feesDict.international_student_all_fees = international_student_all_fees_array
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }

                        case 'course_campus_location': { // Location Launceston 
                            const rootElementDictList = courseScrappedData.course_campus_location;
                            console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedSelectorsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                // let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        if (selList && selList.length > 0) {
                                            console.log('course_campus_location selList = ' + JSON.stringify(selList));
                                            let concatnatedSelStr = [];
                                            for (let selItem of selList) {
                                                selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ',').split(",");
                                                for (let sel of selItem) {
                                                    sel = sel.trim();
                                                    concatnatedSelStr.push(sel);
                                                }

                                            } // selList
                                            console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);
                                            if (concatnatedSelStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                                // ctitle= format_functions.titleCase(title)
                                            }
                                        }
                                    } // selectorsList 
                                } // elementsList 
                            } // rootElementDictList 

                            const rootElementDictList111 = courseScrappedData.course_cricos_code;
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList111));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList111) {
                                console.log(funcName + '\n\r rootEleDict course_cricos_code = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict course_cricos_code = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList  course_cricos_code = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList course_cricos_code 1 = ' + selList);
                                        var crcode = selList.toString().split(' ');
                                        const cricoscode = crcode[0];
                                        concatnatedRootElementsStr = cricoscode;
                                    }
                                }
                            }
                            // add only if it has valid value 
                            if (concatnatedSelectorsStr && concatnatedSelectorsStr.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                var campusedata = [];
                                concatnatedSelectorsStr.forEach(element => {
                                    campusedata.push({
                                        "name": format_functions.titleCase(element),
                                        "code": concatnatedRootElementsStr.toString()
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);
                                var study_mode = [];
                                if (campLocationValTrimmed.trim().length > 0) {
                                    study_mode.push('On campus');
                                }
                                resJsonData.course_study_mode = study_mode.toString();//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                            }
                            else {
                                throw new Error("No campus location found.");
                            }
                            break;
                        }

                        case 'course_study_mode': { // Location Launceston
                            //resJsonData.course_study_mode = coursestudymodeText;
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            var courseIntakeStr = [];//= await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            let anotherArray = [];

                            const courseKeyVal = courseScrappedData.course_intake;
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList =112 ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            let splitArray = [];
                                            if (selList.length > 0) {
                                                if (selList[0].indexOf('.') > -1) {
                                                    let tempsellist = selList[0].split('.')[0];
                                                    selList[0] = tempsellist;
                                                    console.log("selList[0] intake -->", selList[0]);
                                                }
                                                if (selList[0].indexOf('and/or') > -1) {
                                                    if (selList[0].indexOf('(subject to availability)') > -1) {
                                                        selList[0] = selList[0].replace(/\(subject to availability\)/g, '');
                                                    }
                                                    splitArray = selList[0].split(" and/or ");
                                                } else {
                                                    if (selList[0].indexOf('and') > -1) {
                                                        splitArray = selList[0].replace(/ and /g, ',').split(',');
                                                    } else {
                                                        splitArray.push(selList[0]);
                                                    }
                                                    console.log("splitArray -->", splitArray);
                                                }
                                                for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                                                    courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": splitArray });
                                                }

                                            } else {
                                                //splitArray.push("");
                                                for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                                                    courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": anotherArray });
                                                }
                                            }
                                            // }
                                        }
                                    }
                                }
                            }
                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            let filtered_intake_value = await format_functions.providemyintake(courseIntakeStr, "MOM", "");
                            // console.log("resJsonData.course_intake_url --> ", resJsonData.course_intake_url);
                            console.log("intakes" + JSON.stringify(filtered_intake_value));

                            var intakedata = {};
                            intakedata.intake = filtered_intake_value;
                            // let valmonths11 = "";
                            // for (let intakeval of filtered_intake_value) {
                            //     console.log("intakeval--->", intakeval);
                            //     for (let month of intakeval.value) {
                            //         console.log("month--->", month);
                            //         for (valmonths11 of intake_months) {
                            //             console.log("valmonths11--->", valmonths11);
                            //             if (valmonths11.key == month.month) {
                            //                 console.log("month.key--->", valmonths11.key);
                            //                 month.filterdate = valmonths11.value;
                            //                 console.log("intake_months--->", month.filterdate);
                            //                 break;
                            //             }
                            //         }
                            //     }
                            // }
                            intakedata.more_details = more_details;
                            resJsonData.course_intake = intakedata;

                            break;
                        }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: resJsonData.course_url
                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                if (application_fee.length > 0) {
                                    resJsonData.application_fee = application_fee;
                                } else {
                                    resJsonData.application_fee = '';
                                }

                                break;
                            }
                        case 'course_title': {
                            const title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            var ctitle = format_functions.titleCase(title)
                            console.log("ctitle@@@", ctitle)
                            resJsonData.course_title = ctitle
                            break;
                        }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_country = resAccomodationCostJson;
                            }
                            break;
                        }
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            } else {
                                resJsonData[key] = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            console.log('course_career_outcome matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log('course_career_outcome key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedSelStr = [];
                            for (const rootEleDict of rootElementDictList) {
                                console.log('course_career_outcome rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                // let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log('course_career_outcome eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        if (selList && selList.length > 0) {
                                            console.log('course_career_outcome selList = ' + JSON.stringify(selList));

                                            for (const selItem of selList) {
                                                concatnatedSelStr.push(selItem);
                                            } // selList
                                            console.log('course_career_outcome concatnatedSelStr = ' + concatnatedSelStr);
                                            // if (concatnatedSelStr) {
                                            //   concatnatedSelectorsStr = concatnatedSelStr;
                                            // }
                                        }
                                    } // selectorsList                  
                                } // elementsList                
                            } // rootElementDictList 

                            // add only if it has valid value              
                            if (concatnatedSelStr && concatnatedSelStr.length > 0) {
                                resJsonData.course_career_outcome = concatnatedSelStr;
                            }
                            else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }

                        case 'course_study_level': {
                            const cStudyLevel = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
                            if (cStudyLevel) {
                                resJsonData.course_study_level = cStudyLevel;
                            }
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            ////start genrating new file location wise

            var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "NA";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;

                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

            }
            return resJsonData;
            ///end grnrating new file location wise
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }
    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
