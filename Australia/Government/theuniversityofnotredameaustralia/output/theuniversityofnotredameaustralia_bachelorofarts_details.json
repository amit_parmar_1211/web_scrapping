{
      "univ_id": "theuniversityofnotredameaustralia",
      "course_url": "https://www.notredame.edu.au/programs/fremantle/school-of-arts-and-sciences/undergraduate/bachelor-of-arts",
      "course_title": "Bachelor Of Arts",
      "course_discipline": [
            "Arts & Sciences"
      ],
      "course_study_level": "Undergraduate",
      "course_campus_location": [
            {
                  "name": "Fremantle",
                  "code": "015324A"
            }
      ],
      "course_study_mode": "On campus",
      "program_code": "3009",
      "course_duration": "3 years full-time or equivalent part-time",
      "course_duration_display": [
            {
                  "unit": "years",
                  "duration": "3",
                  "display": "Full-Time",
                  "filterduration": 3
            },
            {
                  "unit": "years",
                  "duration": "6",
                  "display": "Part-Time",
                  "filterduration": 6
            }
      ],
      "isfulltime": true,
      "isparttime": true,
      "course_intake": {
            "intake": [
                  {
                        "name": "Fremantle",
                        "value": [
                              {
                                    "actualdate": "17 February, 2020",
                                    "month": "February"
                              },
                              {
                                    "actualdate": "27 July, 2020",
                                    "month": "July"
                              }
                        ]
                  }
            ],
            "more_details": "https://www.notredame.edu.au/__data/assets/pdf_file/0019/50095/University-Calendar-2020.pdf"
      },
      "course_overview": "The Bachelor of Arts develops your ability to research, analyse, interpret, draw conclusions, problem-solve and communicate. These are the skills needed in the 21st century workplace. Along with in-depth knowledge about your chosen discipline area, you will be given opportunities to show initiative, work as a team member, and be flexible and adaptable.  . You can develop all these skills while following your personal and professional interests across a broad range of Majors and Minors in a variety of discipline areas. This freedom to choose enhances your personal development, future employment prospects and potential to contribute to the cultural and intellectual life of the broader community. No matter what you specialise in, completing a Bachelor of Arts gives you the opportunity to work almost anywhere in the world. You might decide to travel during your degree, with both long- and short-term opportunities available through our Study Abroad or Experience the World programs. The Bachelor of Arts has a unique Major in Social Justice. The courses in this Major are open to students studying any discipline. The concept of social justice challenges you to step out of traditional mindsets and become involved in making a difference in your chosen career and in our world. You will begin to understand the causes of the complex problems in our society and to consider lasting solutions. Service-learning, part of our Social Justice discipline and open to all students in the Bachelor of Arts, combines community service with academic instruction as it focuses on critical, reflective thinking and civic responsibility. Placements are in the local community but there is also the opportunity to take up an international placement and travel to locations such as Cambodia.",
      "course_career_outcome": [],
      "course_admission_requirement": {
            "academic": [
                  "Academic requirements for this program are outlined below.  In addition, to be eligible for admission, all applicants need to satisfy minimum requirements outlined at admission requirements. These include those relating to age and English Language Proficiency.  We also consider your application more broadly – your non-academic achievements (such as any previous leadership roles, volunteering, work, church and/or community involvement) as well as personal qualities - your aspirations and interests and your capacity to complete your chosen program.  Applicants with recent Secondary Education:  Indicative ATAR of 70 with a score of 50+ in ATAR English, ATAR Literature or ATAR English as an Additional Language or Dialect (EALD) OR Minimum International Baccalaureate (IB) score of 24 OR Completed AQF Certificate IV or higher in a relevant discipline, from an accredited provider such as TAFE or a Registered Training Organisation (RTO) with a score of 50+ in ATAR English, ATAR Literature or ATAR English as an Additional Language or Dialect (EALD).  Applicants with Higher Education Study  Other university studies completed at undergraduate level or higher, a minimum of 4 successfully completed subjects; OR Successful completion of the Notre Dame Tertiary Pathway Program or of another Enabling Program at a level deemed sufficient by the University;  Applicants who have successfully completed subjects at another University, which are relevant to the selected program of study, may be eligible for Advanced Standing.  Applicants with Vocational Education and Training (VET) study  Completed AQF Certificate IV or higher in a relevant discipline, from an accredited provider such as TAFE or a Registered Training Organisation (RTO).  Applicants with Work and Life Experience  Applicants who left secondary education more than two years previously and who have not undertaken VET or higher education study since then, but with relevant work and life experience. Such experience may include, but is not limited to:  STAT scores of at least 135 in the Multiple Choice section and 140 in the Written English section are required OR     Mature-age completion of two Year 12 WACE or HSC exams with a resulting ATAR of 70 or above OR Western Australian Certificate of Education (WACE) or equivalent results if these have been completed within the last 5 years.  Please note: all applicants must be of school leaving age or older to apply."
            ],
            "entry_requirements_url": "https://www.notredame.edu.au/study/admission-requirements",
            "academic_requirements_url": "https://www.notredame.edu.au/international/how-to-apply/academic-entry-requirements",
            "english_requirements_url": "https://www.notredame.edu.au/study/admission-requirements/english-language-proficiency-requirements",
            "english": [
                  {
                        "name": "ielts academic",
                        "description": "6.5 overall, with no individual band score lower than 6.0",
                        "min": 0,
                        "require": 6.5,
                        "max": 9,
                        "R": 0,
                        "W": 0,
                        "S": 0,
                        "L": 0,
                        "O": 0
                  },
                  {
                        "name": "toefl ibt",
                        "description": "84 overall, with no score lower than 21 in writing and no other band score lower than 17",
                        "min": 0,
                        "require": 84,
                        "max": 120,
                        "R": 0,
                        "W": 0,
                        "S": 0,
                        "L": 0,
                        "O": 0
                  },
                  {
                        "name": "pte academic",
                        "description": "58 overall, with no individual band score lower than 50",
                        "min": 0,
                        "require": 58,
                        "max": 90,
                        "R": 0,
                        "W": 0,
                        "S": 0,
                        "L": 0,
                        "O": 0
                  },
                  {
                        "name": "cae",
                        "description": "176 overall, with no individual band score lower than 169",
                        "min": 80,
                        "require": 176,
                        "max": 230,
                        "R": 0,
                        "W": 0,
                        "S": 0,
                        "L": 0,
                        "O": 0
                  }
            ]
      },
      "course_tuition_fee": {
            "fees": [
                  {
                        "name": "Fremantle",
                        "value": {
                              "international_student": {
                                    "amount": 3575,
                                    "duration": 1,
                                    "unit": "year",
                                    "description": "3,575.00"
                              },
                              "fee_duration_years": "1",
                              "currency": "AUD"
                        }
                  }
            ]
      },
      "course_outline": {
            "majors": [],
            "minors": [],
            "more_details": "https://www.notredame.edu.au/programs/fremantle/school-of-arts-and-sciences/undergraduate/bachelor-of-arts"
      },
      "application_fee": "",
      "course_country": "Australia",
      "course_id": "bachelorofarts"
}