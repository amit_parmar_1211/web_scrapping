const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    // generate category list

    let select = "select#degree-student-type";
    await page.waitForSelector(select);
    await page.select(select, "all|international");
    await page.waitFor(2000);

    let optionValueList = await page.$x("//select[@id='degree-type']/option")
    let studArray = [];
    for (let val of optionValueList) {
      let optionValueArray = await page.evaluate(el => el.value, val);
      studArray.push(optionValueArray);
    }

    let mainCategories = [];
    //console.log('******************END dropdown*****************************************');    
    //const categoryUrlSelector = this.selectorJson.course_list_selector[0].elements[0].selectors[0];
    const categoryTextSelector = this.selectorJson.course_list_selector[0].elements[0].selectors[1];
    console.log("categoryTextSelector", categoryTextSelector);
    const categoryTextHandles = await page.$$(categoryTextSelector);
    const courseUrlSelector = "#program-container > ul > li > a.program-list__title";
    const courseTextSelector = "#program-container > ul > li > a.program-list__title > span.program-list__degree";
    const study_level = "select#degree-type";
    for (let mainCat of categoryTextHandles) {
      let CategoryText = await page.evaluate(el => el.innerText, mainCat);
      mainCategories.push(CategoryText);
    }


    var totalCourseList = [];
    //iterate through category list

    for (let optionVal of studArray) {
      if (optionVal !== '') {
        await page.select(study_level, optionVal);
        let subCategory = await page.$$(categoryTextSelector);
        console.log("length -> ", subCategory.length);
        for (let target of subCategory) {
          if (target) {
            let CategoryText = await page.evaluate(el => el.innerText, target);
            mainCategories.push(CategoryText);
            //console.log('******************Click on category*****************************');
            console.log('******************Started Scrapping ', CategoryText, '*****************************');
            await target.click();
            // await page.waitFor(2000);
            let courseUrlHandles = await page.$$(courseUrlSelector);
            let courseTextHandles = await page.$$(courseTextSelector);
            if (courseUrlHandles) {
              for (let i = 0; i < courseUrlHandles.length; i++) {
                let elementUrl = await page.evaluate(el => el.href, courseUrlHandles[i]);
                let elementText = await page.evaluate(el => el.innerText, courseTextHandles[i]);
                //var studylevel = await page.evaluate(el => el.innerText, clickarea11[0])
                // console.log("innerText : " + elementText + " category : " + CategoryText + " study_level : " + studylevel);
                totalCourseList.push({ href: elementUrl, innerText: elementText, category: CategoryText, study_level: optionVal });
              }
            }
            console.log("totalcourseList", totalCourseList)
            console.log('******************Finished Scrapping ', CategoryText, '*****************************');
          }
        }
      }
      await page.waitFor(2000);
    }

    await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(mainCategories));
    await fs.writeFileSync("./output/theuniversityofnotredameaustralia_original_courselist.json", JSON.stringify(totalCourseList));
    let uniqueUrl = [];
    //unique url from the courselist file
    for (let i = 0; i < totalCourseList.length; i++) {
      let cnt = 0;
      if (uniqueUrl.length > 0) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (totalCourseList[i].href == uniqueUrl[j].href) {
            cnt = 0;
            break;
          } else {
            cnt++;
          }
        }
        if (cnt > 0) {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
        }
      } else {
        uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
      }
    }
    await fs.writeFileSync("./output/theuniversityofnotredameaustralia_unique_courselist.json", JSON.stringify(uniqueUrl));
    //based on unique urls mapping of categories
    for (let i = 0; i < totalCourseList.length; i++) {
      for (let j = 0; j < uniqueUrl.length; j++) {
        if (uniqueUrl[j].href == totalCourseList[i].href) {
          if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

          } else {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }

        }
      }
    }
    await fs.writeFileSync("./output/theuniversityofnotredameaustralia_courselist.json", JSON.stringify(uniqueUrl));
    console.log(funcName + 'writing course list to file....');
    console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
  }

  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });

      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);

      //set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }
      //return totalCourseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // refers to https://futurestudents.csu.edu.au/courses/all
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // course_level_form_selector key
      // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelList) {
      //   console.log(funcName + 'Invalid formSelList');
      //   throw (new Error('Invalid formSelList'));
      // }
      // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelElementDict) {
      //   console.log(funcName + 'Invalid formSelElementDict');
      //   throw (new Error('Invalid formSelElementDict'));
      // }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      // const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      // console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
