const puppeteer = require('puppeteer');
const fs = require("fs");
async function start() {
    const URL = "https://www.notredame.edu.au/study/programs";
    let browser = await puppeteer.launch({ headless: false }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    // let selector = "#modalTitle > button";

    //click international dropdown
    let internationalStudSel = "#degree-student-type > option:nth-child(2)";
    await page.select("#degree-student-type", "all|international");
    const categoryselectorUrl = "div.course-tiles__item > a";
    const categoryselectorText = "div.course-tiles__item > a";
    var elementstring = "", elementhref = "", allcategory = [];
    var totalCourseList = [];
    const targetLinksCardsUrls = await page.$$(categoryselectorUrl);
    const targetLinksCardsText = await page.$$(categoryselectorText);
    for (let categories of targetLinksCardsUrls) {
        elementstring = await page.evaluate(el => el.innerText, categories);
        console.log("categories -->", elementstring);
        await categories.click();
        let linkselector = "#program-container > ul > li > a.program-list__title";
        let textselector = "#program-container > ul > li > a.program-list__title >span:nth-child(1)"
        
        const targetLinks = await page.$$(linkselector);
        const targetText = await page.$$(textselector);
        console.log("#total link selectors---->" + targetLinks.length);
        for (var i = 0; i < targetLinks.length; i++) {
            var elementstring1 = await page.evaluate(el => el.innerText, targetText[i]);
            const elementlink = await page.evaluate(el => el.href, targetLinks[i]);
            totalCourseList.push({ href: elementlink, innerText: elementstring1, category: elementstring });
        }


    }
    // var targetLinksCardsTotal = [];
    // for (let i = 0; i < targetLinksCardsUrls.length; i++) {
    //     elementstring = await page.evaluate(el => el.innerText, targetLinksCardsText[i]);
    //     elementhref = await page.evaluate(el => el.href, targetLinksCardsUrls[i]);
    //     targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
    // }
    // // await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
    // console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));


    // let linkselector = "#ajax-course-list > article > h3 > a";
    // let textselector = "#ajax-course-list > article > h3 > a"
    // var totalCourseList = [];
    // for (let target of targetLinksCardsTotal) {
    //     await page.goto(target.href, { timeout: 0 });
    //     //scrape university courselist
    //     // let courseButtonClick = "//a[contains(@title,'Courses')]";
    //     // var clickbtn = await page.$x(courseButtonClick);
    //     // console.log("Button");
    //     // await clickbtn[0].click();
    //     // console.log("Clicked");
    //     // let check = "#filter-graduate";
    //     // await page.evaluate((check) => document.querySelector(check).click(), check);
    //     await page.waitFor(5000);
    //     const targetLinks = await page.$$(linkselector);
    //     const targetText = await page.$$(textselector);
    //     await page.waitFor(5000);
    //     console.log("target.innerText -->", target.innerText);
    //     console.log("#total link selectors---->" + targetLinks.length);
    //     for (var i = 0; i < targetLinks.length; i++) {
    //         var elementstring = await page.evaluate(el => el.innerText, targetText[i]);
    //         const elementlink = await page.evaluate(el => el.href, targetLinks[i]);
    //         totalCourseList.push({ href: elementlink, innerText: elementstring, category: target.innerText });
    //     }
    // }
    console.log("totalCourseList -->", totalCourseList);
    fs.writeFileSync("theuniversityofnotredameaustralia_category_courselist.json", JSON.stringify(totalCourseList));
    await browser.close();

}
start();
