const fs = require('fs');
const awsUtil = require('./common/aws_utils');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    static titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    }
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name, study_level) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = category_name;
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {};
                            const pteDict = {};
                            const tofelDict = {};
                            const pbtDict = {};
                            const caeDict = {};
                            let ieltsNumber = null;
                            console.log("category_name val -> ", category_name);
                            const study_level = resJsonData.course_study_level;
                            const title = resJsonData.course_title;
                            var category = "";
                            //set category
                            if (category_name.indexOf("Arts & Sciences") > -1) {
                                category = "Arts & Sciences";
                            }
                            else if (category_name.indexOf("Business") > -1) {
                                category = "Business";
                            }
                            else if (category_name.indexOf("Education") > -1) {
                                category = "Education";
                            }
                            else if (category_name.indexOf("Health Sciences") > -1) {
                                category = "Health Sciences";
                            }
                            else if (category_name.indexOf("Law") > -1) {
                                category = "Law";
                            }
                            else if (category_name.indexOf("Medicine") > -1) {
                                category = "Medicine";
                            }
                            else if (category_name.indexOf("Nursing & Midwifery") > -1) {
                                category = "Nursing & Midwifery";
                            }
                            else if (category_name.indexOf("Philosophy & Theology") > -1) {
                                category = "Philosophy & Theology";
                            }
                            else if (category_name.indexOf("Physiotherapy") > -1) {
                                category = "Physiotherapy";
                            }


                            console.log("R ielts category", category);
                            console.log("R ielts study_level", study_level, "R ielts title", title);

                            if (study_level && study_level.length > 0) {
                                const ieltsMappingDict = JSON.parse(fs.readFileSync("./selectors/ielts_mapping.json"));
                                //console.log("R ieltsMappingDict", JSON.stringify(ieltsMappingDict[1]));                                
                                console.log("R ieltsMappingDict[0].Graduate[0].SchoolOfDesign[0].programs", JSON.stringify(ieltsMappingDict[0].default[0].undergrad));
                                if (study_level == "Undergraduate") {
                                    //underGrad
                                    if (category == "Education" || title.toLowerCase() == "bachelor of health & physical education (primary)" || title.toLowerCase() == "bachelor of health & physical education (secondary)" || title.toLowerCase() == "bachelor of laws" || title.toLowerCase() == "bachelor of laws (graduate entry)" || title.toLowerCase() == "bachelor of nursing" || category == "Physiotherapy") {
                                        //special cases
                                        switch (category) {
                                            case "Education": {
                                                //special for all undergrad
                                                ieltsDict.name = 'ielts academic';
                                                ieltsDict.description = ieltsMappingDict[0].education[0].special.ielts;
                                                englishList.push(ieltsDict);
                                                courseAdminReq.english = englishList;
                                                break;
                                            }
                                            case "Health Sciences": {
                                                //special for specified undergrad
                                                ieltsDict.name = 'ielts academic';
                                                ieltsDict.description = ieltsMappingDict[0].healthSciences[0].special.ielts;
                                                englishList.push(ieltsDict);
                                                courseAdminReq.english = englishList;
                                                break;
                                            }
                                            case "Law": {
                                                //special for specified undergrad
                                                ieltsDict.name = 'ielts academic';
                                                ieltsDict.description = ieltsMappingDict[0].law[0].special.ielts;
                                                englishList.push(ieltsDict);

                                                tofelDict.name = 'toefl ibt';
                                                tofelDict.description = ieltsMappingDict[0].law[0].special.toefl_ibt;
                                                englishList.push(tofelDict);

                                                pteDict.name = 'pte academic';
                                                pteDict.description = ieltsMappingDict[0].law[0].special.pte;
                                                englishList.push(pteDict);

                                                caeDict.name = 'cae';
                                                caeDict.description = ieltsMappingDict[0].law[0].special.cae;
                                                englishList.push(caeDict);

                                                courseAdminReq.english = englishList;
                                                break;
                                            }
                                            case "Nursing & Midwifery": {
                                                //special for specified undergrad
                                                ieltsDict.name = 'ielts academic';
                                                ieltsDict.description = ieltsMappingDict[0].nursing[0].special.ielts;
                                                englishList.push(ieltsDict);

                                                tofelDict.name = 'toefl ibt';
                                                tofelDict.description = ieltsMappingDict[0].nursing[0].special.toefl_ibt;
                                                englishList.push(tofelDict);

                                                pteDict.name = 'pte academic';
                                                pteDict.description = ieltsMappingDict[0].nursing[0].special.pte;
                                                englishList.push(pteDict);

                                                courseAdminReq.english = englishList;
                                                break;
                                            }
                                            case "Physiotherapy": {
                                                //special for all undergrad
                                                ieltsDict.name = 'ielts academic';
                                                ieltsDict.description = ieltsMappingDict[0].physiotherapy[0].special.ielts;
                                                englishList.push(ieltsDict);

                                                tofelDict.name = 'toefl ibt';
                                                tofelDict.description = ieltsMappingDict[0].physiotherapy[0].special.toefl_ibt;
                                                englishList.push(tofelDict);

                                                pteDict.name = 'pte academic';
                                                pteDict.description = ieltsMappingDict[0].physiotherapy[0].special.pte;
                                                englishList.push(pteDict);

                                                courseAdminReq.english = englishList;
                                                break;
                                            }
                                        }
                                    }
                                    else {
                                        //default undergrad
                                        ieltsDict.name = 'ielts academic';
                                        ieltsDict.description = ieltsMappingDict[0].default[0].undergrad.ielts;
                                        englishList.push(ieltsDict);

                                        tofelDict.name = 'toefl ibt';
                                        tofelDict.description = ieltsMappingDict[0].default[0].undergrad.toefl_ibt;
                                        englishList.push(tofelDict);

                                        pteDict.name = 'pte academic';
                                        pteDict.description = ieltsMappingDict[0].default[0].undergrad.pte;
                                        englishList.push(pteDict);

                                        caeDict.name = 'cae';
                                        caeDict.description = ieltsMappingDict[0].default[0].undergrad.cae;
                                        englishList.push(caeDict);
                                        courseAdminReq.english = englishList;

                                    }
                                }
                                else if (study_level == "Postgraduate") {
                                    if (category == "Education" || (category == "Nursing & Midwifery" && title.toLowerCase() != "master of nursing (research)")) {
                                        //special cases
                                        switch (category) {
                                            case "Education": {
                                                //special for all postgrad
                                                ieltsDict.name = 'ielts academic';
                                                ieltsDict.description = ieltsMappingDict[0].education[0].special.ielts;
                                                englishList.push(ieltsDict);

                                                courseAdminReq.english = englishList;
                                                break;
                                            }
                                            case "Nursing & Midwifery": {
                                                //special for specified postgrad
                                                ieltsDict.name = 'ielts academic';
                                                ieltsDict.description = ieltsMappingDict[0].nursing[0].special.ielts;
                                                englishList.push(ieltsDict);

                                                tofelDict.name = 'toefl ibt';
                                                tofelDict.description = ieltsMappingDict[0].nursing[0].special.toefl_ibt;
                                                englishList.push(tofelDict);

                                                pteDict.name = 'pte academic';
                                                pteDict.description = ieltsMappingDict[0].nursing[0].special.pte;
                                                englishList.push(pteDict);

                                                courseAdminReq.english = englishList;
                                                break;
                                            }
                                        }
                                    }
                                    else {
                                        //default postgrad
                                        ieltsDict.name = 'ielts academic';
                                        ieltsDict.description = ieltsMappingDict[0].default[0].postgrad.ielts;
                                        englishList.push(ieltsDict);

                                        tofelDict.name = 'toefl ibt';
                                        tofelDict.description = ieltsMappingDict[0].default[0].postgrad.toefl_ibt;
                                        englishList.push(tofelDict);

                                        pteDict.name = 'pte academic';
                                        pteDict.description = ieltsMappingDict[0].default[0].postgrad.pte;
                                        englishList.push(pteDict);

                                        caeDict.name = 'cae';
                                        caeDict.description = ieltsMappingDict[0].default[0].postgrad.cae;
                                        englishList.push(caeDict);
                                        courseAdminReq.english = englishList;
                                    }
                                }
                                else if (study_level == "Postgraduate Research") {
                                    //Research
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsMappingDict[0].default[0].research.ielts;
                                    englishList.push(ieltsDict);

                                    tofelDict.name = 'toefl ibt';
                                    tofelDict.description = ieltsMappingDict[0].default[0].research.toefl_ibt;
                                    englishList.push(tofelDict);

                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsMappingDict[0].default[0].research.pte;
                                    englishList.push(pteDict);

                                    caeDict.name = 'cae';
                                    caeDict.description = ieltsMappingDict[0].default[0].research.cae;
                                    englishList.push(caeDict);
                                    courseAdminReq.english = englishList;
                                }
                            }


                            const regEx = /[+-]?\d+(\.\d+)?/g;
                            const matchedStrList = String(englishList[0].description).match(regEx);
                            console.log('course_toefl_ielts_score matchedStrList = ' + JSON.stringify(matchedStrList));
                            if (matchedStrList && matchedStrList.length > 0) {
                                ieltsNumber = Number(matchedStrList[0]);
                                console.log('course_toefl_ielts_score ieltsNumber = ' + ieltsNumber);
                            }
                            // academic requirement                           
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq.toString().replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ' ')];
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            // if (courseAdminReq.english || courseAdminReq.academic) {
                            //     resJsonData.course_admission_requirement = courseAdminReq;
                            // }

                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.course_admission_english_more_details;
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            // if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                            //     resJsonData.course_admission_requirement.english_more_details = resEnglishReqMoreDetailsJson;
                            // } else if (resEnglishReqMoreDetailsJson) {
                            //     resJsonData.course_admission_requirement = {};
                            //     resJsonData.course_admission_requirement.english_more_details = resEnglishReqMoreDetailsJson;
                            // }

                            //DYNAMIC PROGRESS BAR //CAN BE COPIED//PBT NOT INCLUDED
                            //progress bar start
                            let penglishList = [];
                            let pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            // extract exact number from string              
                            let matchedStrList_ielts, matchedStrList_pte, matchedStrList_pbt, matchedStrList_tofel, matchedStrList_cae;
                            let ieltsNo, pteNo, pbtNo, ibtNo, caeNo;
                            console.log("ieltsDict", JSON.stringify(ieltsDict));
                            if (ieltsDict) {
                                matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                                if (matchedStrList_ielts && matchedStrList_ielts.length > 0) {
                                    ieltsNo = Number(matchedStrList_ielts[0]);
                                }
                            }
                            if (pteDict) {
                                matchedStrList_pte = String(pteDict.description).match(regEx);
                                if (matchedStrList_pte && matchedStrList_pte.length > 0) {
                                    pteNo = Number(matchedStrList_pte[0]);
                                }
                            }
                            if (pbtDict) {
                                matchedStrList_pbt = String(pbtDict.description).match(regEx);
                                if (matchedStrList_pbt && matchedStrList_pbt.length > 0) {
                                    pbtNo = Number(matchedStrList_pbt[0]);
                                }
                            }
                            if (tofelDict) {
                                matchedStrList_tofel = String(tofelDict.description).match(regEx);
                                if (matchedStrList_tofel && matchedStrList_tofel.length > 0) {
                                    ibtNo = Number(matchedStrList_tofel[0]);
                                }
                            }
                            if (caeDict) {
                                matchedStrList_cae = String(caeDict.description).match(regEx);
                                if (matchedStrList_cae && matchedStrList_cae.length > 0) {
                                    caeNo = Number(matchedStrList_cae[0]);
                                }
                            }

                            if (ieltsNumber && ieltsNumber > 0) {
                                if (ieltsNo) {
                                    pieltsDict.name = 'ielts academic';
                                    pieltsDict.description = ieltsDict.description;
                                    pieltsDict.min = 0;
                                    pieltsDict.require = ieltsNo;
                                    pieltsDict.max = 9;
                                    pieltsDict.R = 0;
                                    pieltsDict.W = 0;
                                    pieltsDict.S = 0;
                                    pieltsDict.L = 0;
                                    pieltsDict.O = 0;
                                    penglishList.push(pieltsDict);
                                }
                                if (ibtNo) {
                                    pibtDict.name = 'toefl ibt';
                                    pibtDict.description = tofelDict.description;
                                    pibtDict.min = 0;
                                    pibtDict.require = ibtNo;
                                    pibtDict.max = 120;
                                    pibtDict.R = 0;
                                    pibtDict.W = 0;
                                    pibtDict.S = 0;
                                    pibtDict.L = 0;
                                    pibtDict.O = 0;
                                    penglishList.push(pibtDict);
                                }
                                if (pbtNo) {
                                    ppbtDict.name = 'toefl pbt';
                                    ppbtDict.description = pbtDict.description;
                                    ppbtDict.min = 0;
                                    ppbtDict.require = pbtNo;
                                    ppbtDict.max = 120;
                                    ppbtDict.R = 0;
                                    ppbtDict.W = 0;
                                    ppbtDict.S = 0;
                                    ppbtDict.L = 0;
                                    ppbtDict.O = 0;
                                    penglishList.push(ppbtDict);
                                }
                                if (pteNo) {
                                    ppteDict.name = 'pte academic';
                                    ppteDict.description = pteDict.description;
                                    ppteDict.min = 0;
                                    ppteDict.require = pteNo;
                                    ppteDict.max = 90;
                                    ppteDict.R = 0;
                                    ppteDict.W = 0;
                                    ppteDict.S = 0;
                                    ppteDict.L = 0;
                                    ppteDict.O = 0;
                                    penglishList.push(ppteDict);
                                }
                                if (caeNo) {
                                    pcaeDict.name = 'cae';
                                    pcaeDict.description = caeDict.description;
                                    pcaeDict.min = 80;
                                    pcaeDict.require = caeNo;
                                    pcaeDict.max = 230;
                                    pcaeDict.R = 0;
                                    pcaeDict.W = 0;
                                    pcaeDict.S = 0;
                                    pcaeDict.L = 0;
                                    pcaeDict.O = 0;
                                    penglishList.push(pcaeDict);
                                }
                            }
                            // progrssbar End
                            resJsonData.course_admission_requirement = {};
                            // if (courseAdminReq.english && courseAdminReq.english.length > 0) {
                            //     resJsonData.course_admission_requirement.english = courseAdminReq.english;
                            // }
                            if (courseAdminReq.academic && courseAdminReq.academic.length > 0) {
                                resJsonData.course_admission_requirement.academic = courseAdminReq.academic;
                            }
                            // add english requirement 'english_more_details' link
                            const entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            const academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            const english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            if (entry_requirements_url && entry_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            }
                            if (academic_requirements_url && academic_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;
                            }
                            if (english_requirements_url && english_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                            }
                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.englishprogress = penglishList;
                                resJsonData.course_admission_requirement.english = courseAdminReq.englishprogress;
                            }

                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            //CUSTOMIZED CODE DO NOT COPY
                            const courseDurationList = [];
                            const courseDurationDisplayList = [];
                            const durationFullTime = {};
                            let not_full_year_formated_data;
                            let durationText = courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0];
                            let durationFullTimeDisplay = null;

                            console.log("*************start formating years*************************");
                            console.log("R full-time", JSON.stringify(courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0]));
                            let _duration;
                            if (courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].indexOf("full-time") > -1) {
                                if (courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0] && courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].length > 0) {
                                    console.log("*************start formating Full Time years*************************");
                                    let temp = courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0];
                                    _duration = await format_functions.validate_course_duration_full_time(courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].toString().replace("or", " "));
                                    console.log("_duration", _duration);
                                    var full_year_formated_data = [{
                                        "unit": _duration[0].unit,
                                        "duration": _duration[0].duration,
                                        "display": _duration[0].display
                                    }];
                                    //full_year_formated_data[0].display = "Full-Time";
                                    console.log("R full_year_formated_data", full_year_formated_data);
                                    ///course duration
                                    not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                                    durationFullTime.duration_full_time = not_full_year_formated_data;
                                    courseDurationList.push(durationFullTime);
                                    ///END course duration

                                    ///course duration display
                                    durationFullTimeDisplay = {};
                                    durationFullTimeDisplay = full_year_formated_data;
                                    console.log("durationFullTimeDisplay", durationFullTimeDisplay);
                                    courseDurationDisplayList.push(durationFullTimeDisplay);

                                    ///END course duration display
                                    console.log('***************END formating Full Time years**************************')
                                }
                            } else {
                                let partitmevar = courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0];
                                console.log("parttime variable -->", partitmevar);
                                let _duration = await format_functions.validate_course_duration_full_time(courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0]);
                                var full_year_formated_data = [{
                                    "unit": _duration[0].unit,
                                    "duration": _duration[0].duration,
                                    "display": _duration[0].display
                                }];

                                durationFullTimeDisplay = {};
                                durationFullTimeDisplay = full_year_formated_data;
                                console.log("durationFullTimeDisplay", durationFullTimeDisplay);
                                courseDurationDisplayList.push(durationFullTimeDisplay);
                                console.log("parttime variable _duration-->", _duration);
                                const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                                durationFullTime.duration_full_time = not_full_year_formated_data;
                                courseDurationList.push(durationFullTime);
                            }

                            if (durationText.indexOf("equivalent") > -1) {
                                if (_duration && _duration.length > 0) {
                                    console.log("*************start formating Part Time years*************************");
                                    let durationPartTimeDisplay = null;
                                    const durationPartTime = {};
                                    var partyear_formated_data = [{
                                        "unit": _duration[1].unit,
                                        "duration": _duration[1].duration,
                                        "display": _duration[1].display
                                    }];
                                    var parttimeduration = JSON.stringify(partyear_formated_data);
                                    if (parttimeduration != '[]') {

                                        ///parttime course duration
                                        // const not_parttime_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_part_time);
                                        // durationPartTime.duration_part_time = not_parttime_year_formated_data;
                                        ///End Parttime course duration

                                        ///partitme course duration display
                                        durationPartTimeDisplay = {};
                                        durationPartTimeDisplay = partyear_formated_data[0];
                                        ///END parttime course duration display

                                        if (durationPartTimeDisplay) {
                                            console.log("durationPartTimeDisplay", durationPartTimeDisplay);
                                            courseDurationDisplayList[0].push(durationPartTimeDisplay);
                                        }
                                    }
                                    ///END course duration display
                                    console.log('***************END formating Part Time years**************************')
                                }
                            }
                            let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                            console.log("filtered_duration_formated -->", filtered_duration_formated);
                            if (not_full_year_formated_data && not_full_year_formated_data.length > 0) {
                                resJsonData.course_duration = not_full_year_formated_data;
                            }
                            if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                resJsonData.course_duration_display = filtered_duration_formated;
                                var isfulltime = false, isparttime = false;
                                filtered_duration_formated.forEach(element => {
                                    if (element.display == "Full-Time") {
                                        isfulltime = true;
                                    }
                                    if (element.display == "Part-Time") {
                                        isparttime = true;
                                    }
                                });
                                resJsonData.isfulltime = isfulltime;
                                resJsonData.isparttime = isparttime
                            }
                            console.log("*************END formating years*************************");
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            let tmpvar_cat;
                            let Pro_code = resJsonData.program_code
                            let category_val = category_name;
                            console.log("category_val--->>>", category_val);
                            //  Pro_code = Course_Code;
                            console.log("Course_Code--->>>", Pro_code);
                            let feesget = JSON.parse(fs.readFileSync(appConfigs.all_fees));

                            let tmpvar = feesget.filter(val => {
                                return val.Course_Code == Pro_code
                            });
                            console.log("tmpvar------->>>>", tmpvar);
                            let temp = String(tmpvar)
                            console.log("temp------->", temp)

                            if (temp != "") {
                                console.log("tempifff------->", temp)
                                tmpvar_cat = tmpvar.filter(val => {
                                    return val.School.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase() == String(category_val).replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase()
                                });
                                console.log("tmpvar_cat======++++>", tmpvar_cat)
                                console.log("tmpvar_cat------->>>>", tmpvar_cat[0].Cost);
                                let Tuition_Fee = tmpvar_cat[0].Cost
                                console.log("tmpvar_cat[0].Cost======?>", Tuition_Fee)

                                //   let feeUrl = "https://www.notredame.edu.au/study/fees";
                                //  feesDict.international_student_all_fees.push("To view unit wise fee information refer to: " + feeUrl);
                                const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                                if (feeYear && feeYear.length > 0) {
                                    courseTuitionFee.year = feeYear;
                                }
                                // if we can extract value as Int successfully then replace Or keep as it is
                                if (Tuition_Fee && Tuition_Fee.length > 0) { // extract only digits
                                    const feesWithDollorTrimmed = String(Tuition_Fee).trim();
                                    console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                    const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                    const arrval = String(feesVal1).split('.');
                                    const feesVal = String(arrval[0]);
                                    console.log(funcName + 'feesVal = ' + feesVal);
                                    if (feesVal) {
                                        const regEx = /\d/g;
                                        let feesValNum = feesVal.match(regEx);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            let feesNumber = null;
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }

                                            console.log(funcName + 'feesNumber = ' + feesNumber);
                                            if (Number(feesNumber)) {
                                                feesDict.international_student = ({
                                                    amount: Number(feesNumber),
                                                    duration: 1,
                                                    unit: "year",
                                                    //isfulltime: true,
                                                    description: Tuition_Fee,
                                                    // type: ""
                                                });
                                            } else {
                                                feesDict.international_student = ({
                                                    amount: 0,
                                                    duration: 1,
                                                    unit: "year",
                                                    // isfulltime: true,
                                                    description: "not available fee",
                                                    //  type: ""
                                                });
                                            }
                                            console.log("feesDictinternational_student-->", feesDict.international_student);
                                        }
                                    }
                                } else {
                                    feesDict.international_student = ({
                                        amount: 0,
                                        duration: 1,
                                        unit: "year",
                                        // isfulltime: true,
                                        description: "not available fee",
                                        //  type: ""
                                    });
                                } // if (feesIntStudent && feesIntStudent.length > 0)
                                // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                                const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                                console.log(funcName + 'cricoseStr = ' + cricoseStr);
                                console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                                if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                    const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                    if (fixedFeesDict) {
                                        for (const codeKey in fixedFeesDict) {
                                            console.log(funcName + 'codeKey = ' + codeKey);
                                            if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                                const keyVal = fixedFeesDict[codeKey];
                                                console.log(funcName + 'keyVal = ' + keyVal);
                                                if (cricoseStr.includes(codeKey)) {
                                                    const feesDictVal = fixedFeesDict[cricoseStr];
                                                    console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                    if (feesDictVal && feesDictVal.length > 0) {
                                                        const inetStudentFees = Number(feesDictVal);
                                                        console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                        if (Number(inetStudentFees)) {
                                                            feesDict.international_student = Number(inetStudentFees);
                                                        }
                                                    }
                                                }
                                            }
                                        } // for
                                    } // if
                                } // if

                                if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                    const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                    const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                    if (feesDuration && feesDuration.length > 0) {
                                        feesDict.fee_duration_years = feesDuration;
                                    }
                                    //    feesDict.international_student_all_fees = [];
                                    // if (international_student_all_fees_array && international_student_all_fees_array.length > 0) {

                                    //     for (let student_all_fees of international_student_all_fees_array) {
                                    //         feesDict.international_student_all_fees.push(student_all_fees.replace(/[\r\n\t ]+/g, ' ').replace('-', '').trim());
                                    //     }
                                    // }
                                    if (feesCurrency && feesCurrency.length > 0) {
                                        feesDict.currency = feesCurrency;
                                    }
                                    if (feesDict) {
                                        var campus = resJsonData.course_campus_location;
                                        for (let loc of campus) {
                                            feesList.push({ name: loc.name, value: feesDict });
                                        }
                                    }
                                    if (feesList && feesList.length > 0) {
                                        courseTuitionFee.fees = feesList;
                                    }
                                    console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                    if (courseTuitionFee) {
                                        resJsonData.course_tuition_fee = courseTuitionFee;
                                    }
                                    // take tuition fee value at json top level so will help in DDB for indexing
                                    if (courseTuitionFee && feesDict.international_student) {
                                        resJsonData.course_tuition_fee = courseTuitionFee;
                                        // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                    }
                                }
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                                if (!feesDict.international_student) {
                                    console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                    console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                    console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                    console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                    return null; // this will add this item into FailedItemList and writes file to local disk
                                }

                            }
                            else {
                                console.log("tempelse------->", temp)
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: 1,
                                    unit: "year",
                                    description: "not available fee"

                                });

                                console.log("fesss########", resJsonData.course_tuition_fee)
                                var fee_desc_more = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                                let courseKeyVal_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_more);
                                console.log("courseKeyVal_more@@1", courseKeyVal_more);

                                fee_desc_more.push(courseKeyVal_more)
                                if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                    const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                    const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                    if (feesDuration && feesDuration.length > 0) {
                                        feesDict.fee_duration_years = feesDuration;
                                        //  feesDict.international_student_all_fees = fee_desc_more
                                        console.log("morefees@@2", fee_desc_more);

                                    }
                                    if (feesCurrency && feesCurrency.length > 0) {
                                        feesDict.currency = feesCurrency;
                                    }
                                    if (feesDict) {
                                        var campus = resJsonData.course_campus_location;
                                        for (let loc of campus) {
                                            feesList.push({ name: loc.name, value: feesDict });
                                        }
                                    }
                                    if (feesList && feesList.length > 0) {
                                        courseTuitionFee.fees = feesList;
                                    }
                                    console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                    if (courseTuitionFee) {
                                        resJsonData.course_tuition_fee = courseTuitionFee;
                                    }

                                    // take tuition fee value at json top level so will help in DDB for indexing
                                    if (courseTuitionFee && feesDict.international_student) {
                                        resJsonData.course_tuition_fee = courseTuitionFee;
                                        // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                    }

                                }


                            }



                            //



                            break
                        }
                        case 'course_campus_location': { // Location Launceston
                            console.log('course_campus_location matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedSelStr = [];
                            for (const rootEleDict of rootElementDictList) {
                                console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                // let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        if (selList && selList.length > 0) {
                                            console.log('course_campus_location selList = ' + JSON.stringify(selList));
                                            for (var selItem of selList) {
                                                console.log('course_campus_location selItem = ' + JSON.stringify(selItem));
                                                if (selItem.indexOf(",") > -1) {
                                                    selItem = selItem.split(",");
                                                    for (let sel of selItem) {
                                                        sel = sel.replace(/[\r\n\t ]+/g, ' ');
                                                        sel = sel.replace(":", " ");
                                                        concatnatedSelStr.push(sel.trim());
                                                    }
                                                }
                                                else {
                                                    selItem = selItem.replace(/[\r\n\t ]+/g, ' ').replace(":", ",");
                                                    concatnatedSelStr.push(selItem.trim());
                                                }
                                            } // selList
                                            console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);
                                        }
                                    } // selectorsList                  
                                } // elementsList                
                            } // rootElementDictList 

                            // add only if it has valid value              
                            if (concatnatedSelStr && concatnatedSelStr.length > 0) {

                                // if (location_val && location_val.length > 0) {
                                var campusedata = [];
                                concatnatedSelStr.forEach(element => {
                                    element = this.titleCase(element);
                                    campusedata.push({
                                        "name": element,
                                        "code": ""
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                resJsonData.course_study_mode = "On campus";
                                console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

                            }
                            else {
                                throw new Error("No campus location found.");
                            }
                            // resJsonData.course_campus_location = concatnatedSelStr;
                            // console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);
                            //  }
                            break;
                        }
                        case 'course_intake': {
                            var courseIntake = [];
                            var more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            const IntakeStr = await utils.getValueFromHardCodedJsonFile('course_intake');
                            var courseIntakeStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            if (courseIntakeStr) {
                                if (courseIntakeStr.indexOf("or") > -1) {
                                    courseIntakeStr = courseIntakeStr.split("or");
                                    console.log("courseIntakeStr", courseIntakeStr);
                                }
                                if (Array.isArray(courseIntakeStr)) {
                                    for (let intake of courseIntakeStr) {
                                        if (intake.indexOf("1") > -1) {
                                            console.log("IntakeStr[0]", IntakeStr[0]);
                                            courseIntake.push(IntakeStr[0]);
                                        }
                                        if (intake.indexOf("2") > -1) {
                                            console.log("IntakeStr[1]", IntakeStr[1]);
                                            courseIntake.push(IntakeStr[1]);
                                        }
                                        // intake = intake.split("-")[1];
                                        // intake = intake.trim() + ", 2019";

                                    }
                                }
                                else {
                                    if (courseIntakeStr.indexOf("1") > -1) {
                                        console.log("IntakeStr[0]", IntakeStr[0]);
                                        courseIntake.push(IntakeStr[0]);
                                    }
                                    // courseIntakeStr = courseIntakeStr.split("-")[1];
                                    // courseIntakeStr = courseIntakeStr.trim() + ", 2019";
                                    // courseIntake.push(courseIntakeStr);
                                }

                            } else {

                                let courseIntakeDisplay = [];
                                var campus = resJsonData.course_campus_location;
                                if (courseIntakeDisplay) {
                                    console.log("In Main If", courseIntakeDisplay);
                                    var myintake = [];
                                    var intakedata = {}
                                    let arr = [];
                                    console.log("arr--->", arr)
                                    for (let i = 0; i < campus.length; i++) {
                                        var intakedetail = {};
                                        console.log("campus--->", campus[i].name)
                                        intakedetail.name = campus[i].name;
                                        intakedetail.value = arr;
                                        myintake.push(intakedetail)
                                        console.log("CourseDetails-->", JSON.stringify(myintake))
                                    }
                                    intakedata.intake = myintake
                                    intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                    resJsonData.course_intake = intakedata;
                                }
                            }

                            var locationArray = resJsonData.course_campus_location;
                            console.log("locationArray", locationArray);

                            if (courseIntake && courseIntake.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let location of locationArray) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location.name,
                                        "value": courseIntake
                                    });
                                }
                                let formatIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "MOM", "");
                                // console.log("formatIntake -->", formatIntake);
                                console.log("FormatIntake", JSON.stringify(formatIntake))
                                // formatIntake.forEach(element => {
                                //     element.value.forEach(elementdate => {
                                //         if (elementdate.actualdate == '17 February, 2020') {
                                //             elementdate.filterdate = "2020-02-17"
                                //         } else if (elementdate.actualdate == '27 July, 2020') {
                                //             elementdate.filterdate = "2020-07-27"
                                //         }
                                //         //elementdate.push({filterdate:myintake_data["term 1_date"]})
                                //         console.log("DATA--->", elementdate)

                                //     })
                                // });
                                resJsonData.course_intake.intake = formatIntake;
                                resJsonData.course_intake.more_details = more_details;
                            }
                            break;
                        }


                        case 'course_country':
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            } else {
                                resJsonData[key] = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            console.log(funcName + 'matched case course_career_outcome: ' + key);
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            var course_career_outcome = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            } // rootElementDictList
                            // add only if it has valid value
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            } else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }
                        case 'course_cricos_code': {
                            //CUSTOMIZED CODE DO NOT COPY. PROGRAM CODE AND CRICOS CODE COMBINED
                            const courseKeyVal = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            let program_code = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log('course_cricos_code rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log('course_cricos_code eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log('course_cricos_code selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                if (selList.toString().indexOf(',') > -1) {
                                                    var codes = selList.toString().split(',');
                                                    var cricoscode = [codes[1].replace("code", "Code").split("Code")[1].trim()];
                                                    course_cricos_code = cricoscode;
                                                    console.log('Cricos code for value :' + course_cricos_code);
                                                    if (codes[0].indexOf(":") > -1) {
                                                        program_code = [codes[0].split(":")[1].trim()];
                                                    }
                                                    else {
                                                        if (codes[0].toLowerCase().indexOf('program code') > -1 || codes[0].toLowerCase().indexOf('course code') > -1) {
                                                            console.log("codes[0].indexOf(_)")
                                                            program_code = [codes[0].replace("code", "Code").split("Code")[1].trim()];
                                                        }
                                                        else {
                                                            program_code = [codes[0].trim()];
                                                        }
                                                    }
                                                }
                                                else {
                                                    var codes = selList.toString().split('CRICOS');
                                                    var cricoscode = [codes[1].split("Code")[1].trim()];
                                                    course_cricos_code = cricoscode;
                                                    console.log('else cricos code for value :' + course_cricos_code);
                                                    if (codes[0].indexOf(":") > -1) {
                                                        program_code = [codes[0].split(":")[1].trim()];
                                                    }
                                                    else {
                                                        program_code = [codes[0].split("Code")[1].trim()];
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            if (course_cricos_code && course_cricos_code.length > 0) {
                                global.cricos_code = course_cricos_code[0];
                                var locations = resJsonData.course_campus_location;
                                var mycodes = [];
                                for (let location of locations) {
                                    mycodes.push({
                                        name: location.name, code: course_cricos_code.toString().trim()
                                    });
                                }
                                resJsonData.course_campus_location = mycodes;
                            }
                            else {
                                throw new Error("Cricos code undefined");
                            }
                            if (program_code) {
                                for (let program_val of program_code) {
                                    resJsonData.program_code = program_val;
                                    console.log("resJsonData.program_code", resJsonData.program_code);
                                }

                            }

                            break;
                        }
                        case 'program_code': {
                            break;
                        }
                        case 'course_title': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr course title = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr.replace("\n", " ");
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).replace("\n", " ").concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr course title = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr course title = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr course title = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                concatnatedRootElementsStr = this.titleCase(concatnatedRootElementsStr);
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            }
                            break;
                        }
                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };
                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal111 = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal111------->", JSON.stringify(courseKeyVal111))
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal111)) {
                                for (const rootEleDict of courseKeyVal111) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }
                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }
                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)
                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;
                            break;
                        }
                        case 'application_fee':
                            {
                                // let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                // if (application_fee.length > 0) {
                                //     resJsonData.application_fee = application_fee;
                                // } else {
                                resJsonData.application_fee = '';
                                // }

                                break;
                            }
                        case 'course_study_level': {
                            console.log(funcName + 'course_title course_study_level = ' + study_level);
                            resJsonData.course_study_level = study_level.trim();
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = course_name.replace('/', '').replace(/\s/g, '').replace(/:/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = course_name;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "NA";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;


                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

            }
            return resJsonData;

        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }
    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
