const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');

const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({
        headless: false
      });
      // course_level_selector key
      const courseListSel = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!courseListSel) {
        console.log(funcName + 'Invalid courseListSel');
        throw (new Error('Invalid courseListSel'));
      }
      const elementDictForCourseListSel = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCourseListSel) {
        console.log(funcName + 'Invalid elementDictForCourseListSel');
        throw (new Error('Invalid elementDictForCourseListSel'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      console.log('---------------------------call selector-------------------------------')
      await utils.checkandclick(s.page, "div.results-action > button.results-action-load-more");
      // anchor list selector of course list
      const totalCourseList = await s.scrapeElement(elementDictForCourseListSel.elementType, courseListSel, rootEleDictUrl, null);
      console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));

      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      if (s) {
        await s.close();
      }
      return totalCourseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({
        headless: false
      });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      // for each course-category, scrape all courses
      let totalCourseList = [];
      let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  async scrapeCourseListCategorywise() {
    const funcName = 'scrapeCourseListCategorywise';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];
      var studylevelarr = [];
      await s.setupNewBrowserPage("https://www.uts.edu.au/future-students");
      var mainCategory = [], proccess = [];
      const maincategoryselector = "//*[@id='block-mainpagecontent']/main/ul/li[1]/nav/div/ul/li/a";
      var category = await s.page.$x(maincategoryselector);
      for (let linkmain of category) {
        var categorystringmain = await s.page.evaluate(el => el.textContent, linkmain);
        var categorystringmainurl = await s.page.evaluate(el => el.href, linkmain);
        mainCategory.push(categorystringmain.trim());
        proccess.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
      }
      //subcategory
      for (let subcategory of proccess) {
        await s.page.goto(subcategory.href, { timeout: 0 });
        const subcategoryurl = "//*[@id='block-mainpagecontent']/div/div[2]/div/aside/div/div/div[2]/nav/div/div/div/div/ul/li/div/span/a"
        var subcategorylink = await s.page.$x(subcategoryurl)
        console.log("category click :" + subcategorylink);
        var categoryurl = [];
        for (let links of subcategorylink) {
          var subcategorystringmainurl = await s.page.evaluate(el => el.href, links);
          categoryurl.push(subcategorystringmainurl);
        }
        for (let subcategorys of categoryurl) {
          await s.page.goto(subcategorys, { timeout: 0 })
          const studylevelselecotor = "//*[@id='block-mainpagecontent']/div/main/div/div/div/div/div/ul/li/a"
          const studyleveltextselector = "//*[@id='block-mainpagecontent']/div/main/div/div/div/div/div/ul/li/a/div/span"
          var studylevelclick = await s.page.$x(studylevelselecotor)
          var studyleveltextclick = await s.page.$x(studyleveltextselector)
          for (let j = 0; j < studylevelclick.length; j++) {
            const studylevelurl = await s.page.evaluate(el => el.href, studylevelclick[j])
            const studyleveltext = await s.page.evaluate(el => el.innerText, studyleveltextclick[j])
            studylevelarr.push({ href: studylevelurl.trim(), innerText: studyleveltext.trim(), category: subcategory.innerText })
          }
          console.log("Studylavelarr", studylevelarr)
        }
      }
      for (let k = 0; k < studylevelarr.length; k++) {

        const studylevelurl = await s.page.evaluate(el => el.href, studylevelarr[k])
        console.log("URL@@@", studylevelarr[k].innerText.toLowerCase())
        await s.page.goto(studylevelurl, { timeout: 0 });
        //studylevelarr.innerText.toLowerCase()
        console.log("YESSSSSS")
        var levelstring = studylevelarr[k].innerText.toLowerCase()
        const selectors = "//*[@id='block-mainpagecontent']/div/main/div/div/div/div/div/ul/li/a/div/span[contains(text(), '" + studylevelarr[k].innerText + "')]/following::div[contains(@id,'" + levelstring + "')]/div/table/tbody/tr/td/a"
        var category = await s.page.$x(selectors);
        console.log("CourseURl@@@@", selectors)
        for (let link of category) {
          var categorystring = await s.page.evaluate(el => el.innerText, link);
          var categoryurl = await s.page.evaluate(el => el.href, link);
          console.log('category name :' + categorystring);
          totalCourseList.push({ href: categoryurl, innerText: categorystring, category: studylevelarr[k].category, study_level: levelstring });
        }
        console.log('totalCourseList' + totalCourseList);
      }
      await fs.writeFileSync("./course_list/main_category_courses.json", JSON.stringify(proccess));

      await fs.writeFileSync("./course_list/universityoftechnologysydney_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
        }
      }
      await fs.writeFileSync("./course_list/universityoftechnologysydney_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }

          }
        }
      }
      await fs.writeFileSync("./course_list/universityoftechnologysydney_courselist.json", JSON.stringify(uniqueUrl));
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
} // class

module.exports = {
  ScrapeCourseList
};