const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, page, study_level) {
    const funcName = 'formatOutput';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id':
              {
                console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                if (!configs.univ_id && configs.univ_id.length > 0) {
                  console.log(funcName + 'configs.univ_id must have valid value');
                  throw (new Error('configs.univ_id must have valid value'));
                }
                resJsonData.univ_id = configs.univ_id;
                break;
              }
            case 'course_discipline': {
              // As of not we do not have way to get this field value
              resJsonData.course_discipline = course_category;
              break;
            }
            case 'course_url': {
              if (courseUrl && courseUrl.length > 0) {
                resJsonData.course_url = courseUrl;
              }
              break;
            }
            case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              const courseAdminReq = {};
              const englishList = [];
              var otherLngDict = null, ieltsDic = null;
              const ieltsDict = {}; const ibtDict = {}; const pteDict = {}; const caeDict = {};
              // english requirement
              var ieltsScoreD = null, ibtScoreD = null, pteScoreD = null, caeScoreD = null;
              if (courseScrappedData.course_toefl_ielts_score) {
                const ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                let extractCricos = resJsonData.course_cricos_code;

                console.log('ielts score :' + ieltsScore);
                if (ieltsScore != null) {
                  var ieltsarra = ieltsScore.toString().split('is: Academic');
                  for (var p = 0; p < ieltsarra.length; p++) {
                    console.log('IELTS Arra Value :' + ieltsarra[p])
                  }
                  if (ieltsarra[1] != null) {
                    var ScoreforIELTS = ieltsarra[1].toString().split(';');
                    for (var k = 0; k < ScoreforIELTS.length; k++) {
                      console.log('IELTS Value for loop :' + k + " - > " + ScoreforIELTS[k])
                      if (ScoreforIELTS[k].indexOf('IELTS') > -1) {
                        console.log('value for IELTS :' + ScoreforIELTS[k].replace('or ', ''));
                        ieltsScoreD = ScoreforIELTS[k].replace('or ', '');
                      }
                      else if (ScoreforIELTS[k].indexOf('TOEFL') > -1) {
                        console.log('value for TOEFL :' + ScoreforIELTS[k].replace('or ', ''));
                        if (ScoreforIELTS[k].indexOf('paper based') > -1) {
                          var tofelScore = ScoreforIELTS[k].toString().replace('or ', '');
                          var ibtScoreF = tofelScore.split(',');
                          ibtScoreD = ibtScoreF[1].toString();
                          console.log('IBT Score For Example IF :' + ibtScoreF[1]);
                        }
                        else {
                          var ibtScoreF = ScoreforIELTS[k].toString().replace('or ', '');
                          ibtScoreD = ibtScoreF;
                          console.log('IBT Score For Example ELSE :' + ibtScoreD);
                        }
                      }
                      else if (ScoreforIELTS[k].indexOf('PTE') > -1) {
                        console.log('value for PTE :' + ScoreforIELTS[k].replace('or ', ''));
                        console.log('PTE VALUES :' + ScoreforIELTS[k]);
                        pteScoreD = ScoreforIELTS[k].replace('or ', '');
                      }
                      else if (ScoreforIELTS[k].indexOf('CAE') > -1) {
                        console.log('value for CAE :' + ScoreforIELTS[k].replace('or ', ''));
                        caeScoreD = ScoreforIELTS[k].replace('or ', '');
                      }
                    }
                  }
                } else {
                  let study_level = resJsonData.course_study_level;
                  console.log("Study_Level -->", study_level);
                  // if (study_level == "Research") {
                  //   ieltsScoreD = "6.5 overall, writing 6.0";
                  //   ibtScoreD = "79–93 overall, writing 21";
                  //   pteScoreD = "58-64";
                  //   caeScoreD = "176-184";
                  // } else
                  if (study_level == "postgraduate") {
                    ieltsScoreD = "6.5 overall, writing 6.0";
                    ibtScoreD = "79–93 overall, writing 21";
                    pteScoreD = "58-64";
                    caeScoreD = "176-184";
                  } else if (study_level == "undergraduate") {
                    ieltsScoreD = "6.5 overall, writing 6.0";
                    ibtScoreD = "79–93 overall, writing 21";
                    pteScoreD = "58-64";
                    caeScoreD = "176-184";
                  } else {
                    throw new Error("Study level not found");
                  }

                }

              }
              //IELTS Score
              if (ieltsScoreD && ieltsScoreD.length > 0) {
                ieltsDict.name = 'ielts academic';
                ieltsDict.description = ieltsScoreD.trim();
                if (ieltsDict.description != "NA" && ieltsDict.description != "N/A") {
                  englishList.push(ieltsDict);
                }

              }

              //IBT SCORE
              if (ibtScoreD) {
                ibtDict.name = 'toefl ibt';
                ibtDict.description = ibtScoreD.trim();
                englishList.push(ibtDict);
              }

              //PTE SCORE      
              if (pteScoreD) {
                console.log('Pte score value :' + pteScoreD)
                pteDict.name = 'pte academic';
                pteDict.description = pteScoreD.trim();
                englishList.push(pteDict);
              }

              //CAE SCORE      
              if (caeScoreD) {
                caeDict.name = 'cae';
                caeDict.description = caeScoreD.trim();
                englishList.push(caeDict);
              }

              ///progress bar start
              const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
              var penglishList = [];

              var ieltsScore = "NA", ibtScore = "NA", pteScore = "NA", caeScore = "NA";
              if (ieltsScoreD) {
                ieltsScore = await utils.giveMeNumber(ieltsScoreD);
                console.log("### IELTS data-->" + ieltsScore);
              }
              if (ibtScoreD) {
                ibtScore = await utils.giveMeNumber(ibtScoreD);
              }
              if (pteScoreD) {
                pteScore = await utils.giveMeNumber(pteScoreD);
              }
              if (caeScoreD) {
                caeScore = await utils.giveMeNumber(caeScoreD);
              }

              if (ieltsScore != "NA") {
                pieltsDict.name = 'ielts academic';
                pieltsDict.description = ieltsDict.description;
                pieltsDict.min = 0;
                pieltsDict.require = ieltsScore;
                pieltsDict.max = 9;
                pieltsDict.R = 0;
                pieltsDict.W = 0;
                pieltsDict.S = 0;
                pieltsDict.L = 0;
                pieltsDict.O = 0;
                penglishList.push(pieltsDict);
              }
              if (ibtScore != "NA") {
                pibtDict.name = 'toefl ibt';
                pibtDict.description = ibtDict.description;
                pibtDict.min = 0;
                pibtDict.require = ibtScore;
                pibtDict.max = 120;
                pibtDict.R = 0;
                pibtDict.W = 0;
                pibtDict.S = 0;
                pibtDict.L = 0;
                pibtDict.O = 0;
                penglishList.push(pibtDict);
              }

              if (pteScore != "NA") {
                ppteDict.name = 'pte academic';
                ppteDict.description = pteDict.description;
                ppteDict.min = 0;
                ppteDict.require = pteScore;
                ppteDict.max = 90;
                ppteDict.R = 0;
                ppteDict.W = 0;
                ppteDict.S = 0;
                ppteDict.L = 0;
                ppteDict.O = 0;
                penglishList.push(ppteDict);
              }

              if (caeScore != "NA") {
                pcaeDict.name = 'cae';
                pcaeDict.description = caeDict.description;
                pcaeDict.min = 80;
                pcaeDict.require = caeScore;
                pcaeDict.max = 230;
                pcaeDict.R = 0;
                pcaeDict.W = 0;
                pcaeDict.S = 0;
                pcaeDict.L = 0;
                pcaeDict.O = 0;
                penglishList.push(pcaeDict);
              }

              if (penglishList && penglishList.length > 0) {
                courseAdminReq.english = penglishList;
              }
              ///progrssbar End

              // if (englishList && englishList.length > 0) {
              //   courseAdminReq.english = englishList;
              // }

              if (courseScrappedData.course_academic_requirement) {
                console.log(funcName + 'matched case course_academic_requirment: ' + key);
                const courseKeyVal = courseScrappedData.course_academic_requirement;
                var course_academic_requirment = []
                console.log("course_academic_requirement details -->" + JSON.stringify(courseKeyVal));
                if (Array.isArray(courseKeyVal)) {
                  for (const rootEleDict of courseKeyVal) {
                    console.log(funcName + '\n\r rootEleDict course_academic_requirement = ' + JSON.stringify(rootEleDict));
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                      console.log(funcName + '\n\r eleDict course_academic_requirement = ' + JSON.stringify(eleDict));
                      const selectorsList = eleDict.selectors;
                      for (const selList of selectorsList) {
                        console.log(funcName + '\n\r selList course_academic_requirement = ' + JSON.stringify(selList));
                        if (Array.isArray(selList) && selList.length > 0) {
                          course_academic_requirment = selList;
                        }
                      }
                    }
                  }
                } // rootElementDictList
                // add only if it has valid value
                if (course_academic_requirment.length > 0) {
                  courseAdminReq.academic = course_academic_requirment;
                }
                else {
                  courseAdminReq.academic = course_academic_requirment;
                }
              }
              // if courseAdminReq has any valid value then only assign it
              if (courseAdminReq.english || courseAdminReq.academic) {
                resJsonData.course_admission_requirement = courseAdminReq;
              }
              // add english requirement 'english_more_details' link
              const courseKeyVal = courseScrappedData.course_admission_english_more_details;
              let resEnglishReqMoreDetailsJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resEnglishReqMoreDetailsJson = selList[0];
                      }
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))
              // add english requirement 'english_more_details' link
              const courseKeyValAcademic = courseScrappedData.course_admission_academic_more_details;
              let resAcademicReqMoreDetailsJson = null;
              if (Array.isArray(courseKeyValAcademic)) {
                for (const rootEleDict of courseKeyValAcademic) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAcademicReqMoreDetailsJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                resJsonData.course_admission_requirement.entry_requirements_url = courseUrl;
                resJsonData.course_admission_requirement.english_requirements_url = courseUrl;
                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
              } else if (resEnglishReqMoreDetailsJson) {
                resJsonData.course_admission_requirement = {};
                resJsonData.course_admission_requirement.entry_requirements_url = courseUrl;
                resJsonData.course_admission_requirement.english_requirements_url = courseUrl;
                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
              }
              break;
            }
            case 'course_duration_full_time': {
              // display full time
              var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
              var fullTimeText = "";
              const courseKeyVal = courseScrappedData.course_duration_full_time;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        fullTimeText = selList[0];
                      }
                    }
                  }
                }
              }
              if (fullTimeText && fullTimeText.length > 0) {
                const resFulltime = fullTimeText;
                if (resFulltime) {
                  // durationFullTime.duration_full_time = resFulltime;
                  // courseDurationList.push(durationFullTime);
                  courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                  console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));

                  let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);

                  if (resFulltime && resFulltime.length > 0) {
                    resJsonData.course_duration = resFulltime;
                  }
                  if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                    resJsonData.course_duration_display = filtered_duration_formated;
                    var isfulltime = false, isparttime = false;
                    filtered_duration_formated.forEach(element => {
                      if (element.display == "Full-Time") {
                        isfulltime = true;
                      }
                      if (element.display == "Part-Time") {
                        isparttime = true;
                      }
                    });
                    resJsonData.isfulltime = isfulltime;
                    resJsonData.isparttime = isparttime;
                  }
                }
              }
              break;
            }
            case 'course_tuition_fee':
            case 'course_study_mode':
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_duration_part_time': {
              console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
              // No need to process anything, its already processed in other case.
              break;
            }
            case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};
              const feesList = [];
              const feesDict = {
                international_student: {}
              };
              console.log('******************************Start Tution fee****************************');
              var finalfeesInt = '';
              const fixedFeesDict = await utils.getValueFromTutionfeeJsonFile();
              var matchfee = false;
              var courseTitle = resJsonData.course_title;
              console.log('course title :' + courseTitle);
              for (var i = 0; i < fixedFeesDict.length; i++) {
                if (fixedFeesDict[i].Course_Name == courseTitle) {
                  console.log('fixedFeesDict saddam :' + ' Amount :' + fixedFeesDict[i].Fee_per_session);
                  finalfeesInt = fixedFeesDict[i].Fee_per_session;
                  matchfee = true;
                }
              }
              if (!matchfee) {
                console.log('****************Program code************************');
                const rootElementDictProList = courseScrappedData['program_code'];
                console.log('progrma code :' + JSON.stringify(rootElementDictProList));
                let concatnatedRootElementsProStr = null;
                for (const rootEleDictpro of rootElementDictProList) {
                  const elementsList = rootEleDictpro.elements;
                  for (const eleDict of elementsList) {
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      var crcodeval = selList.toString();
                      concatnatedRootElementsProStr = crcodeval;
                    }
                  }
                }
                const progvalue = concatnatedRootElementsProStr;
                const programcodeval = progvalue.toString();
                console.log('progvalue ' + progvalue);
                console.log('****************************End Progrma code***************************');
                for (var i = 0; i < fixedFeesDict.length; i++) {
                  if (fixedFeesDict[i].Course_Code.indexOf(programcodeval) > -1) {
                    console.log('fixedFeesDict saddam :' + ' Amount :' + fixedFeesDict[i].Fee_per_session);
                    finalfeesInt = fixedFeesDict[i].Fee_per_session;
                  }
                }
              }
              const feesIntStudent = finalfeesInt;
              // const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
              // if (feeYear && feeYear.length > 0) {
              //   courseTuitionFee.year = feeYear;
              // }
              // if we can extract value as Int successfully then replace Or keep as it is
              if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                const arrval = String(feesVal1).split('.');
                const feesVal = String(arrval[0]);
                console.log(funcName + 'feesVal custome = ' + feesVal);

                console.log('Full time duration text :' + fullTimeText);
                // var isfulltimeflag = false;
                var durationfee = 1;
                var unitfee = "Year";
                console.log('course duration fee :' + feesIntStudent);
                if (fullTimeText.indexOf('1 year') > -1) {
                  //  isfulltimeflag = true;
                  durationfee = 1;
                  unitfee = "Year";
                }
                if (feesVal) {
                  const regEx = /\d/g;
                  let feesValNum = feesVal.match(regEx);
                  if (feesValNum) {
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    feesValNum = feesValNum.join('');
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    let feesNumber = null;
                    if (feesValNum.includes(',')) {
                      feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                    } else {
                      feesNumber = feesValNum;
                    }
                    console.log(funcName + 'feesNumber = ' + feesNumber);

                    if (Number(feesNumber)) {
                      feesDict.international_student = ({
                        amount: Number(feesNumber),
                        duration: durationfee,
                        unit: unitfee,
                        description: feesIntStudent,
                      });
                    } else {
                      feesDict.international_student = ({
                        amount: 0,
                        duration: 1,
                        unit: demoarray.unit,
                        description: "not available fee",
                      });
                    }
                  }
                }
              }
              if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }

                if (feesIntStudent.length > 0) { // extract only digits
                  console.log("international_student" + feesIntStudent)
                  const courseKeyVal = courseScrappedData.course_tuition_fees_international_student_more_details;
                  console.log('value of feee :' + courseKeyVal);
                  if (Array.isArray(courseKeyVal)) {
                    for (const rootEleDict of courseKeyVal) {
                      console.log(funcName + '\n\r rootEleDict fee more= ' + JSON.stringify(rootEleDict));
                      const elementsList = rootEleDict.elements;
                      for (const eleDict of elementsList) {
                        console.log(funcName + '\n\r eleDict fee more= ' + JSON.stringify(eleDict));
                        const selectorsList = eleDict.selectors;
                        for (const selList of selectorsList) {
                          console.log(funcName + '\n\r selList fee more= ' + JSON.stringify(selList));
                          if (Array.isArray(selList) && selList.length > 0) {
                            //feesDict.international_student_all_fees = selList[0];
                            console.log('course fee more details :' + selList[0]);
                          }
                        }
                      }
                    }
                  }
                }
                if (feesDict) {
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                if (courseTuitionFee) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
                // take tuition fee value at json top level so will help in DDB for indexing
                if (courseTuitionFee && feesDict.international_student) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
                console.log('******************************END Tution fee****************************');
              }
              else {
                const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                if (feeYear && feeYear.length > 0) {
                  courseTuitionFee.year = feeYear;
                }
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }
                //feesDict.international_student = "";
                if (Number(inetStudentFees)) {
                  feesDict.international_student = ({
                    amount: 0,
                    duration: "",
                    unit: "",
                    description: "",
                  });
                }
                //feesDict.international_student_all_fees = [];
                if (feesDict) {
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                if (courseTuitionFee) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
              }
              console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
              if (!feesDict.international_student) {
                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                return null; // this will add this item into FailedItemList and writes file to local disk
              }
              break;
            }
            case 'course_campus_location': { // Location Launceston
              const campLocationText = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
              console.log(funcName + 'campLocationText = ' + campLocationText);

              const courseKeyVal = courseScrappedData.course_cricos_code;
              let course_cricos_code = null;
              var getCricosCode = [];
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        selList.forEach(element => {
                          getCricosCode.push(element);
                        });
                        course_cricos_code = getCricosCode;
                      }
                    }
                  }
                }
              }
              if (campLocationText && campLocationText.length > 0) {
                var campLocationValTrimmed = String(campLocationText).trim();
                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                  console.log('course campuse location array :' + campLocationValTrimmed);

                  var CampuseLocationArray = String(campLocationValTrimmed).split(',');
                  var finalLocation = "";
                  for (var s = 0; s < CampuseLocationArray.length; s++) {
                    finalLocation += String(CampuseLocationArray[s]).trim() + ",";
                  }
                  finalLocation = finalLocation.slice(0, -1);
                  let location_val = await utils.giveMeArray(finalLocation.replace(/[\r\n\t]+/g, ';'), ',');

                  if (location_val && location_val.length > 0) {
                    var campusedata = [];
                    location_val.forEach(element => {
                      campusedata.push({
                        "name": element,
                        "code": course_cricos_code.toString()
                      })
                    });
                    resJsonData.course_campus_location = campusedata;
                    console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

                  }
                  else {
                    throw new Error("No campus location found.");
                  }
                  resJsonData.course_study_mode = "On campus";

                  //resJsonData.course_study_mode = await utils.giveMeArray(studymodeType.replace(/[\r\n\t]+/g, ''), ',');;
                  console.log("## FInal string-->" + campLocationValTrimmed);
                }
              } // if (campLocationText && campLocationText.length > 0)
              break;
            }
            case 'course_intake_url': {

              break;
            }
            case 'course_intake': {
              const courseKeyVal = courseScrappedData.course_intake;
              let course_intake = [];
              let course_intake_new = [];
              var courseIntakeStr = [];
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      course_intake = selList;
                    }
                  }
                }
              }
              console.log("course_intake----->>>>", course_intake);
              const courseIntakeStr2 = await utils.getValueFromHardCodedJsonFile("course_intake");
              console.log("courseIntakeStr2----->>>>", courseIntakeStr2);
              if (String(course_intake).includes('Autumn')) {
                if (String(courseIntakeStr2).includes('Autumn')) {
                  let courseIntakeStr3 = String(courseIntakeStr2).split('Autumn =')[1];
                  let courseIntakeStr4 = String(courseIntakeStr3).split(',')[0];
                  console.log("courseIntakeStr3Autumn--->", courseIntakeStr4)
                  course_intake_new.push(courseIntakeStr4)
                  console.log("DATA--->Autumn", course_intake_new)
                }
              }
              if (String(course_intake).includes("Winter")) {
                if (String(courseIntakeStr2).includes('Winter')) {
                  let courseIntakeStr3Winter = String(courseIntakeStr2).split('Winter =')[1];
                  let courseIntakeStr4Winter = String(courseIntakeStr3Winter).split(',')[0];
                  console.log("courseIntakeStr3--->Winter", courseIntakeStr4Winter)
                  course_intake_new.push(courseIntakeStr4Winter)
                  console.log("DATA--->Winter", course_intake_new)
                }
              }
              if (String(course_intake).includes("Spring")) {
                if (String(courseIntakeStr2).includes('Spring')) {
                  let courseIntakeStr3Spring = String(courseIntakeStr2).split('Spring =')[1];
                  let courseIntakeStr4Spring = String(courseIntakeStr3Spring).split(',')[0];
                  console.log("courseIntakeStr3--->Spring", courseIntakeStr4Spring)
                  course_intake_new.push(courseIntakeStr4Spring)
                  console.log("DATA--->Spring", course_intake_new)
                }
              }
              console.log("course_intake_new -->", course_intake_new);
              if (course_intake_new.length == 0) {
                // existing intake value
                const courseIntakeStr1 = await utils.getValueFromHardCodedJsonFile("course_intak_sem");
                if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                  resJsonData.course_intake = courseIntakeStr1;
                  const intakeStrList = String(courseIntakeStr1).split(';');
                  console.log(funcName + 'intakeStrList course_intake = ' + JSON.stringify(intakeStrList));
                  courseIntakeStr = intakeStrList;
                }
                console.log("intakes" + JSON.stringify(courseIntakeStr));

              } else {
                console.log("Inside else condition course_intake_new -->", course_intake_new);
                courseIntakeStr = course_intake_new;
              }
              console.log("courseIntakeStr --> ", JSON.stringify(courseIntakeStr));
              if (courseIntakeStr && courseIntakeStr.length > 0) {
                var campus = resJsonData.course_campus_location
                console.log('Campuse location :' + campus);
                var intakes = [];
                console.log("Intake length-->" + courseIntakeStr.length);
                console.log("Campus length-->" + campus.length);
                for (var camcount = 0; camcount < campus.length; camcount++) {
                  var intakedetail = {};
                  intakedetail.name = campus[camcount].name;
                  intakedetail.value = courseIntakeStr;
                  intakes.push(intakedetail);
                }
                let formatedIntake = await format_functions.providemyintake(intakes, "mom", "");
                console.log("FormatIntake", JSON.stringify(formatedIntake))
                var intakedata = {};
                intakedata.intake = formatedIntake;
                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                intakedata.more_details = more_details;
                resJsonData.course_intake = intakedata;
              }
              break;
            }
          
            case 'program_code': {
              const courseKeyVal = courseScrappedData.program_code;
              var program_code = "";
              var getProgramCode = [];
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        program_code = selList[0];
                        console.log("program code is:" + program_code);
                      }
                    }
                  }
                }
              }
              // for (let program_val of program_code) {
              resJsonData.program_code = program_code;
              //  }
              break;
            }
            
            case 'course_outline': {
              console.log("course_outline--->")
              const outline = {
                  majors: [],
                  minors: [],
                  more_details: ""
              };
              var major, minor
              var intakedata1 = {};
              const courseKeyVal111 = courseScrappedData.course_outline_major;
              console.log("courseKeyVal111------->", JSON.stringify(courseKeyVal111))
              let resScholarshipJson = null;
              if (Array.isArray(courseKeyVal111)) {
                  for (const rootEleDict of courseKeyVal111) {
                      console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                      const elementsList = rootEleDict.elements;
                      for (const eleDict of elementsList) {
                          console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                          const selectorsList = eleDict.selectors;
                          for (const selList of selectorsList) {
                              console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                              if (Array.isArray(selList) && selList.length > 0) {
                                  resScholarshipJson = selList;
                                  console.log("resScholarshipJson", resScholarshipJson)
                              }
                          }
                      }
                  }
              }
              if (resScholarshipJson) {
                  major = resScholarshipJson
              } else {
                  major = []
              }
              const courseKeyVal1 = courseScrappedData.course_outline_minors;
              let resScholarshipJson1 = null;
              if (Array.isArray(courseKeyVal1)) {
                  for (const rootEleDict of courseKeyVal1) {
                      console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                      const elementsList = rootEleDict.elements;
                      for (const eleDict of elementsList) {
                          console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                          const selectorsList = eleDict.selectors;
                          for (const selList of selectorsList) {
                              console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                              if (Array.isArray(selList) && selList.length > 0) {
                                  resScholarshipJson1 = selList;
                              }
                          }
                      }
                  }
              }
              if (resScholarshipJson1) {
                  minor = resScholarshipJson1
              } else {
                  minor = []
              }
              let m_d = resJsonData.course_url;
              console.log("md--------->", m_d)
              var intakedata1 = {};
              intakedata1.majors = major;
              intakedata1.minors = minor;
              intakedata1.more_details = m_d;
              resJsonData.course_outline = intakedata1;
              break;
          }
          case 'application_fee':
              {
                  let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                  if (application_fee.length > 0) {
                      resJsonData.application_fee = application_fee;
                  } else {
                      resJsonData.application_fee = '';
                  }

                  break;
              }
            case 'course_career_outcome': {
              console.log(funcName + 'matched case course_career_outcome: ' + key);
              const courseKeyVal = courseScrappedData.course_career_outcome;
              var course_career_outcome = []
              console.log("course_career_outcome-->" + JSON.stringify(courseKeyVal));
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        course_career_outcome = selList;
                      }
                    }
                  }
                }
              } // rootElementDictList
              // add only if it has valid value
              resJsonData.course_career_outcome = course_career_outcome;
              break;
            }

            case 'course_country':
            case 'course_overview':
            case 'course_title': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
              // add only if it has valid value
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
              }
              break;
            }
            case 'course_study_level': {
              console.log(funcName + 'course_title course_study_level = ' + study_level);
              resJsonData.course_study_level = study_level.trim();
              break;
            }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      var NEWJSONSTRUCT = {}, structDict = [];
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
        console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        console.log('*********************intake error*********************')
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        }
        else {
          NEWJSONSTRUCT.course_intake = "";
        }
        for (let myfees of resJsonData.course_tuition_fee.fees) {
          if (myfees.name == location) {
            NEWJSONSTRUCT.international_student_all_fees = myfees;
          }
        }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        resJsonData.course_id = location_wise_data.course_location_id;
        let basecourseid = location_wise_data.course_id;

        var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));



      }
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = 'scrapeAndFormatCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);
      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }
      const courseUrl = courseDict.href;
      const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file

      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }

      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, s.page, courseDict.study_level);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails
} // class
module.exports = {
  ScrapeCourse
};