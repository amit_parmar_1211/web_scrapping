const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
// const mapping = require('./output/Mapping/main');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, duration, location) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            console.log("courseScrappedData.program_code = ", courseScrappedData.program_code);
                            var program_code = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code.push(selList[0]);
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            for (let program_val of program_code) {
                                resJsonData.program_code = program_val;
                            }
                            break;
                        }
                        case 'course_discipline': {
                            const cTitle111 = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log("cTitle------>>>>>>", cTitle111);
                            let cricose_text = cTitle111.replace("CRICOS Course Code :", '').trim();
                            console.log("cricose_text------>>>>>>", cricose_text);
                            const category_val = await format_functions.getMyCategory(cricose_text);
                            console.log("category_val------>>>>>>", category_val);
                            resJsonData.course_discipline = [category_val.split('-')[1].trim()];
                            console.log("resJsonData.course_discipline------>>>>>>", resJsonData.course_discipline);
                            break;
                        }



                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            var penglishList = [];
                            var potherLngDict = null, ieltsdic = null;
                            // english requirement
                            let scrappedIELTS = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                            let scrappedPBT = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_pbt);
                            let scrappedIBT = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_ibt);
                            let scrappedCAE = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_cae);
                            let scrappedPTE = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_pte);
                            console.log("scrappedIELTS ->", scrappedIELTS)
                            console.log("scrappedPBT ->", scrappedPBT)
                            console.log("scrappedIBT ->", scrappedIBT)
                            console.log("scrappedCAE ->", scrappedCAE)
                            console.log("scrappedPTE ->", scrappedPTE)
                            if (scrappedIELTS != null && scrappedIELTS.length > 0) {
                                let ieltsDict = {};
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = scrappedIELTS.replace(/OR/, '').trim();
                                englishList.push(ieltsDict);

                                var ieltsScore = await utils.giveMeNumber(scrappedIELTS);
                                if (ieltsScore != []) {
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsScore;
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = ieltsDict;
                                    // englishList.push(ieltsDict);
                                }

                            }

                            // else {
                            //     let ieltsDict = {};
                            //     console.log("gdgdfgdgdfgdfg", scrappedIELTS)
                            //     ieltsDict.min = 0;
                            //     ieltsDict.require = "";
                            //     ieltsDict.max = 9;
                            //     ieltsDict.R = 0;
                            //     ieltsDict.W = 0;
                            //     ieltsDict.S = 0;
                            //     ieltsDict.L = 0;
                            //     ieltsDict.O = 0;
                            // }

                            if (scrappedPBT != null && scrappedPBT.length > 0) {
                                let ieltsDict = {};
                                ieltsDict.name = 'toefl pbt';
                                ieltsDict.description = scrappedPBT.replace(/OR/, '').trim();
                                englishList.push(ieltsDict);

                                var pbtScore = await utils.giveMeNumber(scrappedPBT);
                                if (pbtScore != []) {
                                    ieltsDict.min = 310;
                                    ieltsDict.require = pbtScore;
                                    ieltsDict.max = 677;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = value;
                                    // englishList.push(ieltsDict);
                                }
                            }
                            if (scrappedIBT != null && scrappedIBT.length > 0) {
                                let ieltsDict = {};
                                ieltsDict.name = 'toefl ibt';
                                ieltsDict.description = scrappedIBT.replace(/OR/, '').trim();
                                englishList.push(ieltsDict);

                                var ibtScore = await utils.giveMeNumber(scrappedIBT);
                                if (ibtScore != []) {
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ibtScore;
                                    ieltsDict.max = 120;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = value;
                                    // englishList.push(ieltsDict);
                                }
                            }
                            if (scrappedCAE != null && scrappedCAE.length > 0) {
                                let ieltsDict = {};
                                ieltsDict.name = 'cae';
                                ieltsDict.description = scrappedCAE.replace(/OR/, '').trim();
                                englishList.push(ieltsDict);

                                var caeScore = await utils.giveMeNumber(scrappedCAE);
                                if (caeScore != []) {
                                    ieltsDict.min = 80;
                                    ieltsDict.require = caeScore;
                                    ieltsDict.max = 230;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = value;
                                    // englishList.push(ieltsDict);
                                }
                            }
                            if (scrappedPTE != null && scrappedPTE.length > 0) {
                                let ieltsDict = {};
                                ieltsDict.name = 'pte academic';
                                ieltsDict.description = scrappedPTE.replace(/OR/, '').trim();
                                englishList.push(ieltsDict);

                                var pteScore = await utils.giveMeNumber(scrappedPTE);
                                if (pteScore != []) {
                                    ieltsDict.min = 0;
                                    ieltsDict.require = pteScore;
                                    ieltsDict.max = 90;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = value;
                                    // englishList.push(ieltsDict);
                                }
                            }

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }
                            else {
                                //   throw new Error('IELTS not found');

                                courseAdminReq.english = [];
                            }

                            //                      courseAdminReq.academic = "Academic requirements are provided by country please visit the university to know more.";
                            var course_academic_requirement = []
                            const courseKeyVal1 = courseScrappedData.course_academic_requirement;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_academic_requirement = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = (course_academic_requirement) ? course_academic_requirement : [];
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }


                            const courseKeyVal = courseScrappedData.course_entry_more_details;
                            console.log("EntryRequirement:::" + courseKeyVal);
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))

                            // add english requirement 'english_more_details' link
                            const courseKeyValAcademic = courseScrappedData.course_admission_academic_more_details;
                            console.log("AcademicRequirement:::" + courseKeyValAcademic);
                            let resAcademicReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyValAcademic)) {
                                for (const rootEleDict of courseKeyValAcademic) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAcademicReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            // add english requirement 'english_requirements_url'
                            const courseKeyValEnglishReq = courseScrappedData.course_english_more_details;
                            console.log("EnglishRequirement:::" + courseKeyValEnglishReq);
                            let resEnglishReqJson = null;
                            if (Array.isArray(courseKeyValEnglishReq)) {
                                for (const rootEleDict of courseKeyValEnglishReq) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            //const entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            const academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            const english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);

                            let entry_requirements_url;
                            if (resJsonData.course_title.indexOf("Honours") > -1) {
                                entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url_honours);
                            }
                            else if (resJsonData.course_title == "Doctor of Medicine") {
                                entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url_doctorMedicine);
                            }
                            else if (resJsonData.course_study_level == "Under Graduate") {
                                entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url_undergrad);
                            }
                            else if (resJsonData.course_study_level == "Post Graduate") {
                                console.log("Entered postgrad");
                                entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url_postgrad);
                            }
                            else if (resJsonData.course_study_level == "Research") {
                                entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url_research);
                            }
                            if (entry_requirements_url && entry_requirements_url.length > 0) {
                                console.log("Entered entry_requirements_url");
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            }
                            else if (entry_requirements_url != null) {
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                            } else {
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                            }
                            if (academic_requirements_url.length > 0 && academic_requirements_url == null) {

                                resJsonData.course_admission_requirement.academic_requirements_url = "";
                                console.log("Entered ---->academic_requirements_url", academic_requirements_url);
                            }
                            if (academic_requirements_url && academic_requirements_url.length > 0) {
                                console.log("Entered academic_requirements_url", academic_requirements_url);
                                if (academic_requirements_url == null) {
                                    resJsonData.course_admission_requirement.academic_requirements_url = "";
                                }
                                resJsonData.course_admission_requirement.academic_requirements_url = "";

                            }
                            if (english_requirements_url && english_requirements_url.length > 0) {
                                console.log("Entered english_requirements_url");
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                            }
                            break;
                        }

                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = duration.replace('Half', '0.5').trim();
                            console.log("##courseKeyVal--->", courseKeyVal)

                            if (courseKeyVal && courseKeyVal.length > 0) {
                                const resFulltime = courseKeyVal;
                                resJsonData.course_duration = String(courseKeyVal).replace('\n', ' ').trim();
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);

                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        //case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};

                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            let feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);

                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feeYear && feeYear.length > 0) {
                                courseTuitionFee.year = feeYear;
                            }
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: feesWithDollorTrimmed + " Indicative annual fee 2020",
                                            });

                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: 1,
                                                unit: "Year",
                                                description: "not available fee",
                                            });
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)

                            // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = Number(inetStudentFees);
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if

                            //let international_student_all_fees_array = [];
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    //   feesDict.international_student_all_fees = international_student_all_fees_array
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }
                        case 'course_campus_location': {
                            var campLocationText = [];
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            const Cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);

                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList campuses= ' + JSON.stringify(selList));
                                            let campuses = ['Gatton', 'St Lucia'];////
                                            for (let sel of selList) {

                                                for (let i = 0; i < campuses.length; i++) {
                                                    if (sel.toLowerCase().indexOf(campuses[i].toLowerCase()) > -1) {
                                                        //  sel = this.titleCase(sel);
                                                        campLocationText.push({
                                                            "name": sel,
                                                            "code": Cricos

                                                        });
                                                    }
                                                }
                                            }

                                        }

                                    }
                                    if (campLocationText.length == 0) {
                                        throw new Error("Campus Location not found");
                                    }
                                    console.log("Avail-->" + campLocationText)
                                    resJsonData.course_campus_location = campLocationText;

                                    resJsonData.course_study_mode = "On campus";

                                    break;
                                }
                            }
                        }
                        case 'course_study_mode': { // Location Launceston
                            //resJsonData.course_study_mode = coursestudymodeText;
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'course_intake_url': {
                            var intakeUrl = [];
                            intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                            resJsonData.course_intake_url = intakeUrl;
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            const courseIntakeDisplay = [];
                            // existing intake value
                            const courseIntakeStrElem = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            var campus = resJsonData.course_campus_location;
                            console.log("R locationArray", campus);
                            console.log('************************Start course_intake******************************');
                            console.log('courseIntakeStr : ' + courseIntakeStrElem);
                            if (courseIntakeStrElem && courseIntakeStrElem.length > 0) {
                                resJsonData.course_intake = courseIntakeStrElem;
                                const intakeStrList = String(courseIntakeStrElem).split('\n');
                                const split_coma = String(intakeStrList).split(',')
                                const split_bracet = String(split_coma).split('(')
                                const split_bracet22 = String(split_bracet).split(')')
                                console.log('course_intake intakeStrList = ' + JSON.stringify(intakeStrList));
                                console.log('course_intake split_coma = ' + JSON.stringify(split_coma));
                                console.log('course_intake split_bracet = ' + JSON.stringify(split_bracet22));
                                if (split_bracet22 && split_bracet22.length > 0) {
                                    for (var part of split_bracet22) {
                                        if (part && part.length > 0) {
                                            let prat123 = part.replace(',', '').trim()
                                            console.log("R part123", prat123);
                                            courseIntakeDisplay.push(prat123);
                                        }
                                    }
                                }
                                console.log('course_intake courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));
                            }

                            if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let location of campus) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location.name,
                                        "value": courseIntakeDisplay
                                    });
                                }
                            }
                            console.log("intakes123 -->", resJsonData.course_intake.intake);
                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            let formatedIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");
                            console.log("FormatIntake", JSON.stringify(formatedIntake))
                            var intakedata = {};
                            intakedata.intake = formatedIntake;
                            intakedata.more_details = more_details;
                            resJsonData.course_intake = intakedata;
                            break;
                        }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_country = resAccomodationCostJson;
                            }
                            break;

                        }

                        case 'course_overview': {
                            const courseKeyVal11 = courseScrappedData.course_overview;
                            let resAccomodationCostJson11 = null;
                            if (Array.isArray(courseKeyVal11)) {
                                for (const rootEleDict of courseKeyVal11) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson11 = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson11) {
                                resJsonData.course_overview = String(resAccomodationCostJson11);
                            }
                            else {
                                resJsonData.course_overview = ""
                            }
                            break;
                        }

                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome
                            } else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }

                        case 'course_title': {

                            const courseKeyValtitle = courseScrappedData.course_title;
                            let course_title = [];
                            if (Array.isArray(courseKeyValtitle)) {
                                for (const rootEleDict of courseKeyValtitle) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_title = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("course_title@@@2" + course_title);

                            //var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            var title
                            title = String(course_title).replace('(', '( ').replace('\n', '').replace(',', '');
                            console.log("splitStr@@@2" + title);
                            var ctitle = format_functions.titleCase(title).trim();
                            var ctitle2 = ctitle.replace(' ( ', '(');
                            var newString = ctitle2.replace(/ {1,}/g, " ");
                            console.log("ctitle@@@", newString);
                            resJsonData.course_title = newString.trim()
                            break;
                        }
                        case 'course_study_level': {
                            const Cricos11 = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            const study_val = await format_functions.getMyStudyLevel(Cricos11);
                            resJsonData.course_study_level = study_val;
                            console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                            break;
                        }
                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };
                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal111 = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal111------->", JSON.stringify(courseKeyVal111))
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal111)) {
                                for (const rootEleDict of courseKeyVal111) {
                                    console.log(funcName + '\n\r rootEleDict  = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict major = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList major= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }
                            // intakedata1.majors = [String(major).replace('\n    \n','').replace('\n','').trim()];
                            // intakedata1.minors = [String(minor).replace('\n    \n','').replace('\n','').trim()];


                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict minor= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict minor= ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList minor = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }
                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)
                            var intakedata1 = {};
                            intakedata1.majors = major
                            intakedata1.minors = minor
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;
                            break;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                if (application_fee.length > 0) {
                                    resJsonData.application_fee = application_fee;
                                } else {
                                    resJsonData.application_fee = '';
                                }

                                break;
                            }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            ////start genrating new file location wise

            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace('-', '').replace(/[\/]+/g, '_').toLowerCase() + "_" + resJsonData.program_code;
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;
            ///end grnrating new file location wise
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            const programClick = await s.page.$x('/html/body/div[3]/div[2]/div[2]/div/div[1]/a');
            for (let link of programClick) {
                console.log('button link :' + link);
                await link.click();
            }
            const programClick1 = await s.page.$x('//div/a[contains(text(),"international student")]');
            for (let link of programClick1) {
                console.log('button link :' + link);
                await link.click();
            }
            // await s.page.waitForNavigation();


            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.duration, courseDict.location);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
