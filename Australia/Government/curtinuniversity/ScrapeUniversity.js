const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const University = require('./common/university').University;
const configs = require('./configs');

class ScrapeUniversity extends University {
  // format output json
  async formatUnivOutput(univScrappedData) {
    const funcName = 'formatUnivOutput ';
    try {
      Scrape.validateParams([univScrappedData]);
      const resJsonData = {};
      for (const key in univScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (univScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_name must have valid value');
                throw (new Error('configs.univ_name must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'univ_name': {
              console.log(funcName + 'configs.univ_name = ' + configs.univ_name);
              if (!configs.univ_name && configs.univ_name.length > 0) {
                console.log(funcName + 'configs.univ_name must have valid value');
                throw (new Error('configs.univ_name must have valid value'));
              }
              resJsonData.univ_name = configs.univ_name;
              break;
            }
            case 'university_type':
              {
                const courseKeyVal = univScrappedData.university_type;
                let resScholarshipJson = configs.placeNAForEachEmptyValue;
                if (Array.isArray(courseKeyVal)) {
                  for (const rootEleDict of courseKeyVal) {
                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                      console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                      const selectorsList = eleDict.selectors;
                      for (const selList of selectorsList) {
                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        if (Array.isArray(selList) && selList.length > 0) {
                          resScholarshipJson = selList[0];
                        }
                      }
                    }
                  }
                  resJsonData.university_type = resScholarshipJson;
                }
                break;
              }
            case 'univ_url': {
              console.log(funcName + 'this.selectorJson.page_url = ' + this.selectorJson.page_url);
              let univUrl = '';
              if (this.selectorJson && this.selectorJson.page_url) {
                univUrl = this.selectorJson.page_url;
              }
              resJsonData.univ_url = univUrl;
              break;
            }

           
            case "contactInformation": {
              const courseKeyVal = univScrappedData.contactInformation;
              let categoryJson = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        categoryJson = selList[0];
                      }
                    }
                  }
                }
                resJsonData.contactInformation = categoryJson;
              }
              break;
            }
            case "apply_url": {
              const courseKeyVal = univScrappedData.apply_url;
              let categoryJson = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        categoryJson = selList[0];
                      }
                    }
                  }
                }
                resJsonData.apply_url = categoryJson;
              }
              break;
            }
            case "placement_url": {
              const courseKeyVal = univScrappedData.placement_url;
              let categoryJson = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        categoryJson = selList[0];
                      }
                    }
                  }
                }
                resJsonData.placement_url = categoryJson;
              }
              break;
            }
            case "brochure_path": {
              const courseKeyVal = univScrappedData.brochure_path;
              let categoryJson = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        categoryJson = selList[0];
                      }
                    }
                  }
                }
                resJsonData.brochure_path = categoryJson;
              }
              break;
            }
            case "application_fee": {
              const courseKeyVal = univScrappedData.application_fee;
              let categoryJson = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        categoryJson = selList[0];
                      }
                    }
                  }
                }
                resJsonData.application_fee = categoryJson;
              }
              break;
            }
            case "facebook_page_name": {
              const courseKeyVal = univScrappedData.facebook_page_name;
              let categoryJson = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        categoryJson = selList[0];
                      }
                    }
                  }
                }
                resJsonData.facebook_page_name = categoryJson;
              }
              break;
            }
            case "twitter_page_name": {
              const courseKeyVal = univScrappedData.twitter_page_name;
              let twitter_page_name = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        twitter_page_name = selList[0];
                      }
                    }
                  }
                }
                resJsonData.twitter_page_name = twitter_page_name;
              }
              break;
            }
            case "youtube_page_name": {
              const courseKeyVal = univScrappedData.youtube_page_name;
              let youtube_page_name = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        youtube_page_name = selList[0];
                      }
                    }
                  }
                }
                resJsonData.youtube_page_name = youtube_page_name;
              }
              break;
            }
            case "abbreviation": {
              const courseKeyVal = univScrappedData.abbreviation;
              let abbreviation = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        abbreviation = selList[0];
                      }
                    }
                  }
                }
                resJsonData.abbreviation = abbreviation;
              }
              break;
            }
            

            case 'univ_accomodation': {
              const courseKeyVal = univScrappedData.univ_accomodation;
              let resAccomodationCostJson = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
                resJsonData.univ_accomodation = resAccomodationCostJson;
              }
              break;
            }
            case 'univ_rankings': {
              const courseKeyVal = univScrappedData.univ_rankings;
              let resAccomodationCostJson = configs.placeNAForEachEmptyValue;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
                resJsonData.univ_rankings = resAccomodationCostJson;
              }
              break;
            }
            case 'course_current_year':
            case 'univ_country':
            case 'univ_logo':
            case 'univ_about': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = univScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
              resJsonData[key] = String(concatnatedRootElementsStr).trim();
              // console.log(funcName + 'resJsonData[' + key + ']= ' + JSON.stringify(resJsonData[key]));
              break;
            }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default...');
              break;
            }
          } // switch
        }
      } // for
      console.log(funcName + 'univ resJsonData =' + JSON.stringify(resJsonData));
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape everything as per selector file
  async scrapeAndFormatUniversityDetails(selectorFilepath) {
    const funcName = 'scrapeAndFormatUniversityDetails ';
    let s = null;
    try {
      Scrape.validateParams([selectorFilepath]);
      console.log(funcName + 'selectorFilepath = ' + selectorFilepath);
      const fileData = fs.readFileSync(selectorFilepath);
      const selectorJson = JSON.parse(fileData);
      this.selectorJson = selectorJson;
      s = new Scrape();
      await s.init({ headless: true });

      const univScrappedData = {};
      // validate that page_url must exist
      Scrape.validateParams([this.selectorJson.page_url]);
      // set new browser page with page url
      await s.setupNewBrowserPage(this.selectorJson.page_url);

      // for each json key in slector file
      for (const jsonKey in this.selectorJson) {
        // console.log(funcName + 'jsonKey = ' + jsonKey);
        if (this.selectorJson.hasOwnProperty(jsonKey)) {
          console.log(funcName + '\n\r ###########################      Scraping for key = ' + jsonKey + '     ###########################');
          const jsonVal = this.selectorJson[jsonKey];
          // for each element in json-array, will be always 1 item in array
          if (Array.isArray(jsonVal)) {
            const allRootElemetDictScrappedResList = [];
            for (const elementDict of jsonVal) {
              const outRootElementDict = {};
              const elementUrl = elementDict.url;
              // console.log(funcName + '    elementDict url = ' + elementUrl);
              // for each elementDict, set url or page-url
              let url = null;
              if (elementUrl && elementUrl.length > 0) {
                console.log(funcName + 'overwriting page-url with elementDict url...');
                url = elementUrl;
              }
              // else {
              //   console.log(funcName + 'setting defaulr this.selectorJson.page_url...');
              //   url = this.selectorJson.page_url;
              // }
              const elements = elementDict.elements;
              console.log(funcName + '    elementDict elements = ' + JSON.stringify(elements));
              // for each element of the json-key-dict
              if (Array.isArray(elements)) {
                const allElementsScrappedResList = [];
                for (const eleDict of elements) {
                  const outElementDict = {};
                  const eleType = eleDict.elementType;
                  // console.log(funcName + '      elementType = ' + eleType);
                  let eleOptions = null;
                  if (eleDict.option) {
                    eleOptions = eleDict.option;
                  }
                  if (eleDict.value) {
                    eleOptions = eleDict.value;
                  }
                  const eleSelctors = eleDict.selectors;
                  // for each selector of the element
                  if (Array.isArray(eleSelctors)) {
                    const allSelectorsScrappedResList = [];
                    for (const sel of eleSelctors) {
                      const scrapeElementRes = await s.scrapeElement(eleType, sel, url, eleOptions);
                      console.log(funcName + 'scrapeElementRes = ' + scrapeElementRes);
                      if (scrapeElementRes) {
                        allSelectorsScrappedResList.push(scrapeElementRes);
                      }
                    } // for each sel in selectors[]
                    console.log('**');
                    console.log(funcName + 'All selectors scrapped for the element, allSelectorsJson length = ' + allSelectorsScrappedResList.length);
                    console.log(funcName + 'allSelectorsJson = ' + JSON.stringify(allSelectorsScrappedResList));
                    console.log('**');
                    // allElementsScrappedResList.push(allSelectorsScrappedResList);
                    outElementDict.selectors = allSelectorsScrappedResList;
                    allElementsScrappedResList.push(outElementDict);
                  } // if (Array.isArray(eleSelctors)) {
                } // for each element in elements[]
                console.log('***');
                console.log(funcName + 'All elements scrapped for the key, allElementsJson length = ' + allElementsScrappedResList.length);
                console.log(funcName + 'allElementsJson = ' + JSON.stringify(allElementsScrappedResList));
                console.log('***');
                // allRootElemetDictScrappedResList.push(allElementsScrappedResList);
                outRootElementDict.elements = allElementsScrappedResList;
                allRootElemetDictScrappedResList.push(outRootElementDict);
              } // if (Array.isArray(elements)) {
            } // for (const elementDict of jsonVal)
            univScrappedData[jsonKey] = allRootElemetDictScrappedResList;
          } else { // if (Array.isArray(jsonVal)) {
            univScrappedData[jsonKey] = [];
          }
        } // if (this.selectorJson.hasOwnProperty(jsonKey)
        console.log('\n\r**********');
        console.log(funcName + 'univScrappedData[' + jsonKey + '] = ' + JSON.stringify(univScrappedData[jsonKey]));
        console.log('**********\n\r');
      } // for (const jsonKey in this.selectorJson)
      const formattedUnivData = await this.formatUnivOutput(univScrappedData);
      // const polishedUnivData = await University.placeNAForEachEmptyValue(formattedUnivData);
      // const finalCourseData = await Course.removeKeys(formattedCourseData);
      console.log(funcName + 'final scrapped json = ' + JSON.stringify(formattedUnivData));
      if (s) {
        await s.close();
      }
      // return final scrapped data
      return formattedUnivData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class
module.exports = { ScrapeUniversity };
