const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, title, studyLevel, category) {
        const funcName = 'formatOutput ';

        console.log("studyLevel, category -->" + studyLevel + " ayushi ->" + category)
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            let level=[]
                            // As of not we do not have way to get this field value
                            category.forEach(element=>{
                                if(element.includes('\n8')){
                                    level.push(element.replace('\n8',''))
                                }else{
                                    level.push(element.replace('\n',''))
                                }
                            })
                            resJsonData.course_discipline = level;
                            console.log("resJsonData.course_discipline@@@2", level);
                            break;
                        }
                        case 'course_study_level': {
                            let level
                            if(studyLevel.includes('\n')){
                                level=studyLevel.replace(/[\r\n\t]+/g,'')
                            }
                            console.log(funcName + 'course_title course_study_level = ' + studyLevel);
                            resJsonData.course_study_level = level.trim();
                            break;
                        }
                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            console.log("Title--------->", title)
                            // .replace('\n', '')
                            if (title.includes("(")) {
                                title = title.replace('(', '( ').replace('\n', '');
                                console.log("splitStr@@@2" + title);
                                var ctitle = format_functions.titleCase(title).trim();
                                var ctitle2 = ctitle.replace(' ( ', '(');
                                console.log("ctitle@@@", ctitle2.trim());
                                resJsonData.course_title = ctitle2
                            }
                            if (title.includes(",")) {
                                title = title.replace(',', '').replace('\n', '');
                                console.log("splitStr@@@2fsdfdsf" + title);
                                var ctitle = format_functions.titleCase(title).trim();
                                var ctitle2 = ctitle.replace(' , ', '');
                                console.log("ctitle@sdff@@", ctitle2.trim());
                                resJsonData.course_title = ctitle2
                            }

                            resJsonData.course_title = title

                            break;
                        }
                        case 'course_toefl_ielts_score': {
                            const englishList = [];
                            var penglishList = [];
                            const ieltsDict = {},tofelDict={},pteDict={},caeDict={}
                            var courseAdminReq = {};
                            // english requirement
                            if (courseScrappedData.course_toefl_ielts_score) {
                                var ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                               var ieltsrequiredscore= await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_required_score)
                                if (ieltsScore) {
                                    ieltsScore = ieltsScore.replace(/[\n]+/g, ',').replace(/[\t]+/g, ' ')
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsScore;
                                    ieltsDict.min = 0;
                                    ieltsDict.require = Number(ieltsrequiredscore)
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);
                                }

                                console.log("IeltsScore",ieltsrequiredscore)
                               let myscore = await utils.getMappingScore(ieltsDict.description);
                                console.log("myscore",myscore)
                                if (myscore.ibt) {
                                    tofelDict.name = 'toefl ibt';
                                    tofelDict.description = myscore.ibt;
                                    tofelDict.min = 0;
                                    tofelDict.require = await utils.giveMeNumber(myscore.ibt);
                                    tofelDict.max = 120;
                                    tofelDict.R = 0;
                                    tofelDict.W = 0;
                                    tofelDict.S = 0;
                                    tofelDict.L = 0;
                                    tofelDict.O = 0;
                                    englishList.push(tofelDict);
                                }
                                if (myscore.pte) {
                                    pteDict.name = 'pte academic';
                                    pteDict.description = myscore.pte;
                                    pteDict.min = 0;
                                    pteDict.require = await utils.giveMeNumber(myscore.pte);
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);
                                }
                                if (myscore.cae) {
                                    caeDict.name = 'cae';
                                    caeDict.description = myscore.cae;
                                    caeDict.min = 80;
                                    caeDict.require = await utils.giveMeNumber(myscore.cae);;
                                    caeDict.max = 230;
                                    caeDict.R = 0;
                                    caeDict.W = 0;
                                    caeDict.S = 0;
                                    caeDict.L = 0;
                                    caeDict.O = 0;
                                    englishList.push(caeDict);
                                }
                                // ieltsDict.name = 'ielts academic';
                                // ieltsDict.description = ieltsScore;
                                // englishList.push(ieltsDict);
                                if (englishList && englishList.length > 0) {
                                    courseAdminReq.english = englishList;
                                }
                                // }

                                else {
                                    var mapping = await utils.getValueFromHardCodedJsonFile("ielts_mapping");
                                    console.log("study level new:", mapping);
                                    ieltsDict.name = 'ielts academic';
                                    console.log("level iss-->" + resJsonData.course_study_level)
                                    if (resJsonData.course_study_level == "UNDERGRADUATE") {
                                        ieltsDict.description = mapping.UNDERGRADUATE;
                                        ieltsDict.min = 0;
                                        ieltsDict.require = await utils.giveMeNumber(ieltsDict.description.replace(/ /g, ' '));;
                                        ieltsDict.max = 9;
                                        ieltsDict.R = 0;
                                        ieltsDict.W = 0;
                                        ieltsDict.S = 0;
                                        ieltsDict.L = 0;
                                        ieltsDict.O = 0;
                                        englishList.push(ieltsDict);
                                    }
                                    else if (resJsonData.course_study_level == "POSTGRADUATE") {
                                        ieltsDict.description = mapping.POSTGRADUATE;
                                        ieltsDict.min = 0;
                                        ieltsDict.require = await utils.giveMeNumber(ieltsDict.description.replace(/ /g, ' '));;
                                        ieltsDict.max = 9;
                                        ieltsDict.R = 0;
                                        ieltsDict.W = 0;
                                        ieltsDict.S = 0;
                                        ieltsDict.L = 0;
                                        ieltsDict.O = 0;
                                        englishList.push(ieltsDict);
                                    }
                                    else if (resJsonData.course_study_level == "RESEARCH") {
                                        ieltsDict.description = mapping.RESEARCH;
                                        ieltsDict.min = 0;
                                        ieltsDict.require = await utils.giveMeNumber(ieltsDict.description.replace(/ /g, ' '));;
                                        ieltsDict.max = 9;
                                        ieltsDict.R = 0;
                                        ieltsDict.W = 0;
                                        ieltsDict.S = 0;
                                        ieltsDict.L = 0;
                                        ieltsDict.O = 0;
                                        englishList.push(ieltsDict);
                                    }

                                    // englishList.push(ieltsDict.description);
                                    if (englishList && englishList.length > 0) {
                                        courseAdminReq.english = englishList;
                                    } else {
                                        throw new Error("IELTS NOT FOUND");
                                    }


                                }
                                console.log("level iss-->" + ieltsDict.description)
                                if (ieltsDict.description != "Not Mentioned!") {
                                    const ieltsScorenum = await utils.giveMeLstNumber(ieltsDict.description);
                                    if (ieltsScorenum != "NA") {
                                        var pieltsDict = {};
                                        pieltsDict.name = 'ielts academic';
                                        var value = {};
                                        value.min = 0;
                                        value.require = ieltsScorenum;
                                        value.max = 9;
                                        pieltsDict.value = value;
                                        penglishList.push(pieltsDict);
                                    }
                                }

                            }
                            // if (penglishList && penglishList.length > 0) {
                            //     courseAdminReq.englishprogress = penglishList;
                            // }
                            ///progrssbar End
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }

                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.course_admission_english_more_details;
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            const courseKeyValac = courseScrappedData.course_academic_requirement;
                            let course_academic_requirement = null;
                            if (Array.isArray(courseKeyValac)) {
                                for (const rootEleDict of courseKeyValac) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            course_academic_requirement = selList[0];
                                            ///}
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            courseAdminReq.academic = await utils.giveMeArray((course_academic_requirement) ? course_academic_requirement + " \n\n (you can find country wise information on the course site.please refer this course url.)"+" "+courseUrl : "".replace(/[\r\n\t]+/g, ';'), ";");
                            console.log("resJsonData.course_admission_requirement-->", courseAdminReq.academic);
                            if(courseAdminReq.academic){
                                if(courseAdminReq.academic=[]){
                                    courseAdminReq.academic=["you can find country wise information on the course site.please refer this course url"+" "+courseUrl]
                                    console.log("yesss1")
                                }
                                console.log("yesss")

                            }
                            var mydata = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                            var myurl = mydata["English_more"];
                            // var myurl1 = mydata["Entry_more"];
                            var myurl1 = mydata["academic_requirements_url"];
                            courseAdminReq.english_requirements_url = myurl;
                            courseAdminReq.academic_requirements_url = myurl1;
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                            }
                            if (course_academic_requirement && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.academic_requirements_url = myurl1;
                            } else if (course_academic_requirement) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.academic_requirements_url = myurl1;
                            }
                            //console.log("resJsonData.course_admission_requirement-->", courseAdminReq.academic);
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_outline': {
              
                            let mgcrs = null;
                          
                         const majorcrs = {
                           majors:[],
                           minors:[],
                           more_details:""
                         };
                         //for major cousrseList
                           var coursemajor =  courseScrappedData.course_outline;
                           if (Array.isArray(coursemajor)) {
                             for (const rootEleDict of coursemajor) {
                                 console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                 const elementsList = rootEleDict.elements;
                                 for (const eleDict of elementsList) {
                                     console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                     const selectorsList = eleDict.selectors;
                                     for (const selList of selectorsList) {
                                         console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                         if (Array.isArray(selList) && selList.length > 0) {
                                           mgcrs = selList;
                                         }
                                     }
                                 }
                             }
                         }
                           if(mgcrs && mgcrs.length >0){
                             majorcrs.majors=mgcrs
                           }
                           else{
                             majorcrs.majors= majorcrs.majors
                           }
                              resJsonData.course_outline =  majorcrs;
                           //for minor courseList
                              var courseminor =  courseScrappedData.course_outline_minor;
                              let mincrs = null;
                              if (Array.isArray(courseminor)) {
                                for (const rootEleDict of courseminor) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                             mincrs = selList;
                                            }
                                        }
                                    }
                                }
                            }
                              if(mincrs && mincrs.length >0){
                                majorcrs.minors=mincrs
                              }
                              else{
                               majorcrs.minors=majorcrs.minors
                             }
                                 resJsonData.course_outline =  majorcrs;
             
                              console.log("major==>", resJsonData.course_outline)
                        
                          break;
                          }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            let splitt = ""
                            let filtered_duration_formated = {}
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList
                                                console.log("Durationsss----->", selList)
                                                
                                            }
                                           
                                        }
                                    }
                                    if (fullTimeText && fullTimeText.length > 0) {
                                        resJsonData.course_duration = String(fullTimeText);;
                                        const resFulltime = fullTimeText;
                                        if (resFulltime) {
                                            durationFullTime.duration_full_time = resFulltime;
                                            console.log("resFulltime -->", durationFullTime);
                                            courseDurationList.push(durationFullTime);


                                            if (String(durationFullTime.duration_full_time).toLowerCase().indexOf('years') > 1 && String(durationFullTime.duration_full_time).toLowerCase().indexOf('months') > 1
                                            || String(durationFullTime.duration_full_time).toLowerCase().indexOf('year')>-1 && String(durationFullTime.duration_full_time).toLowerCase().indexOf('months')>-1 ) {
                                                let fullString = durationFullTime.duration_full_time[0];
                                                console.log("fullString -->", fullString);
                                                const regEx = /\d/g;
                                                var fullTimeText = fullString.match(regEx);
                                                fullTimeText = Number(fullTimeText[0] * 12) + Number(fullTimeText[1]);
                                                fullTimeText = fullTimeText / 12;
                                                // fullTimeText = Number(fullTimeText + fullTimeText[1]);
                                                // let filtered_duration_formated = {};
                                                filtered_duration_formated.unit = "Year"
                                                filtered_duration_formated.display = "Full-Time";
                                                console.log("fullTimeText -->", fullTimeText);
                                                filtered_duration_formated.duration = fullTimeText + " ";
                                                filtered_duration_formated.filterduration = fullTimeText;
                                                console.log("filtered_duration_formated-->", filtered_duration_formated);
                                                courseDurationDisplayList.push(filtered_duration_formated);
                                                filtered_duration_formated = [filtered_duration_formated];
                                            } else {
                                                console.log("ELSE PART -->", resFulltime);

                                                // let splitt = String(resFulltime).replace("full-time",'').trim();
                                                // console.log("splitt------->", splitt)
                                                // let Fulltime = []
                                                // Fulltime.push(splitt)
                                                // console.log("resFulltimesplitt------->", Fulltime)

                                                courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime[0]));
                                                console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                                filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                                console.log("filtered_duration_formated", filtered_duration_formated);
                                            }
                                            if (courseDurationList && courseDurationList.length > 0) {
                                                console.log("courseDurationList------>", courseDurationList)
                                                let tmpvar = courseDurationList[0].duration_full_time[0];
                                                courseDurationList[0].duration_full_time = tmpvar;
                                                console.log("courseDurationList ympvar------>", tmpvar)
                                               
                                            }
                                            //let filtered_duration_formated = {};
                                            if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                                console.log("filtered_duration_formated----->", filtered_duration_formated)
                                                resJsonData.course_duration_display = filtered_duration_formated;
                                                var isfulltime = false, isparttime = false;
                                                filtered_duration_formated.forEach(element => {
                                                    if (element.display == "Full-Time") {
                                                        isfulltime = true;
                                                    }
                                                    if (element.display == "Part-Time") {
                                                        isparttime = true;
                                                    }
                                                });
                                                resJsonData.isfulltime = isfulltime;
                                                resJsonData.isparttime = isparttime;
                                            }
                                        }
                                    }
                                    else{
                                        resJsonData.course_duration = String(fullTimeText);
                                    }
                                }
                            }
                            break;
                        }

                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':

                        case 'course_campus_location': { // Location Launceston
                            
                            const courseKeyValcd = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyValcd)) {
                                for (const rootEleDict of courseKeyValcd) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            
                            
                            
                            
                            var campLocationText = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;

                            var location = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location)
                            console.log("location----->", location)


                            var campLocation = location;
                            campLocationText = [];
                           
                            if (campLocation) {
                                if (campLocation.toString().toLowerCase().trim().includes("curtin perth") ||campLocation.toString().toLowerCase().trim().includes("perth city") ) {
                                    campLocationText.push("Perth")
                                    console.log("campLocationText----->", campLocationText)
                                }
                                if (campLocation.toString().toLowerCase().trim().includes("curtin kalgoorlie")) {
                                    campLocationText.push("Kalgoorlie")
                                }                            
                                                            
                                if (campLocation.toString().toLowerCase().trim().includes("bentley campus")) {
                                    campLocationText.push("Bentley")
                                }
                                if (campLocation.toString().toLowerCase().trim().includes("geraldton")) {
                                    campLocationText.push("Geraldton")
                                }
                                if (campLocation.toString().toLowerCase().trim().includes("muresk institute")) {
                                    campLocationText.push("Muresk Institute")
                                }
                            }                     



                            console.log("##Campus-->" + campLocationText)
                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();

                                var campusedata = [];
                                campLocationText.forEach(element => {

                                    campusedata.push({
                                        "name": format_functions.titleCase(element),
                                        "code": String(course_cricos_code)
                                    })
                                });
                                //     console.log("campusedata--------->", ccampusedatampLocationText)

                                resJsonData.course_campus_location = campusedata;
                                // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                var study_mode = [];
                                campLocationValTrimmed = campLocationValTrimmed.toLowerCase();
                                
                                campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                                if (campLocationValTrimmed.trim().length > 0) {
                                    study_mode = "On campus";
                                }
                                resJsonData.course_study_mode = study_mode;//.join(',');
                                console.log("## FInal string-->" + JSON.stringify(campLocationValTrimmed));
                                // }
                            } // if (campLocationText && campLocationText.length > 0)
                            else {
                                throw new Error("Campus Location Not Found !!");
                            }
                            break;
                        }

                        case 'course_tuition_fees_international_student': {
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};
                            var feesIntStudent_first_year = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_first_year);;
                            console.log("feesIntStudent_first_year--->>>", feesIntStudent_first_year);
                          
                            if (feesIntStudent_first_year && feesIntStudent_first_year.length > 0) { // extract only digits
                                console.log("international_student    123 check " + feesIntStudent_first_year)
                                // feesDict.international_student = feesIntStudent;
                                const feesWithDollorTrimmed = String(feesIntStudent_first_year).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const myval = feesVal.split(".");
                                    const regEx = /\d/g;
                                    let feesValNum = myval[0].match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            console.log("feess", feesNumber);
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''));

                                        }
                                        //       else {
                                        console.log("fes-.-.-..-", feesNumber);
                                        console.log("fees not")
                                        feesNumber = feesValNum;
                                        var feesint = {};
                                        feesint.amount = 0;
                                        feesint.duration = 1;
                                        feesint.unit = "year";
                                        feesint.description = (feesIntStudent_first_year) ? feesIntStudent_first_year : "";
                                        // feesint.type = "";
                                       feesDict.international_student = feesint;
                                        // }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            var feesint = {};
                                            feesint.amount = Number(feesNumber);
                                            feesint.duration = 1;
                                            feesint.unit = "year";
                                            feesint.description = (feesIntStudent_first_year) ? feesIntStudent_first_year : "";
                                            // feesint.type = "";
                                           feesDict.international_student = feesint;
                                        }
                                    }
                                }
                            }
                            else {
                                console.log("main else",resJsonData.course_campus_location[0].code)
                                let units,fduration
                                let dur=await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time)
                                let result = await format_functions.getMyFees(resJsonData.course_campus_location[0].code)
                                console.log("Result_fees",result)
                                console.log("Durtion_fees",dur)
                                if(String(dur).includes("months")){
                                    let num=await utils.giveMeNumber(dur);
                                    fduration=num
                                    console.log("@@@num",fduration)
                                    units="Months"
                                }else{
                                    units="Years"
                                    let num=await utils.giveMeNumber(dur);
                                    fduration=num
                                }
                               console.log("Units===>",units)
    
                                var feesint = {};
                                feesint.amount = parseInt(result.replace(/,/g, ''), 10);
                                feesint.duration = fduration;
                                feesint.unit = units;
                                feesint.description =result;
                                // feesint.type = "";
                             feesDict.international_student = feesint;
                            }






                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            var courseKeyVal = courseScrappedData.course_tuition_fees_international_student;
                            let feesIntStudent = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                selList.forEach(element => {
                                                    if (element)
                                                        feesIntStudent.push(element.replace(/[*\r\n\t ]+/g, ' ').trim())
                                                });




                                            }
                                        }
                                    }
                                }
                                //   courseTuitionFee.year = feeYear;

                                if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                    console.log("lenght issue" + feesIntStudent);
                                    const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                    const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);

                                    if (feesDuration && feesDuration.length > 0) {
                                        feesDict.fee_duration_years = feesDuration;
                                    }

                                    if (feesCurrency && feesCurrency.length > 0) {
                                        feesDict.currency = feesCurrency;
                                    }
                                    
                                    if (feesDict) {
                                        var campus = resJsonData.course_campus_location;
                                        for (let loc of campus) {
                                            feesList.push({ name: loc.name, value: feesDict});
                                        }
                                    }
                                    if (feesList && feesList.length > 0) {
                                        courseTuitionFee.fees = feesList;
                                    }
                                    else {
                                        courseTuitionFee.fees = [];
                                    }
                                    console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                    if (courseTuitionFee) {
                                        resJsonData.course_tuition_fee = courseTuitionFee;
                                    }
                                }
                                break;
                            }
                        }



                     case 'application_fee': {
                      const courseKeyVal = courseScrappedData.application_fee;
             
                      let applicationfee = null;
                        if (Array.isArray(courseKeyVal)) {
                      for (const rootEleDict of courseKeyVal) {
                     console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                     const elementsList = rootEleDict.elements;
                     for (const eleDict of elementsList) {
                     console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        const selectorsList = eleDict.selectors;
                     for (const selList of selectorsList) {
                         console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        applicationfee = selList[0];
                         }
                         else{
                            applicationfee = selList[0];
                         }
                      }
                     }
                     }
                 }
                if (applicationfee && applicationfee.length>0) {
                  resJsonData.application_fee = applicationfee;
                 console.log("appfee"+ JSON.stringify(resJsonData.application_fee));
                 }
                 else{
                    resJsonData.application_fee = applicationfee;
                 }
             
                  break;
             
            
                }

                        case 'program_code': {
                            
                            var program_code = "";
                            program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                           
                            if (program_code.length > 0) {

                                resJsonData.program_code = program_code;
                            } else {
                                resJsonData.program_code = "";
                            }
                            break;
                        }
                        case 'course_intake': {
                            const courseIntakeDisplay = {};
                            // existing intake value
                            var intakes = [];
                            var courseIntakeStr = [];//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (selList) {
                                                for (let sel of selList) {
                                                    sel = sel.replace(/[*\r\n\t ]+/g, ' ').trim();

                                                    courseIntakeStr.push(sel);

                                                }

                                            }
                                        }


                                    }
                                }
                            }
                            console.log("courseIntakeStr-->", courseIntakeStr);
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var campus = resJsonData.course_campus_location;
                                console.log("CAMPUS -->>>", campus);
                                var intakes = [];
                                console.log("Intake length-->" + courseIntakeStr.length);
                                console.log("Campus length-->" + campus.length);
                                var myintake_data = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                                for (var count = 0; count < campus.length; count++) {
                                  

                                    console.log("intake756", intakes);
                                   
                                    var myintakematch = courseIntakeStr[0];
                                    if (myintakematch) {
                                        var intakedetail = {};
                                        var myintake = [];
                                        if (myintakematch.toLowerCase().trim().includes("semester 1")) {
                                            myintake.push(myintake_data["Semester1"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("semester 2")) {
                                            myintake.push(myintake_data["Semester2"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("trimester 1")) {
                                            myintake.push(myintake_data["Trimester1"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("trimester 2")) {
                                            myintake.push(myintake_data["Trimester2"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("trimester 3")) {
                                            myintake.push(myintake_data["Trimester3"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("trimester 1a")) {
                                            myintake.push(myintake_data["Trimester1A"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("trimester 2a")) {
                                            myintake.push(myintake_data["Trimester2A"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("trimester 3a")) {
                                            myintake.push(myintake_data["Trimester3A"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("trimester 1B")) {
                                            myintake.push(myintake_data["Trimester1B"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("trimester 2B")) {
                                            myintake.push(myintake_data["Trimester2B"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("trimester 3B")) {
                                            myintake.push(myintake_data["Trimester3B"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("research term 1")) {
                                            myintake.push(myintake_data["Research Term 1"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("research term 2")) {
                                            myintake.push(myintake_data["Research Term 2"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("block session 1")) {
                                            myintake.push(myintake_data["Block Session 1"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("block session 2")) {
                                            myintake.push(myintake_data["Block Session 2"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("block session 3")) {
                                            myintake.push(myintake_data["Block Session 3"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("block session 4")) {
                                            myintake.push(myintake_data["Block Session 4"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("block session 5")) {
                                            myintake.push(myintake_data["Block Session 5"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("block session 6")) {
                                            myintake.push(myintake_data["Block Session 6"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("block session 7")) {
                                            myintake.push(myintake_data["Block Session 7"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("block session 8")) {
                                            myintake.push(myintake_data["Block Session 8"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("study period 1")) {
                                            myintake.push(myintake_data["Study Period 1"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("study term 1")) {
                                            myintake.push(myintake_data["Study Term 1"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("study term 4")) {
                                            myintake.push(myintake_data["Study Term 4"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("study term 5")) {
                                            myintake.push(myintake_data["Study Term 5"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("uni term 1")) {
                                            myintake.push(myintake_data["Uni Term 1"])
                                        }
                                        if (myintakematch.toLowerCase().trim().includes("uni term 3")) {
                                            myintake.push(myintake_data["Uni Term 3"])
                                        }
                                        for (let loc of campus)
                                            intakedetail.name = campus[count].name;
                                        intakedetail.value = myintake;
                                        intakes.push(intakedetail);
                                        console.log("inataker", intakes);
                                    }
                                    else {
                                        intakedetail.name = campus[count];
                                        var myintake = [];
                                        intakedetail.value = myintake;
                                        intakes.push(intakedetail);
                                    }
                                }
                                let intake_date = [
                                    {
                                        key: "25 February 2019",
                                        value: "2019-02-25"
                                    },
                                    {
                                        key: "29 July 2019",
                                        value: "2019-07-29"
                                    },
                                    {
                                        key: "14 January 2019",
                                        value: "2019-01-14"
                                    },
                                    {
                                        key: "6 May 2019",
                                        value: "2019-05-06"
                                    },
                                    {
                                        key: "26 August 2019",
                                        value: "2019-08-26"
                                    },
                                    {
                                        key: "25 February 2019",
                                        value: "2019-02-25"
                                    },
                                    {
                                        key: "8 July 2019",
                                        value: "2019-07-08"
                                    },
                                    {
                                        key: "4 November 2019",
                                        value: "2019-11-04"
                                    },
                                    {
                                        key: "14 January 2019",
                                        value: "2019-01-14"
                                    },
                                    {
                                        key: "29 April 2019",
                                        value: "2019-04-29"
                                    },
                                    {
                                        key: "19 August 2019",
                                        value: "2019-08-19"
                                    },
                                    {
                                        key: "1 January 2019",
                                        value: "2019-01-01"
                                    },
                                    {
                                        key: "1 July 2019",
                                        value: "2019-07-01"
                                    },
                                    {
                                        key: "30 December 2019",
                                        value: "2019-12-30"
                                    },
                                    {
                                        key: "5 August 2019",
                                        value: "2019-08-05"
                                    },
                                    {
                                        key: "23 September 2019",
                                        value: "2019-08-05"
                                    },
                                    {
                                        key: "7 January 2019",
                                        value: "2019-01-07"
                                    },
                                    {
                                        key: "10 June 2019",
                                        value: "2019-06-10"
                                    },
                                ]


                                var intakedata = {};
                                intakedata.intake = await utils.providemyintake(intakes, "MOM", "");
                                console.log("intakedetails00", intakedata.intake);
                                intakedata.more_details = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake_url);
                                resJsonData.course_intake = intakedata;
                                console.log("Intakes --> ", JSON.stringify(intakedata.intake));
                                
                                let valmonths11 = "";
                                for (let intakeval of intakedata.intake) {
                                    console.log("intakeval--->", intakeval);
                                    for (let date of intakeval.value) {
                                        console.log("month--->", date);
                                        for (valmonths11 of intake_date) {
                                            console.log("valmonths11--->", valmonths11);
                                            if (valmonths11.key == date.actualdate) {
                                                console.log("month.key--->", valmonths11.key);
                                                date.filterdate = valmonths11.value;

                                                console.log("intake_months--->", date.filterdate);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                var campus = resJsonData.course_campus_location;
                                var intakes = [];
                                for (var count = 0; count < campus.length; count++) {
                                    var intakedetail = {};
                                    intakedetail.name = campus[count];
                                    intakedetail.value = [];
                                    intakes.push(intakedetail);

                                }
                                var intakedata = {};
                                intakedata.intake = await utils.providemyintake(intakes, "MOM", "");

                               
                                intakedata.more_details = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake_url);
                                resJsonData.course_intake = intakedata;
                            }
                           
                        }

                        

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            console.log(funcName + 'matched case : ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr course_career_outcome= ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            }
                            break;
                        }
                        // case 'course_overview': {
                        //     const courseKeyVal = courseScrappedData.course_overview;
                        //     let course_overview = null;
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                     //if (Array.isArray(selList) && selList.length > 0) {
                        //                     course_overview = selList[0];
                        //                     //}
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     //if (course_career_outcome.length > 0) {
                        //     resJsonData.course_overview = course_overview;
                        //     //}
                        //     break;
                        // }
                        case 'course_career_outcome': {
                            let assigned = false;
                            console.log('course_career_outcome matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log('course_career_outcome key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedSelStr = [];
                            for (const rootEleDict of rootElementDictList) {
                                console.log('course_career_outcome rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                // let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log('course_career_outcome eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        if (assigned == false) {
                                            if (selList && selList.length > 0) {
                                                console.log('course_career_outcome selList = ' + JSON.stringify(selList));

                                                for (const selItem of selList) {
                                                    concatnatedSelStr.push(selItem);
                                                }
                                                assigned = true;
                                            }
                                            console.log('course_career_outcome concatnatedSelStr = ' + concatnatedSelStr);
                                            // if (concatnatedSelStr) {
                                            //   concatnatedSelectorsStr = concatnatedSelStr;
                                            // }
                                        }
                                    } // selectorsList                  
                                } // elementsList                
                            } // rootElementDictList 

                            // add only if it has valid value              
                            if (concatnatedSelStr && concatnatedSelStr.length > 0) {
                                resJsonData.course_career_outcome = concatnatedSelStr;
                            } else {
                                resJsonData.course_career_outcome = [];
                            }

                            break;
                        }
                        // case 'course_career_outcome': {
                        //     const courseKeyVal = courseScrappedData.course_career_outcome;
                        //     //  let course_career_outcome = null;
                        //     // if (Array.isArray(courseKeyVal)) {
                        //     //     for (const rootEleDict of courseKeyVal) {
                        //     //         console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //     //         const elementsList = rootEleDict.elements;
                        //     //         for (const eleDict of elementsList) {
                        //     //             console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //     //             const selectorsList = eleDict.selectors;
                        //     //             for (const selList of selectorsList) {
                        //     //                 console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //     //                 //if (Array.isArray(selList) && selList.length > 0) {
                        //     //                 course_career_outcome = selList;
                        //     //                 console.log("course_career_outcome------->", course_career_outcome)
                        //     //                 // }
                        //     //             }
                        //     //         }
                        //     //     }
                        //     // }
                        //    // let course_career_outcome = await Course.extractValueFromScrappedElement(courseScrappedData.course_career_outcome);

                        //     if (course_career_outcome.length > 0) {
                        //         resJsonData.course_career_outcome = [course_career_outcome];
                        //     }
                        //     break;
                        // }



                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                // var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let intakedetail of intakes) {
                    if (location == intakedetail.name) {
                        matchrec = intakedetail.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                // for (let myfees of resJsonData.course_tuition_fee.fees) {
                //     if (myfees.name == location) {
                //         NEWJSONSTRUCT.international_student_all_fees = myfees;

                //     }
                // }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
              
                // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                //     if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                //         resJsonData.course_cricos_code[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_cricos_code[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                //     if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_intake.intake[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_intake.intake[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                //     if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                //     if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_campus_location[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_campus_location[i].iscurrent = false;
                //     }
                // }
                //pcurtinuniversity_masterofphilosophy(ruralmanagement)_curtinperth_details.json
                // resJsonData.course_cricos_code = location_wise_data.cricos_code;
                // NEWJSONSTRUCT.course_campus_location = location.name;
                // resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
                // resJsonData.current_course_campus_location = location_wise_data.course_campus_location;
                //resJsonData.current_course_intake = location_wise_data.course_intake;
                // resJsonData.course_intake_display = location_wise_data.course_intake;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.studyLevel, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
