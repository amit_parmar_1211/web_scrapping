const puppeteer = require('puppeteer');

const WAIT_FOR_SEL_OPTIONS = {
    timeout: 5000, // ms
};
async function scrape_by_xpath(page, selector) {
    var retDict = [];
    try {
        //await page.waitForSelector(selector, WAIT_FOR_SEL_OPTIONS);
        const targetLinks = await page.$x(selector);
        for (let link of targetLinks) {
            elementstring = await page.evaluate(el => el.innerText, link);
            retDict.push(elementstring)
        }
    }
    catch (e) {
        console.log("Error--->" + e);
    }
    return await retDict;
}
async function validate_course_duration_full_time(yeartext) {
    var retValueDict = [];
    try {
        var yeartext = yeartext[0].elements[0].selectors[0][0];
        const YELEM = ["year", "years", "month", "months", "day", "days", "week", "weeks", "sem", "semester", "semesters"];
        const REQCONVERT = ["sem", "semester", "semesters"];
        const YVELEM = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "tan"];
        var yeartext_rep = yeartext.toString().toLowerCase().replace(/\s/g, ' ');;
        var yeardata = yeartext_rep.split(' ');
        console.log("### text to formate for full time year--->" + yeartext);
        console.log("### data after split--->" + yeardata);
        var YEAR = 0;
        YELEM.forEach(element => {
            element = element.replace(/[^a-zA-Z\d\-. ]/g, "");
            if (yeardata.includes(element) && YEAR == 0) {
                console.log("### match-->" + element);
                var PELEM = "", LELEM = "";
                const index_of_elem = yeardata.indexOf(element);
                if (index_of_elem > 0)
                    PELEM = yeardata[index_of_elem - 1];
                if (index_of_elem < yeardata.length - 1)
                    LELEM = yeardata[index_of_elem + 1];
                if (PELEM) {
                    var PELEM_DATA = PELEM.split('-'), YEAR_DATA = [];
                    PELEM_DATA.forEach(element => {
                        if (element.match(/^-?\d*(\.\d+)?$/)) {
                            YEAR_DATA.push(element);
                        }
                        else if (YVELEM.includes(element)) {
                            YEAR_DATA.push(YVELEM.indexOf(element) + 1);
                        }
                    });
                    if (PELEM_DATA.length > 1) {
                        YEAR = YEAR_DATA.join("-")
                    }
                    else {
                        YEAR = YEAR_DATA[0];
                    }
                    console.log("### PELEM-->" + PELEM);
                    console.log("### YEAR-->" + YEAR);
                }
                if (LELEM && YEAR == 0) {
                    var LELEM_DATA = LELEM.split('-'), YEAR_DATA = [];
                    LELEM_DATA.forEach(element => {
                        if (element.match(/^-?\d*(\.\d+)?$/)) {
                            YEAR_DATA.push(element);
                        }
                        else if (YVELEM.includes(element)) {
                            YEAR_DATA.push(YVELEM.indexOf(element) + 1);
                        }
                    });
                    if (LELEM_DATA.length > 1) {
                        YEAR = YEAR_DATA.join("-")
                    }
                    else {
                        YEAR = YEAR_DATA[0];
                    }
                    console.log("### LELEM-->" + LELEM);
                    console.log("### YEAR-->" + YEAR);
                }
                if (element && YEAR) {
                    console.log("### Element-->" + element);
                    console.log("### YEAR-->" + YEAR);
                    if (REQCONVERT.includes(element)) {
                        var ELEM_DATA = YEAR.toString().split('-'), YEAR_DATA = [];
                        ELEM_DATA.forEach(element => {
                            YEAR_DATA.push(element / 2)
                        });
                        if (ELEM_DATA.length > 1) {
                            YEAR = YEAR_DATA.join("-")
                        }
                        else {
                            YEAR = YEAR_DATA[0];
                        }
                        element = "year";
                    }
                    retValueDict = { unit: element, duration_full_time: YEAR }
                }
            }
        });
        console.log("#### formated year--->" + JSON.stringify(retValueDict));
    }
    catch (err) {
        console.log("#### try-catch error--->" + err);
    }
    return await retValueDict;
}

async function not_formated_course_duration(yeartext) {
    var retValueDict = [];
    try {
        yeartext = yeartext[0].elements[0].selectors[0][0];
        var yeardata = yeartext.toLowerCase();
        console.log("### text to formate for full time year--->" + yeartext);
        console.log("### data after split--->" + yeardata);
        retValueDict = yeardata;
        console.log("#### formated year--->" + JSON.stringify(retValueDict));
    }
    catch (err) {
        console.log("#### try-catch error--->" + err);
    }
    return await retValueDict;
}
async function course_list_scrape(page, selector) {
    var courselist = [];
    try {
        for (let sel of selector) {
            switch (sel.action) {
                case "WAIT": {
                    if (sel.istime) {
                        await page.waitFor(sel.time);
                    }
                    else {
                        await page.waitForNavigation();
                    }
                    break;
                }
                case "CLICK": {
                    if (sel.isxpath) {
                        await page.$x(sel.selector, el => el.click());
                    }
                    else {
                        await page.$eval(sel.selector, el => el.click());
                    }
                    break;
                }
                case "SCRAPE": {
                    if (!sel.isxpath) {
                        for (let link of sel.selector) {
                            const scrape_element_url = await page.$$(link.urlselector);
                            const scrape_element_text = await page.$$(link.textselector);
                            for (let count = 0; count < scrape_element_url.length; count++) {
                                var elementstring = await page.evaluate(el => el.innerText, scrape_element_text[count]);
                                var elementurl = await page.evaluate(el => el.href, scrape_element_url[count]);
                                if (sel.iscomb) {
                                    const combtext = await page.evaluate(el => el.innerText, scrape_element_url[count]);
                                    elementstring = combtext + " " + elementstring;
                                }
                                courselist.push({ href: elementurl, innerText: elementstring });
                            }
                        }
                    }
                    else {
                        for (let link of sel.selector) {
                            const scrape_element_url = await page.$x(link.urlselector);
                            const scrape_element_text = await page.$x(link.textselector);
                            for (let count = 0; count < scrape_element_url.length; count++) {
                                var elementstring = await page.evaluate(el => el.innerText, scrape_element_text[count]);
                                var elementurl = await page.evaluate(el => el.href, scrape_element_url[count]);
                                if (sel.iscomb) {
                                    const combtext = await page.evaluate(el => el.innerText, scrape_element_url[count]);
                                    elementstring = combtext + " " + elementstring;
                                }
                                courselist.push({ href: elementurl, innerText: elementstring });
                            }
                        }
                    }
                    break;
                }
            }
        }
    }
    catch (e) {
        console.log("Error--->" + e);
    }
    console.log(JSON.stringify(courselist));
    return await courselist;
}
async function course_outline_scrape(page, selector) {
    var courselist = [];
    try {
        // for (let sel of selector) {
        let sel = selector;
        switch (sel.action) {
            case "WAIT": {
                if (sel.istime) {
                    await page.waitFor(sel.time);
                }
                else {
                    await page.waitForNavigation();
                }
                break;
            }
            case "CLICK": {
                if (sel.isxpath) {
                    await page.$x(sel.selector, el => el.click());
                }
                else {
                    await page.$eval(sel.selector, el => el.click());
                }
                break;
            }
            case "SCRAPE": {
                if (!sel.isxpath) {
                    for (let link of sel.selector) {
                        const scrape_element_url = await page.$$(link.urlselector);
                        const scrape_element_text = await page.$$(link.textselector);
                        for (let count = 0; count < scrape_element_url.length; count++) {
                            var elementstring = await page.evaluate(el => el.innerText, scrape_element_text[count]);
                            var elementurl = await page.evaluate(el => el.href, scrape_element_url[count]);
                            if (sel.iscomb) {
                                const combtext = await page.evaluate(el => el.innerText, scrape_element_url[count]);
                                elementstring = combtext + " " + elementstring;
                            }
                            courselist.push({ href: elementurl, innerText: elementstring });
                        }
                    }
                }
                else {
                    for (let link of sel.selector) {
                        const scrape_element_url = await page.$x(link.urlselector);
                        const scrape_element_text = await page.$x(link.textselector);
                        for (let count = 0; count < scrape_element_url.length; count++) {
                            var elementstring = await page.evaluate(el => el.innerText, scrape_element_text[count]);
                            var elementurl = await page.evaluate(el => el.href, scrape_element_url[count]);
                            if (sel.iscomb) {
                                const combtext = await page.evaluate(el => el.innerText, scrape_element_url[count]);
                                elementstring = combtext + " " + elementstring;
                            }
                            courselist.push({ href: elementurl, innerText: elementstring });
                        }
                    }
                }
                break;
            }
        }
        //}
    }
    catch (e) {
        console.log("Error--->" + e);
    }
    console.log(JSON.stringify(courselist));
    return await courselist;
}
module.exports = {
    validate_course_duration_full_time,
    not_formated_course_duration,
    scrape_by_xpath,
    course_outline_scrape,
    course_list_scrape
};