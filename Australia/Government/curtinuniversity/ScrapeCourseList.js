const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const configs = require('./configs');
class ScrapeCourseList extends Scrape {
  async scrapeCourseListAndPutAtS3() {
    const funcName = 'startScrappingFunc ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var datalist = [];
      await s.setupNewBrowserPage("https://study.curtin.edu.au/search/?search_text=&resident_type=international&study_type=course&study_mode=on-campus");
      var mainCategory = []
      const maincategoryselector = "//*[@id='study_area']/div/label[not(@for='study_area-all')]";
      const activeselector = "//*/span[contains(text(),'Next')]/preceding-sibling::span/ancestor::a[@class='search-pagination__next']";
      const filterclick = "//*//button[@data-filter-reset]";
      var filtercategory = await s.page.$x(filterclick);
      await filtercategory[0].click();
      await s.page.waitFor(5000);
      var category = await s.page.$x(maincategoryselector);
      console.log("Total Category-->" + category.length)
      for (let i = 0; i < category.length; i++) {
        //await category[i].click();
        var categorystring = await s.page.evaluate(el => el.innerText, category[i]);
        var replacedata = categorystring.replace(/[' ',]+/g, '-').toLowerCase();
        console.log("replacedata--->", replacedata.split("\n")[0])
       // replacedata.includes('Physical science and mathematics') || replacedata.includes('Information technology') |||| replacedata.includes('Engineering, mining and related technologies')
        if( replacedata.includes('culture-society-and-indigenous')){
          console.log("yesss")
         let  replacedata="culture-language-and-indigenous"
          var categoryurl= "https://study.curtin.edu.au/search/?search_text=&study_type=course&study_area="+replacedata+"&study_mode=on-campus"
        }else if(replacedata.includes('physical-science-and-mathematics')){
          let replacedata="physical-sciences-and-mathematics"
          var categoryurl="https://study.curtin.edu.au/search/?search_text=&study_type=course&study_area="+replacedata+"&study_mode=on-campus"
        }else if(replacedata.includes('information-technology')){
          let replacedata="it-and-computing"
          var categoryurl="https://study.curtin.edu.au/search/?search_text=&study_type=course&study_area="+replacedata+"&study_mode=on-campus"
        }else if(replacedata.includes('engineering-mining-and-related-technologies')){
          let replacedata="engineering-and-mining"
          var categoryurl="https://study.curtin.edu.au/search/?search_text=&study_type=course&study_area="+replacedata+"&study_mode=on-campus"
        }
        else{
          var categoryurl = "https://study.curtin.edu.au/search/?search_text=&resident_type=international&study_type=course&study_mode=on-campus&study_area=" + replacedata.split("\n")[0]
        }
      
       
        mainCategory.push({ href: categoryurl, innerText: categorystring })
      }
    

      for (let catc of mainCategory) {
        await s.page.goto(catc.href, { timeout: 0 })
        console.log("url", catc.href)
        var ispage = true;
        while (ispage) {
          
          const urlselector = "//*[@id='course-search-results']/section/div/div/div/h2/a"
          let studylevelSel = " //*[@id='course-search-results']/section/div/div/div[1]/p";
          const textselector = "//*[@id='course-search-results']/section/div/div/div/h2/a";
          var categoryurlsele = await s.page.$x(urlselector);
          let studylevelTarget = await s.page.$x(studylevelSel);
          var categorytext = await s.page.$x(textselector);
          for (let i = 0; i < categorytext.length; i++) {
            var categorystring = await s.page.evaluate(el => el.innerText, categorytext[i]);
            var categoryurl = await s.page.evaluate(el => el.href, categoryurlsele[i]);
            const studyLevelLink = await s.page.evaluate(el => el.textContent, studylevelTarget[i]);
            datalist.push({ href: categoryurl, innerText: categorystring, studyLevel: studyLevelLink, category: catc.innerText });
          }
          const active = await s.page.$x(activeselector);
          if (active[0]) {
            console.log("Next");
            await active[0].click();
            await s.page.waitFor(10000);
          }
          else {
            ispage = false;
          }
        }
      }
      //datalist=JSON.parse(fs.readFileSync(configs.opCourseListFilepath));
      var uniquecases = [], uniquedata = [], finalDict = [];
      datalist.forEach(element => {
        if (!uniquecases.includes(element.href)) {
          uniquecases.push(element.href);
          uniquedata.push(element);
        }
      });
      uniquecases.forEach(element => {
        var data = datalist.filter(e => e.href == element);
        var category = [];
        data.forEach(element => {
          if (!category.includes(element.category))
            category.push(element.category);
        });
        data[0].category = (Array.isArray(category)) ? category : [category];
        finalDict.push(data[0]);
      });
      console.log("unique data-->" + uniquecases.length);
      fs.writeFileSync("./output/courselist_uniq.json", JSON.stringify(uniquedata));
      fs.writeFileSync("./output/courselist_original.json", JSON.stringify(datalist));
      console.log(funcName + 'writing courseList to file....');
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(finalDict));
      await fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
}
module.exports = { ScrapeCourseList };