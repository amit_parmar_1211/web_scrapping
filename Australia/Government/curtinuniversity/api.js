const fs = require('fs');
start();
async function start() {
   const generateUnie = false, prrocessfiles = true;
   if (generateUnie) {
      var inputDir = "./Input";
      var myfiles = await readFile();
      var mynewdata = [];
      var newcampuses = [];
      for (let filename of myfiles) {
         var filedata = JSON.parse(fs.readFileSync(inputDir + "/" + filename));
         univId = filedata.univ_id;
         //   console.log("English Requirements",JSON.stringify( filedata.course_admission_requirement.english))
         try {
            filedata.course_admission_requirement.english.forEach(element => {

               if (element.description == "") {
                  console.log(filedata.course_url);
               }
               mynewdata.push({ name: element.name, description: element.description, R: 0, W: 0, S: 0, L: 0, O: 0 });

            });
            filedata.course_campus_location.forEach(element => {
               newcampuses.push(element);
            });

         } catch (error) {
            console.log("ERROR => file ->", filename);
            console.log("ERROR =>", error);
         }

      }
      console.log("total data-->" + mynewdata.length);
      fs.writeFileSync("alldata.json", JSON.stringify(mynewdata));
      fs.writeFileSync("alldataCampus.json", JSON.stringify(newcampuses));
      //IELTS unique cases
      var uniquecases = [], uniquecasesdata = [];
      mynewdata.forEach(element => {
         if (!uniquecasesdata.includes(element.description)) {
            uniquecases.push(element);
            uniquecasesdata.push(element.description);
         }
      });


      //Campus unique cases
      var uniquecasesCampus = [], uniquecasesdataCampus = [];
      newcampuses.forEach(element => {
         if (!uniquecasesdataCampus.includes(element.name)) {
            uniquecasesCampus.push({ name: element.name, city: "", state: "", country: "" });
            uniquecasesdataCampus.push(element.name);
         }

      });

      fs.writeFileSync(univId + "_uniquedataCampus.json", JSON.stringify(uniquecasesCampus));
      fs.writeFileSync(univId + "_uniquedataIELTS.json", JSON.stringify(uniquecases));
   }
   if (prrocessfiles) {
      console.log("ENTERED");
      var fileuniqdata = JSON.parse(fs.readFileSync("engineeringinstituteoftechnology_uniquedataIELTS.json"));
      var fileuniqdataCampus = JSON.parse(fs.readFileSync("engineeringinstituteoftechnology_uniquedataCampus.json"));
      // console.log("fileuniqdata -->", fileuniqdata);
      var inputDir = "./Input";
      var myfiles = await readFile();
      for (let filename of myfiles) {
         var filedata = JSON.parse(fs.readFileSync(inputDir + "/" + filename));
         try {
            var baseId = filedata.course_title;
            filedata.basecourseid = baseId.trim().toLowerCase().replace(/ /g, '');

            // program code
            filedata.program_code = filedata.program_code.join('/');
            var newdata = [];
            filedata.course_admission_requirement.english.forEach(element => {

               var data = fileuniqdata.filter(e => e.description == element.description && e.name == element.name);
               var progressBar = filedata.course_admission_requirement.englishprogress.filter(e => e.name == element.name);

               let payload = data[0];
               payload.min = progressBar[0].value.min;
               payload.require = progressBar[0].value.require;
               payload.max = progressBar[0].value.max;
               // console.log(payload);
               newdata.push(payload);
            });

            delete filedata.course_admission_requirement.englishprogress;
            filedata.course_admission_requirement.english = newdata;
            var campusdata = [];
            filedata.course_campus_location.forEach(element => {
               // let tempArray = [];

               var data = fileuniqdataCampus.filter(e => e.name == element.name);
               //var progressBar = filedata.course_admission_requirement.englishprogress.filter(e => e.name == element.name);

               data[0].iscurrent = element.iscurrent;
               // tempArray.push(data[0]);
               let payload = data[0];
               payload.iscurrent = element.iscurrent;
               payload.city = data[0].city;
               payload.state = data[0].state;
               payload.country = data[0].country;
               // console.log(payload);
               campusdata.push(payload);
            });


            filedata.course_campus_location = campusdata;
            fs.writeFileSync("./Output/" + filename, JSON.stringify(filedata));
         } catch (error) {
            console.log("ERROR =>", filename);
         }


      }

   }
}

async function readFile() {
   return await fs.readdirSync("./Input")
}

