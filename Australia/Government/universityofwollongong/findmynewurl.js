const fs = require('fs');
const puppeteer = require('puppeteer');
start();

async function start() {
    let browser = await puppeteer.launch({ headless: false }); //headless:false so we can debug
    let page = await browser.newPage();
    // await page.goto("https://coursefinder.uow.edu.au/index.html", { timeout: 0 });
    var inputDir = "./Inputfiles";
    var outputdir = "./Outputfiles";
    var comparefile = "universityofwollongong_category_courselist.json";
    var myfiles = await readFile();
    var category404 = [];
    for (let filename of myfiles) {
        var categorymatch = [];
        console.log("## start file-->" + filename)
        var mynewdata = []

        var filedata = JSON.parse(fs.readFileSync(inputDir + "/" + filename));
        var course_url = filedata.course_url;
        await page.goto(course_url, { timeout: 0 });
        console.log(filedata.course_url)
        console.log("****************************************");
        // var course_url = filedata.course_url.replace(/\?year=2019/g, '').replace(/\?year=2020/g, '');
        let newUrl = await page.url();
        filedata.new_course_url = newUrl;
        await page.waitFor(2000);

        fs.writeFileSync(inputDir + "/" + filename, JSON.stringify(filedata));
        console.log("newUrl -->", newUrl);

    }
    await browser.close();
    // fs.writeFileSync(outputdir + "/categoryNotFoundList.json", JSON.stringify(category404));
}
async function readFile() {
    return await fs.readdirSync("./Inputfiles")
}
