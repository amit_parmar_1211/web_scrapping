const puppeteer = require('puppeteer');
const fs = require("fs");
async function start() {
    const URL = "https://www.uow.edu.au/";
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    const categoryselectorUrl = "#main-content > section.grid-container.uw-start-your-journey > div.uw-study-areas.js-scroll-reveal--left > div > div > a";
    const categoryselectorText = "#main-content > section.grid-container.uw-start-your-journey > div.uw-study-areas.js-scroll-reveal--left > div > div > a > span";
    var elementstring = "", elementhref = "", allcategory = [];
    const targetLinksCardsUrls = await page.$$(categoryselectorUrl);
    const targetLinksCardsText = await page.$$(categoryselectorText);
    var targetLinksCardsTotal = [];
    for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        elementstring = await page.evaluate(el => el.innerText, targetLinksCardsText[i]);
        elementhref = await page.evaluate(el => el.href, targetLinksCardsUrls[i]);
        targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
    }
    // await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
    console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));
    let linkselector = "#study-type > div > div > div > ul > li > a";
    let textselector = "#study-type > div > div > div > ul > li > a"
    var totalCourseList = [];
    for (let target of targetLinksCardsTotal) {
        await page.goto(target.href);
        //scrape university courselist
        const targetLinks = await page.$$(linkselector);
        const targetText = await page.$$(textselector);

        
        console.log("#total link selectors---->" + targetLinks.length);
        for (var i = 0; i < targetLinks.length; i++) {
            var elementstring = await page.evaluate(el => el.innerText, targetText[i]);
            const elementlink = await page.evaluate(el => el.href, targetLinks[i]);
            totalCourseList.push({ href: elementlink, innerText: elementstring, category: target.innerText });
        }
    }
    console.log("totalCourseList -->", totalCourseList);
    fs.writeFileSync("universityofwollongong_category_courselist.json", JSON.stringify(totalCourseList));
    await browser.close();

}
start();
