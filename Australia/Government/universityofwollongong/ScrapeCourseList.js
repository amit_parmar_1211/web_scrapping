const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');

const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to https://coursefinder.uow.edu.au/undergrad/index.html
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
      const categoryselectorUrl = "#main-content > section.grid-container.uw-start-your-journey > div.uw-study-areas.js-scroll-reveal--left > div > div > a";
      const categoryselectorText = "#main-content > section.grid-container.uw-start-your-journey > div.uw-study-areas.js-scroll-reveal--left > div > div > a > span";
      var elementstring = "", elementhref = "", allcategory = [];
      const targetLinksCardsUrls = await page.$$(categoryselectorUrl);
      const targetLinksCardsText = await page.$$(categoryselectorText);
      var targetLinksCardsTotal = [];
      for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        elementstring = await page.evaluate(el => el.innerText, targetLinksCardsText[i]);
        elementhref = await page.evaluate(el => el.href, targetLinksCardsUrls[i]);
        targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
      }
       await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
      console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));
      let linkselector = "#study-type > div > div > div > ul > li > a";
      let textselector = "#study-type > div > div > div > ul > li > a"
      var totalCourseList = [];


      for (let target of targetLinksCardsTotal) {
        await page.goto(target.href, { timeout: 0 });
        var links = await page.$x("//*[@id='study-type']/div/div/button[@class='undergraduate']");
        await links[0].click();

        console.log("Clicked")
        let elementstring12 = await page.evaluate(el => el.innerText, links[0]);
        let ugselector = "//*[@class='tab-content undergraduate']/ul/li/a";
        let ugtextselecor = "//*[@class='tab-content undergraduate']/ul/li/a";

        await page.waitFor(5000);
        const ugtargetlinks = await page.$x(ugselector);
        const ugtargettext = await page.$x(ugtextselecor);
        for (var i = 0; i < ugtargetlinks.length; i++) {
          var ugelementstring = await page.evaluate(el => el.innerText, ugtargettext[i])
          const ugelementlink = await page.evaluate(el => el.href, ugtargetlinks[i])
          totalCourseList.push({ href: ugelementlink, innerText: ugelementstring, category: target.innerText, study_level: elementstring12 });
        }
        console.log("totalCourseList ----->", totalCourseList);
        var pglinks = await page.$x("//*[@id='study-type']/div/div/button[@class='postgraduate']")
        await pglinks[0].click();
        console.log("Clicked pg")
        let elementstring123 = await page.evaluate(el => el.innerText, pglinks[0]);
        let pgselector = "//*[@class='tab-content postgraduate']/ul/li/a";
        let pgtextselecor = "//*[@class='tab-content postgraduate']/ul/li/a";
        await page.waitFor(5000);
        const pgtargetlinks = await page.$x(pgselector);
        const pgtargettext = await page.$x(pgtextselecor);

        for (var i = 0; i < pgtargetlinks.length; i++) {
          var pgelementstring = await page.evaluate(el => el.innerText, pgtargettext[i])
          const pgelementlink = await page.evaluate(el => el.href, pgtargetlinks[i])
          totalCourseList.push({ href: pgelementlink, innerText: pgelementstring, category: target.innerText, study_level: elementstring123 });
        }
        await fs.writeFileSync("./output/universityofwollongong_data.json", JSON.stringify(totalCourseList));
        await fs.writeFileSync("./output/universityofwollongong_original_courselist.json", JSON.stringify(totalCourseList));
      }
      await fs.writeFileSync("./output/universityofwollongong_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [],study_level: totalCourseList[i].study_level });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] ,study_level: totalCourseList[i].study_level});
        }
      }
      await fs.writeFileSync("./output/universityofwollongong_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/universityofwollongong_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }

  // scrape course page list of the univ
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  // refers to https://coursefinder.uow.edu.au/undergrad/index.html
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // course_level_form_selector key
      // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelList) {
      //   console.log(funcName + 'Invalid formSelList');
      //   throw (new Error('Invalid formSelList'));
      // }
      // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelElementDict) {
      //   console.log(funcName + 'Invalid formSelElementDict');
      //   throw (new Error('Invalid formSelElementDict'));
      // }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');

      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
