const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const format_functions = require('./common/format_functions');
const configs = require('./configs');
request = require('request');

class ScrapeCourse extends Course {
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, study_level) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      var demoarray = [];
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'course_discipline': {
              // As of not we do not have way to get this field value
              resJsonData.course_discipline = course_category;
              break;
            }
            case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              const courseAdminReq = {};
              const englishList = [];
              var penglishList = [];
              var otherLngDict = null;
              const ieltsDict = {}; const ibtDict = {}; const pbtDict = {}; const pteDict = {}; const caeDict = {}; const cpeDict = {}; let ieltsNumber = null; let toeflNumber = null;
              // english requirement
              var toeflScore;
              console.log('************************start ielts course*************************');
              var ieltsfinaloutput = null;
              var tofelfinaloutput = null;
              let hardcodetoeflString;
              let hardcodeIeltsString;
              const ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);

              if (ieltsScore && ieltsScore.length > 0) {
                console.log('Course english language requirment :' + JSON.stringify(courseScrappedData.course_toefl_ielts_score));
                console.log('Course english language requirment :', ieltsScore);
                let ieltsfinaloutput = ieltsScore.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ';').trim().split(';');
                console.log("IELTS score for user :", ieltsfinaloutput);
                hardcodeIeltsString = ieltsfinaloutput[2];
                if (ieltsfinaloutput.length == 11) {
                  hardcodeIeltsString = "Overall " + ieltsfinaloutput[2] + " Reading " + ieltsfinaloutput[4] + " Writing " + ieltsfinaloutput[6] + " Listening " + ieltsfinaloutput[8] + " Speaking " + ieltsfinaloutput[10];
                }
                if (hardcodeIeltsString && hardcodeIeltsString.length > 0) {

                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(hardcodeIeltsString).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    ieltsNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                  }
                }
              }
              toeflScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_score);
              if (toeflScore && toeflScore.length > 0) {
                console.log('Course english language requirment :' + JSON.stringify(courseScrappedData.course_toefl_score));
                console.log('Course english language requirment :', toeflScore);
                tofelfinaloutput = toeflScore.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ';').trim().split(';');
                console.log("IELTS score for user :", tofelfinaloutput);
                hardcodetoeflString = tofelfinaloutput[2];
                if (tofelfinaloutput.length == 11) {
                  hardcodetoeflString = "Overall " + tofelfinaloutput[2] + " Reading " + tofelfinaloutput[4] + " Writing " + tofelfinaloutput[6] + " Listening " + tofelfinaloutput[8] + " Speaking " + tofelfinaloutput[10];
                }
                if (hardcodetoeflString && hardcodetoeflString.length > 0) {

                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(hardcodetoeflString).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    toeflNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'ieltsNumber = ' + toeflNumber);
                  }
                }
              }
              // ieltsfinaloutput = "Overall Score = " + ieltsNumber;
              // if ieltsNumber is valid then map IBT and PBT accordingly
              console.log("ieltsNumber -->", ieltsNumber);
              console.log("toeflNumber -->", toeflNumber);
              console.log("hardcodetoeflString -->", hardcodetoeflString);
              console.log("hardcodeIeltsString -->", hardcodeIeltsString);
              if (ieltsNumber && ieltsNumber > 0) {
                if (hardcodeIeltsString) {
                  ieltsDict.name = 'ielts academic';
                  ieltsDict.description = hardcodeIeltsString;
                  englishList.push(ieltsDict);
                }
              }
              if (toeflNumber && toeflNumber > 0) {
                if (hardcodetoeflString) {
                  ibtDict.name = 'toefl ibt';
                  ibtDict.description = hardcodetoeflString
                  englishList.push(ibtDict);
                }

              }



              // console.log("englishList --> ", englishList);
              // if (englishList && englishList.length > 0) {
              //   courseAdminReq.english = englishList;
              // } else {
              //   courseAdminReq.english = [];
              // }

              console.log("englishList --> ", englishList);
              ///progress bar start
              const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};


              if (ieltsNumber) {
                pieltsDict.name = 'ielts academic';
                pieltsDict.description = ieltsDict.description;
                pieltsDict.min = 0;
                pieltsDict.require = ieltsNumber;
                pieltsDict.max = 9;
                pieltsDict.R = 0;
                pieltsDict.W = 0;
                pieltsDict.S = 0;
                pieltsDict.L = 0;
                pieltsDict.O = 0;
                penglishList.push(pieltsDict);
              }
              if (toeflNumber) {
                pibtDict.name = 'toefl ibt';
                pibtDict.description = ibtDict.description;
                pibtDict.min = 0;
                pibtDict.require = toeflNumber;
                pibtDict.max = 120;
                pibtDict.R = 0;
                pibtDict.W = 0;
                pibtDict.S = 0;
                pibtDict.L = 0;
                pibtDict.O = 0;
                penglishList.push(pibtDict);
              }
              console.log("penglishList -->", penglishList);
              if (penglishList && penglishList.length > 0) {
                courseAdminReq.english = penglishList;
              } else {
                courseAdminReq.english = [];
              }
              ///progrssbar End

              // academic requirement
              if (courseScrappedData.course_academic_requirement) {
                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                console.log('\n\r');
                console.log(funcName + 'academicReq = ' + academicReq);
                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                console.log('\n\r');
                if (academicReq && String(academicReq).length > 0) {
                  courseAdminReq.academic = [academicReq];
                } else {
                  courseAdminReq.academic = [];
                }
              }
              resJsonData.course_admission_requirement = {};
              // if courseAdminReq has any valid value then only assign it
              if (courseAdminReq.english || courseAdminReq.academic) {
                resJsonData.course_admission_requirement = courseAdminReq;
              }

              let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
              let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
              let academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);

              if (english_requirements_url && english_requirements_url != null) {
                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
              } else {
                resJsonData.course_admission_requirement.english_requirements_url = "";
              }

              if (entry_requirements_url && entry_requirements_url != null) {
                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
              } else {
                resJsonData.course_admission_requirement.entry_requirements_url = "";
              }
              if (academic_requirements_url && academic_requirements_url != null) {
                resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;
              } else {
                resJsonData.course_admission_requirement.academic_requirements_url = "";
              }
              console.log('************************end ielts course*************************');
              break;
            }
            case 'course_url': {
              if (courseUrl && courseUrl.length > 0) {
                resJsonData.course_url = courseUrl;
              }
              break;
            }
            case 'course_outline': {
              console.log("course_outline--->")
              const outline = {
                majors: [],
                minors: [],
                more_details: ""
              };

              var major, minor
              var intakedata1 = {};
              const courseKeyVal = courseScrappedData.course_outline_major;
              console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

              let resScholarshipJson = null;


              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList;
                        console.log("resScholarshipJson", resScholarshipJson)
                      }
                    }
                  }
                }
              }
              if (resScholarshipJson) {
                major = resScholarshipJson
              } else {
                major = []
              }




              const courseKeyVal1 = courseScrappedData.course_outline_minors;
              let resScholarshipJson1 = null;
              if (Array.isArray(courseKeyVal1)) {
                for (const rootEleDict of courseKeyVal1) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson1 = selList;

                      }
                    }
                  }
                }
              }
              if (resScholarshipJson1) {
                minor = resScholarshipJson1
              } else {
                minor = []
              }

              let m_d = resJsonData.course_url;
              console.log("md--------->", m_d)

              var intakedata1 = {};
              intakedata1.majors = major;
              intakedata1.minors = minor;
              intakedata1.more_details = m_d;
              resJsonData.course_outline = intakedata1;


              break;
            }

            case 'application_fee':
              {
                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                resJsonData.application_fee = ""
                break;
              }
            //CUSTOMIZED CODE DO NOT COPY
            case 'course_duration_full_time': {
              const courseDurationList = [];
              let courseDurationDisplayList = [];
              const durationFullTime = {};

              let anotherArray = [];
              let durationFullTimeDisplay = null;
              let duration1 = courseScrappedData.course_duration_full_time;

              console.log("*************start formating Full Time years*************************");
              console.log("courseScrappedData.course_duration_full_time = ", JSON.stringify(courseScrappedData.course_duration_full_time));
              let finalDuration = duration1[0].elements[0].selectors[0][0];
              console.log("Duration1 = ", finalDuration.trim());
              if (finalDuration.trim().indexOf('full-time') == -1) {
                finalDuration = "full-time " + finalDuration;
              }
              if (finalDuration.trim().indexOf('(') > -1) {
                console.log("Duration2 = ", finalDuration.trim());
                //4.5 (9 sessions) years full-time or part-time equivalent
                let tempArray1 = finalDuration.split('(')[0];
                console.log("tempArray1 = ", tempArray1);
                let tempArray = finalDuration.split('(')[1];
                console.log("tempArraytempArraytempArraytempArray = ", tempArray);
                finalDuration = tempArray1 + tempArray.split(')')[1];
              }
              console.log("Duration1 = ", finalDuration.trim());
              const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration.trim());
              courseDurationDisplayList.push(full_year_formated_data);
              demoarray = full_year_formated_data[0];
              console.log("demoarray--->", demoarray);
              console.log("full_year_formated_data", full_year_formated_data);
              ///course duration
              const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
              console.log("not_full_year_formated_data", not_full_year_formated_data);
              try {
                durationFullTime.duration_full_time = not_full_year_formated_data;
                courseDurationList.push(durationFullTime);

                courseDurationDisplayList = [full_year_formated_data];
                // anotherArray.push(courseDurationDisplayList);
                console.log("courseDurationDisplayList", full_year_formated_data);
                ///END course duration display
              } catch (err) {
                console.log("Problem in Full Time years:", err);
              }
              console.log("courseDurationDisplayList1", courseDurationDisplayList);
              let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
              console.log("filtered_duration_formated -->", filtered_duration_formated);
              console.log('***************END formating Full Time years**************************')

              if (courseDurationList && courseDurationList.length > 0) {
                resJsonData.course_duration = finalDuration;
              }
              if (filtered_duration_formated && filtered_duration_formated.length > 0) {

                resJsonData.course_duration_display = filtered_duration_formated;

                var isfulltime = false, isparttime = false;
                filtered_duration_formated.forEach(element => {
                  if (element.display == "Full-Time") {
                    isfulltime = true;
                  }
                  if (element.display == "Part-Time") {
                    isparttime = true;
                  }
                });
                resJsonData.isfulltime = isfulltime;
                resJsonData.isparttime = isparttime
              }
              break;
            }
            case 'course_tuition_fee':
            //case 'course_study_mode':
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_duration_part_time': {
              console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
              // No need to process anything, its already processed in other case.
              break;
            }
            case 'course_tuition_fees_international_student_2020': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};
              // courseTuitionFee.year = configs.propValueNotAvaialble;

              const feesList = [];
              const feesDict = {
                international_student: {}
              };
              // feesDict.international_student = [];
              console.log("demoarray123 -->", demoarray);
              // const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
              let courseKeyVal_fees = courseScrappedData.course_tuition_fees_international_student_2020;
              let coursefees;
              if (Array.isArray(courseKeyVal_fees)) {
                for (const rootEleDict of courseKeyVal_fees) {
                  console.log(funcName + '\n\r rootEleDict ieltsDict= ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict ieltsDict= ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList ieltsDict= ' + JSON.stringify(selList));
                      if (Array.isArray(selList)) {
                        for (let sel of selList) {
                          if (coursefees != sel) {
                            coursefees = sel;
                          }
                        }
                      }
                    }
                  }
                }
              }
              const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
              // if (feeYear && feeYear.length > 0) {
              //   courseTuitionFee.year = feeYear;
              // }
              // if we can extract value as Int successfully then replace Or keep as it is
              if (coursefees && coursefees.length > 0) { // extract only digits
                const feesWithDollorTrimmed = String(coursefees).trim();
                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');

                const arrval11 = String(feesVal1).split('(')[0];
                const arrval = String(arrval11).split('.');
                const feesVal = String(arrval[0]);
                console.log(funcName + 'feesVal = ' + feesVal);
                if (feesVal) {
                  const regEx = /\d/g;
                  let feesValNum = feesVal.match(regEx);
                  if (feesValNum) {
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    feesValNum = feesValNum.join('');
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    let feesNumber = null;
                    if (feesValNum.includes(',')) {
                      feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                    } else {
                      feesNumber = feesValNum;
                    }

                    console.log(funcName + 'feesNumber = ' + feesNumber);
                    if (Number(feesNumber)) {
                      feesDict.international_student = ({
                        amount: Number(feesNumber),
                        duration: Number(demoarray.duration),
                        unit: demoarray.unit,

                        description: feesWithDollorTrimmed

                      });
                    } else {
                      feesDict.international_student = ({
                        amount: 0,
                        duration: Number(demoarray.duration),
                        unit: demoarray.unit,

                        description: "not available fee",

                      });
                    }
                    console.log("feesDictinternational_student-->", feesDict.international_student);
                  }
                }
              } else {
                feesDict.international_student = ({
                  amount: 0,
                  duration: Number(demoarray.duration),
                  unit: demoarray.unit,

                  description: "not available fee"

                });
              } // if (feesIntStudent && feesIntStudent.length > 0)
              // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
              const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
              console.log(funcName + 'cricoseStr = ' + cricoseStr);
              console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
              if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                if (fixedFeesDict) {
                  for (const codeKey in fixedFeesDict) {
                    console.log(funcName + 'codeKey = ' + codeKey);
                    if (fixedFeesDict.hasOwnProperty(codeKey)) {
                      const keyVal = fixedFeesDict[codeKey];
                      console.log(funcName + 'keyVal = ' + keyVal);
                      if (cricoseStr.includes(codeKey)) {
                        const feesDictVal = fixedFeesDict[cricoseStr];
                        console.log(funcName + 'feesDictVal = ' + feesDictVal);
                        if (feesDictVal && feesDictVal.length > 0) {
                          const inetStudentFees = Number(feesDictVal);
                          console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                          if (Number(inetStudentFees)) {
                            feesDict.international_student = Number(inetStudentFees);
                          }
                        }
                      }
                    }
                  } // for
                } // if
              } // if


              var feesIntStudent111 = null;//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
              const courseKeyVal = courseScrappedData.course_tuition_fees_international_student_Session_2020;
              console.log("courseKeyVal-->  ", courseKeyVal);
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        feesIntStudent111 = selList;
                      }
                    }
                  }
                }
              }
              let newfeesmore = [];
              for (let feesvalmore of feesIntStudent111) {
                if (!newfeesmore.includes(feesvalmore)) {
                  newfeesmore.push(feesvalmore);
                  console.log("##Campuscampus-->" + newfeesmore)
                }
              }
              console.log("feesIntStudent111-->  ", feesIntStudent111);
              //  const feeStudent111 = feesIntStudent111.toString().replace(/[\r\n\t ]+/g, ' ').trim();
              //   let international_student_all_fees_array = feesIntStudent111;

              if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                }
                // feesDict.international_student_all_fees = [];
                // if (international_student_all_fees_array && international_student_all_fees_array.length > 0) {

                //   for (let student_all_fees of international_student_all_fees_array) {
                // //    feesDict.international_student_all_fees.push(String(feesIntStudent111));
                //   }
                // }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }
                if (feesDict) {
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                if (courseTuitionFee) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
                // take tuition fee value at json top level so will help in DDB for indexing
                if (courseTuitionFee && feesDict.international_student) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                  // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                }
              }
              console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
              // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
              if (!feesDict.international_student) {
                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                return null; // this will add this item into FailedItemList and writes file to local disk
              }
              break;
            }

            case 'course_campus_location': { // Location Launceston
              console.log("Campus Location = ", JSON.stringify(courseScrappedData.course_campus_location));
              let loc1 = courseScrappedData.course_campus_location[0].elements[0].selectors[0];
              console.log("loc1 --> ", loc1);
              const Cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);

              let campusLocationArray = [];
              campusLocationArray = loc1[0].split(', ');
              let newArray = [];
              console.log("campusLocationArray -->", campusLocationArray);
              for (let campus of campusLocationArray) {
                if (campus.indexOf('Online') > -1 || campus.indexOf('online') > -1) {

                } else {
                  if (campus.toLowerCase().trim().indexOf('southern sydney') > -1) {
                    newArray.push("Southern Sydney");
                  } else if (campus.toLowerCase().trim().indexOf('southern highlands') > -1) {
                    newArray.push("Southern Highlands");
                  } else if (campus.toLowerCase().trim().indexOf('shoalhaven') > -1) {
                    newArray.push("Shoalhaven");
                  } else if (campus.toLowerCase().trim().indexOf('bega') > -1) {
                    newArray.push("Bega");
                  } else if (campus.toLowerCase().trim().indexOf('batemans bay') > -1) {
                    newArray.push("Batemans Bay");
                  } else if (campus.toLowerCase().trim().indexOf('innovation') > -1) {
                    newArray.push("Innovation");
                  } else if (campus.toLowerCase().trim().indexOf('wollongong') > -1) {
                    newArray.push("Wollongong");
                  } else if (campus.toLowerCase().trim().indexOf('south western sydney') > -1) {
                    newArray.push("South Western Sydney");
                  }
                  else if (campus.toLowerCase().trim().indexOf('sydney') > -1) {
                    newArray.push("Sydney");
                  }
                }
              }
              console.log("newArray -->", newArray);


              if (newArray && newArray.length > 0) {
                var campusedata = [];
                newArray.forEach(element => {
                  campusedata.push({
                    "name": format_functions.titleCase(element),
                    "code": Cricos.trim()

                  })
                });
                resJsonData.course_campus_location = campusedata;
                //   resJsonData.course_campus_location = newArray;

              }
              break;
            }
            case 'course_study_mode': { // Location Launceston
              let studyModeArray = [];
              const coursestudymodeText = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_mode);
              console.log(funcName + 'coursestudymodeText = ' + coursestudymodeText);
              // if (coursestudymodeText && coursestudymodeText.length > 0) {
              //   const coursestudymodeValTrimmed = String(coursestudymodeText).trim();
              //   console.log('***********************************strat course study mode****************************');
              //   console.log(funcName + 'coursestudymodeValTrimmed = ' + coursestudymodeValTrimmed);
              //   console.log('***********************************END course study mode********************************');

              // }
              studyModeArray.push('On campus');
              console.log("studyModeArray -->", studyModeArray);
              resJsonData.course_study_mode = "On campus";
              break;
            }

            case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
              //  let staticCampuses = await utils.getValueFromHardCodedJsonFile('campuses');
              var courseIntakeStr = [];//= await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
              let intakeExitsFlag = true;
              const courseKeyVal = courseScrappedData.course_intake;
              console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList Intake= ' + JSON.stringify(selList));
                      // if (Array.isArray(selList) && selList.length > 0) {
                      if (selList && selList.length > 0) {
                        for (let i = 0; i < selList[0].length; i++) {

                          let IntakeDate = [];
                          console.log("selList[0][i] --> ", selList[0][i]);
                          let splitString = selList[1][i];
                          let splitArr = [];
                          if (resJsonData.course_title == 'Doctor Of Medicine') {
                            splitArr = "27 January";
                            IntakeDate = (splitArr);
                          }
                          else {
                            splitArr = splitString.split("Session: ");
                            console.log("splitArr --> ", splitArr);

                            if (splitArr[1].indexOf(' - ') > -1) {
                              IntakeDate = splitArr[1].split(' - ')[0].trim();
                              console.log("IntakeDate --> ", IntakeDate);

                            }
                            else if (splitString.includes("Orientation: Check your campus detailsSession:")) {
                              splitArr = splitString.replace("Orientation: Check your campus detailsSession:", '').trim()
                              console.log("splitArrdsfdfdg --> ", splitArr);
                            }
                            if (splitArr.indexOf('–') > -1) {
                              IntakeDate = splitArr.split('–')[0].trim();
                              console.log("IntakeDdsddfdrate --> ", IntakeDate);

                            }


                            else {
                              IntakeDate = splitArr[1].split(" – ")[0].trim();
                              console.log("IntakedfgdfgdgDate --> ", IntakeDate);
                            }
                          }
                          let selListArray = selList[0][i].split(',');
                          let n = selListArray.length;
                          for (let j = 0; j < n; j++) {
                            if (selListArray[j].toLowerCase().trim().indexOf('southern sydney') > -1) {
                              courseIntakeStr.push({ "name": "Southern Sydney", "value": IntakeDate });
                            } else if (selListArray[j].toLowerCase().trim().indexOf('southern highlands') > -1) {
                              courseIntakeStr.push({ "name": "Southern Highlands", "value": IntakeDate });
                            } else if (selListArray[j].toLowerCase().trim().indexOf('shoalhaven') > -1) {
                              courseIntakeStr.push({ "name": "Shoalhaven", "value": IntakeDate });
                            } else if (selListArray[j].toLowerCase().trim().indexOf('bega') > -1) {
                              courseIntakeStr.push({ "name": "Bega", "value": IntakeDate });
                            } else if (selListArray[j].toLowerCase().trim().indexOf('batemans bay') > -1) {
                              courseIntakeStr.push({ "name": "Batemans Bay", "value": IntakeDate });
                            } else if (selListArray[j].toLowerCase().trim().indexOf('innovation') > -1) {
                              courseIntakeStr.push({ "name": "Innovation", "value": IntakeDate });
                            } else if (selListArray[j].toLowerCase().trim().indexOf('wollongong') > -1) {
                              courseIntakeStr.push({ "name": "Wollongong", "value": IntakeDate });
                            } else if (selListArray[j].toLowerCase().trim().indexOf('south western sydney') > -1) {
                              courseIntakeStr.push({ "name": "South Western Sydney", "value": IntakeDate });
                            } else if (selListArray[j].toLowerCase().trim().indexOf('sydney') > -1) {
                              courseIntakeStr.push({ "name": "Sydney", "value": IntakeDate });
                              console.log("courseIntakeStr======>", courseIntakeStr)
                            }

                          }

                        }
                      } else {
                        console.log("intake not defined")
                        intakeExitsFlag = false;
                        throw (new Error('intake not defined'));


                      }

                    }
                  }
                }
              }
              console.log("Step 1 --> ", courseIntakeStr);
              let finalArray = [];
              if (intakeExitsFlag == true) {
                console.log("intakes" + JSON.stringify(courseIntakeStr));
                let Loca = resJsonData.course_campus_location
                console.log("LocaLocaLocaLocaLoca", Loca)
                if (courseIntakeStr && courseIntakeStr.length > 0) {
                  for (let newName of resJsonData.course_campus_location) {
                    let payload = {};
                    payload.name = newName.name;
                    payload.value = [];
                    let intakeFlag = false;
                    for (let oldName of courseIntakeStr) {
                      if (newName.name == oldName.name.trim()) {
                        console.log("oldName.value[0] -->", oldName.value);
                        payload.value.push(oldName.value);
                        intakeFlag = true;
                      }
                    }
                    console.log("payload -->", payload);
                    if (intakeFlag) {
                      finalArray.push(payload);
                    }
                  }
                  console.log("finalArray --> ", finalArray)

                } else {
                  console.log("intake not defined")
                  intakeExitsFlag = false;
                  throw (new Error('intake not defined'));


                }


                //  else {
                //   //If intakes are not given
                //   for (let newName of resJsonData.course_campus_location) {
                //     let payload = {};
                //     payload.name = newName;
                //     payload.value = [];
                //     console.log("payload -->", payload);
                //     finalArray.push(payload);
                //   }
                // }
                const more_details = await utils.getValueFromHardCodedJsonFile('intake_url');

                let formatedIntake = await format_functions.providemyintake(finalArray, "MOM");
                console.log("formatedIntake -->", formatedIntake);
                var intakedata = {};
                if (formatedIntake && formatedIntake.length > 0) {
                  var intakedata = {};
                  intakedata.intake = formatedIntake;
                  // console.log("FormatIntake", JSON.stringify(formatedIntake))
                  // formatedIntake.forEach(element => {
                  //   element.value.forEach(elementdate => {
                  //     if (elementdate.actualdate == '29 July') {
                  //       elementdate.filterdate = "2019-07-29"
                  //     } else if (elementdate.actualdate == '2 March') {
                  //       elementdate.filterdate = "2020-03-02"
                  //     }
                  //     //elementdate.push({filterdate:myintake_data["term 1_date"]})
                  //     console.log("DATA--->", elementdate)
                  //   })
                  // });
                }
                intakedata.more_details = more_details;
                resJsonData.course_intake = intakedata;
              }

              break;
            }
            case 'course_outline': {
              const MAX_SUBJECTS_COUNT = 25;
              const courseKeyVal = courseScrappedData.course_outline;
              const resOutlineList = []; // const studyUnitsDict = {};
              const concatSelectorsList = []; let moreSubjectsUrl = null;
              let res_course_outline = {
                "subject_areas": [],
                "more_details": ""
              };
              if (Array.isArray(courseKeyVal)) {
                let concatRootElementsList = [];
                for (const rootEleDict of courseKeyVal) {
                  let concatElementsList = [];
                  const elementsList = rootEleDict.elements;
                  console.log(funcName + 'elementsList course_outline = ' + JSON.stringify(elementsList));
                  for (const eleDict of elementsList) {
                    const selectorsList = eleDict.selectors;
                    console.log(funcName + 'selectorsList course_outline = ' + JSON.stringify(selectorsList));
                    for (const selList of selectorsList) {
                      let concatSelList = [];
                      console.log(funcName + 'selList course_outline = ' + JSON.stringify(selList));

                      if (selList.subject_areas && selList.subject_areas.length > 0) {
                        console.log("selList -->", selList);
                        resJsonData.course_outline = selList;
                      } else if (selList.study_units && selList.study_units.length > 0) {
                        resJsonData.course_outline = selList;
                      }

                    } // for (const selList of selectorsList)

                  } // for (const eleDict of elementsList)

                } // for
              } // if (Array.isArray(courseKeyVal))
              console.log("res_course_outline --->", resJsonData.course_outline);

              break;
            }


            case 'univ_name': {
              const courseKeyVal = courseScrappedData.univ_name;
              let resUnivName = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivName = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivName) {
                resJsonData.univ_name = resUnivName;
              }
              break;
            }



            case 'course_overview': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
              // add only if it has valid value
              // let concatnatedRootElementsStrArray = [];
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
              } else {
                resJsonData[key] = "";
              }
              break;
            }
            case 'course_career_outcome': {
              const courseKeyVal = courseScrappedData.course_career_outcome;
              let course_career_outcome = [];
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList course_career_outcome= ' + JSON.stringify(selList));
                      course_career_outcome = selList;
                    }
                  }
                }
              }
              console.log("selList course_career_outcome= -->", course_career_outcome);
              if (course_career_outcome) {
                resJsonData.course_career_outcome = course_career_outcome;
              } else {
                resJsonData.course_career_outcome = [];
              }

              break;
            }
            case 'course_country': {
              const courseKeyVal = courseScrappedData.course_country;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_country = resAccomodationCostJson;
              }
              break;

            }

            case 'course_program_code': {
              const courseKeyVal = courseScrappedData.course_program_code;
              console.log("courseScrappedData.program_code = ", courseScrappedData.course_program_code);
              var program_code = "";
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        console.log("JSON.stringify(selList) -->", JSON.stringify(selList));
                        program_code = selList;
                      }
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))
              for (let program_val of program_code) {

                resJsonData.program_code = program_val;

              }
              break;
            }
            case 'course_title': {
              const title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
              var ctitle = format_functions.titleCase(title)
              console.log("ctitle@@@", ctitle)
              resJsonData.course_title = ctitle
              break;
            }
            case 'course_study_level': {
              let cStudyLevel = null;
              console.log(funcName + 'course_title course_study_level = ' + study_level);
              resJsonData.course_study_level = study_level.trim();

              break;
            }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      ////start genrating new file location wise
      var NEWJSONSTRUCT = {}, structDict = [];
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
        // console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
        NEWJSONSTRUCT.course_campus_location = location.name;
        console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.course_campus_location));
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        console.log("##course_tuition_fee-->" + JSON.stringify(NEWJSONSTRUCT.course_tuition_fee));
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        }
        else {
          NEWJSONSTRUCT.course_intake = "";
        }
        for (let myfees of resJsonData.course_tuition_fee.fees) {
          if (myfees.name == location) {
            //  NEWJSONSTRUCT.international_student_all_fees = myfees;
          }
        }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        resJsonData.course_id = location_wise_data.course_location_id;




        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
      }
      // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
      return resJsonData;
      ///end grnrating new file location wise
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = 'scrapeAndFormatCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);

      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }
      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file

      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }

      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.study_level);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
