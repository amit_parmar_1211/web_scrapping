**University Of Wollongong Australia: Total Courses Details As On 22 Mar 2019**
* Total Courses = 462
* Courses without CRICOS code = 0
* DDB = 0
* Web portal available courses = 0
**academic_requirements_url not given**
**Intakes**
* Mannually not added anything it comes from scrapping
* There are some cases where intakes are not given so NA is pushed

*IELTS*
* There are some cases where ielts is not given so those courses are failed
* eg : https://coursefinder.uow.edu.au/undergrad/ssNODELINK/22757?course=bachelor-laws-honours-4-year-degree
* course no 119
*Fees*
* Additional Information is given i.e session fees 
*Campus location*
* split through , 
*Campuses Issue*
* there were 2 campuses of Sydney i.e South western sydney, southern sydney but took only one as there was issue in this course
* https://coursefinder.uow.edu.au/information/index.html?course=graduate-certificate-in-business