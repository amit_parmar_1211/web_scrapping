const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;

const startScrapping = async function startScrappingFunc() {
  const funcName = 'startScrappingFunc ';
  try {
    s = new Scrape();
    await s.init({ headless: true });
    var datalist = [];
    await s.setupNewBrowserPage("https://search.swinburne.edu.au/s/search.html?collection=swinburne-ug-courses-meta&query=+");
    var mainCategory = [], redirecturl = [];
    var datalist = [];
    //scrape main category
    const maincategoryselector = "//*[@id='refine-search']/div[2]/div/ul/li/a";
    var category = await s.page.$x(maincategoryselector);
    console.log("Total categories-->" + category.length);
    var studyareaopen = "//*[@id='refine-search']/div[2]/h3/a";
    var open = await s.page.$x(studyareaopen);
    await open[0].click();

    for (let link of category) {
      var categorystringmain = await s.page.evaluate(el => el.innerText, link);
      var categorystringmainurl = await s.page.evaluate(el => el.href, link);
      var mcat = categorystringmain.replace(/[\r\n\t]+/g, ' ').trim().split("(");
      mainCategory.push(mcat[0].trim());
      redirecturl.push({ innerText: mcat[0].trim(), href: categorystringmainurl });
    }
    for (let link of redirecturl) {
      console.log("start--> " + link.innerText);
      await s.page.goto(link.href + "&f.Residency%7C3=international", { timeout: 0 })
      var ispage = true;
      const href = "//*[@id='content']/main/section[1]/div/div[3]/div/a";
      const title = "//*[@id='content']/main/section[1]/div/div[3]/div/a//strong";
      const nextpage = "//*[@id='content']/main/section[1]/div/div[3]/footer/div[2]/a//span[contains(text(),'Next')]/../..";
      while (ispage) {
        var titlevalue = await s.page.$x(title);
        var linkvalue = await s.page.$x(href);
        for (let i = 0; i < titlevalue.length; i++) {
          var categorystring = await s.page.evaluate(el => el.innerText, titlevalue[i]);
          var categoryurl = await s.page.evaluate(el => el.href, linkvalue[i]);
          datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: link.innerText });
        }

        var next = await s.page.$x(nextpage);
        if (next[0]) {
          await next[0].click();
          await s.page.waitFor(5000);
          console.log("Clicked next " + datalist.length);
        }
        else {
          console.log("No more pages" + datalist.length);
          ispage = false;
        }
      }

      const clearselection = "//*[@id='content']/main/section[1]/div/div[3]/div[2]/div/a[contains(text(),'Clear All')]";
      var clear = await s.page.$x(clearselection);
      await clear[0].click();
    }

    fs.writeFileSync("./output/universityofcanberra_courselist.json", JSON.stringify(datalist))
    fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
    await s.browser.close();
    console.log(funcName + 'browser closed successfully.....');
    return true;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
};
startScrapping();