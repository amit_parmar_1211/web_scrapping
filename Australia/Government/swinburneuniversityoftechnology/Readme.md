**Swinburne University of Technology: Total Courses Details As On 11 Mar 2019**
* Total Courses = 279 as per https://search.swinburne.edu.au/s/search.html?collection=swinburne-ug-courses-meta&query=+&start_rank=1&f.Residency%7C3=international
**Intake Conditions**
* Hawthorn (Semester 1)
* Split with () and - and \n
* taken from hardcode file
**course_outline**
* Not done by me, it was copied from Shaileshbhai's code as it was able to handle both Study_Areas and study_units
**Career outcome is not given at some places**