const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require("./common/utils");
const appConfigs = require('./common/app-config');
const format_functions = require('./common/format_functions');
const configs = require('./configs');
request = require('request');

class ScrapeCourse extends Course {
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      var demoarray = [];

      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'course_discipline': {
              //  // As of not we do not have way to get this field value
              resJsonData.course_discipline = course_category;
              // const title=await Course.extractValueFromScrappedElement(courseScrappedData.course_discipline)
              //  const title=course_category;
              //   var ctitle= format_functions.titleCase(title)
              //   console.log("ctitle@@@",ctitle)
              //   resJsonData.course_discipline=ctitle
              break;
            }
            case 'program_code': {
              const courseKeyVal = courseScrappedData.program_code;
              console.log("courseScrappedData.program_code = ", courseScrappedData.program_code);
              var program_code = [];
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        program_code.push(selList[0]);
                      }
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))

              for (let program_val of program_code) {

                resJsonData.program_code = program_val;

              }

              break;
            }
            case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              const courseAdminReq = {};
              const englishList = [];
              const penglishList = [];

              const ieltsDict = {}; const ibtDict = {}; const pteDict = {}; const caeDict = {}; let ieltsNumber = null; let pbtNumber = null; let pteNumber = null; let iBtNumber = null;
              var ieltsfinaloutput = null;
              var ptefinaloutput = null;
              var iBtfinaloutput = null;
              // english requirement
              let cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
              let title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
              let t1 = resJsonData.course_title;

              console.log("title@@@@@@@@", t1);
              let splitTitle = cTitle.split('(');
              cTitle = splitTitle[0].trim();
              let finalData = courseScrappedData.course_toefl_ielts_score[0].elements[0].selectors[0];
              console.log("FinalData cTitle ::: ", finalData);
              if (finalData && finalData.length > 0) {
                for (let scoreData of finalData) {
                  console.log("scoreDate -->", scoreData);
                  if (title == "Doctor of Philosophy" || title == "Doctor of Philosophy and Graduate Certificate of Research and Innovation Management" || title == "Doctor of Philosophy (Clinical Psychology)" || title == "Doctor of Psychology (Clinical and Forensic Psychology)" || title == "Master of Arts (Research)" || title == "Master of Business (Research)" || title == "Master of Design (Research)" || title == "Master of Engineering (Research)" || title == "Master of Health Sciences (Research)"
                    || title == "Master of Information and Communication Technologies (Research)" || title == "Master of Law (Research)" || title == "Master of Research"
                    || title == "Master of Science (Research)") {
                    console.log("YESSS")
                    let ieltsString = "minimum IELTS overall band of 6.5 (Academic Module) with no individual band below 6.0";
                    let ibtString = "TOEFL iBT (internet-based) minimum score of 79 (with a reading band no less than 18 and writing band no less than 20)";
                    let pteString = "Pearson (PTE) 58 (no communicative skills less than 50)";
                    let tempScore1 = [
                      {
                        "elements":
                          [
                            {
                              "selectors":
                                [
                                  [ieltsString]
                                ]
                            }
                          ]
                      }
                    ]
                    const ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempScore1);
                    ieltsfinaloutput = ieltsScore.toString();
                    if (ieltsScore && ieltsScore.length > 0) {
                      // extract exact number from string
                      const regEx = /[+-]?\d+(\.\d+)?/g;
                      const matchedStrList = String(ieltsScore).match(regEx);
                      console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                      if (matchedStrList && matchedStrList.length > 0) {
                        ieltsNumber = Number(matchedStrList[0]);
                        console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                      }
                    }

                    console.log("Inside IBT");
                    let tempScore2 = [
                      {
                        "elements":
                          [
                            {
                              "selectors":
                                [
                                  [ibtString]
                                ]
                            }
                          ]
                      }
                    ]
                    const iBtScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempScore2);
                    if (iBtScore && iBtScore.length > 0) {
                      iBtfinaloutput = iBtScore.toString();
                      // extract exact number from string
                      const regEx = /[+-]?\d+(\.\d+)?/g;
                      const matchedStrList = String(iBtScore).match(regEx);
                      console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                      if (matchedStrList && matchedStrList.length > 0) {
                        iBtNumber = Number(matchedStrList[0]);
                        console.log(funcName + 'iBtNumber = ' + iBtNumber);
                      }
                    }

                    console.log("Inside PTE");
                    let tempScore3 = [
                      {
                        "elements":
                          [
                            {
                              "selectors":
                                [
                                  [pteString]
                                ]
                            }
                          ]
                      }
                    ]
                    const pteAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempScore3);
                    if (pteAScore && pteAScore.length > 0) {
                      ptefinaloutput = pteAScore.toString();
                      // extract exact number from string
                      const regEx = /[+-]?\d+(\.\d+)?/g;
                      const matchedStrList = String(pteAScore).match(regEx);
                      console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                      if (matchedStrList && matchedStrList.length > 0) {
                        pteNumber = Number(matchedStrList[0]);
                        console.log(funcName + 'pteNumber = ' + pteNumber);
                      }
                    }





                  }
                  else {
                    if (scoreData.indexOf('IELTS') > -1) {
                      let tempScore = [
                        {
                          "elements":
                            [
                              {
                                "selectors":
                                  [
                                    [scoreData]
                                  ]
                              }
                            ]
                        }
                      ]
                      const ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempScore);
                      ieltsfinaloutput = ieltsScore.toString();
                      if (ieltsScore && ieltsScore.length > 0) {
                        // extract exact number from string
                        const regEx = /[+-]?\d+(\.\d+)?/g;
                        const matchedStrList = String(ieltsScore).match(regEx);
                        console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                        if (matchedStrList && matchedStrList.length > 0) {
                          ieltsNumber = Number(matchedStrList[0]);
                          console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                        }
                      }
                    }
                    if (scoreData.indexOf('TOEFL') > -1) {
                      console.log("Inside IBT");
                      let tempScore = [
                        {
                          "elements":
                            [
                              {
                                "selectors":
                                  [
                                    [scoreData]
                                  ]
                              }
                            ]
                        }
                      ]
                      const iBtScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempScore);
                      if (iBtScore && iBtScore.length > 0) {
                        iBtfinaloutput = iBtScore.toString();
                        // extract exact number from string
                        const regEx = /[+-]?\d+(\.\d+)?/g;
                        const matchedStrList = String(iBtScore).match(regEx);
                        console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                        if (matchedStrList && matchedStrList.length > 0) {
                          iBtNumber = Number(matchedStrList[0]);
                          console.log(funcName + 'iBtNumber = ' + iBtNumber);
                        }
                      }
                    }

                    if (scoreData.indexOf('PTE') > -1) {
                      console.log("Inside PTE");
                      let tempScore = [
                        {
                          "elements":
                            [
                              {
                                "selectors":
                                  [
                                    [scoreData]
                                  ]
                              }
                            ]
                        }
                      ]
                      const pteAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempScore);
                      if (pteAScore && pteAScore.length > 0) {
                        ptefinaloutput = pteAScore.toString();
                        // extract exact number from string
                        const regEx = /[+-]?\d+(\.\d+)?/g;
                        const matchedStrList = String(pteAScore).match(regEx);
                        console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                        if (matchedStrList && matchedStrList.length > 0) {
                          pteNumber = Number(matchedStrList[0]);
                          console.log(funcName + 'pteNumber = ' + pteNumber);
                        }
                      }
                    }
                  }
                }
              } else {
                let ieltsString = "Overall 5.5 No individual band below 5.0";
                let ibtString = "65 (no band less than 15)";
                let pteString = "42 (no communicative skill less than 36)";

                let tempScore1 = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [ieltsString]
                            ]
                        }
                      ]
                  }
                ]
                const ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempScore1);
                ieltsfinaloutput = ieltsScore.toString();
                if (ieltsScore && ieltsScore.length > 0) {
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(ieltsScore).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    ieltsNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                  }
                }

                console.log("Inside IBT");
                let tempScore2 = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [ibtString]
                            ]
                        }
                      ]
                  }
                ]
                const iBtScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempScore2);
                if (iBtScore && iBtScore.length > 0) {
                  iBtfinaloutput = iBtScore.toString();
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(iBtScore).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    iBtNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'iBtNumber = ' + iBtNumber);
                  }
                }

                console.log("Inside PTE");
                let tempScore3 = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [pteString]
                            ]
                        }
                      ]
                  }
                ]
                const pteAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempScore3);
                if (pteAScore && pteAScore.length > 0) {
                  ptefinaloutput = pteAScore.toString();
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(pteAScore).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    pteNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'pteNumber = ' + pteNumber);
                  }
                }
              }

              // if ieltsNumber is valid then map IBT and PBT accordingly
              if (ieltsNumber && ieltsNumber > 0) {

                if (ieltsNumber) {
                  ieltsDict.name = 'ielts academic';
                  ieltsDict.description = ieltsfinaloutput;
                  englishList.push(ieltsDict);
                }
                else {
                  ibtDict.name = 'ielts academic';
                  ibtDict.description = configs.propValueNotAvaialble;
                  englishList.push(ibtDict);
                }

                if (iBtNumber) {
                  ibtDict.name = 'toefl ibt';
                  ibtDict.description = iBtfinaloutput;
                  englishList.push(ibtDict);
                }
                else {
                  ibtDict.name = 'toefl ibt';
                  ibtDict.description = configs.propValueNotAvaialble;
                  englishList.push(ibtDict);
                }

                if (pteNumber) {
                  pteDict.name = 'pte academic';
                  pteDict.description = ptefinaloutput;
                  englishList.push(pteDict);
                }
                else {
                  pteDict.name = 'pte academic';
                  pteDict.description = configs.propValueNotAvaialble;
                  englishList.push(pteDict);
                }
              }

              ///progress bar start
              const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};



              if (ieltsNumber && ieltsNumber > 0) {
                if (ieltsNumber) {
                  pieltsDict.name = 'ielts academic';
                  pieltsDict.description = ieltsDict.description;
                  pieltsDict.min = 0;
                  pieltsDict.require = ieltsNumber;
                  pieltsDict.max = 9;
                  pieltsDict.R = 0;
                  pieltsDict.W = 0;
                  pieltsDict.S = 0;
                  pieltsDict.L = 0;
                  pieltsDict.O = 0;
                  penglishList.push(pieltsDict);
                }
              }
              if (iBtNumber) {
                pibtDict.name = 'toefl ibt';
                pibtDict.description = ibtDict.description;
                pibtDict.min = 0;
                pibtDict.require = iBtNumber;
                pibtDict.max = 120;
                pibtDict.R = 0;
                pibtDict.W = 0;
                pibtDict.S = 0;
                pibtDict.L = 0;
                pibtDict.O = 0;
                penglishList.push(pibtDict);
              }

              if (pteNumber) {
                ppbtDict.name = 'pte';
                ppbtDict.description = pteDict.description;
                ppbtDict.min = 0;
                ppbtDict.require = pteNumber;
                ppbtDict.max = 90;
                ppbtDict.R = 0;
                ppbtDict.W = 0;
                ppbtDict.S = 0;
                ppbtDict.L = 0;
                ppbtDict.O = 0;
                penglishList.push(ppbtDict);
              }

              if (penglishList && penglishList.length > 0) {
                courseAdminReq.english = penglishList;
                // resJsonData.course_admission_requirement.english = courseAdminReq.english;
              } else {
                throw new Error("english not found");
              }

              if (courseScrappedData.course_academic_requirement) {
                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);

                console.log('\n\r');
                console.log(funcName + 'academicReq = ' + academicReq);
                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                console.log('\n\r');
                if (academicReq && String(academicReq).length > 0) {
                  // courseAdminReq.academic = await utils.giveMeArray(academicReq.replace(/[\r\n\t]+/g, ';'), ';');
                  courseAdminReq.academic = [academicReq];
                }
                else {
                  courseAdminReq.academic = [];
                }
              }
              // if courseAdminReq has any valid value then only assign it
              if (courseAdminReq.english || courseAdminReq.academic) {
                resJsonData.course_admission_requirement = courseAdminReq;
              }

              //resJsonData.course_admission_requirement = courseAdminReq;
              let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
              let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
              let academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
              console.log("english_requirements_url -->", academic_requirements_url);
              resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
              resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
              resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;

              break;
            }

            case 'course_url': {
              if (courseUrl && courseUrl.length > 0) {
                resJsonData.course_url = courseUrl;
              }
              break;
            }
            case 'course_duration_full_time': {
              const courseDurationList = [];
              const courseDurationDisplayList = [];
              const durationFullTime = {};
              // let anotherArray = [];
              let duration1 = courseScrappedData.course_duration_full_time;

              console.log("*************start formating Full Time years*************************");
              console.log("courseScrappedData.course_duration_full_time = ", JSON.stringify(courseScrappedData.course_duration_full_time));
              let finalDuration = duration1[0].elements[0].selectors[0][0];
              finalDuration = finalDuration.replace(/(\/)/gm, "");
              const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration);
              console.log("full_year_formated_data", full_year_formated_data);
              ///course duration
              const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
              console.log("not_full_year_formated_data", not_full_year_formated_data);
              try {
                durationFullTime.duration_full_time = not_full_year_formated_data;
                courseDurationList.push(durationFullTime);
                ///END course duration
                console.log("courseDurationList : ", courseDurationList);
                ///course duration display
                courseDurationDisplayList.push(full_year_formated_data);
                // anotherArray.push(courseDurationDisplayList);
                console.log("courseDurationDisplayList", courseDurationDisplayList);
                let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                console.log("FilterData---->" + JSON.stringify(filtered_duration_formated))
                if (courseDurationList && courseDurationList.length > 0) {
                  resJsonData.course_duration = durationFullTime.duration_full_time;
                }
                if (filtered_duration_formated && filtered_duration_formated.length > 0) {

                  resJsonData.course_duration_display = filtered_duration_formated;

                  var isfulltime = false, isparttime = false;
                  filtered_duration_formated.forEach(element => {
                    if (element.display == "Full-Time") {
                      isfulltime = true;
                    }
                    if (element.display == "Part-Time") {
                      isparttime = true;
                    }
                  });
                  resJsonData.isfulltime = isfulltime;
                  resJsonData.isparttime = isparttime;
                }

                if (courseDurationList && courseDurationList.length > 0) {

                  resJsonData.course_duration = durationFullTime.duration_full_time;
                }
                if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                  resJsonData.course_duration_display = filtered_duration_formated;
                }

                ///END course duration display
              } catch (err) {
                console.log("Problem in Full Time years:", err);
              }
              console.log('***************END formating Full Time years**************************')

              if (courseDurationList && courseDurationList.length > 0) {
                resJsonData.course_duration = durationFullTime.duration_full_time;
              }


              break;
            }
            case 'course_tuition_fee':
            case 'course_study_mode':
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_duration_part_time': {
              console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
              // No need to process anything, its already processed in other case.
              break;
            }
            case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};

              const feesList = [];
              const feesDict = {
                international_student: {}
              };
              let feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);

              const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
              if (feeYear && feeYear.length > 0) {
                courseTuitionFee.year = feeYear;
              }
              // if we can extract value as Int successfully then replace Or keep as it is
              if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                let splitFees = courseScrappedData.course_tuition_fees_international_student[0].elements[0].selectors[0][0];
                if (splitFees) {
                  if (splitFees.indexOf('One unit (12.5 credit points)') > -1) {
                    let splitSpecificText = splitFees.split('One unit (12.5 credit points)');
                    console.log("splitSpecificText -->", splitSpecificText);
                    splitFees = splitSpecificText[1];
                  }
                  else {
                    splitFees;
                  }

                  splitFees = splitFees.split('(');
                  console.log("splitFees -->", splitFees);
                  // let tempFees = [
                  //   {
                  //     "elements":
                  //       [
                  //         {
                  //           "selectors":
                  //             [
                  //               [splitFees[0]]
                  //             ]
                  //         }
                  //       ]
                  //   }
                  // ]
                  // feesIntStudent = await Course.extractValueFromScrappedElement(tempFees);
                }
                // else {
                //   feesIntStudent = 0;
                // }
                // const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                // if (feeYear && feeYear.length > 0) {
                //   courseTuitionFee.year = feeYear;
                // }
                // if we can extract value as Int successfully then replace Or keep as it is
                // if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits

                const feesVal1 = String(splitFees).replace('$', '');
                console.log("feesVal1----->", feesVal1);
                let arrval = [];
                if (feesVal1.includes('(')) {
                  arrval = String(feesVal1).split('(');
                }
                else if (feesVal1.includes(',')) {
                  arrval = String(feesVal1).split(',');
                }


                const feesVal = String(arrval[0]);
                console.log(funcName + 'feesVal = ' + feesVal);
                if (feesVal) {
                  const regEx = /\d/g;
                  let feesValNum = feesVal.match(regEx);
                  if (feesValNum) {
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    feesValNum = feesValNum.join('');
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    let feesNumber = null;
                    if (feesValNum.includes(',')) {
                      feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                    } else {
                      feesNumber = feesValNum;
                    }
                    console.log(funcName + 'feesNumber = ' + feesNumber);
                    if (Number(feesNumber)) {
                      feesDict.international_student = ({
                        amount: Number(feesNumber),
                        duration: 1,
                        unit: "Year",

                        description: feesWithDollorTrimmed

                      });
                    }
                  }
                }
              }
              //  } 
              else {
                feesDict.international_student = ({
                  amount: 0,
                  duration: 1,
                  unit: "Year",

                  description: "not available fee"

                });


              } // if (feesIntStudent && feesIntStudent.length > 0)

              // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
              const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
              console.log(funcName + 'cricoseStr = ' + cricoseStr);
              console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
              if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                if (fixedFeesDict) {
                  for (const codeKey in fixedFeesDict) {
                    console.log(funcName + 'codeKey = ' + codeKey);
                    if (fixedFeesDict.hasOwnProperty(codeKey)) {
                      const keyVal = fixedFeesDict[codeKey];
                      console.log(funcName + 'keyVal = ' + keyVal);
                      if (cricoseStr.includes(codeKey)) {
                        const feesDictVal = fixedFeesDict[cricoseStr];
                        console.log(funcName + 'feesDictVal = ' + feesDictVal);
                        if (feesDictVal && feesDictVal.length > 0) {
                          const inetStudentFees = Number(feesDictVal);
                          console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                          if (Number(inetStudentFees)) {
                            feesDict.international_student = Number(inetStudentFees);
                          }
                        }
                      }
                    }
                  } // for
                } // if
              } // if

              let international_student_all_fees_array = [];
              if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                  //  feesDict.international_student_all_fees = international_student_all_fees_array
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }
                if (feesDict) {
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                if (courseTuitionFee) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }

                // take tuition fee value at json top level so will help in DDB for indexing
                if (courseTuitionFee && feesDict.international_student) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                  // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                }

              }
              console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
              // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
              if (!feesDict.international_student) {
                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                return null; // this will add this item into FailedItemList and writes file to local disk
              }

              break;
            }


            case 'course_campus_location': { // Location Launceston
              console.log('course_campus_location matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedSelStr = [];
              let concatnatedSelectorsStr = null;
              let cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);

              for (const rootEleDict of rootElementDictList) {
                console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                // let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  for (const selList of selectorsList) {
                    if (selList && selList.length > 0) {
                      console.log('course_campus_location selList = ' + JSON.stringify(selList));
                      for (var selItem of selList) {
                        selItem = selItem.replace(/[\r\n\t ]+/g, ' ').split(",");
                        for (let sel of selItem) {
                          if (sel != "external" && sel != "External" && sel != "Online" && sel != "online") {
                            concatnatedSelStr.push(sel.trim());
                          }
                        }
                      } // selList
                      console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);
                    }
                  } // selectorsList                  
                } // elementsList                
              } // rootElementDictList 

              // add only if it has valid value              

              var finallocation = [];
              var local
              var loca = String(concatnatedSelStr).split(',');
              loca.forEach(element => {
                if (element.includes('Campus')) {
                  finallocation.push(element.replace('Campus', ''))
                }
                else {

                  finallocation.push(element)
                  console.log("finallocation------->", finallocation)

                }
              })
              var campusss = [];
              if (finallocation.length > 0) {
                console.log("loca------>", finallocation);
                let campuses = ['Hawthorn', 'Sarawak', 'Wantirna', 'Croydon', '']
                console.log("@@campuses------->", campuses);

                for (let i = 0; i < finallocation.length; i++) {
                  campuses.forEach(element => {
                    console.log(element.includes(finallocation[i].trim()))
                    if (element.includes(finallocation[i].trim())) {

                      campusss.push(element)

                    }
                  })
                }
                console.log("@@campuses------->", campusss)


                if (concatnatedSelStr && concatnatedSelStr.length > 0) {
                  var campusedata = [];
                  concatnatedSelStr.forEach(element => {
                    campusedata.push({
                      "name": element,
                      "code": cricos
                    })
                  });
                  resJsonData.course_campus_location = campusedata;
                  console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

                }
                else {
                  throw new Error("No campus location found.");
                }

                resJsonData.course_study_mode = "On campus"
              }
              break;
            }

            case 'course_intake_url': {
              var intakeUrl = [];
              intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
              resJsonData.course_intake_url = intakeUrl;
              break;
            }

            case 'course_outline': {
              console.log("course_outline--->")

              const outline = {
                majors: [],
                minors: [],
                more_details: ""
              };
              var major, minor
              var intakedata1 = {};
              const courseKeyVal = courseScrappedData.course_outline_major;
              let resScholarshipJson = null;


              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList;
                      }
                    }
                  }
                }
              }
              if (resScholarshipJson) {
                major = resScholarshipJson
              } else {
                major = []
              }




              const courseKeyVal1 = courseScrappedData.course_outline_minors;
              let resScholarshipJson1 = null;
              if (Array.isArray(courseKeyVal1)) {
                for (const rootEleDict of courseKeyVal1) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson1 = selList;

                      }
                    }
                  }
                }
              }
              if (resScholarshipJson1) {
                minor = resScholarshipJson1
              } else {
                minor = []
              }

              let m_d = resJsonData.course_url;
              console.log("md--------->", m_d)

              var intakedata1 = {};
              intakedata1.majors = major;
              intakedata1.minors = minor;
              intakedata1.more_details = m_d;
              resJsonData.course_outline = intakedata1;


              break;
            }

       
            case 'application_fee':
              {
                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                resJsonData.application_fee = ""
                break;
              }
            case 'course_intake':
              {
                var splitIntake = [], splitNew = [];
                let intakeFlag = false;
                var finalIntake = [];
                var finalintake = []
                var courseIntakeStr = [];//= awat Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                const courseIntakeDList = await utils.getValueFromHardCodedJsonFile('course_intake');
                console.log("resJsonData.course_title.toLowerCase() -->", resJsonData.course_title.toLowerCase());
                var courseIntakeStr1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);

                console.log("courseIntakeStr1 -->", courseIntakeStr1);
                if (resJsonData.course_title.toLowerCase().indexOf('doctor of philosophy') > -1 || resJsonData.course_title.toLowerCase().indexOf('master of design (research)') > -1 || resJsonData.course_title.toLowerCase().indexOf('master of arts (research)') > -1) {
                  for (let i = 0; i < resJsonData.course_campus_location.length; i++) {
                    courseIntakeStr.push({ "name": resJsonData.course_campus_location[i].name, "value": ["Anytime"] });
                  }


                } else {

                  if (courseIntakeStr1 == '' || courseIntakeStr1 == null || courseIntakeStr1 == undefined) {
                    for (let i = 0; i < resJsonData.course_campus_location.length; i++) {
                      let dateArray = [];

                      courseIntakeStr.push({ "name": resJsonData.course_campus_location[i].name, "value": dateArray });
                    }

                  }


                  else {
                    console.log("courseIntakeStr -->", courseIntakeStr1);

                    if (courseIntakeStr1.indexOf('(') > -1) {
                      if (courseIntakeStr1.indexOf('-') > -1) {
                        splitIntake = courseIntakeStr1.split(' - ');
                      } else {
                        splitIntake = courseIntakeStr1;
                      }
                      console.log("splitIntake -->", splitIntake);
                      let splitNewLine = [];
                      if (splitIntake[0].indexOf('\n') > -1) {
                        splitNewLine = splitIntake[0].split('\n');
                      }
                      else if (courseIntakeStr1.indexOf('-') != -1) {
                        splitNewLine.push(splitIntake[0]);
                      }

                      else {
                        splitNewLine.push(splitIntake);
                      }
                      console.log("splitNewLine -->", splitNewLine);
                      for (let intake_split of splitNewLine) {
                        let splitIntake1 = [];
                        if (intake_split.indexOf('(') > -1) {
                          splitIntake1 = intake_split.split('(');
                          console.log("SplitINtake1", splitIntake1)
                          finalIntake.push(splitIntake1[1].split(','));
                          finalIntake.forEach(element => {
                            element.forEach(value => {
                              if (value.includes(") -")) {
                                var data = value.replace(") -", " ")
                                finalintake.push(data.trim())
                              } else {
                                finalintake.push(value)
                              }
                            })
                            //console.log("Element-->",element)

                          })

                        }
                      }

                      console.log("finalIntake -->", finalintake);
                      console.log("courseIntakeDList -- > ", courseIntakeDList);
                      console.log("resJsonData.course_campus_location -->", resJsonData.course_campus_location);
                      for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                        let fdateArray = [];
                        for (let i = 0; i < finalintake.length; i++) {
                          console.log("finalintake@@@", finalintake)
                          let dateArray = [];
                          for (let intake1 of courseIntakeDList) {
                            console.log("finalIntake[i] -->", finalintake);
                            for (let fIntake of finalintake) {
                              fIntake = fIntake.replace(/(\))/gm, "");
                              if (intake1.indexOf(' - ') > -1) {
                                let date = intake1.split(' - ');
                                if (date[0].toLowerCase().indexOf(fIntake.toLowerCase().trim()) > -1) {
                                  console.log("Matched for intake1 --> ", intake1);
                                  dateArray.push(date[1]);
                                }
                              } else {
                                if (intake1.toLowerCase().indexOf(fIntake.toLowerCase().trim()) > -1) {
                                  console.log("Matched for intake1 --> ", intake1);
                                  dateArray.push(intake1);
                                }
                              }
                            }
                          }
                          fdateArray.push(dateArray);
                        }
                        console.log("DateArray -->", fdateArray);
                        courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": fdateArray[j] });
                      }
                    } else {
                      let arr = []
                      if (courseIntakeStr1.indexOf('-') > -1) {
                        let repStri, repStri1;
                        for (let i = 0; i < resJsonData.course_campus_location.length; i++) {
                          splitIntake = courseIntakeStr1.split('-');

                          console.log("SplitData@@@", splitIntake)

                          splitIntake.forEach(element => {
                            if (!element == '') {
                              if (element.includes(",")) {
                                var data = element.replace(",", ' ')
                                arr.push(data.trim())
                              } else {
                                arr.push(element.trim())
                              }

                            }

                          });


                          for (let i = 0; i < resJsonData.course_campus_location.length; i++) {
                            courseIntakeStr.push({ "name": resJsonData.course_campus_location[i].name, "value": arr });
                            console.log("courseIntakeStr@@@@", courseIntakeStr);
                          }
                        }
                      }

                      else if (courseIntakeStr1.indexOf(',') > -1) {
                        let splitIntake = courseIntakeStr1.split(',');

                        for (let i = 0; i < resJsonData.course_campus_location.length; i++) {
                          let dateArray = [];
                          for (let intake of splitIntake) {
                            dateArray.push(intake + ", 2019");
                          }
                          courseIntakeStr.push({ "name": resJsonData.course_campus_location[i].name, "value": dateArray });
                        }
                      }

                    }
                  }

                }
                console.log("resJsonData.course_intake_url --> ", resJsonData.course_intake_url);
                console.log("intakes" + JSON.stringify(courseIntakeStr));
                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                let formatedIntake = await format_functions.providemyintake(courseIntakeStr, "MOM", "");
                console.log("FormatIntake", JSON.stringify(formatedIntake))
                // formatedIntake.forEach(element => {
                //   element.value.forEach(elementdate => {
                //     if (elementdate.actualdate == '') {
                //       elementdate.filterdate = ""
                //     } else if (elementdate.actualdate == '') {
                //       elementdate.filterdate = ""
                //     }
                //     //elementdate.push({filterdate:myintake_data["term 1_date"]})
                //     console.log("DATA--->", elementdate)

                //   })
                // });

                // console.log(JSON.stringify(providemyintake(intake, "MOM", "")));
                console.log("Intakes --> ", JSON.stringify(formatedIntake));
                var intakedata = {};
                intakedata.intake = formatedIntake;
                intakedata.more_details = more_details;
                resJsonData.course_intake = intakedata;

                break;
              }
            case 'course_current_intake': {
              resJsonData.course_current_intake = await Course.extractValueFromScrappedElement(courseScrappedData.course_current_intake);
              break;
            }

            case 'course_scholarship': {
              const courseKeyVal = courseScrappedData.course_scholarship;
              let resScholarshipJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resScholarshipJson) {
                resJsonData.course_scholarship = resScholarshipJson;
              }
              break;
            }
            case 'univ_name': {
              const courseKeyVal = courseScrappedData.univ_name;
              let resUnivName = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivName = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivName) {
                resJsonData.univ_name = resUnivName;
              }
              break;
            }
            case 'univ_logo': {
              const courseKeyVal = courseScrappedData.univ_logo;
              let resUnivLogo = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivLogo = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivLogo) {
                resJsonData.univ_logo = resUnivLogo;
              }
              break;
            }
            case 'course_accomodation_cost': {
              const courseKeyVal = courseScrappedData.course_accomodation_cost;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_accomodation_cost = resAccomodationCostJson;
              }
              break;
            }



            case 'course_study_level': {
              // resJsonData.course_study_level = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
              const title = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level)
              var ctitle = format_functions.titleCase(title)
              console.log("ctitle@@@", ctitle)
              resJsonData.course_study_level = ctitle
              break;
            }
            case 'course_country': {
              const courseKeyVal = courseScrappedData.course_country;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_country = resAccomodationCostJson;
              }
              break;
            }
            case 'course_overview': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
              }
              break;
            }
            case 'course_career_outcome': {

              console.log(funcName + 'matched case course_career_outcome: ' + key);
              const courseKeyVal = courseScrappedData.course_career_outcome;
              var course_career_outcome = []
              console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        course_career_outcome = selList;
                      }
                    }
                  }
                }
              } // rootElementDictList
              // add only if it has valid value
              if (course_career_outcome.length > 0) {
                resJsonData.course_career_outcome = course_career_outcome;
              } else {
                resJsonData.course_career_outcome = []
              }
              break;
            }
            case 'course_title': {
              const title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
              var ctitle = format_functions.titleCase(title)
              console.log("ctitle@@@", ctitle)
              resJsonData.course_title = ctitle
              break;
            }
            // case 'course_title': {
            //   console.log("Course_Title -->", course_name);
            //   resJsonData[key] = course_name;
            //   break;
            // }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      var NEWJSONSTRUCT = {}, structDict = [];
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
        //   NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id + "_" + location.replace(/\s/g, '').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
        console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        }
        else {
          NEWJSONSTRUCT.course_intake = [];
        }
        for (let myfees of resJsonData.course_tuition_fee.fees) {
          if (myfees.name == location) {
            NEWJSONSTRUCT.international_student_all_fees = myfees;
          }
        }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        resJsonData.course_id = location_wise_data.course_location_id;

        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

      }
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = 'scrapeAndFormatCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);

      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }

      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file

      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }

      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
