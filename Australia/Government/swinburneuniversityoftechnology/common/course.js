const fs = require('fs');
const path = require('path');
const Scrape = require('./../common/scrape').Scrape;
const appConfigs = require('./../common/app-config');
// eslint-disable-next-line import/no-unresolved
const configs = require('./../configs');

class Course extends Scrape {
  // remove unwanted keys
  static async removeKeys(scrappedDataJson) {
    const funcName = 'removeKeys ';
    try {
      const resJson = scrappedDataJson;
      Scrape.validateParams([scrappedDataJson]);
      for (const key of Course.KEYS_TO_REMOVE) {
        console.log(funcName + 'removing key = ' + key);
        delete resJson[key];
      }
      return resJson;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // get values from hard-coded json file
  static async getValueFromHardCodedJsonFile(jsonKeyName) {
    const funcName = 'getValueFromHardCodedJsonFile ';
    try {
      // validateParams([jsonKeyName]);
      const fileData = fs.readFileSync(appConfigs.hardCodedJsonDataFilepath);
      if (!fileData) {
        throw (new Error('Invalid file data, fileData = ' + fileData));
      }
      const dataJSon = JSON.parse(fileData)[0];
      if (!dataJSon) {
        throw (new Error('Invalid json data, dataJSon = ' + JSON.stringify(dataJSon)));
      }
      const keyValData = dataJSon[jsonKeyName];
      return keyValData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  static async removeAllWhiteSpcaesAndLowerCase(val) {
    const funcName = 'removeAllWhiteSpcaesAndLowerCase ';
    try {
      if (!(val && val.length > 0)) {
        console.log(funcName + 'Invalid prama, val = ' + val);
        throw (new Error('Invalid prama, val = ' + val));
      }
      // console.log(funcName + 'val  before operation = ' + val);
      const trimmedVal = String(val).trim();
      // console.log(funcName + 'trimmedVal = ' + trimmedVal);
      const regex = /\s/g;
      const wsRemovedVal = String(trimmedVal).replace(regex, '');
      let ucVal = String(wsRemovedVal).toLowerCase();
      // remove all / slassh from file name else it wiil be considered as dir seperator
      while (String(ucVal).includes('/')) {
        ucVal = String(ucVal).replace('/', '');
        console.log(funcName + 'ucVal = ' + ucVal);
      }
      // console.log(funcName + 'val  after operation = ' + ucVal);
      return ucVal;
    } catch (error) {
      console.log(funcName + 'try-catch error = ', error);
      throw (error);
    }
  }

  // generate opCourseFilePath
  static async generateOutputCourseFilePath(courseDict) {
    const funcName = 'generateOutputCourseFilePath ';
    try {
      let opCourseFilePath = null;
      if (!courseDict) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }
      // create courseId from course title
      const courseId = await Course.removeAllWhiteSpcaesAndLowerCase(courseDict.innerText);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // eslint-disable-next-line no-path-concat
      console.log(funcName + '__dirname = ' + __dirname);
      console.log(funcName + 'path.dirname = ' + path.dirname(__dirname));
      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        // opCourseFilePath = configs.outputDirPath + configs.univ_id + '_' + courseId + '_update_coursedetails.json';
        opCourseFilePath = path.join(path.dirname(__dirname), configs.outputDirPath + configs.univ_id + '_' + courseId + '_update_coursedetails.json');
      } else {
        // opCourseFilePath = configs.outputDirPath + configs.univ_id + '_' + courseId + '_coursedetails.json';
        opCourseFilePath = path.join(path.dirname(__dirname), configs.outputDirPath + configs.univ_id + '_' + courseId + '_coursedetails.json');
      }
      // console.log(funcName + 'opCourseFilePath = ' + opCourseFilePath);
      // const normalizedFilePath = path.normalize(opCourseFilePath);
      // console.log(funcName + 'normalizedFilePath = ' + normalizedFilePath);

      // const fileName = path.basename(opCourseFilePath);
      // console.log(funcName + 'fileName = ' + fileName);
      // const normFilePath = path.normalize(fileName);
      // console.log(funcName + 'normFilePath = ' + normFilePath);

      // const escapedFilePath = escape(fileName);
      // console.log(funcName + 'escapedFilePath = ' + escapedFilePath);

      return opCourseFilePath;
    } catch (error) {
      console.log(funcName + 'try-catch error = ', error);
      throw (error);
    }
  }

  static async scrapeCourseDetails(courseDict) {
    const funcName = 'scrapeCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);

      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }
      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await Course.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await Course.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file
      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }
      
      if (s) {
        await s.close();
      }
      return courseScrappedData;
      // return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      return null;
      // throw (error);
    }
  } // scrapeCourseDetails
}
  
Course.KEYS_TO_REMOVE = ['select_study_year_and_citizenship_type', 'select_english_as_ibt', 'select_english_as_ielts', 'select_english_as_pbt', 
  'course_academic_requirement', 'course_duration_full_time', 'course_duration_part_time', 'course_tuition_fees_year', 'course_tuition_fees_international_student',
  'course_tuition_fee_duration_years', 'course_tuition_fees_currency', 'select_course_tuition_fees_international_student', 'course_toefl_ibt_indicator',
  'course_toefl_ielts_score', 'course_toefl_toefl_pbt_score', 'page_url'];

Course.COURSE_DURATION_UNIT_TYPES_LIST = ['year', 'years', 'month', 'months', 'weeks', 'week'];
Course.COURSE_MONTHS_LIST = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
Course.STUDY_LEVEL_LIST = ['Associate Degree', 'Bachelor', 'Diploma', 'Doctor', 'Graduate', 'Master', 'Undergraduate', 'Career Start'];

module.exports = { Course };
