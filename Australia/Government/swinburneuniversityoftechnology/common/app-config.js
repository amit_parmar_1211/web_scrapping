module.exports = {
  //puptrExecutablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
  puptrDefaultViewportSettings: { width: 1600, height: 900 },
  awsUniversitiesDataBucketName: 'sp.univ.data',
  univNameSeperater: '_',
  createCourseAPIPath: '/Prod/courses',
  createUniversityAPIPath: '/Prod/universities',
  s3KeyIsCourseDetailsFile: 'coursedetails',
  s3KeyIsUnivOverviewFile: 'overview',
  s3KeyFailedItemsListFilePath: '/failed_items/failed_items_list.json',
  s3KeySuccessItemsListFilePath: '/failed_items/success_items_list.json',
  apiOperationCreateCourses: 'apiOperationCreateCourses',
  apiOperationCreateUniversities: 'apiOperationCreateUniversities',
  apiEndpoint: '11nnu2hwsd.execute-api.us-east-1.amazonaws.com',
  // apiEndpoint: '127.0.0.1:3000',
  failedItemsFolderName: 'failed_items',
  failedItemsFileNamePrefixCourseDetails: 'course_details',
  failedItemsFileNamePrefixUnivOverview: 'university_overview',
  failedItemsFileNameError: 'error',

  isPuptrBrowserModeHeadless: true,
  shouldTakeCourseListFromOutputFolder: false,
  shouldTakeFromOutputFolder: false,

  selScrapePageListRootKey: 'page_list',
  selCourseDetailsSelectorsJsonFilepath: './selectors/university_course_details_selectors.json',
  selUpdateCourseDetailsSelectorsJsonFilepath: './selectors/university_update_course_details_selectors.json',
  selUniversityOverviewSelectorsJsonFilepath: './selectors/university_overview_selectors.json',
  selUniversityCourseListSelectorsJsonFilepath: './selectors/university_course_list_selectors.json',
  hardCodedJsonDataFilepath: './selectors/university_hard_coded_data.json',

  AWS_SAM_LOCAL: false,
  AWS_PERFORM_UPDATEITEM_OPERATION: false,
    
  ISREPLACE: false,
  ISUPDATE: false,
  ISINSERT: false,
  SHOULD_TAKE_FROM_FOLDER: false,
  READ_FOLDER_PATH: "put_in_sql",
  // API_URL:"http://apisearchmycourse.konze.com/"
  API_URL: "http://192.168.4.253:1757/"
};
