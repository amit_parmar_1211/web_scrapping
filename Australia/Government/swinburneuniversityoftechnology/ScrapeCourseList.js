const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
      var mainCategory = [], redirecturl = [];
      var datalist = [];
      //scrape main category
      const maincategoryselector = "//*[@id='refine-search']/div[2]/div/ul/li/a";
      var category = await page.$x(maincategoryselector);
      console.log("Total categories-->" + category.length);
      var studyareaopen = "//*[@id='refine-search']/div[2]/h3/a";
      var open = await page.$x(studyareaopen);
      await open[0].click();
  
      for (let link of category) {
        var categorystringmain = await page.evaluate(el => el.innerText, link);
        var categorystringmainurl = await page.evaluate(el => el.href, link);
        var mcat = categorystringmain.replace(/[\r\n\t]+/g, ' ').trim().split("(");
        mainCategory.push(mcat[0].trim());
        redirecturl.push({ innerText: mcat[0].trim(), href: categorystringmainurl });
      }
      for (let link of redirecturl) {
        console.log("start--> " + link.innerText);
        await page.goto(link.href + "&f.Residency%7C3=international", { timeout: 0 })
        var ispage = true;
        const title = "//*[@id='content']/main/section[1]/div/div[3]/div/a//strong";
        const href = "//*[@id='content']/main/section[1]/div/div[3]/div/a";
        const nextpage = "//*[@id='content']/main/section[1]/div/div[3]/footer/div[2]/a//span[contains(text(),'Next')]/../..";
        while (ispage) {
          var titlevalue = await page.$x(title);
          var linkvalue = await page.$x(href);
          for (let i = 0; i < titlevalue.length; i++) {
            var categorystring = await page.evaluate(el => el.innerText, titlevalue[i]);
            var categoryurl = await page.evaluate(el => el.href, linkvalue[i]);
            datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: link.innerText });
          }
  
          var next = await page.$x(nextpage);
          if (next[0]) {
            await next[0].click();
            await page.waitFor(5000);
            console.log("Clicked next " + datalist.length);
          }
          else {
            console.log("No more pages" + datalist.length);
            ispage = false;
          }
        }
  
        const clearselection = "//*[@id='content']/main/section[1]/div/div[3]/div[2]/div/a[contains(text(),'Clear All')]";
        var clear = await page.$x(clearselection);
        await clear[0].click();
      }
  
      await fs.writeFileSync("./output//swinburneuniversityoftechnology_original_courselist.json", JSON.stringify(datalist))
      await fs.writeFileSync("/maincategorylist.json", JSON.stringify(mainCategory))

      // await fs.writeFileSync("./output/swinburneuniversityoftechnology_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < datalist.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (datalist[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [] });
        }
      }

      await fs.writeFileSync("./output//swinburneuniversityoftechnology_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      // for (let i = 0; i < datalist.length; i++) {
      //   for (let j = 0; j < uniqueUrl.length; j++) {
      //     if (uniqueUrl[j].href == datalist[i].href) {
      //       uniqueUrl[j].category.push(datalist[i].category);
      //     }
      //   }
      // }
      //based on unique urls mapping of categories
      for (let i = 0; i < datalist.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == datalist[i].href) {
            if (uniqueUrl[j].category.includes(datalist[i].category)) {
              

            } else {
              uniqueUrl[j].category.push(datalist[i].category);
            }

          }
        }
      }

      
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output//swinburneuniversityoftechnology_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
      return uniqueUrl;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }

  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      //Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
     var data= await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
    // await this.scrapeCourseListForPages(courseItemSelectorList, firstPageSelector, pageURL,path);
    //  await this.scrapeCourseListAndPutAtS3(selFilepath,path);
    //  await this.scrapeCourseList(selFilepath,path);

     if (s) {
        await s.close();
      }
      return data;

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  static async scrapeCourseListForPages(courseItemSelectorList, firstPageSelector, pageURL) {
    const funcName = 'scrapeCourseListForPages ';
    let s = null; let courseList = null;
    try {
      // validate params
      Scrape.validateParams([courseItemSelectorList, firstPageSelector, pageURL]);
      console.log(funcName + 'courseItemSelectorList = ' + JSON.stringify(courseItemSelectorList));
      // create Scrape instance
      s = new Scrape();
      await s.init({ headless: false });
      await s.setupNewBrowserPage(pageURL);
      let nextElementSiblingHandle = null;
      courseList = [];
      const activePaginationItemSel = firstPageSelector;
      let pageCount = 1;

      do {
        await s.page.waitFor(3000);
        // wait for sel
        console.log(funcName + 'waitForSelector sel = ' + activePaginationItemSel);
        await s.page.waitForSelector(activePaginationItemSel, { visible: true });
        // scrape all course list items
        console.log(funcName + '\n\r ********** Scraping for page  ' + pageCount + ' ********** \n\r');
        const resList = [];

        console.log(funcName + 'waitForSelector sel = ' + courseItemSelectorList[0]);
        await s.page.waitForSelector(courseItemSelectorList[0]);

        const anchorEleHandle = await s.page.$(courseItemSelectorList[0]); // anchor element handle
        console.log(funcName + 'anchorEleHandle = ' + anchorEleHandle);

        const hrefList = await s.scrapeAnchorElement(courseItemSelectorList[0], null, '');
        // console.log(funcName + 'hrefList = ' + JSON.stringify(hrefList));
        for (const itemDict of hrefList) {
          const courseDict = {};
          courseDict.href = itemDict.href;
          const itemInnerText = itemDict.innerText;
          const innerTextList = String(itemInnerText).split('\n');
          console.log(funcName + 'innerTextList = ' + JSON.stringify(innerTextList));
          if (innerTextList && innerTextList.length > 0) {
            courseDict.innerText = innerTextList[0];
          }
          console.log(funcName + 'courseDict = ' + JSON.stringify(courseDict));
          resList.push(courseDict);
        } // for

        courseList = courseList.concat(resList);
        // console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
        const eleList = await s.page.$$(activePaginationItemSel);
        if (Array.isArray(eleList) && eleList.length > 0) {
          const activePageButton = eleList[0];
          // reassign next sibling handle
          nextElementSiblingHandle = await activePageButton.getProperty('nextElementSibling');
          console.log(funcName + 'nextElementSiblingHandle = ' + nextElementSiblingHandle);
          if (nextElementSiblingHandle == "JSHandle@node") {
            const propHandle = nextElementSiblingHandle.getProperty('nodeName');
            console.log(funcName + 'propHandle = ' + propHandle);
            let nextSiblingNodeName = null;
            await propHandle.then((res) => {
              console.log('res = ' + res);
              const resJSon = res.jsonValue();
              console.log('resJSon = ' + resJSon);
              resJSon.then((resB) => {
                console.log('resB = ' + resB);
                nextSiblingNodeName = resB;
              });
            });
            console.log(funcName + 'nextSiblingNodeName = ' + nextSiblingNodeName);
            if (String(nextSiblingNodeName) !== 'LI') { // if next is not Anchor element
              console.log(funcName + 'nextElementSiblingHandle includes null....so breaking the loop...');
              break;
            }
            if (nextElementSiblingHandle) {
              await nextElementSiblingHandle.click('middle');
              console.log(funcName + 'Clicked nextElementSiblingHandle..');
            } else {
              throw (new Error('nextElementSiblingHandle invalid'));
            }
          }
          else { break; }
        }

        pageCount += 1;
        console.log(funcName + 'courseList count = ' + courseList.length);
      } while (nextElementSiblingHandle);

      console.log(funcName + 'total pages scrapped = ' + pageCount);
      console.log(funcName + 'courseList count = ' + courseList.length);
      // console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(courseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      if (s) {
        await s.close();
      }
    } catch (error) {
      console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r\n\r');
      console.log(funcName + 'try-catch error = ' + error);
      console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r\n\r');
      if (s) {
        await s.close();
      }
      console.log(funcName + 'writing courseList to tmp file....');
      await fs.writeFileSync(configs.opCourseListTempFilepath, JSON.stringify(courseList));
      console.log(funcName + 'writing courseList to tmp file completed successfully....');
      throw (error);
    }
  }

  // scrape course page list of the univ
  async scrapeCourseListAndPutAtS3(selFilepath) {
    const funcName = 'scrapeCourseListAndPutAtS3 ';
    try {
      // validate params
   //   Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      const pageUrl = this.selectorJson.url;
      console.log(funcName + 'url = ' + pageUrl);
      const jsonDictElements = this.selectorJson.elements;
      console.log(funcName + 'elements = ' + JSON.stringify(jsonDictElements));
      let courseList = null;
      if (Array.isArray(jsonDictElements)) {
        for (const eleDict of jsonDictElements) {
          const eleType = eleDict.elementType;
          console.log(funcName + 'elementType = ' + eleType);
          // ensure type is matching
          if (eleType === Scrape.ELEMENT_TYPE.COURSE_LIST) {
            const courseItemSelector = eleDict.course_item_selector;
            console.log(funcName + 'course_item_selector = ' + courseItemSelector);
            const pageSelector = eleDict.page_selector;
            console.log(funcName + 'page_selector = ' + pageSelector);
            courseList = await ScrapeCourseList.scrapeCourseListForPages(courseItemSelector, pageSelector, pageUrl);
          } // if
        } // for elements
      } // if


      
      // console.log(funcName + 'writing courseList to file....');
      // const selector = "#main > div.center-col.content > table > tbody > tr > th";
      // const innerText = "CRICOS Code:";
      // await utils.validateMyCourseList(this.s.page,configs.opCourseListFilepath,courseList,selector,innerText);
      // //await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(courseList));
      // console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      // const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      //console.log(funcName + 'object location = ' + res);



      return courseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  // refers to http://www.utas.edu.au/courses
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      //Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

// class ScrapeCourseList extends Scrape {
//   async scrapeOnlyInternationalCourseList_Mapping(page) {
//     const funcName = 'scrapeCourseCategoryList ';
//     try {
//       // category of course
//       var mainCategory = [], redirecturl = [];
//       var datalist = [];
//       //scrape main category
//       const maincategoryselector = "//*[@id='refine-search']/div[2]/div/ul/li/a";
//       var category = await page.$x(maincategoryselector);
//       console.log("Total categories-->" + category.length);
//       var studyareaopen = "//*[@id='refine-search']/div[2]/h3/a";
//       var open = await page.$x(studyareaopen);
//       await open[0].click();
  
//       for (let link of category) {
//         var categorystringmain = await page.evaluate(el => el.innerText, link);
//         var categorystringmainurl = await page.evaluate(el => el.href, link);
//         var mcat = categorystringmain.replace(/[\r\n\t]+/g, ' ').trim().split("(");
//         mainCategory.push(mcat[0].trim());
//         redirecturl.push({ innerText: mcat[0].trim(), href: categorystringmainurl });
//       }
//       for (let link of redirecturl) {
//         console.log("start--> " + link.innerText);
//         await page.goto(link.href + "&f.Residency%7C3=international", { timeout: 0 })
//         var ispage = true;
//         const href = "//*[@id='content']/main/section[1]/div/div[3]/div/a";
//         const title = "//*[@id='content']/main/section[1]/div/div[3]/div/a//strong";
//         const nextpage = "//*[@id='content']/main/section[1]/div/div[3]/footer/div[2]/a//span[contains(text(),'Next')]/../..";
//         while (ispage) {
//           var titlevalue = await page.$x(title);
//           var linkvalue = await page.$x(href);
//           for (let i = 0; i < titlevalue.length; i++) {
//             var categorystring = await page.evaluate(el => el.innerText, titlevalue[i]);
//             var categoryurl = await page.evaluate(el => el.href, linkvalue[i]);
//             datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: link.innerText });
//           }
  
//           var next = await page.$x(nextpage);
//           if (next[0]) {
//             await next[0].click();
//             await page.waitFor(5000);
//             console.log("Clicked next " + datalist.length);
//           }
//           else {
//             console.log("No more pages" + datalist.length);
//             ispage = false;
//           }
//         }
  
//         const clearselection = "//*[@id='content']/main/section[1]/div/div[3]/div[2]/div/a[contains(text(),'Clear All')]";
//         var clear = await page.$x(clearselection);
//         await clear[0].click();
//       }
  
//       await fs.writeFileSync("./output/swinburneuniversityoftechnology_original_courselist.json", JSON.stringify(datalist))
//       await fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))

//       // await fs.writeFileSync("./output/swinburneuniversityoftechnology_original_courselist.json", JSON.stringify(totalCourseList));
//       let uniqueUrl = [];
//       //unique url from the courselist file
//       for (let i = 0; i < datalist.length; i++) {
//         let cnt = 0;
//         if (uniqueUrl.length > 0) {
//           for (let j = 0; j < uniqueUrl.length; j++) {
//             if (datalist[i].href == uniqueUrl[j].href) {
//               cnt = 0;
//               break;
//             } else {
//               cnt++;
//             }
//           }
//           if (cnt > 0) {
//             uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [] });
//           }
//         } else {
//           uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [] });
//         }
//       }

//       await fs.writeFileSync("./output/swinburneuniversityoftechnology_unique_courselist.json", JSON.stringify(uniqueUrl));
//       //based on unique urls mapping of categories
//       // for (let i = 0; i < totalCourseList.length; i++) {
//       //   for (let j = 0; j < uniqueUrl.length; j++) {
//       //     if (uniqueUrl[j].href == totalCourseList[i].href) {
//       //       uniqueUrl[j].category.push(totalCourseList[i].category);
//       //     }
//       //   }
//       // }
//       //based on unique urls mapping of categories
//       for (let i = 0; i < datalist.length; i++) {
//         for (let j = 0; j < uniqueUrl.length; j++) {
//           if (uniqueUrl[j].href == datalist[i].href) {
//             if (uniqueUrl[j].category.includes(datalist[i].category)) {

//             } else {
//               uniqueUrl[j].category.push(datalist[i].category);
//             }

//           }
//         }
//       }
//       console.log("totalCourseList -->", uniqueUrl);
//       await fs.writeFileSync("./output/swinburneuniversityoftechnology_courselist.json", JSON.stringify(uniqueUrl));
//       console.log(funcName + 'writing courseList to file completed successfully....');
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//     }
//   }

//   // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  
//   async scrapeOnlyInternationalCourseList(selFilepath) {
//     const funcName = 'scrapeCoursePageList ';
//     let s = null;
//     try {
//       // validate params
//       Scrape.validateParams([selFilepath]);
//       // if should read from local file
//       if (appConfigs.shouldTakeCourseListFromOutputFolder) {
//         const fileData = fs.readFileSync(configs.opCourseListFilepath);
//         if (!fileData) {
//           throw (new Error('Invalif file data, fileData = ' + fileData));
//         }
//         const dataJson = JSON.parse(fileData);
//         console.log(funcName + 'Success in getting local data so returning local data....');
//         // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
//         return dataJson;
//       }
//       // load file
//       const fileData = fs.readFileSync(selFilepath);
//       this.selectorJson = JSON.parse(fileData);
//       // create Scrape object
//       s = new Scrape();
//       await s.init({ headless: true });
//       const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
//       console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
//       // set page to url
//       await s.setupNewBrowserPage(rootEleDictUrl);
//       await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
//       if (s) {
//         await s.close();
//       }

//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       // destroy scrape resources
//       if (s) {
//         await s.close();
//       }
//       throw (error);
//     }
//   }

//   // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
//   static async scrapeCourseListForPages(courseItemSelectorList, firstPageSelector, pageURL) {
//     const funcName = 'scrapeCourseListForPages ';
//     let s = null; let courseList = null;
//     try {
//       // validate params
//       Scrape.validateParams([courseItemSelectorList, firstPageSelector, pageURL]);
//       console.log(funcName + 'courseItemSelectorList = ' + JSON.stringify(courseItemSelectorList));
//       // create Scrape instance
//       s = new Scrape();
//       await s.init({ headless: false });
//       await s.setupNewBrowserPage(pageURL);
//       let nextElementSiblingHandle = null;
//       courseList = [];
//       const activePaginationItemSel = firstPageSelector;
//       let pageCount = 1;

//       do {
//         await s.page.waitFor(3000);
//         // wait for sel
//         console.log(funcName + 'waitForSelector sel = ' + activePaginationItemSel);
//         await s.page.waitForSelector(activePaginationItemSel, { visible: true });
//         // scrape all course list items
//         console.log(funcName + '\n\r ********** Scraping for page  ' + pageCount + ' ********** \n\r');
//         const resList = [];

//         console.log(funcName + 'waitForSelector sel = ' + courseItemSelectorList[0]);
//         await s.page.waitForSelector(courseItemSelectorList[0]);

//         const anchorEleHandle = await s.page.$(courseItemSelectorList[0]); // anchor element handle
//         console.log(funcName + 'anchorEleHandle = ' + anchorEleHandle);

//         const hrefList = await s.scrapeAnchorElement(courseItemSelectorList[0], null, '');
//         // console.log(funcName + 'hrefList = ' + JSON.stringify(hrefList));
//         for (const itemDict of hrefList) {
//           const courseDict = {};
//           courseDict.href = itemDict.href;
//           const itemInnerText = itemDict.innerText;
//           const innerTextList = String(itemInnerText).split('\n');
//           console.log(funcName + 'innerTextList = ' + JSON.stringify(innerTextList));
//           if (innerTextList && innerTextList.length > 0) {
//             courseDict.innerText = innerTextList[0];
//           }
//           console.log(funcName + 'courseDict = ' + JSON.stringify(courseDict));
//           resList.push(courseDict);
//         } // for

//         courseList = courseList.concat(resList);
//         // console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
//         const eleList = await s.page.$$(activePaginationItemSel);
//         if (Array.isArray(eleList) && eleList.length > 0) {
//           const activePageButton = eleList[0];
//           // reassign next sibling handle
//           nextElementSiblingHandle = await activePageButton.getProperty('nextElementSibling');
//           console.log(funcName + 'nextElementSiblingHandle = ' + nextElementSiblingHandle);
//           if (nextElementSiblingHandle == "JSHandle@node") {
//             const propHandle = nextElementSiblingHandle.getProperty('nodeName');
//             console.log(funcName + 'propHandle = ' + propHandle);
//             let nextSiblingNodeName = null;
//             await propHandle.then((res) => {
//               console.log('res = ' + res);
//               const resJSon = res.jsonValue();
//               console.log('resJSon = ' + resJSon);
//               resJSon.then((resB) => {
//                 console.log('resB = ' + resB);
//                 nextSiblingNodeName = resB;
//               });
//             });
//             console.log(funcName + 'nextSiblingNodeName = ' + nextSiblingNodeName);
//             if (String(nextSiblingNodeName) !== 'LI') { // if next is not Anchor element
//               console.log(funcName + 'nextElementSiblingHandle includes null....so breaking the loop...');
//               break;
//             }
//             if (nextElementSiblingHandle) {
//               await nextElementSiblingHandle.click('middle');
//               console.log(funcName + 'Clicked nextElementSiblingHandle..');
//             } else {
//               throw (new Error('nextElementSiblingHandle invalid'));
//             }
//           }
//           else { break; }
//         }

//         pageCount += 1;
//         console.log(funcName + 'courseList count = ' + courseList.length);
//       } while (nextElementSiblingHandle);

//       console.log(funcName + 'total pages scrapped = ' + pageCount);
//       console.log(funcName + 'courseList count = ' + courseList.length);
//       // console.log(funcName + 'courseList = ' + JSON.stringify(courseList));
//       await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(courseList));
//       console.log(funcName + 'writing courseList to file completed successfully....');
//       if (s) {
//         await s.close();
//       }
//     } catch (error) {
//       console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r\n\r');
//       console.log(funcName + 'try-catch error = ' + error);
//       console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r\n\r');
//       if (s) {
//         await s.close();
//       }
//       console.log(funcName + 'writing courseList to tmp file....');
//       await fs.writeFileSync(configs.opCourseListTempFilepath, JSON.stringify(courseList));
//       console.log(funcName + 'writing courseList to tmp file completed successfully....');
//       throw (error);
//     }
//   }

//   // scrape course page list of the univ
//   async scrapeCourseListAndPutAtS3(selFilepath) {
//     const funcName = 'scrapeCourseListAndPutAtS3 ';
//     try {
//       // validate params
//       Scrape.validateParams([selFilepath]);
//       // load file
//       const fileData = fs.readFileSync(selFilepath);
//       this.selectorJson = JSON.parse(fileData);
//       const pageUrl = this.selectorJson.url;
//       console.log(funcName + 'url = ' + pageUrl);
//       const jsonDictElements = this.selectorJson.elements;
//       console.log(funcName + 'elements = ' + JSON.stringify(jsonDictElements));
//       let courseList = null;
//       if (Array.isArray(jsonDictElements)) {
//         for (const eleDict of jsonDictElements) {
//           const eleType = eleDict.elementType;
//           console.log(funcName + 'elementType = ' + eleType);
//           // ensure type is matching
//           if (eleType === Scrape.ELEMENT_TYPE.COURSE_LIST) {
//             const courseItemSelector = eleDict.course_item_selector;
//             console.log(funcName + 'course_item_selector = ' + courseItemSelector);
//             const pageSelector = eleDict.page_selector;
//             console.log(funcName + 'page_selector = ' + pageSelector);
//             courseList = await ScrapeCourseList.scrapeCourseListForPages(courseItemSelector, pageSelector, pageUrl);
//           } // if
//         } // for elements
//       } // if
//       // console.log(funcName + 'writing courseList to file....');
//       // const selector = "#main > div.center-col.content > table > tbody > tr > th";
//       // const innerText = "CRICOS Code:";
//       // await utils.validateMyCourseList(this.s.page,configs.opCourseListFilepath,courseList,selector,innerText);
//       // //await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(courseList));
//       // console.log(funcName + 'writing courseList to file completed successfully....');
//       // put file in S3 bucket
//       // const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
//       //console.log(funcName + 'object location = ' + res);
//       return courseList;
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       throw (error);
//     }
//   }
//   // refers to http://www.utas.edu.au/courses
//   async scrapeCourseList(selFilepath) {
//     const funcName = 'scrapeCoursePageList ';
//     let s = null;
//     try {
//       // validate params
//       Scrape.validateParams([selFilepath]);
//       // load file
//       const fileData = fs.readFileSync(selFilepath);
//       this.selectorJson = JSON.parse(fileData);
//       // course_list selector
//       const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
//       if (!selectorsList) {
//         console.log(funcName + 'Invalid selectorsList');
//         throw (new Error('Invalid selectorsList'));
//       }
//       const sel = selectorsList[0];
//       console.log(funcName + 'sel = ' + JSON.stringify(sel));

//       // create Scrape object
//       s = new Scrape();
//       await s.init({ headless: true });
//       const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
//       if (!elementDict) {
//         console.log(funcName + 'Invalid elementDict');
//         throw (new Error('Invalid elementDict'));
//       }
//       const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
//       if (!rootEleDictUrl) {
//         console.log(funcName + 'Invalid rootEleDictUrl');
//         throw (new Error('Invalid rootEleDictUrl'));
//       }
//       const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
//       console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

//       // course_level_selector key
//       const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
//       if (!coursLvlSelList) {
//         console.log(funcName + 'Invalid coursLvlSelList');
//         throw (new Error('Invalid coursLvlSelList'));
//       }
//       const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
//       if (!elementDictForCorsLvlKey) {
//         console.log(funcName + 'Invalid elementDict');
//         throw (new Error('Invalid elementDict'));
//       }

//       // course_level_form_selector key
//       // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
//       // if (!formSelList) {
//       //   console.log(funcName + 'Invalid formSelList');
//       //   throw (new Error('Invalid formSelList'));
//       // }
//       // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
//       // if (!formSelElementDict) {
//       //   console.log(funcName + 'Invalid formSelElementDict');
//       //   throw (new Error('Invalid formSelElementDict'));
//       // }

//       // for each course-category, scrape all courses
//       let totalCourseList = []; let count = 1;
//       for (const catDict of courseCatDictList) {
//         console.log(funcName + 'count = ' + count);
//         // click on href
//         console.log(funcName + 'catDict.href = ' + catDict.href);
//         await s.setupNewBrowserPage(catDict.href);
//         const selItem = coursLvlSelList[0];
//         console.log(funcName + 'selItem = ' + selItem);
//         const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
//         console.log(funcName + 'res = ' + JSON.stringify(res));
//         if (Array.isArray(res)) {
//           totalCourseList = totalCourseList.concat(res);
//         } else {
//           throw (new Error('res is not array, it must be...'));
//         }
//         // /////// TEMP /////////
//         // if (count >= 2) {
//         //   break;
//         // }
//         // /////// TEMP /////////
//         count += 1;
//       } // for (const catDict of courseDictList)
//       console.log(funcName + 'writing courseList to file....');
//       // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
//       await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
//       console.log(funcName + 'writing courseList to file completed successfully....');
//       // put file in S3 bucket
//       const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
//       console.log(funcName + 'object location = ' + res);
//       // destroy scrape resources
//       if (s) {
//         await s.close();
//       }
//       return null;
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       // destroy scrape resources
//       if (s) {
//         await s.close();
//       }
//       throw (error);
//     }
//   }
// } // class

module.exports = { ScrapeCourseList };
