const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');
class ScrapeCourse extends Course {
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, course_category,study_level) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'course_discipline': {
              // As of not we do not have way to get this field value
              resJsonData.course_discipline = course_category;
              break;
            }
           
            case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              const courseAdminReq = {};
              const englishList = [];
              let programType = resJsonData.course_study_level;
              //progress bar start
              const penglishList = [], pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
              const ieltsDict = {}; const ibtDict = {}; const pteDict = {}; const caedisc = {}; let ieltsMapNumber = null; let ieltsNumber = null; const cpeDict = {};
              const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
              // english requirement
              if (courseScrappedData.course_toefl_ielts_score) {

                const ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);

                const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                console.log("cTitle :" + cTitle);
               
                if (cTitle) {
                  if (String(cTitle).toLowerCase().indexOf('bachelor') > -1 || String(cTitle).toLowerCase().indexOf('bachelors') > -1 || String(cTitle).toLowerCase().indexOf('advanced diploma') > -1) {
                    programType = 'Bachelor';
                  }
                  else if (String(cTitle).toLowerCase().indexOf('master by research') > -1 || String(cTitle).toLowerCase().indexOf('master by researchs') > -1 || String(cTitle).toLowerCase().indexOf('master research') > -1 || String(cTitle).toLowerCase().indexOf('master by researchs') > -1) {
                    programType = 'Research';
                  }
                  else if (String(cTitle).toLowerCase().indexOf('research') > -1 || String(cTitle).toLowerCase().indexOf('researchs') > -1) {
                    programType = 'Research';
                  }
                  else if (String(cTitle).toLowerCase().indexOf('master') > -1 || String(cTitle).toLowerCase().indexOf('masters') > -1) {
                    programType = "Masters";

                  } else if (String(cTitle).toLowerCase().indexOf('graduate diploma') > -1 || String(cTitle).toLowerCase().indexOf('graduate diplomas') > -1) {
                    programType = "GraduateDiplomas";
                  } else if (String(cTitle).toLowerCase().indexOf('diploma') > -1 || String(cTitle).toLowerCase().indexOf('diplomas') > -1) {
                    programType = "Diplomas";

                  } else if (String(cTitle).toLowerCase().indexOf('certificate') > -1 || String(cTitle).toLowerCase().indexOf('certificate') > -1) {
                    programType = "Certificates";

                  } else if (String(cTitle).toLowerCase().indexOf('advanced diploma') > -1 || String(cTitle).toLowerCase().indexOf('advanced diploma') > -1) {
                    programType = "AdvancedDiplomas";

                  } else if (String(cTitle).toLowerCase().indexOf('graduate certificate') > -1 || String(cTitle).toLowerCase().indexOf('graduate certificate') > -1) {
                    programType = "GraduateCertificates";

                  } else if (String(cTitle).toLowerCase().indexOf('phd') > -1 || String(cTitle).toLowerCase().indexOf('doctor') > -1 || String(cTitle).toLowerCase().indexOf('doctorate') > -1 || String(cTitle).toLowerCase().indexOf('doctorates') > -1) {
                    programType = 'Research';
                  }
                  

                  
                }
                console.log("programType :" + programType);
              }
              if (cTitle == "International Foundations at VU") {
                const InternationalMappingDict = await utils.getValueFromHardCodedJsonFile('International Foundations at VU');
                var ieltsScore = ""
                if (InternationalMappingDict) {
                  //englishList.push({ name: "ielts academic", description: InternationalMappingDict.ielts });
                
                if (InternationalMappingDict.ielts) {
                  ieltsScore = await utils.giveMeNumber(InternationalMappingDict.ielts);
                  console.log("### IELTS data-->" + ieltsScore);
                }
                ieltsDict.name = 'ielts academic';
                ieltsDict.description = InternationalMappingDict.ielts;
                ieltsDict.min = 0;
                ieltsDict.require = ieltsScore;
                ieltsDict.max = 9;
                ieltsDict.R = 0;
                ieltsDict.W = 0;
                ieltsDict.S = 0;
                ieltsDict.L = 0;
                ieltsDict.O = 0;
                englishList.push(ieltsDict);
              }
            }

              if (cTitle == "International Foundations at VU (Extended)") {
                const InternationalMappingDict = await utils.getValueFromHardCodedJsonFile('International Foundations at VU (Extended)');
                var ieltsScore = ""
                if (InternationalMappingDict) {
                 // englishList.push({ name: "ielts academic", description: InternationalMappingDict.ielts });
                
                if (InternationalMappingDict.ielts) {
                  ieltsScore = await utils.giveMeNumber(InternationalMappingDict.ielts);
                  console.log("### IELTS data-->" + ieltsScore);
                }
                ieltsDict.name = 'ielts academic';
                  ieltsDict.description = InternationalMappingDict.ielts;
                  ieltsDict.min = 0;
                  ieltsDict.require = ieltsScore;
                  ieltsDict.max = 9;
                  ieltsDict.R = 0;
                  ieltsDict.W = 0;
                  ieltsDict.S = 0;
                  ieltsDict.L = 0;
                  ieltsDict.O = 0;
                  englishList.push(ieltsDict);
              }
            }


              if (programType) {
               
                const ieltsMappingDict = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                if (ieltsMappingDict) {
                  console.log("ptype:" + programType);
                  console.log("-----?ieltsMapping###:" + JSON.stringify(ieltsMappingDict));

                }

                const otherLngDict = ieltsMappingDict[programType];
                console.log(funcName + 'ieltsMappingProgressDict = ' + JSON.stringify(otherLngDict));
                const potherLngDict = otherLngDict;
                if (potherLngDict) {
                  var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                  if (potherLngDict.ielts) {
                    ieltsScore = await utils.giveMeNumber(potherLngDict.ielts);
                    console.log("### IELTS data-->" + ieltsScore);
                  }
                  if (potherLngDict.ibt) {
                    ibtScore = await utils.giveMeNumber(potherLngDict.ibt);
                  }
                  if (potherLngDict.pte) {
                    pteScore = await utils.giveMeNumber(potherLngDict.pte);
                  }
                  if (potherLngDict.cae) {
                    caeScore = await utils.giveMeNumber(potherLngDict.cae);
                  }
                  ieltsDict.name = 'ielts academic';
                  ieltsDict.description = ieltsMappingDict[programType].ielts;
                  ieltsDict.min = 0;
                  ieltsDict.require = ieltsScore;
                  ieltsDict.max = 9;
                  ieltsDict.R = 0;
                  ieltsDict.W = 0;
                  ieltsDict.S = 0;
                  ieltsDict.L = 0;
                  ieltsDict.O = 0;
                  englishList.push(ieltsDict);

                  ibtDict.name='toefl ibt';
                  ibtDict.description = ieltsMappingDict[programType].ibt;
                  ibtDict.min = 0;
                  ibtDict.require =await utils.giveMeNumber(ieltsMappingDict[programType].ibt);
                  ibtDict.max = 120;
                  ibtDict.R = 0;
                  ibtDict.W = 0;
                  ibtDict.S = 0;
                  ibtDict.L = 0;
                  ibtDict.O = 0;
                  englishList.push(ibtDict);

                  pteDict.name='pte academic',
                  pteDict.description=ieltsMappingDict[programType].pte;
                  pteDict.min = 0;
                  pteDict.require = pteScore;
                  pteDict.max = 90;
                  pteDict.R = 0;
                  pteDict.W = 0;
                  pteDict.S = 0;
                  pteDict.L = 0;
                  pteDict.O = 0;
                  englishList.push(pteDict);
                  
                  caedisc.name='cae',
                  caedisc.description=ieltsMappingDict[programType].cae;
                  caedisc.min= 80;
                  caedisc.require = caeScore;
                  caedisc.max = 230;
                  caedisc.R = 0;
                  caedisc.W = 0;
                  caedisc.S = 0;
                  caedisc.L = 0;
                  caedisc.O = 0;
                  englishList.push(caedisc);

                 
                }

              }
             

              if (englishList && englishList.length > 0) {
                courseAdminReq.english = englishList;
              }else{
                courseAdminReq.english=[]
              }

              if (courseScrappedData.course_academic_requirement) {
                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                let arr=[]
                console.log('\n\r');
                console.log(funcName + 'academicReq = ' + academicReq);
                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                console.log('\n\r');
                arr.push(academicReq)

                if (academicReq && String(academicReq).length > 0) {
                  courseAdminReq.academic = arr;
                }
              }
              // if courseAdminReq has any valid value then only assign it
              if (courseAdminReq.english || courseAdminReq.academic) {
                resJsonData.course_admission_requirement = courseAdminReq;
              }

              // add english requirement 'english_more_details' link
              const courseKeyVal = courseScrappedData.entry_requirements_url;
              console.log("EntryRequirement:::" + courseKeyVal);
              let resEnglishReqMoreDetailsJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resEnglishReqMoreDetailsJson = selList[0];
                      }
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))

              // add english requirement 'english_more_details' link
              const courseKeyValAcademic = courseScrappedData.academic_requirements_url;
              console.log("AcademicRequirement:::" + courseKeyValAcademic);
              let resAcademicReqMoreDetailsJson = null;
              if (Array.isArray(courseKeyValAcademic)) {
                for (const rootEleDict of courseKeyValAcademic) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAcademicReqMoreDetailsJson = selList[0];
                      }
                    }
                  }
                }
              }

              // add english requirement 'english_requirements_url'
              const courseKeyValEnglishReq = courseScrappedData.english_requirements_url;
              console.log("EnglishRequirement:::" + courseKeyValEnglishReq);
              let resEnglishReqJson = null;
              if (Array.isArray(courseKeyValEnglishReq)) {
                for (const rootEleDict of courseKeyValEnglishReq) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resEnglishReqJson = selList[0];
                      }
                    }
                  }
                }
              }

              if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqJson;
              } else if (resEnglishReqMoreDetailsJson) {
                resJsonData.course_admission_requirement = {};
                resJsonData.course_admission_requirement.entry_requirements_url = resEnglishReqMoreDetailsJson;
                resJsonData.course_admission_requirement.academic_requirements_url = resAcademicReqMoreDetailsJson;
                resJsonData.course_admission_requirement.english_requirements_url = resEnglishReqJson;
              }
              break;
            }
            case 'course_url': {
              if (courseUrl && courseUrl.length > 0) {
                resJsonData.course_url = courseUrl;
              }
              break;
            }
             case 'course_outline': {
              
               let mgcrs = null;
             
            const majorcrs = {
              majors:[],
              minors:[],
              more_details:""
            };
            //for major cousrseList
              var coursemajor =  courseScrappedData.course_outline;
              if (Array.isArray(coursemajor)) {
                for (const rootEleDict of coursemajor) {
                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        const selectorsList = eleDict.selectors;
                        for (const selList of selectorsList) {
                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                            if (Array.isArray(selList) && selList.length > 0) {
                              mgcrs = selList;
                            }
                        }
                    }
                }
            }
              if(mgcrs && mgcrs.length >0){
                majorcrs.majors=mgcrs
              }
              else{
                majorcrs.majors= majorcrs.majors
              }
                 resJsonData.course_outline =  majorcrs;
              //for minor courseList
                 var courseminor =  courseScrappedData.course_outline_minor;
                 let mincrs = null;
                 if (Array.isArray(courseminor)) {
                   for (const rootEleDict of courseminor) {
                       console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                       const elementsList = rootEleDict.elements;
                       for (const eleDict of elementsList) {
                           console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                           const selectorsList = eleDict.selectors;
                           for (const selList of selectorsList) {
                               console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                               if (Array.isArray(selList) && selList.length > 0) {
                                mincrs = selList;
                               }
                           }
                       }
                   }
               }
                 if(mincrs && mincrs.length >0){
                   majorcrs.minors=mincrs
                 }
                 else{
                  majorcrs.minors=majorcrs.minors
                }
                    resJsonData.course_outline =  majorcrs;

                 console.log("major==>", resJsonData.course_outline)
           
             break;
             }
            case 'course_duration_full_time': {
              var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
              var fullTimeText = "";
              const courseKeyVal = courseScrappedData.course_duration_full_time;
              console.log("courseKeyVal: " + JSON.stringify(courseKeyVal));
              if (Array.isArray(courseKeyVal)) {
                  for (const rootEleDict of courseKeyVal) {
                      console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                      const elementsList = rootEleDict.elements;
                      for (const eleDict of elementsList) {
                          console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                          const selectorsList = eleDict.selectors;
                          for (const selList of selectorsList) {
                              console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                              if (Array.isArray(selList) && selList.length > 0) {
                                  fullTimeText = selList[0];
                              }
                          }
                      }
                  }
              }
             
              if (fullTimeText && fullTimeText.length > 0) {
                resJsonData.course_duration = String(fullTimeText);
                  const resFulltime = fullTimeText;
                  if (resFulltime) {
                      durationFullTime.duration_full_time = resFulltime;
                      courseDurationList.push(durationFullTime);
                      courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                      console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                      let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                      console.log("FilterData---->"+JSON.stringify(filtered_duration_formated))

                      if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                         
                          resJsonData.course_duration_display = filtered_duration_formated;
                          var isfulltime = false, isparttime = false;
                          filtered_duration_formated.forEach(element => {
                              if (element.display == "Full-Time") {
                                  isfulltime = true;
                              }
                              if (element.display == "Part-Time") {
                                  isparttime = true;
                              }
                          });
                          resJsonData.isfulltime = isfulltime;
                          resJsonData.isparttime = isparttime;
                      }
                      
                  }
              }
              break;
          }
            case 'program_code': {
              const courseKeyVal = courseScrappedData.program_code;

              console.log("courseKey: " + JSON.stringify(key));
              var program_code = "";
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        program_code = selList;
                        
                      }
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))
              resJsonData.program_code = String(program_code);
              break;
            }
            case 'course_tuition_fee':
            case 'course_study_mode':
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_duration_part_time': {
              console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
              // No need to process anything, its already processed in other case.
              break;
            }


            case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};
              const feesList = [];
              const feesDict = {
                
              };
              const courseKeyVal1 = courseScrappedData.course_tuition_fees_international_student;
              let fee_val = null;
              if (Array.isArray(courseKeyVal1)) {
                for (const rootEleDict of courseKeyVal1) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        fee_val = selList;
                      }
                    }
                  }
                }
              }
              console.log("My Final fee amount--->" + JSON.stringify(fee_val));
              var fixedFeesDict = "";
              var myfeesarray = await utils.giveMeArray(fee_val[0].replace(/[\r\n\t]+/g, ';'), ";")
              console.log("MyfeesArray!!!" + JSON.stringify(myfeesarray));
              if (String(myfeesarray).includes("2019") || String(myfeesarray).includes("2018") || String(myfeesarray).includes("2020")) {
                myfeesarray.forEach(element => {
                  if (element.indexOf("2019") != -1 || element.indexOf("2020") != -1) {
                   
                    fixedFeesDict = element;
                    console.log("element@@@@",fixedFeesDict)
                  } else if (element.indexOf("2018") != -1) {
                    fixedFeesDict = element;
                  }

                });
                
              } else {
                fixedFeesDict = myfeesarray
              }
              let splitdata;
              if(fixedFeesDict.includes(":"))
              {
                splitdata=fixedFeesDict.split(":")[1]

              }else{
                splitdata= fixedFeesDict
              }
              
              console.log("element@@@@",splitdata)
              const feesIntStudent = splitdata;
              console.log("feesIntStudent###" + feesIntStudent);
              if (feesIntStudent == "NA") {
                feesDict.international_student = 0;
              }

              else {
               
                const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                if (feeYear && feeYear.length > 0) {
                  courseTuitionFee.year = feeYear;
                }
               
                // if we can extract value as Int successfully then replace Or keep as it is
                if (feesIntStudent && feesIntStudent.length >0) {
                 
                  // extract only digits
                  let feesVal = "";
                  const feesWithDollorTrimmed = String(feesIntStudent).trim();
                  console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                  const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                  const arrval = String(feesVal1).split('.');
                  const feesVals = String(arrval[0]);
                  console.log(funcName + 'feesVal custome = ' + feesVals.includes("2019: "));
                  if (feesVals.includes("2019: ")) {
                    feesVal = feesVals.split("2019:");
                  } else {
                    feesVal = feesVals;
                  }

                  //const feesVal = feesVals.substr(6, 13);
                  console.log("Fees---" + feesVal);
                  if (feesVal) {
                    const regEx = /\d/g;
                    let feesValNum = String(feesVal).match(regEx);
                    if (feesValNum) {
                      console.log(funcName + 'feesValNum = ' + feesValNum);
                      feesValNum = feesValNum.join('');
                      console.log(funcName + 'feesValNum = ' + feesValNum);
                      let feesNumber = null;
                      if (feesValNum.includes(',')) {
                        feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                      } else {
                        feesNumber = feesValNum;
                      }
                      console.log(funcName + 'feesNumber = ' + feesNumber);
                      if (Number(feesNumber)) {
                        //feesDict.international_student = Number(feesNumber);
                        feesDict.international_student={
                          amount: Number(feesNumber),
                          duration: 1,
                          unit: "Semester",
                          description: feesIntStudent
                         
                        }


                      }
                    }
                  }
                }else{
                  feesDict.international_student={
                    amount: Number(feesIntStudent),
                    duration: 1,
                    unit: "Year",
                    description: ""
                   
                  }
                }
              }
            //  let international_student_all_fees_array = [];
              if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                //  feesDict.international_student_all_fees = international_student_all_fees_array
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }



                if (feesDict) {
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                if (courseTuitionFee) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
                // take tuition fee value at json top level so will help in DDB for indexing
                if (courseTuitionFee && feesDict.international_student) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                  //resJsonData.course_tuition_fee_amount = feesDict.international_student;
                }
                console.log('******************************END Tution fee****************************');
              }
              console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
              if (!feesDict.international_student) {
                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                return null; // this will add this item into FailedItemList and writes file to local disk
              }

              break;
            }
            case 'course_campus_location': { // Location Launceston

              const courseKeyValcd = courseScrappedData.course_cricos_code;
              let course_cricos_code = null;
              if (Array.isArray(courseKeyValcd)) {
                for (const rootEleDict of courseKeyValcd) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        course_cricos_code = selList;
                      }
                      else{
                      
                          course_cricos_code = selList;
                        }
                      }
                    }
                  }
                }
              
                if(course_cricos_code && course_cricos_code.length >0){
                  course_cricos_code=course_cricos_code;
                }
                else{
                  throw (new Error('Cricose code Not found!!'));
                }
              const courseKeyVal = courseScrappedData.course_campus_location;
              let course_campus_location = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      // if (Array.isArray(selList) && selList.length > 0) {
                      course_campus_location = selList;
                      // }
                    }
                  }
                }
              }
              if(course_campus_location && course_campus_location.length > 0){
                resJsonData.course_campus_location = course_campus_location;
              
              var campusss = [];
              const campLocationText = course_campus_location;
              console.log(funcName + 'campLocationTextArr = ' + campLocationText);
              //const campLocationText = campLocationTextArr.split(" ");
              console.log(funcName + 'campLocationText = ' + campLocationText);

              if (campLocationText && campLocationText.length > 0) {

                var campLocationValTrimmed = String(campLocationText).trim();
                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);

                if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                  
                  var campuss = await utils.giveMeArray(campLocationValTrimmed, ',');
                  console.log("Campuses@@@"+JSON.stringify(campuss))
                 
                  if(campuss.length>0){
                    let campuses=['Footscray Park','City Flinders','City Flinders Lane','City King','City Queen','Footscray Nicholson','St Albans','Sunshine','Werribee','VU at MetroWest (Footscray)','VU Sydney']
                    campuss.forEach(element=>{
                      if(campuses.includes(element)){
                      console.log("YESSSS location is there")
                      for(var i=0;i<campuses.length;i++){
                        if(campuses[i]==element){
                          campusss.push(campuses[i])
                        }
                    }
                      }

                    });
                  }
                  var campusedata = [];
                  campusss.forEach(element => {
                    var cam= format_functions.titleCase(element)
                          campusedata.push({
                              "name":cam ,
                              "code":String(course_cricos_code)
                            
                          })
                      });
                      
                      resJsonData.course_campus_location = campusedata;
              
                
                  // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                  var study_mode = [];
                  campLocationValTrimmed = campLocationValTrimmed.toLowerCase();
                  if (campLocationValTrimmed.includes('distance')) {
                    study_mode.push('Distance');
                    campLocationValTrimmed = campLocationValTrimmed.replace("distance", "");
                  }
                  if (campLocationValTrimmed.includes('online')) {
                    study_mode.push('Online');
                    campLocationValTrimmed = campLocationValTrimmed.replace("online", "");
                  }
                  campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                  if (campLocationValTrimmed.trim().length > 0) {
                    study_mode.push('On campus');
                  }
                  resJsonData.course_study_mode ='On campus';//.join(',');
                  console.log("## FInal string-->" + campLocationValTrimmed);
                }
              } // if (campLocationText && campLocationText.length > 0)
            }
            else{
              throw (new Error('Location Not found!!'));
            }
              break;
            }
          
            case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
              var courseIntakeStr = [];//= await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
              // existing intake value
              let intakeStrLists = "";
              let intakeStrList = "";
              var intakedata = {};
              var month_name = [];
              const courseIntakeStr1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
              var intakearray = await utils.giveMeArray(courseIntakeStr1.replace(/[\r\n\t]+/g, ';'), ";");
             
              if (String(intakearray).includes("2019") || String(intakearray).includes("2018") || String(intakearray).includes("2020")) {

                intakearray.forEach(element => {
                  if (element.indexOf("2019") != -1 || element.indexOf("2020")!= -1) {
                    intakeStrLists = element;
                   } 
                   else if (element.indexOf("2018") != -1) {
                    intakeStrLists = element;
                  }
                });
              }
              else {
                intakeStrList = intakearray;
                console.log("&&&intakes###" + JSON.stringify(String(intakeStrList).split(",")));
                if (intakeStrList) {
                  var monthname = String(intakeStrList).split(",");
                  if (monthname.includes('3') && monthname.includes('7') && monthname.includes('11')) {
                    month_name.push('March', 'July', 'November');
                    intakeStrLists = month_name;
                  }
                }
              }

              console.log(funcName + 'intakeStrList course_intake = ' + JSON.stringify(intakeStrLists));
              /* if(intakeStrLists!="NA"){*/


              if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                resJsonData.course_intake = courseIntakeStr1;
                
                courseIntakeStr = intakeStrLists;
              }



              console.log("intakes---" + JSON.stringify(courseIntakeStr));
              var montharray = null;
              if (courseIntakeStr != "NA") {
                if (courseIntakeStr.includes(":")) {
                  var intakespit = await utils.giveMeArray(courseIntakeStr.replace(/[\r\n\t]+/g, ':'), ':');
                  console.log("intakeArray------" + JSON.stringify(intakespit[1]));
                  
                  montharray = await utils.giveMeArray(intakespit[1].replace("and", ','), ",");
                } else {
                  montharray = courseIntakeStr;
                }
              }
              else {
                montharray = []
              }
             
              console.log("month------" + JSON.stringify(montharray));
              if (courseIntakeStr && courseIntakeStr.length > 0) {
                var cam = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                
                var campus = resJsonData.course_campus_location;


                //console.log("Campus---"+campuss);
                console.log('Campuse location :' + campus);
                var intakes = [];
                console.log("Intake length-->" + montharray);
                console.log("Campus length-->" + campus.length);




                for (var camcount = 0; camcount < campus.length; camcount++) {
                  
                  var intakedetail = {};
                  //intakedetail.campus = campus[count];
                  intakedetail.name = campus[camcount].name;
                  intakedetail.value = montharray // await utils.giveMeArray("2019:".concat(montharray[count].replace(/[\r\n\t ]+/g, ''), ""));//await utils.giveMeArray(courseIntakeStr[count].replace(/[\r\n\t ]+/g, ' '), ";");
                  console.log("Campus length  intakedetail-->" +  JSON.stringify(intakedetail));

                  intakes.push(intakedetail);
                  
                  //}
                }
                console.log("FinalIntake--" + JSON.stringify(intakes));
                let formatedIntake = await format_functions.providemyintake(intakes,"MOM","");
                 console.log("NEw Intake Format@@@@@"+JSON.stringify( formatedIntake))

                intakedata.intake = formatedIntake;
                var intakeUrl = "";
                
              intakeUrl =(await utils.getValueFromHardCodedJsonFile('intake_url'));
                intakedata.more_details =  intakeUrl;
                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
              }
             
              break;
            }

            case 'course_study_level': {
              
              let cStudyLevel = null;
              console.log(funcName + 'course_title course_study_level = ' + study_level);
              resJsonData.course_study_level = study_level;
              
              break;
            }
            case 'course_country': {
              const courseKeyVal = courseScrappedData.course_country;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_country = resAccomodationCostJson;
              }
              break;
            }
            case 'application_fee': {
              const courseKeyVal = courseScrappedData.application_fee;
             
              let applicationfee = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        applicationfee = selList[0];
                      }
                    }
                  }
                }
              }
              if (applicationfee) {
                resJsonData.application_fee = applicationfee;
                console.log("appfee"+ JSON.stringify(resJsonData.application_fee));
              }
             
              break;
             
            
            }
            case 'course_overview': {
              const courseKeyVal = courseScrappedData.course_overview;
              console.log("overview---" + JSON.stringify(courseKeyVal));
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_overview = resAccomodationCostJson;
              }else{
                resJsonData.course_overview="";
              }
              break;
            }
            case 'course_career_outcome': {
              const outcome_data = await Course.extractValueFromScrappedElement(courseScrappedData.course_career_outcome)
              // console.log("outttttttcome:"+outcome_data);
              if (outcome_data) {
                var outcome = await utils.giveMeArray(outcome_data.replace(/[\r\n\t]+/g, ';'), ';');
                if (outcome.length > 0) {
                  resJsonData.course_career_outcome = outcome;
                  console.log("#### Outcome is--->" + resJsonData.course_career_outcome);
                } else{
                  resJsonData.course_career_outcome=[]
                }
              }else{
                resJsonData.course_career_outcome=[]
              }
            
              console.log("#### Outcome is--->" + resJsonData.course_career_outcome);
              break;
            }
            case 'course_title': {
              const title=await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
              var ctitle= format_functions.titleCase(title)
              console.log("ctitle@@@",ctitle)
              resJsonData.course_title=ctitle
              break;
            }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      
      var NEWJSONSTRUCT = {}, structDict = [];
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
          NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
          NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase() ;
          NEWJSONSTRUCT.course_title = resJsonData.course_title;
          NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
          NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
          NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
          NEWJSONSTRUCT.course_url = resJsonData.course_url;
          NEWJSONSTRUCT.course_campus_location = location.name;
          NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
          NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
          NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
          NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
          NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
          var intakes = resJsonData.course_intake.intake;
          var matchrec = [];
          for (let dintake of intakes) {
              if (location == dintake.name) {
                  matchrec = dintake.value;
              }
          }
          if (matchrec.length > 0) {
              NEWJSONSTRUCT.course_intake = matchrec[0];
          }
          else {
              NEWJSONSTRUCT.course_intake = [];
          }
         
          structDict.push(NEWJSONSTRUCT);
          NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
          console.log("location::::" + location_wise_data.course_campus_location);
          resJsonData.course_id = location_wise_data.course_location_id;
       
          var filelocation = "./output/" + resJsonData.univ_id+ "_" +resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase()+ "_details.json";
          console.log("Write file--->" + filelocation)
          fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

          
      }
      return resJsonData;
  } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
  }
}

// scrape each course details
static async scrapeAndFormatCourseDetails(courseDict) {
  const funcName = 'scrapeAndFormatCourseDetails ';
  let s = null;
  try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);

      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
          console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
          throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }
      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
          console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
          throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
          console.log(funcName + 'reading scrapped data from local output file...');
          const fileData = fs.readFileSync(opCourseFilePath);
          if (!fileData) {
              console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
              throw (new Error('Invalid fileData, fileData = ' + fileData));
          }
          courseScrappedData = JSON.parse(fileData);
          return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      


      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
          console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
          courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
          console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
          courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
          throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }



      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData,courseDict.href,courseDict.category,courseDict.study_level);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
          await s.close();
      }
      return finalScrappedData;
  } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
          await s.close();
      }
      throw (error);
  }
} // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };

