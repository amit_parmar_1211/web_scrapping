const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
//const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    const categoryselectorUrl = "//*/div/div/div/div/ul/li/div/span/a[not(contains(@href,'/vu-english/english-language-courses'))]";
    const categoryselectorText = "//*/div/div/div/div/ul/li/div/span/a[not(contains(@href,'/vu-english/english-language-courses'))]"
    var elementstring = "", elementhref = "", allcategory = [];
    const targetLinksCardsUrls = await page.$x(categoryselectorUrl);
    const targetLinksCardsText = await page.$x(categoryselectorText);
    var targetLinksCardsTotal = [];
    for (let i = 0; i < targetLinksCardsUrls.length; i++) {
      elementstring = await page.evaluate(el => el.innerText, targetLinksCardsText[i]);
      elementhref = await page.evaluate(el => el.href, targetLinksCardsUrls[i]);
      targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
    }
    await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
    console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));

    var totalCourseList = [];
    var studylevels=[]
    for (let target of targetLinksCardsTotal) {
      await page.goto(target.href+"?audience=international",{timeout:0});
      await page.waitFor(5000);
      
      var selectcourse=await page.$x("//*/div/div/div/ul/li/div/span/a[contains(text(),'Browse all')]")
      const activeselectors = "//*[@class='search-result-list col-md-9']/nav/ul/li[@class='page-item active']";
      await selectcourse[0].click();
      console.log("Yesssss all")
      await page.waitFor(5000);
      var selectint=await page.$x("//*[@id='search-facets']/div[3]/div/ul/li[2]/a")
     await selectint[0].click();
      console.log("Yessss click non")
      await page.waitFor(2000);
      var ispage = true;
      while (ispage) {
       
        var linkselector="//*[@class='search-result-list col-md-9']/ol/li/a"
        var titleselectior="//*[@class='search-result-list col-md-9']/ol/li/a/div/div/div[@class='card-title']"
        var studylevel=await page.$x("//*[@class='search-result-list col-md-9']/ol/li/a/div/div/div[@class='card-text']/div/div/p")
      
        const targetLinks = await page.$x(linkselector);
        const targettitle=await page.$x(titleselectior);
          for(let i=0;i<targetLinks.length;i++){
          var elementstring = await page.evaluate(el => el.innerText, targettitle[i]);
          const elementlink = await page.evaluate(el => el.href, targetLinks[i]);
          const study_level=await page.evaluate(el=>el.textContent,studylevel[i]);
          var studylevell= study_level.toString().split('|')[0].trim();
          totalCourseList.push({ href: elementlink, innerText: elementstring, category: target.innerText,study_level:studylevell});
        }
        console.log("PAge@@@@@@@",totalCourseList)
        const active = await page.$x(activeselectors);
        if (active[0]) {
          const next = await page.evaluateHandle(el => el.nextElementSibling, active[0]);
         
          if (next && next.toString() != "JSHandle:null") {
            var nodename = await (await next.getProperty('nodeName')).jsonValue();
            console.log("Next-->" + nodename)
            if (nodename == "LI") {
             await next.click();
             
              await page.waitFor(9000);
            }
            else {
              ispage = false;
            }
          }
          else {
            ispage = false;
            console.log("null node-->")
          }
        }
        else {
         
          ispage = false;
        }


      }
      
    }
    
  

   

    

    console.log("totalCourseList -->", totalCourseList);

      await fs.writeFileSync("./output/victoriauniversity_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [],study_level:totalCourseList[i].study_level});
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [],study_level:totalCourseList[i].study_level});
        }
      }

      await fs.writeFileSync("./output/victoriauniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/victoriauniversity_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

}//class

module.exports = { ScrapeCourseList };
