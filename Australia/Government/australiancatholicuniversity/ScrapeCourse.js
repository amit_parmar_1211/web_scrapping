const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');

request = require('request');
class ScrapeCourse extends Course {
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, courseDict, course_category) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'course_discipline': {
              resJsonData.course_discipline = courseDict.category;
              //  let arr =[]
              //  arr.push(course_category)
              //  resJsonData.course_discipline =  arr;
            }
            case 'english_more':
            case 'course_toefl_ielts_score': {
              const courseAdminReq = {};
              const englishList = [];
              const score_data = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
              if (score_data) {
                englishList.push({ name: "ielts academic", "description": score_data });
              }
              if (englishList && englishList.length > 0) {
                courseAdminReq.english = englishList;
              }
              else {
                courseAdminReq.english = [];
              }

              let ieltsNumber = null;
              const pieltsDict = {};
              var penglishList = [];
              if (score_data && score_data.length > 0) {
                var ieltsScore = "";
                if (score_data.toString().indexOf("IELTS") != -1) {
                  ieltsScore = await utils.giveMeNumber(score_data.split("IELTS")[1]);
                }
                else {
                  ieltsScore = await utils.giveMeNumber(score_data);
                }
                // if (ieltsScore != "NA") {
                //   pieltsDict.name = 'ielts academic';
                //   var value = {};
                //   value.min = 0;
                //   value.require = ieltsScore;
                //   value.max = 9;
                //   pieltsDict.value = value;
                //   penglishList.push(pieltsDict);
                // }

                if (ieltsScore) {
                  pieltsDict.name = 'ielts academic';
                  pieltsDict.description = score_data;
                  pieltsDict.min = 0;
                  pieltsDict.require = ieltsScore;
                  pieltsDict.max = 9;
                  pieltsDict.R = 0;
                  pieltsDict.W = 0;
                  pieltsDict.S = 0;
                  pieltsDict.L = 0;
                  pieltsDict.O = 0;
                  penglishList.push(pieltsDict);
                }


              }

              if (penglishList && penglishList.length > 0) {
                courseAdminReq.english = penglishList;
              }
              else {
                // courseAdminReq.englishprogress = [];
              }
              var course_academic_requirement = []
              const courseKeyVal = courseScrappedData.course_academic_requirement;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        course_academic_requirement = selList;
                      }
                    }
                  }
                }
              }
              courseAdminReq.academic = (course_academic_requirement) ? course_academic_requirement : [];
              if (courseAdminReq.english || courseAdminReq.academic) {
                resJsonData.course_admission_requirement = courseAdminReq;
              }

              let resEnglishReqMoreDetailsJson = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);;
              let english_more = await Course.extractValueFromScrappedElement(courseScrappedData.english_more);;
              if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                resJsonData.course_admission_requirement.english_requirements_url = english_more;
                resJsonData.course_admission_requirement.entry_requirements_url = "";
              } else if (resEnglishReqMoreDetailsJson) {
                resJsonData.course_admission_requirement = {};
                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                resJsonData.course_admission_requirement.english_requirements_url = english_more;
                resJsonData.course_admission_requirement.entry_requirements_url = "";
              }
              break;
            }
            case 'course_url': {
              resJsonData.course_url = courseUrl;
              break;
            }
            case 'course_outline': {
              console.log("course_outline--->")
              const outline = {
                majors: [],
                minors: [],
                more_details: ""
              };

              var major, minor
              var intakedata1 = {};
              const courseKeyVal = courseScrappedData.course_outline_major;
              console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

              let resScholarshipJson = null;


              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList;
                        console.log("resScholarshipJson", resScholarshipJson)
                      }
                    }
                  }
                }
              }
              if (resScholarshipJson) {
                major = resScholarshipJson
              } else {
                major = []
              }




              const courseKeyVal1 = courseScrappedData.course_outline_minors;
              let resScholarshipJson1 = null;
              if (Array.isArray(courseKeyVal1)) {
                for (const rootEleDict of courseKeyVal1) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson1 = selList;

                      }
                    }
                  }
                }
              }
              if (resScholarshipJson1) {
                minor = resScholarshipJson1
              } else {
                minor = []
              }

              let m_d = resJsonData.course_url;
              console.log("md--------->", m_d)

              var intakedata1 = {};
              intakedata1.majors = major;
              intakedata1.minors = minor;
              intakedata1.more_details = m_d;
              resJsonData.course_outline = intakedata1;


              break;
            }
            case 'application_fee':
              {
                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                resJsonData.application_fee = ""
                break;
              }

            case 'course_campus_location': {
              // const campLocationText = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
              //console.log(funcName + 'campLocationText = ' + campLocationText);
              var campLocationText; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
              const courseKeyVal = courseScrappedData.course_campus_location;
              const cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      //if (Array.isArray(selList) && selList.length > 0) {
                      campLocationText = selList;

                      ///}
                    }
                  }
                }
              }
              let newcampus = [];
              for (let campus of campLocationText) {
                if (!newcampus.includes(campus)) {
                  newcampus.push(campus);

                }
              }
              console.log("##Campuscampus-->" + newcampus)
              var finallocation = [];
              var local
              for (let f of campLocationText)
                if (String(f).indexOf('\n') > -1) {
                  local = String(f).split("\n");
                  console.log("local----->", local)
                }
                else {
                  local = f
                }
              var loca = String(local).split(',');
              console.log("locaaaa------>", loca)
              loca.forEach(element => {
                if (element.includes('Campus')) {
                  finallocation.push(element.replace('Campus', ''))
                }
                else {

                  finallocation.push(element)
                  console.log("finallocation------->", finallocation)

                }
              })
              var campusss = [];
              if (finallocation.length > 0) {
                console.log("loca------>", finallocation);
                let campuses = ['Ballarat','Brisbane', 'Melbourne', 'North Sydney', 'Canberra', 'Strathfield']
                console.log("@@campuses------->", campuses);

                for (let i = 0; i < finallocation.length; i++) {
                  campuses.forEach(element => {
                    console.log(element.includes(finallocation[i].trim()))
                    if (element.includes(finallocation[i].trim())) {

                      campusss.push(element)

                    }
                  })
                }
                console.log("@@campuses------->", campusss)




                if (campusss && campusss.length > 0) {
                  var campLocationValTrimmed = String(campusss).trim();
                  console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                  if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                    var mylocations = await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                    var mylocations_rem = [];
                    mylocations.forEach(element => {
                      if (element.toLowerCase() != "online" && element.toLowerCase() != "distance") {
                        mylocations_rem.push(element);
                      }

                    });
                    //   resJsonData.course_campus_location = mylocations_rem;
                    // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                    if (campusss && campusss.length > 0) {
                      var campusedata = [];
                      campusss.forEach(element => {
                        campusedata.push({
                          "name": element,
                          "code": cricos

                        })
                      });
                      resJsonData.course_campus_location = campusedata;
                      console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

                    }
                    else {
                      throw new Error("No campus location found.");
                    }
                    resJsonData.course_study_mode = "On campus"
                  }
                }
              } // if (campLocationText && campLocationText.length > 0)
              break;
            }
            case 'course_duration_full_time': {
              var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
              var fullTimeText = "";
              const courseKeyVal = courseScrappedData.course_duration_full_time;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        fullTimeText = selList[0];
                        // console.log("selList111", selList);

                      }
                    }
                  }
                }
              }
              console.log("fultimmmmmmmmmmmmmmm--->", fullTimeText);
              if (fullTimeText && fullTimeText.length > 0) {
                var resFulltime = fullTimeText;
                if (resFulltime) {
                  durationFullTime.duration_full_time = resFulltime;
                  if (resJsonData.course_title == "Bachelor of High Performance Sport") {
                    resFulltime = "3 years full time, or part-time equivalent";
                  }
                  courseDurationList.push(durationFullTime);
                  courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                  let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                  console.log("filtered_duration_formated", filtered_duration_formated);
                  console.log("courseDurationDisplayList2", courseDurationDisplayList);
                  if (courseDurationList && courseDurationList.length > 0) {

                    resJsonData.course_duration = resFulltime;

                  }
                  if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                    resJsonData.course_duration_display = filtered_duration_formated;

                    var isfulltime = false, isparttime = false;
                    filtered_duration_formated.forEach(element => {
                      if (element.display == "Full-Time") {
                        isfulltime = true;
                      }
                      if (element.display == "Part-Time") {
                        isparttime = true;
                      }
                    });
                    resJsonData.isfulltime = isfulltime;
                    resJsonData.isparttime = isparttime;

                  }
                }
              }
              break;
            }
            case 'course_tuition_fee':
            case 'course_study_mode':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':


            case 'course_tuition_fees_international_student': {
              const courseTuitionFee = {};
              const feesList = [];
              const feesDict = {};

              //for first year only
              const feesIntStudent_first_year = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_first_year);
              if (feesIntStudent_first_year.length > 0) { // extract only digits
                console.log("international_student" + feesIntStudent_first_year)
                // feesDict.international_student = feesIntStudent;
                const feesWithDollorTrimmed = String(feesIntStudent_first_year).trim();
                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                console.log(funcName + 'feesVal = ' + feesVal);
                if (feesVal) {
                  const myval = feesVal.split(".");
                  const regEx = /\d/g;
                  let feesValNum = myval[0].match(regEx);
                  if (feesValNum) {
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    feesValNum = feesValNum.join('');
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    let feesNumber = null;
                    if (feesValNum.includes(',')) {
                      feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                    } else {
                      feesNumber = feesValNum;
                    }
                    console.log(funcName + 'feesNumber = ' + feesNumber);
                    if (Number(feesNumber)) {
                      var feesint = {};
                      feesint.amount = Number(feesNumber);
                      feesint.duration = 1;
                      feesint.unit = "year";

                      feesint.description = feesIntStudent_first_year;

                      feesDict.international_student = feesint;
                    }
                  }
                }
              }
              const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
              const courseKeyVal = courseScrappedData.course_tuition_fees_international_student;
              let feesIntStudent = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        feesIntStudent = selList;
                      }
                    }
                  }
                }
              }


              if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }
                if (feesIntStudent.length > 0) { // extract only digits
                  console.log("international_student" + feesIntStudent)
                  //  feesDict.international_student_all_fees = feesIntStudent;
                }
                if (feesDict) {
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                if (courseTuitionFee) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
              }
              break;
            }
            case 'program_code': {
              const courseKeyVal = courseScrappedData.program_code;
              var program_code = []
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        program_code = selList;
                      }
                    }
                  }
                }
              }
              // if (program_code.length > 0) {
              resJsonData.program_code = "";
              //}
              break;
            }

            case 'course_intake': {
              const courseIntakeDisplay = {};
              var courseIntakeStr = [];
              const courseKeyVal = courseScrappedData.course_intake;
              console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        courseIntakeStr = selList;
                      }
                    }
                  }
                }
              }

              var course_intake_checklocation = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake_checklocation);

              console.log("intakes" + JSON.stringify(courseIntakeStr));
              if (courseIntakeStr && courseIntakeStr.length > 0) {
                var campus = resJsonData.course_campus_location;
                var intakes = [];
                console.log("Intake length-->" + courseIntakeStr.length);
                console.log("Campus length-->" + campus.length);
                var intakecount = 0;
                for (var count = 0; count < campus.length; count++) {
                  console.log("intake -->" + count)
                  var intakedetail = {};
                  intakedetail.name = campus[count].name;

                  if (course_intake_checklocation.includes(campus[count] && courseIntakeStr[intakecount])) {
                    intakedetail.value = await utils.giveMeArray(courseIntakeStr[intakecount].replace(/[\r\n\t ]+/g, ' ').trim(), ",");
                    console.log("intakedetail.value@@@", intakedetail.value)

                    intakecount++;
                  }
                  if (intakedetail.value && intakedetail.value.length > 0) {
                    intakes.push(intakedetail);
                  }


                }
                var intakedata = {};
                intakedata.intake = await utils.providemyintake(intakes, "mom", "");
                intakedata.more_details = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake_url);
                resJsonData.course_intake = intakedata;
              }
              break;
            }


            case 'course_study_level': {
              const cStudyLevel = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
              if (cStudyLevel) {

                resJsonData.course_study_level = cStudyLevel;
              } else {
                throw new Error("Study Level not Found");
              }

              break;
            }
            case 'course_country': {
              resJsonData.course_country = await Course.extractValueFromScrappedElement(courseScrappedData.course_country);
              break;
            }
            case 'course_overview': {
              resJsonData.course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
              break;
            }
            case 'course_career_outcome': {
              let assigned = false;
              console.log('course_career_outcome matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log('course_career_outcome key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedSelStr = [];
              for (const rootEleDict of rootElementDictList) {
                console.log('course_career_outcome rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                // let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log('course_career_outcome eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  for (const selList of selectorsList) {
                    if (assigned == false) {
                      if (selList && selList.length > 0) {
                        console.log('course_career_outcome selList = ' + JSON.stringify(selList));

                        for (const selItem of selList) {
                          concatnatedSelStr.push(selItem);
                        }
                        assigned = true;
                      }
                      console.log('course_career_outcome concatnatedSelStr = ' + concatnatedSelStr);

                    }
                  } // selectorsList                  
                } // elementsList                
              } // rootElementDictList 

              // add only if it has valid value              
              if (concatnatedSelStr && concatnatedSelStr.length > 0) {
                resJsonData.course_career_outcome = concatnatedSelStr;
              } else {
                resJsonData.course_career_outcome = [];
              }

              break;
            }

            case 'course_title': {

              let title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
              console.log("title@@@2" + title);
              let title_text;
              if (title.includes('/')) {
                title_text = title.replace('/', ' / ');
                title = title_text;
                console.log("splitStr@@@2" + title);

              }
              if (title.includes('(')) {
                title_text = title.replace('(', ' ( ');
                title = title_text;
                console.log("splitStr@@@2" + title);

              }
              var ctitle = format_functions.titleCase(title)
              console.log("ctitle@@@", ctitle)
              resJsonData.course_title = ctitle
              break;
            }

            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        }
      } // for (const key in courseScrappedData)
      var NEWJSONSTRUCT = {}, structDict = [];
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        }
        else {
          NEWJSONSTRUCT.course_intake = "";
        }
        for (let myfees of resJsonData.course_tuition_fee.fees) {
          if (myfees.name == location) {
            NEWJSONSTRUCT.international_student_all_fees = myfees;
          }
        }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        resJsonData.course_id = location_wise_data.course_location_id;

        resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

      }
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }


  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = 'scrapeAndFormatCourseDetails ';
    console.log("element--->" + JSON.stringify(courseDict));
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);

      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }
      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file
      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }
      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, await s.page.url(), courseDict, courseDict.category);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
