// const fs = require('fs');
// const Scrape = require('./common/scrape').Scrape;
// const utils = require('./common/utils');
// const configs = require('./configs');
// const appConfigs = require('./common/app-config');

// class ScrapeCourseList extends Scrape {

//   async scrapeOnlyInternationalCourseList(selFilepath) {
//     var elementstring = "", elementhref = "", allcategory = [], outcome = [];
//     const funcName = 'scrapeCoursePageList ';
//     let s = null;
//     try {
//       // validate params
//       Scrape.validateParams([selFilepath]);
//       // if should read from local file
//       if (appConfigs.shouldTakeCourseListFromOutputFolder) {
//         const fileData = fs.readFileSync(configs.opCourseListFilepath);
//         if (!fileData) {
//           throw (new Error('Invalif file data, fileData = ' + fileData));
//         }
//         const dataJson = JSON.parse(fileData);
//         console.log(funcName + 'Success in getting local data so returning local data....');
//         // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
//         return dataJson;
//       }
//       // load file
//       const fileData = fs.readFileSync(selFilepath);
//       this.selectorJson = JSON.parse(fileData);
//       // create Scrape object
//       s = new Scrape();
//       await s.init({ headless: false });
//       // course_level_selector key
//       const courseListSel = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
//       if (!courseListSel) {
//         console.log(funcName + 'Invalid courseListSel');
//         throw (new Error('Invalid courseListSel'));
//       }
//       const elementDictForCourseListSel = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
//       if (!elementDictForCourseListSel) {
//         console.log(funcName + 'Invalid elementDictForCourseListSel');
//         throw (new Error('Invalid elementDictForCourseListSel'));
//       }
//       const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
//       console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
//       // set page to url
//       await s.setupNewBrowserPage(rootEleDictUrl);
//       const selectorcard = "#card-no-image";
//       const targetLinksCards = await s.page.$$(selectorcard);
//       var targetLinksCardsTotal = [];
//       for (let link of targetLinksCards) {
//         elementstring = await s.page.evaluate(el => el.innerText, link);
//         elementhref = await s.page.evaluate(el => el.href, link);
//         targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
//       }

//       await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
//       console.log(funcName + 'writing courseList to file completed successfully....');
//       console.log("#### main courses---" + targetLinksCardsTotal.length);

//       var totalCourseList = [];
//       var mainCategories = JSON.parse(await fs.readFileSync("./output/main_category_courses.json"));
//       for (let target of mainCategories) {
//         await s.page.goto(target.href, { timeout: 0 });
//         console.log("Proccessing-->" + target.innerText);

//         const textselector = "//*[@id='courses-search-results']//a[@class='cta cta--red']/ancestor-or-self::div[@class='row']//h5";
//         const linkselector = "//*[@id='courses-search-results']//a[@class='cta cta--red']/preceding-sibling::input";
//         var click = await s.page.$x("//*//label[contains(text(),'International')]");
//         if (click[0]) {
//           await click[0].click();
//         }

//         await s.page.waitFor(5000);
//         const selector = "#courses-search-results > section.search-results-scholarships.desktop-width > div.text-center > button";
//         await utils.checkandclick(s.page, selector);
//         const targetLinks = await s.page.$x(linkselector);
//         const targettextselector = await s.page.$x(textselector);
//         console.log("text length -- " + targetLinks.length + " -- " + targettextselector.length + " -- " + ((targetLinks.length == targettextselector.length) ? "true" : "false"));
//         const courselen = targetLinks.length;

//         var outcomeselector = "//section[@class='career_outcomes']//ul/li";
//         var outcome = [];
//         const outcomevalue = await s.page.$x(outcomeselector);
//         for (let out of outcomevalue) {
//           var outelementstring = await s.page.evaluate(el => el.innerText, out);
//           outcome.push(outelementstring);
//         }
//         for (var i = 0; i < courselen; i++) {
//           const elementstring = await s.page.evaluate(e => e.innerText, targettextselector[i]);
//           const elementlink = await s.page.evaluate(e => e.value, targetLinks[i]);
//           console.log("elementlink --> ", elementlink);
//           totalCourseList.push({ href: elementlink, innerText: elementstring, category: target.innerText, outcome: outcome });
//         }
//       }
//       if (s) {
//         await s.close();
//       }
//       await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
//       console.log(funcName + 'writing courseList to file completed successfully....');
//       return totalCourseList;
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       // destroy scrape resources
//       if (s) {
//         await s.close();
//       }
//       throw (error);
//     }
//   }



//   // refers to http://www.utas.edu.au/courses
//   async scrapeCourseList(selFilepath) {
//     const funcName = 'scrapeCoursePageList ';
//     let s = null;
//     try {
//       // validate params
//       Scrape.validateParams([selFilepath]);
//       // load file
//       const fileData = fs.readFileSync(selFilepath);
//       this.selectorJson = JSON.parse(fileData);
//       // course_list selector
//       const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
//       if (!selectorsList) {
//         console.log(funcName + 'Invalid selectorsList');
//         throw (new Error('Invalid selectorsList'));
//       }
//       const sel = selectorsList[0];
//       console.log(funcName + 'sel = ' + JSON.stringify(sel));

//       // create Scrape object
//       s = new Scrape();
//       await s.init({ headless: true });
//       const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
//       if (!elementDict) {
//         console.log(funcName + 'Invalid elementDict');
//         throw (new Error('Invalid elementDict'));
//       }
//       const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
//       if (!rootEleDictUrl) {
//         console.log(funcName + 'Invalid rootEleDictUrl');
//         throw (new Error('Invalid rootEleDictUrl'));
//       }
//       const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
//       console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

//       // course_level_selector key
//       const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
//       if (!coursLvlSelList) {
//         console.log(funcName + 'Invalid coursLvlSelList');
//         throw (new Error('Invalid coursLvlSelList'));
//       }
//       const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
//       if (!elementDictForCorsLvlKey) {
//         console.log(funcName + 'Invalid elementDict');
//         throw (new Error('Invalid elementDict'));
//       }

//       let totalCourseList = []; let count = 1;
//       for (const catDict of courseCatDictList) {
//         console.log(funcName + 'count = ' + count);
//         // click on href
//         console.log(funcName + 'catDict.href = ' + catDict.href);
//         await s.setupNewBrowserPage(catDict.href);
//         const selItem = coursLvlSelList[0];
//         console.log(funcName + 'selItem = ' + selItem);
//         const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
//         console.log(funcName + 'res = ' + JSON.stringify(res));
//         if (Array.isArray(res)) {
//           totalCourseList = totalCourseList.concat(res);
//         } else {
//           throw (new Error('res is not array, it must be...'));
//         }

//         count += 1;
//       } // for (const catDict of courseDictList)
//       console.log(funcName + 'writing courseList to file....');
//       // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
//       await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
//       console.log(funcName + 'writing courseList to file completed successfully....');

//       // destroy scrape resources
//       if (s) {
//         await s.close();
//       }
//       return null;
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       // destroy scrape resources
//       if (s) {
//         await s.close();
//       }
//       throw (error);
//     }
//   }
// } // class

// module.exports = { ScrapeCourseList };

const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {

  async scrapeOnlyInternationalCourseList(selFilepath) {
    var elementstring = "", elementhref = "", allcategory = [], outcome = [];
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
  //    Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      // course_level_selector key
      const courseListSel = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!courseListSel) {
        console.log(funcName + 'Invalid courseListSel');
        throw (new Error('Invalid courseListSel'));
      }
      const elementDictForCourseListSel = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCourseListSel) {
        console.log(funcName + 'Invalid elementDictForCourseListSel');
        throw (new Error('Invalid elementDictForCourseListSel'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      const selectorcard = "#card-no-image";
      const targetLinksCards = await s.page.$$(selectorcard);
      var targetLinksCardsTotal = [];
      for (let link of targetLinksCards) {
        elementstring = await s.page.evaluate(el => el.innerText, link);
        elementhref = await s.page.evaluate(el => el.href, link);
        targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
        console.log("Href@@@@@@@@@",targetLinksCardsTotal)
      }

      await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
      console.log(funcName + 'writing courseList to file completed successfully....');
      console.log("#### main courses---" + targetLinksCardsTotal.length);

      var totalCourseList = [];
      var mainCategories = JSON.parse(await fs.readFileSync("./output/main_category_courses.json"));
      //       var mainCategories = JSON.parse(await fs.readFileSync("./output/main_category_courses.json"));

      for (let target of mainCategories) {
        await s.page.goto(target.href, { timeout: 0 });
        console.log("Proccessing-->" + target.innerText);

        const textselector = "//*[@id='courses-search-results']//a[@class='cta cta--red']/ancestor-or-self::div[@class='row']//h5";
        const linkselector = "//*[@id='courses-search-results']/section/div/div/section/div/div/input";
        var click = await s.page.$x("//*//label[contains(text(),'International')]");
        if (click[0]) {
          await click[0].click();
        }

        await s.page.waitFor(5000);
        const selector = "#courses-search-results > section.search-results-scholarships.desktop-width > div.text-center > button";
        await utils.checkandclick(s.page, selector);
        const targetLinks = await s.page.$x(linkselector);
        const targettextselector = await s.page.$x(textselector);
        console.log("text length -- " + targetLinks.length + " -- " + targettextselector.length + " -- " + ((targetLinks.length == targettextselector.length) ? "true" : "false"));
        const courselen = targetLinks.length;

        var outcomeselector = "//section[@class='career_outcomes']//ul/li";
        var outcome = [];
        const outcomevalue = await s.page.$x(outcomeselector);
        for (let out of outcomevalue) {
          var outelementstring = await s.page.evaluate(el => el.innerText, out);
          outcome.push(outelementstring);
        }
        for (var i = 0; i < courselen; i++) {
          const elementstring = await s.page.evaluate(e => e.innerText, targettextselector[i]);
          const elementlink = await s.page.evaluate(e => e.value, targetLinks[i]);
          totalCourseList.push({ href: elementlink, innerText: elementstring, category: target.innerText, outcome: outcome });
        }
      }

      await fs.writeFileSync("./output/australiancatholicuniversity_original_courselist.json", JSON.stringify(totalCourseList));



      
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: []});
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: []});
        }
      }

      await fs.writeFileSync("./output/australiancatholicuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            uniqueUrl[j].category.push(totalCourseList[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/australiancatholicuniversity_courselist.json", JSON.stringify(uniqueUrl));
      if (s) {
        await s.close();
      }
      //await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));

      console.log(funcName + 'writing courseList to file completed successfully....');
      return uniqueUrl;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }



  // refers to http://www.utas.edu.au/courses
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
     // Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }

        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');

      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
