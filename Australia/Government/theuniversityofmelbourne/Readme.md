**The University Of Melbourne: Total Courses Details As On 15 April 2019**
* Total Courses = 783
* Courses without CRICOS code = 
* Total available courses = 
* Repeated = 328

**Notes**
* A lot of courses are repeated but they belong to different categories. And ielts score changes according to category.
* Custom ielts mapping.
* Extreme variation in academic requirement. Hence static line added with link to academic requirement of that course.
* Program code NA
* study mode NA. Hence if cricos code is found than study mode is set to on campus.
* the link for scholarship is dynamically added in scrapeCourse.js in the case of course_scholarship. Dynamic url created by taking course url and appending "/fees/#scholarships" to it.


**2019 & conditions**
* 2019 is statically added after month in scrapeCourse.js course_intake.



scrape_by_xpath

 "#fees > div > div > div > div.panel__body > div > div > div:nth-child(1) > ul > li:nth-child(2) > span.fee-item__price.heading-card",
"#ui-toggle-101-panel-1 > div > div > div.fee-info-panel__fees > div:nth-child(1) > ul > li:nth-child(2) > span.fee-item__price",
 "#ui-toggle-104-panel-1 > div > div > div.fee-info-panel__fees > div:nth-child(1) > ul > li:nth-child(2) > span.fee-item__price"