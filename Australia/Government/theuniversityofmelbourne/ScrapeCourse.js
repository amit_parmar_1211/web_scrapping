const fs = require('fs');
const awsUtil = require('./common/aws_utils');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
//const mapping = require('./output/Mapping/main');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name, study_level) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = category_name;
                            break;
                        }
                        case 'program_code': {
                            let code = "";
                            resJsonData.program_code = "";
                            break;
                        }
                        case 'course_toefl_ielts_score': {
                            //CUSTOMIZED MAPPING CODE DONT COPY            
                            var courseAdminReq = {
                                english: []
                            };
                            var englishList = [];
                            var ieltsDict = {};
                            var pteDict = {};
                            var tofelDict = {};
                          
                            var pbtDict = {};
                            var caeDict = {};
                            let ieltsNumber = null;

                            // english requirement  
                            var ieltsExtracted = [];
                            const study_level = resJsonData.course_study_level;
                            const title11 = resJsonData.course_title;
                            const title = title11.toLowerCase();
                            console.log("title -->", title);
                            var multipleCategory = resJsonData.course_discipline;
                            console.log("multipleCategory -->", multipleCategory);
                            // const ieltsMapping = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);   
                            console.log('course_toefl_ielts_score courseScrappedData.course_toefl_ielts_score[0].elements[0].selectors[0][0]: ' + JSON.stringify(courseScrappedData.course_toefl_ielts_score[0].elements[1].selectors[0]));
                            if (courseScrappedData.course_toefl_ielts_score[0].elements[1].selectors[0] && courseScrappedData.course_toefl_ielts_score[0].elements[1].selectors[0].toString().toLowerCase().indexOf("ielts") > -1 && courseScrappedData.course_toefl_ielts_score[0].elements[1].selectors[0].toString().toLowerCase().indexOf("toefl") > -1) {
                                ieltsExtracted = courseScrappedData.course_toefl_ielts_score[0].elements[1].selectors[0];
                                console.log('course_toefl_ielts_score ieltsExtracted = ' + ieltsExtracted[0]);
                                console.log('course_toefl_ielts_score ieltsExtracted = ' + ieltsExtracted[1]);
                                console.log('course_toefl_ielts_score ieltsExtracted = ' + ieltsExtracted[2]);
                                console.log('course_toefl_ielts_score ieltsExtracted4 = ' + ieltsExtracted[4]);
                                console.log('course_toefl_ielts_score ieltsExtracted3 = ' + ieltsExtracted[3]);

                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ieltsExtracted[0].split(":")[1].trim();
                                englishList.push(ieltsDict);

                                // pbtDict.name = 'toefl pbt';
                                // pbtDict.description = ieltsExtracted[1].split(":")[1].trim();
                                // englishList.push(pbtDict);

                                tofelDict.name = 'toefl ibt';
                                tofelDict.description = ieltsExtracted[2].split(":")[1].trim();
                                englishList.push(tofelDict);

                                if (ieltsExtracted.includes("TOEFL (computer-based)")) {
                                    console.log("YESSSSS")
                                    pteDict.name = 'pte academic';
                                    pteDict.description = ieltsExtracted[3].split(":")[1].trim();
                                    englishList.push(pteDict);
                                }
                                else if (ieltsExtracted[3]) {
                                    pteDict.name = 'pte academic';
                                    if(ieltsExtracted[4] != undefined){
                                    pteDict.description = ieltsExtracted[4].split(":")[1].trim();
                                    englishList.push(pteDict);
                                    }
                                  else{
                                    pteDict.description = "";
                                    englishList.push(pteDict);
                                  }
                                }
                                console.log("English@@@@@", englishList)
                                console.log("INif", ieltsExtracted.includes("TOEFL (computer-based):"))

                                courseAdminReq.english = englishList;
                            }
                            else {
                                console.log("inelse")
                                if (study_level && study_level.length > 0) {
                                    const ieltsMappingDict = JSON.parse(fs.readFileSync("./selectors/ielts_mapping.json"));
                                    //console.log("R ieltsMappingDict", JSON.stringify(ieltsMappingDict[1]));                                
                                    if (study_level == "Under Graduate" || study_level == "Bachelor") {
                                        //underGrad
                                        ieltsDict.name = 'ielts academic';
                                        ieltsDict.description = ieltsMappingDict[1].underGraduate[0].all[0].programs.ielts;
                                        englishList.push(ieltsDict);

                                        // pbtDict.name = 'toefl pbt';
                                        // pbtDict.description = ieltsMappingDict[1].underGraduate[0].all[0].programs.toefl_pbt;
                                        // englishList.push(pbtDict);

                                        tofelDict.name = 'toefl ibt';
                                        tofelDict.description = ieltsMappingDict[1].underGraduate[0].all[0].programs.toefl_ibt;
                                        englishList.push(tofelDict);

                                        pteDict.name = 'pte academic';
                                        pteDict.description = ieltsMappingDict[1].underGraduate[0].all[0].programs.pte;
                                        englishList.push(pteDict);

                                        caeDict.name = 'cae';
                                        caeDict.description = ieltsMappingDict[1].underGraduate[0].all[0].programs.cae;
                                        englishList.push(caeDict);
                                        courseAdminReq.english = englishList;
                                    }
                                    else {
                                        console.log("In else of else")
                                        //postGrad and research
                                        for (let category of multipleCategory) {
                                            console.log("category -->", category);
                                            switch (category.trim()) {
                                                case "Architecture, building, planning and design": {
                                                    console.log("ENTERED Architecture, building, planning and design");
                                                    ieltsDict = {}, tofelDict = {}, pteDict = {}, caeDict = {};
                                                    //SchoolOfDesign
                                                    if (title.indexOf("philosophy") > -1) {
                                                        //for philosophy
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[0].SchoolOfDesign[0].MasterDoctor_Philosophy.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        // pbtDict.name = 'toefl pbt (' + category.trim() + ")";
                                                        // pbtDict.description = ieltsMappingDict[0].Graduate[0].SchoolOfDesign[0].MasterDoctor_Philosophy.toefl_pbt;
                                                        // courseAdminReq.english.push(pbtDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[0].SchoolOfDesign[0].MasterDoctor_Philosophy.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[0].SchoolOfDesign[0].MasterDoctor_Philosophy.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[0].SchoolOfDesign[0].MasterDoctor_Philosophy.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else {
                                                        console.log("ENTERED");
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[0].SchoolOfDesign[0].programs.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        // pbtDict.name = 'toefl pbt (' + category.trim() + ")";
                                                        // pbtDict.description = ieltsMappingDict[0].Graduate[0].SchoolOfDesign[0].programs.toefl_pbt;
                                                        // courseAdminReq.english.push(pbtDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[0].SchoolOfDesign[0].programs.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[0].SchoolOfDesign[0].programs.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[0].SchoolOfDesign[0].programs.cae;
                                                        courseAdminReq.english.push(caeDict);
                                                    }
                                                    console.log("JSOn courseAdminReq.english --> ", JSON.stringify(courseAdminReq.english));
                                                    break;
                                                }
                                                case "Arts, humanities and social sciences": {
                                                    //FacultyOfArts
                                                    ieltsDict = {}, tofelDict = {}, pteDict = {}, caeDict = {};
                                                    if (title.indexOf("publishing and communications") > -1 || title.indexOf("creative writing, publishing and editing") > -1 || title.indexOf("journalism (advanced)") > -1 || title.indexOf("of journalism") > -1) {
                                                        //Master of Publishing and Communications; Master of Creative Writing,Publishing and Editing; Master of Journalism; Graduate Diploma/Certificate in Publishing and Communications (Advanced); Graduate Diploma/Certificate in Journalism (Advanced)
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].MasterDiploma_PublishingCommunication_MasterDiploma_journalism_MasterCreativeWriting.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        // pbtDict.name = 'toefl pbt (' + category.trim() + ")";
                                                        // pbtDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].MasterDiploma_PublishingCommunication_MasterDiploma_journalism_MasterCreativeWriting.toefl_pbt;
                                                        // courseAdminReq.english.push(pbtDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].MasterDiploma_PublishingCommunication_MasterDiploma_journalism_MasterCreativeWriting.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].MasterDiploma_PublishingCommunication_MasterDiploma_journalism_MasterCreativeWriting.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].MasterDiploma_PublishingCommunication_MasterDiploma_journalism_MasterCreativeWriting.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title.indexOf("philosophy") > -1 || study_level == "research") {
                                                        //Master of Publishing and Communications; Master of Creative Writing,Publishing and Editing; Master of Journalism; Graduate Diploma/Certificate in Publishing and Communications (Advanced); Graduate Diploma/Certificate in Journalism (Advanced)
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].MasterResearch_DoctorPhilosophy.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        // pbtDict.name = 'toefl pbt (' + category.trim() + ")";
                                                        // pbtDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].MasterResearch_DoctorPhilosophy.toefl_pbt;
                                                        // courseAdminReq.english.push(pbtDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].MasterResearch_DoctorPhilosophy.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].MasterResearch_DoctorPhilosophy.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].MasterResearch_DoctorPhilosophy.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else {
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].programs.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        // pbtDict.name = 'toefl pbt (' + category.trim() + ")";
                                                        // pbtDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].programs.toefl_pbt;
                                                        // courseAdminReq.english.push(pbtDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].programs.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].programs.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[1].FacultyOfArts[0].programs.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    break;
                                                }
                                                case "Business and economics": {
                                                    ieltsDict = {}, tofelDict = {}, pteDict = {}, caeDict = {};
                                                    //School of Business
                                                    if (title == "master of business analytics") {
                                                        //Master of Business Analytics
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].analytics.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        // pbtDict.name = 'toefl pbt (' + category.trim() + ")";
                                                        // pbtDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].analytics.toefl_pbt;
                                                        // courseAdminReq.english.push(pbtDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].analytics.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].analytics.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].analytics.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title == "master of business administration" || title == "master of marketing") {
                                                        //Master of Business Administration && Master of Marketing
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].administration_marketing.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        // pbtDict.name = 'toefl pbt (' + category.trim() + ")";
                                                        // pbtDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].administration_marketing.toefl_pbt;
                                                        // courseAdminReq.english.push(pbtDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].administration_marketing.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].administration_marketing.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].administration_marketing.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title == "master of entrepreneurship") {
                                                        //Master of Entrepreneurship
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].entrepreneurship.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        // pbtDict.name = 'toefl pbt (' + category.trim() + ")";
                                                        // pbtDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].entrepreneurship.toefl_pbt;
                                                        // courseAdminReq.english.push(pbtDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].entrepreneurship.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].entrepreneurship.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].entrepreneurship.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title.indexOf("master of commerce") > -1 || title.indexOf("master of philosophy") > -1) {
                                                        //Master of Commerce && Master of Philosophy
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].master_philosophy.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        // pbtDict.name = 'toefl pbt (' + category.trim() + ")";
                                                        // pbtDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].master_philosophy.toefl_pbt;
                                                        // courseAdminReq.english.push(pbtDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].master_philosophy.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].master_philosophy.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].master_philosophy.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title.indexOf("doctor of philosophy") > -1) {
                                                        //Doctor of Philosophy
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].doctor_philosophy.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        // pbtDict.name = 'toefl pbt (' + category.trim() + ")";
                                                        // pbtDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].doctor_philosophy.toefl_pbt;
                                                        // courseAdminReq.english.push(pbtDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].doctor_philosophy.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].doctor_philosophy.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].doctor_philosophy.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else {
                                                        //all others
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].programs.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        // pbtDict.name = 'toefl pbt (' + category.trim() + ")";
                                                        // pbtDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].programs.toefl_pbt;
                                                        // courseAdminReq.english.push(pbtDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].programs.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].programs.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[2].BusinessSchool[0].programs.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    break;
                                                }
                                                case "Education": {
                                                    ieltsDict = {}, tofelDict = {}, pteDict = {}, caeDict = {};
                                                    //School of Education
                                                    if (title == "master of educational psychology" || title.indexOf("master of teaching") > -1) {
                                                        //Master of Teaching (All streams) && Master of Educational Psychology	
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[3].SchoolOfEducation[0].teaching_psychology.ielts;
                                                        courseAdminReq.english.push(ieltsDict);


                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[3].SchoolOfEducation[0].teaching_psychology.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[3].SchoolOfEducation[0].teaching_psychology.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[3].SchoolOfEducation[0].teaching_psychology.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else {
                                                        //all others
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[3].SchoolOfEducation[0].programs.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                      

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[3].SchoolOfEducation[0].programs.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[3].SchoolOfEducation[0].programs.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[3].SchoolOfEducation[0].programs.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    break;
                                                }
                                                case "Engineering": {
                                                    ieltsDict = {}, tofelDict = {}, pteDict = {}, caeDict = {};
                                                    //School of Engineering
                                                    //all
                                                    ieltsDict = {}, pbtDict = {}, tofelDict = {}, pteDict = {}, caeDict = {};
                                                    ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                    ieltsDict.description = ieltsMappingDict[0].Graduate[4].SchoolOfEngineering_IT[0].programs.ielts;
                                                    courseAdminReq.english.push(ieltsDict);


                                                    tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                    tofelDict.description = ieltsMappingDict[0].Graduate[4].SchoolOfEngineering_IT[0].programs.toefl_ibt;
                                                    courseAdminReq.english.push(tofelDict);

                                                    pteDict.name = 'pte academic (' + category.trim() + ")";
                                                    pteDict.description = ieltsMappingDict[0].Graduate[4].SchoolOfEngineering_IT[0].programs.pte;
                                                    courseAdminReq.english.push(pteDict);

                                                    caeDict.name = 'cae (' + category.trim() + ")";
                                                    caeDict.description = ieltsMappingDict[0].Graduate[4].SchoolOfEngineering_IT[0].programs.cae;
                                                    courseAdminReq.english.push(caeDict);

                                                    break;
                                                }
                                                case "Environment": {
                                                    ieltsDict = {},  tofelDict = {}, pteDict = {}, caeDict = {};
                                                    //Faculty of Science
                                                    //all others
                                                    ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                    ieltsDict.description = ieltsMappingDict[0].Graduate[8].FacultyOfScience[0].programs.ielts;
                                                    courseAdminReq.english.push(ieltsDict);

                                                   

                                                    tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                    tofelDict.description = ieltsMappingDict[0].Graduate[8].FacultyOfScience[0].programs.toefl_ibt;
                                                    courseAdminReq.english.push(tofelDict);

                                                    pteDict.name = 'pte academic (' + category.trim() + ")";
                                                    pteDict.description = ieltsMappingDict[0].Graduate[8].FacultyOfScience[0].programs.pte;
                                                    courseAdminReq.english.push(pteDict);

                                                    caeDict.name = 'cae (' + category.trim() + ")";
                                                    caeDict.description = ieltsMappingDict[0].Graduate[8].FacultyOfScience[0].programs.cae;
                                                    courseAdminReq.english.push(caeDict);

                                                    break;
                                                }
                                                case "Health": {
                                                    ieltsDict = {}, tofelDict = {}, pteDict = {}, caeDict = {};
                                                    //Faculty of Medicine,Dentistry and Health Sciences
                                                    if (title.indexOf("clinical dentistry") > -1) {
                                                        //Doctor of Clinical Dentistry && Graduate Diploma in Clinical Dentistry (Implants)                                                
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].dentistry.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                      
                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].dentistry.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].dentistry.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].dentistry.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title == "doctor of dental surgery" || title == "doctor of medicine" || title == "doctor of physiotherapy" || title == "master of genetic counselling") {
                                                        //Doctor of Dental Surgery && Doctor of Medicine && Doctor of Physiotherapy && Master of Genetic Counselling                                              
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].DoctorDental_DoctorMedicine_DoctorPhysiotherapy_MasterGenetic.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                       

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].DoctorDental_DoctorMedicine_DoctorPhysiotherapy_MasterGenetic.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].DoctorDental_DoctorMedicine_DoctorPhysiotherapy_MasterGenetic.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].DoctorDental_DoctorMedicine_DoctorPhysiotherapy_MasterGenetic.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title == "doctor of optometry") {
                                                        //Doctor of Optometry	
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].DoctorOptometry.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                      

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].DoctorOptometry.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].DoctorOptometry.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].DoctorOptometry.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title == "master of clinical audiology") {
                                                        //Master of Clinical Audiology
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].ClinicalAudiology.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                       

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].ClinicalAudiology.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].ClinicalAudiology.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].ClinicalAudiology.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title.indexOf("clinical education") > -1 || title.indexOf("clinical ultrasound") > -1 || title.indexOf("surgical education") > -1) {
                                                        //Master of Clinical Education# && Master of Clinical Ultrasound# && Master in Surgical Education# && Graduate Diploma in Clinical Education# && Graduate Certificate/Diploma in Clinical Ultrasound# && Graduate Certificate/Diploma in Surgical Education#
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].ClinicalEducation_ClinicalUltrsound_SurgicalEducation.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                      

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].ClinicalEducation_ClinicalUltrsound_SurgicalEducation.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].ClinicalEducation_ClinicalUltrsound_SurgicalEducation.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].ClinicalEducation_ClinicalUltrsound_SurgicalEducation.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title == "master of medicine (radiology)") {
                                                        //Master of Medicine (Radiology)
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterMedicine.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                      
                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterMedicine.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterMedicine.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterMedicine.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title == "master of nursing science") {
                                                        //Master of Nursing Science
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterNursingSci.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                       

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterNursingSci.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterNursingSci.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterNursingSci.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title == "master of psychiatry") {
                                                        //Master of Psychiatry
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterPsychiatry.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterPsychiatry.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterPsychiatry.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterPsychiatry.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title.indexOf("psychology (clinical psychology)") > -1 || title.indexOf("psychology (clinical neuropsychology)") > -1) {
                                                        //Master of Psychology (Clinical Psychology) && Master of Psychology (Clinical Neuropsychology) && Master of Psychology (Clinical Psychology)/Doctor of Philosophy && Master of Psychology (Clinical Neuropsychology)/Doctor of Philosophy
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].Psychology_Neuropsychology.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                      

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].Psychology_Neuropsychology.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].Psychology_Neuropsychology.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].Psychology_Neuropsychology.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title == "master of social work" || title == "master of speech pathology" || title == "master of sports medicine" || title == "master of rehabilitation science") {
                                                        //Master of Social Work && Master of Speech Pathology && Master of Sports Medicine && Master of Rehabilitation Science
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterSocialWork_SpeechPathology_SportsMedicine_RehabilitationScience.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                       
                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterSocialWork_SpeechPathology_SportsMedicine_RehabilitationScience.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterSocialWork_SpeechPathology_SportsMedicine_RehabilitationScience.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MasterSocialWork_SpeechPathology_SportsMedicine_RehabilitationScience.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (study_level == "Research") {
                                                        //All Masters by Research && All Research Doctorates
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MastersDoctoratesResearch.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                       

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MastersDoctoratesResearch.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MastersDoctoratesResearch.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].MastersDoctoratesResearch.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else {
                                                        //All others
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].programs.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                       
                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].programs.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].programs.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[7].FacultyOfMedicine[0].programs.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    break;
                                                }
                                                case "Information Technology and Computer Science": {
                                                    //School of Engineering
                                                    //all
                                                    ieltsDict = {}, tofelDict = {}, pteDict = {}, caeDict = {};
                                                    ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                    ieltsDict.description = ieltsMappingDict[0].Graduate[4].SchoolOfEngineering_IT[0].programs.ielts;
                                                    courseAdminReq.english.push(ieltsDict);

                                                  
                                                    tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                    tofelDict.description = ieltsMappingDict[0].Graduate[4].SchoolOfEngineering_IT[0].programs.toefl_ibt;
                                                    courseAdminReq.english.push(tofelDict);

                                                    pteDict.name = 'pte academic (' + category.trim() + ")";
                                                    pteDict.description = ieltsMappingDict[0].Graduate[4].SchoolOfEngineering_IT[0].programs.pte;
                                                    courseAdminReq.english.push(pteDict);

                                                    caeDict.name = 'cae (' + category.trim() + ")";
                                                    caeDict.description = ieltsMappingDict[0].Graduate[4].SchoolOfEngineering_IT[0].programs.cae;
                                                    courseAdminReq.english.push(caeDict);

                                                    break;
                                                }
                                                case "Law": {
                                                    //Law School
                                                    ieltsDict = {}, tofelDict = {}, pteDict = {}, caeDict = {};
                                                    if (title.indexOf("philosophy") > -1) {
                                                        //Master of Philosophy  && Doctor of Philosophy (PhD)	
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[5].LawSchool[0].philosophy.ielts;
                                                        courseAdminReq.english.push(ieltsDict);


                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[5].LawSchool[0].philosophy.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[5].LawSchool[0].philosophy.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[5].LawSchool[0].philosophy.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else if (title.indexOf("jd (juris doctor)") > -1) {
                                                        //JD (Juris Doctor)
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[5].LawSchool[0].juris_doctor.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                      

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[5].LawSchool[0].juris_doctor.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[5].LawSchool[0].juris_doctor.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[5].LawSchool[0].juris_doctor.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else {
                                                        //All others
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[5].LawSchool[0].programs.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                       

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[5].LawSchool[0].programs.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[5].LawSchool[0].programs.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[5].LawSchool[0].programs.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    break;
                                                }
                                                case "Music and visual and performing arts":
                                                case "Music, visual and performing arts": {
                                                    //FacultyOfFineArts
                                                    //All programs
                                                    ieltsDict = {}, tofelDict = {}, pteDict = {}, caeDict = {};
                                                    ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                    ieltsDict.description = ieltsMappingDict[0].Graduate[10].FacultyOfFineArts[0].programs.ielts;
                                                    courseAdminReq.english.push(ieltsDict);

                                                  

                                                    tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                    tofelDict.description = ieltsMappingDict[0].Graduate[10].FacultyOfFineArts[0].programs.toefl_ibt;
                                                    courseAdminReq.english.push(tofelDict);

                                                    pteDict.name = 'pte academic (' + category.trim() + ")";
                                                    pteDict.description = ieltsMappingDict[0].Graduate[10].FacultyOfFineArts[0].programs.pte;
                                                    courseAdminReq.english.push(pteDict);

                                                    caeDict.name = 'cae (' + category.trim() + ")";
                                                    caeDict.description = ieltsMappingDict[0].Graduate[10].FacultyOfFineArts[0].programs.cae;
                                                    courseAdminReq.english.push(caeDict);

                                                    break;
                                                }
                                                case "Science": {
                                                    //Faculty of Science
                                                    //all others
                                                    ieltsDict = {}, tofelDict = {}, pteDict = {}, caeDict = {};
                                                    ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                    ieltsDict.description = ieltsMappingDict[0].Graduate[8].FacultyOfScience[0].programs.ielts;
                                                    courseAdminReq.english.push(ieltsDict);

                                                  
                                                    tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                    tofelDict.description = ieltsMappingDict[0].Graduate[8].FacultyOfScience[0].programs.toefl_ibt;
                                                    courseAdminReq.english.push(tofelDict);

                                                    pteDict.name = 'pte academic (' + category.trim() + ")";
                                                    pteDict.description = ieltsMappingDict[0].Graduate[8].FacultyOfScience[0].programs.pte;
                                                    courseAdminReq.english.push(pteDict);

                                                    caeDict.name = 'cae (' + category.trim() + ")";
                                                    caeDict.description = ieltsMappingDict[0].Graduate[8].FacultyOfScience[0].programs.cae;
                                                    courseAdminReq.english.push(caeDict);

                                                    break;
                                                }
                                                case "Veterinary, agricultural and food sciences": {
                                                    //Faculty Of Veterinary, agricultural and food sciences
                                                    ieltsDict = {}, tofelDict = {}, pteDict = {}, caeDict = {};
                                                    if (title == "doctor of veterinary medicine") {
                                                        //Doctor of Veterinary Medicine
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[9].FacultyOf_Vet_Agri_Sci[0].DoctorVetMedicine.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[9].FacultyOf_Vet_Agri_Sci[0].DoctorVetMedicine.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[9].FacultyOf_Vet_Agri_Sci[0].DoctorVetMedicine.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[9].FacultyOf_Vet_Agri_Sci[0].DoctorVetMedicine.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    else {
                                                        //All others
                                                        ieltsDict.name = 'ielts academic (' + category.trim() + ")";
                                                        ieltsDict.description = ieltsMappingDict[0].Graduate[9].FacultyOf_Vet_Agri_Sci[0].programs.ielts;
                                                        courseAdminReq.english.push(ieltsDict);

                                                       
                                                        tofelDict.name = 'toefl ibt (' + category.trim() + ")";
                                                        tofelDict.description = ieltsMappingDict[0].Graduate[9].FacultyOf_Vet_Agri_Sci[0].programs.toefl_ibt;
                                                        courseAdminReq.english.push(tofelDict);

                                                        pteDict.name = 'pte academic (' + category.trim() + ")";
                                                        pteDict.description = ieltsMappingDict[0].Graduate[9].FacultyOf_Vet_Agri_Sci[0].programs.pte;
                                                        courseAdminReq.english.push(pteDict);

                                                        caeDict.name = 'cae (' + category.trim() + ")";
                                                        caeDict.description = ieltsMappingDict[0].Graduate[9].FacultyOf_Vet_Agri_Sci[0].programs.cae;
                                                        courseAdminReq.english.push(caeDict);

                                                    }
                                                    break;
                                                }
                                            }
                                        }

                                    }//end if undergrad or not
                                }
                            }
                            console.log("courseAdminReq.english --> ", JSON.stringify(courseAdminReq.english));

                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log("gfffffsfsss-->", academicReq);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + JSON.stringify(academicReq));
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq];
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }

                            //DYNAMIC PROGRESS BAR //CAN BE COPIED//PBT NOT INCLUDED
                            //progress bar start
                            const regEx = /[+-]?\d+(\.\d+)?/g;
                            let penglishList = [];
                            let newEnglish = [];
                            for (let english of courseAdminReq.english) {
                                if (english.description && english.description != null) {
                                    newEnglish.push(english);
                                }
                            }
                            courseAdminReq.english = newEnglish;
                            for (let payload of courseAdminReq.english) {
                                let pieltsDict = {}; const pibtDict = {}; const ppteDict = {}; const pcaeDict = {};
                                // extract exact number from string              
                                let matchedStrList_ielts, matchedStrList_pte, matchedStrList_tofel, matchedStrList_cae;
                                let ieltsNo, pteNo, ibtNo, caeNo;
                                console.log("payload -->", payload);
                                if (payload.name.toLowerCase().indexOf('ielts') > -1) {
                                    matchedStrList_ielts = String(payload.description).match(regEx);
                                    if (matchedStrList_ielts && matchedStrList_ielts.length > 0) {
                                        ieltsNo = Number(matchedStrList_ielts[0]);
                                    }
                                    if (ieltsNo) {
                                        pieltsDict.name = payload.name;
                                        pieltsDict.description = ieltsDict.description;
                                        pieltsDict.min = 0;
                                        pieltsDict.require = ieltsNo;
                                        pieltsDict.max = 9;
                                        pieltsDict.R = 0;
                                        pieltsDict.W = 0;
                                        pieltsDict.S = 0;
                                        pieltsDict.L = 0;
                                        pieltsDict.O = 0;
                                        penglishList.push(pieltsDict);
                                    }
                                }
                                 
                                 else if (payload.name.toLowerCase().indexOf('ibt') > -1) {
                                    matchedStrList_tofel = String(payload.description).match(regEx);
                                    if (matchedStrList_tofel && matchedStrList_tofel.length > 0) {
                                        ibtNo = Number(matchedStrList_tofel[0]);
                                    }
                                    if (ibtNo) {
                                        pibtDict.name = payload.name;
                                        pibtDict.description = tofelDict.description;
                                        pibtDict.min = 0;
                                        pibtDict.require = ibtNo;
                                        pibtDict.max = 120;
                                        pibtDict.R = 0;
                                        pibtDict.W = 0;
                                        pibtDict.S = 0;
                                        pibtDict.L = 0;
                                        pibtDict.O = 0;
                                        penglishList.push(pibtDict);
                                    }
                                } else if (payload.name.toLowerCase().indexOf('pte') > -1) {
                                    matchedStrList_pte = String(payload.description).match(regEx);
                                    if (matchedStrList_pte && matchedStrList_pte.length > 0) {
                                        pteNo = Number(matchedStrList_pte[0]);
                                    }
                                    if (pteNo) {
                                        ppteDict.name = payload.name;
                                        ppteDict.description = pteDict.description;
                                        ppteDict.min = 0;
                                        ppteDict.require = pteNo;
                                        ppteDict.max = 90;
                                        ppteDict.R = 0;
                                        ppteDict.W = 0;
                                        ppteDict.S = 0;
                                        ppteDict.L = 0;
                                        ppteDict.O = 0;
                                        penglishList.push(ppteDict);
                                    }
                                } else if (payload.name.toLowerCase().indexOf('cae') > -1) {
                                    matchedStrList_cae = String(payload.description).match(regEx);
                                    if (matchedStrList_cae && matchedStrList_cae.length > 0) {
                                        caeNo = Number(matchedStrList_cae[0]);
                                    }
                                    if (caeNo) {
                                        pcaeDict.name = payload.name;
                                        pcaeDict.description = caeDict.description;
                                        pcaeDict.min = 80;
                                        pcaeDict.require = caeNo;
                                        pcaeDict.max = 230;
                                        pcaeDict.R = 0;
                                        pcaeDict.W = 0;
                                        pcaeDict.S = 0;
                                        pcaeDict.L = 0;
                                        pcaeDict.O = 0;
                                        penglishList.push(pcaeDict);
                                    }
                                }
                            }

                            console.log("penglishList -->", penglishList);

                            resJsonData.course_admission_requirement = {};
                            // courseAdminReq.english = penglishList;
                            // if (courseAdminReq.english && courseAdminReq.english.length > 0) {
                            //     resJsonData.course_admission_requirement.english = courseAdminReq.penglishList;
                            // } else {
                            //     resJsonData.course_admission_requirement.english = [];
                            // }
                            if (courseAdminReq.academic && courseAdminReq.academic.length > 0) {
                                resJsonData.course_admission_requirement.academic = courseAdminReq.academic;
                            } else {
                                resJsonData.course_admission_requirement.academic = [];
                            }
                            // add english requirement 'english_more_details' link
                            let entry_requirements_url;
                            if (study_level == "Under Graduate" || study_level == 'Bachelor' || (study_level == 'Diploma' && !title.includes('Graduate Diploma'))) {
                                entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url_undergrad);
                            }
                            else if (study_level == "Post Graduate" || study_level == 'Coursework' || study_level == 'Diploma' || study_level == 'Certificate') {
                                entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url_postgrad);
                            }
                            else if (study_level == "Research") {
                                entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url_research);
                            }

                            var academicReqURL = resJsonData.course_url + "/entry-requirements/";
                            const academic_requirements_url = academicReqURL;
                            const english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            if (entry_requirements_url && entry_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            } else {
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                            }
                            if (academic_requirements_url && academic_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;
                            } else {
                                resJsonData.course_admission_requirement.academic_requirements_url = "";
                            }
                            if (english_requirements_url && english_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                            } else {
                                resJsonData.course_admission_requirement.english_requirements_url = "";
                            }
                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.englishprogress = penglishList;
                                resJsonData.course_admission_requirement.english = courseAdminReq.englishprogress;
                            } else {
                                throw new Error('IELTS not found');

                                // resJsonData.course_admission_requirement.englishprogress = [];
                            }

                            break;
                        }
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));

                                            rescourse_url = selList;

                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;

                        }
                        case 'course_outline': {
              
                            let mgcrs = null;
                          
                         const majorcrs = {
                           majors:[],
                           minors:[],
                           more_details:""
                         };
                         //for major cousrseList
                           var coursemajor =  courseScrappedData.course_outline;
                           if (Array.isArray(coursemajor)) {
                             for (const rootEleDict of coursemajor) {
                                 console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                 const elementsList = rootEleDict.elements;
                                 for (const eleDict of elementsList) {
                                     console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                     const selectorsList = eleDict.selectors;
                                     for (const selList of selectorsList) {
                                         console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                         if (Array.isArray(selList) && selList.length > 0) {
                                           mgcrs = selList;
                                         }
                                     }
                                 }
                             }
                         }
                           if(mgcrs && mgcrs.length >0){
                             majorcrs.majors=mgcrs
                            }
                           else{
                             majorcrs.majors= majorcrs.majors
                           }
                              resJsonData.course_outline =  majorcrs;
                           //for minor courseList
                              var courseminor =  courseScrappedData.course_outline_minor;
                              let mincrs = null;
                              if (Array.isArray(courseminor)) {
                                for (const rootEleDict of courseminor) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                             mincrs = selList;
                                            }
                                        }
                                    }
                                }
                            }
                              if(mincrs && mincrs.length >0){
                                majorcrs.minors=mincrs
                              }
                              else{
                               majorcrs.minors=majorcrs.minors
                             }
                             //for more_details 
                             var more_outline = courseScrappedData.course_outline_more;
                             let mor_outline = [];
                             if (Array.isArray(more_outline)) {
                               for (const rootEleDict of more_outline) {
                                   console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                   const elementsList = rootEleDict.elements;
                                   for (const eleDict of elementsList) {
                                       console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                       const selectorsList = eleDict.selectors;
                                       for (const selList of selectorsList) {
                                           console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                           if (Array.isArray(selList) && selList.length > 0) {
                                            for( const sel of selList){
                                            console.log("more_outttt1"+ sel.href);
                                            mor_outline.push(sel.href);
                                            }
                                           }
                                           else{
                                            mor_outline.push(majorcrs.more_details);
                                           }
                                       }
                                   }
                               }
                           }
                            //for course_outline_more_details 
                            if(mor_outline.length > 0){
                          majorcrs.more_details = mor_outline;
                            }
                            // else if(majorcrs.majors.length > 0){
                            //     majorcrs.more_details ="";
                            // }
                            else{
                              majorcrs.more_details = majorcrs.more_details;
                            }
                                 resJsonData.course_outline =  majorcrs;
              
                              console.log("major==>", resJsonData.course_outline)
                        
                          break;
                          }
                        case 'course_duration_full_time': {
                            const courseDurationList = [];
                            const courseDurationDisplayList = [];
                            const durationFullTime = {};

                            let durationFullTimeDisplay = null;

                            console.log("*************start formating years*************************");
                            console.log("R full-time", JSON.stringify(courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0]));

                            if (courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].indexOf("full time") > -1) {
                                if (courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0] && courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].length > 0) {
                                    console.log("*************start formating Full Time years*************************");
                                    let temp = courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0];
                                    if (temp.indexOf("/") > -1) {
                                        courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0] = temp.split("/")[0];
                                        temp = temp.split("/")[1];
                                        courseScrappedData.course_duration_part_time = [{
                                            elements: [{
                                                selectors: [
                                                    [
                                                        temp.toString()
                                                    ]
                                                ]
                                            }]
                                        }];
                                    }
                                    resJsonData.course_duration = temp;

                                    let temp1 = await format_functions.validate_course_duration_full_time(courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].toString());
                                    var full_year_formated_data = [{
                                        "unit": temp1[0].unit,
                                        "duration": temp1[0].duration,
                                        "display": "Full-Time"
                                    }];
                                    //full_year_formated_data[0].display = "Full-Time";
                                    console.log("R full_year_formated_data", full_year_formated_data);
                                    ///course duration
                                    const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                                    durationFullTime.duration_full_time = not_full_year_formated_data;
                                    courseDurationList.push(durationFullTime);
                                    ///END course duration

                                    ///course duration display
                                    durationFullTimeDisplay = {};
                                    durationFullTimeDisplay = full_year_formated_data;
                                    courseDurationDisplayList.push(durationFullTimeDisplay);

                                    ///END course duration display
                                    console.log('***************END formating Full Time years**************************')
                                }
                            }

                            if (courseScrappedData.course_duration_part_time[0].elements[0].selectors[0][0].indexOf("part time") > -1) {
                                if (courseScrappedData.course_duration_part_time && courseScrappedData.course_duration_part_time.length > 0) {
                                    console.log("*************start formating Part Time years*************************");
                                    console.log("R part-time", JSON.stringify(courseScrappedData.course_duration_part_time[0].elements[0]));
                                    let durationPartTimeDisplay = null;
                                    const durationPartTime = {};
                                    let temp2 = await format_functions.validate_course_duration_full_time(courseScrappedData.course_duration_part_time[0].elements[0].selectors[0][0]);
                                    var partyear_formated_data = [{
                                        "unit": temp2[0].unit,
                                        "duration": temp2[0].duration,
                                        "display": "Part-Time"
                                    }]
                                    //partyear_formated_data[0].display = "Part-Time";
                                    var parttimeduration = JSON.stringify(partyear_formated_data);
                                    if (parttimeduration != '[]') {

                                        ///parttime course duration
                                        const not_parttime_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_part_time);
                                        durationPartTime.duration_part_time = not_parttime_year_formated_data;
                                        courseDurationList[0].duration_full_time += ", " + durationPartTime.duration_part_time;
                                        ///End Parttime course duration
                                        
                                        ///partitme course duration display
                                        durationPartTimeDisplay = {};
                                        durationPartTimeDisplay = partyear_formated_data[0];
                                        ///END parttime course duration display

                                        if (durationPartTimeDisplay) {
                                            courseDurationDisplayList.push(durationPartTimeDisplay);
                                        }
                                    }
                                    console.log('***************END formating Part Time years**************************');
                                }
                            }
                            let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                            console.log("filtered_duration_formated -->", filtered_duration_formated);
                           
                            if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                resJsonData.course_duration_display = filtered_duration_formated;
                                var isfulltime = false, isparttime = false;
                                filtered_duration_formated.forEach(element => {
                                    if (element.display == "Full-Time") {
                                        isfulltime = true;
                                    }
                                    if (element.display == "Part-Time") {
                                        isparttime = true;
                                    }
                                });
                                resJsonData.isfulltime = isfulltime;
                                resJsonData.isparttime = isparttime;
                            }
                            console.log("*************END formating years*************************");
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            //CUSTOMIZED CODE DON'T COPY    
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {
                              
                            };
                            const regEx = /\d/g;
                            var annualText, totalText, fees2019;
                            var feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            totalText = await Course.extractValueFromScrappedElement(courseScrappedData.total_fees);
                            console.log("totalText------->>>>>>", totalText);
                            if (totalText == null) {
                                fees2019 = await Course.extractValueFromScrappedElement(courseScrappedData.total_fees_2019);
                                console.log("fees2019------->>>>>>", fees2019);
                                feesIntStudent = fees2019;
                                fees2019 = null;
                            }
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feeYear && feeYear.length > 0) {
                                courseTuitionFee.year = feeYear;
                            }
                            if (!feesIntStudent || feesIntStudent.length == 0) {
                                feesIntStudent = totalText;
                                totalText = null;
                                //console.log("totalText",totalText);
                            }
                            console.log("feesIntStudent------>", feesIntStudent)
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits   
                                console.log("feesIntStudent", feesIntStudent);
                                annualText = feesIntStudent;
                                if (feesIntStudent.indexOf("-") > -1) {
                                    feesIntStudent = feesIntStudent.split("-")[0];
                                }
                                if (totalText && totalText.length > 0) {
                                    if (totalText.indexOf("-") > -1) {
                                        totalText = totalText.split("-")[1].trim();
                                    }
                                }
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {

                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student ={
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: ""
                                               
                                            }
                                        } else {
                                            throw new Error("Fees not found");
                                        }
                                    } else {
                                        throw new Error("Fees not found");
                                    }
                                } else {
                                    throw new Error("Fees not found");
                                }
                            } else {
                                feesDict.international_student = {
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",
                                    description: ""
                                   
                                }
                                //throw new Error("Fees not found");
                            }

                            // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student ={
                                                            amount: Number(inetStudentFees),
                                                            duration: 1,
                                                            unit: "Year",
                                                            description: ""
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                // feesDict.international_student_all_fees = [];
                                // if (annualText) {
                                //     feesDict.international_student_all_fees.push("Typical annual course fee: " + annualText.toString());
                                // }
                                // if (totalText) {
                                //     //console.log("totalText1",totalText);
                                //     feesDict.international_student_all_fees.push("Indicative total course fee: " + totalText.toString());
                                // }


                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }

                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            else {
                                feesDict.international_student = {
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",
                                    description: ""
                                  
                                }
                               // feesDict.international_student_all_fees = [];
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;
                        }
                        case 'course_study_mode': { // Location Launceston
                           
                                resJsonData.course_study_mode = "On campus";
                            
                            break;
                        }
                        case 'course_campus_location': { 
                            
                            const courseKeyValcd = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyValcd)) {
                                for (const rootEleDict of courseKeyValcd) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                var crcode = selList.toString().split(' ');
                                                crcode = crcode[0].split("|")[0];
                                                var cricoscode = [crcode.trim()];
                                                //cricoscode = crcode[0];
                                                course_cricos_code = cricoscode;
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_cricos_code != null) {

                                console.log("course_cricos_code -->", course_cricos_code);
                            }
                            else {
                                throw new Error('Cricos code not found');
                            }
                          
                            
                            
                            
                            
                            
                            
                            // Location Launceston
                            console.log('course_campus_location matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedSelStr = [];
                            for (const rootEleDict of rootElementDictList) {
                                console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                // let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        if (selList && selList.length > 0) {
                                            console.log('course_campus_location selList = ' + JSON.stringify(selList));
                                            for (var selItem of selList) {
                                                console.log('course_campus_location selItem = ' + JSON.stringify(selItem));
                                                selItem = selItem.replace(/[\r\n\t ]+/g, ' ');
                                                if (selItem.indexOf(",") > -1) {
                                                    selItem = selItem.split(",");
                                                    for (let i = 0; i < selItem.length; i++) {
                                                        console.log('course_campus_location sel = ' + JSON.stringify(selItem[i]));
                                                        if (selItem[i] != "external" && selItem[i] != "External" && selItem[i] != "Online" && selItem[i] != "online") {

                                                            if (selItem[i].toLowerCase().trim().indexOf('parkville') > -1) {
                                                                concatnatedSelStr.push('Parkville');
                                                            } else if (selItem[i].toLowerCase().trim().indexOf('southbank') > -1) {
                                                                concatnatedSelStr.push('Southbank');
                                                            } else if (selItem[i].toLowerCase().trim().indexOf('werribee') > -1) {
                                                                concatnatedSelStr.push('Werribee');
                                                            } else if (selItem[i].toLowerCase().trim().indexOf('burnley') > -1) {
                                                                concatnatedSelStr.push('Burnley');
                                                            } else if (selItem[i].toLowerCase().trim().indexOf('creswick') > -1) {
                                                                concatnatedSelStr.push('Creswick');
                                                            } else if (selItem[i].toLowerCase().trim().indexOf('dookie') > -1) {
                                                                concatnatedSelStr.push('Dookie');
                                                            } else if (selItem[i].toLowerCase().trim().indexOf('shepparton') > -1) {
                                                                concatnatedSelStr.push('Shepparton');
                                                            }

                                                        }
                                                    }
                                                }
                                                else {
                                                    console.log("selItem -->", selItem)
                                                    if (selItem.toLowerCase().trim().indexOf('parkville') > -1) {
                                                        concatnatedSelStr.push('Parkville');
                                                    } else if (selItem.toLowerCase().trim().indexOf('southbank') > -1) {
                                                        concatnatedSelStr.push('Southbank');
                                                    } else if (selItem.toLowerCase().trim().indexOf('werribee') > -1) {
                                                        concatnatedSelStr.push('Werribee');
                                                    } else if (selItem.toLowerCase().trim().indexOf('burnley') > -1) {
                                                        concatnatedSelStr.push('Burnley');
                                                    } else if (selItem.toLowerCase().trim().indexOf('creswick') > -1) {
                                                        concatnatedSelStr.push('Creswick');
                                                    } else if (selItem.toLowerCase().trim().indexOf('dookie') > -1) {
                                                        concatnatedSelStr.push('Dookie');
                                                    } else if (selItem.toLowerCase().trim().indexOf('shepparton') > -1) {
                                                        concatnatedSelStr.push('Shepparton');
                                                    }
                                                }

                                            } // selList
                                            console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);
                                        }
                                    } // selectorsList                  
                                } // elementsList                
                            } // rootElementDictList 

                            // add only if it has valid value              
                            if (concatnatedSelStr && concatnatedSelStr.length > 0) {
                                var campusedata = [];
                                concatnatedSelStr.forEach(element => {
                                    campusedata.push({
                                        "name": format_functions.titleCase(element),
                                        "code": String(course_cricos_code)
                                    })
                                });

                                resJsonData.course_campus_location = campusedata;
                                console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);
                            } else {
                                throw new Error('Campus Location not found');
                            }
                            break;
                        }


                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            // Semester 1 (February) and Semester 2 (August)
                            // prepare emty JSON struct
                            const courseIntakeDisplay = {};

                            // existing intake value
                            var courseIntake = [];
                            var courseIntakeStr = null;//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                courseIntakeStr = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("NEWIntake@data-->", JSON.stringify(courseIntakeStr))
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var arr = []
                                // const intake = await utils.getValueFromHardCodedJsonFile('course_intake');
                                //console.log("INTAKE FROM HARDCODE---" + JSON.stringify(courseIntakeStr.indexOf('&')));
                                var locations = resJsonData.course_campus_location;
                                var intakes = [];
                                for (let location of locations) {
                                    var myintakedata = {};
                                    myintakedata.name = location.name;
                                    var myvalues = [];
                                    var myintakestr = courseIntakeStr;
                                    console.log("intake information :" + myintakestr);
                                  
                                        
                                        var intakeinfo = myintakestr;

                                       
                                        if (intakeinfo.indexOf("and") != -1) {
                                          if(intakeinfo.includes("Start Year Intake - February and Mid Year Intake - July")== true) 
                                          {
                                                var month = "February,July";
                                            myvalues.push(month)
                                        
                                        }else if(intakeinfo.includes("Start Year Intake - March and Mid Year Intake - July")== true) 
                                        {
                                              var month = "March,July";
                                          myvalues.push(month)
                                      
                                      }else{
                                        var month = "July";
                                        myvalues.push(month)
                                      }

                                          
                                        }
                                        else{
                                            var month = intakeinfo.toString().split("-")[1];
                                            myvalues.push(month.trim())
                                        
                                      

                                            
                                        }
                                    
                                   
                                    myintakedata.value = myvalues;
                                    intakes.push(myintakedata);
                                }
                                var intakedata = {};
                                let formatedIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                intakedata.intake = formatedIntake;
                                var intakeUrl = [];
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedata.more_details = intakeUrl;
                                resJsonData.course_intake = intakedata
                            }
                            else{
                                var locations = resJsonData.course_campus_location;
                                var intakes = [];
                                for (let location of locations) {
                                    var myintakedata = {};
                                    myintakedata.name = location.name;
                                    var myvalues = [];
                                     
                                       
                                   myvalues.push({
                                    "actualdate": "",
                                    "month": ""
                                  });
                                    myintakedata.value = myvalues;
                                    intakes.push(myintakedata);
                                }
                                var intakedata = {};
                              
                                intakedata.intake = intakes;
                                var intakeUrl = [];
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedata.more_details = intakeUrl;
                                resJsonData.course_intake = intakedata
                            }
                            break;
                        }




                    
                        case 'course_scholarship': {
                            //CUSTOMIZED CODE DO NOT COPY
                            const courseKeyVal = courseScrappedData.course_scholarship;
                            console.log("course_scholarship courseKeyVal", courseKeyVal);
                            let resScholarshipJson = {};
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log('course_scholarship rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log('course_scholarship eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log('course_scholarship selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson.scholarship = selList[0].scholarship;
                                                resScholarshipJson.more_details = resJsonData.course_url + "/fees/#scholarships"
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                resJsonData.course_scholarship = resScholarshipJson;
                            }
                            break;
                        }
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                        case 'course_accomodation_cost': {
                            const courseKeyVal = courseScrappedData.course_accomodation_cost;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_accomodation_cost = resAccomodationCostJson;
                            }
                            break;
                        }
                        case 'course_country':
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            } else {
                                resJsonData[key] = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            let count = 0;
                            console.log('course_career_outcome matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log('course_career_outcome key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedSelectorsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log('course_career_outcome rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                // let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log('course_career_outcome eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        if (selList && selList.length > 0) {
                                            console.log('course_career_outcome selList = ' + JSON.stringify(selList));
                                            let concatnatedSelStr = [];
                                            for (const selItem of selList) {
                                                if (count < 10) {
                                                    concatnatedSelStr.push(selItem);
                                                    count++;
                                                }
                                            } // selList
                                            console.log('course_career_outcome concatnatedSelStr = ' + concatnatedSelStr);
                                            if (concatnatedSelStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            }
                                        }
                                    } // selectorsList                  
                                } // elementsList                
                            } // rootElementDictList 

                            // add only if it has valid value              
                            if (concatnatedSelectorsStr && concatnatedSelectorsStr.length > 0) {
                                resJsonData.course_career_outcome = concatnatedSelectorsStr;
                            } else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }
                        // case 'course_cricos_code': {
                        //     const courseKeyVal = courseScrappedData.course_cricos_code;
                        //     let course_cricos_code = null;
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                     if (Array.isArray(selList) && selList.length > 0) {
                        //                         var crcode = selList.toString().split(' ');
                        //                         crcode = crcode[0].split("|")[0];
                        //                         var cricoscode = [crcode.trim()];
                        //                         //cricoscode = crcode[0];
                        //                         course_cricos_code = cricoscode;
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     if (course_cricos_code) {
                        //         global.cricos_code = course_cricos_code[0];
                        //         var locations = resJsonData.course_campus_location;
                        //         var mycodes = [];
                        //         for (let location of locations) {

                        //             mycodes.push({ location: location.name, code: course_cricos_code.toString(), iscurrent: false });
                        //         }
                        //         resJsonData.course_cricos_code = mycodes;
                        //     }

                        //     break;
                        // }
                        case 'course_title': {
                            const title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            var ctitle = format_functions.titleCase(title)
                            console.log("ctitle@@@", ctitle)
                            resJsonData.course_title = ctitle
                            break;
                        }
                        case 'application_fee': {
                            const courseKeyVal = courseScrappedData.application_fee;
                   
                            let applicationfee = null;
                              if (Array.isArray(courseKeyVal)) {
                            for (const rootEleDict of courseKeyVal) {
                           console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                           const elementsList = rootEleDict.elements;
                           for (const eleDict of elementsList) {
                           console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                              const selectorsList = eleDict.selectors;
                           for (const selList of selectorsList) {
                               console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                            if (Array.isArray(selList) && selList.length > 0) {
                              applicationfee = selList[0];
                               }
                               else{
                                  applicationfee = selList[0];
                               }
                            }
                           }
                           }
                       }
                      if (applicationfee && applicationfee.length>0) {
                        resJsonData.application_fee = applicationfee;
                       console.log("appfee"+ JSON.stringify(resJsonData.application_fee));
                       }
                       else{
                          resJsonData.application_fee = applicationfee;
                       }
                   
                        break;
                  
                      }
                        case 'course_study_level': {
                            resJsonData.course_study_level = study_level;
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            console.log("#location-->" + JSON.stringify(resJsonData.course_title))
            for (let location of locations) {
                // NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                // NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id + "_" + location.name.replace(/\s/g, '').toLowerCase();

                // NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '_').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                // for (let myfees of resJsonData.course_tuition_fee.fees) {
                //     if (myfees.name == location) {
                //         NEWJSONSTRUCT.international_student_all_fees = myfees;
                //     }
                // }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
               // resJsonData.basecourseid = location_wise_data.course_id;

                // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                //     if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                //         resJsonData.course_cricos_code[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_cricos_code[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                //     if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_intake.intake[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_intake.intake[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                //     if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                //     if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_campus_location[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_campus_location[i].iscurrent = false;
                //     }
                // }
                console.log("checkid>",location_wise_data.course_location_id);
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id.replace(/[\n :""]+/g, '').replace(/[\/]+/g, '_') + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };