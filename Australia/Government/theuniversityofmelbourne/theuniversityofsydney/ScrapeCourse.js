const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, category, study_level) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            console.log('course disciplince : ' + category);
                            let uSetcategory = Array.from(new Set(category));
                            resJsonData.course_discipline = uSetcategory;
                            break;
                        }
                        case 'course_admission_requirement': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];

                            const ieltsDict = {}; const ibtDict = {}; const pbtDict = {};
                            // english requirement
                            const ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                            console.log('IELTS Score :' + ieltsScore);
                            if (ieltsScore && ieltsScore.length > 0) {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ieltsScore;
                                englishList.push(ieltsDict);
                            }
                            const ibtScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ibt_indicator);
                            console.log('ibt Score :' + ibtScore);
                            if (ibtScore && ibtScore.length > 0) {
                                ibtDict.name = 'toefl ibt';
                                ibtDict.description = ibtScore;
                                englishList.push(ibtDict);
                            }
                            const pbtScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_toefl_pbt_score);
                            console.log('pbt Score :' + pbtScore);
                            if (pbtScore && pbtScore.length > 0) {
                                pbtDict.name = 'toefl pbt';
                                pbtDict.description = pbtScore;
                                englishList.push(pbtDict);
                            }
                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            var penglishList = [];
                            var ieltsScorep = "", ibtScorep = "", pbtScorep = "";
                            if (ieltsDict.description) {
                                ieltsScorep = await utils.giveMeNumber(ieltsDict.description);
                                console.log("### IELTS data-->" + ieltsScorep);
                            }
                            if (ibtDict.description) {
                                ibtScorep = await utils.giveMeNumber(ibtDict.description);
                            }
                            if (pbtDict.description) {
                                pbtScorep = await utils.giveMeNumber(pbtDict.description);
                            }
                            if (ieltsScorep != "NA") {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = ieltsDict.description;
                                pieltsDict.min = 0;
                                pieltsDict.require = Number(ieltsScorep);
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }
                            if (ibtScorep != "NA") {
                                pibtDict.name = 'toefl ibt';
                                pibtDict.description = ibtDict.description;
                                pibtDict.min = 0;
                                pibtDict.require = Number(ibtScorep);
                                pibtDict.max = 120;
                                pibtDict.R = 0;
                                pibtDict.W = 0;
                                pibtDict.S = 0;
                                pibtDict.L = 0;
                                pibtDict.O = 0;
                                penglishList.push(pibtDict);
                            }
                            if (pbtScorep != "NA") {
                                ppbtDict.name = 'toefl pbt';
                                ppbtDict.description = pbtDict.description;
                                ppbtDict.min = 310;
                                ppbtDict.require = Number(pbtScorep);
                                ppbtDict.max = 677;
                                ppbtDict.R = 0;
                                ppbtDict.W = 0;
                                ppbtDict.S = 0;
                                ppbtDict.L = 0;
                                ppbtDict.O = 0;
                                penglishList.push(ppbtDict);
                            }
                            let english_unfifine = [];
                            for (let english_text of penglishList) {
                                if (english_text.description != undefined) {
                                    english_unfifine.push(english_text);
                                    console.log("english_unfifine------>>", english_unfifine);
                                }
                            }

                            if (english_unfifine && english_unfifine.length > 0) {
                                courseAdminReq.english = english_unfifine;
                            }
                            else {
                                throw new Error("No english found.");
                            }
                            ///progrssbar End
                            // if (englishList && englishList.length > 0) {
                            //     courseAdminReq.english = englishList;
                            // }
                            // academic requirement
                            const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                            console.log('\n\r');
                            console.log(funcName + 'academicReq = ' + academicReq);
                            console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                            console.log('\n\r');
                            if (academicReq && String(academicReq).length > 0) {
                                courseAdminReq.academic = [academicReq];
                            }
                            else {
                                courseAdminReq.academic = [];
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }
                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.course_admission_english_more_details;
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            // add english requirement 'english_more_details' link
                            const courseKeyValAcademic = courseScrappedData.course_admission_academic_more_details;
                            let resAcademicReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyValAcademic)) {
                                for (const rootEleDict of courseKeyValAcademic) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAcademicReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = "";
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = "";
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                            }

                            break;
                        }

                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            var partTimeText = "";
                            console.log("resJsonData.course_cricos_code[0].code ->",  resJsonData.course_campus_location[0].code);
                            const duration_cri = resJsonData.course_campus_location[0].code.trim();

                            if (duration_cri == '083109M') {
                                fullTimeText = "4 years Full Time";
                                console.log("fullTimeText------>>", fullTimeText);

                            } else {
                                const courseKeyValfulltime = courseScrappedData.course_duration_full_time;
                                console.log('Course duration Fulltime display :' + JSON.stringify(courseKeyValfulltime))
                                if (Array.isArray(courseKeyValfulltime)) {
                                    for (const rootEleDict of courseKeyValfulltime) {
                                        console.log(funcName + '\n\r rootEleDict Fulltime duration = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict Fulltime duration = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList Fulltime duration = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                   if(selList[0].indexOf('N/A') > -1){
                                                    throw new Error("Not an international course."); 
                                                   }
                                                    if (selList[0].indexOf('Duration for International students currently not available') > -1 ) {
                                                        fullTimeText = "";
                                                    }
                                                    else {
                                                        //console.log("selList[0]",selList[0].replace('/','_'))
                                                        if(selList[0].includes('/')){    
                                                            console.log("yess",selList[0].replace('/','_'));                                                        
                                                           var replacedata=selList[0].replace('/','_');
                                                           fullTimeText=replacedata+' '+'fulltime';
                                                        }else{
                                                            fullTimeText = selList[0]+' '+'fulltime';
                                                        }
                                                      
                                                        console.log("selList[0]",fullTimeText)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                const courseKeyValparttime = courseScrappedData.course_duration_part_time;
                                console.log('Course duration parttime display :' + JSON.stringify(courseKeyValparttime))
                                if (Array.isArray(courseKeyValparttime)) {
                                    for (const rootEleDict of courseKeyValparttime) {
                                        console.log(funcName + '\n\r rootEleDict Parttime duration = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict Parttime duration = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList Parttime duration = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    console.log('parttime Duration inner html :' + selList[0]);
                                                    var parttimelength = selList[0].toString().split(':');
                                                    console.log('part time course duration length :' + parttimelength.length)
                                                    if (parttimelength.length > 1) {
                                                        console.log("YESSSS")
                                                        var parttimeDesc = parttimelength[1].toString().trim();
                                                        console.log("YESSSS",parttimeDesc)
                                                        if (parttimeDesc.trim() != '') {
                                                            if (parttimeDesc.indexOf('excluding international student visa holders') > -1 || parttimeDesc.indexOf('Not available to International students') > -1 || parttimeDesc.includes('Not available for International students') > -1 || parttimeDesc.indexOf('N/A') > -1||
                                                            parttimeDesc.indexOf('excluding student visa holders') > -1 || parttimeDesc.indexOf('Not available part time') >-1 || parttimeDesc.indexOf('Not applicable part time') > -1 || parttimeDesc.indexOf('Unavailable as part-time course part time') > -1 || parttimeDesc.indexOf('Unavailable as part-time course') > -1
                                                            || parttimeDesc.indexOf('Not applicable') > -1 || parttimeDesc.indexOf('Part-time study available to Domestic students. International students are required to enrol on a full-time basis.') > -1)
                                                             {
                                                                console.log("yess inif")
                                                                if(parttimeDesc.includes('Not available for International students')){
                                                                    throw new Error("Not an international course.");
                                                                }else{
                                                                    partTimeText = ""; 
                                                                }
                                                                //partTimeText = "";
                                                            }
                                                            else {
                                                                console.log("yess in else")
                                                                partTimeText = " / " + parttimeDesc+' '+ "part time";
                                                            }
                                                        }
                                                        else {
                                                            partTimeText = "";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            var duration = '';
                            if (partTimeText) {
                                duration = fullTimeText + "" + partTimeText;
                            } else {
                                duration = fullTimeText + "" + partTimeText;
                            }

                            if (duration && duration.length > 0) {
                                var resFulltime = duration;
                                if (resFulltime) {
                                    console.log('duration full time display :' + resFulltime);

                                    var fulltimeText = resFulltime.split(':');

                                    console.log('Duration full time 1 :' + fulltimeText[0]);
                                    console.log('Duration full time 2 :' + fulltimeText[1]);


                                    if (fulltimeText.length > 2) {
                                        console.log('Assignment contanct veriable duration :' + fulltimeText[1] + " " + fulltimeText[2]);
                                        resFulltime = fulltimeText[1] + "" + fulltimeText[2];
                                    }
                                    else if (fulltimeText.length > 1) {
                                        console.log('Assignment contanct veriable duration :' + fulltimeText[1]);
                                        resFulltime = fulltimeText[1];
                                    }

                                    console.log('assignment local veriable :' + resFulltime);
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    if (resFulltime.toString().trim().toLowerCase().indexOf('as per') > -1) {
                                        console.log('assignment local veriable 2 :' + resFulltime);
                                        var courseDurationDisplayListNA = [];
                                        var _fulltimeduration = {};
                                        courseDurationDisplayListNA.push(_fulltimeduration);
                                        if (courseDurationList && courseDurationList.length > 0) {
                                            resJsonData.course_duration = courseDurationList;
                                        }
                                        console.log('length of course list :' + courseDurationDisplayListNA.length);
                                        if (courseDurationDisplayListNA && courseDurationDisplayListNA.length > 0) {
                                            resJsonData.course_duration_display = courseDurationDisplayListNA;
                                        }
                                    }
                                    else if (resFulltime.toString().trim() == "") {
                                        console.log('assignment local veriable 2 :' + resFulltime);
                                        var courseDurationDisplayListNA = [];
                                        var _fulltimeduration = {};
                                        courseDurationDisplayListNA.push(_fulltimeduration);
                                        if (courseDurationList && courseDurationList.length > 0) {
                                            resJsonData.course_duration = courseDurationList;
                                        }
                                        console.log('length of course list :' + courseDurationDisplayListNA.length);
                                        if (courseDurationDisplayListNA && courseDurationDisplayListNA.length > 0) {
                                            resJsonData.course_duration_display = courseDurationDisplayListNA;
                                        }
                                    }
                                    else {
                                        var splitdata;
                                        var arr=[]
                                        console.log('local veriable :' + resFulltime);
                                        if(resFulltime.includes("/")){
                                            splitdata=resFulltime.split('/')
                                            courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(splitdata[0]))
                                            courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(splitdata[1]))
                                           let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                           let filtered_duration_formated_parttime=await format_functions.getfilterduration(courseDurationDisplayList[1])
                                            console.log(" filtered_duration_formated", filtered_duration_formated)
                                            console.log("filtered_duration_formated_parttime",filtered_duration_formated_parttime)
                                            if (courseDurationList && courseDurationList.length > 0) {
                                                resJsonData.course_duration = resFulltime;
                                            }
                                            if(filtered_duration_formated &&filtered_duration_formated.length >0 && filtered_duration_formated_parttime &&filtered_duration_formated_parttime.length >0 ){
                                               arr.push(filtered_duration_formated[0])
                                                arr.push(filtered_duration_formated_parttime[0])
                                                console.log("finalduration",arr)                                               
                                                resJsonData.course_duration_display=arr
                                                isfulltime = true;
                                                isparttime = true;
                                                resJsonData.isfulltime = isfulltime;
                                                resJsonData.isparttime = isparttime


                                            }

                                        }else{
                                            courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                        let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                        console.log("filtered_duration_formated", filtered_duration_formated);
                                        console.log("courseDurationDisplayList2", courseDurationDisplayList);
                                        if (courseDurationList && courseDurationList.length > 0) {
                                            resJsonData.course_duration = courseDurationList;
                                        }
                                        if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                            resJsonData.course_duration_display = filtered_duration_formated;
                                            var isfulltime = false, isparttime = false;
                                            filtered_duration_formated.forEach(element => {
                                                if (element.display == "Full-Time") {
                                                    isfulltime = true;
                                                }
                                                if (element.display == "Part-Time") {
                                                    isparttime = true;
                                                }
                                            });
                                            resJsonData.isfulltime = isfulltime;
                                            resJsonData.isparttime = isparttime
                                        }
                                        }
                                        
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_campus_location': {
                            const courseKeyVals = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log(funcName + '\n\r rootEleDict course_cricos_code= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict course_cricos_code= ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList course_cricos_code = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                var crcodeval = selList.toString().split(':');
                                                course_cricos_code = crcodeval[1];
                                            }
                                        }
                                    }
                                }
                            }
                           
                           
                           
                           
                           
                           
                           
                           
                            var campLocationText = "";
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                campLocationText = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log(funcName + 'campLocationText = ' + campLocationText);

                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = String(campLocationText).trim();
                                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                if (campLocationValTrimmed.toString().includes("/")) {
                                    var location_val = campLocationValTrimmed.split('/');
                                    console.log('location_val------->>>>>>' + location_val);
                                    var arr = [];
                                    location_val.forEach(element => {
                                        console.log("LOcation@@@", element)
                                        arr.push(element)
                                    })
                                    campLocationValTrimmed == arr;
                                }
                                if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    var myavailcampuses = ["Camperdown",
                                        "Cumberland", "Camden",
                                        "Mallett Street",
                                        "Mallet Street",
                                        "Rozelle",
                                        "Sydney Medical School Campuses and Teaching Hospitals",
                                        "Surry Hills",
                                        "133 Castlereagh Street, CBD",
                                        "Sydney Conservatorium of Music",
                                        "Westmead",
                                        "One Tree Island Research Station",
                                        "Darlington",
                                        "Castlereagh Street",
                                        "Sydney (CBD Campus)",
                                        "Camperdown & CBD Campus"];
                                    var mycamp = [];
                                    console.log('myavailcampuses------->>>>>>' + myavailcampuses);
                                    myavailcampuses.forEach(element => {
                                        if (campLocationValTrimmed.indexOf(element) != -1) {
                                            // mycamp.push(element);
                                            mycamp.push({
                                                "name": this.titleCase(element),
                                                "code":String(course_cricos_code)
                                            })
                                        }
                                    });
                                    console.log("LocationData@@@@", mycamp)
                                    resJsonData.course_campus_location = mycamp;
                                }
                            } else {
                                throw new Error("No campus location found.");
                            }
                            break;
                        }
                        case 'course_tuition_fees_international_student': {
                            const courseTuitionFee = {};
                            const feesList = [];
                            // const feesDict = {};
                            const feesDict = {}                          
                               
                            //for first year only
                            const feesIntStudent_first_year = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            console.log('course tution fee year :' + feesIntStudent_first_year);
                            if (feesIntStudent_first_year != null) {
                                if (feesIntStudent_first_year && feesIntStudent_first_year.length > 0) { // extract only digits
                                    console.log("international_student :" + feesIntStudent_first_year)
                                    let textList = [];
                                    textList = String(feesIntStudent_first_year).split(':');
                                    if (textList && textList.length > 1) {
                                        const feesWithDollor = textList[1];
                                        const feesWithDollorTrimmed = String(feesWithDollor).trim();
                                        console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                        const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                                        console.log(funcName + 'feesVal = ' + feesVal);
                                        if (feesVal) {
                                            const myval = feesVal.split(".");
                                            const regEx = /\d/g;
                                            let feesValNum = myval[0].match(regEx);
                                            if (feesValNum) {
                                                console.log(funcName + 'feesValNum = ' + feesValNum);
                                                feesValNum = feesValNum.join('');
                                                console.log(funcName + 'feesValNum = ' + feesValNum);
                                                let feesNumber = null;
                                                if (feesValNum.includes(',')) {
                                                    feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                                } else {
                                                    feesNumber = feesValNum;
                                                }
                                                console.log(funcName + 'feesNumber = ' + feesNumber);

                                               
                                                if (Number(feesNumber)) {
                                                    feesDict.international_student={
                                                        amount: Number(feesNumber),
                                                        duration: 1,
                                                        unit: "Year",                                                        
                                                        description: feesIntStudent_first_year,
                                                       
                                                    };
                                                }
                                                else {
                                                    feesDict.international_student={
                                                        amount: 0,
                                                        duration: 1,
                                                        unit: "Year",                                                      
                                                        description: "",
                                                        
                                                    }
                                                }

                                                // let fees_unfifine = [];
                                                // for (let intake_text of feesNumber) {
                                                //     if ( feesDict.international_student.amount != "") {
                                                //         fees_unfifine.push( feesDict.international_student);
                                                //         console.log("fees_unfifine------>>", fees_unfifine);
                                                //     }
                                                // }

                                                // if (fees_unfifine && fees_unfifine.length > 0) {

                                                //     feesNumber = fees_unfifine;
                                                // }
                                                // else {
                                                //     throw new Error("intake date not found.");
                                                // }
                                            }
                                        }
                                    }
                                }
                                else {
                                    feesDict.international_student={
                                        amount: 0,
                                        duration: 1,
                                        unit: "Year",                                       
                                        description: "",
                                       
                                    }
                                }
                                

                                if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                    const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                    const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                    if (feesDuration && feesDuration.length > 0) {
                                        feesDict.fee_duration_years = feesDuration;
                                    }
                                    if (feesCurrency && feesCurrency.length > 0) {
                                        feesDict.currency = feesCurrency;
                                    }
                                    
                                    if (feesDict) {
                                        var campus = resJsonData.course_campus_location;
                                        for (let loc of campus) {
                                            feesList.push({ name: loc.name, value: feesDict });
                                        }
                                    }
                                    if (feesList && feesList.length > 0) {
                                        courseTuitionFee.fees = feesList;
                                    }
                                    console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                    if (courseTuitionFee) {
                                        resJsonData.course_tuition_fee = courseTuitionFee;

                                    }
                                }
                                else {
                                    const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                                    if (feeYear && feeYear.length > 0) {
                                        courseTuitionFee.year = feeYear;
                                    }
                                    const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                    const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                    if (feesDuration && feesDuration.length > 0) {
                                        feesDict.fee_duration_years = feesDuration;
                                    }
                                    if (feesCurrency && feesCurrency.length > 0) {
                                        feesDict.currency = feesCurrency;
                                    }

                                    feesDict.international_student = "";
                                  
                                   
                                    if (feesDict) {
                                        var campus = resJsonData.course_campus_location;
                                        for (let loc of campus) {
                                            feesList.push({ name: loc.name, value: feesDict });
                                        }
                                    }
                                    if (feesList && feesList.length > 0) {
                                        courseTuitionFee.fees = feesList;
                                    }
                                    if (courseTuitionFee) {

                                        resJsonData.course_tuition_fee = courseTuitionFee;
                                    }
                                }
                            }
                            else {
                                const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                                if (feeYear && feeYear.length > 0) {
                                    courseTuitionFee.year = feeYear;
                                }

                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                feesDict.international_student = "";
                               
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;
                        }

                        case 'course_study_mode': {
                            resJsonData.course_study_mode = "On campus";
                           
                            break;
                        }


                        case 'course_intake': {
                            var courseIntakeStr = [];                           
                            const courseIntakeStr1 = await Course.extractdurationValueFromScrappedElement(courseScrappedData.course_intake);
                            // const courseIntakeStr1=await Course.extractValueFromScrappedElement(courseScrappedData.course_intake)
                            console.log("DATA@@@@@@@@@", JSON.stringify(courseScrappedData.course_intake))
                            if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                                resJsonData.course_intake = courseIntakeStr1;
                                console.log('changes intake date :' + courseIntakeStr1);
                                const intakeStrList = String(courseIntakeStr1).split('semester');
                                console.log(funcName + 'intakeStrList course_intake = ' + JSON.stringify(intakeStrList));
                                courseIntakeStr = intakeStrList;
                            }
                            console.log("intakes date for course" + JSON.stringify(courseIntakeStr));
                            console.log('intake duration length :' + courseIntakeStr.length);

                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var campus = resJsonData.course_campus_location;
                                console.log('Campuse location :' + campus);
                                var intakesArray = [];
                                var durationArr = '';
                                var intakes = [];
                                for (var camcount = 0; camcount < campus.length; camcount++) {
                                    for (var count = 0; count < courseIntakeStr.length; count++) {
                                        console.log("intake -->" + count)
                                        var intakedetail = {};
                                        intakedetail.name = campus[camcount].name;
                                        intakedetail.value = await utils.giveMeArray(courseIntakeStr[count].replace(/[\r\n\t ]+/g, ' '), ";");
                                        intakes.push(intakedetail);
                                    }
                                }
                                var intakedata = {};
                                //intakedata.intake = await utils.providemyintake(intakes, "MOM", "");
                                let formatIntake = await utils.providemyintake(intakes, "mom", "");
                                //var intakedata = {};
                             
                                intakedata.intake = formatIntake;
                                let valmonths11 = "";
                               
                              
                                intakedata.more_details = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake_url);
                                resJsonData.course_intake = intakedata;
                            }

                            else {
                                var campus = resJsonData.course_campus_location;
                                console.log('Campuse location :' + campus);
                                var IntakeNotgiven = "";
                                var intakesArray = [];
                                var durationArr = '';
                                var intakes = [];
                                for (var camcount = 0; camcount < campus.length; camcount++) {
                                    console.log("intake -->" + count)
                                    var intakedetail = {};
                                    intakedetail.name = campus[camcount].name;
                                    intakedetail.value = await utils.giveMeArray(IntakeNotgiven.replace(/[\r\n\t ]+/g, ' '), ";");
                                    intakes.push(intakedetail);
                                }
                                var intakedata = {};
                                intakedata.intake = intakes;
                                intakedata.more_details = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake_url);;
                                resJsonData.course_intake = intakedata;
                            }
                            break;
                        }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_country = resAccomodationCostJson;
                            }
                            break;
                        }
                        case 'course_overview': {
                            resJsonData.course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            break;
                        }
                        case 'course_career_outcome': {
                            console.log(funcName + 'matched case course_career_outcome: ' + key);
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            var course_career_outcome = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            }
                            else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }
                        
                        case 'program_code': {
                            console.log(funcName + 'matched case program_code: ' + key);
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                var programcode = selList.toString().split(':');
                                                program_code = programcode[1];
                                            }
                                        }
                                    }
                                }
                            }
                            resJsonData.program_code = program_code.trim();
                            break;
                        }

                        case 'course_title': {
                            resJsonData.course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            break;
                        }
                        case 'course_study_level': {
                            console.log(funcName + 'course_title course_study_level = ' + study_level);
                            resJsonData.course_study_level = study_level.trim();
                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = courseScrappedData.course_outline_minor;
                            const courseKeyVal_major =courseScrappedData.course_outline;
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                            let course_outlines = {};
                            console.log("MAjor==>", JSON.stringify(courseKeyVal_major))
                            if (courseKeyVal_minor != null) {
                               let minor=null;
                               if (Array.isArray(courseKeyVal_minor)) {
                                for (const rootEleDict of courseKeyVal_minor) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                minor = selList;
                                            }
                                        }
                                    }
                                }
                                if(minor!=null){
                                    course_outlines.minors = minor
                                }else{
                                    course_outlines.minors = []
                                }

                            } else {
                                course_outlines.minors = []
                               
                                }
                            }
                            if (courseKeyVal_major) {
                                let course_career_outcome = null;
                                if (Array.isArray(courseKeyVal_major)) {
                                    for (const rootEleDict of courseKeyVal_major) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    course_career_outcome = selList;
                                                }
                                            }
                                        }
                                    }
                                }
                                if(course_career_outcome!=null){
                                    course_outlines.majors = course_career_outcome
                                }else{
                                    course_outlines.majors = []
                                }
                              
                            }
                            if (courseKeyVal != null) {
                                course_outlines.more_details = courseKeyVal
                            } else {
                                course_outlines.more_details = ""
                            }
                            resJsonData.course_outline = course_outlines
                            break;
                        }
                        case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            console.log("Application==>",courseKeyVal)
                            if (courseKeyVal == null || courseKeyVal == "null" ) {
                                resJsonData.application_fees = ""
                            } else {
                                resJsonData.application_fees = courseKeyVal
                            }
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                }
            }
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
               
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }

            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, await s.page.url(), courseDict.category, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails

    static titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    }
} // class
module.exports = { ScrapeCourse };
