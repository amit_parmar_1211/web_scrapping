const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
class ScrapeCourseList extends Scrape {
  async scrapeCourseListForPages() {
    const funcName = 'scrapeCourseListForPages ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var datalist = [];
      await s.setupNewBrowserPage("https://sydney.edu.au/courses/search.html");
      var mainCategory = []
      const study_leval = "/html/body/div[1]/div[5]/div/div[1]/div[1]/div/div/div[3]/div/div[1]/div/label/span[not(contains(text(),'All'))]";
      const maincategoryselector = "/html/body/div[1]/div[5]/div/div[1]/div[1]/div/div/div[3]/div/div[3]/label/span[not(contains(text(),'All'))]";
     // const buttonselector = "/html/body/div[1]/div[5]/div/div[1]/div[2]/div[2]/div/div[1]/a";
      const activeselector = "//*/a[@class='m-pagination__item m-pagination__item--highlight m-pagination__item--next cc_pointer'] | //*/a[@class='m-pagination__item m-pagination__item--highlight m-pagination__item--next']";
      var study_leval_val = await s.page.$x(study_leval);
      console.log("Total study_leval_val-->" + study_leval_val.length);

      // for (let study_val of study_leval_val){
      for (let i = 0; i < study_leval_val.length; i++) {
        var category = await s.page.$x(maincategoryselector);
        console.log("Total categories-->" + category.length);
       // var button = await s.page.$x(buttonselector);
        for (let link of category) {
          var ispage = true;
          var categorystringmain = await s.page.evaluate(el => el.innerText, link);
          var study_text = await s.page.evaluate(el => el.innerText, study_leval_val[i]);
          console.log("Total categorystringmain-->" + categorystringmain);
          var study_text_val = study_text.replace('Coursework', '').trim();
          mainCategory.push(categorystringmain.trim());
          await study_leval_val[i].click();
          await s.page.waitFor(5000);
          await link.click();
          await s.page.waitFor(5000);
         // await button[0].click();
         // await s.page.waitFor(5000);
          while (ispage) {
            const selector = "/html/body/div[1]/div[5]/div/div[1]/div[2]/div[2]/div/div[1]/a";
            const iner = "/html/body/div[1]/div[5]/div/div[1]/div[2]/div[2]/div/div[1]/a/div/following-sibling::div[not(contains(@class,'b-result-container__item b-result-container__item--cursor'))]/text()"
            var category11 = await s.page.$x(selector);
            var inertxt = await s.page.$x(iner);
            console.log("Total category11-->" + category11);
            for (let iii = 0; iii < inertxt.length; iii++) {
              console.log("hello -->");
              var categorystring = await s.page.evaluate(el => el.textContent, inertxt[iii]);
              var categoryurl = await s.page.evaluate(el => el.href, category11[iii]);
              console.log("Total categorystring-->" + categorystring);
              datalist.push({ href: categoryurl, innerText: categorystring, category: categorystringmain, study_level: study_text_val });
            }
            console.log("study_text-->" + study_text_val);
            console.log("Data-->" + categorystringmain + "-->" + datalist.length);
            const active = await s.page.$x(activeselector);
            if (active[0]) {
              await active[0].click();
              await s.page.waitFor(5000);
            }
            else {
              ispage = false;
            }
          }
          await link.click();
          await s.page.waitFor(5000);
        }
        fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
        fs.writeFileSync("./output/theuniversityofsydney_original_courselist.json", JSON.stringify(datalist))
      }

      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < datalist.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (datalist[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [], study_level: datalist[i].study_level });
          }
        } else {
          uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [], study_level: datalist[i].study_level });
        }
      }
      await fs.writeFileSync("./output/theuniversityofsydney_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      // for (let i = 0; i < datalist.length; i++) {
      //   for (let j = 0; j < uniqueUrl.length; j++) {
      //     if (uniqueUrl[j].href == datalist[i].href) {
      //       uniqueUrl[j].category.push(datalist[i].category);
      //     }
      //   }
      // }
      for (let i = 0; i < datalist.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == datalist[i].href) {
            if (uniqueUrl[j].category.includes(datalist[i].category)) {

            } else {
              uniqueUrl[j].category.push(datalist[i].category);
            }

          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/theuniversityofsydney_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing course list to file....');
      console.log(funcName + 'totalCourseList = ' + JSON.stringify(datalist));




      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

} // class

module.exports = { ScrapeCourseList };
