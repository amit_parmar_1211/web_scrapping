const puppeteer = require('puppeteer');
const fs = require("fs");
async function start() {
    const URL = "https://study.unimelb.edu.au/find/";
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    let selector = "#modalTitle > button";

    await page.waitFor(5000);
    console.log("wait over");
    var display = await page.$$(selector);
    console.log("display variable");
    await display[0].click();

    


    const categoryselectorUrl = "#top-interests > div > div > div > a";
    const categoryselectorText = "#top-interests > div > div > div > a > div > span > div > div > div > div > span";
    var elementstring = "", elementhref = "", allcategory = [];

    const targetLinksCardsUrls = await page.$$(categoryselectorUrl);
    const targetLinksCardsText = await page.$$(categoryselectorText);
    var targetLinksCardsTotal = [];
    for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        elementstring = await page.evaluate(el => el.innerText, targetLinksCardsText[i]);
        elementhref = await page.evaluate(el => el.href, targetLinksCardsUrls[i]);
        targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
    }
    // await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
    console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));



    let linkselector = "div.tabs__panel.max:not([style*='display: none']) > ul > li > a";
    let textselector = "div.tabs__panel.max:not([style*='display: none']) > ul > li > a > span:nth-child(2)"
    var totalCourseList = [];
    for (let target of targetLinksCardsTotal) {
        await page.goto(target.href);
        //scrape university courselist
        await page.waitFor(5000);
        let check = "#filter-graduate";
        await page.evaluate((check) => document.querySelector(check).click(), check);
        const targetLinks = await page.$$(linkselector);
        const targetText = await page.$$(textselector);


        console.log("#total link selectors---->" + targetLinks.length);
        for (var i = 0; i < targetLinks.length; i++) {
            var elementstring = await page.evaluate(el => el.innerText, targetText[i]);
            const elementlink = await page.evaluate(el => el.href, targetLinks[i]);
            totalCourseList.push({ href: elementlink, innerText: elementstring, category: target.innerText });
        }
    }
    // console.log("totalCourseList -->", totalCourseList);
    fs.writeFileSync("theuniversityofmelbourne_category_courselist.json", JSON.stringify(totalCourseList));
    await browser.close();

}
start();
