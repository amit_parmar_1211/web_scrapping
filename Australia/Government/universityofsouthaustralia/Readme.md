**University of South Australia : Total Courses Details As On 12 April 2019**
* Total Courses =238
* Courses without CRICOS code = 0
* Total available courses = 427
* Repeted = 0
* All cases for ielts --> https://study.unisa.edu.au/degrees/master-of-teaching-early-childhood/int, https://study.unisa.edu.au/degrees/master-of-teaching-primary/int
* Doctor of Education --> ielts --> https://study.unisa.edu.au/degrees/doctor-of-education/int

**Courses written in tobeAddedCourses should be included in courselist json file**
**Created main_category_courses.json**
* contains all the categories from where the scrapping is done

**course_toefl_ielts_score**
* for Masters by Research, Doctor of Philosophy the ielts mapping is Fixed and for others it differs and is refered from the mapping file

**course_intake**
* for intake Masters by Research, Doctor of Philosophy intake is fixed and for others it is scrapped from the course details page itself

**There are 427 courses**
* But file generated is only 346 because there are some courses that are repeted and the json file gets replaced
* eg : https://study.unisa.edu.au/degrees/bachelor-of-social-science-human-services-bachelor-of-arts/int
* Repeated 3 times