const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
// const mapping = require('./output/Mapping/main');
request = require('request');

class ScrapeCourse extends Course {
  static titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      // You do not need to check if i is larger than splitStr length, as your for does that for you
      // Assign it back to the array
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
  }
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'program_code': {
              const courseKeyVal = courseScrappedData.program_code;
              console.log("courseScrappedData.program_code = ", courseScrappedData.program_code);
              var program_code = "";
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        // program_code = selList[0].split(": ");
                        var programcode = selList.toString().split("PROGRAM CODE");


                        // crcode1.push(crcode[1].replace(/[\r\n\t ]+/g, ''));
                        program_code = programcode[1].replace(/[\r\n\t ]+/g, '');
                      }
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))
              console.log("program_code -->", program_code);
              let program_code_array = [];
              if (program_code == '' || program_code == undefined) {
                resJsonData.program_code = program_code;
              } else {
                program_code_array.push(program_code);
                resJsonData.program_code = program_code;
              }

              break;
            }
            case 'course_discipline': {
              // As of not we do not have way to get this field value
              resJsonData.course_discipline = course_category;
              break;
            }
            case 'application_fee': {
              const courseKeyVal = courseScrappedData.application_fee;
     
              let applicationfee = null;
                if (Array.isArray(courseKeyVal)) {
              for (const rootEleDict of courseKeyVal) {
             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
             const elementsList = rootEleDict.elements;
             for (const eleDict of elementsList) {
             console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                const selectorsList = eleDict.selectors;
             for (const selList of selectorsList) {
                 console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
              if (Array.isArray(selList) && selList.length > 0) {
                applicationfee = selList[0];
                 }
                 else{
                    applicationfee = selList[0];
                 }
              }
             }
             }
         }
        if (applicationfee && applicationfee.length>0) {
          resJsonData.application_fee = applicationfee;
         console.log("appfee"+ JSON.stringify(resJsonData.application_fee));
         }
         else{
            resJsonData.application_fee = applicationfee;
         }
     
          break;
    
        }
            case 'course_scholarship': {
              const courseKeyVal = courseScrappedData.course_scholarship;
              let resScholarshipJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList Scholarship= ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resScholarshipJson) {
                resJsonData.course_scholarship = resScholarshipJson;
              }
              break;
            }
            case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              let ieltsString = "";
              const courseAdminReq = {};
              let ieltsNumber = null; let pbtNumber = null; let pteNumber = null; let caeNumber = null; let ibtNumber = null;
              // english requirement
              let reading = "";
              let writing = "";
              let listing = "";
              let speaking = "";
              let demoString = "(";
              let ieltsfinaloutput = null;
              var myscrore = '';
              let entryString = "";
              console.log('Course english language requirment :' + JSON.stringify(courseScrappedData.course_toefl_ielts_score));
              let courseTitle = resJsonData.course_title;
              console.log("studyLevel -->", courseTitle);
              if (courseTitle.toLowerCase().indexOf("masters by research") == -1 && courseTitle.toLowerCase().indexOf("doctor of philosophy") == -1 && courseTitle.toLowerCase().indexOf("doctor of education") == -1 && courseTitle.toLowerCase().indexOf("bachelor of pharmaceutical science, bachelor of pharmacy (honours)") == -1) {


                let finalData = courseScrappedData.course_toefl_ielts_score[0].elements[0].selectors[0];
                for (let score of finalData) {
                  ieltsString += score + " ";
                  // console.log("score -->", score);
                  let tempScore = [
                    {
                      "elements":
                        [
                          {
                            "selectors":
                              [
                                [score]
                              ]
                          }
                        ]
                    }
                  ]
                  if (score.includes("reading") && score.includes("7")) {

                    const ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempScore);
                    // let ieltsfinaloutput = ieltsScore.toString();

                    if (ieltsScore && ieltsScore.length > 0) {
                      // extract exact number from string
                      const regEx = /[+-]?\d+(\.\d+)?/g;
                      const matchedStrList = String(ieltsScore).match(regEx);
                      console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                      if (matchedStrList && matchedStrList.length > 0) {
                        reading = Number(matchedStrList[0]);
                        console.log(funcName + 'ieltsNumber = ' + reading);
                      }
                    }
                    console.log("Reading -->", reading);
                    demoString += reading + "R ";
                  } else if (score.includes("reading") && !score.includes("7")) {
                    const ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempScore);
                    // let ieltsfinaloutput = ieltsScore.toString();

                    if (ieltsScore && ieltsScore.length > 0) {
                      // extract exact number from string
                      const regEx = /[+-]?\d+(\.\d+)?/g;
                      const matchedStrList = String(ieltsScore).match(regEx);
                      console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                      if (matchedStrList && matchedStrList.length > 0) {
                        reading = Number(matchedStrList[0]);
                        console.log(funcName + 'ieltsNumber = ' + reading);
                      }
                    }
                    console.log("Reading -->", reading);
                    demoString += reading + " R";
                  }
                  if (score.includes("writing") && score.includes("7")) {
                    const ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                    // let ieltsfinaloutput = ieltsScore.toString();

                    if (ieltsScore && ieltsScore.length > 0) {
                      // extract exact number from string
                      const regEx = /[+-]?\d+(\.\d+)?/g;
                      const matchedStrList = String(ieltsScore).match(regEx);
                      console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                      if (matchedStrList && matchedStrList.length > 0) {
                        writing = Number(matchedStrList[0]);
                        console.log(funcName + 'writing = ' + writing);
                      }
                    }
                    console.log("writing -->", writing);
                    demoString += writing + "W ";
                  } else if (score.includes("writing") && !score.includes("7")) {
                    demoString += "W";
                  }
                  if (score.includes("speaking") && score.includes("7")) {
                    const ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                    // let ieltsfinaloutput = ieltsScore.toString();

                    if (ieltsScore && ieltsScore.length > 0) {
                      // extract exact number from string
                      const regEx = /[+-]?\d+(\.\d+)?/g;
                      const matchedStrList = String(ieltsScore).match(regEx);
                      console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                      if (matchedStrList && matchedStrList.length > 0) {
                        speaking = Number(matchedStrList[0]);
                        console.log(funcName + 'speaking = ' + speaking);
                      }
                    }
                    demoString += speaking + "S ";
                  } else if (score.includes("speaking") && !score.includes("7")) {
                    demoString += "S";
                  }
                  if (score.includes("listening") && score.includes("7")) {
                    const ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                    // let ieltsfinaloutput = ieltsScore.toString();
                    if (ieltsScore && ieltsScore.length > 0) {
                      // extract exact number from string
                      const regEx = /[+-]?\d+(\.\d+)?/g;
                      const matchedStrList = String(ieltsScore).match(regEx);
                      console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                      if (matchedStrList && matchedStrList.length > 0) {
                        listing = Number(matchedStrList[0]);
                        console.log(funcName + 'listing = ' + listing);
                      }
                    }
                    demoString += listing + "L";
                  } else if (score.includes("listening") && !score.includes("7")) {
                    demoString += "L";
                  }
                }

                console.log("finalData -->", finalData);
                if (courseScrappedData.course_toefl_ielts_score) {
                  console.log('Course english language requirment :' + JSON.stringify(courseScrappedData.course_toefl_ielts_score));
                  const ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                  ieltsfinaloutput = ieltsScore.toString();

                  if (ieltsScore && ieltsScore.length > 0) {
                    // extract exact number from string
                    const regEx = /[+-]?\d+(\.\d+)?/g;
                    const matchedStrList = String(ieltsScore).match(regEx);
                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                    if (matchedStrList && matchedStrList.length > 0) {
                      ieltsNumber = Number(matchedStrList[0]);
                      console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                    }
                  }
                }

                console.log("demoString -->", demoString);
                entryString += ieltsNumber + demoString + ")";


              } else {
                ieltsString = "6.5 (with 6.0 in Reading and Writing)";
                let score = "6.5 (with 6.0 in Reading and Writing) ";
                let tempScore = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [score]
                            ]
                        }
                      ]
                  }
                ]
                const ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempScore);
                ieltsfinaloutput = ieltsScore.toString();

                if (ieltsScore && ieltsScore.length > 0) {
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(ieltsScore).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    ieltsNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                  }
                }
                entryString = "Research";
              }
              console.log("ieltsString -->", ieltsString);
              console.log("entryString -->", entryString);
              myscrore = await utils.getMappingScore(entryString);
              // console.log("myscrore -->", myscrore);
              var englishList = [];
              var ibtDict = {}, catDict = {}, pteDict = {}, ieltsDict = {}, pbtDict = {}, oetDict = {}, jcuceapDict = {};
              ieltsDict.name = 'ielts academic';
              ieltsDict.description = ieltsString;
              englishList.push(ieltsDict);
              ibtDict.name = 'toefl ibt';
              ibtDict.description = myscrore.ibt;
              englishList.push(ibtDict);
              pbtDict.name = 'toefl pbt';
              pbtDict.description = myscrore.pbt;
              englishList.push(pbtDict);
              pteDict.name = 'pte academic';
              pteDict.description = myscrore.pte;
              englishList.push(pteDict);
              catDict.name = 'cae';
              catDict.description = myscrore.cae;
              englishList.push(catDict);

              if (ieltsNumber && ieltsNumber > 0) {
                //tofel score
                console.log("myscrore.pbt -->", myscrore.pbt);
                let tempTOFEL = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [myscrore.pbt]
                            ]
                        }
                      ]
                  }
                ]
                const tofelScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempTOFEL);
                if (tofelScore && tofelScore.length > 0) {
                  // tofelfinaloutput = tofelScore.toString();
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(tofelScore).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    pbtNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'pbtNumber = ' + pbtNumber);
                  }
                }
                //tofel ibt
                let tempIBT = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [myscrore.ibt]
                            ]
                        }
                      ]
                  }
                ]
                const tofelScoreibt = await Course.extractValueEnglishlanguareFromScrappedElement(tempIBT);
                if (tofelScoreibt && tofelScoreibt.length > 0) {
                  // tofelfinaloutput = tofelScore.toString();
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(tofelScoreibt).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    ibtNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'ibtNumber = ' + ibtNumber);
                  }
                }
                //pte score
                console.log("myscrore.PTE -->", myscrore.pte);
                let tempPTE = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [myscrore.pte]
                            ]
                        }
                      ]
                  }
                ]
                const pteAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempPTE);
                if (pteAScore && pteAScore.length > 0) {
                  // ptefinaloutput = pteAScore.toString();
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(pteAScore).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    pteNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'pteNumber = ' + pteNumber);
                  }
                }
                //cae score
                console.log("myscrore.CAE -->", myscrore.cae);
                let tempCAE = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [myscrore.cae]
                            ]
                        }
                      ]
                  }
                ]

                const caeAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempCAE);
                if (caeAScore && caeAScore.length > 0) {
                  // caefinaloutput = caeAScore.toString();
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(caeAScore).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    caeNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'caeNumber = ' + caeNumber);
                  }
                }
              }

              ///progress bar start
              const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
              const penglishList = [];
              if (ieltsNumber && ieltsNumber > 0) {
                var IELTSJSON = {
                  "name": "ielts academic",
                  "description": ieltsDict.description,
                  "min": 0,
                  "require": ieltsNumber,
                  "max": 9,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (ieltsNumber) {
                  // pieltsDict.name = 'ielts academic';
                  // pieltsDict.value = IELTSJSON;
                  penglishList.push(IELTSJSON);
                }
              }

              if (ibtNumber && ibtNumber > 0) {
                var IBTJSON = {
                  "name": "toefl ibt",
                  "description": ibtDict.description,
                  "min": 0,
                  "require": ibtNumber,
                  "max": 120,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (ibtNumber) {
                  // pibtDict.name = 'toefl ibt';
                  // pibtDict.value = IBTJSON;
                  penglishList.push(IBTJSON);
                }
              }
              if (pbtNumber && pbtNumber > 0) {
                var PBTJSON = {
                  "name": "toefl pbt",
                  "description": pbtDict.description,
                  "min": 310,
                  "require": pbtNumber,
                  "max": 677,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (pbtNumber) {
                  // ppbtDict.name = 'toefl pbt';
                  // ppbtDict.value = PBTJSON;
                  penglishList.push(PBTJSON);
                }
              }

              if (pteNumber && pteNumber > 0) {
                var PTEJSON = {
                  "name": "pte academic",
                  "description": pteDict.description,
                  "min": 0,
                  "require": pteNumber,
                  "max": 90,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (pteNumber) {
                  // ppteDict.name = 'pte academic';
                  // ppteDict.value = PTEJSON;
                  penglishList.push(PTEJSON);
                }
              }

              if (caeNumber && caeNumber > 0) {
                var CAEJSON = {
                  "name": "cae",
                  "description": catDict.description,
                  "min": 80,
                  "require": caeNumber,
                  "max": 230,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (caeNumber) {
                  // pcaeDict.name = 'cae';
                  // pcaeDict.value = CAEJSON;
                  penglishList.push(CAEJSON);
                }
              }

              if (penglishList && penglishList.length > 0) {
                courseAdminReq.english = penglishList;
              } else {
                courseAdminReq.english = [];
              }
              ///progrssbar End

              // academic requirement
              var academicReq = [];
              if (courseTitle.indexOf("Masters by Research") == -1 && courseTitle.indexOf("Doctor of Philosophy") == -1 && courseTitle.indexOf("Doctor of Education") == -1) {
                if (courseScrappedData.course_academic_requirement) {
                  academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                  console.log('\n\r');
                  console.log(funcName + 'academicReq = ' + academicReq);
                  console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                  console.log('\n\r');

                }
              } else {
                academicReq = [
                  "An applicant must satisfy the entry requirements set by the Research Degrees Committee which demonstrate competence to undertake a higher degree by research by:",
                  "a. holding or being eligible to hold from an approved higher education institution an award in an appropriate discipline which is assessed by this University as being equivalent to:",
                  "i. an Honours degree or a Bachelor degree with Honours of at least class 2a standard; or",
                  "ii. an appropriate Masters degree; or",
                  "b. satisfying the relevant Associate Dean: Research Education (or delegate) of their fitness to undertake further advanced work on the basis of their standard of achievement in, and the relevance of, previous higher education studies, and/or relevant professional experience and/or published research work. Schools may admit candidates to study a research degree provided they have evidence that the applicant has a reasonable probability of successfully completing the degree to which they are admitted."
                ];
              }
              console.log('academicReq final = ' + academicReq);
              if (academicReq && academicReq.length > 0) {
                courseAdminReq.academic = [academicReq];
              } else {
                courseAdminReq.academic = [];
                // throw new Error("Academic array empty");
              }

              // if courseAdminReq has any valid value then only assign it
              if (courseAdminReq.english || courseAdminReq.academic) {
                resJsonData.course_admission_requirement = courseAdminReq;
              }

              let resEnglishReqMoreDetailsJson = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
              let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
              // let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
              if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                resJsonData.course_admission_requirement.entry_requirements_url = "";
              } else if (resEnglishReqMoreDetailsJson) {
                resJsonData.course_admission_requirement = {};
                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                resJsonData.course_admission_requirement.entry_requirements_url = "";
              }

              break;
            }
            case 'course_url': {
              if (courseUrl && courseUrl.length > 0) {
                resJsonData.course_url = courseUrl;
              }
              break;
            }
            case 'course_duration_full_time': {
              const courseDurationList = [];
              const courseDurationDisplayList = [];
              const durationFullTime = {};

              let anotherArray = [];
              let durationFullTimeDisplay = null;
              let duration1 = courseScrappedData.course_duration_full_time;
              if (duration1 == '' || duration1 == null || duration1 == undefined) {
                resJsonData.course_duration_display = courseDurationDisplayList;
                resJsonData.course_duration = resJsonData.course_duration;
                // throw new Error("course_duration and course_duration_display not given");
              } else {
                console.log("*************start formating Full Time years*************************");
                console.log("courseScrappedData.course_duration_full_time = ", JSON.stringify(courseScrappedData.course_duration_full_time));
                let finalDuration = duration1[0].elements[0].selectors[0][0];
                finalDuration = finalDuration.replace(/[\r\n\t ]+/g, ' ');
                finalDuration = finalDuration.replace(/[\[\]']/g, '');
                finalDuration = finalDuration.replace(/[\(\)']/g, '');
                console.log("Duration1 = ", finalDuration);
                if(finalDuration.includes('DURATION')){
                  finalDuration = finalDuration.split('DURATION')[1].trim();
                  console.log("Duration 2 =", finalDuration);
                  resJsonData.course_duration = finalDuration;
                }
                const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration.trim());
                console.log("full_year_formated_data", full_year_formated_data);
                ///course duration
                const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                console.log("not_full_year_formated_data", not_full_year_formated_data);
                try {
                  durationFullTime.duration_full_time = not_full_year_formated_data;
                  courseDurationList.push(durationFullTime);
                  console.log("courseDurationList : ", courseDurationList);
                  ///course duration display
                  durationFullTimeDisplay = {};
                  durationFullTimeDisplay.duration = full_year_formated_data[0].duration;
                  durationFullTimeDisplay.unit = full_year_formated_data[0].unit;
                  durationFullTimeDisplay.display = "Full-Time";
                  console.log("durationFullTimeDisplay", durationFullTimeDisplay);
                  courseDurationDisplayList.push(full_year_formated_data);
                  console.log("courseDurationDisplayList", courseDurationDisplayList);
                 
                } catch (err) {
                  console.log("Problem in Full Time years:", err);
                }

                console.log("courseDurationDisplayList2", courseDurationDisplayList);
                let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
               
                if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                  resJsonData.course_duration_display = filtered_duration_formated;
                  var isfulltime = false, isparttime = false;
                  filtered_duration_formated.forEach(element => {
                    if (element.display == "Full-Time") {
                      isfulltime = true;
                    }
                    if (element.display == "Part-Time") {
                      isparttime = true;
                    }
                  });
                  resJsonData.isfulltime = isfulltime;
                  resJsonData.isparttime = isparttime;
                }
              }

              break;
            }
            case 'course_tuition_fee':
            //case 'course_study_mode':
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_duration_part_time': {
              console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
              // No need to process anything, its already processed in other case.
              break;
            }
            case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};
              // courseTuitionFee.year = configs.propValueNotAvaialble;
              let extraDetails = null;
              const feesList = [];
              const feesDict = {
               
              };
              let moredetails = null;
              const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
              const courseKeyVal = courseScrappedData.course_tuition_fees_international_student;

              let feesIntStudent = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        let splitArray = selList[0].split('\n');
                        console.log("splitArray --> ", splitArray);
                        if (splitArray[2]) {
                          extraDetails = splitArray[2];
                        }
                        moredetails=selList.toString().replace('\n',' ');
                        let array2019 = splitArray[1].split(':');
                        console.log("array2019 --> ", array2019);
                        let finalArray = array2019[1].split('(');
                        console.log("finalArray -->", finalArray);
                        feesIntStudent = finalArray[0];

                        console.log("feesIntStudent --> ", selList);

                      }
                      else{
                        feesIntStudent = selList;
                      }
                    }
                  }
                }
              }
            let otherArray = [];
              //const anotherArray = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_more);
              if (feesIntStudent.length > 0) { // extract only digits
                console.log("international_student" + feesIntStudent)
                if (extraDetails == null) {

                } else {
                  otherArray.push(moredetails.trim());
                }
              
              } else {
                feesIntStudent=feesIntStudent;
              }
             
              if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                const arrval = String(feesVal1).split('.');
                const feesVal = String(arrval[0]);
                console.log(funcName + 'feesVal = ' + feesVal);
                if (feesVal) {
                  const regEx = /\d/g;
                  let feesValNum = feesVal.match(regEx);
                  if (feesValNum) {
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    feesValNum = feesValNum.join('');
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    let feesNumber = null;
                    if (feesValNum.includes(',')) {
                      feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                    } else {
                      feesNumber = feesValNum;
                    }
                    console.log(funcName + 'feesNumber = ' + feesNumber);
                    if (Number(feesNumber)) {
                      feesDict.international_student={
                        amount: Number(feesNumber),
                        duration: 1,
                        unit: "Year",
                        description: String(otherArray)
                       
                      }

                    } else {
                      feesDict.international_student={
                        amount: 0,
                        duration: 1,
                        unit: "Year",
                        description: "not available fee"
                       
                      }
                    }
                  }
                }
              } 
              else {
                feesDict.international_student={
                  amount: 0,
                  duration: 1,
                  unit: "Year",
                  description: "not available fee"
                 
                }
              }
            
              if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }
                if (feesDict) {
                  // feesList.push(feesDict);
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                if (courseTuitionFee) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                } else {
                  throw new Error("Fees not found");
                }
                console.log("feesDict.international_student = ", feesDict.international_student);

              }
              break;
            }


            case 'course_campus_location': {
             
              let crcode1 = [ ];
              const courseKeyValcd = courseScrappedData.course_cricos_code;
              let course_cricos_code = null;
             
              if (Array.isArray(courseKeyValcd)) {
                for (const rootEleDict of courseKeyValcd) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList course_cricos_code= ' + JSON.stringify(selList));
                        
                    if (selList.indexOf("CRICOS CODE \nSee research areas (PDF)") == -1) {
                        if (selList.toString().includes("CRICOS")) {
                        var crcode = selList.toString().split("CRICOS CODE");
                         var  ccrcode1=crcode[1].toString().replace(/[\r\n\t ]+/g, '');

                            if(ccrcode1.includes(',')){
                              let splitCricos = ccrcode1.split(',');
                              for (let cricos of splitCricos) {
                               crcode1.push(cricos);
                           
                            }
                            console.log("crccode13"+crcode1);
                          }
                            else{
                              var crcode = selList.toString().split("CRICOS CODE");
                              var  ccrcode1=crcode[1].replace(/[\r\n\t ]+/g, '');
                               crcode1.push(ccrcode1);
                               console.log("crccode14"+crcode1);
                            }
                       
                      }
                      }
                        else{
                          var crcode = selList.toString().split("CRICOS CODE");
                         var  ccrcode1=crcode[1].replace(/[\r\n\t ]+/g, '');
                          crcode1.push(ccrcode1);
                          console.log("crccode14"+crcode1);
                        }
                      
                      }
                    }
                  
                }
              }
              
              
              if(ccrcode1 && ccrcode1.length >0){
                ccrcode1=ccrcode1;
              }
              else{
                throw (new Error('Cricose code Not found!!'));
              }
              // Location Launceston 
              console.log('course_campus_location matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedSelectorsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                // let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  for (const selList of selectorsList) {
                    if (selList && selList.length > 0) {
                      console.log('course_campus_location selList = ' + JSON.stringify(selList));
                      let concatnatedSelStr = [];
                      for (let selItem of selList) {
                        if (selItem.includes(',')) {
                          selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ' ').split(",");
                        } else {
                          selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ' ').split(";");
                        }

                        for (let sel of selItem) {
                          sel = sel.trim();
                          if (sel == 'External') {

                          } else {
                            concatnatedSelStr.push(sel);
                          }

                        }

                      } // selList
                      console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);
                      if (concatnatedSelStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      }


                    }
                  } // selectorsList 
                } // elementsList 
              } // rootElementDictList 


              if (concatnatedSelectorsStr && concatnatedSelectorsStr.length > 0) {
                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                var campusedata = [];
                concatnatedSelectorsStr.forEach(element => {
                  element = this.titleCase(element);
                  campusedata.push({
                    "name": element,
                    "code": String(crcode1)
                  })
                });
                resJsonData.course_campus_location = campusedata;
                console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);
                var study_mode = [];
                if (campLocationValTrimmed.trim().length > 0) {
                  study_mode.push('On campus');
                }
                resJsonData.course_study_mode = study_mode.toString();//.join(',');
                console.log("## FInal string-->" + campLocationValTrimmed);
              }
              else {
                throw new Error("No campus location found.");
              }
              break;

            }
            case 'course_study_mode': { // Location Launceston
              resJsonData.course_study_mode = "On campus";
              break;
            }

            case 'course_intake':
              {
                let courseIntakeStr = [];
                let intake_months = [
                  {
                    key: "January",
                    value: "2020-01-31"
                  },
                  {
                    key: "February",
                    value: "2020-02-29"
                  },
                  {
                    key: "March",
                    value: "2020-03-31"
                  },
                  {
                    key: "April",
                    value: "2020-04-30"
                  },
                  {
                    key: "May",
                    value: "2020-05-31"
                  },
                  {
                    key: "June",
                    value: "2020-06-30"
                  },
                  {
                    key: "July",
                    value: "2020-07-31"
                  },
                  {
                    key: "August",
                    value: "2020-08-31"
                  },
                  {
                    key: "September",
                    value: "2020-09-30"
                  },
                  {
                    key: "October",
                    value: "2020-10-31"
                  },
                  {
                    key: "November",
                    value: "2020-11-30"
                  },
                  {
                    key: "December",
                    value: "2020-12-31"
                  }
                ];
                console.log("resJsonData.course_title ---> ", resJsonData.course_title.trim().indexOf("masters by research"));

                if (resJsonData.course_title.trim().toLowerCase().indexOf("masters by research") > -1 || resJsonData.course_title.toLowerCase().trim().indexOf("doctor of philosophy") > -1 || resJsonData.course_title.trim().toLowerCase().indexOf("doctor of education") > -1) {
                  let anotherArray = [];
                  anotherArray.push("1 January, 2020");
                  anotherArray.push("1 April, 2020");
                  anotherArray.push("1 July, 2020");
                  anotherArray.push("1 October, 2020");
                  for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                    courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": anotherArray });
                  }
                } else {
                  const courseKeyVal = courseScrappedData.course_intake;
                  console.log("resJsonData.course_campus_location -->", resJsonData.course_campus_location);
                  console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                  if (Array.isArray(courseKeyVal)) {
                    for (const rootEleDict of courseKeyVal) {
                      console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                      const elementsList = rootEleDict.elements;
                      for (const eleDict of elementsList) {
                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        const selectorsList = eleDict.selectors;
                        for (const selList of selectorsList) {
                          console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                          if (Array.isArray(selList) && selList.length > 0) {

                          for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                            let anotherArray = [];
                            // selList[0].replace('START DATE', '');
                            console.log("selList[0] -->", selList[0]);
                            let replaceArray = selList[0].split("START DATE");
                            console.log("replaceArray -->", replaceArray);
                            let x = replaceArray[1].trim().split('\n');
                            console.log("X -->", x);
                            for (let i = 0; i < x.length; i++) {
                              console.log("x[i] -->", x[i]);

                              anotherArray.push(x[i].replace(/[\r\n\t ]+/g, ''));

                            }
                            courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": anotherArray });

                          }
                        }//else for intake is not define
                        else{
                          for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                           
                            courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": [] });
                         
                        }
                        }
                      }
                    }
                  }
                }
              }
                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                let formatIntake = await format_functions.providemyintake(courseIntakeStr, "MOM", "");
                if (formatIntake && formatIntake.length > 0) {
                  var intakedata = {};
                  intakedata.intake = formatIntake;
                 
                  intakedata.more_details = more_details;
                  resJsonData.course_intake = intakedata;
                }
                break;
            }

            
            case 'univ_name': {
              const courseKeyVal = courseScrappedData.univ_name;
              let resUnivName = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivName = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivName) {
                resJsonData.univ_name = resUnivName;
              }
              break;
            }
            case 'univ_logo': {
              const courseKeyVal = courseScrappedData.univ_logo;
              let resUnivLogo = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivLogo = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivLogo) {
                resJsonData.univ_logo = resUnivLogo;
              }
              break;
            }
            case 'course_accomodation_cost': {
              const courseKeyVal = courseScrappedData.course_accomodation_cost;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_accomodation_cost = resAccomodationCostJson;
              }
              break;
            }
            case 'course_country': {
              const courseKeyVal = courseScrappedData.course_country;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_country = resAccomodationCostJson;
              }
              break;
            }
            case 'course_outline': {
              
              let mgcrs = null;
            
           const majorcrs = {
             majors:[],
             minors:[],
             more_details:""
           };
           //for major cousrseList
             var coursemajor =  courseScrappedData.course_outline;
             if (Array.isArray(coursemajor)) {
               for (const rootEleDict of coursemajor) {
                   console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                   const elementsList = rootEleDict.elements;
                   for (const eleDict of elementsList) {
                       console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                       const selectorsList = eleDict.selectors;
                       for (const selList of selectorsList) {
                           console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                           if (Array.isArray(selList) && selList.length > 0) {
                             mgcrs = selList;
                           }
                       }
                   }
               }
           }
             if(mgcrs && mgcrs.length >0){
               majorcrs.majors=mgcrs
                majorcrs.more_details="https://study.unisa.edu.au/msm/"
             }
             else{
               majorcrs.majors= majorcrs.majors
             }
                resJsonData.course_outline =  majorcrs;
             //for minor courseList
                var courseminor =  courseScrappedData.course_outline_minor;
                let mincrs = null;
                if (Array.isArray(courseminor)) {
                  for (const rootEleDict of courseminor) {
                      console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                      const elementsList = rootEleDict.elements;
                      for (const eleDict of elementsList) {
                          console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                          const selectorsList = eleDict.selectors;
                          for (const selList of selectorsList) {
                              console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                              if (Array.isArray(selList) && selList.length > 0) {
                               mincrs = selList;
                              }
                          }
                      }
                  }
              }
                if(mincrs && mincrs.length >0){
                  majorcrs.minors=mincrs
                   majorcrs.more_details="https://study.unisa.edu.au/msm/"
                }
                else{
                 majorcrs.minors=majorcrs.minors
               }
               //for more_details of major-minor
               var more_outline = courseScrappedData.course_outline_more;
               let mor_outline = [];
               if (Array.isArray(more_outline)) {
                 for (const rootEleDict of more_outline) {
                     console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                     const elementsList = rootEleDict.elements;
                     for (const eleDict of elementsList) {
                         console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                         const selectorsList = eleDict.selectors;
                         for (const selList of selectorsList) {
                             console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                             if (Array.isArray(selList) && selList.length > 0) {
                              for( const sel of selList){
                              console.log("more_outttt1"+ sel.innerText);
                              mor_outline.push(sel.innerText);
                              }
                             }
                             else{
                              mor_outline.push(majorcrs.more_details);
                             }
                         }
                     }
                 }
             }
              //for course_outline_more_details 
              if(mor_outline.length > 0){
            majorcrs.majors = mor_outline;
              }
              else{
                majorcrs.majors = majorcrs.majors
              }
                   resJsonData.course_outline =  majorcrs;

                console.log("major==>", resJsonData.course_outline)
          
            break;
            }
            case 'course_overview': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
              // add only if it has valid value
              // let concatnatedRootElementsStrArray = [];
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                // console.log("resJsonData[key] -->11",resJsonData[key]);
              }
              break;
            }

            case 'course_career_outcome': {
              console.log('course_career_outcome matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log('course_career_outcome key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedSelStr = [];
              for (const rootEleDict of rootElementDictList) {
                console.log('course_career_outcome rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                // let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log('course_career_outcome eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  for (const selList of selectorsList) {
                    if (selList && selList.length > 0) {
                      console.log('course_career_outcome selList = ' + JSON.stringify(selList));

                      for (const selItem of selList) {
                        concatnatedSelStr.push(selItem);
                      } // selList
                      console.log('course_career_outcome concatnatedSelStr = ' + concatnatedSelStr);
                      // if (concatnatedSelStr) {
                      // concatnatedSelectorsStr = concatnatedSelStr;
                      // }
                    }
                  } // selectorsList 
                } // elementsList 
              } // rootElementDictList 

              // add only if it has valid value 
              if (concatnatedSelStr && concatnatedSelStr.length > 0) {
                resJsonData.course_career_outcome = concatnatedSelStr;
              } else {
                resJsonData.course_career_outcome = [];
              }
              break;
            }
            
            case 'course_title': {
              console.log("Hardcoded CourseTitle -->", course_name);
              if (course_name) {
                course_name = this.titleCase(course_name);
                resJsonData[key] = String(course_name).trim();
              }
              break;
            }
            case 'course_study_level': {
              const cStudyLevel = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);

              if (cStudyLevel) {
                resJsonData.course_study_level = cStudyLevel.replace("DEGREE LEVEL", '').trim();
              }

              break;
            }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      var NEWJSONSTRUCT = {}, structDict = [];
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase() + resJsonData.program_code.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
        //console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        }
        else {
          NEWJSONSTRUCT.course_intake = "";
        }
        for (let myfees of resJsonData.course_tuition_fee.fees) {
          if (myfees.name == location) {
            NEWJSONSTRUCT.international_student_all_fees = myfees;
          }
        }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        resJsonData.course_id = location_wise_data.course_location_id;
      
        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

       
      }
    
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }


  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = 'scrapeAndFormatCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);

      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }

      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file

      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }

      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
