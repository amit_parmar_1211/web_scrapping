const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
//const mapping = require('./output/Mapping/main');
request = require('request');

class ScrapeCourse extends Course {
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, course_name, category_name, studyLevel) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'course_discipline': {
              // As of not we do not have way to get this field value
              resJsonData.course_discipline = category_name;
              break;
            }
            case 'program_code': {
              let code = "";
              resJsonData.program_code = code;
              break;
            }
            case 'course_academic_requirement':
            case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              const courseAdminReq = {};
              let englishList = [];
              let ieltsDict = {};
              let pteDict = {};
              let tofelDict = {};
              let caeDict = {};
              let ieltsNumber = null;
              // english requirement
              const regEx = /[+-]?\d+(\.\d+)?/g;
              if (courseScrappedData.course_toefl_ielts_score) {
                let ieltsScore = null;
                let courseKeyValIelts = courseScrappedData.course_toefl_ielts_score;
                //const ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                if (Array.isArray(courseKeyValIelts)) {
                  for (const rootEleDict of courseKeyValIelts) {
                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                      console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                      const selectorsList = eleDict.selectors;
                      for (const selList of selectorsList) {
                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        if (Array.isArray(selList) && selList.length > 0) {
                          ieltsScore = selList;
                        }
                      }
                    }
                  }
                } // if (Array.isArray(courseKeyVal))

                console.log("ieltsScore ->", ieltsScore);

                if (ieltsScore && ieltsScore.length > 0) {
                  if (ieltsScore.length == 2) {
                    ieltsDict.name = 'ielts academic (PhD)';
                    ieltsDict.description = ieltsScore[0];
                    ieltsDict.min = 0;
                    ieltsDict.require = await utils.giveMeNumber(ieltsScore[0]);
                    ieltsDict.max = 9;
                    ieltsDict.R = 0;
                    ieltsDict.W = 0;
                    ieltsDict.S = 0;
                    ieltsDict.L = 0;
                    ieltsDict.O = 0;

                    englishList.push(ieltsDict);
                    // extract exact number from string

                    // const matchedStrList = String(ieltsScore[0]).match(regEx);
                    // console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                    // if (matchedStrList && matchedStrList.length > 0) {
                    //   ieltsNumber = Number(matchedStrList[0]);
                    //   console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                    // }
                    ieltsDict = {};
                    ieltsDict.name = 'ielts academic (M.Phil)';
                    ieltsDict.description = ieltsScore[1];
                    ieltsDict.min = 0;
                    ieltsDict.require = await utils.giveMeNumber(ieltsScore[1]);
                    ieltsDict.max = 9;
                    ieltsDict.R = 0;
                    ieltsDict.W = 0;
                    ieltsDict.S = 0;
                    ieltsDict.L = 0;
                    ieltsDict.O = 0;

                    englishList.push(ieltsDict);
                    // extract exact number from string

                    // const matchedStrList = String(ieltsScore[1]).match(regEx);
                    // console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                    // if (matchedStrList && matchedStrList.length > 0) {
                    //   ieltsNumber = Number(matchedStrList[0]);
                    //   console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                    // }
                  } else {
                    console.log("else part ielts-->");
                    ieltsDict.name = 'ielts academic';
                    ieltsDict.description = ieltsScore[0];
                    ieltsDict.min = 0;
                    ieltsDict.require = await utils.giveMeNumber(ieltsScore[0]);
                    ieltsDict.max = 9;
                    ieltsDict.R = 0;
                    ieltsDict.W = 0;
                    ieltsDict.S = 0;
                    ieltsDict.L = 0;
                    ieltsDict.O = 0;
                    englishList.push(ieltsDict);
                    // extract exact number from string

                    // const matchedStrList = String(ieltsScore[0]).match(regEx);
                    // console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                    // if (matchedStrList && matchedStrList.length > 0) {
                    //   ieltsNumber = Number(matchedStrList[0]);
                    //   console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                    // }
                  }

                  //ieltsDic = ieltsDict;

                }
              }


              if (englishList && englishList.length > 0) {
                courseAdminReq.english = englishList;
              }

              // academic requirement
              // if (courseScrappedData.course_academic_requirement) {
              //   const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
              //   console.log('academicReq------->', academicReq);
              //   console.log(funcName + 'academicReq = ' + academicReq);
              //   console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
              //   console.log('\n\r');
              //   if (academicReq && String(academicReq).length > 0) {
              //     courseAdminReq.academic = academicReq.toString().replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ' ');
              //   }
              // }
              var academicReq = null;
              const courseKeyVal11 = courseScrappedData.course_academic_requirement;
              console.log("Academic_req-->" + JSON.stringify(courseKeyVal11))
              if (Array.isArray(courseKeyVal11)) {
                for (const rootEleDict of courseKeyVal11) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      academicReq = selList
                      console.log("acedamiiiii------>", academicReq)
                    }
                  }
                }
              }
              courseAdminReq.academic = (academicReq) ? academicReq.toString() : [];
              resJsonData.course_admission_requirement = courseAdminReq;
              // add english requirement 'english_more_details' link
              const courseKeyVal = courseScrappedData.course_admission_english_more_details;
              let resEnglishReqMoreDetailsJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resEnglishReqMoreDetailsJson = selList[0];
                      }
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))


              //DYNAMIC PROGRESS BAR //CAN BE COPIED//PBT NOT INCLUDED
              //progress bar start
              let penglishList = [];
              let pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
              // extract exact number from string              
              let matchedStrList_ielts, matchedStrList_pte, matchedStrList_tofel, matchedStrList_cae;
              let ieltsNo, pteNo, ibtNo, caeNo;
              if (ieltsDict) {
                matchedStrList_ielts = String(ieltsDict.description).match(regEx);
              }
              if (pteDict) {
                matchedStrList_pte = String(pteDict.description).match(regEx);
              }
              if (tofelDict) {
                matchedStrList_tofel = String(tofelDict.description).match(regEx);
              }
              if (caeDict) {
                matchedStrList_cae = String(caeDict.description).match(regEx);
              }

              if (matchedStrList_ielts && matchedStrList_ielts.length > 0) {
                ieltsNo = Number(matchedStrList_ielts[0]);
              }
              if (matchedStrList_pte && matchedStrList_pte.length > 0) {
                pteNo = Number(matchedStrList_pte[0]);
              }
              if (matchedStrList_tofel && matchedStrList_tofel.length > 0) {
                ibtNo = Number(matchedStrList_tofel[0]);
              }
              if (matchedStrList_cae && matchedStrList_cae.length > 0) {
                caeNo = Number(matchedStrList_cae[0]);
              }
              if (ieltsNumber && ieltsNumber > 0) {
                if (ieltsNo) {
                  pieltsDict.name = 'ielts academic';
                  pieltsDict.value = {};
                  pieltsDict.value.min = 0;
                  pieltsDict.value.require = ieltsNo;
                  pieltsDict.value.max = 9;
                  pieltsDict.value.R = 0;
                  pieltsDict.value.W = 0;
                  pieltsDict.value.S = 0;
                  pieltsDict.value.L = 0;
                  pieltsDict.value.O = 0;
                  penglishList.push(pieltsDict);
                }
                if (ibtNo) {
                  pibtDict.name = 'toefl ibt';
                  pibtDict.value = {};
                  pibtDict.value.min = 0;
                  pibtDict.value.require = ibtNo;
                  pibtDict.value.max = 120;
                  pibtDict.value.R = 0;
                  pibtDict.value.W = 0;
                  pibtDict.value.S = 0;
                  pibtDict.value.L = 0;
                  pibtDict.value.O = 0;
                  penglishList.push(pibtDict);
                }
                if (pteNo) {
                  ppteDict.name = 'pte academic';
                  ppteDict.value = {};
                  ppteDict.value.min = 0;
                  ppteDict.value.require = pteNo;
                  ppteDict.value.max = 90;
                  ppteDict.value.R = 0;
                  ppteDict.value.W = 0;
                  ppteDict.value.S = 0;
                  ppteDict.value.L = 0;
                  ppteDict.value.O = 0;
                  penglishList.push(ppteDict);
                }
                if (caeNo) {
                  pcaeDict.name = 'cae';
                  pcaeDict.value = {};
                  pcaeDict.value.min = 80;
                  pcaeDict.value.require = caeNo;
                  pcaeDict.value.max = 230;
                  pcaeDict.value.R = 0;
                  pcaeDict.value.W = 0;
                  pcaeDict.value.S = 0;
                  pcaeDict.value.L = 0;
                  pcaeDict.value.O = 0;
                  penglishList.push(pcaeDict);
                }
              }
              resJsonData.course_admission_requirement = {};
              if (courseAdminReq.english && courseAdminReq.english.length > 0) {
                resJsonData.course_admission_requirement.english = courseAdminReq.english;
              } else {
                throw new Error("IELTS not found");
                resJsonData.course_admission_requirement.english = [];
              }
              if (courseAdminReq.academic && courseAdminReq.academic.length > 0) {
                resJsonData.course_admission_requirement.academic = [courseAdminReq.academic];
              } else {
                resJsonData.course_admission_requirement.academic = [];
              }
              const entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
              const academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
              const english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
              if (entry_requirements_url && entry_requirements_url.length > 0) {
                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
              }
              if (academic_requirements_url && academic_requirements_url.length > 0) {
                resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;
              }
              if (english_requirements_url && english_requirements_url.length > 0) {
                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
              }
              break;
            }
            case 'course_url': {
              if (courseUrl && courseUrl.length > 0) {
                resJsonData.course_url = courseUrl;
              }
              break;
            }
             case 'course_duration_full_time': {
                              var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                              var fullTimeText = "";
                              let abc = "";
                              const courseKeyVal = courseScrappedData.course_duration_full_time;
                              if (Array.isArray(courseKeyVal)) {
                                  for (const rootEleDict of courseKeyVal) {
                                      console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                      const elementsList = rootEleDict.elements;
                                      for (const eleDict of elementsList) {
                                          console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                          const selectorsList = eleDict.selectors;
                                          for (const selList of selectorsList) {
                                              console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                              if (Array.isArray(selList) && selList.length > 0) {
                                                  fullTimeText = selList[0];
                                                  if (fullTimeText == "available full time / 4 years part time") {
                                                      abc = fullTimeText.replace("available full time / 4 years part time", "2 year full time / 4 years part time").trim();
                                                      console.log("SapratedVlaue===>", abc)
                                                  }
                                                  else {
                                                      abc = fullTimeText
                                                  }
                                                  // console.log("selList111", selList);

                                              }
                                          }
                                      }
                                  }
                              }
                              console.log("fultimmmmmmmmmmmmmmm--->", abc);
                              if (abc && abc.length > 0) {
                                  var resFulltime = abc;
                                  if (resFulltime) {
                                      durationFullTime.duration_full_time = resFulltime;
                                      if (resJsonData.course_title == "Bachelor of High Performance Sport") {
                                          resFulltime = "3 years full time, or part-time equivalent";
                                      }
                                      courseDurationList.push(durationFullTime);
                                      courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                      let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                      console.log("filtered_duration_formated", filtered_duration_formated);
                                      console.log("courseDurationDisplayList2", courseDurationDisplayList);
                                      if (courseDurationList && courseDurationList.length > 0) {

                                          resJsonData.course_duration = resFulltime;

                                      }
                                      if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                          resJsonData.course_duration_display = filtered_duration_formated;

                                          var isfulltime = false, isparttime = false;
                                          filtered_duration_formated.forEach(element => {
                                              if (element.display == "Full-Time") {
                                                  isfulltime = true;
                                              }
                                              if (element.display == "Part-Time") {
                                                  isparttime = true;
                                              }
                                          });
                                          resJsonData.isfulltime = isfulltime;
                                          resJsonData.isparttime = isparttime;

                                      }
                                  }
                              }
                              break;
                          }


            // case 'course_duration_full_time': {
            //   let fullTimeText
            //   function format(searchstring) {
            //     const _units = ["semester", "semesters", "year", "years", "month", "months", "day", "days", "week", "weeks", "quater", "quaters", "trimester", "trimesters"];
            //     var _duration = {};
            //     var seper_string = searchstring.split(" ");
            //     _units.forEach(element => {
            //       if (searchstring.includes(element)) {
            //         _duration.unit = element;
            //         var index = seper_string.indexOf(element);
            //         if (hasNumber(seper_string[index - 1])) {
            //           _duration.duration = seper_string[index - 1];
            //         }
            //         else if (hasNumber(seper_string[index + 1])) {
            //           _duration.duration = seper_string[index + 1];
            //         }
            //       }
            //     });
            //     console.log("## Duration data-->" + JSON.stringify(_duration));
            //     return _duration;
            //     function hasNumber(myString) {
            //       return /\d/.test(myString);
            //     }
            //   }
            //   const courseDurationList = [];
            //   const courseDurationDisplayList = [];
            //   const durationFullTime = {};

            //   let durationFullTimeDisplay = null;

            //   console.log("*************start formating Full Time years*************************");
            //   console.log("courseScrappedData.course_duration_full_time", JSON.stringify(courseScrappedData.course_duration_full_time));
            //   for (let i = 0; i < courseScrappedData.course_duration_full_time[0].elements[0].selectors.length; i++) {
            //     if (courseScrappedData.course_duration_full_time[0].elements[0].selectors[i] && courseScrappedData.course_duration_full_time[0].elements[0].selectors[i].length > 0) {
            //       fullTimeText = courseScrappedData.course_duration_full_time[0].elements[0].selectors[i].toString();

            //       fullTimeText = fullTimeText.replace(';', '');
            //       console.log("fullTimeText", fullTimeText);
            //       let full_year_formated_data = await format(fullTimeText.replace(/[\r\n\t()*]+/g, ' '));
            //       full_year_formated_data.display = "Full-Time";
            //       console.log("full_year_formated_data", full_year_formated_data);
            //       let fulltimeduration = JSON.stringify(full_year_formated_data);
            //       if (fulltimeduration != '[]') {
            //         durationFullTime.duration_full_time = fullTimeText.toString().trim();
            //         courseDurationList.push(durationFullTime);
            //         ///END course duration

            //         ///course duration display
            //         // durationFullTimeDisplay = {};
            //         // durationFullTimeDisplay = full_year_formated_data;
            //         if (full_year_formated_data) {
            //           courseDurationDisplayList.push(full_year_formated_data);
            //           console.log("courseDurationDisplayList", courseDurationDisplayList);
            //         }
            //         ///END course duration display
            //         break;
            //       }
            //     }

            //   }


            //   console.log('***************END formating Full Time years**************************')
            //   console.log("*************start formating Part Time years*************************");
            //   let durationPartTimeDisplay = null;
            //   const durationPartTime = {};
            //   for (let i = 0; i < courseScrappedData.course_duration_part_time[0].elements[0].selectors.length; i++) {
            //     if (courseScrappedData.course_duration_part_time[0].elements[0].selectors[i] && courseScrappedData.course_duration_part_time[0].elements[0].selectors[i].length > 0) {
            //       var partTimeText = courseScrappedData.course_duration_part_time[0].elements[0].selectors[i].toString();
            //       console.log("partTimeText", partTimeText);
            //       let tempCheck = partTimeText.toString().toLowerCase();
            //       if (tempCheck.indexOf("options available") == -1) {
            //         console.log("Entered if");
            //         //part time properly defined
            //         var partyear_formated_data = await format(partTimeText.replace(/[\r\n\t()*]+/g, ' '));
            //         partyear_formated_data.display = "Part-Time";
            //         console.log("partyear_formated_data", partyear_formated_data);
            //         var parttimeduration = JSON.stringify(partyear_formated_data);
            //         if (parttimeduration != '[]') {
            //           durationPartTime.duration_part_time = partTimeText.toString().trim();
            //           courseDurationList.push(durationPartTime);
            //           ///End Parttime course duration

            //           ///partitme course duration display
            //           // durationPartTimeDisplay = {};
            //           // durationPartTimeDisplay = partyear_formated_data[0];

            //           if (partyear_formated_data) {
            //             courseDurationDisplayList.push(partyear_formated_data);
            //           }
            //           ///END parttime course duration display
            //           break;
            //         }
            //       } else {
            //         //part time=options available
            //         console.log("else");
            //         courseDurationList[0].duration_full_time += " and part time options available";
            //       }
            //     }
            //   }

            //   console.log('***************END formating Part Time years**************************')




            //   console.log('***************END formating Part Time years**************************')

            //   if (courseDurationList && courseDurationList.length > 0) {
            //     resJsonData.course_duration = fullTimeText //+ partTimeText;
            //   }
            //   console.log("courseDurationDisplayList -->", courseDurationDisplayList);
            //   let formatDuration = await format_functions.getfilterduration(courseDurationDisplayList);
            //   if (formatDuration && formatDuration.length > 0) {

            //     resJsonData.course_duration_display = formatDuration;
            //     var isfulltime = false, isparttime = false;
            //     formatDuration.forEach(element => {
            //       if (element.display == "Full-Time") {
            //         isfulltime = true;
            //       }
            //       if (element.display == "Part-Time") {
            //         isparttime = true;
            //       }
            //     });
            //     resJsonData.isfulltime = isfulltime;
            //     resJsonData.isparttime = isparttime;
            //   }

            //   break;
            // }
            case 'course_outline': {
              console.log("course_outline--->")
              const outline = {
                majors: [],
                minors: [],
                more_details: ""
              };

              var major, minor
              var intakedata1 = {};
              const courseKeyVal = courseScrappedData.course_outline_major;
              console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

              let resScholarshipJson = null;


              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList;
                        console.log("resScholarshipJson", resScholarshipJson)
                      }
                    }
                  }
                }
              }
              if (resScholarshipJson) {
                major = resScholarshipJson
              } else {
                major = []
              }




              const courseKeyVal1 = courseScrappedData.course_outline_minors;
              let resScholarshipJson1 = null;
              if (Array.isArray(courseKeyVal1)) {
                for (const rootEleDict of courseKeyVal1) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson1 = selList;

                      }
                    }
                  }
                }
              }
              if (resScholarshipJson1) {
                minor = resScholarshipJson1
              } else {
                minor = []
              }

              let m_d = resJsonData.course_url;
              console.log("md--------->", m_d)

              var intakedata1 = {};
              intakedata1.majors = major;
              intakedata1.minors = minor;
              intakedata1.more_details = m_d;
              resJsonData.course_outline = intakedata1;


              break;
            }

            case 'application_fee':
              {
                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                resJsonData.application_fee = ""
                break;
              }

            // case 'course_campus_location': { // Location Launceston             
            //   console.log(' course_campus_location ' + key);
            //   //let Cricos = resJsonData.course_cricos_code
            //   let Cri;
            //   let split_campus2, replace_val;
            //   const rootElementDictList = courseScrappedData[key];
            //   console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));
            //   let concatnatedSelectorsStr = null;
            //   let Cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
            //   console.log("Cricos----->", Cricos)
            //   if (Cricos.includes("CRICOS CODE:")) {
            //     Cri = Cricos.split("CRICOS CODE:")[1]

            //   }
            //   else {
            //     Cri = Cricos;
            //   }
            //   for (const rootEleDict of rootElementDictList) {
            //     console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
            //     const elementsList = rootEleDict.elements;
            //     // let concatnatedElementsStr = null;
            //     for (const eleDict of elementsList) {
            //       console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
            //       const selectorsList = eleDict.selectors;
            //       for (const selList of selectorsList) {
            //         if (selList && selList.length > 0) {
            //           console.log('course_campus_location selList = ' + JSON.stringify(selList));
            //           let concatnatedSelStr = [];
            //           for (let selItem of selList) {
            //             selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ' ').split(",");
            //             for (let sel of selItem) {
            //               sel = sel.trim();



            //               let newcampus = [];
            //               for (let campus of sel) {
            //                 if (!newcampus.includes(campus)) {
            //                   replace_val = String(campus).replace(/Vic/g, '').trim();
            //                   if (!replace_val == '') {
            //                     console.log("##123456-->" + replace_val)
            //                     const location_hardcode = await utils.getValueFromHardCodedJsonFile('campuses');
            //                     console.log("location_hardcode-------->>>>", location_hardcode);
            //                     let tmpvar123 = location_hardcode.filter(val => {
            //                       return val.name.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase() == replace_val.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase()
            //                     });
            //                     console.log("##tmpvar123-->" + JSON.stringify(tmpvar123[0].Location))
            //                     let loc_val = tmpvar123[0].Location;
            //                     console.log("##Campuscampus-->" + loc_val)
            //                     newcampus.push(loc_val);
            //                   }
            //                 }
            //               }


            //               if (concatnatedSelStr.includes(sel) == false && sel.toLowerCase().trim() != "external" && sel.toLowerCase().trim() != "online" && sel.toLowerCase().trim() != "online." && sel.indexOf("International students on a student visa must not enrol ") == -1 && sel.indexOf("Available online") == -1) {
            //                 // concatnatedSelStr.push(sel);
            //                 concatnatedSelStr.push({
            //                   "name": sel,
            //                   "code": Cri.trim()

            //                 });
            //               }
            //             }

            //           } // selList
            //           console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);
            //           if (concatnatedSelStr) {
            //             concatnatedSelectorsStr = concatnatedSelStr;
            //           }
            //           //   concatnatedSelStr.push({
            //           //     "name": sel,
            //           //     "city": "",
            //           //     "state": "",
            //           //     "country": "",
            //           //     "iscurrent": false
            //           // });
            //         }
            //       } // selectorsList                  
            //     } // elementsList                
            //   } // rootElementDictList 

            //   // add only if it has valid value              
            //   if (concatnatedSelectorsStr && concatnatedSelectorsStr.length > 0) {
            //     resJsonData.course_campus_location = concatnatedSelectorsStr;
            //   }
            //   else {
            //     throw new Error("Campus location not found");
            //   }
            //   break;

            // }
            case 'course_campus_location': { // Location Launceston
              var campLocationText; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
              let course_cricos_code = [];
              let split_campus2, replace_val, Cri;
              const courseKeyVal = courseScrappedData.course_campus_location;
              let Cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
              console.log("Cricos----->", Cricos)
              if (Cricos.includes("CRICOS CODE:")) {
                Cri = Cricos.split("CRICOS CODE:")[1]

              }
              else {
                Cri = Cricos;
              }

              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      //if (Array.isArray(selList) && selList.length > 0) {
                      campLocationText = selList;
                      ///}
                      if (String(campLocationText).includes(",") && String(campLocationText).includes(",")) {
                        split_campus2 = String(campLocationText).split(',');
                        // let split_campus123 = String(split_campus2).split(',');
                        //campLocationText = split_campus2;
                        console.log("split_campu@@@@1111111------>>>>>", campLocationText);
                      }
                      else {
                        split_campus2 = campLocationText
                        console.log("else======>", split_campus2)
                      }
                    }
                  }
                }
              }

              var earray = [];
              for (var i in split_campus2) {
                if (earray.indexOf(split_campus2[i]) === -1) {
                  earray.push(split_campus2[i]);
                }
                console.log("earray---->>", earray);
              }


              let newcampus = [];
              for (let campus of earray) {
                if (!newcampus.includes(campus)) {
                  replace_val = String(campus).replace(/Vic/g, '').trim();
                  if (!replace_val == '') {
                    console.log("##123456-->" + replace_val)
                    const location_hardcode = await utils.getValueFromHardCodedJsonFile('campuses');
                    console.log("location_hardcode-------->>>>", location_hardcode);
                    let tmpvar123 = location_hardcode.filter(val => {
                      return val.name.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase() == replace_val.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase()
                    });
                    console.log("##tmpvar123-->" + JSON.stringify(tmpvar123[0].Location))
                    let loc_val = tmpvar123[0].Location;
                    console.log("##Campuscampus-->" + loc_val)
                    newcampus.push(loc_val);
                  }
                }
              }




              if (newcampus && newcampus.length > 0) {
                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                resJsonData.course_study_mode = "On campus";
                //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                var campusedata = [];
                newcampus.forEach(element => {
                  campusedata.push({
                    "name": element,
                    "code": Cri.trim()

                  })
                });
                resJsonData.course_campus_location = campusedata;
                // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                var study_mode = [];
                // campLocationValTrimmed = campLocationValTrimmed.toLowerCase();

                // campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                if (campLocationValTrimmed.trim().length > 0) {
                  study_mode.push('On campus');
                }
                resJsonData.course_study_mode = study_mode.toString();//.join(',');
                console.log("## FInal string-->" + campLocationValTrimmed);
                // }
              } // if (campLocationText && campLocationText.length > 0)
              // }
              break;
            }


            case 'course_tuition_fee':
            case 'page_url':

            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_duration_part_time':
            case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              //CUSTOMIZED CODE DON'T COPY  
              const courseTuitionFee = {};
              const feesList = [];
              let feesDict1 = {
                international_student: {}
              };

              console.log('******************************Start Tution fee****************************');
              const rootElementDictList = courseScrappedData['course_cricos_code'];

              let concatnatedRootElementsStr = null;
              let feesFlag = false;
              for (const rootEleDict of rootElementDictList) {
                const elementsList = rootEleDict.elements;
                //console.log("elementsList",elementsList);
                for (const eleDict of elementsList) {
                  const selectorsList = eleDict.selectors;
                  //console.log("selectorsList",selectorsList);
                  for (const selList of selectorsList) {
                    //console.log("R selList",selList);
                    if (selList[0] != null) {
                      var crcodeval = selList.toString();
                      console.log('cricos code value :' + crcodeval);
                      if (crcodeval.indexOf('CRICOS CODE') > -1) {
                        var crcode = selList.toString().split(':');
                        const cricoscode = crcode[1];
                        concatnatedRootElementsStr = cricoscode;
                        console.log('Cricos code :' + concatnatedRootElementsStr);
                        feesFlag = true;
                        break;
                      }
                      else {
                        const cricoscode = selList.toString();
                        concatnatedRootElementsStr = cricoscode;
                        console.log('Cricos code :' + concatnatedRootElementsStr);
                        feesFlag = true;
                        break;
                      }
                    }
                  }
                }
              }
              console.log("end of all fors::", concatnatedRootElementsStr);
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0 && feesFlag == true) {

                console.log("concatnatedRootElementsStr", concatnatedRootElementsStr);
                var feesIntStudent = '', totalfees;

                const cricosecode = concatnatedRootElementsStr.toString();
                const fixedFeesDict = await utils.getValueFromTutionfeeJsonFile();

                for (var i = 0; i < fixedFeesDict.length; i++) {
                  if (fixedFeesDict[i].cricos_code == cricosecode.trim()) {
                    console.log('R fixedFeesDict :' + fixedFeesDict[i].cricos_code + ' Amount :' + fixedFeesDict[i].course_fee);
                    feesIntStudent = fixedFeesDict[i].course_fee;
                    totalfees = fixedFeesDict[i].total_fee;
                    console.log("R totalfees", totalfees);
                    // break;
                  }
                }
                //annual fees extract digits
                const regEx = /\d/g;
                if (feesIntStudent.length > 0) { // extract only digits
                  console.log("international_student" + feesIntStudent)
                  // feesDict1.international_student = feesIntStudent;
                  const feesWithDollorTrimmed = String(feesIntStudent).trim();
                  console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                  const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                  console.log(funcName + 'feesVal = ' + feesVal);
                  if (feesVal) {
                    const myval = feesVal.split(".");

                    let feesValNum = myval[0].match(regEx);
                    if (feesValNum) {
                      console.log(funcName + 'feesValNum = ' + feesValNum);
                      feesValNum = feesValNum.join('');
                      console.log(funcName + 'feesValNum = ' + feesValNum);
                      let feesNumber = null;
                      if (feesValNum.includes(',')) {
                        feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                      } else {
                        feesNumber = feesValNum;
                      }
                      console.log(funcName + 'feesNumber = ' + feesNumber);
                      if (Number(feesNumber)) {
                        feesDict1.international_student = ({
                          amount: Number(feesNumber),
                          duration: 1,
                          unit: "Year",

                          description: feesIntStudent

                        });
                      }
                    }
                  }
                } else {
                  feesDict1.international_student = ({
                    amount: 0,
                    duration: 1,
                    unit: "Year",

                    description: feesIntStudent

                  });
                }

                //feesDict.international_student= finalfeesInt;                
                //const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                //let feesIntStudent = feesDict.international_student.toString();
                // if (feeYear && feeYear.length > 0) {
                //   courseTuitionFee.year = feeYear;
                // }
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict1.fee_duration_years = feesDuration;
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict1.currency = feesCurrency;
                }

                if (feesDict1.international_student) { // if we have fees value then add following supporting attributes
                  if (feesIntStudent.length > 0) { // extract only digits
                    console.log("international_student" + feesIntStudent)
                    //     feesDict1.international_student_all_fees = [];

                    if (totalfees) {
                      //  feesDict1.international_student_all_fees.push("Estimated Total Cost: " + totalfees);
                    }

                  }
                  if (feesDict1) {
                    var campus = resJsonData.course_campus_location;
                    for (let loc of campus) {
                      feesList.push({ name: loc.name, value: feesDict1 });
                    }
                  }
                  if (feesList && feesList.length > 0) {
                    courseTuitionFee.fees = feesList;
                  }
                  console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                  if (courseTuitionFee) {
                    resJsonData.course_tuition_fee = courseTuitionFee;
                  }
                }
                else {
                  feesDict1.international_student = ({
                    amount: 0,
                    duration: 1,
                    unit: "Year",
                    description: ""

                  });
                  feesDict1.international_student_all_fees = [];
                  if (feesDict) {
                    var campus = resJsonData.course_campus_location;
                    for (let loc of campus) {
                      feesList.push({ name: loc.name, value: feesDict1 });
                    }
                  }
                  if (feesList && feesList.length > 0) {
                    courseTuitionFee.fees = feesList;
                  }
                  if (courseTuitionFee) {
                    resJsonData.course_tuition_fee_amount = feesDict1.international_student;
                    resJsonData.course_tuition_fee = courseTuitionFee;
                  }
                }
                break;

              }
              break;
            }
            case 'course_study_mode': { // Location Launceston

              resJsonData.course_study_mode = "On campus";

              break;
            }
            case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
              const courseIntakeDisplay = [];

              let courseKeyVal_intake = courseScrappedData.course_intake;
              let courseintake;
              let rep_intake;
              if (Array.isArray(courseKeyVal_intake)) {
                for (const rootEleDict of courseKeyVal_intake) {
                  console.log(funcName + '\n\r rootEleDict ieltsDict= ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict ieltsDict= ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList ieltsDict= ' + JSON.stringify(selList));
                      if (Array.isArray(selList)) {
                        for (let sel of selList) {
                          if (courseintake != sel) {
                            courseintake = sel;
                            console.log("courseintake====>", courseintake)
                          }
                        }
                      }
                    }
                  }
                }
              }
              if (courseintake.includes("Next intake is on")) {
                rep_intake = courseintake.replace("Next intake is on", "").trim();
                console.log("rep_intake===>", rep_intake)
              }


              console.log("R locationArray", campus);
              console.log('************************Start course_intake******************************');
              console.log('courseIntakeStr : ' + rep_intake);
              if (rep_intake && rep_intake.length > 0) {
                resJsonData.course_intake = rep_intake;
                const intakeStrList = String(rep_intake).split('\n');
                const intakeSplit = String(intakeStrList).split(',');
                console.log('course_intake intakeStrList = ' + JSON.stringify(intakeSplit));
                if (intakeSplit && intakeSplit.length > 0) {
                  for (var part of intakeSplit) {
                    console.log("R part", part);
                    part = part.trim();
                    courseIntakeDisplay.push(part);
                  } // for
                }
                console.log('course_intake courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));
              }
              var campus = resJsonData.course_campus_location;

              if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
                resJsonData.course_intake = {};
                resJsonData.course_intake.intake = [];
                for (let location11 of campus) {
                  resJsonData.course_intake.intake.push({
                    "name": location11.name,
                    "value": courseIntakeDisplay
                  });
                }
              }
              console.log("intakes123 -->", resJsonData.course_intake.intake);
              let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
              let formatedIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");
              console.log("Intakes --> ", JSON.stringify(formatedIntake));
              var intakedata = {};
              intakedata.intake = formatedIntake;
              intakedata.more_details = more_details;
              resJsonData.course_intake = intakedata;
              break;
            }
            // case 'course_intake': {
            //   const courseIntakeDisplay = [], intake_array_sub = [];
            //   // existing intake value

            //   let courseKeyVal_intake = courseScrappedData.course_intake;
            //   // let courseKeyVal_intake = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
            //   let courseintake = [];
            //   if (Array.isArray(courseKeyVal_intake)) {
            //     for (const rootEleDict of courseKeyVal_intake) {
            //       console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
            //       const elementsList = rootEleDict.elements;
            //       for (const eleDict of elementsList) {
            //         console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
            //         const selectorsList = eleDict.selectors;
            //         for (const selList of selectorsList) {
            //           console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
            //           if (Array.isArray(selList) && selList.length > 0) {
            //             for (let intake_text of selList) {
            //               courseintake = intake_text;
            //               console.log(funcName + '\n\r courseintake = ' + JSON.stringify(courseintake));
            //               if (String(courseintake.includes('Next intake is on'))) {
            //                 let replace_inteake = String(courseintake).replace('Next intake is on', '').trim();
            //                 intake_array_sub.push(replace_inteake);
            //                 console.log('intake_array_subintake_array_sub : ', intake_array_sub);
            //               } else {
            //                 intake_array_sub.push(courseintake);
            //                 console.log('intake_array_sub : ', intake_array_sub);
            //               }
            //             }
            //           }
            //         }
            //       }
            //     }
            //   }

            //   console.log("R locationArray", campus);
            //   console.log('************************Start course_intake******************************');
            //   console.log('courseIntakeStr : ' + intake_array_sub[0]);
            //   // let intake_array = [], intake_array_new = [];
            //   // for (intake_array of intake_array_sub[0]) {
            //   //     intake_array_new = intake_array;
            //   //     console.log('intake_array_new--->>>', intake_array_new);
            //   // }
            //   if (intake_array_sub && intake_array_sub.length > 0) {
            //     resJsonData.course_intake = intake_array_sub;
            //     const intakeStrList = String(intake_array_sub).split('\n');
            //     const intakeSplit = String(intakeStrList).split(',');
            //     console.log('course_intake intakeStrList = ' + JSON.stringify(intakeSplit));
            //     if (intakeSplit && intakeSplit.length > 0) {
            //       for (var part of intakeSplit) {

            //         console.log("R part", part);
            //         part = part.trim();
            //         courseIntakeDisplay.push(part);
            //       } // for
            //     }
            //     console.log('course_intake courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));
            //   }
            //   var campus = resJsonData.course_campus_location;
            //   if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
            //     resJsonData.course_intake = {};
            //     resJsonData.course_intake.intake = [];
            //     for (let location11 of campus) {
            //       resJsonData.course_intake.intake.push({
            //         "name": location11.name,
            //         "value": courseIntakeDisplay
            //       });
            //     }
            //   }
            //   console.log("intakes123 -->", resJsonData.course_intake.intake);
            //   let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
            //   let formatedIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");

            //   console.log("Intakes --> ", JSON.stringify(formatedIntake));
            //   var intakedata = {};
            //   intakedata.intake = formatedIntake;
            //   intakedata.more_details = more_details;
            //   resJsonData.course_intake = intakedata;
            //   break;

            // }


            case 'course_scholarship': {
              const courseKeyVal = courseScrappedData.course_scholarship;
              let resScholarshipJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resScholarshipJson) {
                resJsonData.course_scholarship = resScholarshipJson;
              }
              break;
            }
            case 'univ_name': {
              const courseKeyVal = courseScrappedData.univ_name;
              let resUnivName = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivName = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivName) {
                resJsonData.univ_name = resUnivName;
              }
              break;
            }
            case 'univ_logo': {
              const courseKeyVal = courseScrappedData.univ_logo;
              let resUnivLogo = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivLogo = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivLogo) {
                resJsonData.univ_logo = resUnivLogo;
              }
              break;
            }
            case 'course_accomodation_cost': {
              const courseKeyVal = courseScrappedData.course_accomodation_cost;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_accomodation_cost = resAccomodationCostJson;
              }
              break;
            }
            case 'course_overview': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
              // add only if it has valid value
              // let concatnatedRootElementsStrArray = [];
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                resJsonData[key] = String(concatnatedRootElementsStr).replace(/[\r\n\t()*]+/g, ' ').trim();
                // console.log("resJsonData[key] -->11",resJsonData[key]);
              } else {
                resJsonData[key] = "";
              }
              break;
            }
            case 'course_country': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
              // add only if it has valid value
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
              }
              break;
            }
            case 'course_career_outcome': {
              console.log('course_career_outcome matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log('course_career_outcome key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedSelectorsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log('course_career_outcome rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                // let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log('course_career_outcome eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  for (const selList of selectorsList) {
                    if (selList && selList.length > 0) {
                      console.log('course_career_outcome selList = ' + JSON.stringify(selList));
                      let concatnatedSelStr = [];
                      for (const selItem of selList) {
                        concatnatedSelStr.push(selItem);
                      } // selList
                      console.log('course_career_outcome concatnatedSelStr = ' + concatnatedSelStr);
                      if (concatnatedSelStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      }
                    }
                  } // selectorsList                  
                } // elementsList                
              } // rootElementDictList 

              // add only if it has valid value              
              if (concatnatedSelectorsStr && concatnatedSelectorsStr.length > 0) {
                resJsonData.course_career_outcome = concatnatedSelectorsStr;
              } else {
                resJsonData.course_career_outcome = [];
              }
              break;
            }

            case 'course_title': {
              var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
              if (title.includes(' (')) {
                const title_text = title.replace('(', ' (');
                console.log("splitStr@@@2" + title_text);
                title = title_text;
                console.log("splitStr@@@1" + title);

              }
              var ctitle = format_functions.titleCase(title).trim();
              // var ctitle1 = ctitle.replace('(','').replace(')','').trim();
              var ctitle2 = ctitle.replace(' (', '(');
              var ctitle3 = ctitle2.replace(' (', '(').trim();
              console.log("ctitle@@@", ctitle3.trim());
              resJsonData.course_title = ctitle3
              break;
            }
            case 'course_study_level': {
              resJsonData.course_study_level = studyLevel;
              break;
            }

            // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      ////start genrating new file location wise

      var NEWJSONSTRUCT = {}, structDict = [];
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        // NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
        // console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        }
        else {
          NEWJSONSTRUCT.course_intake = "[]";
        }
        for (let myfees of resJsonData.course_tuition_fee.fees) {
          if (myfees.name == location) {
            NEWJSONSTRUCT.international_student_all_fees = myfees;
          }
        }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        resJsonData.course_id = location_wise_data.course_location_id;

        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
        try {
          if (appConfigs.ISINSERT) {
            request.post({
              "headers": { "content-type": "application/json" },
              "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
              "body": JSON.stringify(resJsonData)
            }, (error, response, body) => {
              if (error) {
                console.log(error);
              }
              var myresponse = JSON.parse(body);
              if (!myresponse.flag) {
                var errorlog = [];
                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                  errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                  errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                }
                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
              }
              console.log("success save data! " + JSON.stringify(body));
            });
          }
          else if (appConfigs.ISREPLACE) {
            request.post({
              "headers": { "content-type": "application/json" },
              "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
              "body": JSON.stringify(resJsonData)
            }, (error, response, body) => {
              if (error) {
                console.log(error);
              }
              var myresponse = JSON.parse(body);
              console.log("MyMessage-->" + JSON.stringify(myresponse))
              if (!myresponse.flag) {
                var errorlog = [];
                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                  errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                  errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                }
                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
              }
              console.log("success save data! " + JSON.stringify(body));
            });
          }
          else if (appConfigs.ISUPDATE) {
            request.post({
              "headers": { "content-type": "application/json" },
              "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
              "body": JSON.stringify(resJsonData)
            }, (error, response, body) => {
              if (error) {
                console.log(error);
              }
              var myresponse = JSON.parse(body);
              if (!myresponse.flag) {
                var errorlog = [];
                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                  errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                  errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                }
                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
              }
              console.log("success save data! " + JSON.stringify(body));
            });
          }
        }
        catch (err) {
          console.log("#### try-catch error--->" + err);
        }


      }
      // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
      return resJsonData;

      ///end grnrating new file location wise
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    console.log("Reached scrapeAndFormatCourseDetails");
    const funcName = 'scrapeAndFormatCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);

      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }
      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file

      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }

      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.studyLevel);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };