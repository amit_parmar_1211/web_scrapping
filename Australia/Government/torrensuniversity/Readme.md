**Torrens University: Total Courses Details As On 25 Mar 2019**
* Total Courses = 111
* Total available courses = 111

**Notes**
* fees is read from json file which is generated from the downloaded pdf.
* Study mode scrapped with courselist.
* no program code is given.
* only ielts score is given. 
* Separate file for list of categories is generated.
* Modification in scrape course list function.
* Study mode included in courselist
* 3 different layouts
* 1 known case of multi cricos code


**2019 & conditions**
* ScrapeCourse.js for intake 2019 is statically added.
* Link of academic calendar is specifically for 2019.
* fees pdf is for 2019 fees only.
* In fees.json find and replace See * with NA.