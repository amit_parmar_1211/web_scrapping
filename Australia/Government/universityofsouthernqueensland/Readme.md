**University of outhern Queensland : Total Courses Details As On 01 April 2019**
* Course Link : https://www.usq.edu.au/study
* pathway programs add manually in courselist file
* link : https://www.usq.edu.au/study/degrees/pathway-programs
* copy extra courses from ToAddCourse.json file in selectors folder
* Total Courses = 500
* Failed list = 161
**Doubts**
* External and Cricos given Campus not given : https://www.usq.edu.au/study/degrees/bachelor-of-science/physical-sciences/international#fees
* https://www.usq.edu.au/study/degrees/graduate-diploma-of-science/physics-astronomy/international
* https://www.usq.edu.au/study/degrees/master-of-science/astrophysics/international
* https://www.usq.edu.au/study/degrees/graduate-diploma-of-science/environment-sustainability/international
* https://www.usq.edu.au/study/degrees/master-of-science/environment-sustainability/international

**Mapping is done based on program code and studylevel**
**if ielts does not match anywhere default mapping is given in 3.1 English Language Proficiency Program Category**


**Type of Errors**
* Error: Invalid value of parameter, invalidParamsList = [null] [131 such errors] --> Cricos not given and COURSE CAMPUS LOCATION not given Eg: https://www.usq.edu.au/study/degrees/graduate-certificate-of-professional-communication/international

* Error: Invalid courseScrappedData, courseScrappedData = null [Cricos not given] --> 132 fail
* Error: Invalid value of parameter, invalidParamsList = [null] [Campus not given] --> 28 fail