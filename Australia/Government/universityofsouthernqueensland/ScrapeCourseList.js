const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');

const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    // "https://www.usq.edu.au/study/"
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
      var totalCourseList = [];
      var mainCategory = [], redirecturl = [], Sub_category_href = [], redirecturl_sub = [];
      //scrape main category
      const maincategoryselector = "//*[@class='col-sm-6 no-gutter']//a";
      var category = await page.$x(maincategoryselector);
      console.log("Total categories-->" + category.length);
      for (let link of category) {
        var categorystringmain = await page.evaluate(el => el.innerText, link);
        var categorystringmainurl = await page.evaluate(el => el.href, link);
        mainCategory.push(categorystringmain.trim());
        redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
      }
      console.log("redirecturl ->", redirecturl);
      for (let catc_sub of redirecturl) {
        await page.goto(catc_sub.href, { timeout: 0 });
        // const subcategort_href = "//*[@class='button-grid__button-container col-sm-4']//a/span[1][not(contains(text(),'Online'))]";
        const subcategort_href = "//*[@class='button-grid__button-container col-sm-4']//a[not(contains(@href,'/study/degrees/education-and-teaching/online-learning'))]"
        const select_suyb = await page.$x(subcategort_href)
        for (let sub_cata of select_suyb) {
          var categorystringmain_sub = await page.evaluate(el => el.innerText, sub_cata);
          var categorystringmainurl_sub = await page.evaluate(el => el.href, sub_cata);
          console.log("categorystringmain_sub -> ", categorystringmain_sub);
          redirecturl_sub.push({ innerText: categorystringmain_sub.trim(), href: categorystringmainurl_sub, category: catc_sub.innerText });
        }
        //  console.log("Sub_category-->", JSON.stringify(redirecturl_sub));
      }
      // scrape courses
      for (let catcug_sub of redirecturl_sub) {
        await page.goto(catcug_sub.href, { timeout: 0 });
        const selectorug = "//*[@class='content-tabs__wrapper']/ul/li/a[contains(text(),'Undergraduate')]";
        const course_href = "//*[@id='undergraduate']/div/div/a"
        var titles = await page.$x(selectorug)
        var title_href = await page.$x(course_href);
        for (let title_inner_val of title_href) {
          var categorystring = await page.evaluate(el => el.innerText, title_inner_val);
          if (categorystring.includes('and')) {
            let element = categorystring.replace('and', '').trim();
            categorystring = element;
          }
          if (categorystring.includes('Major')) {
            let element = categorystring.replace('Major', '').trim();
            categorystring = element;
          }
          if (categorystring.includes('&')) {
            let element = categorystring.replace('&', '').trim();
            categorystring = element;
          }
          if (categorystring.includes('/')) {
            let element = categorystring.replace('/', '').trim();
            categorystring = element;
          }
          if (categorystring.includes(',')) {
            let element = categorystring.replace(',', '').trim();
            categorystring = element;
          }
          if (categorystring.includes('Specialisation')) {
            let element = categorystring.replace('Specialisation', '').trim();
            categorystring = element;
          }
          var categoryurl = await page.evaluate(el => el.href, title_inner_val);
          var studylevel = await page.evaluate(el => el.innerText, titles[0])
          // console.log("UG-->", JSON.stringify(studylevel))
          totalCourseList.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: catcug_sub.category, study_level: studylevel });
        }
      }
      //console.log("UG-->", JSON.stringify(totalCourseList))

      for (let catcpg_sub of redirecturl_sub) {
        await page.goto(catcpg_sub.href, { timeout: 0 });
        const selectorpg = "//*[@class='content-tabs__wrapper']/ul/li/a[contains(text(),'Postgraduate')]";
        const selector = "//*[@id='postgraduate']/div/div/a";
        var titles = await page.$x(selectorpg)
        var title = await page.$x(selector);
        for (let t of title) {
          var categorystring = await page.evaluate(el => el.innerText, t);
          var categoryurl = await page.evaluate(el => el.href, t);
          var studylevel = await page.evaluate(el => el.innerText, titles[0])
          totalCourseList.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: catcpg_sub.category, study_level: studylevel });
        }
      }

      for (let catcrs_sub of redirecturl_sub) {
        await page.goto(catcrs_sub.href, { timeout: 0 });
        const selectorrs = "//*[@class='content-tabs__wrapper']/ul/li/a[contains(text(),'Research')]";
        const selector = "//*[@id='research']/div/div/a";
        var titles = await page.$x(selectorrs)
        var title = await page.$x(selector);
        for (let t of title) {
          var categorystring = await page.evaluate(el => el.innerText, t);
          var categoryurl = await page.evaluate(el => el.href, t);
          var studylevel = await page.evaluate(el => el.innerText, titles[0])
          totalCourseList.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: catcrs_sub.category, study_level: studylevel });
        }
      }
      await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(mainCategory))
      await fs.writeFileSync("./output/universityofsouthernqueensland_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [], study_level: totalCourseList[i].study_level });
        }
      }
      await fs.writeFileSync("./output/universityofsouthernqueensland_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {
            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/universityofsouthernqueensland_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
      return uniqueUrl;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // refers to https://futurestudents.csu.edu.au/courses/all
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        // break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');

      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
