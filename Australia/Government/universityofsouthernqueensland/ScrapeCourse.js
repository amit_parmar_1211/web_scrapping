const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
// const mapping = require('./output/Mapping/main');
request = require('request');

class ScrapeCourse extends Course {
  static titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      // You do not need to check if i is larger than splitStr length, as your for does that for you
      // Assign it back to the array
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
  }
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, study_level) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'program_code': {
              const courseKeyVal = courseScrappedData.program_code;
              console.log("courseScrappedData.program_code = ", courseScrappedData.program_code);
              var program_code = "";
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        program_code = selList;
                        // for (let sel of selList) {
                        //   if (sel.indexOf('\n') > -1) {
                        //     let splitNewLine = sel.split('\n');
                        //     console.log("SplitNewLine -->", splitNewLine);
                        //     let splitBracket = splitNewLine[0].split('(');
                        //     sel = splitBracket[1].replace(/\(|\)/g, "").trim();
                        //     console.log("selItem -->", sel);
                        //     // .replace(/\(|\)/g, "").trim();
                        //     program_code = sel;
                        //   } else {
                        //     let splitBracket = sel.split('(');
                        //     sel = splitBracket[1].replace(/\(|\)/g, "").trim();
                        //     console.log("selItem -->", sel);
                        //     // .replace(/\(|\)/g, "").trim();
                        //     program_code = sel;
                        //   }
                        // }
                      }
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))
              console.log("program_codeprogram_code --> ", program_code.length);
              let newProgramcode = "";
              if (program_code && program_code.length > 0) {
                for (let pcode of program_code) {
                  if (pcode.indexOf('\n') > -1) {
                    pcode = pcode.split('\n')[0];
                  }
                  let pCodeIndex1 = pcode.lastIndexOf('(');
                  let pCodeIndex2 = pcode.lastIndexOf(')');
                  newProgramcode = pcode.slice(pCodeIndex1+1, pCodeIndex2);
                }
              }
              if (newProgramcode.length > 0) {
                resJsonData.program_code = newProgramcode;
              } else {
                resJsonData.program_code = "";
              }
              // let program_code_array = [];
              // if (program_code == '' || program_code == undefined) {
              //   for (let program_val of program_code) {

              //     resJsonData.program_code = program_val;

              //   }
              //   //  resJsonData.program_code = program_code_array;
              // } else {
              //   program_code_array.push(program_code);
              //   for (let program_val of program_code_array) {

              //     resJsonData.program_code = program_val;

              //   }
              //   // resJsonData.program_code = program_code_array;
              // }

              break;
            }
            case 'course_discipline': {
              // As of not we do not have way to get this field value
              resJsonData.course_discipline = course_category;
              break;
            }
            case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              let courseAdminReq = {};
              const englishList = [];
              var myscrore = '';
              let pbtNumber = null; let pteNumber = null; let caeNumber = null; let ibtNumber = null;
              let ieltsNumber = null;
              var ieltsfinaloutput = null;
              let ieltsMappingFromHardcodeFile = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
              let ieltsScore = courseScrappedData.course_toefl_ielts_score[0].elements[0].selectors[0][0];
              // console.log("courseScrappedData.course_toefl_ielts_score -->>", courseScrappedData.course_toefl_ielts_score);
              if (courseScrappedData.course_toefl_ielts_score[0].elements[0].selectors[0][0] != undefined) {
                console.log("InFI@@@@")
                console.log('Course english language requirment :' + JSON.stringify(courseScrappedData.course_toefl_ielts_score));
                //const ieltsScore = await Course.extractValueEnglishlanguareFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                if (ieltsScore && ieltsScore.length > 0) {
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(ieltsScore).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    ieltsNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                  }

                }
                myscrore = await utils.getMappingScore(ieltsNumber);
              } else {
                console.log("InElse@@@@")
                // if ielts is not given mapping is based on program code
                // console.log("HArdCodedData--->", ieltsMappingFromHardcodeFile)
                for (let record of ieltsMappingFromHardcodeFile) {
                  let splitProgramCode = record.split(' - ');
                  if (resJsonData.program_code.includes(splitProgramCode[0].trim())) {
                    let ieltsStr = splitProgramCode[1];
                    if (ieltsStr && ieltsStr.length > 0) {
                      // extract exact number from string
                      const regEx = /[+-]?\d+(\.\d+)?/g;
                      const matchedStrList = String(ieltsStr).match(regEx);
                      console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                      if (matchedStrList && matchedStrList.length > 0) {
                        ieltsNumber = Number(matchedStrList[0]);
                        console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                      }
                    }
                  }
                  if (ieltsNumber == null) {
                    console.log("ieltsNumber --->> null");
                    let ieltsStr = "6.0 (or better) (no component lower than 5.5)";
                    const regEx = /[+-]?\d+(\.\d+)?/g;
                    const matchedStrList = String(ieltsStr).match(regEx);
                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                    if (matchedStrList && matchedStrList.length > 0) {
                      ieltsNumber = Number(matchedStrList[0]);
                      console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                    }
                  }

                }
                myscrore = await utils.getMappingScore(ieltsNumber);
              }

              // let score1 = courseScrappedData.course_toefl_ielts_score;

              ieltsfinaloutput = myscrore.ielts;
              console.log("Tofel@@@@", myscrore)
              var ibtDict = {}, caeDict = {}, pteDict = {}, ieltsDict = {}, pbtDict = {}, oetDict = {}, jcuceapDict = {};
              ieltsDict.name = 'ielts academic';
              ieltsDict.description = myscrore.ielts;
              englishList.push(ieltsDict);
              ibtDict.name = 'toefl ibt';
              ibtDict.description = myscrore.ibt;
              englishList.push(ibtDict);
              pbtDict.name = 'toefl pbt';
              pbtDict.description = myscrore.pbt;
              englishList.push(pbtDict);
              pteDict.name = 'pte academic';
              pteDict.description = myscrore.pte;
              englishList.push(pteDict);
              caeDict.name = 'cae';
              caeDict.description = myscrore.cae;
              englishList.push(caeDict);


              // if (englishList && englishList.length > 0) {
              //   courseAdminReq.english = englishList;
              // }

              // academic requirement
              if (courseScrappedData.course_academic_requirement) {
                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                console.log('\n\r');
                console.log(funcName + 'academicReq = ' + academicReq);
                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                console.log('\n\r');
                if (academicReq && String(academicReq).length > 0) {
                  courseAdminReq.academic = [academicReq];
                } else {
                  courseAdminReq.academic = [];
                }
              }
              ///if courseAdminReq has any valid value then only assign it
              if (courseAdminReq.english || courseAdminReq.academic) {
                let temp = courseAdminReq;
                resJsonData.course_admission_requirement = temp;

                console.log("resJsonData.course_admission_requirement --> ", resJsonData.course_admission_requirement);
              }

              // add english requirement 'english_more_details' link
              let resEnglishReqMoreDetailsJson = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
              let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
              // let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
              if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                resJsonData.course_admission_requirement.entry_requirements_url = "";
              } else if (resEnglishReqMoreDetailsJson) {
                resJsonData.course_admission_requirement = {};
                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                resJsonData.course_admission_requirement.entry_requirements_url = "";
                // resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
              }
              if (ieltsNumber && ieltsNumber > 0) {

                //tofel ibt
                let tempIBT = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [myscrore.ibt]
                            ]
                        }
                      ]
                  }
                ]
                const tofelScoreibt = await Course.extractValueEnglishlanguareFromScrappedElement(tempIBT);
                if (tofelScoreibt && tofelScoreibt.length > 0) {
                  // tofelfinaloutput = tofelScore.toString();
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(tofelScoreibt).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    ibtNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'ibtNumber = ' + ibtNumber);
                  }
                }
                //tofel pbt
                let tempPBT = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [myscrore.pbt]
                            ]
                        }
                      ]
                  }
                ]
                const tofelScorepbt = await Course.extractValueEnglishlanguareFromScrappedElement(tempPBT);
                if (tofelScorepbt && tofelScorepbt.length > 0) {
                  // tofelfinaloutput = tofelScore.toString();
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(tofelScorepbt).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    pbtNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'ibtNumber = ' + pbtNumber);
                  }
                }
                //pte score
                console.log("myscrore.PTE -->", myscrore.pte);
                let tempPTE = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [myscrore.pte]
                            ]
                        }
                      ]
                  }
                ]
                const pteAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempPTE);
                if (pteAScore && pteAScore.length > 0) {
                  // ptefinaloutput = pteAScore.toString();
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(pteAScore).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    pteNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'pteNumber = ' + pteNumber);
                  }
                }


                //cae score
                console.log("myscrore.PTE -->", myscrore.cae);
                let tempCAE = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [myscrore.cae]
                            ]
                        }
                      ]
                  }
                ]
                const caeScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempCAE);
                if (caeScore && caeScore.length > 0) {
                  // ptefinaloutput = pteAScore.toString();
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(caeScore).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    caeNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'caeNumber = ' + caeNumber);
                  }
                }
              }

              ///progress bar start
              const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
              const penglishList = [];
              if (ieltsNumber && ieltsNumber > 0) {
                var IELTSJSON = {
                  "name": 'ielts academic',
                  "description": ieltsDict.description,
                  "min": 0,
                  "require": ieltsNumber,
                  "max": 9,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (ieltsNumber) {
                  // pieltsDict.name = 'ielts academic';
                  // pieltsDict.value = IELTSJSON;
                  penglishList.push(IELTSJSON);
                }
              }

              if (ibtNumber && ibtNumber > 0) {
                var IBTJSON = {
                  "name": 'toefl ibt',
                  "description": ibtDict.description,
                  "min": 0,
                  "require": ibtNumber,
                  "max": 120,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (ibtNumber) {
                  // pibtDict.name = 'toefl ibt';
                  // pibtDict.value = IBTJSON;
                  penglishList.push(IBTJSON);
                }
              }
              if (pbtNumber && pbtNumber > 0) {
                var PBTJSON = {
                  "name": 'toefl pbt',
                  "description": pbtDict.description,
                  "min": 310,
                  "require": pbtNumber,
                  "max": 677,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (pbtNumber) {
                  // ppbtDict.name = 'toefl pbt';
                  // ppbtDict.value = PBTJSON;
                  penglishList.push(PBTJSON);
                }
              }



              if (pteNumber && pteNumber > 0) {
                var PTEJSON = {
                  "name": 'pte academic',
                  "description": pteDict.description,
                  "min": 0,
                  "require": pteNumber,
                  "max": 90,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (pteNumber) {
                  // ppteDict.name = 'pte academic';
                  // ppteDict.value = PTEJSON;
                  penglishList.push(PTEJSON);
                }
              }
              if (caeNumber && caeNumber > 0) {
                var CAEJSON = {
                  "name": 'cae',
                  "description": caeDict.description,
                  "min": 80,
                  "require": caeNumber,
                  "max": 230,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (caeNumber) {
                  // pcaeDict.name = 'cae';
                  //pcaeDict.value = CAEJSON;
                  penglishList.push(CAEJSON);
                }
              }

              if (penglishList && penglishList.length > 0) {
                courseAdminReq.english = penglishList;
              }
              else {
                throw new Error("english not found");
              }
              ///progrssbar End
              break;
            }
            case 'course_url': {
              let newUrl = courseScrappedData.course_url;
              let rescourse_url = null;
              if (Array.isArray(newUrl)) {
                for (const rootEleDict of newUrl) {
                  console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));

                      rescourse_url = selList;

                    }
                  }
                }
              }
              if (rescourse_url) {
                resJsonData.course_url = rescourse_url;
              }
              break;

            }
            case 'course_outline': {
              
              let mgcrs = null;
            
           const majorcrs = {
             majors:[],
             minors:[],
             more_details:""
           };
           //for major cousrseList
             var coursemajor =  courseScrappedData.course_outline;
             if (Array.isArray(coursemajor)) {
               for (const rootEleDict of coursemajor) {
                   console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                   const elementsList = rootEleDict.elements;
                   for (const eleDict of elementsList) {
                       console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                       const selectorsList = eleDict.selectors;
                       for (const selList of selectorsList) {
                           console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                           if (Array.isArray(selList) && selList.length > 0) {
                             mgcrs = selList;
                           }
                       }
                   }
               }
           }
             if(mgcrs && mgcrs.length >0){
               majorcrs.majors=mgcrs
              }
             else{
               majorcrs.majors= majorcrs.majors
             }
                resJsonData.course_outline =  majorcrs;
             //for minor courseList
                var courseminor =  courseScrappedData.course_outline_minor;
                let mincrs = null;
                if (Array.isArray(courseminor)) {
                  for (const rootEleDict of courseminor) {
                      console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                      const elementsList = rootEleDict.elements;
                      for (const eleDict of elementsList) {
                          console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                          const selectorsList = eleDict.selectors;
                          for (const selList of selectorsList) {
                              console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                              if (Array.isArray(selList) && selList.length > 0) {
                               mincrs = selList;
                              }
                          }
                      }
                  }
              }
                if(mincrs && mincrs.length >0){
                  majorcrs.minors=mincrs
                }
                else{
                 majorcrs.minors=majorcrs.minors
               }
               //for more_details 
               var more_outline = courseScrappedData.course_outline_more;
               let mor_outline = [];
               if (Array.isArray(more_outline)) {
                 for (const rootEleDict of more_outline) {
                     console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                     const elementsList = rootEleDict.elements;
                     for (const eleDict of elementsList) {
                         console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                         const selectorsList = eleDict.selectors;
                         for (const selList of selectorsList) {
                             console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                             if (Array.isArray(selList) && selList.length > 0) {
                              for( const sel of selList){
                              console.log("more_outttt1"+ sel.href);
                              mor_outline.push(sel.href);
                              }
                             }
                             else{
                              mor_outline.push(majorcrs.more_details);
                             }
                         }
                     }
                 }
             }
              //for course_outline_more_details 
              if(mor_outline.length > 0){
            majorcrs.more_details = mor_outline;
              }
              else{
                majorcrs.more_details = majorcrs.more_details;
              }
                   resJsonData.course_outline =  majorcrs;

                console.log("major==>", resJsonData.course_outline)
          
            break;
            }
            case 'course_duration_full_time': {
              //FULL TIME AND PART TIME IS DONE IN THIS
              //CUSTOMIZED CODE DONOT COPY
              const courseDurationList = [];
              const courseDurationDisplayList = [];
              const durationFullTime = {};
              let crsduration = "";
              // let anotherArray = [];
              let duration1 = courseScrappedData.course_duration_full_time;
              let duration11 =  courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0];
              if (Array.isArray(duration1)) {
                for (const rootEleDict of duration1) {
                  console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));

                      duration11 = selList;

                    }
                  }
                }
              }
               
              console.log("duration1-->", duration11);
             let ccode = resJsonData.course_campus_location[0].code;
             console.log("dcode-->", ccode);
              console.log("*************start formating Full Time years*************************");
              console.log("courseScrappedData.course_duration_full_time = ", JSON.stringify(courseScrappedData.course_duration_full_time));
              let finalDuration =  courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0];
              if (ccode == '096810E') {
                finalDuration = '0.5 years full-time or part-time equivalent';
                courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0] = finalDuration;
              }
             if (ccode  == '099088B') {
                finalDuration = '4 years full-time or part-time equivalent';
                courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0] = finalDuration;
              }  if (ccode  == '088073M' || ccode == '091551D' ||ccode == '077414G') {
                finalDuration = '3 years full-time or part-time equivalent';
                courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0] = finalDuration;
              }
              if(ccode == '083407A'){
                finalDuration = '2 years full-time or part-time equivalent';
                courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0] = finalDuration;
              }
              if(ccode == '070618G'){
                finalDuration = '1.5 years full-time or part-time equivalent';
                courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0] = finalDuration;
              }
              else{
               
                finalDuration = finalDuration.replace(/[\r\n\t() ]+/g, ' ');
                finalDuration = finalDuration.replace(/[\[\]']/g, '');
                finalDuration = finalDuration.replace(/[\(\)']/g, '');
            
                }
              
              if (finalDuration.toLowerCase().indexOf('part-time') > -1 && finalDuration.toLowerCase().indexOf('full-time') > -1 && finalDuration.toLowerCase().indexOf('full time') > -1 && finalDuration.toLowerCase().indexOf('part time') > -1) {
                finalDuration = finalDuration + " full-time";
              }
              console.log("Final Duration -->", finalDuration);
              if (finalDuration.indexOf('-') > -1) {
                finalDuration = finalDuration.replace(/-/g, ' ');
              }
              console.log("Final Duration3 -->", finalDuration);
              //duration show
              crsduration=finalDuration;
              resJsonData.course_duration = crsduration;
              if (finalDuration.indexOf('(') > -1) {
                finalDuration = finalDuration.replace(/[\r\n\t() ]+/g, ' ')
              }
              
              const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration.trim());
              console.log("full_year_formated_data", full_year_formated_data);
              ///course duration
              const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
              console.log("not_full_year_formated_data", not_full_year_formated_data);
             
              try {
                durationFullTime.duration_full_time = not_full_year_formated_data.trim();
                courseDurationList.push(durationFullTime);
                ///END course duration
                console.log("courseDurationList : ", courseDurationList);
                ///course duration display
              
                courseDurationDisplayList.push(full_year_formated_data);
                // anotherArray.push(courseDurationDisplayList);
                console.log("courseDurationDisplayList", courseDurationDisplayList);
                ///END course duration display
              } catch (err) {
                console.log("Problem in Full Time years:", err);
              }
              console.log('***************END formating Full Time years**************************')
              let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
              
              if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                resJsonData.course_duration_display = filtered_duration_formated;
                var isfulltime = false, isparttime = false;
                filtered_duration_formated.forEach(element => {
                  if (element.display == "Full-Time") {
                    isfulltime = true;
                  }
                  if (element.display == "Part-Time") {
                    isparttime = true;
                  }
                });
                resJsonData.isfulltime = isfulltime;
                resJsonData.isparttime = isparttime
              }
              break;
            }
          
            case 'course_tuition_fee':
            //case 'course_study_mode':
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_duration_part_time': {
              console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
              // No need to process anything, its already processed in other case.
              break;
            }
            case 'course_tuition_fees_international_student': { // {"year": "2020","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};
              // courseTuitionFee.year = configs.propValueNotAvaialble;

              const feesList = [];
              const feesDict = {
                
              };
              const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
              const courseKeyVal = courseScrappedData.course_tuition_fees_international_student;
              const courseKeyValAdditional = courseScrappedData.course_tuition_fees_international_student_additional;
              console.log("courseKeyValAdditional -->", JSON.stringify(courseKeyValAdditional));
              let feesIntStudent = [];
              let feesIntStudentAdditional = [];
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        feesIntStudent = selList;
                      }
                    }
                  }
                }
              }

              //External study mode fees are different
              if (Array.isArray(courseKeyValAdditional)) {
                for (const rootEleDict of courseKeyValAdditional) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        feesIntStudentAdditional = selList;
                      }
                    }
                  }
                }
              }
              if (feesIntStudentAdditional && feesIntStudentAdditional.length > 0) { // extract only digits
                const feesWithDollorTrimmed = String(feesIntStudentAdditional).trim();

                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                const arrval = String(feesVal1).split('.');

                const feesVal = String(arrval[0]);

                console.log(funcName + 'feesVal = ' + feesVal);
                if (feesVal) {
                  const regEx = /\d/g;
                  let feesValNum = feesVal.match(regEx);
                  if (feesValNum) {
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    feesValNum = feesValNum.join('');
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    let feesNumber = null;
                    if (feesValNum.includes(',')) {
                      feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                    } else {
                      feesNumber = feesValNum;
                    }
                    console.log(funcName + 'feesNumber = ' + feesNumber);
                    if (Number(feesNumber)) {
                      feesIntStudentAdditional = Number(feesNumber);
                    }
                  }
                }
              } // if (feesIntStudent && feesIntStudent.length > 0)


              console.log("feesIntStudentAdditional -->", feesIntStudentAdditional);
              let arr = [];
              // if we can extract value as Int successfully then replace Or keep as it is
              if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
               
                if (feesIntStudentAdditional == '' || feesIntStudentAdditional == undefined) {

                } else {
                  feesIntStudentAdditional = "External/Online - AUD " + feesIntStudentAdditional;
                  arr.push(feesIntStudentAdditional);
                  feesIntStudent = "On-campus - " + feesIntStudent;
                  arr.push(feesIntStudent);
                }

               
              } else {
               
                  feesDict.international_student={
                    amount: 0,
                    duration: 1,
                    unit: "Year",
                    description: ""
                   
                 
                } 
                
              }

                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                const arrval = String(feesVal1).split('.');

                const feesVal = String(arrval[0]);

                console.log(funcName + 'feesVal = ' + feesVal);
                if (feesVal) {
                  const regEx = /\d/g;
                  let feesValNum = feesVal.match(regEx);
                  if (feesValNum) {
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    feesValNum = feesValNum.join('');
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    let feesNumber = null;
                    if (feesValNum.includes(',')) {
                      feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                    } else {
                      feesNumber = feesValNum;
                    }
                    console.log(funcName + 'feesNumber = ' + feesNumber);
                    if (Number(feesNumber)) {
                      feesDict.international_student={
                        amount: Number(feesNumber),
                        duration: 1,
                        unit: "Year",
                        description: String(arr)
                       
                      }
                    } 
                  
                  }
                }
              
             
            
              if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }
                if (feesDict) {
                  // feesList.push(feesDict);
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                if (courseTuitionFee) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
                console.log("feesDict.international_student = ", feesDict.international_student);
                console.log("resJsonData.course_tuition_fee_amount = ", resJsonData.course_tuition_fee_amount);
              }

              break;
            }

            case 'course_campus_location': { // Location Launceston 

              const courseKeyVal = courseScrappedData.course_cricos_code;
              let course_cricos_code = null;
              let crcode1 = [];
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      var crcode = selList.toString().split('CRICOS:');
                      console.log("crcode:", crcode);
                      crcode1.push(crcode[1].trim());
                    
                    }
                  }
                }
              }
              if (Array.isArray(crcode1) && crcode1.length > 0) {
                course_cricos_code = crcode1;
              }
              else{
                throw new Error("cricos_code Not Found !!");
              }

              let concatnatedSelStr = [];
              console.log('course_campus_location matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedSelectorsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                // let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  for (const selList of selectorsList) {
                    if (selList && selList.length > 0) {
                      console.log('course_campus_location selList = ' + JSON.stringify(selList));

                      for (let selItem of selList) {
                        selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ',').split(",");
                        for (let sel of selItem) {
                          sel = sel.trim();
                          if (sel.indexOf('-') > -1) {
                            throw new Error("No campus location found.");
                          }
                          concatnatedSelStr.push(sel);
                        }

                      } // selList
                      console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);
                   
                    }
                  } // selectorsList 
                } // elementsList 
              } // rootElementDictList 

              if (concatnatedSelStr && concatnatedSelStr.length > 0) {
                var campusedata = [];
                concatnatedSelStr.forEach(element => {
                  element = this.titleCase(element);
                  campusedata.push({
                    "name": element,
                    "code":String(course_cricos_code)
                  })
                });
                resJsonData.course_campus_location = campusedata;
                console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

              }
              else {
                throw new Error("No campus location found.");
              }
              break;

            }

            case 'course_study_mode': { // Location Launceston

              resJsonData.course_study_mode = "On campus";

              break;
            }
            case 'application_fee': {
              const courseKeyVal = courseScrappedData.application_fee;
     
              let applicationfee = null;
                if (Array.isArray(courseKeyVal)) {
              for (const rootEleDict of courseKeyVal) {
             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
             const elementsList = rootEleDict.elements;
             for (const eleDict of elementsList) {
             console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                const selectorsList = eleDict.selectors;
             for (const selList of selectorsList) {
                 console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
              if (Array.isArray(selList) && selList.length > 0) {
                applicationfee = selList[0];
                 }
                 else{
                    applicationfee = selList[0];
                 }
              }
             }
             }
         }
        if (applicationfee && applicationfee.length>0) {
          resJsonData.application_fee = applicationfee;
         console.log("appfee"+ JSON.stringify(resJsonData.application_fee));
         }
         else{
            resJsonData.application_fee = applicationfee;
         }
     
          break;
    
        }
            case 'course_intake':
              {
                let courseIntakeStr = [];
                const courseKeyVal = courseScrappedData.course_intake;
                console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                console.log("resJsonData.course_campus_location -->", resJsonData.course_campus_location);
                if (Array.isArray(courseKeyVal)) {
                  for (const rootEleDict of courseKeyVal) {
                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                      console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                      const selectorsList = eleDict.selectors;
                      for (const selList of selectorsList) {
                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        // if (Array.isArray(selList) && selList.length > 0) {

                        for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                          let anotherArray = [];
                          
                          for (let sel of selList) {
                            console.log("sel -->", sel);
                            if (sel.indexOf('(') > -1) {
                              let splitSel = sel.split('(')
                              sel = splitSel[1].replace(/\(|\)/g, "").trim();
                              console.log("selSplit -->", sel);
                              anotherArray.push(sel);
                            } else {
                              //if sellist contains - 
                            }

                          }
                          console.log("selList[i] -->", anotherArray);
                          courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": anotherArray });
                          console.log("courseIntakeStr -->", courseIntakeStr);
                        }
                      }
                    }
                  }
                }

                console.log("resJsonData.course_intake_url --> ", resJsonData.course_intake_url);
                console.log("intakes" + JSON.stringify(courseIntakeStr));
                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                let formatIntake = await format_functions.providemyintake(courseIntakeStr, "MOM", "");
                // if (formatIntake && formatIntake.length > 0) {
                var intakedata = {};

                if (formatIntake && formatIntake.length > 0) {
                  var intakedata = {};
                  intakedata.intake = formatIntake;
                 
                  
                  intakedata.more_details = more_details;
                  resJsonData.course_intake = intakedata;
                }
                break;
              }
            
            case 'course_scholarship': {
              const courseKeyVal = courseScrappedData.course_scholarship;
              let resScholarshipJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resScholarshipJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resScholarshipJson) {
                resJsonData.course_scholarship = resScholarshipJson;
              }
              break;
            }
            case 'univ_name': {
              const courseKeyVal = courseScrappedData.univ_name;
              let resUnivName = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivName = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivName) {
                resJsonData.univ_name = resUnivName;
              }
              break;
            }
            case 'univ_logo': {
              const courseKeyVal = courseScrappedData.univ_logo;
              let resUnivLogo = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resUnivLogo = selList[0];
                      }
                    }
                  }
                }
              }
              if (resUnivLogo) {
                resJsonData.univ_logo = resUnivLogo;
              }
              break;
            }
            case 'course_accomodation_cost': {
              const courseKeyVal = courseScrappedData.course_accomodation_cost;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_accomodation_cost = resAccomodationCostJson;
              }
              break;
            }
            case 'course_country': {
              const courseKeyVal = courseScrappedData.course_country;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_country = resAccomodationCostJson;
              }
              break;
            }
            case 'course_overview': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
              // add only if it has valid value
              // let concatnatedRootElementsStrArray = [];
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                // console.log("resJsonData[key] -->11",resJsonData[key]);
              } else {
                resJsonData[key] = "";
              }
              break;
            }
            case 'course_career_outcome': {
              if (resJsonData.course_admission_requirement.academic.length == 0 && resJsonData.course_tuition_fee.fees[0].value.international_student.length == 0) {
                throw new Error('Course not to be taken');
              }

              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
              // add only if it has valid value
              let concatnatedRootElementsStrArray = [];
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                resJsonData[key] = concatnatedRootElementsStrArray;
                // console.log("resJsonData[key] -->11",resJsonData[key]);
              } else {
                resJsonData[key] = [];
              }
              break;
            }
           
            case 'course_title': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is for title' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr course title = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr.replace("\n", " ");
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).replace("\n", " ").concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr course title = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr course title = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr course title = ' + concatnatedRootElementsStr);
              // add only if it has valid value
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                concatnatedRootElementsStr = this.titleCase(concatnatedRootElementsStr);
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
              }
              break;
            }
            case 'course_study_level': {
              console.log(funcName + 'course_title course_study_level = ' + study_level);
              resJsonData.course_study_level = study_level.trim();
              break;
            }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      var NEWJSONSTRUCT = {}, structDict = [];
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
        console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        }
        else {
          NEWJSONSTRUCT.course_intake = "";
        }
        for (let myfees of resJsonData.course_tuition_fee.fees) {
          if (myfees.name == location) {
            NEWJSONSTRUCT.international_student_all_fees = myfees;
          }
        }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        resJsonData.course_id = location_wise_data.course_location_id;
      
        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


      }
      // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
      return resJsonData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }


  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = 'scrapeAndFormatCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);

      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }

      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file

      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }

      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.study_level);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
