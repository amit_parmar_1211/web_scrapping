**Western Sydney University: Total Courses Details As On 01 April 2019**
* Total Courses = 360
* Courses without CRICOS code = 0
* Total available courses = 
* Repeted = 0
* Uploaded = 0
* Failed = 85
**international_student_all_fees**
* No extra information related to fees is given so by default pushed NA
**Default Fees**
* Master of Research --> Fees are given based on area of study
* so static value is pushed in international_student_all_fees --> [Humanities, Arts and Social Science: AUD$24,760, Science, Technology, Engineering and Mathematics: AUD $31, 616]
**Static value given for fees**
* Master of Teaching (Secondary) --> fees static as selector was different for this course
**Intakes**
* Not pushed 2019 manually
* July (2019) is scrapped from course details
**course_toefl_ielts_score**
* Mapping is done based on course name
* name is searched in the file if mapping is available the it will take that and if not than default case is made
* changes made in getMappingScore which is there in utils.js file
* for courses having nursing name and master of teaching name have different ielts condition is specified
**Problem**
* Master of Research
* https://www.westernsydney.edu.au/future/study/courses/research/master-of-research.html
* Cricos code is not given in Handbook and 2 Cricos are given


* Cricos not given in handbook : 22
* 46
* 51
* 53

* https://www.westernsydney.edu.au/future/study/courses/research/master-of-information-and-communications-technology-research.html
* Cricos code not given in handbook
* Failed because not getting proper selector for fees --> https://www.westernsydney.edu.au/future/study/courses/postgraduate/master-of-teaching-secondary.html


**Failed Type errors**
* TypeError: Cannot read property 'fees' of undefined --> only available to domestic students