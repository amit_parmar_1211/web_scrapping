const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
// const mapping = require('./output/Mapping/main');
request = require('request');
class ScrapeCourse extends Course {
  static titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      // You do not need to check if i is larger than splitStr length, as your for does that for you
      // Assign it back to the array
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
  }
  // format courseScrappedData, format output json
  static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
    const funcName = 'formatOutput ';
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      for (const key in courseScrappedData) {
        console.log('\n\r' + funcName + 'key = ' + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case 'univ_id': {
              console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + 'configs.univ_id must have valid value');
                throw (new Error('configs.univ_id must have valid value'));
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case 'program_code': {
              const courseKeyVal = courseScrappedData.program_code;
              console.log("courseScrappedData.program_code = ", courseScrappedData.program_code);
              var program_code = "";
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        program_code = selList[0].split(": ");
                        program_code = program_code[1];
                      }
                    }
                  }
                }
              } // if (Array.isArray(courseKeyVal))
              let program_code_array = "";
              if (program_code == '' || program_code == undefined) {
                resJsonData.program_code = program_code_array;
              } else {
                let pcode = program_code.split('.');
                console.log("pcode -->", pcode);
                program_code = pcode[0];
                program_code_array = program_code;
                resJsonData.program_code = program_code_array;
              }

              break;
            }
            case 'course_discipline': {
              // As of not we do not have way to get this field value
              resJsonData.course_discipline = course_category;
              console.log("resJsonData.course_discipline -->", resJsonData.course_discipline);
              break;
            }
            case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              const courseAdminReq = {};
              const englishList = [];
              var myscrore = '';

              // let score1 = courseScrappedData.course_toefl_ielts_score;
              myscrore = await utils.getMappingScore(course_name);

              var ibtDict = {}, catDict = {}, pteDict = {}, ieltsDict = {}, islprDict = {}, oetDict = {}, jcuceapDict = {};
              ibtDict.name = 'toefl ibt';
              ibtDict.description = myscrore.TOFEL;
              englishList.push(ibtDict);
              pteDict.name = 'pte academic';
              pteDict.description = myscrore.PTE;
              englishList.push(pteDict);
              ieltsDict.name = 'ielts academic';
              ieltsDict.description = myscrore.IELTS;
              englishList.push(ieltsDict);

              // if (englishList && englishList.length > 0) {
              //   courseAdminReq.english = englishList;
              // } else {
              //   courseAdminReq.english = [];
              // }

              // academic requirement
              if (courseScrappedData.course_academic_requirement) {
                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                console.log('\n\r');
                console.log(funcName + 'academicReq = ' + academicReq);
                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                console.log('\n\r');
                if (academicReq && String(academicReq).length > 0) {
                  courseAdminReq.academic = [academicReq];
                } else {
                  courseAdminReq.academic = ["To check the academic requirement in the degree program, please see this link:https://www.westernsydney.edu.au/international/home/apply/admissions/entry_requirements"];
                }
              }
              // if courseAdminReq has any valid value then only assign it
              if (courseAdminReq.english || courseAdminReq.academic) {
                resJsonData.course_admission_requirement = courseAdminReq;
              }

              // add english requirement 'english_more_details' link
              let resEnglishReqMoreDetailsJson = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
              let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
              // let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
              if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                resJsonData.course_admission_requirement.entry_requirements_url = "";
              } else if (resEnglishReqMoreDetailsJson) {
                resJsonData.course_admission_requirement = {};
                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                resJsonData.course_admission_requirement.entry_requirements_url = "";
              }

              let pbtNumber = null; let pteNumber = null; let caeNumber = null; let ibtNumber = null;
              let ieltsNumber = null;
              const regEx = /[+-]?\d+(\.\d+)?/g;
              const matchedStrList = String(myscrore.IELTS).match(regEx);
              console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
              if (matchedStrList && matchedStrList.length > 0) {
                ieltsNumber = Number(matchedStrList[0]);
                console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
              }
              if (ieltsNumber && ieltsNumber > 0) {

                //tofel ibt
                let tempIBT = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [myscrore.TOFEL]
                            ]
                        }
                      ]
                  }
                ]
                const tofelScoreibt = await Course.extractValueEnglishlanguareFromScrappedElement(tempIBT);
                if (tofelScoreibt && tofelScoreibt.length > 0) {
                  // tofelfinaloutput = tofelScore.toString();
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(tofelScoreibt).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    ibtNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'ibtNumber = ' + ibtNumber);
                  }
                }
                //pte score
                console.log("myscrore.PTE -->", myscrore.PTE);
                let tempPTE = [
                  {
                    "elements":
                      [
                        {
                          "selectors":
                            [
                              [myscrore.PTE]
                            ]
                        }
                      ]
                  }
                ]
                const pteAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempPTE);
                if (pteAScore && pteAScore.length > 0) {
                  // ptefinaloutput = pteAScore.toString();
                  // extract exact number from string
                  const regEx = /[+-]?\d+(\.\d+)?/g;
                  const matchedStrList = String(pteAScore).match(regEx);
                  console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                  if (matchedStrList && matchedStrList.length > 0) {
                    pteNumber = Number(matchedStrList[0]);
                    console.log(funcName + 'pteNumber = ' + pteNumber);
                  }
                }

              }

              ///progress bar start
              const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
              const penglishList = [];
              if (ieltsNumber && ieltsNumber > 0) {
                var IELTSJSON = {
                  "name": "ielts academic",
                  "description": ieltsDict.description,
                  "min": 0,
                  "require": ieltsNumber,
                  "max": 9,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (ieltsNumber) {
                  // pieltsDict.name = 'ielts academic';
                  // pieltsDict.value = IELTSJSON;
                  penglishList.push(IELTSJSON);
                }
              }

              if (ibtNumber && ibtNumber > 0) {
                var IBTJSON = {
                  "name": "toefl ibt",
                  "description": ibtDict.description,
                  "min": 0,
                  "require": ibtNumber,
                  "max": 120,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (ibtNumber) {
                  // pibtDict.name = 'toefl ibt';
                  // pibtDict.value = IBTJSON;
                  penglishList.push(IBTJSON);
                }
              }


              if (pteNumber && pteNumber > 0) {
                var PTEJSON = {
                  "name": "pte academic",
                  "description": pteDict.description,
                  "min": 0,
                  "require": pteNumber,
                  "max": 90,
                  "R": 0,
                  "W": 0,
                  "S": 0,
                  "L": 0,
                  "O": 0
                };
                if (pteNumber) {
                  // ppteDict.name = 'pte academic';
                  // ppteDict.value = PTEJSON;
                  penglishList.push(PTEJSON);
                }
              }

              if (penglishList && penglishList.length > 0) {
                courseAdminReq.english = penglishList;
              } else {
                throw new Error('IELTS not found');
                courseAdminReq.english = [];
              }
              ///progrssbar End
              break;
            }
            case 'course_url': {
              if (courseUrl && courseUrl.length > 0) {
                resJsonData.course_url = courseUrl;
              }
              break;
            }
            case 'course_duration_full_time': {
              const courseDurationList = [];
              const courseDurationDisplayList = [];
              const durationFullTime = {};

              let anotherArray = [];
              let durationFullTimeDisplay = null;
              let duration1 = courseScrappedData.course_duration_full_time;

              console.log("*************start formating Full Time years*************************");
              console.log("courseScrappedData.course_duration_full_time = ", JSON.stringify(courseScrappedData.course_duration_full_time));
              let finalDuration = duration1[0].elements[0].selectors[0][0];
              console.log("Duration1 = ", finalDuration.trim());
              const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration.trim());
              console.log("full_year_formated_data", full_year_formated_data);
              ///course duration
              const not_full_year_formated_data = await format_functions.not_formated_course_duration(finalDuration.trim());
              console.log("not_full_year_formated_data", not_full_year_formated_data);
              try {
                durationFullTime.duration_full_time = not_full_year_formated_data;
                courseDurationList.push(durationFullTime);
                ///END course duration
                console.log("courseDurationList : ", courseDurationList);
                ///course duration display
                durationFullTimeDisplay = {};
                durationFullTimeDisplay.duration = full_year_formated_data[0].duration_full_time;
                durationFullTimeDisplay.unit = full_year_formated_data[0].unit;
                durationFullTimeDisplay.display = "Full-Time";
                console.log("durationFullTimeDisplay", durationFullTimeDisplay);
                courseDurationDisplayList.push(durationFullTimeDisplay);
                // anotherArray.push(courseDurationDisplayList);
                console.log("courseDurationDisplayList", courseDurationDisplayList);
                ///END course duration display
              } catch (err) {
                console.log("Problem in Full Time years:", err);
              }
              console.log("courseDurationDisplayList1", courseDurationDisplayList);

              console.log('***************END formating Full Time years**************************')

              console.log("*************start formating Part Time years*************************");
              let durationPartTimeDisplay = null;
              const durationPartTime = {};
              let duration2 = courseScrappedData.course_duration_part_time;
              console.log("courseScrappedData.course_duration_part_time = ", JSON.stringify(courseScrappedData.course_duration_part_time));
              let partyear_formated_data = ''
              let finalDuration2 = '';
              if (duration2 == '' || duration2 == undefined) {

              } else {
                finalDuration2 = duration2[0].elements[0].selectors[0][0];
                console.log("finalDuration2 ---->> ", finalDuration2);
                if (finalDuration2 == '' || finalDuration2 == undefined) {
                  console.log("If");
                } else {
                  partyear_formated_data = await format_functions.validate_course_duration_full_time(finalDuration2.trim());

                  var parttimeduration = JSON.stringify(partyear_formated_data);

                  if (parttimeduration != '[]') {
                    ///parttime course duration
                    const not_parttime_year_formated_data = await format_functions.not_formated_course_duration(finalDuration2.trim());
                    durationPartTime.duration_part_time = not_parttime_year_formated_data;
                    courseDurationList[0].duration_full_time += " full-time" + "," + durationPartTime.duration_part_time + " part-time";
                    //courseDurationList.push(durationPartTime);
                    ///End Parttime course duration

                    ///partitme course duration display
                    durationPartTimeDisplay = {};
                    durationPartTimeDisplay.duration = partyear_formated_data[0].duration_full_time;
                    durationPartTimeDisplay.unit = partyear_formated_data[0].unit;
                    durationPartTimeDisplay.display = "Part-Time"
                    ///END parttime course duration display

                    if (durationPartTimeDisplay) {
                      courseDurationDisplayList.push(durationPartTimeDisplay);

                    }
                  }
                  console.log('***************END formating Part Time years**************************')

                }


              }
              let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList);
              console.log("courseDurationDisplayList2", courseDurationDisplayList);
              if (courseDurationList && courseDurationList.length > 0) {
                resJsonData.course_duration = not_full_year_formated_data;
              }
              if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                resJsonData.course_duration_display = filtered_duration_formated;
                var isfulltime = false, isparttime = false;
                filtered_duration_formated.forEach(element => {
                  if (element.display == "Full-Time") {
                    isfulltime = true;
                  }
                  if (element.display == "Part-Time") {
                    isparttime = true;
                  }
                });
                resJsonData.isfulltime = isfulltime;
                resJsonData.isparttime = isparttime;
              }
              break;
            }
            case 'course_tuition_fee':
            //case 'course_study_mode':
            case 'page_url':
            case 'course_academic_requirement':
            case 'select_english_as_ielts':
            case 'select_english_as_ibt':
            case 'course_toefl_ibt_indicator':
            case 'select_english_as_pbt':
            case 'course_toefl_toefl_pbt_score':
            case 'course_duration':
            case 'course_tuition_fee_duration_years':
            case 'course_tuition_fees_currency':
            case 'course_tuition_fees_year':
            case 'course_admission_requirement':
            case 'course_duration_part_time': {
              console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
              // No need to process anything, its already processed in other case.
              break;
            }
            case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};
              // courseTuitionFee.year = configs.propValueNotAvaialble;

              const feesList = [];
              var feesDict = {};
              const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
              const courseKeyVal = courseScrappedData.course_tuition_fees_international_student;
              let feesIntStudent = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selListFees = ' + JSON.stringify(selList));
                      if (resJsonData.course_title.toLowerCase() == 'master of teaching (secondary)') {
                        let arr = [];
                        arr.push("AUD $25,720");
                        feesIntStudent = arr;
                      }
                      if (Array.isArray(selList) && selList.length > 0) {
                        feesIntStudent = selList;
                      }
                    }
                  }
                }
              }

              console.log("feesIntStudent -->", feesIntStudent);
              // if we can extract value as Int successfully then replace Or keep as it is
              if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                const arrval = String(feesVal1).split('.');

                const feesVal = String(arrval[0]);

                console.log(funcName + 'feesVal = ' + feesVal);
                if (feesVal) {
                  const regEx = /\d/g;
                  let feesValNum = feesVal.match(regEx);
                  if (feesValNum) {
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    feesValNum = feesValNum.join('');
                    console.log(funcName + 'feesValNum = ' + feesValNum);
                    let feesNumber = null;
                    if (feesValNum.includes(',')) {
                      feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                    } else {
                      feesNumber = feesValNum;
                    }
                    console.log(funcName + 'feesNumber = ' + feesNumber);
                    if (Number(feesNumber)) {
                      feesDict.international_student={
                        amount: Number(feesNumber),
                        duration: 1,
                        unit: "Year",                        
                       description: "",
                        
                      };
                    } else {
                      feesDict.international_student={
                        amount: 0,
                        duration: 1,
                        unit: "Year",                        
                        description: ""
                        
                      }
                    }
                  } else {
                    console.log("Fees Not present1");
                    feesDict.international_student={
                      amount: 0,
                      duration: 1,
                      unit: "Year",                     
                      description: ""
                      
                    };
                  }
                } else {
                  console.log("Fees Not present1");
                  feesDict.international_student={
                    amount: 0,
                    duration: 1,
                    unit: "Year",                    
                    description: ""
                   
                  }
                }
              } else {
                feesDict.international_student={
                  amount: 0,
                  duration: 1,
                  unit: "Year",                  
                  description: ""
                 
                }

              }


              // if (feesDict.international_student) { // extract only digits
              //   console.log("international_student" + feesIntStudent)
              //   let arr = [];

              //   if (resJsonData.course_title.toLowerCase() == 'master of research') {
              //     arr.push("Humanities, Arts and Social Science: AUD$24,760");
              //     arr.push("Science, Technology, Engineering and Mathematics: AUD $31, 616");
              //     feesDict.international_student_all_fees = arr;
              //   } else {
              //     feesDict.international_student_all_fees = arr;
              //   }
              // }
              if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                }
                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }
                if (feesDict) {
                  var campus = resJsonData.course_campus_location;
                  for (let loc of campus) {
                    feesList.push({ name: loc.name, value: feesDict });
                  }
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                if (courseTuitionFee) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
                console.log("feesDict.international_student = ", feesDict.international_student);
                console.log("resJsonData.course_tuition_fee_amount = ", resJsonData.course_tuition_fee_amount);
              }
              break;
            }
            case 'course_campus_location': { // Location Launceston
              const courseKeyVals = courseScrappedData.course_cricos_code;
              let course_cricos_code = null;
              let crcode1 = [];
              if (Array.isArray(courseKeyVals)) {
                for (const rootEleDict of courseKeyVals) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (let selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      // selList = 
                      if (Array.isArray(selList) && selList.length > 0) {
                        if (selList[0].indexOf('|') > -1) {
                          let splitCricos = selList[0].split('|');
                          selList = splitCricos;
                        }
                        console.log("selList Cricos -->", selList);
                        course_cricos_code = selList;
                      }
                    }
                  }
                }
              }
              
              
              var campLocationText = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
              const courseKeyVal = courseScrappedData.course_campus_location;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        campLocationText = selList;
                      }
                    }
                  }
                }
              }
              if (campLocationText && campLocationText.length > 0) {
                var campusedata = [];
                var flocation=[]
                var locations=["Bankstown","Campbelltown","Hawkesbury","Liverpool","Nirimba","Parramatta City","Parramatta South","Penrith","Sydney"]
                for(let i=0;i<locations.length;i++){
                  campLocationText.forEach(element => {
                    if(element.includes(locations[i])){
                      flocation.push(locations[i])
                    }
                    
                  })
                }
                flocation.forEach(element=>{
                  campusedata.push({
                    "name": element,
                    "code":String(course_cricos_code)
                  })
                })                
               
                resJsonData.course_campus_location = campusedata;


                //resJsonData.course_campus_location=campLocationText
                console.log("resJsonData.course_campus_location --> ", resJsonData.course_campus_location);
              } else {
                throw new Error("location not found");
              }
              console.log("Course_location", campLocationText)
              break
            }

            case 'course_study_mode': { // Location Launceston
              resJsonData.course_study_mode = "On campus";
              break;
            }
            case 'course_intake':
              {
                let courseIntakeStr = [];

                const courseKeyVal = courseScrappedData.course_intake;
                console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                if (Array.isArray(courseKeyVal)) {
                  for (const rootEleDict of courseKeyVal) {
                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                    const elementsList = rootEleDict.elements;
                    for (const eleDict of elementsList) {
                      console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                      const selectorsList = eleDict.selectors;
                      for (const selList of selectorsList) {
                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        // if (Array.isArray(selList) && selList.length > 0) {
                        for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                          let anotherArray = [];
                          for (let i = 0; i < selList.length; i++) {
                            anotherArray.push(selList[i]);

                          }
                          console.log("selList[i] -->", anotherArray);
                          if (selList.length == 0) {
                            throw new Error('intake not found');
                          } else {
                            courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": anotherArray });
                          }

                          console.log("courseIntakeStr -->", courseIntakeStr);
                        }
                      }
                    }
                  }
                }
                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                let formatedIntake = await format_functions.providemyintake(courseIntakeStr, "MOM", "");


                console.log("resJsonData.course_intake_url --> ", resJsonData.course_intake_url);
                console.log("intakes" + JSON.stringify(courseIntakeStr));
                if (formatedIntake && formatedIntake.length > 0) {
                  var intakedata = {};
                  intakedata.intake = formatedIntake;
                  console.log("FormatIntake", JSON.stringify(formatedIntake))
                  // formatedIntake.forEach(element => {
                  //   element.value.forEach(elementdate => {
                  //     if (elementdate.actualdate.includes('July')) {
                  //       elementdate.filterdate = "2020-07-06"
                  //     } else if (elementdate.actualdate.includes('March')) {
                  //       elementdate.filterdate = "2020-03-02"
                  //     }
                  //     //elementdate.push({filterdate:myintake_data["term 1_date"]})
                  //     console.log("DATA--->", elementdate)

                  //   })
                  // });
                  intakedata.more_details = more_details;
                  resJsonData.course_intake = intakedata;
                  console.log("resJsonData.course_intake -->> ", resJsonData.course_intake);
                }
                break;
              }
              case 'course_outline': {
                const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                const courseKeyVal_major =courseScrappedData.course_outline;
                const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                let course_outlines = {};
                console.log("MAjor==>", JSON.stringify(courseKeyVal_major))
                if (courseKeyVal_minor != null) {
                    course_outlines.minors = courseKeyVal_minor
                } else {
                    course_outlines.minors = []
                }
                if (courseKeyVal_major) {
                    let course_career_outcome = null;
                    if (Array.isArray(courseKeyVal_major)) {
                        for (const rootEleDict of courseKeyVal_major) {
                            console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                            const elementsList = rootEleDict.elements;
                            for (const eleDict of elementsList) {
                                console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                const selectorsList = eleDict.selectors;
                                for (const selList of selectorsList) {
                                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                    if (Array.isArray(selList) && selList.length > 0) {
                                        course_career_outcome = selList;
                                    }
                                }
                            }
                        }
                    }
                    if(course_career_outcome!=null){
                        course_outlines.majors = course_career_outcome
                    }else{
                        course_outlines.majors = []
                        course_outlines.more_details=resJsonData.course_url
                    }
                  
                }
                
                if (courseKeyVal != null) {
                    course_outlines.more_details = courseKeyVal
                } else{
                  course_outlines.more_details=resJsonData.course_url
                }
                resJsonData.course_outline = course_outlines
                break;
            }
            case 'application_fees': {
                const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                console.log("Application==>",courseKeyVal)
                if (courseKeyVal == null || courseKeyVal == "null" ) {
                    resJsonData.application_fee = ""
                } else {
                    resJsonData.application_fee = courseKeyVal
                }
                break;
            }
           
            case 'course_country': {
              const courseKeyVal = courseScrappedData.course_country;
              let resAccomodationCostJson = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                      if (Array.isArray(selList) && selList.length > 0) {
                        resAccomodationCostJson = selList[0];
                      }
                    }
                  }
                }
              }
              if (resAccomodationCostJson) {
                resJsonData.course_country = resAccomodationCostJson;
              }
              break;
            }
            case 'course_overview': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
              // add only if it has valid value
              // let concatnatedRootElementsStrArray = [];
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                // console.log("resJsonData[key] -->11",resJsonData[key]);
              }
              break;
            }
            case 'course_career_outcome': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
              // add only if it has valid value
              let concatnatedRootElementsStrArray = [];
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                resJsonData[key] = concatnatedRootElementsStrArray;
                // console.log("resJsonData[key] -->11",resJsonData[key]);
              } else {
                resJsonData[key] = [];
              }
              break;
            }
            // case 'course_cricos_code': {
            //   const courseKeyVal = courseScrappedData.course_cricos_code;
            //   let course_cricos_code = null;
            //   let crcode1 = [];
            //   if (Array.isArray(courseKeyVal)) {
            //     for (const rootEleDict of courseKeyVal) {
            //       console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
            //       const elementsList = rootEleDict.elements;
            //       for (const eleDict of elementsList) {
            //         console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
            //         const selectorsList = eleDict.selectors;
            //         for (let selList of selectorsList) {
            //           console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
            //           // selList = 
            //           if (Array.isArray(selList) && selList.length > 0) {
            //             if (selList[0].indexOf('|') > -1) {
            //               let splitCricos = selList[0].split('|');
            //               selList = splitCricos;
            //             }
            //             console.log("selList Cricos -->", selList);
            //             course_cricos_code = selList;
            //           }
            //         }
            //       }
            //     }
            //   }
            //   const mycodes = [];
            //   if (course_cricos_code && course_cricos_code.length > 0) {
            //     const locations = resJsonData.course_campus_location;
            //     console.log("locations----->", locations);
            //     console.log("course_cricos_code----->", course_cricos_code);
            //     for (let location of locations) {
            //       mycodes.push({
            //         location: location.name, code: course_cricos_code.toString(), iscurrent: false
            //       });
            //     }
            //     resJsonData.course_cricos_code = mycodes;
            //   } else {
            //     throw new Error("Campus not found");
            //   }
            //   break;
            // }
            case 'course_title': {
              console.log(funcName + 'matched case: ' + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                    console.log(funcName + '\n\r selList = ' + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                      }
                    } // selList
                    console.log(funcName + 'concatnatedSelStr course title = ' + concatnatedSelStr);
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr.replace("\n", " ");
                      } else {
                        concatnatedSelectorsStr = String(concatnatedSelectorsStr).replace("\n", " ").concat(' ').concat(concatnatedSelStr).trim();
                      }
                    }
                  } // selectorsList
                  console.log(funcName + 'concatnatedSelectorsStr course title = ' + concatnatedSelectorsStr);
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                    }
                  }
                } // elementsList
                console.log(funcName + 'concatnatedElementsStr course title = ' + concatnatedElementsStr);
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                  }
                }
              } // rootElementDictList
              console.log(funcName + 'concatnatedRootElementsStr course title = ' + concatnatedRootElementsStr);
              // add only if it has valid value
              if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                concatnatedRootElementsStr = this.titleCase(concatnatedRootElementsStr);
                resJsonData[key] = String(concatnatedRootElementsStr).trim();
              }
              break;
            }
            case 'course_study_level': {
              const cStudyLevel = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);

              if (cStudyLevel) {
                resJsonData.course_study_level = cStudyLevel;
              }

              break;
            }
            default: {
              console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
              break;
            } // default case
          } //
          console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      var NEWJSONSTRUCT = {}, structDict = [];
      console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
        console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        }
        else {
          NEWJSONSTRUCT.course_intake = "";
        }
        for (let myfees of resJsonData.course_tuition_fee.fees) {
          if (myfees.name == location) {
            NEWJSONSTRUCT.international_student_all_fees = myfees;
          }
        }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        resJsonData.course_id = location_wise_data.course_location_id;
        let basecourseid = location_wise_data.course_id;
        var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
        console.log("Write file--->" + filelocation)
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));        
      }
      // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
      return resJsonData;
      ///end grnrating new file location wise
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }


  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = 'scrapeAndFormatCourseDetails ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);

      Scrape.validateParams([courseDict]);
      // get course href and innerText from courseDict and ensure it is valid
      if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
        console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
        throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
      }

      const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
      console.log(funcName + 'courseTitle = ' + courseTitle);
      console.log(funcName + 'courseUrl = ' + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
      // validate generated courseId
      console.log(funcName + 'courseId = ' + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
        throw (new Error('Invalid courseId, courseId = ' + courseId));
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
        console.log(funcName + 'reading scrapped data from local output file...');
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
          throw (new Error('Invalid fileData, fileData = ' + fileData));
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file

      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
      } else {
        console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
        courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
      }
      if (!courseScrappedData) {
        throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
      }

      const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category);
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
