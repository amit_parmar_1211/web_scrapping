const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {

  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    var scraped_data = [];
    var totalCourseList = [];
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });

      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      //scrape all main categories
      console.log("Sested ....")
      let targetLinksCardsTotal=[]
      //var targetLinksCardsTotal = JSON.parse(await fs.readFileSync("./output/main_category_courses.json"));
        const category="//*[@id='studyAreas']/div/a"
        const catc=await s.page.$x(category)
        for (let i = 0; i < catc.length; i++) {
          var elementstring = await s.page.evaluate(el => el.innerText, catc[i]);
          var elementhref = await s.page.evaluate(el => el.href, catc[i]);
          targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
        }
        console.log("MainCAtegory-->",targetLinksCardsTotal)
        fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));  
      // const dataSelector = "//*[@id='courses']/ul/li[@data-residenttype='International']//ul[1]//a";
      const dataSelector = "//*[@id='courses']//ul/li/a";
      for (let target of targetLinksCardsTotal) {
        console.log("target...." + target.href)
        await s.page.goto(target.href, { timeout: 0 });
        await s.page.waitForXPath(dataSelector, { timeout: 0 });
        const targetLinks = await s.page.$x(dataSelector);
        console.log("length--->" + targetLinks.length)

        let links = await s.page.$x("//*[@id='courses']/ul/li[@data-studylevel='Undergraduate']/button");
        await links[0].click();
        // console.log("@@@@",links[0].innerText)
        console.log("ClickedUG")
        var level = await s.page.evaluate(el => el.innerText, links[0])
        let ugselector = "//*[@id='courses']/ul/li[@data-studylevel='Undergraduate']/button/following::div[1]/ul/li/a";
        let ugtextselecor = "//*[@id='courses']/ul/li[@data-studylevel='Undergraduate']/button/following::div[1]/ul/li/a";
        await s.page.waitFor(5000);
        const ugtargetlinks = await s.page.$x(ugselector);
        const ugtargettext = await s.page.$x(ugtextselecor);
        for (var i = 0; i < ugtargetlinks.length; i++) {
          var ugelementstring = await s.page.evaluate(el => el.innerText,  ugtargettext[i])
          const ugelementlink = await s.page.evaluate(el => el.href,  ugtargetlinks [i])
          scraped_data.push({ href: ugelementlink, innerText: ugelementstring, category: target.innerText, study_level: level });
        }
       // console.log("@@@@ug",JSON.stringify(scraped_data))
        // let links1 = await s.page.$x("//*[@id='courses']/ul/li[2]/button");
        // await links1[0].click();
        // // console.log("@@@@",links[0].innerText)
        // console.log("ClickedDD") 
        // var ddlevel = await s.page.evaluate(el => el.innerText, links1[0])
        // let ddselector = "//*[@id='accordion-group-2']/ul/li/a";
        // let ddtextselecor = "//*[@id='accordion-group-2']/ul/li/a";
        // await s.page.waitFor(5000);
        // const ddtargetlinks = await s.page.$x(ddselector);
        // const ddtargettext = await s.page.$x(ddtextselecor);
        // for (var i = 0; i < ddtargetlinks.length; i++) {
        //   var ddelementstring = await s.page.evaluate(el => el.innerText, ddtargettext[i])
        //   const ddelementlink = await s.page.evaluate(el => el.href, ddtargetlinks[i])
        //   totalCourseList.push({ href: ddelementlink, innerText: ddelementstring, category: target.innerText, study_level: ddlevel });
        // }

        let links2 = await s.page.$x("//*[@id='courses']/ul/li[@data-studylevel='Postgraduate']/button");
        await links2[0].click();
        // console.log("@@@@",links[0].innerText)
        console.log("ClickedPG")
        var pglevel = await s.page.evaluate(el => el.innerText, links2[0])
        let pgselector = "//*[@id='courses']/ul/li[@data-studylevel='Postgraduate']/button/following::div[1]/ul/li/a";
        let pgtextselecor = "//*[@id='courses']/ul/li[@data-studylevel='Postgraduate']/button/following::div[1]/ul/li/a";
        await s.page.waitFor(5000);
        const pgtargetlinks = await s.page.$x(pgselector);
        const pgtargettext = await s.page.$x(pgtextselecor);
        for (var i = 0; i < pgtargetlinks.length; i++) {
          var pgelementstring = await s.page.evaluate(el => el.innerText, pgtargettext[i])
          const pgelementlink = await s.page.evaluate(el => el.href, pgtargetlinks[i])
          scraped_data.push({ href: pgelementlink, innerText: pgelementstring, category: target.innerText, study_level: pglevel });
        }

        // let links3 = await s.page.$x("//*[@id='courses']/ul/li[4]/button");
        // await links3[0].click();
        // // console.log("@@@@",links[0].innerText)
        // console.log("ClickedSW")
        // var swlevel = await s.page.evaluate(el => el.innerText, links3[0])
        // let swselector = "//*[@id='accordion-group-4']/ul/li/a";
        // let swtextselecor = "//*[@id='accordion-group-4']/ul/li/a";
        // await s.page.waitFor(5000);
        // const swtargetlinks = await s.page.$x(swselector);
        // const swtargettext = await s.page.$x(swtextselecor);
        // for (var i = 0; i < swtargetlinks.length; i++) {
        //   var swelementstring = await s.page.evaluate(el => el.innerText, swtargettext[i])
        //   const swelementlink = await s.page.evaluate(el => el.href, swtargetlinks[i])
        //   totalCourseList.push({ href: swelementlink, innerText: swelementstring, category: target.innerText, study_level: swlevel });
        // }
        // let links4 = await s.page.$x("//*[@id='courses']/ul/li[5]/button");
        // await links4[0].click();
        // // console.log("@@@@",links[0].innerText)
        // console.log("ClickedIN")
        // var islevel = await s.page.evaluate(el => el.innerText, links4[0])
        // let isselector = "//*[@id='accordion-group-5']/ul[1]/li/a";
        // let istextselecor = "//*[@id='accordion-group-5']/ul[1]/li/a";
        // await s.page.waitFor(5000);
        // const istargetlinks = await s.page.$x(isselector);
        // const istargettext = await s.page.$x(istextselecor);
        // for (var i = 0; i < istargetlinks.length; i++) {
        //   var iselementstring = await s.page.evaluate(el => el.innerText, istargettext[i])
        //   const iselementlink = await s.page.evaluate(el => el.href, istargetlinks[i])
        //   totalCourseList.push({ href: iselementlink, innerText: iselementstring, category: target.innerText, study_level: islevel });
        // }
        

    

        //   for (let datalink of targetLinks) {
        //     var elementurl = await s.page.evaluate(el => el.href, datalink);
        //     var elementstring = await s.page.evaluate(el => el.textContent, datalink);
        //     var isin = scraped_data.filter(item => item.href.trim() == elementurl.trim());
        //     if (isin.length == 0) {
        //       scraped_data.push({ innerText: elementstring.trim(), href: elementurl.trim(), category: target.innerText.trim(),study_level:educationLevelCatList[0]  });
        //     }
        //   }
      }
      console.log("scraped_data", scraped_data);


      var uniquecases = [], uniquedata = [], finalDict = [];
      scraped_data.forEach(element => {
        if (!uniquecases.includes(element.href)) {
          uniquecases.push(element.href);
          uniquedata.push(element);
        }
      });
      uniquecases.forEach(element => {
        var data = scraped_data.filter(e => e.href == element);
        var category = [];

        data.forEach(element => {
          category.push(element.category);
        });
        data[0].category = category;
        finalDict.push(data[0]);
      });

      console.log("unique data-->" + uniquecases.length);
      fs.writeFileSync("./output/courselist_uniq.json", JSON.stringify(uniquedata));
      fs.writeFileSync("./output/courselist_original.json", JSON.stringify(scraped_data));
      console.log(funcName + 'writing courseList to file....');
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(finalDict));

      if (s) {
        await s.close();
      }
      return scraped_data;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }



  // refers to http://www.utas.edu.au/courses
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }


      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');

      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
