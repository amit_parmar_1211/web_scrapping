{
    "univ_id": "edithcowanuniversity",
    "course_title": "Doctor Of Philosophy",
    "program_code": "L61",
    "course_discipline": [
        "ARTS & HUMANITIES",
        "BUSINESS & LAW",
        "ENGINEERING & TECHNOLOGY",
        "MEDICAL & HEALTH SCIENCES",
        "NURSING & MIDWIFERY",
        "SCIENCE",
        "TEACHER EDUCATION",
        "WESTERN AUSTRALIAN ACADEMY OF PERFORMING ARTS"
    ],
    "course_study_level": "Postgraduate",
    "course_url": "https://www.ecu.edu.au/degrees/courses/doctor-of-philosophy",
    "course_intake": {
        "intake": [
            {
                "name": "South West",
                "value": [
                    {
                        "actualdate": "24 Feb,2020",
                        "month": "February"
                    }
                ]
            },
            {
                "name": "Mount Lawley",
                "value": [
                    {
                        "actualdate": "24 Feb,2020",
                        "month": "February"
                    }
                ]
            },
            {
                "name": "Joondalup",
                "value": [
                    {
                        "actualdate": "24 Feb,2020",
                        "month": "February"
                    }
                ]
            }
        ],
        "more_details": "https://intranet.ecu.edu.au/__data/assets/pdf_file/0010/651970/2020-Academic-Calendar.pdf"
    },
    "course_campus_location": [
        {
            "name": "South West",
            "code": "085257E"
        },
        {
            "name": "Mount Lawley",
            "code": "085257E"
        },
        {
            "name": "Joondalup",
            "code": "085257E"
        }
    ],
    "course_study_mode": "On campus",
    "course_duration": "4 years full-time or part-time equivalent",
    "course_duration_display": [
        {
            "unit": "years",
            "duration": "4",
            "display": "Full-Time",
            "filterduration": 4
        },
        {
            "unit": "years",
            "duration": "8",
            "display": "Part-Time",
            "filterduration": 8
        }
    ],
    "isfulltime": true,
    "isparttime": true,
    "course_overview": "A Doctor of Philosophy (PhD) is an opportunity to pursue a topic you're passionate about at the highest academic level. A PhD is an independent, supervised research project that is assessed on the submission of a thesis or creative work and exegesis. This course may be undertaken in any School within ECU, provided there are supervisors available in your proposed research area, and the resources and facilities required to complete your proposed project are accessible. ECU encourages interdisciplinary research that may span two or more disciplines. As a PhD candidate, you will be expected to develop a project that can be completed in three years full-time. You may submit your thesis any time after two years of full-time study, and the maximum course duration for a PhD is four years full-time. A PhD is a significant undertaking, and if enrolled full-time you are expected to commit at least 35 hours per week to your research.",
    "course_career_outcome": [],
    "course_admission_requirement": {
        "english": [
            {
                "name": "ielts academic",
                "description": "IELTS Academic Overall band minimum score of 6.5 (no individual band less than 6.0); English competency requirements may be satisfied through completion of one of the following:",
                "min": 0,
                "require": 6.5,
                "max": 9,
                "R": 0,
                "W": 0,
                "S": 0,
                "L": 0,
                "O": 0
            },
            {
                "name": "toefl ibt",
                "description": "Minimum score of 84, with no individual score less than 17",
                "min": 0,
                "require": 84,
                "max": 120,
                "R": 0,
                "W": 0,
                "S": 0,
                "L": 0,
                "O": 0
            },
            {
                "name": "toefl pbt",
                "description": "Minimum score of 573, including Test of Written English of 5.0 or better",
                "min": 310,
                "require": 573,
                "max": 677,
                "R": 0,
                "W": 0,
                "S": 0,
                "L": 0,
                "O": 0
            },
            {
                "name": "pte academic",
                "description": "PTE Academic 58, with no score less than 50 for postgraduate standard entry courses",
                "min": 0,
                "require": 58,
                "max": 90,
                "R": 0,
                "W": 0,
                "S": 0,
                "L": 0,
                "O": 0
            }
        ],
        "academic": [
            "The following course-specific admission requirements are mandatory and must be satisfied by all applicants. These requirements are in addition to or supersede the minimum requirements outlined within the Academic admission requirements band section below.\n\nAll applicants are required to have the equivalent of 6 months full time research (which must involve a research output which includes the conception and design of the project, and analysis and interpretation of findings) if applying with a Masters qualification other than a Master by Research. Applicants seeking online study will be required to provide additional information to support their application, as not all projects nor disciplines are suited to online study. Special entry may be considered for applications to the Western Australian Academy of Performing Arts (WAAPA) who will be required to attend an audition and/or interview.\n\nNoteFor more information, including guidelines for specific disciplines, please see our Interviews, folios and auditions web page.\n\nAcademic admission requirements (Band 11) may be satisfied through completion of one of the following:\n\nMasters Degree (Research); or\nBachelor Honours Degree (First or Upper Second class); or\nDemonstrated capacity to undertake original PhD-level research."
        ],
        "entry_requirements_url": "https://www.ecu.edu.au/future-students/course-entry",
        "english_requirements_url": "https://www.ecu.edu.au/future-students/course-entry/english-competency",
        "academic_requirements_url": ""
    },
    "course_country": "Australia",
    "course_tuition_fee": {
        "fees": [
            {
                "name": "South West",
                "value": {
                    "international_student": {
                        "amount": 34250,
                        "duration": "1",
                        "unit": "year",
                        "description": "AUD $34,250"
                    },
                    "fee_duration_years": "1",
                    "currency": "AUD"
                }
            },
            {
                "name": "Mount Lawley",
                "value": {
                    "international_student": {
                        "amount": 34250,
                        "duration": "1",
                        "unit": "year",
                        "description": "AUD $34,250"
                    },
                    "fee_duration_years": "1",
                    "currency": "AUD"
                }
            },
            {
                "name": "Joondalup",
                "value": {
                    "international_student": {
                        "amount": 34250,
                        "duration": "1",
                        "unit": "year",
                        "description": "AUD $34,250"
                    },
                    "fee_duration_years": "1",
                    "currency": "AUD"
                }
            }
        ]
    },
    "course_outline": {
        "minors": [],
        "majors": [],
        "more_details": ""
    },
    "application_fees": "75",
    "course_id": "doctorofphilosophy"
}