const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');
var myintake = "";
class ScrapeCourse extends Course {

    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, category, studyLevel) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = category;
                            break;
                        }
                        case 'course_title': {
                            const title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)                                                  
                            var ctitle = format_functions.titleCase(title)
                            console.log("ctitle@@@", ctitle)
                            if(ctitle.includes(':')){
                                resJsonData.course_title = ctitle.replace(':','_')  
                            }else{
                                resJsonData.course_title = ctitle
                            }
                            
                            break;
                        }

                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {};
                            var IELTSSCORE = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                            var studylevel = resJsonData.course_study_level;
                            var score = await utils.getValueFromHardCodedJsonFile('ielts_mapping_default')
                            var level;
                            var ieltsScore, ibt, pte, pbt
                            var pieltsDict = {};
                            var ppteDict = {}, ppbtDict = {}, pibtDict = {}
                            console.log("### IELTS data-->" + JSON.stringify(score));
                            var penglishList = [];
                            ieltsDict.name = 'ielts academic';
                            if (IELTSSCORE) {
                                ieltsScore = await utils.giveMeNumber(IELTSSCORE);
                            }
                            if (ieltsScore && ieltsScore > 0) {
                                if (ieltsScore) {
                                    pieltsDict.name = 'ielts academic';
                                    pieltsDict.description = IELTSSCORE;
                                    pieltsDict.min = 0;
                                    pieltsDict.require = ieltsScore;
                                    pieltsDict.max = 9;
                                    pieltsDict.R = 0;
                                    pieltsDict.W = 0;
                                    pieltsDict.S = 0;
                                    pieltsDict.L = 0;
                                    pieltsDict.O = 0;
                                    penglishList.push(pieltsDict);
                                }
                            }
                            if (studylevel.trim() == 'Undergraduate') {
                                level = 'ug';
                                if (level == score[0].level) {
                                   
                                    ibt = score[0].ibt
                                    pte = score[0].pte
                                    pbt = score[0].toefl
                                   
                                }
                            } else  {
                                 level = 'pg';
                                console.log("inelse",score[1].level)
                                if (level == score[1].level) {
                                    ibt = score[1].ibt
                                    pte = score[1].pte
                                    pbt = score[1].toefl
                                   
                                }
                            }
                            
                            if (ibt) {
                                pibtDict.name = 'toefl ibt';
                                pibtDict.description = ibt
                                pibtDict.min = 0;
                                pibtDict.require = await utils.giveMeNumber(ibt);
                                pibtDict.max = 120;
                                pibtDict.R = 0;
                                pibtDict.W = 0;
                                pibtDict.S = 0;
                                pibtDict.L = 0;
                                pibtDict.O = 0;
                                penglishList.push(pibtDict);
                            }
                            if(pbt){
                                ppbtDict.name = 'toefl pbt';
                                ppbtDict.description = pbt
                                ppbtDict.min = 310;
                                ppbtDict.require = await utils.giveMeNumber(pbt);
                                ppbtDict.max = 677;
                                ppbtDict.R = 0;
                                ppbtDict.W = 0;
                                ppbtDict.S = 0;
                                ppbtDict.L = 0;
                                ppbtDict.O = 0;
                                penglishList.push(ppbtDict);
                            }

                            if(pte){
                                ppteDict.name = 'pte academic';
                                ppteDict.description = pte
                                ppteDict.min = 0;
                                ppteDict.require = await utils.giveMeNumber(pte);
                                ppteDict.max = 90;
                                ppteDict.R = 0;
                                ppteDict.W = 0;
                                ppteDict.S = 0;
                                ppteDict.L = 0;
                                ppteDict.O = 0;
                                penglishList.push(ppteDict);
                            }                       
                           if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }
                            else {
                                courseAdminReq.englishprogress = [];
                            }
                            
                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq];
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }

                            resJsonData.course_admission_requirement.entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            resJsonData.course_admission_requirement.english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_more_details);
                            resJsonData.course_admission_requirement.academic_requirements_url = ""
                            //await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);


                            break;
                        }
                        case 'course_url': {

                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("FilterData---->" + JSON.stringify(filtered_duration_formated))
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {

                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;

                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':

                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            for (let program_val of program_code) {

                                resJsonData.program_code = program_val;

                            }

                            break;
                        }
                        case 'course_campus_location': {

                            const courseKeyVals = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            var campLocationText = null;
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        campLocationText = selectorsList;
                                    }
                                }
                            }

                            
                            if (campLocationText && campLocationText.length > 0) {
                                
                                var mylocationdata = await JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                                var campuses = mylocationdata["campuses"];
                                var location_data = [];
                                var arr=[]
                                // var location_data_duration = [];
                                var intake_data = [];
                                var nsplitdata

                                var scrapped_location_data = await utils.giveMeArray(campLocationText.toString(), ",");
                                console.log("campLocationText###===>",String(campLocationText[0].includes('Semester 1'))&& String(campLocationText[0].includes("Semester 2")))
                                if(String(campLocationText[0]).includes(":") && String(campLocationText[0].includes('Semester 1'))){
                                    let splitdata=String(campLocationText[0]).split(":")
                                    console.log("SplitData==>",splitdata)
                                    let splitpart=String(splitdata[1]).split(",");
                                    console.log(splitpart);
                                    splitpart.forEach(data=>{
                                       var mergelocation=splitdata[0]+" "+data
                                       arr.push(mergelocation)
                                    })
                                }
                                else if(String(campLocationText[0].includes('Semester 2')) && String(campLocationText[0].includes("Semester 1"))){
                                    console.log("yessss")
                                    let splitdata=String(campLocationText[0]).split(":")
                                    console.log("SplitDatainif==>",splitdata)
                                    let splitpart=String(splitdata[1]).split(",");
                                    console.log(splitpart);
                                    var loc=splitdata[0]+""+splitpart[0]+""+splitpart[1]
                                    arr.push(loc)
                                    var locs=splitpart[2]+""+splitpart[0]+""+splitpart[1]
                                    arr.push(locs)

                                }
                                 
                                console.log("campLocationText===>",arr)
                                campuses.forEach(element => {
                                    var value = [];

                                    var ismatch = false;
                                    var isfulltime = false;
                                    var isparttime = false;
                                    arr.forEach(elementloc => {
                                        if (elementloc.includes(element)) {
                                            ismatch = true;
                                           console.log("yes")
                                            if (elementloc.includes("Semester 1") ) {
                                                value.push(mylocationdata["Semester1"]);
                                            }
                                            if (elementloc.includes("Semester 2")) {
                                                value.push(mylocationdata["Semester2"]);
                                            }
                                            if (elementloc.toLowerCase().includes("full-time")) {
                                                isfulltime = true;
                                            }
                                            if (elementloc.toLowerCase().includes("part-time")) {
                                                isparttime = true;
                                            }
                                        }
                                    });
                                    console.log("#####Campus-->" + value)
                                    if (value.length > 0 && ismatch) {
                                        intake_data.push({ name: element, value: value });
                                        location_data.push(element);
                                        //  location_data_duration.push({ name: element, isfulltime: isfulltime, isparttime: isparttime })
                                    }
                                    else if (ismatch) {
                                        intake_data.push({ name: element, value: [] });

                                    }
                                });



                                let formatIntake = await utils.providemyintake(intake_data, "mom", "");
                                console.log("FormatIntake", JSON.stringify(formatIntake))

                                var intakedata = {};
                                intakedata.intake = formatIntake;
                                intakedata.more_details = mylocationdata["intake_url"];
                                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
                                //   resJsonData.location_data_duration = location_data_duration;
                                console.log("Campus_data" + JSON.stringify(location_data));
                                console.log("Intake_data" + JSON.stringify(intake_data));
                                var campLocationValTrimmed = String(campLocationText).trim();
                                if (location_data && location_data.length > 0) {                                  
                                    var campusedata = [];
                                    location_data.forEach(element => {
                                        campusedata.push({
                                            "name": element,
                                            "code": String(course_cricos_code)
                                        })
                                    });
                                    resJsonData.course_campus_location = campusedata;
                                    console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

                                }
                                else {
                                    throw new Error("No campus location found.");
                                }

                                resJsonData.course_study_mode = "On campus"
                            }
                            break;
                        }


                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};
                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feeYear && feeYear.length > 0) {
                                courseTuitionFee.year = feeYear;
                            }
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            var feesint = {};
                                            feesint.amount = Number(feesNumber);
                                            feesint.duration = "1";
                                            feesint.unit = "year";                                         
                                            feesint.description = feesIntStudent;                                         
                                            feesDict.international_student = feesint;




                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                var feesint = {};
                                feesint.amount = 0;
                                feesint.duration = "1";
                                feesint.unit = "year";                              
                                feesint.description = (feesIntStudent) ? feesIntStudent : "";                              
                                feesDict.international_student = feesint;
                            }
                            const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                            const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                            if (feesDuration && feesDuration.length > 0) {
                                feesDict.fee_duration_years = feesDuration;
                            }
                            if (feesCurrency && feesCurrency.length > 0) {
                                feesDict.currency = feesCurrency;
                            }
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                // feesDict.international_student_all_fees = [];
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                }
                            }
                            else {
                                feesDict.international_student = 0

                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                if (courseTuitionFee) {
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);

                            break;
                        }


                        case 'course_country': {
                            resJsonData.course_country = await Course.extractValueFromScrappedElement(courseScrappedData.course_country);
                            break;
                        }
                        case 'course_overview': {
                            resJsonData.course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_career_outcome) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            }
                            else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }


                        case 'course_study_level': {
                            //  const studyLevel = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
                            if (studyLevel) {
                                const arrval1 = String(studyLevel).split('courses');
                                const feesVal = String(arrval1[0]);

                                resJsonData.course_study_level = feesVal.trim();
                            } else {
                                throw new Error("Study Level not Found");
                            }

                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                            const courseKeyVal_major =courseScrappedData.course_outline;
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                            let course_outlines = {};
                            console.log("MAjor==>", JSON.stringify(courseKeyVal_major))
                            if (courseKeyVal_minor != null) {
                                course_outlines.minors = courseKeyVal_minor
                            } else {
                                course_outlines.minors = []
                            }
                            if (courseKeyVal_major) {
                                let course_career_outcome = null;
                                if (Array.isArray(courseKeyVal_major)) {
                                    for (const rootEleDict of courseKeyVal_major) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    course_career_outcome = selList;
                                                }
                                            }
                                        }
                                    }
                                }
                                if(course_career_outcome!=null){
                                    course_outlines.majors = course_career_outcome
                                }else{
                                    course_outlines.majors = []
                                }
                              
                            } else {
                                course_outlines.course_major = []
                            }
                            if (courseKeyVal != null) {
                                course_outlines.more_details = courseKeyVal
                            } else {
                                course_outlines.more_details = ""
                            }
                            resJsonData.course_outline = course_outlines
                            break;
                        }
                        case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            if (courseKeyVal != null) {
                                resJsonData.application_fees = courseKeyVal
                            } else {
                                resJsonData.application_fees = ""
                            }
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;




                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));




            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, await s.page.url(), courseDict.category, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
