**University Of Tasmania: Total Courses Details As On 22 April 2019**
* Total Courses = 465
* Courses without CRICOS code = 0
* Total available courses = 
* Repeted = 0
**InternationalTabFlag**
* first condition is checked for international student flag
**IELTS**
* IELTS taken default from --> http://www.utas.edu.au/international/applying/entry-requirements/english-language-requirements
* mapping is done based on coursename and studylevel
**Fees**
* http://www.utas.edu.au/courses/dvc-research/courses/s8b-master-of-applied-science-in-aquaculture
**Static content**
* Added this content if duration is not available
* eg: http://utas.edu.au/courses/E9G
"course_duration": [
        {
            "duration_full_time": "Not available duration"
        }
    ],
    "course_duration_display": [
        [
            {
                "unit": "",
                "duration": "NA",
                "display": "NA"
            }
        ]
    ],
**Duration static added**
* https://www.utas.edu.au/courses/cale/courses/f5d-graduate-certificate-in-fine-art-and-design-specialisation
* https://www.utas.edu.au/courses/cale/courses/f6g-graduate-diploma-of-fine-art-and-design-specialisation
* https://www.utas.edu.au/courses/cale/courses/f7d-master-of-fine-art-and-design-specialisation
* https://www.utas.edu.au/courses/cale/courses/63s-bachelor-of-legal-studies
**Doubt**
* http://www.utas.edu.au/courses/chm/courses/m5i-graduate-certificate-of-counselling
* International tab not given still contains CRICOS CODE dont take this course manually remove it Index = 44