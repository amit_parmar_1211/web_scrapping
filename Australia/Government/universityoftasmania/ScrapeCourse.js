const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
// const mapping = require('./output/Mapping/main');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name, study_level) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            let more_details = []
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'InternationalTabFlag': {
                            console.log("ENTERED CASE!!");
                            //const courseKeyVal = courseScrappedData.InternationalTabFlag;                            
                            var InternationalTabFlag = await Course.extractValueFromScrappedElement(courseScrappedData.InternationalTabFlag);
                            console.log("courseScrappedData.InternationalTabFlag = ", InternationalTabFlag);
                            if (!InternationalTabFlag || InternationalTabFlag.length == 0) {
                                throw new Error("Not available for International Student");
                            }

                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            console.log("courseScrappedData.program_code = ", courseScrappedData.program_code);
                            var program_code = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList[0].split(": ");
                                                program_code = program_code[1];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            let program_code_array = "";
                            if (program_code) {
                                program_code_array = program_code;
                                resJsonData.program_code = program_code_array;
                            } else {
                                resJsonData.program_code = "";
                            }

                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = category_name;
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];

                            var myscrore = '';
                            let ieltsNumber = null; let pbtNumber = null; let pteNumber = null; let caeNumber = null; let ibtNumber = null

                            // english requirement
                            let titlescrap = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            console.log("resJsonData.course_title -->", titlescrap);
                            console.log("resJsonData.course_study_level -->", resJsonData.course_study_level);
                            let studyLevel = resJsonData.course_study_level;
                            if (studyLevel == "Undergraduate") {
                                myscrore = await utils.getMappingScore(titlescrap, "Undergraduate");
                            }
                            else if (studyLevel == "Higher degrees by research") {
                                myscrore = await utils.getMappingScore(titlescrap, "Higher degrees by research");
                            }
                            else {
                                myscrore = await utils.getMappingScore(titlescrap, "Postgraduate");
                            }

                            console.log("myscrore -->", myscrore);
                            // if ieltsNumber is valid then map IBT and PBT accordingly
                            var ibtDict = {}, catDict = {}, pteDict = {}, ieltsDict = {}, pbtDict = {}, oetDict = {}, jcuceapDict = {};
                            if (myscrore.IELTS) {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = myscrore.IELTS;
                                englishList.push(ieltsDict);
                            }
                            if (myscrore.IBT) {
                                ibtDict.name = 'toefl ibt';
                                ibtDict.description = myscrore.IBT;
                                englishList.push(ibtDict);
                            }
                            if (myscrore.PTE) {
                                pteDict.name = 'pte academic';
                                pteDict.description = myscrore.PTE;
                                englishList.push(pteDict);
                            }


                            const regEx = /[+-]?\d+(\.\d+)?/g;
                            const matchedStrList = String(myscrore.IELTS).match(regEx);
                            console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                            if (matchedStrList && matchedStrList.length > 0) {
                                ieltsNumber = Number(matchedStrList[0]);
                                console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                            }
                            if (ieltsNumber && ieltsNumber > 0) {

                                //toefl ibt
                                let tempIBT = [
                                    {
                                        "elements":
                                            [
                                                {
                                                    "selectors":
                                                        [
                                                            [myscrore.IBT]
                                                        ]
                                                }
                                            ]
                                    }
                                ]
                                const toeflScoreibt = await Course.extractValueEnglishlanguareFromScrappedElement(tempIBT);
                                if (toeflScoreibt && toeflScoreibt.length > 0) {
                                    // toeflfinaloutput = toeflScore.toString();
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(toeflScoreibt).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ibtNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'ibtNumber = ' + ibtNumber);
                                    }
                                }
                                //pte score
                                console.log("myscrore.PTE -->", myscrore.PTE);
                                let tempPTE = [
                                    {
                                        "elements":
                                            [
                                                {
                                                    "selectors":
                                                        [
                                                            [myscrore.PTE]
                                                        ]
                                                }
                                            ]
                                    }
                                ]
                                const pteAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempPTE);
                                if (pteAScore && pteAScore.length > 0) {
                                    // ptefinaloutput = pteAScore.toString();
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(pteAScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pteNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'pteNumber = ' + pteNumber);
                                    }
                                }
                            }

                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            const penglishList = [];
                            if (ieltsNumber && ieltsNumber > 0) {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = ieltsDict.description;
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsNumber;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }
                            if (ibtNumber && ibtNumber > 0) {
                                pibtDict.name = 'toefl ibt';
                                pibtDict.description = ibtDict.description;
                                pibtDict.min = 0;
                                pibtDict.require = ibtNumber;
                                pibtDict.max = 9;
                                pibtDict.R = 0;
                                pibtDict.W = 0;
                                pibtDict.S = 0;
                                pibtDict.L = 0;
                                pibtDict.O = 0;
                                penglishList.push(pibtDict);
                            }


                            if (pteNumber && pteNumber > 0) {
                                ppteDict.name = 'pte academic';
                                ppteDict.description = pteDict.description;
                                ppteDict.min = 0;
                                ppteDict.require = pteNumber;
                                ppteDict.max = 9;
                                ppteDict.R = 0;
                                ppteDict.W = 0;
                                ppteDict.S = 0;
                                ppteDict.L = 0;
                                ppteDict.O = 0;
                                penglishList.push(ppteDict);

                            }

                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            } else {
                                courseAdminReq.english = [];
                            }
                            ///progrssbar End


                            // if (englishList && englishList.length > 0) {
                            //     courseAdminReq.english = englishList;
                            // } else {
                            //     courseAdminReq.english = [];
                            // }

                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq];
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }

                            // if courseAdminReq has any valid value then only assign it
                            resJsonData.course_admission_requirement = courseAdminReq;
                            // add english requirement 'english_more_details' link
                            let resEnglishReqMoreDetailsJson = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            //let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                //resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            }

                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            //FULL TIME AND PART TIME IS DONE IN THIS
                            //CUSTOMIZED CODE DONOT COPY
                            const courseDurationList = [];
                            const courseDurationDisplayList = [];
                            const durationFullTime = {};

                            // let anotherArray = [];
                            let duration1 = courseScrappedData.course_duration_full_time;

                            console.log("*************start formating Full Time years*************************");
                            console.log("courseScrappedData.course_duration_full_time = ", JSON.stringify(courseScrappedData.course_duration_full_time));
                            let finalDuration = duration1[0].elements[0].selectors[0][0];
                            if (finalDuration != undefined) {
                                const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration);
                                console.log("full_year_formated_data", full_year_formated_data);
                                ///course duration
                                const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                                console.log("not_full_year_formated_data", not_full_year_formated_data);
                                try {
                                    // durationFullTime.duration_full_time = not_full_year_formated_data;
                                    // courseDurationList.push(durationFullTime);
                                    ///END course duration
                                    console.log("courseDurationList : ", courseDurationList);
                                    ///course duration display

                                    courseDurationDisplayList.push(full_year_formated_data);

                                    demoarray = courseDurationDisplayList[0];
                                    console.log("demoarray--->", demoarray);
                                    // anotherArray.push(courseDurationDisplayList);
                                    console.log("courseDurationDisplayList", courseDurationDisplayList);
                                    ///END course duration display
                                } catch (err) {
                                    console.log("Problem in Full Time years:", err);
                                }
                                console.log("courseDurationDisplayList1", courseDurationDisplayList);
                                console.log('***************END formating Full Time years**************************')
                                let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                //  if (courseDurationList && courseDurationList.length > 0) {
                                resJsonData.course_duration = not_full_year_formated_data;
                                //  }
                                if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                    resJsonData.course_duration_display = filtered_duration_formated;
                                    var isfulltime = false, isparttime = false;
                                    filtered_duration_formated.forEach(element => {
                                        if (element.display == "Full-Time") {
                                            isfulltime = true;
                                        }
                                        if (element.display == "Part-Time") {
                                            isparttime = true;
                                        }
                                    });
                                    resJsonData.isfulltime = isfulltime;
                                    resJsonData.isparttime = isparttime;
                                }
                            }
                            // else {
                            //     throw new Error("Duration not available");

                            // }

                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            console.log("demoarray123 -->", demoarray);
                            if (demoarray == '') {
                                console.log("Yes you entered-->");
                                let feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);


                                console.log("feesIntStudentBefore -->", feesIntStudent);
                                let feesIntStudentsecond;
                                if (feesIntStudent && feesIntStudent.indexOf('of') > -1) {
                                    let splitFees = feesIntStudent.split('of');
                                    feesIntStudentsecond = splitFees[1];
                                }
                                console.log("feesIntStudentAfter -->", feesIntStudentsecond);
                                // if we can extract value as Int successfully then replace Or keep as it is
                                if (feesIntStudentsecond && feesIntStudentsecond.length > 0) { // extract only digits
                                    const feesWithDollorTrimmed = String(feesIntStudentsecond).trim();
                                    console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                    const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                                    console.log(funcName + 'feesVal = ' + feesVal);
                                    if (feesVal) {
                                        // const regEx = /\d/g;
                                        const regEx = /\d+\.?\d*/g; // /[0-9]/g;
                                        let feesValNum = feesVal.match(regEx);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            let feesNumber = null;
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }
                                            console.log(funcName + 'feesNumber77 = ' + feesNumber);
                                            if (Number(feesNumber)) {
                                                feesDict.international_student = ({
                                                    amount: Number(feesNumber),
                                                    duration: 1,
                                                    unit: "Year",
                                                    description: "",
                                                });
                                            } else {
                                                feesDict.international_student = ({
                                                    amount: 0,
                                                    duration: 1,
                                                    unit: "Year",
                                                    description: "not available fee",
                                                });
                                            }

                                        }
                                    }
                                }

                                if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                    const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                    const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                    if (feesDuration && feesDuration.length > 0) {
                                        feesDict.fee_duration_years = feesDuration;
                                    }
                                    if (feesCurrency && feesCurrency.length > 0) {
                                        feesDict.currency = feesCurrency;
                                    }
                                    console.log("(feesIntStudent.length > 0) -->", feesIntStudent);
                                    if (feesDict.international_student.length > 0) { // extract only digits
                                        console.log("international_student -->" + feesIntStudent)
                                        const courseKeyVal = courseScrappedData.course_tuition_fees_international_student_more_details;
                                        console.log('value of feee :' + courseKeyVal);
                                        if (Array.isArray(courseKeyVal)) {
                                            for (const rootEleDict of courseKeyVal) {
                                                console.log(funcName + '\n\r rootEleDict fee more= ' + JSON.stringify(rootEleDict));
                                                const elementsList = rootEleDict.elements;
                                                for (const eleDict of elementsList) {
                                                    console.log(funcName + '\n\r eleDict fee more= ' + JSON.stringify(eleDict));
                                                    const selectorsList = eleDict.selectors;
                                                    for (const selList of selectorsList) {
                                                        console.log(funcName + '\n\r selList fee more= ' + JSON.stringify(selList));
                                                        if (Array.isArray(selList) && selList.length > 0) {
                                                            more_details.push(selList[0]);
                                                            console.log('course fee more details1232432354 :' + more_details);
                                                            //  feesDict.international_student_all_fees = more_details;
                                                            //console.log('course fee more details :' + feesDict.international_student_all_fees);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (feesDict) {
                                        // feesList.push(feesDict);
                                        var campus = resJsonData.course_campus_location;
                                        for (let loc of campus) {
                                            feesList.push({ name: loc.name, value: feesDict });
                                        }
                                    }
                                    if (feesList && feesList.length > 0) {
                                        courseTuitionFee.fees = feesList;
                                    }
                                    console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                    // take tuition fee value at json top level so will help in DDB for indexing
                                    if (courseTuitionFee && feesDict.international_student) {
                                        resJsonData.course_tuition_fee = courseTuitionFee;
                                    }
                                }
                                break;
                            }

                            let duration_yesr = demoarray[0];
                            console.log("duration_yesr -->", duration_yesr);

                            let duration_unit = demoarray[0];
                            console.log("duration_yesr -->", duration_unit);
                            let feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);


                            console.log("feesIntStudentBefore -->", feesIntStudent);
                            let feesIntStudentsecond;
                            if (feesIntStudent && feesIntStudent.indexOf('of') > -1) {
                                let splitFees = feesIntStudent.split('of');
                                feesIntStudentsecond = splitFees[1];
                            }
                            console.log("feesIntStudentAfter -->", feesIntStudentsecond);
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudentsecond && feesIntStudentsecond.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudentsecond).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    // const regEx = /\d/g;
                                    const regEx = /\d+\.?\d*/g; // /[0-9]/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: feesIntStudent,
                                            });
                                        }
                                        if (feesNumber == "0") {
                                            console.log("FEesNumber = 0");
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: 1,
                                                unit: "Year",
                                                description: "",
                                            });
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",
                                    description: "",
                                });
                            }

                            // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = ({
                                                            amount: Number(inetStudentFees),
                                                            duration: 1,
                                                            unit: "Year",
                                                            description: "",
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if

                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                console.log("(feesIntStudent.length > 0) -->", feesIntStudent);
                                if (feesDict.international_student.length > 0) { // extract only digits
                                    console.log("international_student -->" + feesIntStudent)
                                    const courseKeyVal = courseScrappedData.course_tuition_fees_international_student_more_details;
                                    console.log('value of feee :' + courseKeyVal);
                                    if (Array.isArray(courseKeyVal)) {
                                        for (const rootEleDict of courseKeyVal) {
                                            console.log(funcName + '\n\r rootEleDict fee more= ' + JSON.stringify(rootEleDict));
                                            const elementsList = rootEleDict.elements;
                                            for (const eleDict of elementsList) {
                                                console.log(funcName + '\n\r eleDict fee more= ' + JSON.stringify(eleDict));
                                                const selectorsList = eleDict.selectors;
                                                for (const selList of selectorsList) {
                                                    console.log(funcName + '\n\r selList fee more= ' + JSON.stringify(selList));
                                                    if (Array.isArray(selList) && selList.length > 0) {
                                                        more_details.push(selList[0]);
                                                        console.log('course fee more details1232432354 :' + more_details);
                                                        // feesDict.international_student_all_fees = more_details;
                                                        // console.log('course fee more details :' + feesDict.international_student_all_fees);

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (feesDict) {
                                    // feesList.push(feesDict);
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                                if (!feesDict.international_student) {
                                    console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                    console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                    console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                    console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                    return null; // this will add this item into FailedItemList and writes file to local disk
                                }
                            } else {

                            }
                            break;
                        }
                        case 'course_campus_location': { // Location Launceston 
                            const rootElementDictList = courseScrappedData.course_campus_location;
                            console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedSelectorsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                // let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        if (selList && selList.length > 0) {
                                            console.log('course_campus_location selList = ' + JSON.stringify(selList));
                                            let concatnatedSelStr = [];
                                            for (let selItem of selList) {

                                                selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ';').split(";");
                                                for (let sel of selItem) {
                                                    sel = sel.toLowerCase().trim();
                                                    console.log("sel -->", sel.toLowerCase());

                                                    if (sel.toLowerCase().indexOf("distance") == -1 && sel.toLowerCase().indexOf(" online") == -1 && sel.toLowerCase().indexOf("external") == -1) {
                                                        concatnatedSelStr.push(sel);
                                                    }
                                                }
                                            }

                                            const rootElementDictList111 = courseScrappedData.course_cricos_code;
                                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList111));
                                            let concatnatedRootElementsStr = null;
                                            let splitCricos;
                                            for (const rootEleDict of rootElementDictList111) {
                                                console.log(funcName + '\n\r rootEleDict course_cricos_code = ' + JSON.stringify(rootEleDict));
                                                const elementsList = rootEleDict.elements;
                                                for (const eleDict of elementsList) {
                                                    console.log(funcName + '\n\r eleDict course_cricos_code = ' + JSON.stringify(eleDict));
                                                    const selectorsList = eleDict.selectors;
                                                    for (const selList of selectorsList) {
                                                        // console.log(funcName + '\n\r selList  course_cricos_code = ' + JSON.stringify(selList));
                                                        console.log(funcName + '\n\r selList course_cricos_code 1 = ' + selList);

                                                        if (selList[0].indexOf("CRICOS:") > -1) {
                                                            splitCricos = selList[0].split("CRICOS:")[1].trim();
                                                            concatnatedRootElementsStr = splitCricos;
                                                            console.log("concatnatedRootElementsStr------>>>>", concatnatedRootElementsStr);
                                                        }
                                                    }
                                                }
                                            }
                                            // selList
                                            console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);
                                            if (concatnatedSelStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            }


                                            console.log("Capusss@@@@", JSON.stringify(concatnatedSelectorsStr))
                                            if (concatnatedSelectorsStr && concatnatedSelectorsStr.length > 0) {
                                                var campusedata = [];
                                                concatnatedSelectorsStr.forEach(element => {
                                                    campusedata.push({
                                                        "name": format_functions.titleCase(element),
                                                        "code": concatnatedRootElementsStr.toString()
                                                    })
                                                });

                                                resJsonData.course_campus_location = campusedata;

                                                console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

                                                // study_mode_array.push("On campus");
                                                resJsonData.course_study_mode = "On campus"
                                            } else {
                                                throw new Error("Campus location not found");
                                            }
                                        }
                                        else {
                                            throw new Error("Campus location not found");
                                        }
                                    } // selectorsList 
                                } // elementsList 
                            } // rootElementDictList 
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            // Semester 1 (February) and Semester 2 (August)
                            // prepare emty JSON struct
                            var courseIntakeStr = [];//= await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            let anotherArray = [];

                            const courseIntakeDList = await utils.getValueFromHardCodedJsonFile('course_intake');
                            const courseKeyVal = courseScrappedData.course_intake;

                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r Intake selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                for (let intake of selList) {
                                                    let splitIntake = intake.split(', ');
                                                    let individualIntake = [];
                                                    for (let scrapeIntake of splitIntake) {
                                                        for (let fileIntake of courseIntakeDList) {
                                                            let filrSplitIntake = fileIntake.split(' - ');
                                                            if (filrSplitIntake[0] == scrapeIntake) {
                                                                if (!individualIntake.includes(filrSplitIntake[1])) {
                                                                    individualIntake.push(filrSplitIntake[1]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    anotherArray.push(individualIntake);
                                                }
                                            }
                                        }
                                        console.log("anotherArray1 -->", anotherArray);
                                        for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                                            courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": anotherArray[j] });
                                        }
                                    }
                                }
                            }
                            console.log("courseIntakeStr -->", courseIntakeStr);
                            let formatIntake = await format_functions.providemyintake(courseIntakeStr, "MOM", "");
                            var intakeUrl = await utils.getValueFromHardCodedJsonFile('intake_url');
                            if (formatIntake && formatIntake.length > 0) {
                                var intakedata = {};
                                intakedata.intake = formatIntake;
                                intakedata.more_details = intakeUrl;
                                resJsonData.course_intake = intakedata;
                                // for (let intakeval of resJsonData.course_intake.intake) {
                                //     console.log("intakeval--->", intakeval);
                                //     for (let intakeval111 of intakeval.value) {
                                //         console.log("month--->", intakeval);
                                //         for (intakeval of intake_date) {
                                //             console.log("valmonths11--->", intakeval);
                                //             if (intakeval.key == intakeval111.actualdate) {
                                //                 console.log("month.key--->", intakeval.key);
                                //                 intakeval111.filterdate = intakeval.value;
                                //                 // formatedIntake.filterdate.push(valmonths11.value);
                                //                 //formatedIntake.push(intakedata.filterdate );
                                //                 console.log("intake_months--->", intakeval111.filterdate);
                                //                 break;
                                //             }
                                //         }
                                //     }
                                // }
                            }
                            break;
                        }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_country = resAccomodationCostJson;
                            }
                            break;
                        }
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            } else {
                                resJsonData[key] = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                console.log("Inside If Condition");
                                concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = concatnatedRootElementsStrArray;
                            }
                            break;
                        }
                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };
                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal111 = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal111------->", JSON.stringify(courseKeyVal111))
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal111)) {
                                for (const rootEleDict of courseKeyVal111) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }
                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }
                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)
                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;
                            break;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                // if (application_fee.length > 0) {
                                //     resJsonData.application_fee = application_fee;
                                // } else {
                                resJsonData.application_fee = '';
                                //  }

                                break;
                            }
                        case 'course_program_code': {
                            console.log(funcName + 'matched case course_program_code: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict course_program_code = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict course_program_code = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList  course_program_code = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList course_program_code 1 = ' + selList);
                                        var crcodeval = selList.toString();
                                        concatnatedRootElementsStr = crcodeval;
                                    }
                                }
                            }
                            console.log(funcName + 'concatnatedRootElementsStr course_program_code = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            }
                            break;
                        }
                        case 'course_title': {
                            const title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            var ctitle = format_functions.titleCase(title);
                            console.log("ctitle@@@", ctitle)
                            resJsonData.course_title = ctitle
                            break;
                        }
                        case 'course_study_level': {
                            console.log(funcName + 'course_title course_study_level = ' + study_level);
                            resJsonData.course_study_level = study_level.trim();
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)

            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase() + resJsonData.program_code;
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                // resJsonData.basecourseid = location_wise_data.course_id;

                // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                //     if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                //         resJsonData.course_cricos_code[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_cricos_code[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                //     if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_intake.intake[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_intake.intake[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                //     if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                //     if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_campus_location[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_campus_location[i].iscurrent = false;
                //     }
                // }
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;

            ///end grnrating new file location wise
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            const programClick = await s.page.$x('/html/body/div[3]/div[2]/div[2]/div/div[1]/a');
            for (let link of programClick) {
                console.log('button link :' + link);
                await link.click();
            }
            const programClick1 = await s.page.$x('//div/a[contains(text(),"international student")]');
            for (let link of programClick1) {
                console.log('button link :' + link);
                await link.click();
            }
            // await s.page.waitForNavigation();


            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };