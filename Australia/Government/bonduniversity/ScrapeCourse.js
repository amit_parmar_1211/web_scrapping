const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
// const mapping = require('./output/Mapping/main');
request = require('request');
class ScrapeCourse extends Course {
    static titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    }

    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = course_category;
                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            console.log("courseScrappedData.program_code = ", courseScrappedData.course_program_code);
                            var program_code = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                console.log("JSON.stringify(selList) -->", JSON.stringify(selList));
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))

                            for (let program_val of program_code) {

                                resJsonData.program_code = program_val;
                            }
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const myscrore = await utils.getMappingScore(course_name);
                            console.log("## Score result--->" + JSON.stringify(myscrore));

                            var ibtDict = {}, catDict = {}, pteDict = {}, ieltsDict = {}, caeDict = {};
                            ibtDict.name = 'toefl ibt';
                            ibtDict.description = myscrore.TOEFL;
                            englishList.push(ibtDict);
                            pteDict.name = 'pte academic';
                            pteDict.description = myscrore.PTE;
                            englishList.push(pteDict);
                            ieltsDict.name = 'ielts academic';
                            ieltsDict.description = myscrore.IELTS;
                            englishList.push(ieltsDict);
                            catDict.name = 'cae';
                            catDict.description = myscrore.CAE;
                            englishList.push(catDict);

                            let ieltsNumber = null; let pbtNumber = null; let pteNumber = null; let caeNumber = null;
                            const penglishList = [];
                            var ieltsfinaloutput = null;
                            var tofelfinaloutput = null;
                            var ptefinaloutput = null;
                            var caefinaloutput = null;
                            const regEx = /[+-]?\d+(\.\d+)?/g;
                            const matchedStrList = String(myscrore.IELTS).match(regEx);
                            console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                            if (matchedStrList && matchedStrList.length > 0) {
                                ieltsNumber = Number(matchedStrList[0]);
                                console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                            }
                            if (ieltsNumber && ieltsNumber > 0) {
                                //tofel score
                                console.log("myscrore.TOEFL -->", myscrore.TOEFL);
                                let tempTOFEL = [
                                    {
                                        "elements":
                                            [
                                                {
                                                    "selectors":
                                                        [
                                                            [myscrore.TOEFL]
                                                        ]
                                                }
                                            ]
                                    }
                                ]
                                const tofelScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempTOFEL);
                                if (tofelScore && tofelScore.length > 0) {
                                    tofelfinaloutput = tofelScore.toString();
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(tofelScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pbtNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'pbtNumber = ' + pbtNumber);
                                    }
                                }
                                //pte score
                                console.log("myscrore.PTE -->", myscrore.PTE);
                                let tempPTE = [
                                    {
                                        "elements":
                                            [
                                                {
                                                    "selectors":
                                                        [
                                                            [myscrore.PTE]
                                                        ]
                                                }
                                            ]
                                    }
                                ]
                                const pteAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempPTE);
                                if (pteAScore && pteAScore.length > 0) {
                                    ptefinaloutput = pteAScore.toString();
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(pteAScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pteNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'pteNumber = ' + pteNumber);
                                    }
                                }
                                //cae score
                                console.log("myscrore.CAE -->", myscrore.CAE);
                                let tempCAE = [
                                    {
                                        "elements":
                                            [
                                                {
                                                    "selectors":
                                                        [
                                                            [myscrore.CAE]
                                                        ]
                                                }
                                            ]
                                    }
                                ]

                                const caeAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempCAE);
                                if (caeAScore && caeAScore.length > 0) {
                                    caefinaloutput = caeAScore.toString();
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(caeAScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        caeNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'caeNumber = ' + caeNumber);
                                    }
                                }
                            }

                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};

                            if (ieltsNumber && ieltsNumber > 0) {
                                var IELTSJSON = {
                                    "name": "ielts academic",
                                    "description": ieltsDict.description,
                                    "min": 0,
                                    "require": ieltsNumber,
                                    "max": 9,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (ieltsNumber) {
                                    penglishList.push(IELTSJSON);
                                }
                            }

                            if (pbtNumber && pbtNumber > 0) {
                                var IBTJSON = {
                                    "name": "toefl ibt",
                                    "description": ibtDict.description,
                                    "min": 0,
                                    "require": pbtNumber,
                                    "max": 120,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (pbtNumber) {
                                    penglishList.push(IBTJSON);
                                }
                            }

                            if (pteNumber && pteNumber > 0) {
                                var PTEJSON = {
                                    "name": "pte academic",
                                    "description": pteDict.description,
                                    "min": 0,
                                    "require": pteNumber,
                                    "max": 90,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (pteNumber) {
                                    penglishList.push(PTEJSON);
                                }
                            }

                            if (caeNumber && caeNumber > 0) {
                                var CAEJSON = {
                                    "name": "cae",
                                    "description": catDict.description,
                                    "min": 80,
                                    "require": caeNumber,
                                    "max": 230,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (caeNumber) {
                                    penglishList.push(CAEJSON);
                                }
                            }

                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }

                            var academicReq = "";
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList;
                                        }
                                    }
                                }
                            }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq];
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }
                            ///progrssbar End
                            let resEnglishReqMoreDetailsJson = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            // let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.academic_requirements_url = "";
                                resJsonData.course_admission_requirement.english_requirements_url = "";
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                            }
                            break;
                        }
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        //CUSTOMIZED CODE DO NOT COPY
                        case 'course_duration_full_time': {
                            const courseDurationList = [];
                            const courseDurationDisplayList = [];
                            const durationFullTime = {};

                            let anotherArray = [];
                            let durationFullTimeDisplay = null;
                            let duration1 = courseScrappedData.course_duration_full_time;

                            console.log("*************start formating Full Time years*************************");
                            console.log("courseScrappedData.course_duration_full_time = ", JSON.stringify(courseScrappedData.course_duration_full_time));
                            let finalDuration = duration1[0].elements[0].selectors[0][0];
                            // finalDuration += " Full Time";
                            console.log("Final Duration --> ", finalDuration);
                            if (finalDuration.includes('(')) {
                                // finalDuration = finalDuration.replace(/(?:,|,)/g, '');
                                let splitBracket = finalDuration.split('(');
                                console.log("splitBracket -->", splitBracket);
                                // splitBracket[1].replace(/(?:,|,)/g, '');
                                let splitBracket1 = splitBracket[1].split(')');
                                console.log("splitBracket1 -->", splitBracket1);
                                finalDuration = splitBracket1[0];
                            }
                            finalDuration += " Full Time";
                            console.log("Final Duration --> ", finalDuration);
                            const full_year_formated_data = await format_functions.validate_course_duration_full_time(finalDuration.trim());
                            console.log("full_year_formated_data", full_year_formated_data);
                            ///course duration
                            const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                            console.log("not_full_year_formated_data", not_full_year_formated_data);
                            try {
                                // durationFullTime.duration_full_time = not_full_year_formated_data;
                                // courseDurationList.push(durationFullTime);
                                // ///END course duration
                                console.log("courseDurationList : ", courseDurationList);
                                ///course duration display
                                durationFullTimeDisplay = {};
                                durationFullTimeDisplay.duration = full_year_formated_data[0].duration;
                                durationFullTimeDisplay.unit = full_year_formated_data[0].unit;
                                durationFullTimeDisplay.display = "Full-Time";
                                console.log("durationFullTimeDisplay", durationFullTimeDisplay);
                                courseDurationDisplayList.push(full_year_formated_data);
                                // anotherArray.push(courseDurationDisplayList);
                                console.log("courseDurationDisplayList", courseDurationDisplayList);
                                ///END course duration display
                            } catch (err) {
                                console.log("Problem in Full Time years:", err);
                            }

                            console.log("courseDurationDisplayList2", courseDurationDisplayList);
                            let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                            console.log("filtered_duration_formated", filtered_duration_formated);
                            //  if (courseDurationList && courseDurationList.length > 0) {
                            resJsonData.course_duration = not_full_year_formated_data;
                            // }
                            if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                resJsonData.course_duration_display = filtered_duration_formated;
                                var isfulltime = false, isparttime = false;
                                filtered_duration_formated.forEach(element => {
                                    if (element.display == "Full-Time") {
                                        isfulltime = true;
                                    }
                                    if (element.display == "Part-Time") {
                                        isparttime = true;
                                    }
                                });
                                resJsonData.isfulltime = isfulltime;
                                resJsonData.isparttime = isparttime;
                            }
                            break;

                        }
                        case 'course_tuition_fee':
                        //case 'course_study_mode':
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {}; const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            var feesIntStudent = [];
                            feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            console.log("courseScrappedData.course_tuition_fees_international_student :::::::: ", courseScrappedData.course_tuition_fees_international_student.elements);

                            //  const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            const feemore = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_more);
                            // console.log("feeYear ::::::: ", feeYear);
                            //Output : 2019 fees: $17,160 per semester (based on 4 subjects per semester)
                            let splitColon;
                            let splitBracket;
                            if (feesIntStudent) {
                                if (feesIntStudent.indexOf(':') > -1) {
                                    splitColon = feesIntStudent.split(':');
                                    console.log("splitColon ::::::::: ", splitColon);
                                }
                                if (feesIntStudent.indexOf('(') > -1) {
                                    splitBracket = splitColon[1].split('(');
                                    console.log("splitColon[1] :: ", splitColon[1]);
                                    console.log("splitBracket[0]=", splitBracket[0].trim());
                                    feesIntStudent = splitBracket[0];
                                } else {
                                    feesIntStudent = splitColon[1];
                                }
                            }

                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "semester",
                                                description: feesWithDollorTrimmed,
                                            });
                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: 1,
                                                unit: "semester",
                                                description: "not available fee",
                                            });
                                        }
                                        if (feemore != null) {
                                            //  feesDict.international_student_all_fees = [feemore];
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: 1,
                                    unit: "semester",
                                    description: "",
                                });
                                if (feemore != null) {
                                    // feesDict.international_student_all_fees = [feemore];
                                }
                            }

                            // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = Number(inetStudentFees);
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if

                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }
                        case 'course_campus_location': { // Location Launceston

                            var campLocationText; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            console.log("##Campus-->" + JSON.stringify(campLocationText))



                            const rootElementDictList111 = courseScrappedData.course_cricos_code;
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList111));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList111) {
                                console.log(funcName + '\n\r rootEleDict course_cricos_code = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict course_cricos_code = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList  course_cricos_code = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList course_cricos_code 1 = ' + selList);
                                        if (selList.indexOf('This program is not available to international students who intend to apply for a student visa.') > -1) {

                                        } else {
                                            var crcode = selList.toString().split(' ');
                                            const cricoscode = crcode[0];
                                            concatnatedRootElementsStr = cricoscode;
                                        }

                                    }
                                }
                            }
                            for (let location_val of campLocationText) {
                                if (location_val && location_val.length > 0) {
                                    var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                    //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                    // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                    var campusedata = [];

                                    location_val.forEach(element => {
                                        campusedata.push({
                                            "name": this.titleCase(element),
                                            "code": concatnatedRootElementsStr.toString()
                                        })
                                    });
                                    resJsonData.course_campus_location = campusedata;
                                }
                            }
                            break;
                        }
                        case 'course_study_mode': {
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }


                        case 'course_intake':
                            {
                                let courseIntakeStr = [];
                                const courseKeyVal = courseScrappedData.course_intake;

                                console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                // if (Array.isArray(selList) && selList.length > 0) {
                                                for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                                                    let anotherArray = [];
                                                    for (let i = 0; i < selList.length; i++) {
                                                        anotherArray.push(selList[i]);

                                                    }
                                                    console.log("selList[i] -->", anotherArray);
                                                    courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": anotherArray });
                                                    console.log("courseIntakeStr -->", courseIntakeStr);
                                                }
                                            }
                                        }
                                    }
                                }
                                console.log("resJsonData.course_intake_url --> ", resJsonData.course_intake_url);
                                console.log("intakes" + JSON.stringify(courseIntakeStr));
                                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                let formatIntake = await format_functions.providemyintake(courseIntakeStr, "MOM", "");

                                if (formatIntake && formatIntake.length > 0) {
                                    var intakedata = {};
                                    intakedata.intake = formatIntake;
                                    intakedata.more_details = more_details;
                                    resJsonData.course_intake = intakedata;
                                    console.log("resJsonData.course_intake -->> ", resJsonData.course_intake);
                                }
                                break;
                            }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            else {
                                resJsonData[key] = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_career_outcome) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            } else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }

                        // case 'course_cricos_code': {
                        //     console.log(funcName + 'matched case course_cricos_code: ' + key);
                        //     const rootElementDictList = courseScrappedData[key];
                        //     console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                        //     let concatnatedRootElementsStr = null;
                        //     for (const rootEleDict of rootElementDictList) {
                        //         console.log(funcName + '\n\r rootEleDict course_cricos_code = ' + JSON.stringify(rootEleDict));
                        //         const elementsList = rootEleDict.elements;
                        //         for (const eleDict of elementsList) {
                        //             console.log(funcName + '\n\r eleDict course_cricos_code = ' + JSON.stringify(eleDict));
                        //             const selectorsList = eleDict.selectors;
                        //             for (const selList of selectorsList) {
                        //                 console.log(funcName + '\n\r selList  course_cricos_code = ' + JSON.stringify(selList));
                        //                 console.log(funcName + '\n\r selList course_cricos_code 1 = ' + selList);
                        //                 if (selList.indexOf('This program is not available to international students who intend to apply for a student visa.') > -1) {

                        //                 } else {
                        //                     var crcode = selList.toString().split(' ');
                        //                     const cricoscode = crcode[0];
                        //                     concatnatedRootElementsStr = cricoscode;
                        //                 }

                        //             }
                        //         }
                        //     }
                        //     console.log(funcName + 'concatnatedRootElementsStr course_cricos_code = ' + concatnatedRootElementsStr);
                        //     // add only if it has valid value
                        //     const mycodes = [];
                        //     if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                        //         const locations = resJsonData.course_campus_location;
                        //         console.log("locations----->", locations);
                        //         console.log("course_cricos_code----->", concatnatedRootElementsStr);
                        //         for (let location of locations) {
                        //             mycodes.push({
                        //                 location: location.name, code: concatnatedRootElementsStr.toString(), iscurrent: false
                        //             });
                        //         }
                        //         resJsonData.course_cricos_code = mycodes;
                        //     } else {
                        //         throw new Error("Cricos Code not found");
                        //     }
                        //     break;
                        // }
                        case 'course_title': {
                            console.log(funcName + 'matched case course_title: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is course_title' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict course_title= ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict course_title= ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList course_title= ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList course_title= ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr course_title = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr.replace("\n", " ");
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).replace("\n", " ").concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr course_title = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr course_title = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            concatnatedRootElementsStr = this.titleCase(concatnatedRootElementsStr);
                            console.log(funcName + 'concatnatedRootElementsStr course title = ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                            }
                            break;
                        }
                        case 'course_study_level': {
                            const cStudyLevel = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
                            if (cStudyLevel) {
                                resJsonData.course_study_level = cStudyLevel;
                            }
                            break;
                        }
                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };
                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal111 = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal111------->", JSON.stringify(courseKeyVal111))
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal111)) {
                                for (const rootEleDict of courseKeyVal111) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }
                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }
                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)
                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;
                            break;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                // if (application_fee.length > 0) {
                                //     resJsonData.application_fee = application_fee;
                                // } else {
                                    resJsonData.application_fee = '';
                                // }

                                break;
                            }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            ////start genrating new file location wise
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;

                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href.replace('https://bond.edu.au/', 'https://bond.edu.au/intl/'));

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
