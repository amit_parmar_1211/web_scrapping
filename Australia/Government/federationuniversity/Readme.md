**Federation University: Total Courses Details As On 2 April 2019**
* Total Courses = 133
* Courses without CRICOS code = 0
* DDB = 0
* Web portal available courses = 0

**Notes**
* Ielts mapping specified in the hardcoded file is unique and custom to the site.
* Location fetched from location and cricos code string. Plus brisbane location added in logic according to special case.
* If IELTS is not given default 6 is given



**2019 & conditions**
* link of academic calendar might only be valid for 2019.
