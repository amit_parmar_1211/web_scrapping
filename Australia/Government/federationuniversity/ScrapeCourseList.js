const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');

const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
      var totalCourseList = [];
      var mainCategory = [];
      var studyLevelArray = [];
      //click on internation button
      const buttonInternational = "//*[@id='modal-placeholder']/div[1]/div/div/div/button[2]";
      var buttonInternationalelem = await page.$x(buttonInternational);
      await buttonInternationalelem[0].click();

      //scrape main category
      const maincategoryselector = "//*[@id='results-view']/div[2]/div/programs-list/div/div/div/a";
      var category = await page.$x(maincategoryselector);

      const studyLevelselectorName = "//h2[contains(text(),'Level of study ')]/following-sibling::fieldset[@class='inner_scroll']/label/span";

      var studyLevelName = await page.$x(studyLevelselectorName);
      console.log("Total categories-->" + category.length);
      for (let link of category) {
        var categorystringmain = await page.evaluate(el => el.innerText, link);
        mainCategory.push(categorystringmain.trim());
      }
      for (let link of studyLevelName) {
        var studyLevelmain = await page.evaluate(el => el.innerText, link);
        studyLevelArray.push(studyLevelmain.trim());
      }
  
      //scrape courses
      for (let catc of mainCategory) {
        for (let k = 0; k < studyLevelArray.length; k++) {
          const link1 = await page.$("#results-view > div.container > div > programs-filter > div > div:nth-child(3) > fieldset > label:nth-child(" + (k + 1) + ")");
          var studyL = await page.evaluate(el => el.innerText, link1);
        
          try {
            await page.evaluate((el) => {

              return el.click()
            }, link1); await page.waitFor(1500);
          } catch (e) { console.log(e); }
          const titleselector = "//*[@id='results-view']/div[2]/div/programs-list/div/div/div/a[contains(h2/text(),'" + catc + "')]/../following-sibling::div//span[@class='title']";
          const linkselector = "//*[@id='results-view']/div[2]/div/programs-list/div/div/div/a[contains(h2/text(),'" + catc + "')]/../following-sibling::div//span[@class='title']/../following-sibling::div//a";
          var title = await page.$x(titleselector);
          var link = await page.$x(linkselector);
          let studylev = studyL.split("(");
          let splitstudyL = studylev[0].trim();
         console.log("title --> " + title.length + " href -> " + link.length + " studylevel ->" + splitstudyL);
          //split
          // let studylev = studyL.split("(");
          // studyL = studylev[0].trim();
         // console.log("studyyyyyyyyyyy",studyL);
          for (let i = 0; i < title.length; i++) {
            var categorystring = await page.evaluate(el => el.innerText, title[i]);
            var categoryurl = await page.evaluate(el => el.href, link[i]);
            var studyL = await page.evaluate(el => el.innerText, link1);
            totalCourseList.push({ href: categoryurl, innerText: categorystring, studyLevel: splitstudyL, category: catc });
            
          }
          try {
            await page.evaluate((el) => { return el.click() }, link1); await page.waitFor(1500);
          } catch (e) { console.log(e); }
          // await buttonInternationalelem[0].click();
        }
      }

      await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(mainCategory));

      await fs.writeFileSync("./output/federationuniversity_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, studyLevel: totalCourseList[i].studyLevel, category: []});
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, studyLevel: totalCourseList[i].studyLevel, category: []});
        }
      }
      // var flags = [], uniqueUrl = [], l = totalCourseList.length, i;
      // for (i = 0; i < l; i++) {
      //   if (flags[totalCourseList[i].href]) continue;
      //   flags[totalCourseList[i].href] = true;
      //   uniqueUrl.push(totalCourseList[i]);
      // }

      await fs.writeFileSync("./output/federationuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }

          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/federationuniversity_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  // refers to https://programsandcourses.anu.edu.au/catalogue
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // course_level_form_selector key
      // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelList) {
      //   console.log(funcName + 'Invalid formSelList');
      //   throw (new Error('Invalid formSelList'));
      // }
      // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelElementDict) {
      //   console.log(funcName + 'Invalid formSelElementDict');
      //   throw (new Error('Invalid formSelElementDict'));
      // }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');

      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
