const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name, studyLevel) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var course_cricos_code = []
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;

                            var program_code = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                let temp = selList[0].split(": ");
                                                program_code = temp[1].trim();
                                            }
                                            else {
                                                program_code = "";
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            resJsonData.program_code = program_code;
                            break;
                        }
                        //  if (program_code.length > 0) {

                        //         resJsonData.program_code = program_code;
                        //     } else {
                        //         resJsonData.program_code = "";
                        //     }
                        //     break;
                        // }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: courseUrl


                            };

                            resJsonData.course_outline = outline;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = category_name;
                            break;
                        }
                        case 'application_fee': {
                            resJsonData.application_fee = "Students who apply through the Online Application Centre will attract a $25 application fee. Students who submit a paper-based application (pdf, 372kb) will attract a $50 application fee.";
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}

                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {};
                            const pteDict = {};
                            const ibtDict = {};
                            const pbtDict = {};
                            const caeDict = {};
                            let ieltsNumber = null;
                            let compare = null;

                            // english requirement
                            const regEx = /[+-]?\d+(\.\d+)?/g;
                            let ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                            console.log("ieltsScore -->", ieltsScore);
                            if (courseScrappedData.course_toefl_ielts_score) {
                                console.log("R ieltsScore", ieltsScore);
                                if (ieltsScore && ieltsScore.length > 0) {

                                    compare = ieltsScore;
                                    const matchedStrList = String(ieltsScore).match(regEx);
                                    console.log('course_toefl_ielts_score matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ieltsNumber = Number(matchedStrList[0]);
                                        console.log('course_toefl_ielts_score ieltsNumber = ' + ieltsNumber);
                                    }
                                }
                            }
                            if (ieltsNumber == null) {
                                ieltsScore = "Overall 6.0 (no band less than 6.0)";
                                console.log("R1 ieltsScore", ieltsScore);
                                if (ieltsScore && ieltsScore.length > 0) {

                                    compare = ieltsScore;
                                    const matchedStrList = String(ieltsScore).match(regEx);
                                    console.log('course_toefl_ielts_score matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ieltsNumber = Number(matchedStrList[0]);
                                        console.log('course_toefl_ielts_score ieltsNumber = ' + ieltsNumber);
                                    }
                                }
                            }
                            if (ieltsNumber && ieltsNumber > 0) {
                                console.log("ieltsNumber -->", ieltsNumber);
                                const ieltsMappingDict = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                                var otherLngDict;
                                //special case
                                if (ieltsMappingDict) {
                                    if (ieltsNumber == 5.5) {
                                        if (compare.indexOf("5.0") > -1) {
                                            otherLngDict = ieltsMappingDict["5.5(no band less than 5.0)"];
                                        }
                                        else {
                                            otherLngDict = ieltsMappingDict["5.5"];
                                        }//end special case
                                    } else {
                                        otherLngDict = ieltsMappingDict[ieltsNumber];
                                    }
                                    console.log('course_toefl_ielts_score otherLngDict = ' + JSON.stringify(otherLngDict));
                                    if (otherLngDict) {
                                        const ibtScore = otherLngDict.toefl;
                                        const pteScore = otherLngDict.pte;
                                        const caeScore = otherLngDict.cae;
                                        const pbtScore = otherLngDict.pbt;

                                        if (ieltsScore) {
                                            ieltsDict.name = 'ielts academic';
                                            ieltsDict.description = ieltsScore;
                                            ieltsDict.min = 0;
                                            ieltsDict.require = await utils.giveMeNumber(ieltsScore);
                                            ieltsDict.max = 9;
                                            ieltsDict.R = 0;
                                            ieltsDict.W = 0;
                                            ieltsDict.S = 0;
                                            ieltsDict.L = 0;
                                            ieltsDict.O = 0;
                                            englishList.push(ieltsDict);
                                        }
                                        if (pteScore) {
                                            pteDict.name = 'pte academic';
                                            pteDict.description = pteScore;
                                            pteDict.min = 0;
                                            pteDict.require = await utils.giveMeNumber(pteScore);
                                            pteDict.max = 90;
                                            pteDict.R = 0;
                                            pteDict.W = 0;
                                            pteDict.S = 0;
                                            pteDict.L = 0;
                                            pteDict.O = 0;
                                            englishList.push(pteDict);
                                        }
                                        if (ibtScore) {
                                            ibtDict.name = 'toefl ibt';
                                            ibtDict.description = ibtScore;
                                            ibtDict.min = 0;
                                            ibtDict.require = await utils.giveMeNumber(ibtScore);
                                            ibtDict.max = 120;
                                            ibtDict.R = 0;
                                            ibtDict.W = 0;
                                            ibtDict.S = 0;
                                            ibtDict.L = 0;
                                            ibtDict.O = 0;
                                            englishList.push(ibtDict);
                                        }
                                        if (pbtScore) {
                                            pbtDict.name = 'toefl pbt';
                                            pbtDict.description = pbtScore;
                                            pbtDict.min = 310;
                                            pbtDict.require = await utils.giveMeNumber(pbtScore);
                                            pbtDict.max = 677;
                                            pbtDict.R = 0;
                                            pbtDict.W = 0;
                                            pbtDict.S = 0;
                                            pbtDict.L = 0;
                                            pbtDict.O = 0;
                                            englishList.push(pbtDict);
                                        }
                                        if (caeScore) {
                                            caeDict.name = 'cae';
                                            caeDict.description = caeScore;
                                            caeDict.min = 80;
                                            caeDict.require = await utils.giveMeNumber(caeScore);
                                            caeDict.max = 230;
                                            caeDict.R = 0;
                                            caeDict.W = 0;
                                            caeDict.S = 0;
                                            caeDict.L = 0;
                                            caeDict.O = 0;
                                            englishList.push(caeDict);
                                        }
                                    }
                                }
                            }

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            } else {
                                courseAdminReq.english = [];
                            }

                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq];
                                } 
                            }
                            else {
                                courseAdminReq.academic = [];
                            }
                            // // if courseAdminReq has any valid value then only assign it
                            // if (courseAdminReq.english || courseAdminReq.academic) {
                            //     resJsonData.course_admission_requirement = courseAdminReq;
                            // }

                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.course_admission_english_more_details;
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            } // if (Array.isArray(courseKeyVal))
                            // if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                            //     resJsonData.course_admission_requirement.english_more_details = resEnglishReqMoreDetailsJson;
                            // } else if (resEnglishReqMoreDetailsJson) {
                            //     resJsonData.course_admission_requirement = {};
                            //     resJsonData.course_admission_requirement.english_more_details = resEnglishReqMoreDetailsJson;
                            // }


                            //DYNAMIC PROGRESS BAR //CAN BE COPIED//PBT NOT INCLUDED
                            //progress bar start
                            let penglishList = [];
                            let pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            // extract exact number from string              
                            let matchedStrList_ielts, matchedStrList_pte, matchedStrList_ibt, matchedStrList_pbt, matchedStrList_cae;
                            let ieltsNo, pteNo, ibtNo, caeNo, pbtNo;
                            if (ieltsDict) {
                                matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                            }
                            if (pteDict) {
                                matchedStrList_pte = String(pteDict.description).match(regEx);
                            }
                            if (ibtDict) {
                                matchedStrList_ibt = String(ibtDict.description).match(regEx);
                            }
                            if (pbtDict) {
                                matchedStrList_pbt = String(pbtDict.description).match(regEx);
                            }
                            if (caeDict) {
                                matchedStrList_cae = String(caeDict.description).match(regEx);
                            }

                            if (matchedStrList_ielts && matchedStrList_ielts.length > 0) {
                                ieltsNo = Number(matchedStrList_ielts[0]);
                            }
                            if (matchedStrList_pte && matchedStrList_pte.length > 0) {
                                pteNo = Number(matchedStrList_pte[0]);
                            }
                            if (matchedStrList_ibt && matchedStrList_ibt.length > 0) {
                                ibtNo = Number(matchedStrList_ibt[0]);
                            }
                            if (matchedStrList_pbt && matchedStrList_pbt.length > 0) {
                                pbtNo = Number(matchedStrList_pbt[0]);
                            }
                            if (matchedStrList_cae && matchedStrList_cae.length > 0) {
                                caeNo = Number(matchedStrList_cae[0]);
                            }
                            if (ieltsNumber && ieltsNumber > 0) {
                                if (ieltsNo) {
                                    pieltsDict.name = 'ielts academic';
                                    pieltsDict.value = {};
                                    pieltsDict.value.min = 0;
                                    pieltsDict.value.require = ieltsNo;
                                    pieltsDict.value.max = 9;
                                    penglishList.push(pieltsDict);
                                }
                                if (ibtNo) {
                                    pibtDict.name = 'toefl ibt';
                                    pibtDict.value = {};
                                    pibtDict.value.min = 0;
                                    pibtDict.value.require = ibtNo;
                                    pibtDict.value.max = 120;
                                    penglishList.push(pibtDict);
                                }
                                if (pbtNo) {
                                    ppbtDict.name = 'toefl pbt';
                                    ppbtDict.value = {};
                                    ppbtDict.value.min = 0;
                                    ppbtDict.value.require = pbtNo;
                                    ppbtDict.value.max = 120;
                                    penglishList.push(ppbtDict);
                                }
                                if (pteNo) {
                                    ppteDict.name = 'pte academic';
                                    ppteDict.value = {};
                                    ppteDict.value.min = 0;
                                    ppteDict.value.require = pteNo;
                                    ppteDict.value.max = 90;
                                    penglishList.push(ppteDict);
                                }
                                if (caeNo) {
                                    pcaeDict.name = 'cae';
                                    pcaeDict.value = {};
                                    pcaeDict.value.min = 80;
                                    pcaeDict.value.require = caeNo;
                                    pcaeDict.value.max = 230;
                                    penglishList.push(pcaeDict);
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            resJsonData.course_admission_requirement = {};
                            if (courseAdminReq.english && courseAdminReq.english.length > 0) {
                                resJsonData.course_admission_requirement.english = courseAdminReq.english;
                            }
                            if (courseAdminReq.academic && courseAdminReq.academic.length > 0) {
                                resJsonData.course_admission_requirement.academic = courseAdminReq.academic;
                            }
                            // add english requirement 'english_more_details' link
                            const entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            const academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            const english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);

                            if (entry_requirements_url && entry_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            }
                            if (academic_requirements_url && academic_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;
                            }
                            if (english_requirements_url && english_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                            }

                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                console.log("URL -->1 ", courseUrl);
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            //NO CUSTOMIZATION BUT ONLY FULL TIME
                            var courseDurationList = "";
                            var courseDurationDisplayList = [];
                            var durationFullTime = {};

                            let durationFullTimeDisplay = null;

                            console.log("*************start formating years*************************");
                            console.log("R full-time", JSON.stringify(courseScrappedData.course_duration_full_time[0].elements[0]));



                            if (courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0] && courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0].length > 0) {
                                let temp1 = await format_functions.validate_course_duration_full_time(courseScrappedData.course_duration_full_time[0].elements[0].selectors[0][0]);
                                var full_year_formated_data = [{
                                    "unit": temp1[0].unit,
                                    "duration": temp1[0].duration_full_time,
                                    "display": "Full-Time"
                                }];
                                //full_year_formated_data[0].display = "Full-Time";

                                ///course duration
                                const not_full_year_formated_data = await format_functions.not_formated_course_duration(courseScrappedData.course_duration_full_time);
                                durationFullTime.duration_full_time = not_full_year_formated_data;
                                courseDurationList = durationFullTime;
                                ///END course duration

                                ///course duration display
                                durationFullTimeDisplay = {};
                                durationFullTimeDisplay = full_year_formated_data;
                                courseDurationDisplayList.push(durationFullTimeDisplay);

                                ///END course duration display

                                console.log('***************END formating Full Time years**************************')
                            }

                            if (courseDurationList && courseDurationList.length > 0) {

                                resJsonData.course_duration = courseDurationList;
                            }
                            else if (String(resJsonData.course_title).toLowerCase().indexOf("weeks") > -1) {

                                let temp = resJsonData.course_title;
                                let week = temp.split("(");

                                var weekcheck = [];
                                for (let s = 0; s < week.length; s++) {
                                    console.log('weeks check :' + String(week[s]).toLowerCase());
                                    if (String(week[s]).toLowerCase().includes("weeks)")) {

                                        weekcheck.push(week[s]);
                                    }
                                }


                                for (let i = 0; i < weekcheck.length; i++) {

                                    if (String(weekcheck).toLowerCase().includes("weeks)")) {

                                        console.log("Duration MATCHED", weekcheck[i]);
                                        durationFullTime = String(weekcheck[i]);
                                        console.log('durationFullTime.duration_full_time :' + durationFullTime.duration_full_time);
                                        durationFullTime = String(weekcheck[i]).replace("(", "").replace(")", "").trim()
                                        console.log('testettetetet :' + durationFullTime)
                                        courseDurationList = durationFullTime;
                                        resJsonData.course_duration = courseDurationList;
                                        console.log("resJsonData.course_duration", resJsonData.course_duration);
                                        let durationVal = String(weekcheck[i]).replace("(", "").replace(")", "").trim();
                                        console.log('duration value :' + durationVal);

                                        resJsonData.course_duration_display = [
                                            {
                                                unit: "weeks",
                                                duration: String(parseInt(durationVal)),
                                                display: "Full-Time"
                                            }
                                        ]
                                        let durationFormated = await format_functions.getfilterduration(resJsonData.course_duration_display, "MOM", "");
                                        resJsonData.course_duration_display = durationFormated;

                                        var isfulltime = false, isparttime = false;
                                        durationFormated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                    break;
                                }
                            }
                            else {

                                const crik = course_cricos_code;


                                let cDuration = await format_functions.getDuration(crik);

                                resJsonData.course_duration = cDuration + " weeks";
                            }


                            if (courseDurationDisplayList && courseDurationDisplayList.length > 0) {

                                let formatIntake = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                resJsonData.course_duration_display = formatIntake;
                                var isfulltime = false, isparttime = false;
                                courseDurationDisplayList.forEach(element => {
                                    if (element.display == "Full-Time") {
                                        isfulltime = true;
                                    }
                                    if (element.display == "Part-Time") {
                                        isparttime = true;
                                    }
                                });
                                resJsonData.isfulltime = isfulltime;
                                resJsonData.isparttime = isparttime;

                            }

                            console.log("*************END formating years*************************");
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode': {
                           
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'page_url':
                        case 'course_academic_requirement':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_duration_part_time': {
                            console.log(funcName + 'No need to process key = ' + key + ' as its alerady processed in some other relavant case...');
                            // No need to process anything, its already processed in other case.
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            let newcampus = [];

                            for (let campus of resJsonData.course_campus_location) {
                                if (campus.name.toLowerCase().indexOf('online') > -1 || campus.name.toLowerCase().indexOf('line learning') > -1) {

                                } else {
                                    newcampus.push(campus);
                                }
                            }

                            resJsonData.course_campus_location = newcampus;
                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            // feesDict.international_student = configs.propValueNotAvaialble;
                            // feesDict.fee_duration_years = configs.propValueNotAvaialble;
                            // feesDict.currency = configs.propValueNotAvaialble;
                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);


                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                                const arrval = String(feesVal1).split('.');

                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            console.log('*************fees one*********************');
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",

                                                description: "",

                                            });
                                        } else {
                                            console.log('*************fees one*********************');
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: 1,
                                                unit: "Year",

                                                description: "",

                                            });
                                        }
                                    } else {
                                        feesDict.international_student = ({
                                            amount: 0,
                                            duration: 1,
                                            unit: "Year",

                                            description: "",

                                        });
                                    }
                                } else {
                                    feesDict.international_student = ({
                                        amount: 0,
                                        duration: 1,
                                        unit: "Year",

                                        description: "",

                                    });
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)

                            // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                            var cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {

                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    console.log("cricoseStr[i]", cricoseStr);
                                    console.log("cricoseStr[i].toString()", cricoseStr.toString());
                                    console.log("cricoseStr[i].toString().split(':')", cricoseStr.toString().split(":"));
                                    let temp = cricoseStr.toString().split(":");
                                    temp = temp[2].trim();
                                    console.log("TEMP", temp);
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            console.log("temp=", temp, "codeKey=", codeKey, "temp.indexOf(codeKey)>-1", temp.indexOf(codeKey) > -1);
                                            if (temp.indexOf(codeKey) > -1) {
                                                console.log("Entered!!");
                                                const feesDictVal = fixedFeesDict[temp];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = ({
                                                            amount: Number(inetStudentFees),
                                                            duration: 1,
                                                            unit: "Year",

                                                            description: "",

                                                        });
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if

                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesIntStudent.length > 0) { // extract only digits
                                    console.log("international_student" + feesIntStudent)
                                    // feesDict.international_student_all_fees = [];

                                }
                                if (feesDict) {
                                    console.log("resJsonData.course_campus_location --> ", resJsonData.course_campus_location);
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",

                                    description: "",

                                });
                                //   feesDict.international_student_all_fees = [];

                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;
                        }

                        case 'course_campus_location': { // Location Launceston                  
                            console.log('course_campus_location matched case: ' + key);
                            let concatnatedSelectorsStr = null;
                            let concatnatedSelStr = [];
                            const rootElementDictList = courseScrappedData[key];
                            var myintake_data = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                            const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('course_location');
                            const fixedFeesDict1 =  String(fixedFeesDict).split(',');

                            console.log("fixedFeesDict-0000000-0/-->",fixedFeesDict1);

                            // console.log("myintake_data--->>>>",myintake_data);
                            try {
                                // let location_display = resJsonData.course_campus_location;
                                // if (!location_display) {
                                //     location_display = [];
                                // }
                                let cricos_display = "";
                                var result = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                                var cricosStr = result.split(": ");
                                cricosStr = cricosStr[1];
                                console.log("cricosStr-0-0-0-0-0-0-0-0-0-0-0>", cricosStr);
                                //loops
                                if (cricosStr.indexOf(",") > -1) {
                                    //set cricos code
                                    cricosStr = cricosStr.split(",");
                                    // console.log("R cricosStr", cricosStr);
                                    cricos_display = cricosStr[0].split("(");
                                    cricos_display = cricos_display[0].trim();
                                    course_cricos_code = [];
                                    course_cricos_code.push(cricos_display);
                                    //add to location array
                                    for (let text of cricosStr) {
                                        if (text.indexOf("(") > -1) {
                                            let temp = text.split("(");
                                            let code = temp[0].trim();
                                            let location = temp[1].split(")");
                                            location = location[0];
                                            //  console.log("R location", location);
                                            let newarr = [];
                                            // for (let campus of location_display) {
                                            //     newarr.push(campus.name);
                                            // }
                                            // if (newarr.includes(location) == false) {
                                            //     location_display.push({
                                            //         name: location,

                                            //     });
                                            // }


                                        }
                                    }
                                    //  console.log("R location_display", location_display);
                                    // resJsonData.course_campus_location = location_display;
                                }
                                else if (cricosStr.indexOf("(") > -1) {
                                    let temp = cricosStr.split("(");
                                    let code = temp[0].trim();
                                    let location = temp[1].split(")");
                                    location = location[0];
                                    // console.log("R code", code);
                                    // console.log("R location", location);
                                    let newarr = [];
                                    // for (let campus of location_display) {
                                    //     newarr.push(campus.name);
                                    // }
                                    // if (newarr.includes(location) == false) {
                                    //     location_display.push({
                                    //         name: location,

                                    //     });
                                    // }
                                    let tempCode = [];
                                    //tempCode.push(location + "-" + code);
                                    tempCode.push(code);
                                    course_cricos_code = tempCode;
                                    // resJsonData.course_campus_location = location_display;
                                }
                                else {
                                    course_cricos_code = [];
                                    //   console.log("ELSEEEEEE comdition -->", cricosStr);
                                    if (cricosStr) {
                                        course_cricos_code.push(cricosStr.trim());
                                    }

                                    //  resJsonData.course_cricos_code.push(cricosStr);
                                }
                                // console.log("CRICOS resJsonData.course_campus_location", resJsonData.course_campus_location);
                                console.log("resJsonData.course_cricos_code000000--->", course_cricos_code);
                                // let newcampus = [];

                                // for (let campus of resJsonData.course_campus_location) {
                                //     if (campus.name.toLowerCase().indexOf('online') > -1 || campus.name.toLowerCase().indexOf('line learning') > -1) {

                                //     } else {
                                //         newcampus.push(campus);
                                //     }
                                // }

                                //  resJsonData.course_campus_location = newcampus;
                                //global.cricos_code = resJsonData.course_cricos_code[0];
                                //  var locations = resJsonData.course_campus_location;

                                // course_cricos_code = mycodes;
                                // console.log("CRICOS resJsonData.course_cricos_code", course_cricos_code);
                                //console.log("CRICOS after resJsonData.course_campus_location", resJsonData.course_campus_location);
                            } catch (err) {
                                console.log("Problem in course_cricos_code:", err);
                                throw new Error("Cricos code undefined");
                            }
                            console.log('course_campus_location key-val is ' + JSON.stringify(rootElementDictList));

                            for (const rootEleDict of rootElementDictList) {
                                console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                // let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        if (selList && selList.length > 0) {
                                            console.log('course_campus_location selList = ' + JSON.stringify(selList));

                                            for (let selItem of selList) {
                                                selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ' ').split(",");
                                                for (let sel of selItem) {
                                                    if (sel.indexOf("-") > -1) {
                                                        sel = sel.split("-")[1];
                                                        sel = sel.trim();
                                                    }
                                                    else {
                                                        sel = sel.trim();
                                                    }
                                                    // concatnatedSelStr.push(sel);
                                                    concatnatedSelStr.push({
                                                        "name": sel,
                                                        "code": course_cricos_code.toString()
                                                    });
                                                }

                                            } // selList
                                            console.log('course_campus_location concatnatedSelStr = ' + concatnatedSelStr);
                                            if (concatnatedSelStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            }
                                        }
                                    } // selectorsList                  
                                } // elementsList                
                            } // rootElementDictList 

                            // add only if it has valid value              
                            if (concatnatedSelectorsStr && concatnatedSelectorsStr.length > 0) {
                                resJsonData.course_campus_location = concatnatedSelectorsStr;
                            }
                            else {
                                console.log("crickkkos-->", course_cricos_code);
                                var locate = course_cricos_code;
                                var campuss = []
                                const campp = await format_functions.getLocation(locate)
                                for (let ca of campp) {
                                    var campusval = String(ca).split('- ')[1].trim();
                                    //var campusval1 = campusval.split('Federation University')[1].trim()
                                    campuss.push(campusval);
                                    console.log("camphjjhh-->", campuss)
                                    concatnatedSelStr.push({
                                        "name": campusval,
                                        "code": course_cricos_code.toString()
                                    });
                                }
                                
                                resJsonData.course_campus_location = concatnatedSelStr


                                // resJsonData.course_study_level = studylevel
                                // if (!resJsonData.course_study_level) {
                                //   //throw  new error("Study_level not found")
                                //   throw (new Error('Study_level not found'));
                                // }

                                // console.log("capmus-->",campLocationText);
                            }
                            console.log("resJsonData.course_campus_location567-->", resJsonData.course_campus_location);
                            let newcampus = [];

                            for (let campus of resJsonData.course_campus_location) {
                                if (campus.name.toLowerCase().indexOf('online') > -1 || campus.name.toLowerCase().indexOf('line learning') > -1) {

                                } else {
                                    newcampus.push(campus);
                                }
                            }

                            resJsonData.course_campus_location = newcampus;

                            break;
                        }

                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            //CUSTOMIZED CODE DO NOT COPY
                            const courseIntakeDisplay = [];
                            // existing intake value
                            var intakeUrl = [];


                            intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                            const courseIntakeStrElem = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);

                            const courseIntakeStrFile = await utils.getValueFromHardCodedJsonFile("course_intake");

                            let newcampus = [];

                            for (let campus of resJsonData.course_campus_location) {
                                if (campus.name.toLowerCase().indexOf('online') > -1 || campus.name.toLowerCase().indexOf('line learning') > -1) {

                                } else {
                                    newcampus.push(campus);
                                }
                            }

                            resJsonData.course_campus_location = newcampus;
                            const locationArray = resJsonData.course_campus_location;

                            console.log('************************Start course_intake******************************');

                            let specialCase = false;
                            if (courseIntakeStrElem && courseIntakeStrElem.length > 0) {
                                resJsonData.course_intake = courseIntakeStrElem;

                                const intakeStrList = String(courseIntakeStrElem).split('\n');

                                if (intakeStrList && intakeStrList.length > 0) {
                                    for (var part of intakeStrList) {
                                        for (let format of courseIntakeStrFile) {

                                            let tempPart = part.toString().toLowerCase().trim();
                                            if (format == "anytime") {
                                                if (tempPart.indexOf(format) > -1) {
                                                    courseIntakeDisplay.push(part);
                                                }
                                            }
                                            else if (tempPart == "Semesters 1 & 2, summer semester for international students (brisbane campus)") {
                                                // specialCase = true;
                                                let temp = courseIntakeStrFile[0].split("- ")[1];
                                                let temp2 = courseIntakeStrFile[1].split("- ")[1];
                                                courseIntakeDisplay.push(temp);
                                                courseIntakeDisplay.push(temp2);
                                                break;
                                            }
                                            else {
                                                let tempFormat = format.split("- ");
                                                let tempDate = tempFormat[1];
                                                tempFormat = tempFormat[0].trim();
                                                if (tempPart.indexOf(tempFormat) > -1) {

                                                    courseIntakeDisplay.push(tempDate);
                                                }
                                            }




                                        } // for
                                    }
                                }
                                console.log('course_intake courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));


                            }
                            if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
                                if (specialCase == true) {
                                    if (locationArray.includes("Brisbane") == false) {
                                        locationArray.push("Brisbane");
                                    }
                                    resJsonData.course_intake = {};
                                    resJsonData.course_intake.intake = [];
                                    for (let location of locationArray) {
                                        if (location == "Brisbane") {
                                            let temp = courseIntakeStrFile[2].split("- ")[1];
                                            resJsonData.course_intake.intake.push({
                                                "name": "Brisbane",
                                                "value": [temp]
                                            });
                                        }
                                        else {
                                            resJsonData.course_intake.intake.push({
                                                "name": location.name,
                                                "value": courseIntakeDisplay
                                            });
                                        }
                                    }
                                    let formatIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "MOM", "");
                                    console.log("formatIntakes--->", formatIntake);
                                    course_intake.intake = formatIntake;
                                    resJsonData.course_intake.more_details = intakeUrl;

                                }
                                else {
                                    resJsonData.course_intake = {};
                                    resJsonData.course_intake.intake = [];
                                    for (let location of locationArray) {
                                        resJsonData.course_intake.intake.push({
                                            "name": location.name,
                                            "value": courseIntakeDisplay
                                        });
                                    }
                                    let formatIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "MOM", "");
                                    resJsonData.course_intake.intake = formatIntake;

                                    for (let intakeval of resJsonData.course_intake.intake) {
                                        console.log("intakeval--->", intakeval);

                                    }
                                    resJsonData.course_intake.more_details = intakeUrl;
                                }
                            }
                            else {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];

                                for (let location of locationArray) {

                                    resJsonData.course_intake.intake.push({
                                        "name": location.name,
                                        "value": []
                                    });
                                }
                               // throw new Error("Intake problem");
                                let formatIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "MOM", "");
                                resJsonData.course_intake.intake = formatIntake;
                                resJsonData.course_intake.more_details = intakeUrl;
                            }

                            break;
                        }


                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                        case 'course_accomodation_cost': {
                            const courseKeyVal = courseScrappedData.course_accomodation_cost;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_accomodation_cost = resAccomodationCostJson;
                            }
                            break;
                        }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_country = resAccomodationCostJson;
                            }
                            break;

                        }
                        case 'course_overview': {
                            var result = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            if (result.length > 0) {
                                resJsonData.course_overview = result;
                            } else {
                                resJsonData.course_overview = "";
                            }


                            break;
                        }
                        case 'course_career_outcome': {
                            console.log('course_career_outcome matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log('course_career_outcome key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedSelectorsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log('course_career_outcome rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                // let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log('course_career_outcome eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    for (const selList of selectorsList) {
                                        if (selList && selList.length > 0) {
                                            console.log('course_career_outcome selList = ' + JSON.stringify(selList));
                                            let concatnatedSelStr = [];
                                            for (const selItem of selList) {
                                                concatnatedSelStr.push(selItem);
                                            } // selList
                                            console.log('course_career_outcome concatnatedSelStr = ' + concatnatedSelStr);
                                            if (concatnatedSelStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            }
                                        }
                                        else {
                                            resJsonData.course_career_outcome = [];
                                        }
                                    } // selectorsList                  
                                } // elementsList                
                            } // rootElementDictList 

                            // add only if it has valid value              
                            if (concatnatedSelectorsStr && concatnatedSelectorsStr.length > 0) {
                                resJsonData.course_career_outcome = concatnatedSelectorsStr;
                            } else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }
                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            var progg = await Course.extractValueFromScrappedElement(courseScrappedData.program_code)
                            var progg1 = progg.split(':')[1].trim();
                            console.log("splitStr@@@2" + title);
                            var ctitle = format_functions.titleCase(title).trim();
                            console.log("ctitle@@@", ctitle.trim());
                            resJsonData.course_title = ctitle
                            var course_title1 = resJsonData.course_title + "(" + progg1 + ")";
                            break;
                        }

                        // case 'course_study_level': {
                        //     resJsonData.course_study_level = studyLevel;
                        //     break;
                        // }
                        case 'course_study_level':
                            {
                                const cTitle = course_cricos_code;
                                console.log("studylevelllll----->", cTitle)

                                let cStudyLevel = await format_functions.getMyStudyLevel(cTitle);
                                console.log("course_study_level--------", cStudyLevel);
                                resJsonData.course_study_level = cStudyLevel
                            }

                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            ////start genrating new file location wise

            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = course_title1.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        //   NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                // resJsonData.basecourseid = location_wise_data.course_id;
                // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                //     if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                //         resJsonData.course_cricos_code[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_cricos_code[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                //     if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_intake.intake[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_intake.intake[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                //     if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                //     if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_campus_location[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_campus_location[i].iscurrent = false;
                //     }
                // }
                //resJsonData.course_cricos_code = location_wise_data.cricos_code;
                // resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
                //  resJsonData.current_course_campus_location = location_wise_data.course_campus_location;
                //resJsonData.current_course_intake = location_wise_data.course_intake;
                // resJsonData.course_intake_display = location_wise_data.course_intake;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }

            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;
            ///end grnrating new file location wise

        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({
                headless: true
            });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href;
            const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //   //custom call
            //   const actionname = [];
            //   const actionelemselector = [];
            //   const rootSelector = ["#study > div > p[style='margin-left: 40px;']", 
            //   "#study > div > p[style='margin-left: 80px;']",
            //   "#study > div > p[style='margin-left:72.0pt;']",
            //   "#study > div > p[style='padding-left: 60px;']",
            //   "#study > div > p[style='margin-left: 72pt']",
            //   "#study > div > p[style='padding-left: 40px']",
            //   "#study > div > p[style='margin-left: 120px;']",
            //   "#study > div > p[style='padding-left: 20px']"
            // ];
            //   const seperator = "<br>";
            //   const remover = ["list:","courses:","specialisations:","units","University:","of:","graduate courses"];
            //   const limit = 20;
            //   const course_outline_data = s.findScrapeRemoveSeperatePerform_fun(courseDict.href, rootSelector, remover, seperator, actionname, actionelemselector, limit)
            //   console.log("Couser_Outline_Data without prom:" + course_outline_data);
            //   var promise1 = Promise.resolve(course_outline_data);
            //   var data = [];
            //   await promise1.then(function (value) {
            //     console.log("Couser_Outline_Data:" + value);
            //     data = value;
            //   });

            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            // courseScrappedData.course_outline = data;

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category, courseDict.studyLevel);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails

    // static titleCase(str) {
    //     console.log("stryyyyyy-->",str);
    //     var splitStr = str.split(' ');
    //     for (var i = 0; i < splitStr.length; i++) {
    //         // You do not need to check if i is larger than splitStr length, as your for does that for you
    //         // Assign it back to the array
    //         splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    //     }
    //     // Directly return the joined string
    //     return splitStr.join(' ');
    // }

} // class
module.exports = {
    ScrapeCourse
};