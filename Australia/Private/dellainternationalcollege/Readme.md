**The University Of Queensland: Total Courses Details As On 27 Mar 2019**
* Total Courses = 388
* Courses without CRICOS code = 10
* Total available courses = 
* Repeated = 

**Notes**
* fees is for two programs is read from hardcode file.
* entry requirements url is different for undergrad,postgrad,research and honours courses.
* ielts score is scrapped but the other scores are mapped from hardcode.Even though values are given because score of ielts,toefl,pte is given in one paragraph.
* the ielts mapping written in hardcode file has been taken from https://my.uq.edu.au/files/5696/PPL34014d3-Table3.pdf
* entry requirement url is different for honours, undergrad, postgrad, research and doctor of medicine courses.
* extra selectors added for the same in course details selectors.json .
* 2 special case for ielts for which static value of 6.5 has been specified.
* {
        "href": "https://future-students.uq.edu.au/study/program/Bachelors-of-Commerce-Arts-2443",
        "innerText": "Commerce/Arts",
        "reason": "TypeError: Cannot read property '0' of undefined",
        "index": 278       
    },
    {
        "href": "https://future-students.uq.edu.au/study/program/Bachelors-of-Commerce-Science-2445",
        "innerText": "Commerce/Science",
        "reason": "TypeError: Cannot read property '0' of undefined",
        "index": 282        
*   }


**2019 & conditions**

