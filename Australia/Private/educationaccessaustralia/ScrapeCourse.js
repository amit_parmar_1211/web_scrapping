const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const puppeteer = require('puppeteer');

const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            //var Course_Name;


            //     const intakefees = JSON.parse(fs.readFileSync("./output/educationaccessaustralia_fees_caregary.json"));
            //     console.log("intakefees-------->>>>", intakefees);
            //     let tmpvar = intakefees.filter(val => {
            //         return val.Duration.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase() == course_name.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase()
            //     });
            //     console.log("tmpvar------->>>>", tmpvar);

            //     console.log("programcode------->>>>", tmpvar[0].Course_Duration);
            //     const Course_Duration = tmpvar[0].Duration
            //     console.log("Course_Duration------->>>>", Course_Duration);
            //    resJsonData.course_title = Course_Duration;



            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = course_category;
                            // As of not we do not have way to get this field value
                            break;
                        }
                        case 'course_title': {
                            resJsonData.course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("resJsonData.course_title3333", resJsonData.course_title);

                        }
                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }



                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };

                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

                            let resScholarshipJson = null;


                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }




                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }

                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)

                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;


                            break;
                        }
                        case 'application_fee':
                            {
                                let applicationfee = JSON.parse(fs.readFileSync(appConfigs.Locations));
                                resJsonData.application_fee = ""
                                break;
                            }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':

                        case 'course_campus_location': { // Location Launceston
                            let newCampusses = [];
                            let campLocationText;
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            let course_campus_location = [];
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            let CRICOS = cTitle.split('CRICOS CODE')[1];
                            console.log("resJsonData.sssss------>>>>>>", CRICOS);
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            course_campus_location.push(selList);
                                            console.log("course_campus_location----->>>>", course_campus_location);
                                            // }
                                        }
                                    }
                                }
                            }

                            if (course_campus_location && course_campus_location.length > 0) {
                                var campLocationValTrimmed = String(course_campus_location).trim();
                                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);

                                if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    // resJsonData.course_campus_location = await utils.giveMeArray(campLocationValTrimmed, ',');
                                    //   var campuss = await utils.giveMeArray(campLocationValTrimmed, ',');

                                    //let campuses= ['West Melbourne','North Melbourne','Melbourne']

                                    let campuses = ['West Melbourne', 'North Melbourne']
                                    campuses.forEach(element => {
                                        if (campLocationValTrimmed.includes(element)) {
                                            newCampusses.push(element);
                                            console.log("YESSSS location is there")

                                        }

                                    });
                                    console.log("newCampusses --> ", newCampusses);
                                    var campusedata = [];
                                    newCampusses.forEach(element => {
                                        campusedata.push({
                                            "name": element.trim(),
                                            "code": CRICOS.trim()

                                        })
                                    });

                                    resJsonData.course_campus_location = campusedata;
                                    // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                    var study_mode = [];
                                    if (campLocationValTrimmed.trim().length > 0) {
                                        study_mode.push('On campus');
                                    }
                                    resJsonData.course_study_mode = 'On campus';//.join(',');
                                    console.log("## FInal string-->" + campLocationValTrimmed);
                                }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_study_mode': { // Location Launceston
                            //resJsonData.course_study_mode = coursestudymodeText;
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {};
                            const pteDict = {};
                            let ielts, pte;
                            var ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_req);
                            if (ielts_req.includes("PTE")) {
                                ielts = ielts_req.split("PTE")[0];

                                console.log(" ", ielts)
                            }
                            console.log("ielts_req", ielts_req);

                            if (ielts) {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ielts;
                                ieltsDict.min = 0;
                                ieltsDict.require = await utils.giveMeNumber(ielts.replace(/ /g, ' '));;
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);
                            }
                            if (ielts_req.includes("PTE")) {
                                pte = ielts_req.split("PTE")[1];

                                console.log("##pte_req-->", pte)
                            }
                            // console.log("ielts_req", ielts_req);

                            if (pte) {
                                pteDict.name = 'pte academic';
                                pteDict.description = pte;
                                pteDict.min = 0;
                                pteDict.require = await utils.giveMeNumber(pte.replace(/ /g, ' '));;
                                pteDict.max = 90;
                                pteDict.R = 0;
                                pteDict.W = 0;
                                pteDict.S = 0;
                                pteDict.L = 0;
                                pteDict.O = 0;
                                englishList.push(pteDict);
                            }
                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }

                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];
                            courseAdminReq.english_requirements_url = "";
                            courseAdminReq.academic_requirements_url = "";
                            courseAdminReq.entry_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {
                                international_student: {},


                            };

                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);


                            const fee = JSON.parse(fs.readFileSync(appConfigs.fees));
                            var durations = [];
                            const courseKeyVal = resJsonData.program_code;
                            console.log("pc.sssss------>>>>>>", JSON.stringify(courseKeyVal));
                            for (var durat of fee) {
                                console.log("feee@@", durat.Prog_code)
                                // console.log("durat_@@@@",splitCricos)
                                if (courseKeyVal.includes(durat.Prog_code) == true) {
                                    durations.push(durat.Total_Fee);
                                    console.log("feee------->", JSON.stringify(durations));
                                    var feeeeee = durations;

                                }
                            }

                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feeeeee && feeeeee.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feeeeee).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const arrval = String(feesWithDollorTrimmed).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,

                                                description: String(feeeeee)

                                            });

                                        }
                                        if (feesNumber == "0") {
                                            console.log("FEesNumber = 0");
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,

                                                description: String(feeeeee)
                                            });
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: Number(demoarray.duration),
                                    unit: demoarray.unit,

                                    description: ""

                                });
                            }
                            var fee_desc_more = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            let courseKeyVal_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_more);
                            console.log("courseKeyVal_more@@1", courseKeyVal_more);

                            fee_desc_more.push(courseKeyVal_more)
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    // feesDict.international_student_all_fees = fee_desc_more
                                    // feesDict.international_student_all_fees = [];
                                    console.log("morefees@@2", fee_desc_more);

                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }

                        case 'program_code': {
                            resJsonData.program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            console.log("resJsonData.Program", resJsonData.program_code);
                            const pro = String(resJsonData.program_code);

                            if (pro.includes('Certificate')) {
                                console.log("pro@@@@@", pro);
                                var splitpro = pro.split("Certificate")[0].trim();
                                console.log("splitpro@@@@@", splitpro);

                                //feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                            }
                            if (pro.includes('Diploma')) {
                                console.log("pro@@@@@", pro);
                                var splitpro = pro.split("Diploma")[0].trim();
                                console.log("splitpro@@@@@", splitpro);

                                //feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                            }
                            if (pro.includes('Advanced')) {
                                console.log("pro@@@@@", pro);
                                var splitpro = pro.split("Advanced")[0].trim();
                                console.log("splitpro@@@@@", splitpro);

                                //feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                            }
                            resJsonData.program_code = splitpro;


                        }
                        case 'course_duration_full_time': {
                            const duration = JSON.parse(fs.readFileSync("./output/educationaccessaustralia_fees_caregary.json"));
                            // const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            var durations = [];
                            const courseKeyVal = resJsonData.program_code;


                            console.log("pc.sssss------>>>>>>", JSON.stringify(courseKeyVal));

                            for (var durat of duration) {
                                console.log("durat_@@", durat.Prog_code)
                                // console.log("durat_@@@@",splitCricos)
                                if (courseKeyVal.includes(durat.Prog_code) == true) {
                                    durations.push(durat.Duration + " week");
                                    console.log("duration#####", JSON.stringify(durations));


                                }
                            }
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];

                            if (durations && durations.length > 0) {
                                const resFulltime = durations[0];
                                console.log("resFull" + JSON.stringify(resFulltime));
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime);
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("okkkkk", demoarray);

                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("fullTimeText" + JSON.stringify(filtered_duration_formated))
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {

                                        resJsonData.course_duration_display = filtered_duration_formated;

                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }

                                }
                            }
                            break;
                        }
                        case 'course_intake': {
                            var courseIntake = [];
                            const courseIntakeStr = await utils.getValueFromHardCodedJsonFile('course_intake');
                            console.log("courseIntakeStr@@@@@", courseIntakeStr)
                            for (let temp of courseIntakeStr) {
                                temp = temp.trim();
                                courseIntake.push(temp);
                            }
                            var locationArray = resJsonData.course_campus_location;
                            console.log("locationArray", locationArray);

                            if (courseIntake && courseIntake.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let location of locationArray) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location.name,
                                        "value": courseIntake

                                    });
                                    console.log("resJsonData.course_intake.intake", resJsonData.course_intake.intake);

                                }
                                let formatIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");
                                var intakeUrl = [];
                                console.log("formatIntake@@@@@", formatIntake);

                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                resJsonData.course_intake.more_details = intakeUrl + "";
                                resJsonData.course_intake.intake = formatIntake;
                                let valmonths11 = "";


                            }
                            break;
                        }


                        case 'course_scholarship': {
                            const courseKeyVal = courseScrappedData.course_scholarship;
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                resJsonData.course_scholarship = resScholarshipJson;
                            }
                            break;
                        }
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                        case 'course_accomodation_cost': {
                            const courseKeyVal = courseScrappedData.course_accomodation_cost;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_accomodation_cost = resAccomodationCostJson;
                            }
                            break;
                        }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            let course_overview1 = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList;
                                                console.log("course_overview---->", course_overview);
                                            }
                                        }
                                    }
                                }
                            }
                            course_overview1 = course_overview
                            console.log("course_overvieww---->", course_overview1);
                            if (course_overview) {
                                resJsonData.course_overview = String(course_overview);
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                console.log("Outcome is--->" + JSON.stringify(selList))
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : "";
                            // }
                            break;
                        }
                        case 'course_study_level': {
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);

                            let splitCricos = cTitle.split('CRICOS CODE')[1];
                            console.log("resJsonData.sssss------>>>>>>", splitCricos);

                            const study_val = await format_functions.getMyStudyLevel(splitCricos.trim());
                            resJsonData.course_study_level = study_val;
                            console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;

            for (let location of locations) {
                //   NEWJSONSTRUCT.course_id = resJsonData.course_title;
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();

                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }


                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;

                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
