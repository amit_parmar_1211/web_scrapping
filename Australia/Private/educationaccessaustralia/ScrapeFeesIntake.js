const puppeteer = require('puppeteer');
const fs = require("fs");
async function start() {
    const URL = "https://www.eaa.edu.au/fees-and-charges/";
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    var subjectAreasArray = [];
    let Course_Name = "//*[@id='tablepress-3']/tbody/tr/td[1][not(em[contains(text(),'Faculty of')])]";
    let Course_code = "//*[@id='tablepress-3']/tbody/tr/td[2]";
    let Total_Fee = "//*[@id='tablepress-3']/tbody/tr/td[8]";
    let Material_Fee = "//*[@id='tablepress-3']/tbody/tr/td[6]";
    let Tuition_Fee = "//*[@id='tablepress-3']/tbody/tr/td[5]";
    let Duration = "//*[@id='tablepress-3']/tbody/tr/td[3]"
    let Intake_Dates = ""

    const Course_Name11 = await page.$x(Course_Name);
    const Course_code11 = await page.$x(Course_code);
    const Application_Fee11 = await page.$x(Total_Fee);
    const Material_Fee11 = await page.$x(Material_Fee);
    const Tuition_Fee11 = await page.$x(Tuition_Fee);
     const Duration11 = await page.$x(Duration);
    // const Intake_Dates11 = await page.$x(Intake_Dates);

    for (let i = 0; i < Course_Name11.length; i++) {
        let Course_Name22 = await page.evaluate(el => el.textContent, Course_Name11[i]);
        let Course_code22 = await page.evaluate(el => el.textContent, Course_code11[i]);
        let Application_Fee22 = await page.evaluate(el => el.textContent, Application_Fee11[i]);
        let Material_Fee22 = await page.evaluate(el => el.textContent, Material_Fee11[i]);
        let Tuition_Fee22 = await page.evaluate(el => el.textContent, Tuition_Fee11[i]);
         let Duration22 = await page.evaluate(el => el.textContent, Duration11[i]);
        // let Intake_Dates22 = await page.evaluate(el => el.textContent, Intake_Dates11[i]);
        
        subjectAreasArray.push({
            Course_Name: Course_Name22,
            Course_code: Course_code22,
            Total_Fee: Application_Fee22,
            Material_Fee: Material_Fee22,
            Tuition_Fee: Tuition_Fee22,
            Duration: Duration22,
            // Intake_Dates: Intake_Dates22
        });
    }
    await fs.writeFileSync("./output/educationaccessaustralia_fees_caregary.json", JSON.stringify(subjectAreasArray));
    console.log("name-->", JSON.stringify(subjectAreasArray));
    await browser.close();
}
start();
