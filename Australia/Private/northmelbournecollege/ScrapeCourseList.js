// /* const fs = require('fs');
// const Scrape = require('./common/scrape').Scrape;
// const utils = require('./common/utils');
// const configs = require('./configs');
// const awsUtil = require('./common/aws_utils');
// const appConfigs = require('./common/app-config');

// class ScrapeCourseList extends Scrape {
//   // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
//   async scrapeOnlyInternationalCourseList_Mapping(page) {
//     const funcName = 'scrapeCourseCategoryList ';
//     try {
//       var datalist = [];
//       let s;
//       s = new Scrape();
//       await s.init({ headless: true });
//       await s.setupNewBrowserPage("http://nmc.vic.edu.au/courses/");
//       const selector = "//*[@class='wp-block-table']/tbody/tr/td[2]/a";
//       const cricos = "//*[@class='entry-content']/table/tbody/tr/td[3]";
//       var title = await page.$x(selector);
//       var cri = await page.$x(cricos);
//       for (let i = 0; i < title.length; i++) {
//         var categorystring = await page.evaluate(el => el.innerText, title[i]);
//         var categoryurl = await page.evaluate(el => el.href, title[i]);
//         var cric = await page.evaluate(el => el.innerText, cri[i]);
//         console.log("categoryurl------->>>", cric);
//         datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), cricos: cric });
//       }
//       console.log("datalist------->>>", datalist);
//       fs.writeFileSync("./output/northmelbournecollege_courselist.json", JSON.stringify(datalist))
//       await fs.writeFileSync("./output/northmelbournecollege_original_courselist.json", JSON.stringify(datalist));
//       let uniqueUrl = [];
//       for (let i = 0; i < datalist.length; i++) {
//         let cnt = 0;
//         if (uniqueUrl.length > 0) {
//           for (let j = 0; j < uniqueUrl.length; j++) {
//             if (datalist[i].href == uniqueUrl[j].href) {
//               cnt = 0;
//               break;
//             } else {
//               cnt++;
//             }
//           }
//           if (cnt > 0) {
//             uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, cricos: [datalist[i].cricos] });
//           }
//         } else {
//           uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, cricos: [datalist[i].cricos] });
//         }
//       }
//       await fs.writeFileSync("./output/northmelbournecollege_unique_courselist.json", JSON.stringify(uniqueUrl));
//       //based on unique urls mapping of categories
//       for (let i = 0; i < datalist.length; i++) {
//         for (let j = 0; j < uniqueUrl.length; j++) {
//           if (uniqueUrl[j].href == datalist[i].href) {
//             uniqueUrl[j].category.push(datalist[i].category);
//           }
//         }
//       }
//       console.log("totalCourseList -->", uniqueUrl);
//       await fs.writeFileSync("./output/northmelbournecollege_courselist.json", JSON.stringify(uniqueUrl));
//       console.log(funcName + 'writing courseList to file completed successfully....');
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//     }
//   }
//   async scrapeOnlyInternationalCourseList(selFilepath) {
//     const funcName = 'scrapeCoursePageList ';
//     let s = null;
//     try {
//       Scrape.validateParams([selFilepath]);
//       if (appConfigs.shouldTakeCourseListFromOutputFolder) {
//         const fileData = fs.readFileSync(configs.opCourseListFilepath);
//         if (!fileData) {
//           throw (new Error('Invalif file data, fileData = ' + fileData));
//         }
//         const dataJson = JSON.parse(fileData);
//         console.log(funcName + 'Success in getting local data so returning local data....');
//         return dataJson;
//       }
//       const fileData = fs.readFileSync(selFilepath);
//       this.selectorJson = JSON.parse(fileData);
//       s = new Scrape();
//       await s.init({ headless: true });
//       const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
//       console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
//       // set page to url
//       await s.setupNewBrowserPage(rootEleDictUrl);
//       await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
//       if (s) {
//         await s.close();
//       }

//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       // destroy scrape resources
//       if (s) {
//         await s.close();
//       }
//       throw (error);
//     }
//   }
//   async scrapeCourseList(selFilepath) {
//     const funcName = 'scrapeCoursePageList ';
//     let s = null;
//     try {
//       // validate params
//       Scrape.validateParams([selFilepath]);
//       // load file
//       const fileData = fs.readFileSync(selFilepath);
//       this.selectorJson = JSON.parse(fileData);
//       // course_list selector
//       const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
//       if (!selectorsList) {
//         console.log(funcName + 'Invalid selectorsList');
//         throw (new Error('Invalid selectorsList'));
//       }
//       const sel = selectorsList[0];
//       console.log(funcName + 'sel = ' + JSON.stringify(sel));
//       // create Scrape object
//       s = new Scrape();
//       await s.init({ headless: true });
//       const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
//       if (!elementDict) {
//         console.log(funcName + 'Invalid elementDict');
//         throw (new Error('Invalid elementDict'));
//       }
//       const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
//       if (!rootEleDictUrl) {
//         console.log(funcName + 'Invalid rootEleDictUrl');
//         throw (new Error('Invalid rootEleDictUrl'));
//       }
//       const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
//       console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));
//       // course_level_selector key
//       const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
//       if (!coursLvlSelList) {
//         console.log(funcName + 'Invalid coursLvlSelList');
//         throw (new Error('Invalid coursLvlSelList'));
//       }
//       const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
//       if (!elementDictForCorsLvlKey) {
//         console.log(funcName + 'Invalid elementDict');
//         throw (new Error('Invalid elementDict'));
//       }
//       let totalCourseList = []; let count = 1;
//       for (const catDict of courseCatDictList) {
//         console.log(funcName + 'count = ' + count);
//         // click on href
//         console.log(funcName + 'catDict.href = ' + catDict.href);
//         await s.setupNewBrowserPage(catDict.href);
//         const selItem = coursLvlSelList[0];
//         console.log(funcName + 'selItem = ' + selItem);
//         const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
//         console.log(funcName + 'res = ' + JSON.stringify(res));
//         if (Array.isArray(res)) {
//           totalCourseList = totalCourseList.concat(res);
//         } else {
//           throw (new Error('res is not array, it must be...'));
//         }
//         count += 1;
//       } // for (const catDict of courseDictList)
//       console.log(funcName + 'writing courseList to file....');
//       // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
//       await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
//       console.log(funcName + 'writing courseList to file completed successfully....');
//       // put file in S3 bucket
//       const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
//       console.log(funcName + 'object location = ' + res);
//       // destroy scrape resources
//       if (s) {
//         await s.close();
//       }
//       return null;
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       // destroy scrape resources
//       if (s) {
//         await s.close();
//       }
//       throw (error);
//     }
//   }
// } // class

// module.exports = { ScrapeCourseList };
//  */
const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
const format_functions = require('./common/format_functions');
class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'scrapeCourseCategoryList ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];
      var name;
      var name1;
      await s.setupNewBrowserPage("http://nmc.vic.edu.au/courses/");
      var mainCategory = [], redirecturl = [];


      const subjects = await s.page.$x("//*[@class='wp-block-table']/tbody/tr/td[2]/a");
      console.log("subjects---->", subjects)
      const cric = await s.page.$x("//*[@class='wp-block-table']/tbody/tr/td[3]");
      for (let i = 0; i < subjects.length; i++) {
        var elementstring = await s.page.evaluate(el => el.textContent, subjects[i]);

        var elementlink = await s.page.evaluate(el => el.href, subjects[i])
        var elementCric = await s.page.evaluate(el => el.textContent, cric[i])
        console.log("elementCric--->", elementCric);
        totalCourseList.push({ href: elementlink, innerText: elementstring, cricos: elementCric })
      }

      console.log("totalCourseList -->", totalCourseList);
      await fs.writeFileSync("./output/northmelbournecollege_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, cricos: elementCric });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, cricos: elementCric });
        }
      }
      await fs.writeFileSync("./output/northmelbournecollege_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/northmelbournecollege_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  };
}
module.exports = { ScrapeCourseList };


