const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_cricos) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_title': {
                            resJsonData.course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("resJsonData.course_title3333", resJsonData.course_title);
                            const title = String(resJsonData.course_title);
                            console.log("title ", title);
                            let program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            const pro = program_code;
                            let pr = pro.split("–")[0];
                            console.log("programwithtitle ", pr);
                            if (title.indexOf(pr) > -1) {
                                var splitCricos = title.split(pr)[1];
                                let sp1 = splitCricos.split("–")[1]
                                console.log("gdzdhghf", splitCricos);
                                resJsonData.course_title = sp1.trim();
                            } else {
                                resJsonData.course_title = title.trim();;
                            }
                            if (title == "General English") {
                                console.log("General English_General English", title)
                                resJsonData.course_title = title.trim();;
                            }
                        }
                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':

                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            let f1, f2;
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            let spl;
                            var courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time);
                            console.log("courseKeyVal@@@@", courseKeyVal);
                            if (courseKeyVal.indexOf("Duration:") > -1) {
                                f1 = courseKeyVal.split("Duration:")[1].trim();
                                console.log("fullTimeText@@@@", f1);
                            }
                            if (courseKeyVal.indexOf("school") > -1) {
                                f1 = courseKeyVal.split("school")[1];
                                console.log("fullTimeText@@@@", f1);
                            }
                            if (courseKeyVal.indexOf("1200") > -1) {
                                f1 = courseKeyVal.split("1200")[0];
                                console.log("fullTimeText@@@@", f1);
                            }
                            if (f1.includes('week')) {
                                spl = f1.replace("week", " Week");//" full-time
                                console.log("spl------------>", spl);
                            }
                            if (f1.includes('weeks')) {
                                spl = f1.replace("weeks.", "Week");//" full-time
                                console.log("spl@@@@@@@------------>", spl);
                            }
                            else {
                                f1 = courseKeyVal;
                                console.log("in Else@@@@", f1);
                            }
                            if (spl && spl.length > 0) {
                                const resFulltime = spl;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    console.log("DAta--->", JSON.stringify(resFulltime))
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = courseDurationList;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const pieltsDict = {}; const pbtDict = {}; const ppteDict = {}; const ibtDict = {}; const caeDict = {};
                            var penglishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {};; let ieltsNumber = null;
                            let ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_req);
                            let cae = await Course.extractValueFromScrappedElement(courseScrappedData.cae);
                            let ibt = await Course.extractValueFromScrappedElement(courseScrappedData.toefl_ibt);
                            let pte = await Course.extractValueFromScrappedElement(courseScrappedData.pte);
                            let ibt1, cae1, pte1, ielts_req1;
                            console.log("##ielts_req-->" + ielts_req);
                            let iel = "An IELTS level of 5.5 is required for International students entering into this program. (To find out about IELTS, visit www.ielts.org (Only for Overseas Students)"
                            if (ielts_req == iel) {
                                console.log("ABCD")
                                var ieltsScore = "";
                                if (ielts_req) {
                                    ieltsScore = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                                }
                                if (ieltsScore) {
                                    pieltsDict.name = 'ielts academic';
                                    pieltsDict.description = "IELTS" + ielts_req
                                    pieltsDict.min = 0;
                                    pieltsDict.require = ieltsScore;
                                    pieltsDict.max = 9;
                                    pieltsDict.R = 0;
                                    pieltsDict.W = 0;
                                    pieltsDict.S = 0;
                                    pieltsDict.L = 0;
                                    pieltsDict.O = 0;
                                    penglishList.push(pieltsDict);
                                }
                            }
                            else {
                                if (ielts_req.includes("IELTS")) {
                                    ielts_req1 = ielts_req.split("IELTS")[1];
                                    console.log("IELTS@@@@@@", ielts_req1);
                                }
                                if (cae.includes("(CAE)")) {
                                    cae1 = cae.split("(CAE)")[1];
                                    console.log("cea1@@@@@@", cae1);
                                }
                                if (ibt.includes("TOEFL iBT")) {
                                    ibt1 = ibt.split("TOEFL iBT")[1];
                                    console.log("ibt1@@@@@@", ibt1);
                                }
                                if (pte.includes("PTE")) {
                                    pte1 = pte.split("PTE")[1];
                                    console.log("pte1@@@@@@", pte1);
                                }
                            }
                            if (ielts_req.length == 0) {
                                //  ielts_req = "Academic IELTS of 5.5 with 5.0 in all bands, or equivalent.";
                            }
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                            if (ielts_req1) {
                                ieltsScore = await utils.giveMeNumber(ielts_req1.replace(/ /g, ' '));
                            }
                            if (cae1) {
                                caeScore = await utils.giveMeNumber(cae1.replace(/ /g, ' '));
                            }
                            if (ibt1) {
                                ibtScore = await utils.giveMeNumber(ibt1.replace(/ /g, ' '));
                            }
                            if (pte1) {
                                pteScore = await utils.giveMeNumber(pte1.replace(/ /g, ' '));
                            }
                            if (ieltsScore) {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = "IELTS" + ielts_req1
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsScore;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }
                            if (pteScore) {
                                ppteDict.name = 'pte academic';
                                ppteDict.description = "PTE" + pte1
                                ppteDict.min = 0;
                                ppteDict.require = pteScore;
                                ppteDict.max = 90;
                                ppteDict.R = 0;
                                ppteDict.W = 0;
                                ppteDict.S = 0;
                                ppteDict.L = 0;
                                ppteDict.O = 0;
                                penglishList.push(ppteDict);
                            }
                            if (ibtScore) {
                                ibtDict.name = 'toefl ibt';
                                ibtDict.description = "TOEFL iBT" + ibt1
                                ibtDict.min = 0;
                                ibtDict.require = ibtScore;
                                ibtDict.max = 120;
                                ibtDict.R = 0;
                                ibtDict.W = 0;
                                ibtDict.S = 0;
                                ibtDict.L = 0;
                                ibtDict.O = 0;
                                penglishList.push(ibtDict);
                            }
                            if (caeScore) {
                                pbtDict.name = 'cae';
                                pbtDict.description = "Cambridge English Advancedae (CAE)" + cae1;
                                pbtDict.min = 80;
                                pbtDict.require = caeScore;
                                pbtDict.max = 230;
                                pbtDict.R = 0;
                                pbtDict.W = 0;
                                pbtDict.S = 0;
                                pbtDict.L = 0;
                                pbtDict.O = 0;
                                penglishList.push(pbtDict);
                            }
                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            } else {
                                throw new Error("english not found");
                            }
                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var academicReq = "";
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList;
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }
                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }
                            courseAdminReq.entry_requirements_url = "";
                            courseAdminReq.english_requirements_url = "";
                            courseAdminReq.academic_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':

                        case 'course_campus_location': { // Location Launceston
                            let campLocationText = await utils.getValueFromHardCodedJsonFile('location');
                            console.log("fgdugdbgkugyre", campLocationText);
                            let newcampus = [];
                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                resJsonData.course_study_mode = "On campus";
                                console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                var campusedata = [];
                                [campLocationText].forEach(element => {
                                    campusedata.push({
                                        "name": element,
                                        "city": "",
                                        "state": "",
                                        "country": "",
                                        "iscurrent": false
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                // ************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE ************** //
                                var study_mode = [];
                                if (campLocationValTrimmed.trim().length > 0) {
                                    study_mode.push('On campus');
                                }
                                resJsonData.course_study_mode = study_mode.toString();//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);

                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_study_mode':
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {
                                international_student: [],
                            };
                            let part
                            let feesIntStudent = await utils.getValueFromHardCodedJsonFile('fees_mapping');
                            console.log("feesIntStudent@@@", feesIntStudent);
                            let dat = feesIntStudent;
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            console.log("splitStr@@@2" + title);
                            let tmpvar = dat.filter(element => {
                                return element.name.toLowerCase().trim() == title.toLowerCase().trim()
                            });
                            let one = tmpvar[0].fees;
                            console.log("one----->>>>>", one);
                            if (one && one.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(one).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const arrval = String(feesWithDollorTrimmed).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        if (Number(feesNumber)) {
                                            feesDict.international_student.push({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                isfulltime: true,
                                                description: one,
                                                type: ""
                                            });
                                        }
                                        if (feesNumber == "0") {
                                            console.log("FEesNumber = 0");
                                            feesDict.international_student.push({
                                                amount: 0,
                                                duration: 1,
                                                unit: "Year",
                                                isfulltime: true,
                                                description: "",
                                                type: ""
                                            });
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                feesDict.international_student.push({
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",
                                    isfulltime: true,
                                    description: "",
                                    type: ""
                                });
                            }
                            var fee_desc_more = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            let courseKeyVal_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_more);
                            console.log("courseKeyVal_more@@1", courseKeyVal_more);
                            fee_desc_more.push(courseKeyVal_more)
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    feesDict.international_student_all_fees = []
                                    console.log("morefees@@2", fee_desc_more);
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }
                            break;
                        }
                        case 'program_code': {
                            let program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            console.log("program_code@@@@@", program_code)
                            let pr = program_code.split("–")[0];
                            console.log("pr@@@@@@@@@@", pr);
                            resJsonData.program_code = pr.trim();
                            if (program_code == "General English") {
                                console.log("General English_General English_General", program_code)
                                resJsonData.program_code = "";
                            }
                            break;
                        }
                        case 'course_intake': {
                            let datas = [];
                            var campus = resJsonData.course_campus_location;
                            for (let location11 of campus) {
                                var intakedetail = {};
                                intakedetail.name = location11.name;
                                intakedetail.value = [
                                    {
                                        "actualdate": "anytime",
                                        "month": "",
                                        "intake": "",
                                        "filterdate": ""
                                    }
                                ];
                                datas.push(intakedetail);
                            }
                            console.log("DAte--->", datas)
                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            //  let formatedIntake = await format_functions.providemyintake(datas);
                            var intakedata = {};
                            intakedata.intake = datas;
                            intakedata.more_details = more_details;
                            resJsonData.course_intake = intakedata;
                            break;
                        }
                      
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        
                        case 'course_country': {
                            resJsonData.course_country = await Course.extractValueFromScrappedElement(courseScrappedData.course_country)
                            break;
                        }
                        case 'course_overview': {
                            resJsonData.course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            let course_overview = resJsonData.course_overview;
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            //  const courseKeyVal = courseScrappedData.course_career_outcome;
                            resJsonData.course_career_outcome = await Course.extractValueFromScrappedElement(courseScrappedData.course_career_outcome);
                            let course_career_outcome = resJsonData.course_career_outcome;
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = [course_career_outcome]
                            } else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }
                        case 'course_discipline': {

                            break;
                        }
                        case 'course_study_level': {
                            //let cTitle = 
                            const payloadJSON = await format_functions.getMyStudyLevel(course_cricos);
                           
                            resJsonData.course_study_level = payloadJSON.studylevel;
                            resJsonData.course_discipline = payloadJSON.discipline;
                            console.log("resJsonData.course_study_level------>>>>>>", payloadJSON);
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        }
                    }
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                }
            } var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                resJsonData.basecourseid = location_wise_data.course_id;
                for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                    if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                        resJsonData.course_cricos_code[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_cricos_code[i].iscurrent = false;
                    }
                }
                for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                    if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                        resJsonData.course_intake.intake[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_intake.intake[i].iscurrent = false;
                    }
                }
                for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                    if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                        resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                    }
                }
                for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                    if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                        resJsonData.course_campus_location[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_campus_location[i].iscurrent = false;
                    }
                }
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }
    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file
            //custom call
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText,courseDict.cricos);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
