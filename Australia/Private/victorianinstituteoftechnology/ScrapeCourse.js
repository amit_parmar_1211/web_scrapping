const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, desciplain,courseUrl) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline':
                        case 'course_title': {
                            let title_text = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("new titles--->", title_text);
                            var ctitle = format_functions.titleCase(title_text).trim();
                            // var ctitle1 = ctitle.replace('(','').replace(')','').trim();
                        }
                           var ctitle2 = ctitle.replace(' (','(');
                           var ctitle3 = ctitle2.replace(' (','(').trim();
                             console.log("ctitle@@@", ctitle3.trim());
                             resJsonData.course_title = ctitle3
                           
                    
                            // let  title1 = resJsonData.course_title.split("15 ")[1];
                            resJsonData.course_title = ctitle3;
                            //   console.log("titleeee--->",(title1));
                            resJsonData.course_discipline = desciplain;
                            break;
                    
                
                            
                
                        
                        case 'ielts_req':
                        case 'ibt_req':
                        case 'cpe_req':
                        case 'cae_req':
                        case 'pte_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; const cpeDict = {};
                            var myIELTS = "";
                            var myAcademic = [];
                            let IeltsStudent = await utils.getValueFromHardCodedJsonFile('Requirements');
                            var title=await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("titletitle-->",title);
                            let tmpvar = IeltsStudent.filter(val => {
                                return val.name.toLowerCase() == title.toLowerCase()
                            });
                           
                            let one = tmpvar[0].ielts
                            var IeltsIntStudent = one;
                            console.log("Fee in student--->" + IeltsIntStudent);
                       
                            if (one) {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = IeltsIntStudent;
                                ieltsDict.min = 0;
                                ieltsDict.require = await utils.giveMeNumber(IeltsIntStudent.replace(/ /g, ' '));;
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);
                            }
                          
                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }

                            //var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var filedata =  await utils.getValueFromHardCodedJsonFile('Requirements');
                            console.log("ielts_reqielts_req-->",filedata);
                            var title=await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("titletitle-->",title);
                             tmpvar = IeltsStudent.filter(val => {
                                return val.name.toLowerCase() == title.toLowerCase()
                            });
                           
                             one = tmpvar[0].academic
                            const AcademicIntStudent = one;
                            console.log("Academic of student--->" + AcademicIntStudent);
                          
                            
                             courseAdminReq.academic = (AcademicIntStudent) ? [AcademicIntStudent.toString()] : [];
                             console.log("Academic of student--->" + courseAdminReq.academic);
                            // courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];
                            courseAdminReq.english_requirements_url = "https://vit1.sharepoint.com/sites/WebsiteDocuments/Shared%20Documents/Entry%20Requirements.pdf?&originalPath=aHR0cHM6Ly92aXQxLnNoYXJlcG9pbnQuY29tLzpiOi9zL1dlYnNpdGVEb2N1bWVudHMvRWFqU1FoMEhldjFBbWNqUmdValJ6WlVCaTh3akdLMzBYRDV1bm14RUZTbGpNQT9ydGltZT0tdWYtWWN0czEwZw";
                            courseAdminReq.academic_requirements_url = ""
                            courseAdminReq.entry_requirements_url = "https://vit1.sharepoint.com/sites/WebsiteDocuments/Shared%20Documents/Entry%20Requirements.pdf?&originalPath=aHR0cHM6Ly92aXQxLnNoYXJlcG9pbnQuY29tLzpiOi9zL1dlYnNpdGVEb2N1bWVudHMvRWFqU1FoMEhldjFBbWNqUmdValJ6WlVCaTh3akdLMzBYRDV1bm14RUZTbGpNQT9ydGltZT0tdWYtWWN0czEwZw";
                            //courseAdminReq.academic_more_details = academic_more;
                            resJsonData.course_admission_requirement = courseAdminReq;

                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                                console.log("url------->",resJsonData.course_url);
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("filtered_duration_formated", filtered_duration_formated);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston


                            const courseKeyVals = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            var campLocationText = null;
                            var splitdata;
                            let array11 = [] //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict55 = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict565 = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList433 = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList;
                                            if (campLocationText.length > 0) {
                                                campLocationText.forEach(element => {
                                                    if (element.indexOf("Campus:")) {
                                                        splitdata = element.split("Campus:")[0].trim();
                                                        array11.push(splitdata);
                                                        console.log("progrcode-->", array11);
                                                       // console.log("progrcode1--->", splitdata);
                                                    }
                                                })
                                            }
                                            ///}
                                        }
                                    }
                                }
                            }
                          campLocationText = array11;
                          console.log("campLocationTextcampLocationTextcampLocationText",campLocationText);
                          // var campLocationText1 =  String(campLocationText).split(":")[0];
                           // console.log("##Campus-->" + campLocationText1)
                            if (array11 && array11.length > 0) {
                                var campLocationValTrimmed = String(array11);
                                console.log("lastlocation--->",campLocationValTrimmed);
                                var campusedata = [];
                                campLocationText.forEach(element => {
                                    campusedata.push({
                                        "name": element,
                                        "code":String(course_cricos_code)
                                    })
                                });
                                //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                resJsonData.course_campus_location = campusedata;//await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                                // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                var study_mode = [];
                                campLocationValTrimmed = campLocationValTrimmed.toLowerCase();

                                campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                                if (campLocationValTrimmed.trim().length > 0) {
                                    study_mode = "On campus";
                                }
                                resJsonData.course_study_mode = study_mode;//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                                // }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        // 
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                        const courseTuitionFee = {};
                        // courseTuitionFee.year = configs.propValueNotAvaialble;
          
                        const feesList = [];
                        const feesDict = {}
                        let durations 
                        let feesIntStudent = await Course.extractValueFromScrappedElement
                          (courseScrappedData.course_tuition_fees_international_student);
                        let feesIntStudent_desc = feesIntStudent.split("\n")[0];
                        const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                       console.log("feesIntStudentBefore -->", feesIntStudent);
          
                         if (feesIntStudent && feesIntStudent.length) {
                          let splitFees = feesIntStudent;
                          feesIntStudent = splitFees;
                          console.log("feesIntStudentAfte@@@@2r -->", feesIntStudent);
                        } else {
                          throw new Error("Fees new case");
                        }
                        console.log("feesIntStudentAfter -->", feesIntStudent);
                        console.log(" resJsonData.course_duration", resJsonData.course_duration)
                        var duration=resJsonData.course_duration
                       
                        // if we can extract value as Int successfully then replace Or keep as it is
                        if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                          const feesWithDollorTrimmed = String(feesIntStudent).trim();
                          console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                          const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                          console.log(funcName + 'feesVal = ' + feesVal);
                          if (feesVal) {
                            // const regEx = /\d/g;
                            const regEx = /\d+\.?\d*/g; // /[0-9]/g;
                            let feesValNum = feesVal.match(regEx);
                            let finalduration=duration.match(regEx);
                            console.log("finalduration.split(',')[0]",String(finalduration).split(',')[0])
                            if(String(finalduration).includes(',')){
                                 durations=String(finalduration).split(',')[0]
                            }else{
                                durations=finalduration
                            }
                            if (feesValNum) {
                                console.log("Final duration==>",finalduration)
                              console.log(funcName + 'feesValNum = ' + feesValNum);
                              feesValNum = feesValNum.join('');
                              console.log(funcName + 'feesValNum = ' + feesValNum);
                              let feesNumber = null;
                              if (feesValNum.includes(',')) {
                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                              } else {
                                feesNumber = feesValNum;
                              }
                              console.log(funcName + 'feesNumber = ' + feesNumber);
                              if (Number(feesNumber)) {
                                feesDict.international_student={
                                  amount: Number(feesNumber),
                                  duration: durations,
                                  unit: "Weeks",                                  
                                  description: feesIntStudent_desc
                                 
                                }
                              }
                              // if (feesIntStudent && feesIntStudent.length > 0)
                        else {
                          feesDict.international_student={
                            amount: 0,
                            duration: 1,
                            unit: "Year",                            
                            description: "",
                          
                          };
                        }
          
                        // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                        const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                        console.log(funcName + 'cricoseStr = ' + cricoseStr);
                        console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                        if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                          const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                          if (fixedFeesDict) {
                            for (const codeKey in fixedFeesDict) {
                              console.log(funcName + 'codeKey = ' + codeKey);
                              if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                const keyVal = fixedFeesDict[codeKey];
                                console.log(funcName + 'keyVal = ' + keyVal);
                                if (cricoseStr.includes(codeKey)) {
                                  const feesDictVal = fixedFeesDict[cricoseStr];
                                  console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                  if (feesDictVal && feesDictVal.length > 0) {
                                    const inetStudentFees = Number(feesDictVal);
                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                    if (Number(inetStudentFees)) {
                                      feesDict.international_student={
                                        amount: Number(inetStudentFees),
                                        duration: 1,
                                        unit: "weeks",                                       
                                        description: ""
                                       
                                      }
                                    }
                                  }
                                }
                              }
                            } // for
                          } // if
                        } // if
          
                        if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                          const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                          const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                          if (feesDuration && feesDuration.length > 0) {
                            feesDict.fee_duration_years = feesDuration;
                          }
                          if (feesCurrency && feesCurrency.length > 0) {
                            feesDict.currency = feesCurrency;
                          }
                          console.log("(feesIntStudent.length > 0) -->", feesIntStudent);
                          
                          if (feesDict) {
                            // feesList.push(feesDict);
                            var campus = resJsonData.course_campus_location;
                            for (let loc of campus) {
                              feesList.push({ name: loc.name, value: feesDict });
                            }
                          }
                          if (feesList && feesList.length > 0) {
                            courseTuitionFee.fees = feesList;
                          }
                          console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                          // take tuition fee value at json top level so will help in DDB for indexing
                          if (courseTuitionFee && feesDict.international_student) {
                            resJsonData.course_tuition_fee = courseTuitionFee;
                          }
                          // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                          if (!feesDict.international_student) {
                            console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                            console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                            return null; // this will add this item into FailedItemList and writes file to local disk
                          }
                        } else {
          
                        }
                        break;
                    }
                }
            }
        }

                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                // program_code = program_code.split(" ");

                                                program_code = JSON.stringify(selList).trim();
                                                console.log("program_code----->",program_code);
                                                if(program_code.includes("Diploma of Information Technology Networking"))
                                                {
                                                    
                                                    let program_code2 = await Course.extractValueFromScrappedElement(courseScrappedData.program_code1)
                                                   
                                                    let program_code4 = program_code2.slice(-8);
                                                    console.log("program_code2ggrg---->",program_code4);
                                                    resJsonData.program_code = program_code4;
                                                }
                                                else if(program_code.includes("Certificate IV in Patisserie"))
                                                {
                                                    let program_code2 = await Course.extractValueFromScrappedElement(courseScrappedData.program_code1)
                                                   
                                                    let program_code4 = program_code2.slice(-8);
                                                    console.log("program_code2ggrg---->",program_code4);
                                                    resJsonData.program_code = program_code4;
                                                }
                                                else if(program_code.includes("("))
                                                {
                                                   let  program_code1 = program_code.split("(")[0].trim();
                                                   console.log("program_code2---->",program_code1);
                                                   let program_code2 = program_code1.slice(-9).trim();
                                                   console.log("program_code2---->",program_code2);
                                                   resJsonData.program_code = program_code2;
                                                   console.log("resJsonData.program_code-->",resJsonData.program_code);
                                                }
                                                else{
                                                //     let program_code1 = program_code.split("(")[0].trim();
                                                //    console.log("program_code2---->",program_code1);
                                                   let program_code2 = program_code.slice(-10).trim();
                                                   console.log("program_code78678768---->",program_code2);
                                                   let program_code3 = program_code2.replace('\"]','').trim();
                                                   console.log("program_code3---->",program_code3);
                                                   resJsonData.program_code = program_code3;
                                                   console.log("resJsonData.program_code-F->",resJsonData.program_code);
                                                }
                                            //     let program_code1 = program_code.split("(")[0];
                                            //    console.log("program_code1",program_code1);
                                            //     let alert = program_code1[program_code1.length - 1];
                                            //     console.log("gdfgjdefjdf",alert);
                                            //     let alert1 = alert.replace('"', '');
                                            //     let alert2 = alert1.replace(']', '');
                                            //     //  let prog1 = program_code.replace("[\"","");
                                            //     console.log("new program code-->", alert2);
                                            //     console.log("progrramcode---->", JSON.stringify(alert2));
                                            //     resJsonData.program_code = (alert2) ? alert2 : "";
                                            }
                                        }
                                    }
                                }
                            }
                            // if (program_code.length > 0) {

                            //}
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            // Semester 1 (February) and Semester 2 (August)
                            // prepare emty JSON struct
                            const courseIntakeDisplay = {};
                            // existing intake value
                            var courseIntakeStr = null;//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;
                            console.log("intake str-?" + JSON.stringify(courseScrappedData.course_intake))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                courseIntakeStr = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (!courseIntakeStr) {
                                courseIntakeStr = [];
                            }

                            if (courseIntakeStr) {
                                var intakes = [];
                                var campus = resJsonData.course_campus_location;
                                console.log("CampIntake--->" + JSON.stringify(courseIntakeStr))
                                var myvalue = [];
                                var filedata = JSON.parse(await fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                                var myintk = filedata["intakes"];

                                
                                for (let loc of campus) {
                                    var intakedetail = {};
                                    intakedetail.name = loc.name;
                                    intakedetail.value = myintk;
                                    intakes.push(intakedetail);
                                    var intakeUrl = [];
                                    intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                    let formatIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                    if (formatIntake && formatIntake.length > 0) {
                                        var intakedata = {};
                                        intakedata.intake = formatIntake;
                                        intakedata.more_details = String(intakeUrl);
                                        resJsonData.course_intake = intakedata;
                                    }

                                }

                            }
                            break;
                        }


                        
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            resJsonData.course_overview = course_overview;
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                console.log("Outcome is--->" + JSON.stringify(selList))
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }
                        case 'course_study_level':
                            {
                                const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code)
                                console.log("studylevelllll----->", cTitle)

                                let cStudyLevel = await format_functions.getMyStudyLevel(cTitle);
                                console.log("course_study_level--------", cStudyLevel);
                                resJsonData.course_study_level = cStudyLevel
                                break
                            }
                            case 'course_outline': {
                                const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                                 const courseKeyVal_major = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline);
                                 const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                                 let course_outlines = {};
                                 if (courseKeyVal_minor != null) {
                                     course_outlines.minors = courseKeyVal_minor
                                 } else {
                                     course_outlines.minors = []
                                 }
                                 if (courseKeyVal_major != null) {
                                     course_outlines.majors = courseKeyVal_major
                                 } else {
                                     course_outlines.majors = []
                                 }
                                 if (courseKeyVal != null) {
                                     course_outlines.more_details = courseKeyVal
                                 } else {
                                     course_outlines.more_details = ""
                                 }
                                 resJsonData.course_outline = course_outlines
                                 break;
                             }
                             case 'application_fees':{
                                 const courseKeyVal=await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                                 if(courseKeyVal != null){
                                     resJsonData.application_fees=courseKeyVal
                                 }else{
                                     resJsonData.application_fees=""
                                 }
                                 break
                             }

                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            // console.log('\n\r' + funcName + 'course resJsonData =' + JSON.stringify(resJsonData));
            // var resJsonData = JSON.parse(fs.readFileSync("australiancatholicuniversity_graduatediplomainrehabilitation_coursedetails.json"))[0];
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                console.log("resJsonData.course_title-->", resJsonData.course_title);
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
               NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;              
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.category,courseUrl, courseDict.study_level);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
