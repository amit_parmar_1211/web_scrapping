
const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');

const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {

    // refers to https://futurestudents.csu.edu.au/courses/all

    async scrapeOnlyInternationalCourseList() {
        const funcName = 'scrapeCourseCategoryList ';
        var s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            var totalCourseList = [];
            await s.setupNewBrowserPage("https://www.vit.edu.au/courses-fees/");
            var mainCategory = [], redirecturl = [];
            //scrape main category

            const maincategoryselector = "//*[@class='accord']/h3[contains(text(),'International Students')]/..//ul/li/a[not(contains(text(),'English Language'))]";


            var category = await s.page.$x(maincategoryselector);
            console.log("Total categories-->" + category.length);
            for (let link of category) {
                var categorystringmain = await s.page.evaluate(el => el.innerText, link);
                var categorystringmainurl = await s.page.evaluate(el => el.href, link);
                mainCategory.push(categorystringmain.trim());
                redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
            }

            //scrape courses
            for (let catc of redirecturl) {
                console.log("#processed-->" + catc.innerText);
                await s.page.goto(catc.href, { timeout: 0 });
                const linkforHSPTLT = "//*[@class='innr-cntbxmn']/div[@class='bussbxtab bussovrscrll']/table/tbody/tr/td/a[@class='inrach']"
                var title = await s.page.$x(linkforHSPTLT);
                if (title && title.length > 0) {
                    for (let i = 0; i < title.length; i++) {
                        var titleLink = await s.page.evaluate(el => el.href, title[i]);
                        var titleString = await s.page.evaluate(el => el.innerText, title[i]);
                        totalCourseList.push({ href: titleLink, innerText: titleString, category: catc.innerText });
                    }
                }
                else {
                    const titleforIT = "//*[@class='innr-cntbxmn']/h2";
                    var titlesel = await s.page.$x(titleforIT);
                    var titleselval = await s.page.evaluate(el => el.innerText, titlesel[0]);
                    totalCourseList.push({ href: catc.href, innerText: titleselval, category: catc.innerText });
                }
            }

            fs.writeFileSync("./output/victorianinstituteoftechnology_maincategorylist.json", JSON.stringify(mainCategory))
            console.log("totalCourseList -->", totalCourseList);
            await fs.writeFileSync("./output/victorianinstituteoftechnology_original_courselist.json", JSON.stringify(totalCourseList));
            let uniqueUrl = [];
            //unique url from the courselist file
            for (let i = 0; i < totalCourseList.length; i++) {
                let cnt = 0;
                if (uniqueUrl.length > 0) {
                    for (let j = 0; j < uniqueUrl.length; j++) {
                        if (totalCourseList[i].href == uniqueUrl[j].href) {
                            cnt = 0;
                            break;
                        } else {
                            cnt++;
                        }
                    }
                    if (cnt > 0) {
                        uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
                    }
                } else {
                    uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
                }
            }
            await fs.writeFileSync("./output/victorianinstituteoftechnology_unique_courselist.json", JSON.stringify(uniqueUrl));
            //based on unique urls mapping of categories
            for (let i = 0; i < totalCourseList.length; i++) {
                for (let j = 0; j < uniqueUrl.length; j++) {
                    if (uniqueUrl[j].href == totalCourseList[i].href) {
                        if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

                        } else {
                            uniqueUrl[j].category.push(totalCourseList[i].category);
                        }
                    }
                }
            }
            // console.log("uniqueUrl -->", uniqueUrl);
            await fs.writeFileSync("./output/victorianinstituteoftechnology_courselist.json", JSON.stringify(uniqueUrl));
            console.log(funcName + 'writing courseList to file completed successfully....');
            await s.browser.close();
            console.log(funcName + 'browser closed successfully.....');
            return true;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }



} // class

module.exports = { ScrapeCourseList };
