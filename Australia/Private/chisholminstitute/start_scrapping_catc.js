const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;

const startScrapping = async function startScrappingFunc() {
  const funcName = 'startScrappingFunc ';
  try {
    s = new Scrape();
    await s.init({ headless: true });
    //var datalist = [];
    await s.setupNewBrowserPage("https://www.chisholm.edu.au/students/international/international-courses");
    var mainCategory = [], redirecturl = [],datalist = [];;
    //scrape main category
    const maincategoryselector = "//*[@id='main-navigation']/li[2]/div/ul/li/a[not(contains(text(),'Short courses'))]";
    var category = await s.page.$x(maincategoryselector);
    console.log("Total categories-->" + category.length);
    for (let i = 0; i < category.length; i++) {
      var categorystringmain = await s.page.evaluate(el => el.innerText, category[i]);
      var categorystringmainurl = await s.page.evaluate(el => el.href, category[i]);
      mainCategory.push(categorystringmain.trim());
      redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
    }

    //scrape courses
    for (let catc of redirecturl) {
      await s.page.goto(catc.href, { timeout: 0 });
      const urlselector = "//*/ul[@class='accordion']//ul/li/a[1]";
      const textselector = "//*/ul[@class='accordion']//ul/li/a[1]/span/text()[1]";
      var title = await s.page.$x(textselector);
      var link = await s.page.$x(urlselector);
      console.log("title-->" + title.length + " links -->" + link.length);
      for (let t = 0; t < title.length; t++) {
        var categorystring = await s.page.evaluate(el => el.textContent, title[t]);
        var categoryurl = await s.page.evaluate(el => el.href, link[t]);
        datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: catc.innerText });
      }
    }

    fs.writeFileSync("./output/universityofcanberra_courselist.json", JSON.stringify(datalist))
    fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
    await s.browser.close();
    console.log(funcName + 'browser closed successfully.....');
    return true;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
};
startScrapping();