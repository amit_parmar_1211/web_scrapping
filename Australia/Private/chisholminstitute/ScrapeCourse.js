const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const puppeteer = require('puppeteer');
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {

                            resJsonData.course_discipline = course_category;

                            console.log("course_discipline--->" + JSON.stringify(resJsonData.course_discipline))

                            break;
                        }
                        case 'course_title_category':
                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            // .replace('\n', '')
                            //  title = title.replace('/', '/ ');
                            //  console.log("splitStr@@@2" + title);
                            var ctitle = format_functions.titleCase(title).trim();
                            // var ctitle2 = ctitle.replace(' / ', '/');
                            console.log("ctitle@@@", ctitle);
                            resJsonData.course_title = ctitle
                            break;
                        }


                        case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':                        
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            let ieltsNumber = null, pteNumber = null, ibtNumber = null, caeNumber = null;
                            let myscore = '';
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; const pbtDict = {};
                            console.log("resJsonData.course_toefl_ielts_score -->", JSON.stringify(courseScrappedData.course_toefl_ielts_score));
                            let ieltsString = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                            let finalIeltsString = '';
                         
                            if(resJsonData.course_title.includes("Elicos International English Course") || resJsonData.course_title.includes("Bachelor Of Accounting")){
                                finalIeltsString=""
                            }
                            else if (ieltsString.indexOf('IELTS') > -1) {
                                let ieltsSplitString = ieltsString.split('IELTS');
                                finalIeltsString = ieltsSplitString[1]
                            }
                           
                            console.log("finalIeltsString", finalIeltsString)
                            if (finalIeltsString && finalIeltsString.length > 0) {

                                // extract exact number from string
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                const matchedStrList = String(finalIeltsString).match(regEx);
                                console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    ieltsNumber = Number(matchedStrList[0]);
                                    console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                }
                                myscore = await utils.getMappingScore(ieltsNumber);
                                var ibt_req = '';
                                var pbt_req = '';
                                var pte_req = '';
                                var cae_req = '';
                                if (myscore.ibt && myscore.ibt.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.ibt).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ibt_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'ibtNumber = ' + ibt_req);
                                    }
                                }
                                if (myscore.pbt && myscore.pbt.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.pbt).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pbt_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'pbtNumber = ' + pbt_req);
                                    }
                                }
                                if (myscore.pte && myscore.pte.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.pte).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pte_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'pteNumber = ' + pte_req);
                                    }
                                }
                                if (myscore.cae && myscore.cae.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.cae).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        cae_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'caeNumber = ' + cae_req);
                                    }
                                }

                                if (ieltsNumber) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = myscore.ielts;
                                    englishList.push(ieltsDict);
                                }

                                if (ibt_req) {
                                    tofelDict.name = 'toefl ibt';
                                    tofelDict.description = myscore.ibt;
                                    englishList.push(tofelDict);
                                }
                                if (pbt_req) {
                                    pbtDict.name = 'toefl pbt';
                                    pbtDict.description = myscore.pbt;
                                    englishList.push(pbtDict);
                                }
                                if (pte_req) {
                                    pteDict.name = 'pte academic';
                                    pteDict.description = myscore.pte;
                                    englishList.push(pteDict);
                                }

                                ///progress bar start
                                const pieltsDict = {}; const pibtDict = {}; const ppteDict = {}; const pcaeDict = {}; const ppbtDict = {};
                                var penglishList = [];
                                var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";


                                if (ieltsNumber != "") {
                                    pieltsDict.name = 'ielts academic';
                                    pieltsDict.description = finalIeltsString;
                                    pieltsDict.min = 0;
                                    pieltsDict.require = await utils.giveMeNumber(finalIeltsString.replace(/ /g, ' '));
                                    pieltsDict.max = 9;
                                    pieltsDict.R = 0;
                                    pieltsDict.W = 0;
                                    pieltsDict.S = 0;
                                    pieltsDict.L = 0;
                                    pieltsDict.O = 0;
                                    penglishList.push(pieltsDict);
                                }
                                if (ibt_req) {
                                    pibtDict.name = 'toefl ibt';
                                    pibtDict.description = tofelDict.description;
                                    pibtDict.min = 0;
                                    pibtDict.require = ibt_req;
                                    pibtDict.max = 120;
                                    pibtDict.R = 0;
                                    pibtDict.W = 0;
                                    pibtDict.S = 0;
                                    pibtDict.L = 0;
                                    pibtDict.O = 0;
                                    penglishList.push(pibtDict);
                                }
                                if (pbt_req) {
                                    ppbtDict.name = 'toefl pbt';
                                    ppbtDict.description = pbtDict.description;
                                    ppbtDict.min = 310;
                                    ppbtDict.require = pbt_req;
                                    ppbtDict.max = 677;
                                    ppbtDict.R = 0;
                                    ppbtDict.W = 0;
                                    ppbtDict.S = 0;
                                    ppbtDict.L = 0;
                                    ppbtDict.O = 0;
                                    penglishList.push(ppbtDict);
                                }
                                if (pte_req) {
                                    ppteDict.name = 'pte academic';
                                    ppteDict.description = pteDict.description;
                                    ppteDict.min = 0;
                                    ppteDict.require = pte_req;
                                    ppteDict.max = 90;
                                    ppteDict.R = 0;
                                    ppteDict.W = 0;
                                    ppteDict.S = 0;
                                    ppteDict.L = 0;
                                    ppteDict.O = 0;
                                    penglishList.push(ppteDict);
                                }
                                if (cae_req) {
                                    pcaeDict.name = 'cae';
                                    pcaeDict.description = myscore.cae;
                                    pcaeDict.min = 80;
                                    pcaeDict.require = cae_req;
                                    pcaeDict.max = 230;
                                    pcaeDict.R = 0;
                                    pcaeDict.W = 0;
                                    pcaeDict.S = 0;
                                    pcaeDict.L = 0;
                                    pcaeDict.O = 0;
                                    penglishList.push(pcaeDict);
                                }
                            }




                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            } else {
                                courseAdminReq.english = []
                            }



                            //var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var english_req = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var academic_req = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirement_url);


                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            // console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            academicReq = selList;
                                            //}
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }
                            // courseAdminReq.academic = (academicReq) ? academicReq.toString() : [];
                            console.log("Academic requirement" + JSON.stringify(courseAdminReq.academic));

                            //  courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];

                            courseAdminReq.english_requirements_url = english_req;
                            courseAdminReq.entry_requirements_url = "";
                            courseAdminReq.academic_requirements_url = "";



                            //courseAdminReq.academic_more_details = academic_more;
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));

                                            rescourse_url = selList;

                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;

                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            for (let sel of selList) {
                                                sel = sel.replace(/years./g, 'years');
                                                sel = sel.replace(/year./g, 'year');
                                                sel = sel.replace(/months./g, 'months');
                                                sel = sel.replace(/weeks./g, 'weeks');
                                                sel = sel.replace(/year /g, 'years');
                                                sel = sel.replace(/\(|\)/g, " ");
                                                sel = sel.replace(/time:/g, 'time ');
                                                sel = sel.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ');
                                                fullTimeText = sel;
                                            }


                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    // if (courseDurationDisplayList && courseDurationDisplayList.length > 0) {
                                    //     resJsonData.course_duration_display = courseDurationDisplayList;
                                    // }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston
                            const courseKeyVals = courseScrappedData.course_cricos_code;
                            let course_cricos_code = [];
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {

                                                for (let sel of selList) {
                                                    if (sel.indexOf('-') > -1) {
                                                        let splitCricos = sel.split('-');
                                                        course_cricos_code.push(splitCricos[1].trim());
                                                    } else {
                                                        course_cricos_code.push(sel.trim());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            var campLocationText; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            let newcampus = [];
                            let flocation = []
                            for (let campus of campLocationText) {
                                if (!newcampus.includes(campus)) {
                                    newcampus.push(campus);
                                    console.log("##Campuscampus-->" + campus)
                                }
                            }
                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            //   let valfainal =campLocationText.toString();

                            if (newcampus && newcampus.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                var location = ["Dandenong", "Frankston", "Berwick", "Chisholm at 311", "Mornington peninsula"]
                                for (let i = 0; i < location.length; i++) {
                                    newcampus.forEach(element => {
                                        console.log("element",element)
                                        if (element.includes(location[i])) {
                                            flocation.push(location[i])

                                        }
                                    })
                                }
                                var campusedata = [];
                                flocation.forEach(element => {
                                    campusedata.push({
                                        "name": element,
                                        "code": String(course_cricos_code)
                                    })
                                });
                                console.log("campusedata",campusedata)
                                if(campusedata.length > 0){
                                    resJsonData.course_campus_location = campusedata;
                                }else{
                                    throw error("location not found")
                                }
                              

                                resJsonData.course_study_mode = "On campus";//.join(',');

                                // }
                            } // if (campLocationText && campLocationText.length > 0)

                            break;
                        }

                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;
                            const feesList = [];
                            const feesDict = {};

                            let campLocationTextfees;
                            const courseKeyValfees = courseScrappedData.course_tuition_fees_international_student;
                            console.log("courseKeyValfees---->", JSON.stringify(courseKeyValfees));
                            if (Array.isArray(courseKeyValfees)) {
                                for (const rootEleDict of courseKeyValfees) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                campLocationTextfees = selList;


                                            }

                                        }
                                    }
                                }
                            }


                            // const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);


                            if (campLocationTextfees && campLocationTextfees.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(campLocationTextfees).replace(/[\r\n\t ]+/g, ' ').trim();
                                //const feesWithDollorTrimmed = String(campLocationTextfees).replace  (/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s|\s\s\s)/g, '').trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                let newFees = feesWithDollorTrimmed.split('=')[1];
                                console.log("new feess -->", newFees)
                                const feesVal1 = String(newFees).replace('$', '');
                                const arrval = String(feesVal1).split('for');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);

                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = {
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: feesWithDollorTrimmed,
                                            };

                                        }
                                    }
                                    else {
                                        feesDict.international_student = {
                                            amount: 0,
                                            duration: 1,
                                            unit: "Year",
                                            description: "",
                                        };
                                    }

                                }

                            } // if (feesIntStudent && feesIntStudent.length > 0)                                                

                            let international_student_all_fees_array = [];
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const MaterialsFee = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_more);
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;

                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }

                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = [];
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            for (let program_val of program_code) {
                                resJsonData.program_code = program_val;
                            }
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            // Semester 1 (February) and Semester 2 (August)
                            // prepare emty JSON struct
                            const courseIntakeDisplay = [];


                            // existing intake value
                            var courseIntakeStr = [];//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            //     courseIntakeStr = selList;
                                            // }

                                            for (let selItem of selList) {
                                                if (selItem.includes('and')) {
                                                    selItem = selItem.replace(/and/g, ',');
                                                }
                                                if (selItem.includes('followed by ongoing enrolments')) {
                                                    selItem = selItem.replace(/followed by ongoing enrolments/g, '');
                                                }
                                                selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s|)/g, '').split(",");
                                                console.log("selItem -->", selItem);
                                                for (let sel of selItem) {
                                                    if (sel.includes('Feb ')) {
                                                        sel = 'February'
                                                    } else {
                                                        sel = sel.trim();
                                                    }
                                                    console.log("selItemIntake -->", selItem);

                                                    courseIntakeStr.push(sel + ",2020");
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            if (!courseIntakeStr) {
                                courseIntakeStr = ["NA"];
                            }
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                console.log("courseIntakeStr -->", courseIntakeStr);
                                var intakes = [];
                                var campus = resJsonData.course_campus_location;

                                for (let loc of campus) {
                                    var intakedetail = {};
                                    var myintake = [];
                                    courseIntakeStr[0]

                                    // myintake = await utils.giveMeArray(courseIntakeStr[0].replace(/[\r\n\t()]+/g, '').replace('Semester 1', '').replace('Semester 2', ''), "and");
                                    intakedetail.name = loc.name;
                                    intakedetail.value = courseIntakeStr;
                                    intakes.push(intakedetail);
                                }

                                var intakedata = {};
                                intakedata.intake = intakes;
                                intakedata.more_details = resJsonData.course_intake_url;
                                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');

                            }



                            console.log("FinalIntake--" + JSON.stringify(intakes));
                            let formatedIntake = await format_functions.providemyintake(intakes, "MOM", "");
                            console.log("NEw Intake Format@@@@@" + JSON.stringify(formatedIntake))
                            var intakedata = {};

                            intakedata.intake = formatedIntake;

                            let valmonths11 = "";

                            //   intakedata.more_details = more_details;
                            resJsonData.course_intake = intakedata;                          
                            intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url')
                            break;
                        }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }

                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }

                        case 'course_study_level': {
                            const code = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log("Cricodecbhhfofdhdess---", code);
                            let study_l = await format_functions.getMyStudyLevel(code);
                            console.log("NEw Intake Formagfdt@@@@@" + JSON.stringify(study_l))
                            // for (let study_l of code) {
                            resJsonData.course_study_level = study_l;

                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                            const courseKeyVal_major = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline);
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                            let course_outlines = {};
                            if (courseKeyVal_minor != null) {
                                course_outlines.minors = courseKeyVal_minor
                            } else {
                                course_outlines.minors = []
                            }
                            if (courseKeyVal_major != null) {
                                course_outlines.majors = courseKeyVal_major
                            } else {
                                course_outlines.majors = []
                            }
                            if (courseKeyVal != null) {
                                course_outlines.more_details = courseKeyVal
                            } else {
                                course_outlines.more_details = ""
                            }
                            resJsonData.course_outline = course_outlines
                            break;
                        }
                        case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            if (courseKeyVal != null) {
                                resJsonData.application_fee = courseKeyVal
                            } else {
                                resJsonData.application_fee = ""
                            }
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];

            var locations = resJsonData.course_campus_location;
            console.log("locations",locations)
            for (let location of locations) {
                console.log("location",location)
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }

                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "NA";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;
                
                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = 0;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            //const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText);
            //
            // const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category);
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category, s.page.url());


            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
