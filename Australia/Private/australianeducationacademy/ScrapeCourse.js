const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = course_category;
                            // As of not we do not have way to get this field value
                            break;
                        }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""


                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        case 'application_fee': {
                            resJsonData.application_fee = "";
                        }
                        case 'course_title': {
                            resJsonData.course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("resJsonData.course_title3333", resJsonData.course_title);
                            const title = String(resJsonData.course_title);
                            console.log("title ", title);
                            resJsonData.program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            const pro = String(resJsonData.program_code);
                            console.log("programwithtitle ", pro);
                            if (title.indexOf(pro) > -1) {
                                var splitCricos = title.split(pro)[1];
                                console.log("gdzdhghf", splitCricos);
                                var ctitle = format_functions.titleCase(splitCricos).trim(); 
                                resJsonData.course_title = ctitle;
                            } else {
                                console.log("gdzdsgdfgzdhghf", splitCricos);
                            }
                        }

                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                                case 'course_toefl_ielts_score': {
                                    const ielts_req = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                                    console.log("ielts_req@@@@@",ielts_req);
                                    var courseIntake = [];
                                    const courseAdminReq = {};
                                    const englishList = [];
                                    const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; let ieltsNumber = null;
                                   // let ielts_req //= "A minimum IELTS (General) test score of 5.5 or equivalent for direct entry into a VET course.";
                                    // if (ielts_req.length == 0) {
                                    //     "A minimum IELTS (General) test score of 5.5 or equivalent for direct entry into a VET course.";
                                    // }

                                    const pieltsDict = {}; const pibtDict = {}; const ppteDict = {};
                                    var penglishList = [];
                                    var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                                    if (ielts_req) {
                                        ieltsScore = await utils.giveMeNumber(String(ielts_req).replace(/ /g, ' '));
                                    }
                                    if (ieltsScore) {
                                        pieltsDict.name = 'ielts academic';
                                        pieltsDict.description = ielts_req;
                                        pieltsDict.min = 0;
                                        pieltsDict.require = ieltsScore;
                                        pieltsDict.max = 9;
                                        pieltsDict.R = 0;
                                        pieltsDict.W = 0;
                                        pieltsDict.S = 0;
                                        pieltsDict.L = 0;
                                        pieltsDict.O = 0;
                                        penglishList.push(pieltsDict);
                                    }
        
                                    if (penglishList && penglishList.length > 0) {
                                        courseAdminReq.english = penglishList;
                                    } else {
                                        throw new Error("english not found");
                                    }
        
                                    var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                                    var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                                    var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                                    var academicReq = "";
                                    const courseKeyVal = courseScrappedData.course_academic_requirement;
                                    console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                                    if (Array.isArray(courseKeyVal)) {
                                        for (const rootEleDict of courseKeyVal) {
                                            console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                            const elementsList = rootEleDict.elements;
                                            for (const eleDict of elementsList) {
                                                console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                                const selectorsList = eleDict.selectors;
                                                for (const selList of selectorsList) {
                                                    academicReq = selList;
                                                }
                                            }
                                        }
                                    }
                                    courseAdminReq.academic = [];
                                    for (let academicvalue of academicReq) {
                                        courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                                    }
        
                                    courseAdminReq.academic = [];
                                    for (let academicvalue of academicReq) {
                                        courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                                    }
                                    courseAdminReq.entry_requirements_url = entry_requirements_url;
                                    courseAdminReq.english_requirements_url = "";
                                    courseAdminReq.academic_requirements_url = academic_more;
                                    resJsonData.course_admission_requirement = courseAdminReq;
                                    break;
                                }


                        // case 'course_toefl_ielts_score': {
                        //     var courseIntake = [];
                        //     const courseAdminReq = {};
                        //     const englishList = [];
                        //     const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; let ieltsNumber = null;
                        //     let ielts_req = "A minimum IELTS (General) test score of 5.5 or equivalent for direct entry into a VET course.";
                        //     if (ielts_req.length == 0) {
                        //         "A minimum IELTS (General) test score of 5.5 or equivalent for direct entry into a VET course.";
                        //     }
                        //     const pieltsDict = {}; const pibtDict = {}; const ppteDict = {};
                        //     var penglishList = [];
                        //     var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                        //     if (ielts_req) {
                        //         ieltsScore = await utils.giveMeNumber(String(ielts_req).replace(/ /g, ' '));
                        //     }
                        //     if (ieltsScore) {
                        //         pieltsDict.name = 'ielts academic';
                        //         pieltsDict.description = ielts_req;
                        //         pieltsDict.min = 0;
                        //         pieltsDict.require = ieltsScore;
                        //         pieltsDict.max = 9;
                        //         pieltsDict.R = 0;
                        //         pieltsDict.W = 0;
                        //         pieltsDict.S = 0;
                        //         pieltsDict.L = 0;
                        //         pieltsDict.O = 0;
                        //         penglishList.push(pieltsDict);
                        //     }

                        //     if (penglishList && penglishList.length > 0) {
                        //         courseAdminReq.english = penglishList;
                        //     } else {
                        //         throw new Error("english not found");
                        //     }

                        //     var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                        //     var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                        //     var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                        //     var academicReq = "";
                        //     const courseKeyVal = courseScrappedData.course_academic_requirement;
                        //     console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     academicReq = selList;
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     courseAdminReq.academic = [];
                        //     for (let academicvalue of academicReq) {
                        //         courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                        //     }

                        //     courseAdminReq.academic = [];
                        //     for (let academicvalue of academicReq) {
                        //         courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                        //     }
                        //     courseAdminReq.entry_requirements_url = entry_requirements_url;
                        //     courseAdminReq.english_requirements_url = "";
                        //     courseAdminReq.academic_requirements_url = "";
                        //     resJsonData.course_admission_requirement = courseAdminReq;
                        //     break;
                        // }
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = "";
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                                if (fullTimeText.includes('Theory& Practical'))
                                                {
                                                    fullTimeText = fullTimeText.split('. Theory& Practical')[0].trim();
                                                    fullTimeText = fullTimeText.split('period of')[1].trim();
                                                }
                                                console.log("##fullTimeText--->" + JSON.stringify(fullTimeText));
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime = resFulltime;
                                    courseDurationList = durationFullTime;
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = courseDurationList;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':

                        case 'course_campus_location': { // Location Launceston
                            var campLocationText; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                           var cricc = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            let newcampus = [];
                            for (let campus of campLocationText) {
                                if (!newcampus.includes(campus)) {
                                    newcampus.push(campus);
                                    console.log("##Campuscampus-->" + campus)
                                }
                            }
                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            //   let valfainal =campLocationText.toString();
                            var campusedata = [];
                            if (campLocationText.includes("Level 8, 190 Queen Street,Melbourne 3000")) {
                                campusedata.push({
                                    "name": "Melbourne",
                                    "code": cricc,
                                   
                                });

                            }

                            if (campLocationText.includes("10 Blissington Street, Springvale Vic 3171;  97A Smith St, Fitzroy, Vic 3065")) {
                          
                                campusedata.push({
                                    "name": "Melbourne",
                                    "code": cricc,
                                });

                            }
                            if (campLocationText.includes("10 Blissington Street, Springvale Vic 3171")) {
                             
                                campusedata.push({
                                    "name": "Melbourne",
                                    "code": cricc,
                                   
                                });

                            }
                            console.log("##campusedata-->" + campusedata)
                            resJsonData.course_campus_location = campusedata;




                            var loca = String(campLocationText).split('and');

                            if (loca && loca.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                resJsonData.course_study_mode = "On campus";
                                //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                //  if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                // console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                // var campusedata = [];
                                // loca.forEach(element => {
                                //     campusedata.push({
                                //         "name": element,
                                //         "city": "",
                                //         "state": "",
                                //         "country": "",
                                //         "iscurrent": false
                                //     })
                                // });
                                resJsonData.course_campus_location = campusedata;
                                // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                var study_mode = [];
                                campLocationValTrimmed = campLocationValTrimmed.toLowerCase();

                                campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                                if (campLocationValTrimmed.trim().length > 0) {
                                    study_mode.push('On campus');
                                }
                                resJsonData.course_study_mode = study_mode.toString();//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                                //  }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_study_mode':
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            const courseKeyVal = courseScrappedData.course_tuition_fees_international_student;
                            const courseKeyValAdditional = courseScrappedData.course_tuition_fees_international_student_additional;
                            console.log("courseKeyValAdditional -->", JSON.stringify(courseKeyValAdditional));
                            let feesIntStudent = [];
                            let feesIntStudentAdditional = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                feesIntStudent = selList;
                                            }
                                        }
                                    }
                                }
                            }

                            //External study mode fees are different
                            if (Array.isArray(courseKeyValAdditional)) {
                                for (const rootEleDict of courseKeyValAdditional) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                feesIntStudentAdditional = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (feesIntStudentAdditional && feesIntStudentAdditional.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudentAdditional).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                                const arrval = String(feesVal1).split('.');

                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesIntStudentAdditional = Number(feesNumber);
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)


                            console.log("feesIntStudentAdditional -->", feesIntStudentAdditional);
                            //
                            // if (feeYear && feeYear.length > 0) {
                            //   courseTuitionFee.year = feeYear;
                            // }

                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                                const arrval = String(feesVal1).split('.');

                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,
                                                
                                                description: feesVal,
                                               
                                            });
                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,
                                               
                                                description: "",
                                               
                                            });
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: Number(demoarray.duration),
                                    unit: demoarray.unit,
                                   
                                    description: "",
                                   
                                });
                            }
                            if (feesIntStudent.length > 0) { // extract only digits
                                let arr = [];
                                if (feesIntStudentAdditional == '' || feesIntStudentAdditional == undefined) {

                                } else {
                                    feesIntStudentAdditional = "External/Online - AUD " + feesIntStudentAdditional;
                                    arr.push(feesIntStudentAdditional);
                                    feesIntStudent = "On-campus - " + feesIntStudent;
                                    arr.push(feesIntStudent);
                                }

                               // feesDict.international_student_all_fees = arr;
                            } else {
                                let arr = [];

                               // feesDict.international_student_all_fees = arr;
                            }
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    // feesList.push(feesDict);
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                console.log("feesDict.international_student = ", feesDict.international_student);
                                console.log("resJsonData.course_tuition_fee_amount = ", resJsonData.course_tuition_fee_amount);
                            }

                            break;
                        }


                        case 'program_code': {
                            resJsonData.program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            break;
                        }
                        case 'course_intake': {
                            var courseIntake = [];
                            const courseIntakeStr = await utils.getValueFromHardCodedJsonFile('course_intake');
                            // let intake_date = [
                            //   {
                            //     key: "Semester 1 - 21 January,2019",
                            //     value: "2019-01-21"
                            //   },
                            //   {
                            //     key: "Semester 2 - 8 July,2019",
                            //     value: "2019-07-08"
                            //   }
                            // ]
                            //let intakeArray = courseIntakeStr.toString().split(",");

                            for (let temp of courseIntakeStr) {
                                temp = temp.trim();
                                courseIntake.push(temp);
                            }
                            var locationArray = resJsonData.course_campus_location;
                            console.log("locationArray", locationArray);

                            if (courseIntake && courseIntake.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let location of locationArray) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location.name,
                                        "value": courseIntake
                                    });
                                }
                                let formatIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "MOM", "");
                                var intakeUrl = [];
                                // const intake = await utils.getValueFromHardCodedJsonFile('intake_url');
                                // intakeUrl

                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                resJsonData.course_intake.more_details = intakeUrl + "";
                                resJsonData.course_intake.intake = formatIntake;
                                let valmonths11 = "";

                                formatIntake.forEach(element => {
                                    element.value.forEach(elementdate => {
                                        if (elementdate.actualdate == 'Semester 1 - 21 January,2019') {
                                            elementdate.filterdate = "2019-01-21"
                                        } else if (elementdate.actualdate == 'Semester 2 - 8 July,2019') {
                                            elementdate.filterdate = "2019-07-08"
                                        }


                                        //elementdate.push({filterdate:myintake_data["term 1_date"]})
                                        console.log("DATA--->", elementdate)

                                    })
                                });

                            }
                            break;
                        }

                       
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                      
                       
                        case 'course_country': {
                            resJsonData.course_country = await Course.extractValueFromScrappedElement(courseScrappedData.course_country)
                            break;
                        }
                        case 'course_overview': {
                            resJsonData.course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            let course_overview = resJsonData.course_overview;
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            //  const courseKeyVal = courseScrappedData.course_career_outcome;
                            resJsonData.course_career_outcome = await Course.extractValueFromScrappedElement(courseScrappedData.course_career_outcome);
                            let course_career_outcome = resJsonData.course_career_outcome;
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = [course_career_outcome]
                            } else {
                                course_career_outcome = [];
                            }


                            break;
                        }
                        case 'course_study_level': {
                            var studyLevfromTitle = splitCricos
                           
                            if(studyLevfromTitle.includes('in')){
                                studyLevfromTitle = studyLevfromTitle.split('in')[0].trim();
                            } 
                            if(studyLevfromTitle.includes('of')){
                                studyLevfromTitle = studyLevfromTitle.split('of')[0].trim();
                            } 
                            console.log("studyLevfromTitle--->",studyLevfromTitle);
                            resJsonData.course_study_level = studyLevfromTitle
                            console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;

            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                //     var intakes = resJsonData.course_intake.intake;
                //       var matchrec = [];
                //     for (let dintake of intakes) {
                //         if (location == dintake.name) {
                //             matchrec = dintake.value;
                //         }
                //     }
                // if (matchrec.length > 0) {
                //     NEWJSONSTRUCT.course_intake = matchrec[0];
                // }
                // else {
                //     NEWJSONSTRUCT.course_intake = "";
                // }


                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                       // NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                //resJsonData.basecourseid = location_wise_data.course_id;

                // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                //     if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                //         resJsonData.course_cricos_code[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_cricos_code[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                //     if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_intake.intake[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_intake.intake[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                //     if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                //     if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_campus_location[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_campus_location[i].iscurrent = false;
                //     }
                // }



                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
