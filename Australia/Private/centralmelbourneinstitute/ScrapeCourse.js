const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {

        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }

                        case 'course_discipline': {


                            const cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                          
                          
                            let DEESCIPLINE = await format_functions.getDISCIPLINE(cricos);

                            var DEESCIPLINE1 = DEESCIPLINE.split("-")[1].trim();
                            resJsonData.course_discipline = [DEESCIPLINE1]



                        }

                        case 'course_title': {

                            const coursetitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("splitdataa1--->",coursetitle);
                            var splitdata1 = coursetitle.slice(8).trim()
                            console.log("splitdataa--->",splitdata1);
                            var splitdata2  = splitdata1.split('CRICOS:')[0].trim()
                            console.log("CourseTitle1", JSON.stringify(splitdata2))
                            var ctitle = format_functions.titleCase(splitdata2).trim();
                            resJsonData.course_title = ctitle.trim();


                            break;
                        }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: courseUrl


                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        case 'application_fee': {
                            resJsonData.application_fee = "";
                        }
                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const ibtDict = {}; const caeDict = {}; const pbtDict = {};
                            let ieltsNumber = null;
                            var ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_req);
                          
                           
                          


                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppteDict = {}; const ppbtDict = {}; const pcaeDict = {};
                            var penglishList = [];
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "", pbtScore = "";
                            if (ielts_req) {
                                // ieltsScore = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ielts_req;
                                ieltsDict.min = 0;
                                ieltsDict.require = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));;
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);
                            }
                            // if (pbt_req) {
                            //     // ieltsScore = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                            //     pbtDict.name = 'toefl pbt';
                            //     pbtDict.description = ielts_req;
                            //     pbtDict.min = 310;
                            //     pbtDict.require = await utils.giveMeNumber(pbt_req.replace(/ /g, ' '));;
                            //     pbtDict.max = 677;
                            //     pbtDict.R = 0;
                            //     pbtDict.W = 0;
                            //     pbtDict.S = 0;
                            //     pbtDict.L = 0;
                            //     pbtDict.O = 0;
                            //     englishList.push(pbtDict);
                            // }
                            // if (ibt_req) {
                            //     // ieltsScore = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                            //     ibtDict.name = 'toefl ibt';
                            //     ibtDict.description = ielts_req;
                            //     ibtDict.min = 0;
                            //     ibtDict.require = await utils.giveMeNumber(ibt_req.replace(/ /g, ' '));;
                            //     ibtDict.max = 120;
                            //     ibtDict.R = 0;
                            //     ibtDict.W = 0;
                            //     ibtDict.S = 0;
                            //     ibtDict.L = 0;
                            //     ibtDict.O = 0;
                            //     englishList.push(ibtDict);
                            // }
                            // if (pte_req) {
                            //     // ieltsScore = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                            //     pteDict.name = 'pte academic';
                            //     pteDict.description = ielts_req;
                            //     pteDict.min = 0;
                            //     pteDict.require = await utils.giveMeNumber(pte_req.replace(/ /g, ' '));;
                            //     pteDict.max = 90;
                            //     pteDict.R = 0;
                            //     pteDict.W = 0;
                            //     pteDict.S = 0;
                            //     pteDict.L = 0;
                            //     pteDict.O = 0;
                            //     englishList.push(pteDict);
                            // }
                            // if (cae_req) {
                            //     // ieltsScore = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                            //     caeDict.name = 'cae';
                            //     caeDict.description = ielts_req;
                            //     caeDict.min = 80;
                            //     caeDict.require = await utils.giveMeNumber(cae_req1.replace(/ /g, ' '));;
                            //     caeDict.max = 230;
                            //     caeDict.R = 0;
                            //     caeDict.W = 0;
                            //     caeDict.S = 0;
                            //     caeDict.L = 0;
                            //     caeDict.O = 0;
                            //     englishList.push(caeDict);
                            // }
                            // if (ieltsScore != []) {
                            //     pieltsDict.name = 'ielts academic';
                            //     var value = {};
                            //     value.min = 0;
                            //     value.require = ieltsScore;
                            //     value.max = 9;
                            //     pieltsDict.value = value;
                            //     penglishList.push(pieltsDict);
                            // }
                            // if (pbtScore != []) {
                            //     ppbtDict.name = 'toefl pbt';
                            //     var value = {};
                            //     value.min = 310;
                            //     value.require = pbtScore;
                            //     value.max = 677;
                            //     pieltsDict.value = value;
                            //     penglishList.push(ppbtDict);
                            // }
                            // if (ibtScore != []) {
                            //     pibtDict.name = 'toefl ibt';
                            //     var value = {};
                            //     value.min = 0;
                            //     value.require = ibtScore;
                            //     value.max = 120;
                            //     pibtDict.value = value;
                            //     penglishList.push(pibtDict);
                            // }
                            // if (pteScore != []) {
                            //     ppteDict.name = 'pte academic';
                            //     var value = {};
                            //     value.min = 0;
                            //     value.require = pteScore;
                            //     value.max = 90;
                            //     pieltsDict.value = value;
                            //     penglishList.push(ppteDict);
                            // }
                            // if (caeScore != []) {
                            //     pcaeDict.name = 'cae';
                            //     var value = {};
                            //     value.min = 80;
                            //     value.require = caeScore;
                            //     value.max = 230;
                            //     pcaeDict.value = value;
                            //     penglishList.push(caeDict);
                            // }
                            // if (penglishList && penglishList.length > 0) {
                            //     courseAdminReq.englishprogress = penglishList;
                            // }
                            // else {
                            //     courseAdminReq.englishprogress = [];
                            // }
                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }
                            else {
                                courseAdminReq.english = [];
                            }

                            //var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var entry_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_entry_more_details);
                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            var arr = [];
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            // console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            academicReq = selList;
                                            //}
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = (academicReq) ? academicReq.toString() : [];


                            courseAdminReq.english_requirements_url = (english_more) ? english_more.toString() : "";
                            courseAdminReq.entry_requirements_url = (entry_more) ? entry_more.toString() : "";
                            courseAdminReq.academic_requirements_url = (academic_more) ? academic_more.toString() : "";

                            //courseAdminReq.academic_more_details = academic_more;
                            resJsonData.course_admission_requirement = courseAdminReq;
                            // arr.push(academicReq)
                            if (academicReq && String(academicReq).length > 0) {
                                // courseAdminReq.academic = arr;
                                //arr.push(academicReq)
                                courseAdminReq.academic = academicReq;
                            }
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = "", courseDurationList = {};
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                           console.log("ssfsfsfsfsfs--->",courseKeyVal);
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList course_duration_full_time= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = String(selList[0]).trim();
                                               
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime = resFulltime;
                                    courseDurationList = durationFullTime;
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = courseDurationList;

                                    } else {
                                        resJsonData.course_duration;
                                    }

                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {

                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }

                        case 'course_tuition_fee':

                        case 'page_url':
                        case 'course_study_mode': { // Location Launceston
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston                  
                            console.log('course_campus_location matched case: ' + key);
                            let concatnatedSelectorsStr = [];
                            let courseIntake = [];
                            const rootElementDictList = courseScrappedData[key];
                            var myintake_data = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                            const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('location');
                            console.log("ficedfees--->",fixedFeesDict);
                            const fixedFeesDict1 =  [fixedFeesDict]
                     
                            for (let temp of fixedFeesDict1) {
                                temp = temp.trim();
                                courseIntake.push(temp);
                            }
                            let newcampus = [];
                           
                           var crickcode =   await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code)
                           
                          
                           if (courseIntake && courseIntake.length > 0) {
                            //resJsonData.course_intake = {};
                             resJsonData.course_campus_location = [];
                            for (let location of courseIntake) {
                                resJsonData.course_campus_location.push({
                                    "name": location,
                                    "code": crickcode

                                });
                                console.log("resJsonData.course_intake.intake", resJsonData.course_campus_location);

                            }
                            break;
                        }
                        }
                        // case 'course_campus_location': { // Location Launceston
                        //     var campLocationText = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                        //     var campLocationText1 = []
                        //     const courseKeyVal2 = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                        //     const cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                        //     var crick = cricos.split('CRICOS CODE:')[1].trim();
                        //     console.log("crickcrick--->",crick);
                        //     if(crick.includes('Course Duration:'))
                        //     {
                        //         var crick1 = crick.split('Course Duration:')[0].trim()
                        //         crick = crick1
                        //     }
                        //     if (courseKeyVal2.includes('Thornbury')) {
                        //         var courseKeyVal = 'Thornbury';
                        //         campLocationText1.push(courseKeyVal)



                        //     }
                        //     if (courseKeyVal2.includes('Queen Street')) {
                        //         console.log("enter---->")
                        //         var courseKeyVal3 = 'Melbourne';
                        //         campLocationText1.push(courseKeyVal3)
                        //     }
                        //     campLocationText = campLocationText1
                        //     console.log("##Campus7-->" + JSON.stringify(campLocationText))
                        //     if (campLocationText && campLocationText.length > 0) {
                        //         for (let camp of campLocationText) {


                        //         }
                        //         //  var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                        //         //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                        //         // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                        //         var campusedata = [];
                        //         campLocationText.forEach(element => {
                        //             campusedata.push({
                        //                 "name": String(element).trim(),
                        //                 "code": crick
                        //             })
                        //         });
                        //         resJsonData.course_campus_location = campusedata;
                        //         console.log("resJsonData.course_campus_location--->>>>>", resJsonData.course_campus_location);
                        //         //   resJsonData.course_campus_location = campLocationText;//await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                        //         // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //

                        //         //  campLocationValTrimmed = campLocationValTrimmed.toLowerCase();


                        //         //.join(',');
                        //         // console.log("## FInal string-->" + campLocationValTrimmed);
                        //         // }
                        //     } // if (campLocationText && campLocationText.length > 0)
                        //     break;
                        // }

                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            var Tuition_Fee = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                          
                          
                            let DESCIPLINE1 = await format_functions.getFees(Tuition_Fee);

                           // var DESCIPLINE1 = DiSCIPLINE.split("-")[1];


                          //  console.log("DESCIPLINE1---->",DESCIPLINE1);

                            // var Tuition_Fee = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            // var Tuition_Fee1 = Tuition_Fee.split('FEES- Tuition:')[1].trim()
                            // Tuition_Fee =  Tuition_Fee1.split('Materials:')[0].trim()
                            // console.log("Course_Name_title------->>>>", Tuition_Fee);
                           

                            // let morefees = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_more);
                            // console.log("morefees_val -->", morefees);
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (DESCIPLINE1 && DESCIPLINE1.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(DESCIPLINE1).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,
                                                description: feesNumber,
                                            });
                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,
                                                description: "not available fee",
                                            });
                                        }
                                        console.log("feesDictinternational_student-->", feesDict.international_student);
                                    }
                                }
                            } else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: Number(demoarray.duration),
                                    unit: demoarray.unit,
                                    description: "not available fee",
                                });
                            }
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = Number(inetStudentFees);
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if

                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }

                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                console.log("hiiii---->")
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    console.log("hello there--->", campus);
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                }
                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }
                            break;
                        }


                        case 'program_code': {
                            var program_code = await await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                           var program_code1 = program_code.slice(0,8);
                           console.log("program_code1",program_code1);


                            resJsonData.program_code = program_code1;

                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            // Semester 1 (February) and Semester 2 (August)
                            // prepare emty JSON struct
                            const courseIntakeDisplay = {};
                          
                            // existing intake value
                            var courseIntakeStr = [];//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;
                           
                           
                          //  if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var intakes = [];
                                console.log("CALLED resJsonData.course_campus_location after intake");
                                var campus = resJsonData.course_campus_location;
                                for (let loc of campus) {
                                    var intakedetail = {};

                                    intakedetail.name = loc.name;
                                    intakedetail.value = [];
                                    intakes.push(intakedetail);
                                }

                              //  let formatIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                var intakedata = {};
                                var intakeUrl = await utils.getValueFromHardCodedJsonFile('intake_url');
                                console.log("moredetails--->")
                                intakedata.intake = intakes;
                                intakedata.more_details = intakeUrl;
                                // intakedata.more_details = resJsonData.course_intake_url;
                               

                                resJsonData.course_intake = intakedata;
                         //      }
                            break;
                        }
                        

                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }


                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        // case 'course_overview':
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                console.log("Outcome is--->" + JSON.stringify(selList))
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }
                        case 'course_study_level':
                            {
                                const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code)
                               
                              

                                let cStudyLevel = await format_functions.getMyStudyLevel(cTitle);
                                console.log("course_study_level--------", cStudyLevel);
                                resJsonData.course_study_level = cStudyLevel
                            }


                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            // console.log('\n\r' + funcName + 'course resJsonData =' + JSON.stringify(resJsonData));
            // var resJsonData = JSON.parse(fs.readFileSync("australiancatholicuniversity_graduatediplomainrehabilitation_coursedetails.json"))[0];
            var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(':', '').replace('**', '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                //  console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.courseKeyVal));
                //  console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                // NEWJSONSTRUCT.course_campus_location = location;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;

                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                // var intakes = resJsonData.course_intake.intake;


                var matchrec = [];
                // for (let dintake of intakes) {
                //     if (location == dintake.name) {
                //         matchrec = dintake.value;
                //     }
                // }
                // if (matchrec.length > 0) {
                //     NEWJSONSTRUCT.course_intake = matchrec[0];
                // }
                // else {
                //     NEWJSONSTRUCT.course_intake = [];
                // }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        //  NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                    // else
                    // {
                    //     NEWJSONSTRUCT.international_student_all_fees = [];
                    // }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                //resJsonData.course_cricos_code = location_wise_data.cricos_code;
                console.log("location::::" + location_wise_data.course_campus_location);
                // resJsonData.current_course_campus_location = location_wise_data.course_campus_location;
                //resJsonData.current_course_intake = location_wise_data.course_intake;
                // resJsonData.course_intake_display = location_wise_data.course_intake;
                //   resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;


                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
                // const res = await awsUtil.putFileInBucket(filelocation);
                //console.log(funcName + 'S3 object location = ' + res);
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
