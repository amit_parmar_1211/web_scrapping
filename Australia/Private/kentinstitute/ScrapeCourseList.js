const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to https://kent.edu.au/kent3/courses/
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
      var datalist = [];
      // await s.setupNewBrowserPage("https://kent.edu.au/kent3/courses/");
      var mainCategory1 = [], redirecturl1 = [], redirecturl2 = [], redirecturl3 = [];
      //scrape main category
      const maincategoryselector1 = "//*//a[@id='back_to_top']/following-sibling::div//h5[1]";
      const maincategoryselector2 = "//*//a[@id='back_to_top']/following-sibling::div//h5[2]";
      var category1 = await page.$x(maincategoryselector1);
      var category2 = await page.$x(maincategoryselector2);
      console.log("Total categories-->" + category1.length);

      for (let i = 0; i < category1.length; i++) {
        var categorystringmain1 = await page.evaluate(el => el.innerText, category1[i]);
        mainCategory1.push(categorystringmain1.trim());
        redirecturl1.push({ innerText: categorystringmain1.trim() });
      }
      for (let i = 0; i < category2.length; i++) {
        var categorystringmain2 = await page.evaluate(el => el.innerText, category2[i]);
        mainCategory1.push(categorystringmain2.trim());
        redirecturl2.push({ innerText: categorystringmain2.trim() });
      }
      //scrape courses
      for (let catc1 of redirecturl1) {
        const selector1 = "//*//a[@id='back_to_top']/following-sibling::div//h5[1]/following-sibling::ul[1]//a";
        var title1 = await page.$x(selector1);
        for (let t1 of title1) {
          var categorystring1 = await page.evaluate(el => el.innerText, t1);
          var categoryurl1 = await page.evaluate(el => el.href, t1);
          console.log("categoryurl------->>>", categoryurl1);
          datalist.push({ href: categoryurl1, innerText: categorystring1.replace(/[\r\n\t]+/g, ' ').trim(), category: catc1.innerText });
        }
      }
      for (let catc2 of redirecturl2) {
        const selector2 = "//*//a[@id='back_to_top']/following-sibling::div//h5[1]/following-sibling::ul[2]//a";
        var title2 = await page.$x(selector2);
        for (let t2 of title2) {
          var categorystring2 = await page.evaluate(el => el.innerText, t2);
          var categoryurl2 = await page.evaluate(el => el.href, t2);
          console.log("categoryurl------->>>", categoryurl2);
          datalist.push({ href: categoryurl2, innerText: categorystring2.replace(/[\r\n\t]+/g, ' ').trim(), category: catc2.innerText });
        }
      }

      for (let catc3 of redirecturl1) {
        const selector3 = "//*//a[@id='back_to_top']/following-sibling::div//h4[1]/following-sibling::ul[1]//a";
        var title3 = await page.$x(selector3);
        for (let t3 of title3) {
          var categorystring3 = await page.evaluate(el => el.innerText, t3);
          if (categorystring3.includes('Bachelor of Business')) {
            let hardcode = 'Business Courses'
            catc3 = hardcode;
            console.log("catc3----->>>", catc3);
          }
          if (categorystring3.includes('Bachelor of Accounting')) {
            let hardcode = 'Business Courses'
            catc3 = hardcode;
          }
          if (categorystring3.includes('Bachelor of Information Technology')) {
            let hardcode = 'Information Technology Courses'
            catc3 = hardcode;
          }
          var categoryurl3 = await page.evaluate(el => el.href, t3);
          console.log("categoryurl------->>>", categoryurl3);
          datalist.push({ href: categoryurl3, innerText: categorystring3.replace(/[\r\n\t]+/g, ' ').trim(), category: catc3 });
        }
      }
      await fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory1))
      await fs.writeFileSync("./output/kentinstitute_original_courselist.json", JSON.stringify(datalist));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < datalist.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (datalist[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [] });
        }
      }
      await fs.writeFileSync("./output/kentinstitute_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < datalist.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == datalist[i].href) {
            uniqueUrl[j].category.push(datalist[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/kentinstitute_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }

  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }


  // refers to https://programsandcourses.anu.edu.au/catalogue
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // course_level_form_selector key
      // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelList) {
      //   console.log(funcName + 'Invalid formSelList');
      //   throw (new Error('Invalid formSelList'));
      // }
      // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelElementDict) {
      //   console.log(funcName + 'Invalid formSelElementDict');
      //   throw (new Error('Invalid formSelElementDict'));
      // }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
