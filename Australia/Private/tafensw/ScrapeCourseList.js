const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'startScrappingFunc ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var datalist = [];
      await s.setupNewBrowserPage("https://www.tafensw.edu.au/international");
      var mainCategory = [], redirecturl = [];
      //scrape main category
      const maincategoryselector = "//*[@id='Subject']/option[@value!=0]";
      var category = await s.page.$x(maincategoryselector);
      console.log("Total categories-->" + category.length);
      for (let link of category) {
        var categorystringmain = await s.page.evaluate(el => el.innerText, link);
        var categorystringmainurl = await s.page.evaluate(el => el.value, link);
        mainCategory.push(categorystringmain.trim());
        redirecturl.push({ innerText: categorystringmain.trim(), value: categorystringmainurl });
      }

      //scrape courses
      for (let catc of redirecturl) {
        await s.page.select("#Subject", catc.value);
        const button = "//*[@id='course-search-btn']";
        var buttons = await s.page.$x(button);
        await buttons[0].click();
        await s.page.waitForNavigation({ timeout: 0 });

        const titleselector = "//*[@id='all']/div/div[1]/div/h3[1]/a";
        var title = await s.page.$x(titleselector);
        for (let i = 0; i < title.length; i++) {
          var categorystring = await s.page.evaluate(el => el.innerText, title[i]);
          var categoryurl = await s.page.evaluate(el => el.href, title[i]);
          datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: catc.innerText });
        }
      }
      var uniquecases = [], uniquedata = [], finalDict = [];
      datalist.forEach(element => {
        if (!uniquecases.includes(element.href)) {
          uniquecases.push(element.href);
          uniquedata.push(element);
        }
      });
      uniquecases.forEach(element => {
        var dataobj = {};
        var data = datalist.filter(e => e.href == element);
        var category = [];
        data.forEach(element => {
          if (!category.includes(element.category))
            category.push(element.category);
        });
        dataobj.href = element;
        dataobj.innerText = data[0].innerText;
        dataobj.category = (Array.isArray(category)) ? category : [category];
        finalDict.push(dataobj);
      });
      fs.writeFileSync("./output/tafensw-all_courselist.json", JSON.stringify(finalDict));
      fs.writeFileSync("./output/tafensw_courselist.json", JSON.stringify(finalDict));
      fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory));
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }


} // class

module.exports = { ScrapeCourseList };
