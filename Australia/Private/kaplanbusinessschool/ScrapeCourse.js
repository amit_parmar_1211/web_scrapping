const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];

            var course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
            console.log("course_title---->>>", course_title);
            let course_title_text = format_functions.titleCase(course_title.replace('\n', '').replace('MBA –', '').trim());;
            resJsonData.course_title = course_title_text;
            console.log("course_name-------->>>>", course_name);
            const intakefees = JSON.parse(fs.readFileSync(appConfigs.fees_kaplan));
            console.log("intakefees-------->>>>", intakefees);
            let tmpvar = intakefees.filter(val => {
                return val.Course_Name.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase() == course_title_text.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase()
            });
            console.log("tmpvar------->>>>", tmpvar);
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log("resJsonData.sssss------>>>>>>", cTitle)
                            const category_val = await format_functions.getMyCategory(cTitle.replace('.', '').trim());
                            console.log("category_val------>>>>>>", category_val);
                            resJsonData.course_discipline = [category_val.split('-')[1].trim()];
                            console.log("resJsonData.course_discipline------>>>>>>", resJsonData.course_discipline);
                            break;
                        }
                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; let ieltsNumber = null;
                            let courseKeyVal_ielts = courseScrappedData.ielts_req;
                            let ielts_req = ""
                            let title_ielts = resJsonData.course_title;
                            var myscrore = await utils.getMappingScore(title_ielts, resJsonData.course_study_level);
                            console.log("intakefees-------->>>>", JSON.stringify(myscrore.IELTS));

                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            var penglishList = [];
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                            if (myscrore.IELTS) {
                                ieltsScore = await utils.giveMeNumber(myscrore.IELTS.replace(/ /g, ' '));
                            }
                            if (myscrore.PTE) {
                                pteScore = await utils.giveMeNumber(myscrore.PTE.replace(/ /g, ' '));
                            }
                            if (myscrore.TOEFL) {
                                ibtScore = await utils.giveMeNumber(myscrore.TOEFL.replace(/ /g, ' '));
                                console.log("ibtScore-------->>>>", ibtScore);
                            }
                            if (myscrore.CAE) {
                                caeScore = await utils.giveMeNumber(myscrore.CAE.replace(/ /g, ' '));
                            }
                            if (ieltsScore) {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = myscrore.IELTS;
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsScore;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }
                            if (pteScore) {
                                ppteDict.name = 'pte academic';
                                ppteDict.description = myscrore.PTE;
                                ppteDict.min = 0;
                                ppteDict.require = pteScore;
                                ppteDict.max = 90;
                                ppteDict.R = 0;
                                ppteDict.W = 0;
                                ppteDict.S = 0;
                                ppteDict.L = 0;
                                ppteDict.O = 0;
                                penglishList.push(ppteDict);
                            }
                            if (ibtScore) {
                                pibtDict.name = 'toefl ibt';
                                pibtDict.description = myscrore.TOEFL;
                                pibtDict.min = 0;
                                pibtDict.require = ibtScore;
                                pibtDict.max = 120;
                                pibtDict.R = 0;
                                pibtDict.W = 0;
                                pibtDict.S = 0;
                                pibtDict.L = 0;
                                pibtDict.O = 0;
                                penglishList.push(pibtDict);
                            }
                            if (caeScore) {
                                pcaeDict.name = 'cae';
                                pcaeDict.description = myscrore.CAE;
                                pcaeDict.min = 80;
                                pcaeDict.require = caeScore;
                                pcaeDict.max = 230;
                                pcaeDict.R = 0;
                                pcaeDict.W = 0;
                                pcaeDict.S = 0;
                                pcaeDict.L = 0;
                                pcaeDict.O = 0;
                                penglishList.push(pcaeDict);
                            }
                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            } else {
                                throw new Error("english not found");
                            }
                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var academicReq = "";
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList;
                                        }
                                    }
                                }
                            }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            courseAdminReq.entry_requirements_url = entry_requirements_url;
                            courseAdminReq.english_requirements_url = "";
                            courseAdminReq.academic_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [], resFulltime = [];
                            var fullTimeText = "";
                            let spl;
                            // const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time);
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            console.log("SplcourseKeyVal----->>>>>>", JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList;
                                                console.log("##fullTimeText--->" + JSON.stringify(fullTimeText));
                                                var durations;
                                                for (let duration of fullTimeText) {
                                                    if ((duration.includes("/")) | (duration.includes(","))) {
                                                        spl = duration.split("/");
                                                        spl.forEach(element => {
                                                            if ((element.toLowerCase().includes('years')) ||
                                                                (element.toLowerCase().includes('months')) ||
                                                                (element.toLowerCase().includes('year')) ||
                                                                (element.toLowerCase().includes('month'))) {
                                                                if (element.toLowerCase().includes(',')) {
                                                                    durations = element.split(',')[0];
                                                                } else {
                                                                    durations = element;
                                                                }
                                                            }
                                                        })
                                                        console.log("Spl@@@@@@@@@@@@@@", durations);
                                                    }
                                                    resFulltime.push(durations);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (resFulltime && resFulltime.length > 0) {

                                if (resFulltime) {
                                    // durationFullTime.duration_full_time = String(fullTimeText).trim();
                                    // courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    // if (courseDurationList && courseDurationList.length > 0) {
                                    resJsonData.course_duration = String(resFulltime);
                                    /// }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': {
                            var campusedata = [];
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            console.log("courseKeyVal------>>>>", courseKeyVal)
                            const locationsss = await utils.getValueFromHardCodedJsonFile('locations');
                            console.log("locationsss####", locationsss);
                            console.log("courseKeyValcourseKeyVal------>>>>", courseKeyVal)
                            let loca = courseKeyVal.split('/');
                            let newcampus = [];
                            for (let campus of loca) {
                                if (!newcampus.includes(campus)) {
                                    newcampus.push(campus);
                                    console.log("##Campuscampus-->" + campus)
                                }
                            }
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log("resJsonData.sssss------>>>>>>", cTitle)
                            const category_val = cTitle.toString().replace("COURSE CRICOS CODE", '').replace('.', '').trim()
                            console.log("category_val------>>>>>>", category_val);
                            if (newcampus && newcampus.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                resJsonData.course_study_mode = "On campus"
                                console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                var campusedata = [];
                                newcampus.forEach(element => {
                                    campusedata.push({
                                        "name": format_functions.titleCase(String(element).trim()),
                                        "code": category_val.toString()
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                            }
                            break;
                        }
                        case 'course_study_mode': { // Location Launceston
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            console.log("demoarray123 -->", demoarray);
                            console.log("programcode------->>>>", tmpvar[0].Tuition_Fee);
                            const Tuition_Fee = tmpvar[0].Tuition_Fee
                            console.log("Course_Name_title------->>>>", Tuition_Fee);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feeYear && feeYear.length > 0) {
                                courseTuitionFee.year = feeYear;
                            }

                            let morefees = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_more);
                            console.log("morefees_val -->", morefees);
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (Tuition_Fee && Tuition_Fee.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(Tuition_Fee).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,
                                                description: morefees,
                                            });
                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,
                                                description: "not available fee",
                                            });
                                        }
                                        console.log("feesDictinternational_student-->", feesDict.international_student);
                                    }
                                }
                            } else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: Number(demoarray.duration),
                                    unit: demoarray.unit,
                                    description: "not available fee",
                                });
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = Number(inetStudentFees);
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if

                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                //  feesDict.international_student_all_fees = [];
                                // if (international_student_all_fees_array && international_student_all_fees_array.length > 0) {
                                //     for (let student_all_fees of international_student_all_fees_array) {
                                //         feesDict.international_student_all_fees.push(student_all_fees.replace(/[\r\n\t ]+/g, ' ').replace('-', '').trim());
                                //     }
                                // }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                }
                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }
                            break;
                        }

                        case 'program_code': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            // var program_code = []
                            // console.log("program_code-->" + JSON.stringify(courseKeyVal));
                            // if (Array.isArray(courseKeyVal)) {
                            //     for (const rootEleDict of courseKeyVal) {
                            //         console.log(funcName + '\n\r program_code = ' + JSON.stringify(rootEleDict));
                            //         const elementsList = rootEleDict.elements;
                            //         for (const eleDict of elementsList) {
                            //             console.log(funcName + '\n\r program_code = ' + JSON.stringify(eleDict));
                            //             const selectorsList = eleDict.selectors;
                            //             for (const selList of selectorsList) {
                            //                 console.log(funcName + '\n\r program_code = ' + JSON.stringify(selList));
                            //                 if (Array.isArray(selList) && selList.length > 0) {
                            //                     program_code = selList;
                            //                 }
                            //             }
                            //         }
                            //     }
                            // }
                            console.log("program_codeprogram_code-->" + JSON.stringify(courseKeyVal));
                            // if (courseKeyVal.length > 0) {
                            //     console.log("program_codeprogram_codeprogram_code-->" + JSON.stringify(courseKeyVal));
                            //     resJsonData.program_code = courseKeyVal;
                            // } else {
                            resJsonData.program_code = "";
                            //   }
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            const courseIntakeDisplay = [];
                            // existing intake value
                            // const courseIntakeStrElem = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);

                            let courseKeyVal_intake = courseScrappedData.course_intake;
                            let courseintake;
                            if (Array.isArray(courseKeyVal_intake)) {
                                for (const rootEleDict of courseKeyVal_intake) {
                                    console.log(funcName + '\n\r rootEleDict ieltsDict= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict ieltsDict= ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList ieltsDict= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList)) {
                                                for (let sel of selList) {
                                                    if (courseintake != sel) {
                                                        courseintake = sel;
                                                        if (courseintake.includes('+')) {
                                                            courseintake = courseintake.split('+');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("R locationArray", campus);
                            console.log('************************Start course_intake******************************');
                            console.log('courseIntakeStr : ' + courseintake);
                            //console.log("R courseIntakeStrFile", courseIntakeStrFile);
                            // const intakeSplit =[];
                            if (courseintake && courseintake.length > 0) {
                                resJsonData.course_intake = courseintake;
                                //resJsonData.course_intake_display = courseIntakeStr;
                                courseintake = String(courseintake).split('\n');
                                courseintake = String(courseintake).split(',');
                                //   courseintake = String(courseintake).split('or');
                                console.log('course_intake intakeStrList = ' + JSON.stringify(courseintake));
                                //const regEx = /[ ]/g; var semList = [];
                                if (courseintake && courseintake.length > 0) {
                                    for (var part of courseintake) {
                                        // part = part.split("(");
                                        // part = part[1].split(")")[0];
                                        console.log("R part", part);
                                        part = part.replace('.', '').replace('Enrol in', '').trim();
                                        courseIntakeDisplay.push(part);
                                    } // for
                                }
                                console.log('course_intake courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));
                            }
                            var campus = resJsonData.course_campus_location;
                            if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let location11 of campus) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location11.name,
                                        "value": courseIntakeDisplay
                                    });
                                }
                            }
                            console.log("intakes123 -->", resJsonData.course_intake.intake);
                            var intakedata = {};
                            let formatedIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");
                            console.log("NEw Intake Format@@@@@" + JSON.stringify(formatedIntake))
                            intakedata.intake = formatedIntake;
                            var intakeUrl = [];
                            intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                            intakedata.more_details = intakeUrl;
                            resJsonData.course_intake = intakedata
                            break;
                        }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));

                                            console.log("sel----->", selList);
                                            course_career_outcome = selList

                                        }
                                    }
                                }
                            }
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome
                            } else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: resJsonData.course_url
                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);

                                resJsonData.application_fee = application_fee;
                                break;
                            }
                        case 'course_study_level': {
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log("resJsonData.sssss------>>>>>>", cTitle)
                            const study_val = await format_functions.getMyStudyLevel(cTitle.trim());
                            resJsonData.course_study_level = study_val;
                            console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                // NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;

                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
