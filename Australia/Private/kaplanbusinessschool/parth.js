const puppeteer = require('puppeteer');
const fs = require('fs');
async function scrapeEpisodeLinks(root, filename) {
    const browser = await puppeteer.launch({ headless: true });

    const page = await browser.newPage();
    await page.goto('https://migration.qld.gov.au/skilled-occupation-lists/');
    let datastr = "<html><body><table border=1>"
    const count = await page.$x("//*[@id='" + root + "']/table/tbody/tr/td[1]");
    for (let a = 2; a < count.length; a++) {
        const list1code = "//*[@id='" + root + "']/table/tbody/tr[" + a + "]/td[1]";
        const list1occ = "//*[@id='" + root + "']/table/tbody/tr[" + a + "]/td[2]";
        const listt491y = "//*[@id='" + root + "']/table/tbody/tr[" + a + "]/td[3]/span[@class='fa fa-check']";
        const list1190y = "//*[@id='" + root + "']/table/tbody/tr[" + a + "]/td[4]/span[@class='fa fa-check']";
        const list1sr = "//*[@id='" + root + "']/table/tbody/tr[" + a + "]/td[5]";

        const list1codeelm = await page.$x(list1code);
        const list1occelm = await page.$x(list1occ);
        const listt491elmy = await page.$x(listt491y);
        const list1190elmy = await page.$x(list1190y);
        const list1srelm = await page.$x(list1sr);

        const y491 = (listt491elmy.length > 0) ? 'true' : 'false';
        const y190 = (list1190elmy.length > 0) ? 'true' : 'false';

        datastr += "<tr><td>" + await page.evaluate(el => el.innerText, list1codeelm[0]) + "</td>";
        datastr += "<td>" + await page.evaluate(el => el.innerText, list1occelm[0]) + "</td>";
        datastr += "<td>" + y491 + "</td>";
        datastr += "<td>" + y190 + "</td>";
        datastr += "<td>" + await page.evaluate(el => el.innerText, list1srelm[0]) + "</td></tr>";
    }
    fs.writeFileSync(filename, datastr)
    browser.close();
}
scrapeEpisodeLinks('accordion-item-2', './first.html');
scrapeEpisodeLinks('accordion-item-3', './second.html');
scrapeEpisodeLinks('accordion-item-4', './third.html');