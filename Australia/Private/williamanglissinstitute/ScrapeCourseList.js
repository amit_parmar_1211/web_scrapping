
const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeCourseList() {
    const funcName = 'startScrappingFunc ';
    try {
      let s = new Scrape();
      await s.init({ headless: true });
      var datalist = [];
      let bookingUrl="https://www.angliss.edu.au/courses/"
      await s.setupNewBrowserPage(bookingUrl);
  
      var mainCategory = [], redirecturl = [];
      //scrape main category
      await s.page.waitFor(1000);
      const studentselection = "//*/span[contains(text(),'Are you an international or local student?') and @class='desktop']"
      const rdiobtnstd ="//*/label[text()='International Student']/."
      const citystd="//*/label[text()='Melbourne']/."
      const btntd ="//input[@class='btn-submit btn-skin-1']"

      var stsel= await s.page.$x(studentselection);
      await stsel[0].click();
      await s.page.waitFor(500);
      var interst = await s.page.$x(rdiobtnstd);
      await interst[0].click();
      await s.page.waitFor(500);
      var citrd = await s.page.$x(citystd);
      await citrd[0].click();
      await s.page.waitFor(700);
      var  svbtn = await s.page.$x(btntd);
      await svbtn[0].click();
      await s.page.waitFor(5000);
      
      let url=["https://www.angliss.edu.au/courses/foods/",
      "https://www.angliss.edu.au/courses/tourism/",
  "https://www.angliss.edu.au/courses/hospitality/",
"https://www.angliss.edu.au/courses/hotel-management/",
"https://www.angliss.edu.au/courses/events/"]
var datalist = [];
//await s.page.goto(bookingUrl,{timeout:0});  
var mainCategory = [], redirecturl = [];
//scrape main category

      for (let urls of url){
      await s.page.goto(urls,{timeout:0});
      const maincategoryurl=await s.page.$x("//*[@class='s-tile-pile']/div/div/a")
      const maincategorytext=await s.page.$x("//*[@class='s-tile-pile']/div/div/a/div[2]/h3")

    for(let i=0;i<maincategoryurl.length;i++){
    const url=await s.page.evaluate(el=>el.href,maincategoryurl[i])
    const innertext=await s.page.evaluate(el=>el.innerText,maincategorytext[i])
    mainCategory.push({href:url,innertext:innertext})
    }

    }

      for(let category of mainCategory ){
        await s.page.goto(category.href,{timeout:0})
        const courseurl=await s.page.$x("//*[@class='s-tile-pile']/div/div/a")
        const coursetext=await s.page.$x("//*[@class='s-tile-pile']/div/div/a/div[2]/h3")
        for(let i=0;i<courseurl.length;i++){
      const url=await s.page.evaluate(el=>el.href,courseurl[i])
      const innertext=await s.page.evaluate(el=>el.innerText,coursetext[i])
        datalist.push({href:url,innerText:innertext,category:category.innertext})
 
      }
}

      fs.writeFileSync("./output/williamanglissinstitute_courselist.json", JSON.stringify(datalist))
      fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
}



 module.exports = { ScrapeCourseList };