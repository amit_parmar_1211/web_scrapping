const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = category_name;
                            break;
                        }
                        case 'course_title_category':
                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            if (title.includes(' (')) {
                              const title_text = title.replace('(', ' (');
                              console.log("splitStr@@@2" + title_text);
                              title = title_text;
                              console.log("splitStr@@@1" + title);
                      
                            }
                            var ctitle = format_functions.titleCase(title).trim();
                           // var ctitle1 = ctitle.replace('(','').replace(')','').trim();
                          var ctitle2 = ctitle.replace(' (','(');
                          var ctitle3 = ctitle2.replace(' (','(').trim();
                            console.log("ctitle@@@", ctitle3.trim());
                            resJsonData.course_title = ctitle3
                            break;
                        }
                        case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            let ieltsNumber = null, pteNumber = null, ibtNumber = null, caeNumber = null;
                            let myscore = '';
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const pbtDict = {};
                            // console.log("resJsonData.course_toefl_ielts_score -->", JSON.stringify(courseScrappedData.course_toefl_ielts_score));
                            console.log("resJsonData.course_study_level --> ", resJsonData.course_study_level);
                            // let studyLevel = resJsonData.course_study_level;
                            let studyLevel = [];
                            if (resJsonData.course_study_level.toLowerCase().includes('certificate')) {
                                studyLevel = resJsonData.course_study_level.split(' ');
                                myscore = await utils.getMappingScore(studyLevel[0]);
                            } else {
                                myscore = await utils.getMappingScore(resJsonData.course_study_level);
                            }

                            console.log("myscore : ", myscore);
                            let ieltsSplitString = myscore.ielts;
                            console.log("ieltsSplitString -->", ieltsSplitString);
                            if (ieltsSplitString && ieltsSplitString.length > 0) {
                                // extract exact number from string
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                const matchedStrList = String(ieltsSplitString).match(regEx);
                                console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    ieltsNumber = Number(matchedStrList[0]);
                                    console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                }
                            }


                            var pbt_req = '';
                            var ibt_req = '';
                            var pte_req = '';
                            var cae_req = '';
                            if (myscore.ibt && myscore.ibt.length > 0) {
                                // extract exact number from string
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                const matchedStrList = String(myscore.ibt).match(regEx);
                                console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    ibt_req = Number(matchedStrList[0]);
                                    console.log(funcName + 'ibtNumber = ' + ibt_req);
                                }
                            }
                            if (myscore.pbt && myscore.pbt.length > 0) {
                                // extract exact number from string
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                const matchedStrList = String(myscore.pbt).match(regEx);
                                console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    pbt_req = Number(matchedStrList[0]);
                                    console.log(funcName + 'pbtNumber = ' + pbt_req);
                                }
                            }
                            if (myscore.pte && myscore.pte.length > 0) {
                                // extract exact number from string
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                const matchedStrList = String(myscore.pte).match(regEx);
                                console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    pte_req = Number(matchedStrList[0]);
                                    console.log(funcName + 'pteNumber = ' + pte_req);
                                }
                            }


                            if (ieltsNumber) {
                              
                                
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = myscore.ielts;
                                ieltsDict.min = 0;
                                ieltsDict.require = await utils.giveMeNumber( myscore.ielts.replace(/ /g, ' '));;
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);
                            }
                            if (ibt_req) {
                                tofelDict.name = 'toefl ibt';
                                tofelDict.description = myscore.ibt;
                                tofelDict.min = 0;
                                tofelDict.require = await utils.giveMeNumber(myscore.ibt.replace(/ /g, ' '));;
                                tofelDict.max = 120;
                                tofelDict.R = 0;
                                tofelDict.W = 0;
                                tofelDict.S = 0;
                                tofelDict.L = 0;
                                tofelDict.O = 0;
                                englishList.push(tofelDict);
                            }
                            if (pte_req) {
                                pteDict.name = 'pte academic';
                                pteDict.description = myscore.pte;
                                pteDict.min = 0;
                                pteDict.require = await utils.giveMeNumber(myscore.pte.replace(/ /g, ' '));;
                                pteDict.max = 90;
                                pteDict.R = 0;
                                pteDict.W = 0;
                                pteDict.S = 0;
                                pteDict.L = 0;
                                pteDict.O = 0;
                                englishList.push(pteDict);
                            }
                          

                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppteDict = {}; const ppbtDict = {};
                            var penglishList = [];
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";

                            if (ieltsNumber != []) {
                                pieltsDict.name = 'ielts academic';
                                var value = {};
                                value.min = 0;
                                value.require = ieltsNumber;
                                value.max = 9;
                                pieltsDict.value = value;
                                penglishList.push(pieltsDict);
                            }
                            if (ibt_req != []) {
                                pibtDict.name = 'toefl ibt';
                                var value = {};
                                value.min = 0;
                                value.require = ibt_req;
                                value.max = 120;
                                pibtDict.value = value;
                                penglishList.push(pibtDict);
                            }
                            if (pte_req != []) {
                                ppteDict.name = 'pte academic';
                                var value = {};
                                value.min = 0;
                                value.require = pte_req;
                                value.max = 90;
                                ppteDict.value = value;
                                penglishList.push(ppteDict);
                            }
                           
                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }

                            //var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var entry_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_entry_more_details);
                            console.log("english more -->", english_more);
                            var academicReq = null;
                            var arr = [];
                            academicReq = myscore.academic_requirement;
                            courseAdminReq.academic = (academicReq) ? academicReq.toString() : [];
                            arr.push(academicReq)
                            console.log("acad--------->",arr);

                            courseAdminReq.english_requirements_url = english_more;
                            courseAdminReq.entry_requirements_url = entry_more;
                            courseAdminReq.academic_requirements_url = academic_more;
                            //courseAdminReq.academic_more_details = academic_more;
                            if (academicReq && String(academicReq).length > 0) {
                                courseAdminReq.academic = arr;
                            }
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                           
                                            for (let sel of selList) {
                                                sel = sel.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ');
                                                console.log("Sel 1111 -->>", sel);
                                                fullTimeText = sel;
                                            }


                                        }
                                    }
                                }
                            }
                           
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("filtered_duration_formated", filtered_duration_formated);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                                   
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { 
                            const courseKeyValcd = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyValcd)) {
                                for (const rootEleDict of courseKeyValcd) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;
                                            }
                                            
                                        }
                                    }
                                }
                            }
                           
                            // Location Launceston
                            var campLocationText = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                          

                                            for (let selItem of selList) {
                                                if (selItem.toLowerCase().includes('campus')) {
                                                    selItem = selItem.replace(/CAMPUS/g, '');

                                                }
                                                selItem = selItem.trim().replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s)/g, ',').split(',');
                                                console.log("selItem -->", selItem);
                                                for (let sel of selItem) {
                                                    sel = sel.trim();
                                                    if (sel != '' && sel != null) {
                                                        campLocationText.push({
                                                            "name": sel,
                                                            "code": String(course_cricos_code)
                                                        });
                                                    }

                                                }

                                            } // selList
                                        }
                                    }
                                }
                            }
                            console.log("##Campus-->" + campLocationText)
                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                               
                                resJsonData.course_campus_location = campLocationText;//await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                                // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                var study_mode = "";
                                campLocationValTrimmed = campLocationValTrimmed.toLowerCase();

                                campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                                if (campLocationValTrimmed.trim().length > 0) {
                                    study_mode = "On campus";
                                }
                                resJsonData.course_study_mode = study_mode;//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                                // }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                           
                            const feesList = [];
                            const feesDict = {
                               


                            };
                            var fee_desc_more = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal_more = courseScrappedData.course_tuition_fees_international_student_more;
                            if (Array.isArray(courseKeyVal_more)) {
                                for (const rootEleDict of courseKeyVal_more) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            

                                            for (let sel of selList) {
                                                sel = sel.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s)/g, ' ')
                                                fee_desc_more.push(sel);

                                            }
                                        }
                                    }
                                }
                            }
                            
                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            console.log("All fees-->", JSON.stringify(feesIntStudent))
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);



                                const arrval = String(feesWithDollorTrimmed).split('.');

                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            if(fee_desc_more.length > 0){
                                            feesDict.international_student={
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: String(fee_desc_more)
                                              
                                            }
                                        }
                                        else{
                                            feesDict.international_student={
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: feesIntStudent 
                                            }
                                        }
                                        }
                                        if (feesNumber == "0") {
                                            console.log("FEesNumber = 0");
                                            feesDict.international_student={
                                                amount: 0,
                                                duration: 1,
                                                unit: "Year",
                                                description: ""
                                              
                                            }
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                feesDict.international_student={
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",
                                    description: ""
                                   
                                }
                            }
                           
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }
                            
                        case 'program_code': {
                            let courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                           
                            var program_code = "";
                            if (courseKeyVal.includes('ADMISSION CRITERIA')) {
                                courseKeyVal = courseKeyVal.replace(/ADMISSION CRITERIA/g, '');
                                console.log("progcode-->",courseKeyVal);
                            }
                            else{
                            courseKeyVal = courseKeyVal.replace(/COURSE CODE/g, '');
                            courseKeyVal = courseKeyVal.trim().replace(/[\r\n\t ]+/g, ' ');
                            console.log("courseProgCode-->" + JSON.stringify(courseKeyVal));
                            }
                            // if (program_code.length > 0) {
                            if (courseKeyVal && courseKeyVal.length > 0) {
                                resJsonData.program_code = courseKeyVal;
                            } else {
                                program_code = "";
                            }

                            //}
                            break;
                        }
                        case 'course_intake': { 
                            const courseIntakeDisplay = {};
                            var courseIntakeStr = [];//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                          
                                           
                                            for (let selItem of selList) {
                                                if (selItem.includes('COURSE INTAKE')) {
                                                    selItem = selItem.replace(/COURSE INTAKE/g, '');
                                                }
                                                selItem = selItem.trim().replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s|)/g, '').split(",");
                                                console.log("selItemIntake -->", selItem);
                                                for (let sel of selItem) {
                                                    sel = sel.trim();
                                                    courseIntakeStr.push(sel + ", 2020");
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            if (!courseIntakeStr) {
                                courseIntakeStr = [];
                            }
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var intakes = [];
                                console.log("CALLED resJsonData.course_campus_location after intake");
                                var campus = resJsonData.course_campus_location;
                                for (let loc of campus) {
                                    var intakedetail = {};
                                     intakedetail.name = loc.name;
                                    intakedetail.value = courseIntakeStr;
                                    intakes.push(intakedetail);
                                }

                                let formatIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                var intakedata = {};
                                var intakeUrl = [];
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedata.intake = formatIntake;
                                intakedata.more_details = intakeUrl;
                               // intakedata.more_details = resJsonData.course_intake_url;
                                console.log("moredetails--->")

                                resJsonData.course_intake = intakedata;
                              
                            }
                            break;
                        }
                       
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_outline': {
              
                            let mgcrs = null;
                          
                         const majorcrs = {
                           majors:[],
                           minors:[],
                           more_details:""
                         };
                         //for major cousrseList
                           var coursemajor =  courseScrappedData.course_outline;
                           if (Array.isArray(coursemajor)) {
                             for (const rootEleDict of coursemajor) {
                                 console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                 const elementsList = rootEleDict.elements;
                                 for (const eleDict of elementsList) {
                                     console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                     const selectorsList = eleDict.selectors;
                                     for (const selList of selectorsList) {
                                         console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                         if (Array.isArray(selList) && selList.length > 0) {
                                           mgcrs = selList;
                                         }
                                     }
                                 }
                             }
                         }
                           if(mgcrs && mgcrs.length >0){
                             majorcrs.majors=mgcrs
                           }
                           else{
                             majorcrs.majors= majorcrs.majors
                           }
                              resJsonData.course_outline =  majorcrs;
                           //for minor courseList
                              var courseminor =  courseScrappedData.course_outline_minor;
                              let mincrs = null;
                              if (Array.isArray(courseminor)) {
                                for (const rootEleDict of courseminor) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                             mincrs = selList;
                                            }
                                        }
                                    }
                                }
                            }
                              if(mincrs && mincrs.length >0){
                                majorcrs.minors=mincrs
                              }
                              else{
                               majorcrs.minors=majorcrs.minors
                             }
                                 resJsonData.course_outline =  majorcrs;
              
                              console.log("major==>", resJsonData.course_outline)
                        
                          break;
                          }
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            break;
                        }
                        case 'application_fee': {
                            const courseKeyVal = courseScrappedData.application_fee;
                   
                            let applicationfee = null;
                              if (Array.isArray(courseKeyVal)) {
                            for (const rootEleDict of courseKeyVal) {
                           console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                           const elementsList = rootEleDict.elements;
                           for (const eleDict of elementsList) {
                           console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                              const selectorsList = eleDict.selectors;
                           for (const selList of selectorsList) {
                               console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                            if (Array.isArray(selList) && selList.length > 0) {
                              applicationfee = selList[0];
                               }
                               else{
                                  applicationfee = selList[0];
                               }
                            }
                           }
                           }
                       }
                      if (applicationfee && applicationfee.length>0) {
                        resJsonData.application_fee = applicationfee;
                       console.log("appfee"+ JSON.stringify(resJsonData.application_fee));
                       }
                       else{
                          resJsonData.application_fee = applicationfee;
                       }
                   
                        break;
                   
                  
                      }
                        case 'course_career_outcome': {
                            
                            let course_career_outcome = null;
                            
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                           
                            break;
                        }

                        case 'course_study_level': {
                            let courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
                            var study_level = "";

                            courseKeyVal = courseKeyVal.replace(/COURSE LEVEL/g, '');
                            courseKeyVal = courseKeyVal.trim().replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s|)/g, '');
                            console.log("courseStudyLev-->" + JSON.stringify(courseKeyVal));

                            if (courseKeyVal && courseKeyVal.length > 0) {
                                resJsonData.course_study_level = courseKeyVal;
                            } else {
                                course_study_level = "";
                            }
                            break;
                        }
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];

            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                //console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
              

                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            //custom call
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
