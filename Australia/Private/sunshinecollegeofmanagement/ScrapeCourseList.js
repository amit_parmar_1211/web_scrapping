const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'scrapeCourseCategoryList ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];
      var name;
      await s.setupNewBrowserPage("http://www.scm.vic.edu.au/Default.aspx");
      var mainCategory = [], redirecturl = [];
   
      const category_val = "//*[@id='lnkCourses']/ul/li/a";
      const levels = await s.page.$x(category_val)
      for (let country of levels) {
         name = await s.page.evaluate(el => el.innerText, country);
         redirecturl.push({ name: name });
        mainCategory.push(name);
      }
      console.log("subjectAreasArray-->",redirecturl)
        for(let catc of mainCategory ){
          const subjects = await s.page.$x("//*//a[(contains(text(),'" + catc + "'))]//following-sibling::ul/li/a");
          for(let i=0;i<subjects.length;i++){
            var elementstring=await s.page.evaluate(el => el.innerText, subjects[i]);
            var elementlink=await s.page.evaluate(el=>el.href,subjects[i])
            totalCourseList.push({href:elementlink,innerText:elementstring,category:catc})
          }
        }
     // fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(mainCategory))
      // "//*[@class='vc_column-inner ']/div/h2[(contains(text(),'" + catc + "'))]/following::div[3]//div[contains(@class,'stm-btn-container stm-btn-container_right')]/a")
      // fs.writeFileSync("./output/canberrainsituteoftechnology_courselist.json", JSON.stringify(datalist))
      fs.writeFileSync("./output/sunshinecollegeofmanagement_maincategorylist.json", JSON.stringify(mainCategory))
      console.log("totalCourseList -->", totalCourseList);
      await fs.writeFileSync("./output/sunshinecollegeofmanagement_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
      await fs.writeFileSync("./output/sunshinecollegeofmanagement_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/sunshinecollegeofmanagement_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  };
}
module.exports = { ScrapeCourseList };


