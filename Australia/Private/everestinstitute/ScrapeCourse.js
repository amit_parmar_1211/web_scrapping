const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = category_name;
                            break;
                        }
                        case 'course_title_category':
                        case 'course_title': {

                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)


                            if (title.includes('CRCIOS CODE:')) {
                                var ctitle70 = title.split('CRCIOS CODE:')[0].trim();

                                resJsonData.course_title = ctitle70;

                                var ctitle080 = ctitle70.slice(8);

                                resJsonData.course_title = ctitle080.trim();
                                var ctitle = format_functions.titleCase(ctitle080).trim();

                                //   
                                resJsonData.course_title = ctitle
                            }
                            else if (title.includes('CONTACT US')) {
                                var ctitle70 = await Course.extractValueFromScrappedElement(courseScrappedData.course_title1)

                                var newTitle = ctitle70.split('FOR')[1].trim();

                                var resNewTitle = newTitle.slice(9).trim();

                                var resNewTitle1 = resNewTitle.split(':')[0].trim();

                                var ctitle = format_functions.titleCase(resNewTitle1).trim();

                                //   
                                resJsonData.course_title = ctitle;
                            }
                            else if (title.includes('CRICOS CODE:')) {
                                var ctitle90 = title.split('CRICOS CODE:')[0].trim();
                                console.log("hghfgjdfghgjcgvhxgbgjfx", ctitle90);
                                var ctitle090 = ctitle90.slice(9).trim();

                                resJsonData.course_title = ctitle090;
                                var ctitle = format_functions.titleCase(ctitle090).trim();

                                //   
                                resJsonData.course_title = ctitle

                            }

                            else {

                                resJsonData.course_title = title

                            }
                            // console.log("titleNEW---->", ctitle)




                            break;
                        }

                        case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; const cpeDict = {};
                            var myIELTS = "";
                            var myAcademic = [];
                            // var filedata = JSON.parse(fs.readFileSync("./selectors/university_hard_coded_data.json"))[0];
                            var filedata = await utils.getValueFromHardCodedJsonFile('fees');

                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            // let tmpvar = filedata.filter(val => {
                            //     return val.name.toLowerCase() == title.toLowerCase()
                            // });


                            if (title.includes('CONTACT US')) {

                                var titleNW = await Course.extractValueFromScrappedElement(courseScrappedData.course_title1);
                                var title0000 = titleNW.split("FOR")[1].trim();
                                var titleid = title0000.split(":")[0].trim()

                                let tmpvar = filedata.filter(val => {
                                    return val.name.toLowerCase() == titleid.toLowerCase();
                                });
                                console.log("tmpvar1=====>", tmpvar);
                                let oneielts = tmpvar[0].IELTS;

                                var IeltsIntStudent = oneielts;
                            }
                            if (title.includes('CRCIOS CODE:')) {


                                var title0000 = title.split("CRCIOS CODE:")[0].trim();

                                let tmpvar = filedata.filter(val => {
                                    return val.name.toLowerCase() == title0000.toLowerCase();
                                });
                                console.log("tmpvar2=====>", tmpvar);
                                let oneielts = tmpvar[0].IELTS;

                                var IeltsIntStudent = oneielts;
                            }
                            if (title.includes('CRICOS CODE:')) {
                                var title600 = title.split("CRICOS CODE:")[0].trim();

                                // var titlt100 = title600.slice(8, 1);

                                let tmpvar = filedata.filter(val => {
                                    return val.name.toLowerCase() == title600.toLowerCase();
                                });
                                console.log("tmpvar3=====>", tmpvar);
                                let oneielts = tmpvar[0].IELTS;

                                var IeltsIntStudent = oneielts;
                                console.log("tmpvaIELTS--->>>>>", IeltsIntStudent);

                            }
                            // console.log("tmpvaIELTS--->>>>>", tmpvar);
                            // let oneielts = tmpvar[0].IELTS;

                            // const IeltsIntStudent = myIELTS;


                            if (IeltsIntStudent) {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = IeltsIntStudent;
                                ieltsDict.min = 0;
                                ieltsDict.require = await utils.giveMeNumber(IeltsIntStudent.replace(/ /g, ' '));;
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);
                            }

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }

                            //var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);


                            var AcademicIntStudent = "";
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            var filedata = await utils.getValueFromHardCodedJsonFile('fees');
                            if (title.includes('CONTACT US')) {
                                var titlelastt = await Course.extractValueFromScrappedElement(courseScrappedData.course_title1);
                                var title500 = titlelastt.split("FOR")[1].trim();
                                var titlefinale = title500.split(":")[0].trim();

                                let tmpvar = filedata.filter(val => {
                                    return val.name.toLowerCase() == titlefinale.toLowerCase()
                                });


                                let one = tmpvar[0].academic

                                AcademicIntStudent = one;
                                console.log("hjfdsfdjfgsjsgjskdgj", AcademicIntStudent);

                            }
                            let feesIntStudent111 = await utils.getValueFromHardCodedJsonFile('fees');
                            if (title.includes('CRCIOS CODE:')) {
                                var title5001 = title.split("CRCIOS CODE:")[0].trim();

                                // filedata["fees"].forEach(element => {
                                //     if (element.name == title5001) {
                                //         myAcademic = element.academic;
                                //     }
                                // });
                                let tmpvar = feesIntStudent111.filter(val => {
                                    return val.name.toLowerCase() == title5001.toLowerCase()

                                });

                                let one = tmpvar[0].fee
                            }
                            var filedata = await utils.getValueFromHardCodedJsonFile('fees');
                            if (title.includes('CRICOS CODE:')) {
                                var title500 = title.split("CRICOS CODE:")[0].trim();

                                // filedata["fees"].forEach(element => {
                                //     if (element.name == title500) {
                                //         myAcademic = element.academic;
                                //     }
                                // });
                                let tmpvar = filedata.filter(val => {
                                    return val.name.toLowerCase() == title500.toLowerCase()
                                });

                                let one = tmpvar[0].academic
                                AcademicIntStudent = one;
                            }
                            if (title.includes('CRCIOS CODE:')) {
                                var title500 = title.split("CRCIOS CODE:")[0].trim();
                                console.log("gfhggkfhgjfgbvjdnjsd bnsbnfn ", title500);
                                // filedata["fees"].forEach(element => {
                                //     if (element.name == title500) {
                                //         myAcademic = element.academic;
                                //     }
                                // });
                                let tmpvar = filedata.filter(val => {
                                    return val.name.toLowerCase() == title500.toLowerCase()
                                });

                                let one = tmpvar[0].academic
                                AcademicIntStudent = one;

                            }

                            courseAdminReq.academic = (AcademicIntStudent) ? [AcademicIntStudent.toString()] : [];

                            // courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];
                            courseAdminReq.english_requirements_url = "https://www.everest.edu.au/uploads/8/2/6/4/82648546/sc_35_-_entry_requirement_p_p_v1.4.pdf";
                            courseAdminReq.academic_requirements_url = "https://www.everest.edu.au/uploads/8/2/6/4/82648546/sc_35_-_entry_requirement_p_p_v1.4.pdf";
                            courseAdminReq.entry_requirements_url = "https://www.everest.edu.au/uploads/8/2/6/4/82648546/sc_35_-_entry_requirement_p_p_v1.4.pdf";
                            //courseAdminReq.academic_more_details = academic_more;
                            resJsonData.course_admission_requirement = courseAdminReq;

                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }

                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict787554 = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict64633 = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList6565 = ' + JSON.stringify(selList));

                                            for (let sel of selList) {

                                                sel = sel.replace(/[\r\n\t ]+/g, ' ');

                                                var sel1 = sel.split('Course Duration:')[1];

                                                var sel2 = sel1.split('(')[0]

                                                if (sel2.includes('Full Time')) {
                                                    var sel3 = sel2.split('Full Time')[1].trim();

                                                    fullTimeText = sel3;
                                                }
                                                else {
                                                    fullTimeText = sel2.trim();
                                                }

                                            }


                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));


                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                }
                            }
                        }
                            if (courseDurationDisplayList && courseDurationDisplayList.length > 0) {
                                resJsonData.course_duration_display = courseDurationDisplayList;
                                let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                var isfulltime = false, isparttime = false;
                                filtered_duration_formated.forEach(element => {
                                    if (element.display == "Full-Time") {
                                        isfulltime = true;
                                    }
                                    if (element.display == "Part-Time") {
                                        isparttime = true;
                                    }
                                });
                                resJsonData.isfulltime = isfulltime;
                                resJsonData.isparttime = isparttime;


                                break;
                            }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':


                        case 'course_campus_location': {
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            var campLocationText = null;
                            var cricoosnew;
                            const Cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log("Cricos---->", Cricos)
                            if (Cricos.includes('\nCRICOS CODE:')) {
                                cricoosnew = Cricos.split('\nCRICOS CODE:')[1].trim();


                            }
                            else if (Cricos.includes('\nCRCIOS CODE:')) {
                                cricoosnew = Cricos.split('\nCRCIOS CODE:')[1].trim();


                            }
                            else if (Cricos.includes('CONTACT US')) {
                                cricoosnew = Cricos.replace('CONTACT US', '091294E').trim();


                            }



                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {

                                                var course_campus_location = selList;

                                            }

                                        }
                                    }
                                }
                            }
                            const courseLocation = await utils.getValueFromHardCodedJsonFile('campuse');
                            var newlocation = String(courseLocation);

                            var campusedata = [];

                            campusedata.push({
                                "name": newlocation,
                                "code": cricoosnew

                            })


                            resJsonData.course_campus_location = campusedata;
                            //await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');

                            resJsonData.course_study_mode = 'On campus';//.join(',');

                            break;
                        }
                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };

                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

                            let resScholarshipJson = null;


                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }




                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }

                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)

                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;


                            break;
                        }

                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                resJsonData.application_fee = ""
                                break;
                            }

                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};
                            var filedata = await utils.getValueFromHardCodedJsonFile('fees');;
                            var FeesNew = "";
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);

                            if (title.includes('CONTACT US')) {
                                var titleAfter = await Course.extractValueFromScrappedElement(courseScrappedData.course_title1);

                                var titleAfter1 = titleAfter.split('FOR')[1].trim();
                                var titleAfter2 = titleAfter1.split(':')[0].trim();
                                let tmpvar = filedata.filter(val => {
                                    return val.name.toLowerCase() == titleAfter2.toLowerCase();
                                });

                                let FeesNew = tmpvar[0].fee;

                                var feesIntStudent = FeesNew;


                            }
                            if (title.includes('FOR')) {
                                var title890 = title.split("FOR")[1].trim();
                                var rest1 = title890.split(':')[0].trim();
                                let tmpvar = filedata.filter(val => {
                                    return val.name.toLowerCase() == rest1.toLowerCase();
                                });

                                let FeesNew = tmpvar[0].fee;

                                var feesIntStudent = FeesNew;

                            }
                            if (title.includes('CRCIOS CODE:')) {
                                var title55 = title.split("CRCIOS CODE:")[0].trim();
                                let tmpvar = filedata.filter(val => {
                                    return val.name.toLowerCase() == title55.toLowerCase();
                                });

                                let FeesNew = tmpvar[0].fee;

                                var feesIntStudent = FeesNew;
                                // filedata["fees"].forEach(element => {
                                //     if (element.name == title55) {
                                //         myfee = element.fee;
                                //     }
                                // });
                            }
                            if (title.includes('CRICOS CODE:')) {
                                var title4 = title.split("CRICOS CODE:")[0].trim();
                                let tmpvar = filedata.filter(val => {
                                    return val.name.toLowerCase() == title4.toLowerCase();
                                });

                                let FeesNew = tmpvar[0].fee;

                                var feesIntStudent = FeesNew;

                            }



                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);

                            if (feesIntStudent) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    if (feesVal != "NA" && feesVal != "N A") {
                                        const regEx = /\d/g;
                                        let feesValNum = feesVal.match(regEx);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            let feesNumber = null;
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }
                                            console.log(funcName + 'feesNumber = ' + feesNumber);
                                            if (Number(feesNumber)) {
                                                var feesint = {};
                                                feesint.amount = Number(feesNumber);
                                                feesint.duration = 1;
                                                feesint.unit = "year";

                                                feesint.description = feesIntStudent + FeesNew;

                                                feesDict.international_student = feesint;
                                            }
                                        }
                                    }
                                    else {
                                        var feesint = {};
                                        feesint.amount = 0;
                                        feesint.duration = 1;
                                        feesint.unit = "year";

                                        feesint.description = feesIntStudent + FeesNew;

                                        feesDict.international_student = feesint;
                                    }
                                }
                            }
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                    var more_fee = [];
                                    //  feesDict.international_student_all_fees = more_fee;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;
                        }

                        case 'program_code': {
                            let courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);

                            var program_code = "";

                            if (courseKeyVal.includes('CONTACT US')) {
                                var courseKeyVal1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_title1);

                                var courseKeyValPrognew = courseKeyVal1.split('FOR')[1].trim();
                                var courseKeyValProgRes1 = courseKeyValPrognew.split(" ")[0];



                                resJsonData.program_code = courseKeyValProgRes1;
                            }

                            else {
                                var courseKeyValPrognew = courseKeyVal.split(" ")[0];
                                courseKeyValProgRes1 = courseKeyValPrognew.substring(0, 8).trim();

                                resJsonData.program_code = courseKeyValProgRes1;
                            }
                            // if (program_code.length > 0) {
                            if (courseKeyValProgRes1 && courseKeyValProgRes1.length > 0) {
                                resJsonData.program_code = courseKeyValProgRes1;
                            } else {
                                program_code = "";
                            }

                            //}
                            break;
                        }
                        case 'course_intake': {
                            var courseIntakeStr = [];
                            // existing intake value
                            const courseIntakeStr1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            var campus = resJsonData.course_campus_location
                            if (String(courseIntakeStr1).includes("Weekly")) {
                                if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                                    console.log("In Main If", courseIntakeStr1);
                                    var myintake = [];
                                    var intakedata = {}
                                    if (String(courseIntakeStr1).indexOf("Weekly") > -1) {



                                        var arr = [
                                            {
                                                "actualdate": "anytime",
                                                "month": ""

                                            }]

                                        console.log("arr--->", arr)
                                        console.log("campus.length--->", campus.length)
                                        for (let i = 0; i < campus.length; i++) {
                                            var intakedetail = {};
                                            console.log("campus--->", campus[i].name)
                                            intakedetail.name = campus[i].name;
                                            intakedetail.value = arr;
                                            myintake.push(intakedetail)
                                            console.log("CourseDetails-->", JSON.stringify(myintake))

                                        }
                                        intakedata.intake = myintake
                                        let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                        intakedata.more_details = more_details;
                                        resJsonData.course_intake = intakedata;

                                    }
                                }
                            }

                            else {
                                if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                                    resJsonData.course_intake = courseIntakeStr1;
                                    const intakeStrList = String(courseIntakeStr1).split(';');
                                    console.log(funcName + 'intakeStrList course_intake = ' + JSON.stringify(intakeStrList));
                                    courseIntakeStr = intakeStrList;
                                }
                                console.log("intakes" + JSON.stringify(courseIntakeStr));

                                const d = String(courseIntakeStr).split(' ');
                                console.log("dvgdgffdhrh" + JSON.stringify(d));

                                if (d && d.length > 0) {

                                    console.log('Campuse location :' + campus);
                                    var intakes = [];
                                    console.log("Intake length-->" + d.length);
                                    console.log("Campus length-->" + campus.length);
                                    for (var camcount = 0; camcount < campus.length; camcount++) {
                                        var intakedetail = {};
                                        intakedetail.name = campus[camcount].name;
                                        intakedetail.value = d;
                                        intakes.push(intakedetail);
                                        // console.log("Intakekkeee", intakes);
                                    }
                                    console.log("Intakekkeee", intakes);
                                    let formatedIntake = await format_functions.providemyintake(intakes, "mom", "");
                                    console.log("FormatIntake", JSON.stringify(formatedIntake))
                                    var intakedata = {};
                                    intakedata.intake = formatedIntake;

                                    let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                    intakedata.more_details = more_details;
                                    resJsonData.course_intake = intakedata;
                                }
                            }
                            break;
                        }
                        // case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                        //     let courseIntakeDisplay = [];
                        //     var campus = resJsonData.course_campus_location;
                        //     if (courseIntakeDisplay) {
                        //         console.log("In Main If", courseIntakeDisplay);
                        //         var myintake = [];
                        //         var intakedata = {}
                        //         let arr = [];
                        //         console.log("arr--->", arr)
                        //         for (i = 0; i < campus.length; i++) {
                        //             var intakedetail = {};
                        //             console.log("campus--->", campus[i].name)
                        //             intakedetail.name = campus[i].name;
                        //             intakedetail.value = arr;
                        //             myintake.push(intakedetail)
                        //             console.log("CourseDetails-->", JSON.stringify(myintake))
                        //         }
                        //         intakedata.intake = myintake
                        //         intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                        //         resJsonData.course_intake = intakedata;
                        //     }
                        //     break;
                        // }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }

                        case 'course_overview': {
                            let overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            var titleforovrvw = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("titleforovrvw=---->", titleforovrvw);
                            if (titleforovrvw.includes('BSB51918')) {
                                console.log("entered--->")
                                var ovrw = "Diploma of Leadership and Management (BSB51918) is a nationally recognised course. This course helps in developing the skill and knowledge to work in a Leadership and Management role in organisations.This course will also aims to equip students with skills like planning, organizing, staffing and managing etc. that requires an individual to succeed in management."
                                resJsonData.course_overview = ovrw;
                            }
                            else if (titleforovrvw.includes('SIT60316')) {
                                var ovrw = "Advanced Diploma of Hospitality Management course is designed for the people in Hospitality industry who want to uplift their career from middle management to senior management roles. This course reflects the role of highly skilled senior managers who use a broad range of hospitality skills combined with specialised managerial skills and substantial knowledge of industry to coordinate hospitality operations. They operate with significant autonomy and are responsible for making strategic business management decisions."
                                resJsonData.course_overview = ovrw;
                            }


                            else {
                                var overview1 = overview.replace(/[\n]+/g, '');
                                console.log("overviewoverview-->", overview1);
                                resJsonData.course_overview = overview1;
                            }

                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r careerselList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                // course_career_outcome = selList;
                                                for (let selItem of selList) {

                                                    selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ');
                                                    course_career_outcome.push(selItem.trim())
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }

                        case 'course_study_level':
                            {
                                const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code)

                                if (title.includes('CONTACT US')) {
                                    var cTitle655 = "091294E"

                                    let cStudyLevel = await format_functions.getMyStudyLevel(cTitle655);

                                    resJsonData.course_study_level = cStudyLevel;
                                }
                                else if (cTitle == "SIT30816 Certificate III in Commercial Cookery") {
                                    resJsonData.course_study_level = "Package Course";
                                }
                                else if (cTitle == "SIT40516 Certificate IV in Commercial Cookery") {
                                    resJsonData.course_study_level = "Package Course";
                                }
                                else if (cTitle == "SIT50416 Diploma of Hospitality Management") {
                                    resJsonData.course_study_level = "Package Course";
                                }
                                else if (cTitle == "SIT60316 Advanced Diploma of Hospitality Management") {
                                    resJsonData.course_study_level = "Package Course";
                                }
                                else if (cTitle.includes('CRCIOS CODE:')) {
                                    var cTitle4000 = cTitle.split("CRCIOS CODE:")[1].trim();


                                    let cStudyLevel = await format_functions.getMyStudyLevel(cTitle4000);

                                    resJsonData.course_study_level = cStudyLevel;
                                }

                                else if (cTitle.includes('CRICOS CODE:')) {
                                    var cTitle8000 = cTitle.split("CRICOS CODE:")[1].trim();

                                    let cStudyLevel = await format_functions.getMyStudyLevel(cTitle8000);

                                    resJsonData.course_study_level = cStudyLevel;
                                }
                                else {
                                    let cStudyLevel = await format_functions.getMyStudyLevel(cTitle);

                                    resJsonData.course_study_level = cStudyLevel;
                                }

                            }
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                console.log("lastlocation---->", resJsonData.course_title);

                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                console.log("prevlocation---->");
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                //console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let FeesNew of resJsonData.course_tuition_fee.fees) {
                    if (FeesNew.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = FeesNew;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;

                //resJsonData.course_cricos_code = location_wise_data.cricos_code;
                //  resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;

                //resJsonData.current_course_intake = location_wise_data.course_intake;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
                // const res = await awsUtil.putFileInBucket(filelocation);
                // console.log(funcName + 'S3 object location = ' + res);
            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
