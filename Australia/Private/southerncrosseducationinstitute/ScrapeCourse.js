const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
const { Console } = require('console');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name, programcode) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            console.log("ok----->", course_name)
            const resJsonData = {};
            var demoarray = [];
            const feesIntStudent11 =  await utils.getValueFromHardCodedJsonFile('Locationmap');
                        var titlenew = ""
                        var title = course_name
                        let tmpvar = feesIntStudent11.filter(val => {
                            return val.name.toLowerCase() == title.toLowerCase();
                        });
                        console.log("tmpvar---->",tmpvar);
                         titlenew = tmpvar;
                         let location = titlenew[0].Location;
                         console.log("titlenew---->", location);
                        var feesIntStudent= titlenew[0].Fees;
                        console.log("feesnew---->",feesIntStudent);
                        let courseIntakeStr = titlenew[0].intake;
                        console.log("intakesnew---->",courseIntakeStr);
            Scrape.validateParams([courseScrappedData]);
            //const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'preaction': {
                            const valp = await Course.extractValueFromScrappedElement(courseScrappedData.preaction);
                            if (valp && valp.length > 0) { }
                            break;

                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = category_name;
                            break;
                        }
                        
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const ibtDict = {}; const caeDict = {}; const toeflDict = {}; let ieltsNumber = null;
                            let ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);

                            console.log("ielts_req------>", ielts_req);

                            if (ielts_req) {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ielts_req;
                                ieltsDict.min = 0;
                                ieltsDict.require = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));;
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);
                            }

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }

                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);

                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];
                            courseAdminReq.english_requirements_url = "";
                            courseAdminReq.academic_requirements_url = "";
                            courseAdminReq.entry_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'ielts_req':
                        case 'course_title_category':
                            case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_title': {

                            var ctitle = format_functions.titleCase(course_name).trim();


                            resJsonData.course_title = ctitle
                            break;
                        }
                       
                        
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration':
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                                console.log("##fullTimeText--->" + JSON.stringify(fullTimeText));
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                  
                                    let tempvar = await format_functions.validate_course_duration_full_time(fullTimeText);
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                       resJsonData.course_duration = fullTimeText.replace('\n','');
                                   
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                    
                                }
                                break;

                            }
                            case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                       
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':



                        case 'course_overview': {
                            resJsonData.course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            let course_overview = resJsonData.course_overview;
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                      
                       
                       
                        case 'course_campus_location': {

                            let course_cricos_code = null;
                            var crcode = "";
                            const courseKeyValcd = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            if (courseKeyValcd.includes("-CRICOS: ")) {
                                course_cricos_code = courseKeyValcd.split("-CRICOS: ")[1];
                                console.log("course_cricos_code11111---->>>", course_cricos_code);
                            }
                            else {
                                course_cricos_code = courseKeyValcd.split("-CRICOS ")[1];
                                console.log("course_cricos_code11111---->>>", course_cricos_code);
                            }
                            crcode = course_cricos_code;
                            console.log("cri00>", crcode);
                            // Location Launceston
                            var campLocationText = [];
                           

                            for (let sel of location) {
                                campLocationText.push({
                                    "name": sel,
                                    "code": String(crcode)
                                });
                            }

                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            if (campLocationText && campLocationText.length > 0) {

                                resJsonData.course_campus_location = campLocationText;//await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                                // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                var study_mode = "";
                                study_mode = "On campus";

                                resJsonData.course_study_mode = study_mode;//.join(',');
                                console.log("Campus----")
                                // console.log("Course_Name_title------->>>>", Tuition_Fee);
                                // }
                            }
                            console.log("cmp", resJsonData.course_campus_location)
                            break;
                        }
                        case 'course_study_mode':
                           
                        case 'course_tuition_fees_international_student_more':
                      
                       

                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                        const courseTuitionFee = {};
                        // courseTuitionFee.year = configs.propValueNotAvaialble;
                        console.log("demoarray1--->", demoarray);
                                  
                        const feesList = [];
                        const feesDict = {
                            international_student: {},

                        };
                        const courseKeyValfee = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                           
                       
                       
                        // if we can extract value as Int successfully then replace Or keep as it is
                        if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                            const feesWithDollorTrimmed = String(feesIntStudent).trim();

                            console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                            const arrval = String(feesWithDollorTrimmed).split('.');
                            const feesVal = String(arrval[0]);
                            console.log(funcName + 'feesVal = ' + feesVal);
                            if (feesVal) {
                                const regEx = /\d/g;
                                let feesValNum = feesVal.match(regEx);
                                if (feesValNum) {
                                    console.log(funcName + 'feesValNum = ' + feesValNum);
                                    feesValNum = feesValNum.join('');
                                    console.log(funcName + 'feesValNum = ' + feesValNum);
                                    let feesNumber = null;
                                    if (feesValNum.includes(',')) {
                                        feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                    } else {
                                        feesNumber = feesValNum;
                                    }
                                   
                                    if (Number(feesNumber)) {
                                        feesDict.international_student = ({
                                            amount: Number(feesNumber),
                                            duration: Number(demoarray.duration),
                                            unit: demoarray.unit,
                                            description: feesIntStudent

                                        });

                                    }
                                    if (feesNumber == "0") {
                                        console.log("FEesNumber = 0");
                                        feesDict.international_student = ({
                                            amount: 0,
                                            duration: Number(demoarray.duration),
                                            unit: demoarray.unit,
                                            description: ""

                                        });
                                    }
                                }
                            }
                        } // if (feesIntStudent && feesIntStudent.length > 0)
                        else {
                            feesDict.international_student = ({
                                amount: 0,
                                duration: Number(demoarray.duration),
                                unit: demoarray.unit,
                                description: ""

                            });
                        }
                        var fee_desc_more = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                        let courseKeyVal_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_more);
                        console.log("courseKeyVal_more@@1", courseKeyVal_more);
                        fee_desc_more.push(courseKeyVal_more)
                        if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                            const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                            const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                            if (feesDuration && feesDuration.length > 0) {
                                feesDict.fee_duration_years = feesDuration;
                                // feesDict.international_student_all_fees = []
                                console.log("morefees@@2", fee_desc_more);

                            }
                            if (feesCurrency && feesCurrency.length > 0) {
                                feesDict.currency = feesCurrency;
                            }
                            if (feesDict) {
                                var campus = resJsonData.course_campus_location;
                                for (let loc of campus) {
                                    feesList.push({ name: loc.name, value: feesDict });
                                }
                            }
                            if (feesList && feesList.length > 0) {
                                courseTuitionFee.fees = feesList;
                            }
                            console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                            if (courseTuitionFee) {
                                resJsonData.course_tuition_fee = courseTuitionFee;
                            }

                            // take tuition fee value at json top level so will help in DDB for indexing
                            if (courseTuitionFee && feesDict.international_student) {
                                resJsonData.course_tuition_fee = courseTuitionFee;
                                // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                            }

                        }
                        console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                        // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                        if (!feesDict.international_student) {
                            console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                            console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                            return null; // this will add this item into FailedItemList and writes file to local disk
                        }

                        break;
                    }
                  

                        case 'program_code': {

                            resJsonData.program_code = programcode;

                            break;
                        }
                        
                        case 'course_outline': {

                            let mgcrs = null;

                            const majorcrs = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };
                            //for major cousrseList
                            var coursemajor = courseScrappedData.course_outline;
                            if (Array.isArray(coursemajor)) {
                                for (const rootEleDict of coursemajor) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                mgcrs = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (mgcrs && mgcrs.length > 0) {
                                majorcrs.majors = mgcrs
                            }
                            else {
                                majorcrs.majors = majorcrs.majors
                            }
                            resJsonData.course_outline = majorcrs;
                            //for minor courseList
                            var courseminor = courseScrappedData.course_outline_minor;
                            let mincrs = null;
                            if (Array.isArray(courseminor)) {
                                for (const rootEleDict of courseminor) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                mincrs = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (mincrs && mincrs.length > 0) {
                                majorcrs.minors = mincrs
                            }
                            else {
                                majorcrs.minors = majorcrs.minors
                            }
                            resJsonData.course_outline = majorcrs;

                            console.log("major==>", resJsonData.course_outline)

                            break;
                        }
                        case 'course_intake': {
                            const courseIntakeDisplay = {};
                            // var courseIntakeStr = [];//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            // const courseKeyVal = courseScrappedData.course_intake;

                            // courseIntakeStr = ["May", "July"]
                            if (!courseIntakeStr) {
                                courseIntakeStr = [];
                            }
                            console.log("courseIntakeStr",courseIntakeStr);
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var intakes = [];
                                console.log("CALLED resJsonData.course_campus_location after intake");
                                var campus = resJsonData.course_campus_location;
                                for (let loc of campus) {
                                    var intakedetail = {};
                                    intakedetail.name = loc.name;
                                    intakedetail.value = courseIntakeStr;
                                    intakes.push(intakedetail);
                                }

                                let formatIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                var intakedata = {};
                                var intakeUrl = [];
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedata.intake = formatIntake;
                                intakedata.more_details = intakeUrl;
                                // intakedata.more_details = resJsonData.course_intake_url;
                                console.log("moredetails--->")

                                resJsonData.course_intake = intakedata;

                            }
                            break;
                        }

                        case 'course_country': {
                            resJsonData.course_country = await Course.extractValueFromScrappedElement(courseScrappedData.course_country)
                            break;
                        }
                        case 'application_fee': {
                            const courseKeyVal = courseScrappedData.application_fee;

                            let applicationfee = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                applicationfee = selList[0];
                                            }
                                            else {
                                                applicationfee = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (applicationfee && applicationfee.length > 0) {
                                resJsonData.application_fee = applicationfee;
                                console.log("appfee" + JSON.stringify(resJsonData.application_fee));
                            }
                            else {
                                resJsonData.application_fee = applicationfee;
                            }

                            break;


                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));

                                            console.log("sel----->", selList);
                                            course_career_outcome = selList

                                        }
                                    }
                                }
                            }
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome
                            } else {
                                resJsonData.course_career_outcome = [];
                            }
                            break;
                        }

                        case 'course_study_level': {
                            let course_crc = null;

                            const coursecd = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            if (coursecd.includes("-CRICOS: ")) {
                                course_crc = coursecd.split("-CRICOS: ")[1];

                            }
                            else {
                                course_crc = coursecd.split("-CRICOS ")[1];

                            }

                            const study_val = await format_functions.getMyStudyLevel(course_crc);
                            resJsonData.course_study_level = study_val;
                            console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                            break;
                        }
                            break;

                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                  NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                 NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                 NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                 var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
               
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;


                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;

        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            //custom call
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category, courseDict.programcode);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                //  await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
