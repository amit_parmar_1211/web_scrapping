const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];

            let cricosIntStudent = await utils.getValueFromHardCodedJsonFile('course_CRICOS_mapping');
            console.log("cricosIntStudent----->>>>>", cricosIntStudent);
            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
            // .replace('\n', '')
            let title11 = title.split('|')[0];
            title = title11.split('–')[1];
            console.log("splitStr@@@2" + title);
            let tmpvar = cricosIntStudent.filter(element => {
                return element.name.toLowerCase().trim() == title.toLowerCase().trim()
            });
            console.log("tmpvar----->>>>>", tmpvar);
            let CRICOS = tmpvar[0].CRICOS
            console.log("one----->>>>>", CRICOS);


            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            // let cricose_text = cTitle.replace('CRICOS Code :', '').trim();
                            console.log("cricose_text------>>>>>>", CRICOS);
                            const category_val = await format_functions.getMyCategory(CRICOS);
                            console.log("category_val------>>>>>>", category_val);
                            resJsonData.course_discipline = [category_val.split('-')[1].trim()];
                            console.log("resJsonData.course_discipline------>>>>>>", resJsonData.course_discipline);
                            break;
                        }
                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            // .replace('\n', '')
                            title = title.split('|')[0];
                            console.log("splitStr@@@2" + title);
                            var ctitle = format_functions.titleCase(title).trim();
                            // var ctitle2 = ctitle.replace(' ( ', '(');
                            console.log("ctitle@@@", ctitle.trim());
                            let program_code = ctitle.split('–')[1].trim();
                            resJsonData.course_title = program_code
                            break;
                        }
                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; let ieltsNumber = null;
                            let courseKeyVal_ielts = courseScrappedData.ielts_req;
                            let ielts_req = ""
                            if (Array.isArray(courseKeyVal_ielts)) {
                                for (const rootEleDict of courseKeyVal_ielts) {
                                    console.log(funcName + '\n\r rootEleDict ieltsDict= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict ieltsDict= ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList ieltsDict= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList)) {
                                                for (let sel of selList) {
                                                    if (ielts_req != sel) {
                                                        ielts_req = sel;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // console.log("##ielts_req-->" + ielts_req);
                            // if (ielts_req.length == 0) {
                            //     ielts_req = "Academic IELTS of 5.5 with 5.0 in all bands, or equivalent.";
                            // }
                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppteDict = {};
                            var penglishList = [];
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                            if (ielts_req) {
                                ieltsScore = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                            }
                            if (ieltsScore) {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = ielts_req;
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsScore;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }
                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            } else {
                                throw new Error("english not found");
                            }
                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var academicReq = "";
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList;
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }
                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }
                            courseAdminReq.entry_requirements_url = "";
                            courseAdminReq.english_requirements_url = "";
                            courseAdminReq.academic_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            let newUrl = courseUrl;
                            if (newUrl) {
                                resJsonData.course_url = newUrl;
                                console.log("courses url------>>>>", resJsonData.course_url);
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                                console.log("##fullTimeText--->" + JSON.stringify(fullTimeText));
                                                let duration_text = String(fullTimeText).replace('Duration  :')
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime.replace('Duration  :', '').trim();
                                    courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push((tempvar));
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston
                            var campLocationText; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location_intake;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            if (campLocationText.length == 0) {
                                let location_val = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                                console.log("location_val----->>>>", location_val);
                                //    console.log("tmpvar23423523464356----->>>>>", tmpvar[0].location);
                                if (String(location_val.includes(',') && location_val.includes('and'))) {
                                    let location_text = location_val.split(',');
                                    console.log("location_text----->>>>", location_text);
                                    let location_text11 = String(location_text).split('and');
                                    let textcoma = String(location_text11).split(',');
                                    campLocationText = textcoma;
                                    console.log("location_text1textcoma1----->>>>", campLocationText);
                                }
                            }
                            let newcampus = [];
                            for (let campus of campLocationText) {
                                if (!newcampus.includes(campus)) {
                                    newcampus.push(campus);
                                    console.log("##Campuscampus-->" + campus)
                                }
                            }
                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            if (newcampus && newcampus.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                resJsonData.course_study_mode = "On campus";
                                console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                var campusedata = [];
                                newcampus.forEach(element => {
                                    campusedata.push({
                                        "name": element.trim(),
                                        "code": CRICOS.toString()
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                console.log("## resJsonData.course_campus_location-->", resJsonData.course_campus_location);
                                // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                var study_mode = [];
                                if (campLocationValTrimmed.trim().length > 0) {
                                    study_mode.push('On campus');
                                }
                                resJsonData.course_study_mode = study_mode.toString();//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                            }
                            break;
                        }
                        case 'course_study_mode': {
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': {
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            console.log("demoarray123 -->", demoarray);
                            let courseKeyVal_fees = courseScrappedData.course_tuition_fees_international_student;
                            let coursefees;
                            if (Array.isArray(courseKeyVal_fees)) {
                                for (const rootEleDict of courseKeyVal_fees) {
                                    console.log(funcName + '\n\r rootEleDict ieltsDict= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict ieltsDict= ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList ieltsDict= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList)) {
                                                for (let sel of selList) {
                                                    if (coursefees != sel) {
                                                        coursefees = sel;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feeYear && feeYear.length > 0) {
                                courseTuitionFee.year = feeYear;
                            }
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (coursefees && coursefees.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(coursefees).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('–');
                                const feesVal = String(arrval[1]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,
                                                description: coursefees,
                                            });
                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,
                                                description: "not available fee",
                                            });
                                        }
                                        console.log("feesDictinternational_student-->", feesDict.international_student);
                                    }
                                }
                            } else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: Number(demoarray.duration),
                                    unit: demoarray.unit,
                                    description: "not available fee",
                                });
                            }
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = Number(inetStudentFees);
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if
                            var feesIntStudent111 = [];
                            const courseKeyVal = courseScrappedData.course_tuition_fees_international_student_more;
                            console.log("courseKeyVal-->  ", courseKeyVal);
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                feesIntStudent111 = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            let newfeesmore = [];
                            for (let feesvalmore of feesIntStudent111) {
                                if (!newfeesmore.includes(feesvalmore)) {
                                    newfeesmore.push(feesvalmore);
                                    console.log("##Campuscampus-->" + newfeesmore)
                                }
                            }
                            console.log("feesIntStudent111-->  ", feesIntStudent111);
                            //  let international_student_all_fees_array = newfeesmore;
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                // feesDict.international_student_all_fees = [];
                                // if (international_student_all_fees_array && international_student_all_fees_array.length > 0) {

                                //     for (let student_all_fees of international_student_all_fees_array) {
                                //         feesDict.international_student_all_fees.push(student_all_fees.replace(/[\r\n\t ]+/g, ' ').replace('-', '').trim());
                                //     }
                                // }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                }
                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null;
                            }
                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            console.log("courseKeyVal--->", courseKeyVal)
                            let program_code = courseKeyVal.split('–')[0].trim();
                            console.log("program_code--->", program_code)
                            if (program_code.length > 0) {
                                resJsonData.program_code = program_code;
                            }
                            else {
                                resJsonData.program_code = "";
                            }
                            break;
                        }
                        case 'course_intake': {
                            // existing intake value
                            var courseIntakeStr = [];
                            const courseKeyVal = courseScrappedData.course_intake;
                            console.log("Intake--->", courseKeyVal)
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r course_intake rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r course_intake eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_intake selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                courseIntakeStr = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var intakes = [];
                                var campus_location = resJsonData.course_campus_location;
                                console.log("campus_location11----->>>>", campus_location)
                                let tmpIntakeArray = [];
                                for (let outintake of courseIntakeStr) {
                                    console.log("outintake --> ", outintake);
                                    for (let campus of campus_location) {
                                        let tmpvar = outintake.split('\n');
                                        if (campus.name.toLowerCase().indexOf(tmpvar[0].toLowerCase()) > -1) {
                                            console.log("campus.name.toLowerCase() ==>");
                                            let intake = [];
                                            for (let data of tmpvar) {
                                                var matches = data.match(/\d+/g);
                                                if (matches != null) {
                                                    intake.push(data);
                                                }
                                            }
                                            tmpIntakeArray.push({ name: campus.name, value: intake })
                                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                            let formatIntake = await format_functions.providemyintake(tmpIntakeArray, "mom", "");
                                            if (formatIntake && formatIntake.length > 0) {
                                                var intakedata = {};
                                                intakedata.intake = formatIntake;
                                                intakedata.more_details = more_details;
                                                resJsonData.course_intake = intakedata;
                                            }
                                        }
                                        console.log("tmpIntakeArray------>>>>", tmpIntakeArray);
                                    }
                                }
<<<<<<< HEAD

                            } else if (courseIntakeStr.length == 0) {
=======
                            }
                            // if (String(courseIntakeStr).includes("Enrol any Time!")) {
                            //     console.log("courseIntakeDisplay--->", courseIntakeStr)
                            //     var campus = resJsonData.course_campus_location;
                            //     console.log("campuscampus--->", campus[0].name)
                            //     var myintake = [];
                            //     var intakedata = {}
                            //     var arr = [
                            //         {
                            //             "actualdate": "anytime",
                            //             "month": ""
                            //         }]
                            //     for (let i = 0; i < campus.length; i++) {
                            //         var intakedetail = {};
                            //         intakedetail.name = campus[i].name;
                            //         intakedetail.value = arr;
                            //         console.log("intakedetail.value--->", intakedetail.value)
                            //         myintake.push(intakedetail)
                            //     }
                            //     intakedata.intake = myintake
                            //     intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            //     resJsonData.course_intake = intakedata;
                            // } else 
                            else if (courseIntakeStr.length == 0) {
>>>>>>> e15c772eb3a767278f598d9ed85e1d7eab529a42
                                let courseIntakeDisplay = [];
                                var campus = resJsonData.course_campus_location;
                                if (courseIntakeDisplay) {
                                    console.log("In Main If", courseIntakeDisplay);
                                    var myintake = [];
                                    var intakedata = {}
                                    let arr = [];
                                    console.log("arr--->", arr)
                                    for (i = 0; i < campus.length; i++) {
                                        var intakedetail = {};
                                        console.log("campus--->", campus[i].name)
                                        intakedetail.name = campus[i].name;
                                        intakedetail.value = arr;
                                        myintake.push(intakedetail)
                                        console.log("CourseDetails-->", JSON.stringify(myintake))
                                    }
                                    intakedata.intake = myintake
                                    intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                    resJsonData.course_intake = intakedata;

                                }
                            }
                            break;
                        }

                        // case 'course_cricos_code': {
                        //     const mycodes = [];
                        //     if (CRICOS) {
                        //         const locations = resJsonData.course_campus_location;
                        //         console.log("locations----->", locations);
                        //         console.log("course_cricos_code----->", CRICOS);
                        //         for (let location of locations) {
                        //             mycodes.push({
                        //                 location: location.name, code: CRICOS.toString().replace('CRICOS Code :', '').trim(), iscurrent: false
                        //             });
                        //         }
                        //         resJsonData.course_cricos_code = mycodes;
                        //     } else {
                        //         throw new Error("CRICOS not found");
                        //     }
                        //     break;
                        // }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r course_career_outcome rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_career_outcome selList = ' + JSON.stringify(selList));
                                            course_career_outcome = String(selList).trim();
                                        }
                                    }
                                }
                            }
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = [course_career_outcome]
                            } else {
                                course_career_outcome = [];
                            }
                            break;
                        }
                        case 'course_study_level': {
                            // const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            const study_val = await format_functions.getMyStudyLevel(CRICOS);
                            resJsonData.course_study_level = study_val;
                            console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                            break;
                        }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                // if (application_fee.length > 0) {
                                //     resJsonData.application_fee = application_fee;
                                // } else {
                                resJsonData.application_fee = '';
                                //  }

                                break;
                            }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                }
            } var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                // NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;

                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }
    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            }
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };