const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {

        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }

                       
                        case 'course_discipline': {
                            console.log("course_category" + JSON.stringify(course_category))
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = ["Engineering"];
                            break;
                        }

                        case 'course_title': {

                            const coursetitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("CourseTitle1", JSON.stringify(coursetitle))
                            var courseTitle1 = coursetitle.slice(8)
                            console.log("coursetitle--->",courseTitle1);
                           
                            

                            var ctitle = format_functions.titleCase(courseTitle1).trim();
                            resJsonData.course_title = ctitle.trim();
                            console.log("New Title-->", resJsonData.course_title);

                            break;
                        }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: courseUrl


                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        case 'application_fee': {
                            resJsonData.application_fee = "250";
                        }
                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; let ieltsNumber = null;
                            var ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_req);
                            var pte_req = await Course.extractValueFromScrappedElement(courseScrappedData.pte_req);
                            var pte_req1 = pte_req.split('5.5 or')[1].trim()
                            console.log("ielts_req--->", ielts_req);
                            // if (courseScrappedData.ielts_req) {
                            //     ieltsDict.name = 'ielts academic';
                            //     ieltsDict.description = ielts_req;
                            //     englishList.push(ieltsDict);
                            // }


                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppteDict = {};
                            var penglishList = [];
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                            if (ielts_req) {
                                // ieltsScore = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ielts_req;
                                ieltsDict.min = 0;
                                ieltsDict.require = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));;
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);
                            }
                            if (pte_req1) {
                                // ieltsScore = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                                pteDict.name = 'pte academic';
                                pteDict.description = pte_req1;
                                pteDict.min = 0;
                                pteDict.require = await utils.giveMeNumber(pte_req1.replace(/ /g, ' '));;
                                pteDict.max = 90;
                                pteDict.R = 0;
                                pteDict.W = 0;
                                pteDict.S = 0;
                                pteDict.L = 0;
                                pteDict.O = 0;
                                englishList.push(pteDict);
                            }


                            if (ieltsScore != []) {
                                pieltsDict.name = 'ielts academic';
                                var value = {};
                                value.min = 0;
                                value.require = ieltsScore;
                                value.max = 9;
                                pieltsDict.value = value;
                                penglishList.push(pieltsDict);
                            }
                            if (pteScore != []) {
                                ppteDict.name = 'pte academic';
                                var value = {};
                                value.min = 0;
                                value.require = pteScore;
                                value.max = 90;
                                ppteDict.value = value;
                                penglishList.push(ppteDict);
                            }
                            // if (penglishList && penglishList.length > 0) {
                            //     courseAdminReq.englishprogress = penglishList;
                            // }
                            // else {
                            //     courseAdminReq.englishprogress = [];
                            // }
                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }
                            else {
                                courseAdminReq.english = [];
                            }

                            //var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var entry_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_entry_more_details);
                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            var arr = [];
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            // console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            academicReq = selList;
                                            //}
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = (academicReq) ? academicReq.toString() : [];

                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                          
                            courseAdminReq.entry_requirements_url = "";
                            courseAdminReq.english_requirements_url = "";
                            courseAdminReq.academic_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            // arr.push(academicReq)
                            if (academicReq && String(academicReq).length > 0) {
                                // courseAdminReq.academic = arr;
                                //arr.push(academicReq)
                                courseAdminReq.academic = academicReq;
                            }
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = "", courseDurationList = {};
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList course_duration_full_time= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime = resFulltime;
                                    courseDurationList = durationFullTime;
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = courseDurationList;

                                    } else {
                                        resJsonData.course_duration;
                                    }

                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {

                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                       resJsonData.isfulltime = isfulltime;
                                      resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }

                        case 'course_tuition_fee':

                        case 'page_url':
                        case 'course_study_mode': { // Location Launceston
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston                  
                            console.log('course_campus_location matched case: ' + key);
                            let concatnatedSelectorsStr = [];
                            let courseIntake = [];
                            const rootElementDictList = courseScrappedData[key];
                            var myintake_data = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                            const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('location');
                            const fixedFeesDict1 =  String(fixedFeesDict).split(',');
                            const cricc = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            var cricc1 = cricc.split('Cricos course code:')[1].trim();
                            console.log("fixedFeesDict-0000000-0/-->",fixedFeesDict1);
                            for (let temp of fixedFeesDict1) {
                                temp = temp.trim();
                                courseIntake.push(temp);
                            }
                           console.log("courseIntakearray-->",courseIntake);
                           if (courseIntake && courseIntake.length > 0) {
                            //resJsonData.course_intake = {};
                             resJsonData.course_campus_location = [];
                            for (let location of courseIntake) {
                                resJsonData.course_campus_location.push({
                                    "name": location,
                                    "value": cricc1

                                });
                                console.log("resJsonData.course_intake.intake", resJsonData.course_campus_location);

                            }
                            break;
                        }
                        }

                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                        const courseTuitionFee = {};
                        // courseTuitionFee.year = configs.propValueNotAvaialble;

                        let newcampus = [];

                        for (let campus of resJsonData.course_campus_location) {
                            if (campus.name.toLowerCase().indexOf('online') > -1 || campus.name.toLowerCase().indexOf('line learning') > -1) {

                            } else {
                                newcampus.push(campus);
                            }
                        }

                        resJsonData.course_campus_location = newcampus;
                        const feesList = [];
                        const feesDict = {
                            international_student: {}
                        };
                        // feesDict.international_student = configs.propValueNotAvaialble;
                        // feesDict.fee_duration_years = configs.propValueNotAvaialble;
                        // feesDict.currency = configs.propValueNotAvaialble;
                        const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);


                        // if we can extract value as Int successfully then replace Or keep as it is
                        if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                            const feesWithDollorTrimmed = String(feesIntStudent).trim();

                            console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                            const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                            const arrval = String(feesVal1).split('.');

                            const feesVal = String(arrval[0]);

                            console.log(funcName + 'feesVal = ' + feesVal);
                            if (feesVal) {
                                const regEx = /\d/g;
                                let feesValNum = feesVal.match(regEx);
                                if (feesValNum) {
                                    console.log(funcName + 'feesValNum = ' + feesValNum);
                                    feesValNum = feesValNum.join('');
                                    console.log(funcName + 'feesValNum = ' + feesValNum);
                                    let feesNumber = null;
                                    if (feesValNum.includes(',')) {
                                        feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                    } else {
                                        feesNumber = feesValNum;
                                    }
                                    console.log(funcName + 'feesNumber = ' + feesNumber);
                                    if (Number(feesNumber)) {
                                        console.log('*************fees one*********************');
                                        feesDict.international_student = ({
                                            amount: Number(feesNumber),
                                            duration: 1,
                                            unit: "Year",

                                            description: "",

                                        });
                                    } else {
                                        console.log('*************fees one*********************');
                                        feesDict.international_student = ({
                                            amount: 0,
                                            duration: 1,
                                            unit: "Year",

                                            description: "",

                                        });
                                    }
                                } else {
                                    feesDict.international_student = ({
                                        amount: 0,
                                        duration: 1,
                                        unit: "Year",

                                        description: "",

                                    });
                                }
                            } else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",

                                    description: "",

                                });
                            }
                        } // if (feesIntStudent && feesIntStudent.length > 0)

                        // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                        var cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                        console.log(funcName + 'cricoseStr = ' + cricoseStr);
                        console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                        if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {

                            const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                            if (fixedFeesDict) {
                                console.log("cricoseStr[i]", cricoseStr);
                                console.log("cricoseStr[i].toString()", cricoseStr.toString());
                                console.log("cricoseStr[i].toString().split(':')", cricoseStr.toString().split(":"));
                                let temp = cricoseStr.toString().split(":");
                                temp = temp[2].trim();
                                console.log("TEMP", temp);
                                for (const codeKey in fixedFeesDict) {
                                    console.log(funcName + 'codeKey = ' + codeKey);
                                    if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                        const keyVal = fixedFeesDict[codeKey];
                                        console.log(funcName + 'keyVal = ' + keyVal);
                                        console.log("temp=", temp, "codeKey=", codeKey, "temp.indexOf(codeKey)>-1", temp.indexOf(codeKey) > -1);
                                        if (temp.indexOf(codeKey) > -1) {
                                            console.log("Entered!!");
                                            const feesDictVal = fixedFeesDict[temp];
                                            console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                            if (feesDictVal && feesDictVal.length > 0) {
                                                const inetStudentFees = Number(feesDictVal);
                                                console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                if (Number(inetStudentFees)) {
                                                    feesDict.international_student = ({
                                                        amount: Number(inetStudentFees),
                                                        duration: 1,
                                                        unit: "Year",

                                                        description: "",

                                                    });
                                                }
                                            }
                                        }
                                    }
                                } // for
                            } // if
                        } // if

                        if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                            const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                            const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                            if (feesDuration && feesDuration.length > 0) {
                                feesDict.fee_duration_years = feesDuration;
                            }
                            if (feesCurrency && feesCurrency.length > 0) {
                                feesDict.currency = feesCurrency;
                            }
                            if (feesIntStudent.length > 0) { // extract only digits
                                console.log("international_student" + feesIntStudent)
                                // feesDict.international_student_all_fees = [];

                            }
                            if (feesDict) {
                                console.log("resJsonData.course_campus_location --> ", resJsonData.course_campus_location);
                                var campus = resJsonData.course_campus_location;
                                for (let loc of campus) {
                                    feesList.push({ name: loc.name, value: feesDict });
                                }
                            }
                            if (feesList && feesList.length > 0) {
                                courseTuitionFee.fees = feesList;
                            }
                            console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                            if (courseTuitionFee) {
                                resJsonData.course_tuition_fee = courseTuitionFee;
                            }
                        }
                        else {
                            feesDict.international_student = ({
                                amount: 0,
                                duration: 1,
                                unit: "Year",

                                description: "",

                            });
                            //   feesDict.international_student_all_fees = [];

                            if (feesDict) {
                                var campus = resJsonData.course_campus_location;
                                for (let loc of campus) {
                                    feesList.push({ name: loc.name, value: feesDict });
                                }
                            }
                            if (feesList && feesList.length > 0) {
                                courseTuitionFee.fees = feesList;
                            }
                            if (courseTuitionFee) {
                                resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                resJsonData.course_tuition_fee = courseTuitionFee;
                            }
                        }
                        break;
                    }
                     


                        case 'program_code': {
                            var program_code = "";
                            program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            var program_code1 = program_code.slice(0,8).trim();


                            resJsonData.program_code = program_code1;

                            break;
                        }
                        case 'course_intake': {
                            var courseIntake = [];
                            const courseIntakeStr = await utils.getValueFromHardCodedJsonFile('course_intake');
                            console.log("courseIntakeStr@@@@@", courseIntakeStr)
                            for (let temp of courseIntakeStr) {
                                temp = temp.trim();
                                courseIntake.push(temp);
                            }
                            var locationArray = resJsonData.course_campus_location;
                            console.log("locationArray", locationArray);

                            if (courseIntake && courseIntake.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let location of locationArray) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location.name,
                                        "value": courseIntake

                                    });
                                    console.log("resJsonData.course_intake.intake", resJsonData.course_intake.intake);

                                }
                                let formatIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");
                                var intakeUrl = [];
                                console.log("formatIntake@@@@@", formatIntake);

                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                resJsonData.course_intake.more_details = intakeUrl + "";
                                resJsonData.course_intake.intake = formatIntake;
                                let valmonths11 = "";


                            }
                            break;
                        }
 

                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }


                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        // case 'course_overview':
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                console.log("Outcome is--->" + JSON.stringify(selList))
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }
                        case 'course_study_level':
                            {
                                const cTitle = courseTitle1
                                if(cTitle.includes('in')){
                                    var cTitle2 = cTitle.split('in')[0].trim()
                                }
                                if(cTitle.includes('of')){
                                    var cTitle2 = cTitle.split('of')[0].trim()
                                }
                                resJsonData.course_study_level = cTitle2
                            }


                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            // console.log('\n\r' + funcName + 'course resJsonData =' + JSON.stringify(resJsonData));
            // var resJsonData = JSON.parse(fs.readFileSync("australiancatholicuniversity_graduatediplomainrehabilitation_coursedetails.json"))[0];
            var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(':', '').replace('**', '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                //  console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.courseKeyVal));
                //  console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                // NEWJSONSTRUCT.course_campus_location = location;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;

                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                // var intakes = resJsonData.course_intake.intake;


                var matchrec = [];
                // for (let dintake of intakes) {
                //     if (location == dintake.name) {
                //         matchrec = dintake.value;
                //     }
                // }
                // if (matchrec.length > 0) {
                //     NEWJSONSTRUCT.course_intake = matchrec[0];
                // }
                // else {
                //     NEWJSONSTRUCT.course_intake = [];
                // }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        //  NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                    // else
                    // {
                    //     NEWJSONSTRUCT.international_student_all_fees = [];
                    // }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                //resJsonData.course_cricos_code = location_wise_data.cricos_code;
                console.log("location::::" + location_wise_data.course_campus_location);
                // resJsonData.current_course_campus_location = location_wise_data.course_campus_location;
                //resJsonData.current_course_intake = location_wise_data.course_intake;
                // resJsonData.course_intake_display = location_wise_data.course_intake;
                //   resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;


                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
                // const res = await awsUtil.putFileInBucket(filelocation);
                //console.log(funcName + 'S3 object location = ' + res);
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
