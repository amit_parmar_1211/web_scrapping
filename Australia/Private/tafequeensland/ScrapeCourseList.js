const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  async scrapeOnlyInternationalCourseList() {
    let s = null;
    const funcName = 'startScrappingFunc ';
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var datalist = [];
      await s.setupNewBrowserPage("https://tafeqld.edu.au/information-for/international-students/courses-for-international-students.html?");
      var mainCategory = [], redirecturl = [];
      //scrape main category
      const maincategoryselectorurl = "//*[@id='ways-to-study']/div[1]/div[2]/div/ul/li/a";
      const maincategoryselector = "//*[@id='ways-to-study']/div[1]/div[2]/div/ul/li/a//span";
      var category = await s.page.$x(maincategoryselector);
      var categoryurl = await s.page.$x(maincategoryselectorurl);
      console.log("Total categories-->" + category.length);
      for (let i = 0; i < category.length; i++) {
        var categorystringmain = await s.page.evaluate(el => el.innerText, category[i]);
        var categorystringmainurl = await s.page.evaluate(el => el.href, categoryurl[i]);
        mainCategory.push(categorystringmain.trim());
        redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
      }


      //scrape courses
      for (let catc of redirecturl) {
        await s.page.goto(catc.href, { timeout: 0 });
        var ispage = true;
        while (ispage) {
          const selector = "//*[@id='search-results-courses']/div/div[1]/a";
          var title = await s.page.$x(selector);
          for (let t of title) {
            var categorystring = await s.page.evaluate(el => el.innerText, t);
            var categoryurl = await s.page.evaluate(el => el.href, t);
            datalist.push({ href: categoryurl, innerText: categorystring.replace(/[\r\n\t]+/g, ' ').trim(), category: catc.innerText });
          }
          const pageselector = "//*/ul/li[@class='page-item next']";
          var pageel = await s.page.$x(pageselector);
          if (pageel[0]) {
            await pageel[0].click();
            console.log("Next page.");
          }
          else {
            ispage = false;
            console.log("Done for - " + catc.innerText);
          }
        }

      }

      var uniquecases = [], uniquedata = [], finalDict = [];
      datalist.forEach(element => {
        if (!uniquecases.includes(element.href)) {
          uniquecases.push(element.href);
          uniquedata.push(element);
        }
      });
      uniquecases.forEach(element => {
        var dataobj = {};
        var data = datalist.filter(e => e.href == element);
        var category = [];
        data.forEach(element => {
          if (!category.includes(element.category))
            category.push(element.category);
        });
        dataobj.href = element;
        dataobj.innerText = data[0].innerText;
        dataobj.category = (Array.isArray(category)) ? category : category;
        finalDict.push(dataobj);
      });
      fs.writeFileSync("./output/tafequeensland-all_courselist.json", JSON.stringify(finalDict));
      fs.writeFileSync("./output/tafequeensland_courselist.json", JSON.stringify(finalDict));
      fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

} // class

module.exports = { ScrapeCourseList };
