const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, category) {
        const funcName = 'formatOutput ';
        var istaken = false;
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                // var demoarray = [];
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = category;
                            // As of not we do not have way to get this field value
                            break;
                        }
                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            // .replace('\n', '')
                            // title = title.replace('(', '( ').replace('\n', '');
                            console.log("splitStr@@@2" + title);
                            var ctitle = format_functions.titleCase(title).trim();
                            // var ctitle2 = ctitle.replace(' ( ', '(');
                            console.log("ctitle@@@", ctitle.trim());
                            resJsonData.course_title = ctitle
                            break;
                        }
                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; let ieltsNumber = null;
                            var ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_req);
                            console.log("ielts_req" + await utils.giveMeNumber(ielts_req.replace(/ /g, ' ')))
                            //   ielts_req111 = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                            if (ielts_req) {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ielts_req;
                                ieltsDict.min = 0;
                                ieltsDict.require = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0
                                englishList.push(ieltsDict);
                            }
                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            } else {
                                courseAdminReq.english = [];
                            }
                            //var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseScrappedData.course_academic_requirement))
                            if (courseKeyVal == null || courseKeyVal == '') {
                                courseAdminReq.academic = []
                            }
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList;

                                        }
                                    }
                                }
                            }
                            console.log("AcademicRequirement@@@@", JSON.stringify(academicReq))
                            courseAdminReq.academic = (academicReq.toString()) ? [academicReq.toString()] : [];
                            courseAdminReq.english_requirements_url = await utils.getValueFromHardCodedJsonFile('english_requirements_url');
                            courseAdminReq.academic_requirements_url = english_more;
                            courseAdminReq.entry_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            console.log("CRGPl", JSON.stringify(courseScrappedData.course_url));
                            const courseKeyVal = courseScrappedData.course_url;
                            var url = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));

                                            url = selList;

                                        }
                                    }
                                }
                            }
                            resJsonData.course_url = url;
                            break;
                        }
                        case 'course_duration_full_time': {

                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            console.log("DURATION------>", JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("FullTime------>", JSON.stringify(fullTimeText))
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                resJsonData.course_duration = String(fullTimeText);
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);

                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston
                            var campLocationText = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));

                                            campLocationText = selList;

                                        }
                                    }
                                }
                            }


                            const courseKeyVal11 = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyVal11)) {
                                for (const rootEleDict of courseKeyVal11) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("demoarray123 -->", demoarray);
                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            if (campLocationText && campLocationText[0].length > 0) {
                                istaken = true;
                                Array.prototype.unique = function () {
                                    var arr = [];
                                    for (var i = 0; i < this.length; i++) {
                                        if (!arr.includes(this[i])) {
                                            arr.push(this[i]);
                                        }
                                    }
                                    return arr;
                                }
                                var uniques = campLocationText[0].unique();
                                var campusedata = [];
                                uniques.forEach(element => {
                                    campusedata.push({
                                        "name": format_functions.titleCase(element),
                                        "code": course_cricos_code.toString()
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                var intakes = [];
                                const courseTuitionFee = {};
                                const feesList = [];
                                const feesDict = {};
                                var durationarray = []
                                for (let locuni of uniques) {
                                    var intakedetail = {};
                                    var myintake = [];
                                    var feetaken = false;
                                    for (var count = 0; count < campLocationText[0].length; count++) {

                                        if (locuni == campLocationText[0][count]) {
                                            if (myintake.indexOf(campLocationText[3][count]) == -1) {
                                                myintake.push(campLocationText[3][count]);
                                                if (!intakedetail.name) {
                                                    intakedetail.name = campLocationText[0][count].trim();
                                                }
                                            }
                                            let dutration_val = resJsonData.course_duration_display;
                                            console.log("dutration_val====>>>111", dutration_val)
                                            if (!feetaken) {
                                                var display = await format_functions.validate_course_duration_full_time(campLocationText[1][count]);
                                                console.log("DIsplay--->", JSON.stringify(campLocationText[1][count]))
                                                durationarray.push({ name: locuni, course_duration: campLocationText[1][count] + " Workload (" + campLocationText[4][count] + ")", course_duration_display: display })
                                                console.log("DURAtion@@@@", JSON.stringify(durationarray))
                                                const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);

                                                if (feesDuration && feesDuration.length > 0) {
                                                    feesDict.fee_duration_years = feesDuration;
                                                }
                                                if (feesCurrency && feesCurrency.length > 0) {
                                                    feesDict.currency = feesCurrency;
                                                }
                                                let dura = campLocationText[1][count].split(' ');
                                                console.log("dura=======>>>", dura)
                                                feesDict.international_student = {};
                                                feesDict.international_student = ({
                                                    amount: await utils.giveMeNumber(campLocationText[2][count].toString().replace(/[$\r\n\t ]+/g, ' ')),
                                                    duration: await utils.giveMeNumber(dura[0]),
                                                    unit: dura[1],
                                                    description: campLocationText[2][count].toString(),
                                                });


                                                const feesinfo = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_info);
                                                console.log("FeesInfo--->", JSON.stringify(feesinfo))


                                                feesList.push({ name: campLocationText[0][count].trim(), value: feesDict });
                                                if (feesList && feesList.length > 0) {
                                                    courseTuitionFee.fees = feesList;
                                                }
                                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                                if (courseTuitionFee) {
                                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                                    feetaken = true;
                                                }
                                            }
                                        }
                                    }
                                    intakedetail.value = myintake;
                                    intakes.push(intakedetail);
                                }
                                var intakedata = {};
                                let arr = [];
                                intakedata.intake = await format_functions.providemyintake(intakes, "MOM", "");
                                intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');

                                console.log("DATA@@@@", JSON.stringify(arr))
                                resJsonData.durationarray = durationarray;
                                resJsonData.course_intake = intakedata;//courseIn
                                console.log("campus" + JSON.stringify(resJsonData.course_campus_location))
                                var study_mode = "";
                                var campLocationValTrimmed = String(resJsonData.course_campus_location).trim().toLowerCase();
                                if (campLocationValTrimmed.includes('distance')) {
                                    study_mode.push('Distance');
                                    campLocationValTrimmed = campLocationValTrimmed.replace("distance", "");
                                }
                                if (campLocationValTrimmed.includes('online')) {
                                    study_mode.push('Online');
                                    campLocationValTrimmed = campLocationValTrimmed.replace("online", "");
                                }
                                campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                                if (campLocationValTrimmed.trim().length > 0) {
                                    study_mode = 'On campus';
                                }
                                resJsonData.course_study_mode = study_mode;//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);

                            }
                            break;
                        }
                        case 'course_campus_location_local': { // Location Launceston
                            console.log("111course_campus_location_local" + istaken);
                            if (!istaken) {
                                var campLocationText = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                                const courseKeyVal = courseScrappedData.course_campus_location_local;
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));

                                                campLocationText = selList;

                                            }
                                        }
                                    }
                                }
                                console.log("##Campus-->" + JSON.stringify(campLocationText))


                                const courseKeyVal11 = courseScrappedData.course_cricos_code;
                                let course_cricos_code = null;
                                if (Array.isArray(courseKeyVal11)) {
                                    for (const rootEleDict of courseKeyVal11) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    course_cricos_code = selList;
                                                }
                                            }
                                        }
                                    }
                                }


                                if (campLocationText && campLocationText[0].length > 0) {
                                    istaken = true;
                                    Array.prototype.unique = function () {
                                        var arr = [];
                                        for (var i = 0; i < this.length; i++) {
                                            if (!arr.includes(this[i])) {
                                                arr.push(this[i]);
                                            }
                                        }
                                        return arr;
                                    }
                                    var uniques = campLocationText[0].unique();
                                    var campusedata = [];
                                    uniques.forEach(element => {
                                        campusedata.push({
                                            "name": element,
                                            "code": course_cricos_code.toString()
                                        })
                                    });
                                    resJsonData.course_campus_location = campusedata;

                                    var intakes = [];
                                    const courseTuitionFee = {};
                                    const feesList = [];
                                    const feesDict = {};
                                    var durationarray = []
                                    for (let locuni of uniques) {
                                        var intakedetail = {};
                                        var myintake = [];
                                        var feetaken = false;
                                        for (var count = 0; count < campLocationText[0].length; count++) {

                                            if (locuni == campLocationText[0][count]) {
                                                if (myintake.indexOf(campLocationText[3][count]) == -1) {
                                                    myintake.push(campLocationText[3][count]);
                                                    if (!intakedetail.name) {
                                                        intakedetail.name = campLocationText[0][count].trim();
                                                    }
                                                }
                                                let dutration_val = resJsonData.course_duration_display;
                                                console.log("dutration_val====>>>222", dutration_val)
                                                if (!feetaken) {
                                                    var display = await format_functions.validate_course_duration_full_time(campLocationText[1][count]);
                                                    durationarray.push({ name: locuni, course_duration: campLocationText[1][count] + " Workload (" + campLocationText[4][count] + ")", course_duration_display: display })
                                                    const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                                                    const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                                    const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                                    if (feeYear && feeYear.length > 0) {
                                                        courseTuitionFee.year = feeYear;
                                                    }
                                                    if (feesDuration && feesDuration.length > 0) {
                                                        feesDict.fee_duration_years = feesDuration;
                                                    }
                                                    if (feesCurrency && feesCurrency.length > 0) {
                                                        feesDict.currency = feesCurrency;
                                                    }
                                                    let dura11 = campLocationText[1][count].split(' ');
                                                    console.log("dura=======>>>", dura11)
                                                    feesDict.international_student = {};
                                                    feesDict.international_student = ({
                                                        amount: await utils.giveMeNumber(campLocationText[2][count].toString().replace(/[$\r\n\t ]+/g, ' ')),
                                                        duration: await utils.giveMeNumber(dura11[0]),
                                                        unit: dura11[1],
                                                        description: campLocationText[2][count].toString().replace(/[$\r\n\t ]+/g, ' ').trim(),
                                                    });

                                                    feesList.push({ name: campLocationText[0][count].trim(), value: feesDict, iscurrent: false });
                                                    if (feesList && feesList.length > 0) {
                                                        courseTuitionFee.fees = feesList;
                                                    }
                                                    console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                                    if (courseTuitionFee) {
                                                        resJsonData.course_tuition_fee = courseTuitionFee;
                                                        feetaken = true;
                                                    }
                                                }
                                            }
                                        }
                                        intakedetail.value = myintake;
                                        intakes.push(intakedetail);
                                    }
                                    var intakedata = {};

                                    intakedata.intake = await format_functions.providemyintake(intakes, "MOM", "");
                                    intakedata.more_details = [await utils.getValueFromHardCodedJsonFile('intake_url')];
                                    resJsonData.durationarray = durationarray;
                                    resJsonData.course_intake = intakedata;//courseIn
                                    console.log("campus" + JSON.stringify(resJsonData.course_campus_location))
                                    var study_mode = "";
                                    var campLocationValTrimmed = String(resJsonData.course_campus_location).trim().toLowerCase();
                                    if (campLocationValTrimmed.includes('distance')) {
                                        study_mode.push('Distance');
                                        campLocationValTrimmed = campLocationValTrimmed.replace("distance", "");
                                    }
                                    if (campLocationValTrimmed.includes('online')) {
                                        study_mode.push('Online');
                                        campLocationValTrimmed = campLocationValTrimmed.replace("online", "");
                                    }
                                    campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                                    if (campLocationValTrimmed.trim().length > 0) {
                                        study_mode = 'On campus';
                                    }
                                    resJsonData.course_study_mode = study_mode;//.join(',');
                                    console.log("## FInal string-->" + campLocationValTrimmed);

                                }
                            }
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student':
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }

                            resJsonData.program_code = program_code.join(", ");

                            break;
                        }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));

                                            course_country = selList[0];

                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            resJsonData.course_overview = course_overview;
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                console.log("Outcome is--->" + JSON.stringify(selList))
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome
                            } else {
                                course_career_outcome = [];
                            }
                            break;
                        }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: resJsonData.course_url
                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                resJsonData.application_fee = "";
                                break;
                            }
                        case 'course_study_level': {
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            const title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);

                            console.log(funcName + 'course_title course_study_level = ' + cTitle);
                            console.log("CourseTitle", title)
                            if (title.includes("/") && cTitle.includes("/")) {
                                resJsonData.course_study_level = "Package Course";
                            } else {
                                const cStudyLevel = await format_functions.getMyStudyLevel(cTitle)
                                console.log("Study_level", cStudyLevel)
                                if (cStudyLevel) {
                                    resJsonData.course_study_level = cStudyLevel;
                                }
                            }
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        }
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                }
            }
            var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;

                var duration = resJsonData.durationarray;
                duration.forEach(element => {
                    if (element.name == location.name) {
                        var durationd = [];
                        NEWJSONSTRUCT.course_duration = element.course_duration;
                        durationd.push(element.course_duration_display);
                        NEWJSONSTRUCT.course_duration_display = durationd;
                    }
                });
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                //   resJsonData.basecourseid = location_wise_data.course_id;
                resJsonData.course_duration = location_wise_data.course_duration;
                delete resJsonData.durationarray;
                let filtered_duration_formated = await format_functions.getfilterduration(location_wise_data.course_duration_display[0]);
                console.log("filtered_duration_formated", filtered_duration_formated);
                console.log("CourseDUration@@@", JSON.stringify(courseDurationList))
                if (courseDurationList && courseDurationList.length > 0) {
                    resJsonData.course_duration = courseDurationList;
                }
                if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                    resJsonData.course_duration_display = filtered_duration_formated;
                    var isfulltime = false, isparttime = false;
                    filtered_duration_formated.forEach(element => {
                        if (element.display == "Full-Time") {
                            isfulltime = true;
                        }
                        if (element.display == "Part-Time") {
                            isparttime = true;
                        }
                    });
                    resJsonData.isfulltime = isfulltime;
                    resJsonData.isparttime = isparttime;
                }

                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                await fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }
    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            }
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
