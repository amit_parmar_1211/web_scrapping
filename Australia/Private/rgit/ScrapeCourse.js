const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = course_category;
                            // As of not we do not have way to get this field value
                            break;
                        }
                        case 'course_title': {
                            let t1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);

                            console.log("resJsonData.course_title3333", resJsonData.course_title);

                            // var title1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            // var title = format_functions.titleCase(title1).trim();

                            const title = t1.trim();
                            console.log("title ", title);
                            resJsonData.program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            const pro = String(resJsonData.program_code);
                            console.log("programwithtitle ", pro);
                            if (title.indexOf(pro) > -1) {
                                var splitCricos = title.split(pro)[1];
                                console.log("gdzdhghf", splitCricos);
                                resJsonData.course_title = splitCricos.trim();
                            } else {
                                resJsonData.course_title = t1;
                                console.log("gdzdsgdfgzdhghf", splitCricos);
                            }
                        }
                        // case 'course_title': {
                        //     resJsonData.course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                        //     console.log("resJsonData.course_title3333", resJsonData.course_title);
                        //     // const tt = String(resJsonData.course_title).split(' ')[1];
                        //     // console.log("tt@@@", tt);

                        //    // resJsonData.course_title = tt;

                        // }

                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; let ieltsNumber = null;
                            let courseKeyVal_ielts = courseScrappedData.ielts_req;
                            let ielts_req = ""
                            if (Array.isArray(courseKeyVal_ielts)) {
                                for (const rootEleDict of courseKeyVal_ielts) {
                                    console.log(funcName + '\n\r rootEleDict ieltsDict= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict ieltsDict= ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList ieltsDict= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList)) {
                                                for (let sel of selList) {
                                                    if (ielts_req != sel) {
                                                        ielts_req = sel;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            console.log("##ielts_req-->" + ielts_req);
                            if (ielts_req.length == 0) {
                                //  ielts_req = "Academic IELTS of 5.5 with 5.0 in all bands, or equivalent.";
                            }

                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppteDict = {};
                            var penglishList = [];
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                            if (ielts_req) {
                                ieltsScore = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                            }


                            if (ieltsScore) {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = ielts_req;
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsScore;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }



                            else {
                                console.log("ieltsScore@@@", ieltsScore);
                                if (ieltsScore.length == 0) {
                                    console.log("ieltsScore@@@@@@@", ieltsScore);


                                    pieltsDict.name = 'ielts academic';
                                    pieltsDict.description = ielts_req;
                                    pieltsDict.min = 0;
                                    pieltsDict.require = ieltsScore;
                                    pieltsDict.max = 9;
                                    pieltsDict.R = 0;
                                    pieltsDict.W = 0;
                                    pieltsDict.S = 0;
                                    pieltsDict.L = 0;
                                    pieltsDict.O = 0;
                                    penglishList.push(pieltsDict);
                                }
                            }
                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }
                            // else {
                            //     throw new Error("english not found");
                            // }

                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var academicReq = "";
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList;
                                        }
                                    }
                                }
                            }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            courseAdminReq.entry_requirements_url = "";
                            courseAdminReq.english_requirements_url = english_more;
                            courseAdminReq.academic_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                                console.log("##fullTimeText--->" + JSON.stringify(fullTimeText));
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston
                            var campLocationText; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            const Cricos = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log("Cricos---->", Cricos)

                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList;

                                            ///}
                                        }
                                    }
                                }
                            }
                            let newcampus = [];
                            for (let campus of campLocationText) {
                                if (!newcampus.includes(campus)) {
                                    newcampus.push(campus);
                                    console.log("##Campuscampus-->" + campus)
                                }
                            }
                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            //   let valfainal =campLocationText.toString();
                            var finallocation = [];

                            if (String(campLocationText).includes('and')) {
                                var local = String(campLocationText).replace("and", ',');
                                console.log("local----->", local)
                            }
                            else {
                                local = campLocationText
                            }

                            var loca = String(local).split(',');
                            loca.forEach(element => {
                                if (element.includes('Campus')) {
                                    finallocation.push(element.replace('Campus', ''))
                                }
                                else {
                                    finallocation.push(element)
                                    console.log("finallocation------->", finallocation)
                                }
                            })
                            var campusss = [];
                            if (finallocation.length > 0) {
                                console.log("loca------>", finallocation);
                                let campuses = ['Melbourne', 'Hobart']
                                console.log("@@campuses------->", campuses);

                                for (let i = 0; i < finallocation.length; i++) {
                                    campuses.forEach(element => {
                                        console.log(element.includes(finallocation[i].trim()))
                                        if (element.includes(finallocation[i].trim())) {

                                            campusss.push(element)

                                        }
                                    })
                                }
                                console.log("@@campuses------->", campusss)


                                if (campusss && campusss.length > 0) {
                                    var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                    resJsonData.course_study_mode = "On campus";
                                    //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                    // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                    var campusedata = [];
                                    campusss.forEach(element => {
                                        campusedata.push({
                                            "name": format_functions.titleCase(element).trim(),
                                            "code": Cricos.trim()
                                        })
                                    });
                                    resJsonData.course_campus_location = campusedata;
                                    // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                    var study_mode = [];
                                    // campLocationValTrimmed = campLocationValTrimmed.toLowerCase();

                                    // campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                                    if (campLocationValTrimmed.trim().length > 0) {
                                        study_mode.push('On campus');
                                    }
                                    resJsonData.course_study_mode = study_mode.toString();//.join(',');
                                    console.log("## FInal string-->" + campLocationValTrimmed);
                                    // }

                                }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_study_mode': { // Location Launceston
                            //resJsonData.course_study_mode = coursestudymodeText;
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            const courseKeyVal = courseScrappedData.course_tuition_fees_international_student;
                            const courseKeyValAdditional = courseScrappedData.course_tuition_fees_international_student_additional;
                            console.log("courseKeyValAdditional -->", JSON.stringify(courseKeyValAdditional));
                            let feesIntStudent = [];
                            let feesIntStudentAdditional = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                feesIntStudent = selList;
                                            }
                                        }
                                    }
                                }
                            }

                            //External study mode fees are different
                            if (Array.isArray(courseKeyValAdditional)) {
                                for (const rootEleDict of courseKeyValAdditional) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                feesIntStudentAdditional = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (feesIntStudentAdditional && feesIntStudentAdditional.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudentAdditional).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                                const arrval = String(feesVal1).split('.');

                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesIntStudentAdditional = Number(feesNumber);
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)


                            console.log("feesIntStudentAdditional -->", feesIntStudentAdditional);
                            //
                            // if (feeYear && feeYear.length > 0) {
                            //   courseTuitionFee.year = feeYear;
                            // }

                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                                const arrval = String(feesVal1).split('.');

                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,

                                                description: feesVal

                                            });
                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,
                                                description: ""
                                            });
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: Number(demoarray.duration),
                                    unit: demoarray.unit,
                                    description: "",

                                });
                            }
                            if (feesIntStudent.length > 0) { // extract only digits
                                let arr = [];
                                if (feesIntStudentAdditional == '' || feesIntStudentAdditional == undefined) {

                                } else {
                                    feesIntStudentAdditional = "External/Online - AUD " + feesIntStudentAdditional;
                                    arr.push(feesIntStudentAdditional);
                                    feesIntStudent = "On-campus - " + feesIntStudent;
                                    arr.push(feesIntStudent);
                                }

                                //  feesDict.international_student_all_fees = arr;
                            } else {
                                let arr = [];

                                //feesDict.international_student_all_fees = arr;
                            }
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    // feesList.push(feesDict);
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                console.log("feesDict.international_student = ", feesDict.international_student);
                                console.log("resJsonData.course_tuition_fee_amount = ", resJsonData.course_tuition_fee_amount);
                            }

                            break;
                        }
                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };

                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

                            let resScholarshipJson = null;


                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }




                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }

                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)

                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;


                            break;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);

                                // let applicationfee = JSON.parse(fs.readFileSync(appConfigs.Locations));
                                // console.log("cricosIntStudent----->>>>>", applicationfee);
                                // var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                                // console.log("splitStr@@@2" + title);
                                // let tmpvar = applicationfee.filter(element => {
                                //     return element.Course_Name.toLowerCase().trim() == title.toLowerCase().trim()
                                // });
                                // console.log("tmpvar----->>>>>", tmpvar);
                                // let application = tmpvar[0].Application_Fee;
                                // console.log("one----->>>>>", application);
                                resJsonData.application_fee = ""
                                break;
                            }

                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = ""
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (program_code.length > 0) {
                                for (let arr of program_code) {

                                    resJsonData.program_code = arr;
                                }
                            }
                            else {
                                resJsonData.program_code = "";
                            }

                            break;
                        }
                        case 'course_intake': {
                            var courseIntakeStr = [];
                            // existing intake value
                            const courseIntakeStr1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            var campus = resJsonData.course_campus_location
                            if (String(courseIntakeStr1).includes("Weekly")) {
                                if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                                    console.log("In Main If", courseIntakeStr1);
                                    var myintake = [];
                                    var intakedata = {}
                                    if (String(courseIntakeStr1).indexOf("Weekly") > -1) {



                                        var arr = [
                                            {
                                                "actualdate": "anytime",
                                                "month": ""

                                            }]

                                        console.log("arr--->", arr)
                                        console.log("campus.length--->", campus.length)
                                        for (let i = 0; i < campus.length; i++) {
                                            var intakedetail = {};
                                            console.log("campus--->", campus[i].name)
                                            intakedetail.name = campus[i].name;
                                            intakedetail.value = arr;
                                            myintake.push(intakedetail)
                                            console.log("CourseDetails-->", JSON.stringify(myintake))

                                        }
                                        intakedata.intake = myintake
                                        let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                        intakedata.more_details = more_details;
                                        resJsonData.course_intake = intakedata;

                                    }
                                }
                            }

                            else {
                                if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                                    resJsonData.course_intake = courseIntakeStr1;
                                    const intakeStrList = String(courseIntakeStr1).split(';');
                                    console.log(funcName + 'intakeStrList course_intake = ' + JSON.stringify(intakeStrList));
                                    courseIntakeStr = intakeStrList;
                                }
                                console.log("intakes" + JSON.stringify(courseIntakeStr));

                                const d = String(courseIntakeStr).split(' ');
                                console.log("dvgdgffdhrh" + JSON.stringify(d));

                                if (d && d.length > 0) {

                                    console.log('Campuse location :' + campus);
                                    var intakes = [];
                                    console.log("Intake length-->" + d.length);
                                    console.log("Campus length-->" + campus.length);
                                    for (var camcount = 0; camcount < campus.length; camcount++) {
                                        var intakedetail = {};
                                        intakedetail.name = campus[camcount].name;
                                        intakedetail.value = d;
                                        intakes.push(intakedetail);
                                        // console.log("Intakekkeee", intakes);
                                    }
                                    console.log("Intakekkeee", intakes);
                                    let formatedIntake = await format_functions.providemyintake(intakes, "mom", "");
                                    console.log("FormatIntake", JSON.stringify(formatedIntake))
                                    var intakedata = {};
                                    intakedata.intake = formatedIntake;

                                    let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                    intakedata.more_details = more_details;
                                    resJsonData.course_intake = intakedata;
                                }
                            }
                            break;
                        }
                        // case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                        //     let courseIntakeDisplay = [];
                        //     // existing intake value

                        //     // const courseIntakeStrElem = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);

                        //     let courseKeyVal_intake = courseScrappedData.course_intake;
                        //     let courseintake;
                        //     if (Array.isArray(courseKeyVal_intake)) {
                        //         for (const rootEleDict of courseKeyVal_intake) {
                        //             console.log(funcName + '\n\r rootEleDict ieltsDict= ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict ieltsDict= ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList ieltsDict= ' + JSON.stringify(selList));
                        //                     if (Array.isArray(selList)) {
                        //                         for (let sel of selList) {
                        //                             if (courseintake != sel) {
                        //                                 courseintake = sel;
                        //                             }
                        //                         }
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }


                        //     console.log("R locationArray", campus);
                        //     console.log('************************Start course_intake******************************');
                        //     console.log('courseIntakeStr : ' + courseintake);
                        //     //console.log("R courseIntakeStrFile", courseIntakeStrFile);
                        //     // const intakeSplit =[];
                        //     if (courseintake && courseintake.length > 0) {
                        //         resJsonData.course_intake = courseintake;
                        //         //resJsonData.course_intake_display = courseIntakeStr;
                        //         const intakeStrList = String(courseintake).split('\n');
                        //         const intakeSplit = String(intakeStrList).split(',');
                        //         console.log('course_intake intakeStrList = ' + JSON.stringify(intakeSplit));
                        //         //const regEx = /[ ]/g; var semList = [];
                        //         if (intakeSplit && intakeSplit.length > 0) {
                        //             for (var part of intakeSplit) {
                        //                 // part = part.split("(");
                        //                 // part = part[1].split(")")[0];
                        //                 console.log("R part", part);
                        //                 part = part.trim();
                        //                 courseIntakeDisplay.push(part);
                        //             } // for
                        //         }

                        //         console.log('course_intake courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));

                        //     }

                        //     var campus = resJsonData.course_campus_location;

                        //     if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
                        //         console.log("In Main If", courseIntakeDisplay);
                        //         var myintake = [];
                        //         var intakedata = {}
                        //         if (String(courseIntakeDisplay).indexOf("Weekly") > -1) {



                        //             var arr = [
                        //                 {
                        //                     "actualdate": "anytime",
                        //                     "month": "",
                        //                     "intake": "",
                        //                     "filterdate": ""
                        //                 }]

                        //             console.log("arr--->", arr)
                        //             for (i = 0; i < campus.length; i++) {
                        //                 var intakedetail = {};
                        //                 console.log("campus--->", campus[i].name)
                        //                 intakedetail.name = campus[i].name;
                        //                 intakedetail.value = arr;
                        //                 myintake.push(intakedetail)
                        //                 console.log("CourseDetails-->", JSON.stringify(myintake))

                        //             }
                        //             intakedata.intake = myintake
                        //             intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');


                        //             intakedata.more_details = more_details;
                        //             resJsonData.course_intake = intakedata;

                        //         }
                        //         else {
                        //             console.log("Including date", courseIntakeDisplay);
                        //             resJsonData.course_intake = {};
                        //             resJsonData.course_intake.intake = [];
                        //             for (let location of campus) {
                        //                 resJsonData.course_intake.intake.push({
                        //                     "name": location.name,
                        //                     "value": courseIntakeDisplay
                        //                 });
                        //                 let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                        //                 intakedata.more_details = more_details;
                        //             }
                        //             var intake_data = [];
                        //             console.log("intakes123 -->", resJsonData.course_intake.intake);


                        //             let formatedIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "MOM", "");
                        //             console.log("FormatIntake", JSON.stringify(formatedIntake))
                        //             var intakedata = {};
                        //             intakedata.intake = formatedIntake;
                        //             let valmonths11 = "";
                        //             // for (let intakeval of formatedIntake) {
                        //             //     console.log("intakeval--->", intakeval);
                        //             //     for (let month of intakeval.value) {
                        //             //         console.log("month--->", month);
                        //             //         for (valmonths11 of intake_months) {
                        //             //             console.log("valmonths11--->", valmonths11);
                        //             //             if (valmonths11.key == month.month) {
                        //             //                 console.log("month.key--->", valmonths11.key);
                        //             //                 month.filterdate = valmonths11.value;

                        //             //                 // formatedIntake.filterdate.push(valmonths11.value);
                        //             //                 //formatedIntake.push(intakedata.filterdate );
                        //             //                 console.log("intake_months--->", month.filterdate);
                        //             //                 resJsonData.course_intake = intakedata;
                        //             //                 break;
                        //             //             }
                        //             //         }
                        //             //     }
                        //             // }

                        //         }
                        //         let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                        //         console.log("more_details date", more_details);
                        //         intakedata.more_details = more_details;

                        //     }

                        //     break;

                        // }
                        case 'course_scholarship': {
                            const courseKeyVal = courseScrappedData.course_scholarship;
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                resJsonData.course_scholarship = resScholarshipJson;
                            }
                            break;
                        }
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                        case 'course_accomodation_cost': {
                            const courseKeyVal = courseScrappedData.course_accomodation_cost;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_accomodation_cost = resAccomodationCostJson;
                            }
                            break;
                        }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                       //     let course_overview;
                            const course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);

                            // if (Array.isArray(courseKeyVal)) {
                            //     for (const rootEleDict of courseKeyVal) {
                            //         console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                            //         const elementsList = rootEleDict.elements;
                            //         for (const eleDict of elementsList) {
                            //             console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                            //             const selectorsList = eleDict.selectors;
                            //             for (const selList of selectorsList) {
                            //                 console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                            //                 if (Array.isArray(selList) && selList.length > 0) {
                            //                     course_overview = selList;
                            //                     console.log("course_overview----->", course_overview)
                            //                 }
                            //             }
                            //         }
                            //     }
                            // }
                            // if (course_overview > 0) {
                            resJsonData.course_overview = course_overview;
                            // } else {
                            //     resJsonData.course_overview = "";
                            // }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }

                            }
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome
                            } else {
                                resJsonData.course_career_outcome = [];
                            }


                            break;
                        }
                        case 'course_study_level': {
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            // let splitCricos = cTitle.split(':')[1];
                            // console.log("resJsonData.sssss------>>>>>>", splitCricos);

                            const study_val = await format_functions.getMyStudyLevel(cTitle.trim());
                            resJsonData.course_study_level = study_val;
                            console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;

            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                // NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                // console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;

                var intakes = resJsonData.course_intake.intake;

                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }


                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";

                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
