const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData,courseUrl, desciplain, study_level, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            if (title.includes('Business')) {
                                resJsonData.course_discipline = ["Business"]
                            }
                            if (title.includes('Project Management')) {
                                resJsonData.course_discipline = ["Project Management"]
                            }
                            // As of not we do not have way to get this field value
                            break;
                        }
                        case 'application_fee': {
                            resJsonData.application_fee = "200";
                        }
                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            console.log("splitStr@@@2" + title);
                            var ctitle = format_functions.titleCase(title).trim();
                            console.log("ctitle@@@", ctitle.trim());
                            resJsonData.course_title = ctitle
                            break;
                        }
                       
                        case 'ielts_req':
                        case 'ibt_req':
                        case 'cpe_req':
                        case 'cae_req':
                        case 'pte_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; const cpeDict = {};
                            var ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_req);
                            var ibt_req = await Course.extractValueFromScrappedElement(courseScrappedData.ibt_req);
                            var cpe_req = await Course.extractValueFromScrappedElement(courseScrappedData.cpe_req);
                            var cae_req = await Course.extractValueFromScrappedElement(courseScrappedData.cae_req);
                            var pte_req = await Course.extractValueFromScrappedElement(courseScrappedData.pte_req);
                            if (ielts_req) {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ielts_req;
                                ieltsDict.min = 0;
                                ieltsDict.require = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));;
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);
                            }
                            if (ibt_req) {
                                tofelDict.name = 'toefl ibt';
                                tofelDict.description = ibt_req;
                                tofelDict.min = 0;
                                tofelDict.require = await utils.giveMeNumber(ibt_req.replace(/ /g, ' '));;
                                tofelDict.max = 120;
                                tofelDict.R = 0;
                                tofelDict.W = 0;
                                tofelDict.S = 0;
                                tofelDict.L = 0;
                                tofelDict.O = 0;
                                englishList.push(tofelDict);
                            }
                            if (cpe_req) {
                                cpeDict.name = 'cpe';
                                cpeDict.description = cpe_req;
                                cpeDict.min = 80;
                                cpeDict.require = await utils.giveMeNumber(cpe_req.replace(/ /g, ' '));;
                                cpeDict.max = 230;
                                cpeDict.R = 0;
                                cpeDict.W = 0;
                                cpeDict.S = 0;
                                cpeDict.L = 0;
                                cpeDict.O = 0;
                                englishList.push(cpeDict);
                            }
                            if (cae_req) {
                                caeDict.name = 'cae';
                                caeDict.description = cae_req;
                                caeDict.min = 80;
                                caeDict.require = await utils.giveMeNumber(cae_req.replace(/ /g, ' '));;
                                caeDict.max = 230;
                                caeDict.R = 0;
                                caeDict.W = 0;
                                caeDict.S = 0;
                                caeDict.L = 0;
                                caeDict.O = 0;
                                englishList.push(caeDict);
                            }
                            if (pte_req) {
                                pteDict.name = 'pte academic';
                                pteDict.description = pte_req;
                                pteDict.min = 0;
                                pteDict.require = await utils.giveMeNumber(pte_req.replace(/ /g, ' '));;
                                pteDict.max = 90;
                                pteDict.R = 0;
                                pteDict.W = 0;
                                pteDict.S = 0;
                                pteDict.L = 0;
                                pteDict.O = 0;
                                englishList.push(pteDict);
                            }

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }

                            //var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details)
                            var entry_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_entry_more_details)
                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            // console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            academicReq = selList;
                                            //}
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];
                            courseAdminReq.english_requirements_url = english_more;
                            courseAdminReq.academic_requirements_url = academic_more;
                            courseAdminReq.entry_requirements_url = entry_more;
                            //courseAdminReq.academic_more_details = academic_more;
                            resJsonData.course_admission_requirement = courseAdminReq;

                            break;
                        }
                        case 'course_url': {
                            console.log("CRGPl", JSON.stringify(courseScrappedData.course_url));
                            const courseKeyVal = courseScrappedData.course_url;
                            var url = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            url = selList;
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_url = url;
                            break;
                        }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: courseUrl


                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = "", courseDurationList = {};
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime = resFulltime;
                                    courseDurationList = durationFullTime;
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("filtered_duration_formated", filtered_duration_formated);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = courseDurationList;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston
                            var campLocationText = null; //
                            //const courseKeyVal=await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);

                            const courseKeyVal = courseScrappedData.course_campus_location;
                            const cricc = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log("cricc-->", cricc);
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList[0];
                                            ///}
                                        }
                                    }
                                }
                            }

                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            let arr = []
                       
                            var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                            //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                            if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {

                                var campusedata = [];
                                campLocationText.forEach(element => {
                                    campusedata.push({
                                        "name": format_functions.titleCase(element),
                                        "code": cricc,
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                var study_mode = [];
                                campLocationValTrimmed = campLocationValTrimmed.toLowerCase();
                               
                                campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                                if (campLocationValTrimmed.trim().length > 0) {
                                    study_mode.push('On campus');
                                }
                                resJsonData.course_study_mode = study_mode.toString();//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                                // }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};




                            const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                            const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                            if (feesDuration && feesDuration.length > 0) {
                                feesDict.fee_duration_years = feesDuration;
                            }
                            if (feesCurrency && feesCurrency.length > 0) {
                                feesDict.currency = feesCurrency;
                            }
                            var campus = resJsonData.course_campus_location;
                            for (let loc of campus) {
                                var filedata = JSON.parse(await fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                                console.log("filedata----->>>>", filedata);
                                var myfees = filedata["fees"];
                                for (let fee of myfees) {
                                    if (fee.course.toLowerCase() == resJsonData.course_title.toLowerCase()) {
                                        feesDict.international_student = {};
                                        feesDict.international_student = ({
                                            amount: fee.fees,
                                            duration: 1,
                                            unit: "Year",
                                            description: "",

                                        });
                                    }
                                }

                                //feesDict.international_student_all_fees = [];
                                feesList.push({ name: loc.name, value: feesDict });
                            }
                            if (feesList && feesList.length > 0) {
                                courseTuitionFee.fees = feesList;
                            }
                            console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                            if (courseTuitionFee) {
                                resJsonData.course_tuition_fee = courseTuitionFee;
                            }
                            break;
                        }

                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            // if (program_code.length > 0) {
                            resJsonData.program_code = (program_code[0]) ? program_code : "";
                            //}
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            // Semester 1 (February) and Semester 2 (August)
                            // prepare emty JSON struct
                            const courseIntakeDisplay = {};
                            // existing intake value
                            var courseIntakeStr = null;//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;
                            console.log("intake str-?" + JSON.stringify(courseScrappedData.course_intake))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                courseIntakeStr = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (!courseIntakeStr) {
                                courseIntakeStr = [];
                            }
                            if (courseIntakeStr) {
                                var intakes = [];
                                var campus = resJsonData.course_campus_location;
                              
                                var myvalue = [];
                                var filedata = JSON.parse(await fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                                var myintk = filedata["intakes"];
                                if (courseIntakeStr.toString().toLowerCase().trim().includes("semester 1")) {
                                    myvalue.push(myintk.semester1);
                                   
                                }
                                if (courseIntakeStr.toString().toLowerCase().trim().includes("semester 2")) {
                                    myvalue.push(myintk.semester2);
                                   
                                }
                                if (courseIntakeStr.toString().toLowerCase().trim().includes("semester 3")) {
                                    myvalue.push(myintk.semester3);
                                    
                                }
                                if (courseIntakeStr.toString().toLowerCase().trim().includes("trimester 1")) {
                                    myvalue.push(myintk.trimester1);
                                   
                                }
                                if (courseIntakeStr.toString().toLowerCase().trim().includes("trimester 2")) {
                                    myvalue.push(myintk.trimester2);
                                  
                                }
                                if (courseIntakeStr.toString().toLowerCase().trim().includes("trimester 3")) {
                                    myvalue.push(myintk.trimester3);
                                   
                                }
                                // if (courseIntakeStr.toString().toLowerCase().trim().includes("semester 4")) {
                                //     myvalue.push(myintk.semester1);
                                // }
                                // if (courseIntakeStr.toString().toLowerCase().trim().includes("semester 5")) {
                                //     myvalue.push(myintk.semester2);
                                // }
                                // if (courseIntakeStr.toString().toLowerCase().trim().includes("semester 6")) {
                                //     myvalue.push(myintk.semester3);
                                // }

                                for (let loc of campus) {
                                    var intakedetail = {};
                                    intakedetail.name = loc.name;
                                    intakedetail.value = myvalue;
                                    intakes.push(intakedetail);
                                    var intakeUrl = await utils.getValueFromHardCodedJsonFile('intake_url');
                                    let formatIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                    if (formatIntake && formatIntake.length > 0) {
                                        var intakedata = {};
                                        intakedata.intake = formatIntake;
                                        intakedata.more_details = intakeUrl;
                                        resJsonData.course_intake = intakedata;
                                    }

                                }

                            }
                            break;
                        }


                     
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            resJsonData.course_overview = course_overview;
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                console.log("Outcome is--->" + JSON.stringify(selList))
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }

                        case 'course_study_level': {
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            const study_val = await format_functions.getMyStudyLevel(cTitle);
                            resJsonData.course_study_level = study_val;
                            console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                            break;
                        }

                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            // console.log('\n\r' + funcName + 'course resJsonData =' + JSON.stringify(resJsonData));
            // var resJsonData = JSON.parse(fs.readFileSync("australiancatholicuniversity_graduatediplomainrehabilitation_coursedetails.json"))[0];
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        // NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                // resJsonData.basecourseid = location_wise_data.course_id;

                // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                //     if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                //         resJsonData.course_cricos_code[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_cricos_code[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                //     if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_intake.intake[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_intake.intake[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                //     if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                //     if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_campus_location[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_campus_location[i].iscurrent = false;
                //     }
                // }
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }


            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
