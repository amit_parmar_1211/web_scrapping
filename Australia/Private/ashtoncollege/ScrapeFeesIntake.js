const puppeteer = require('puppeteer');
const fs = require("fs");
async function start() {
  const URL = "http://www.ashtoncollege.edu.au/fees-and-intake-dates/#FeesandIntakes";
  let browser = await puppeteer.launch({ headless: false }); //headless:false so we can debug
  let page = await browser.newPage();
  await page.goto(URL, { timeout: 0 });
  var subjectAreasArray = [];
  // let Course_Name = "//*//table[@width='1018']/tbody/tr/td[1][not(contains(text(),'Course Name'))]";
  // let Course_code = "//*//table[@width='1018']/tbody/tr/td[2][not(contains(text(),'Course'))]";
  // let Application_Fee = "//*//table[@width='1018']/tbody/tr/td[3][not(contains(text(),'Application Fee'))]";
  // let Material_Fee = "//*//table[@width='1018']/tbody/tr/td[4][not(contains(text(),'Material Fee'))]";
  // let Tuition_Fee = "//*//table[@width='1018']/tbody/tr/td[5][not(contains(text(),'Tuition Fee'))]";
  // let Duration = "//*//table[@width='1018']/tbody/tr/td[6][not(contains(strong,'Duration'))]"
  // let Intake_Dates = "//*//table[@width='1018']/tbody/tr/td[7][not(contains(text(),'Intake Dates'))]"

  let Course_Name = "//*//table[@class='student'][not(contains(@width,'374'))]/tbody/tr/td[1][(not(contains(text(),'ELICOS'))) and (not(contains(text(),'Course Name')))]";
  let Course_code = "//*//table[@class='student'][not(contains(@width,'374'))]/tbody/tr/td[2][(not(contains(text(),'Course'))) and (not(contains(text(),'08150')))]";
  let Application_Fee = "//*//table[@class='student'][not(contains(@width,'374'))]/tbody/tr/td[3][not(contains(text(),'Application Fee'))]";
  let Material_Fee = "//*//table[@class='student'][not(contains(@width,'374'))]/tbody/tr/td[4][not(contains(text(),'Material Fee'))]";
  let Tuition_Fee = "//*//table[@class='student'][not(contains(@width,'374'))]/tbody/tr/td[5][(not(contains(text(),'Tuition Fee'))) and (not(contains(text(),'week')))]";
  let Duration = "//*//table[@class='student'][not(contains(@width,'374'))]/tbody/tr/td[6][(not(contains(strong,'Duration'))) and (not(contains(text(),'10')))]"
  let Intake_Dates = "//*//table[@class='student'][not(contains(@width,'374'))]/tbody/tr/td[7][(not(contains(text(),'Intake Dates'))) and (not(contains(text(),'Every Month')))]"

  const Course_Name11 = await page.$x(Course_Name);
  const Course_code11 = await page.$x(Course_code);
  const Application_Fee11 = await page.$x(Application_Fee);
  const Material_Fee11 = await page.$x(Material_Fee);
  const Tuition_Fee11 = await page.$x(Tuition_Fee);
  const Duration11 = await page.$x(Duration);
  const Intake_Dates11 = await page.$x(Intake_Dates);

  for (let i = 0; i < Course_Name11.length; i++) {
    let Course_Name22 = await page.evaluate(el => el.textContent, Course_Name11[i]);
    if (Course_Name22.includes('Certificate IV in Commercial Cookery1')) {
      let couresename = 'Certificate IV in Commercial Cookery';
      Course_Name22 = couresename
    }
    if (Course_Name22.includes('Diploma of Hospitality Management2')) {
      let couresename = 'Diploma of Hospitality Management';
      Course_Name22 = couresename
    }
    if (Course_Name22.includes('Certificate III in Light Vehicle Mechanical Technology')) {
      let couresename = 'Cert. III in Light Vehicle Mechanical Technology';
      Course_Name22 = couresename
    }
    let Course_code22 = await page.evaluate(el => el.textContent, Course_code11[i]);
    // let Application_Fee22 = await page.evaluate(el => el.textContent, Application_Fee11[i]);
    // let Material_Fee22 = await page.evaluate(el => el.textContent, Material_Fee11[i]);
    let Tuition_Fee22 = await page.evaluate(el => el.textContent, Tuition_Fee11[i]);
    if (Tuition_Fee22.includes('\n')) {
      let Tuition_Fee111 = Tuition_Fee22.split('\n')[0];
      Tuition_Fee22 = Tuition_Fee111
    }
    let Duration22 = await page.evaluate(el => el.textContent, Duration11[i]);
    let Intake_Dates22 = await page.evaluate(el => el.textContent, Intake_Dates11[i]);
    console.log("Intake_Dates22----->", Intake_Dates22)
    subjectAreasArray.push({
      Course_Name: Course_Name22,
      Course_code: Course_code22,
      // Application_Fee: Application_Fee22,
      // Material_Fee: Material_Fee22,
      Tuition_Fee: Tuition_Fee22,
      Duration: Duration22,
      Intake_Dates: Intake_Dates22
    });
  }
  await fs.writeFileSync("./output/ashtoncollege_fees_intakes_caregary.json", JSON.stringify(subjectAreasArray));
  console.log("name-->", JSON.stringify(subjectAreasArray));
  // await browser.close();
}
start();
