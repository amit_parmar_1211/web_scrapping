// const fs = require('fs');
// const Scrape = require('./common/scrape').Scrape;
// const utils = require('./common/utils');
// const configs = require('./configs');
// const awsUtil = require('./common/aws_utils');
// const appConfigs = require('./common/app-config');
// const puppeteer = require('puppeteer');

// class ScrapeCourseList extends Scrape {
//   // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
//   async scrapeOnlyInternationalCourseList_Mapping(page) {
//     const funcName = 'scrapeCourseCategoryList ';
//     try {
//       // category of course
//       var subjectAreasArray = [];
//       var datalist = [], mainCategory = [];
//       var name;
//       //scrape main category
//       // "//*[@class='heading heading-with-icon icon-left']/a//h2[not(contains(text(),'ENGLISH'))]"
//       // "//*[@class='heading heading-with-icon icon-left']/a//h2[(contains(text(),'BUSINESS'))]/following::div[2]//p//a"
//       // "//*[@id='main']/h3[contains(text(),'" + catc + "')]/following-sibling::table[1]/tbody/tr/td/a"

//       const category_val = "(//*[@id='menu-item-2286']/a)[contains(text(),'Fees and Intake Dates')][1]/preceding::a[contains(text(),'Hospitality Courses') or contains(text(),'Business Courses') or contains(text(),'Automotive Courses')]";
//       const levels = await page.$x(category_val)
//       for (let country of levels) {
//         name = await page.evaluate(el => el.textContent, country);
//         subjectAreasArray.push({ name: name });
//         mainCategory.push(name);
//       }
//       console.log("subjectAreasArray-->", subjectAreasArray)
//       for (let catc of mainCategory) {
//         const subjects = await page.$x("(//*[@id='menu-item-2286']/a)[contains(text(),'Fees and Intake Dates')][1]/preceding::a[contains(text(),'" + catc + "')]/../ul/li/a");

//         for (let i = 0; i < subjects.length; i++) {


//           var elementlink = await page.evaluate(el => el.href, subjects[i])
//           var elementstring = await page.evaluate(el => el.innerText, subjects[i]);
//           datalist.push({ href: elementlink, innerText: elementstring, category: catc })
//         }
//       }
//       fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(mainCategory))
//       console.log("country length-->" + JSON.stringify(datalist));

//       await fs.writeFileSync("./output/ashtoncollege_original_courselist.json", JSON.stringify(datalist));
//       let uniqueUrl = [];
//       //unique url from the courselist file
//       for (let i = 0; i < datalist.length; i++) {
//         let cnt = 0;
//         if (uniqueUrl.length > 0) {
//           for (let j = 0; j < uniqueUrl.length; j++) {
//             if (datalist[i].href == uniqueUrl[j].href) {
//               cnt = 0;
//               break;
//             } else {
//               cnt++;
//             }
//           }
//           if (cnt > 0) {
//             uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [] });
//           }
//         } else {
//           uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innerText, category: [] });
//         }
//       }
//       await fs.writeFileSync("./output/ashtoncollege_unique_courselist.json", JSON.stringify(uniqueUrl));
//       //based on unique urls mapping of categories
//       for (let i = 0; i < datalist.length; i++) {
//         for (let j = 0; j < uniqueUrl.length; j++) {
//           if (uniqueUrl[j].href == datalist[i].href) {
//             uniqueUrl[j].category.push(datalist[i].category);
//           }
//         }
//       }
//       console.log("totalCourseList -->", uniqueUrl);
//       await fs.writeFileSync("./output/ashtoncollege_courselist.json", JSON.stringify(uniqueUrl));
//       console.log(funcName + 'writing courseList to file completed successfully....');
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//     }
//   }
//   async scrapeintakesfess() {
//     const URL = "http://www.ashtoncollege.edu.au/fees-and-intake-dates/#FeesandIntakes";
//     let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
//     let page = await browser.newPage();
//     await page.goto(URL, { timeout: 0 });
//     var subjectAreasArray = [];
//     //let Course_Name = "//*//table[@width='1018']/tbody/tr/td[1][not(contains(text(),'Course Name'))]";
//     let Course_Name = "//*//table[@class='student cc_cursor']/tbody/tr/td[1][(not(contains(text(),'ELICOS'))) and (not(contains(text(),'Course Name')))]";
//     let Course_code = "//*//table[@class='student cc_cursor']/tbody/tr/td[2][(not(contains(text(),'Course'))) and (not(contains(text(),'08150')))]";
//     // let Application_Fee = "//*//table[@width='1018']/tbody/tr/td[3][not(contains(text(),'Application Fee'))]";
//     // let Material_Fee = "//*//table[@width='1018']/tbody/tr/td[4][not(contains(text(),'Material Fee'))]";
//     let Tuition_Fee = "//*//table[@class='student cc_cursor']/tbody/tr/td[5][(not(contains(text(),'Tuition Fee'))) and (not(contains(text(),'week')))]";
//     let Duration = "//*//table[@class='student cc_cursor']/tbody/tr/td[6][(not(contains(strong,'Duration'))) and (not(contains(text(),'10')))]"
//     let Intake_Dates = "//*//table[@class='student cc_cursor']/tbody/tr/td[7][(not(contains(text(),'Intake Dates'))) and (not(contains(text(),'Every Month')))]"

//     const Course_Name11 = await page.$x(Course_Name);

//     const Course_code11 = await page.$x(Course_code);
//     // const Application_Fee11 = await page.$x(Application_Fee);
//     // const Material_Fee11 = await page.$x(Material_Fee);
//     const Tuition_Fee11 = await page.$x(Tuition_Fee);
//     const Duration11 = await page.$x(Duration);
//     const Intake_Dates11 = await page.$x(Intake_Dates);

//     for (let i = 0; i < Course_Name11.length; i++) {
//       let Course_Name22 = await page.evaluate(el => el.textContent, Course_Name11[i]);
//       if (Course_Name22.includes('Certificate IV in Commercial Cookery1')) {
//         let couresename = 'Certificate IV in Commercial Cookery';
//         Course_Name22 = couresename
//       }
//       if (Course_Name22.includes('Diploma of Hospitality Management2')) {
//         let couresename = 'Diploma of Hospitality Management';
//         Course_Name22 = couresename
//       }
//       let Course_code22 = await page.evaluate(el => el.textContent, Course_code11[i]);
//       // let Application_Fee22 = await page.evaluate(el => el.textContent, Application_Fee11[i]);
//       // let Material_Fee22 = await page.evaluate(el => el.textContent, Material_Fee11[i]);
//       let Tuition_Fee22 = await page.evaluate(el => el.textContent, Tuition_Fee11[i]);
//       if (Tuition_Fee22.includes('\n')) {
//         let Tuition_Fee111 = Tuition_Fee22.split('\n')[0];
//         Tuition_Fee22 = Tuition_Fee111
//       }
//       let Duration22 = await page.evaluate(el => el.textContent, Duration11[i]);
//       let Intake_Dates22 = await page.evaluate(el => el.textContent, Intake_Dates11[i]);
//       console.log("Intake_Dates22----->", Intake_Dates22)
//       subjectAreasArray.push({
//         Course_Name: Course_Name22,
//         Course_code: Course_code22,
//         // Application_Fee: Application_Fee22,
//         // Material_Fee: Material_Fee22,
//         Tuition_Fee: Tuition_Fee22,
//         Duration: Duration22,
//         Intake_Dates: Intake_Dates22
//       });
//     }
//     await fs.writeFileSync("./output/ashtoncollege_fees_intakes_caregary.json", JSON.stringify(subjectAreasArray));
//     console.log("name-->", JSON.stringify(subjectAreasArray));
//     // await browser.close();
//   }
//   async scrapeOnlyInternationalCourseList(selFilepath) {
//     const funcName = 'scrapeCoursePageList ';
//     let s = null;
//     try {
//       // validate params
//       Scrape.validateParams([selFilepath]);
//       // if should read from local file
//       if (appConfigs.shouldTakeCourseListFromOutputFolder) {
//         const fileData = fs.readFileSync(configs.opCourseListFilepath);
//         if (!fileData) {
//           throw (new Error('Invalif file data, fileData = ' + fileData));
//         }
//         const dataJson = JSON.parse(fileData);
//         console.log(funcName + 'Success in getting local data so returning local data....');
//         // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
//         return dataJson;
//       }
//       // load file
//       const fileData = fs.readFileSync(selFilepath);
//       this.selectorJson = JSON.parse(fileData);
//       // create Scrape object
//       s = new Scrape();
//       await s.init({ headless: true });
//       const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
//       console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
//       // set page to url
//       await s.setupNewBrowserPage(rootEleDictUrl);
//       await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
//       await this.scrapeintakesfess();
//       if (s) {
//         await s.close();
//       }

//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       // destroy scrape resources
//       if (s) {
//         await s.close();
//       }
//       throw (error);
//     }
//   }


//   // refers to https://programsandcourses.anu.edu.au/catalogue
//   async scrapeCourseList(selFilepath) {
//     const funcName = 'scrapeCoursePageList ';
//     let s = null;
//     try {
//       // validate params
//       Scrape.validateParams([selFilepath]);
//       // load file
//       const fileData = fs.readFileSync(selFilepath);
//       this.selectorJson = JSON.parse(fileData);
//       // course_list selector
//       const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
//       if (!selectorsList) {
//         console.log(funcName + 'Invalid selectorsList');
//         throw (new Error('Invalid selectorsList'));
//       }
//       const sel = selectorsList[0];
//       console.log(funcName + 'sel = ' + JSON.stringify(sel));

//       // create Scrape object
//       s = new Scrape();
//       await s.init({ headless: true });
//       const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
//       if (!elementDict) {
//         console.log(funcName + 'Invalid elementDict');
//         throw (new Error('Invalid elementDict'));
//       }
//       const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
//       if (!rootEleDictUrl) {
//         console.log(funcName + 'Invalid rootEleDictUrl');
//         throw (new Error('Invalid rootEleDictUrl'));
//       }
//       const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
//       console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

//       // course_level_selector key
//       const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
//       if (!coursLvlSelList) {
//         console.log(funcName + 'Invalid coursLvlSelList');
//         throw (new Error('Invalid coursLvlSelList'));
//       }
//       const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
//       if (!elementDictForCorsLvlKey) {
//         console.log(funcName + 'Invalid elementDict');
//         throw (new Error('Invalid elementDict'));
//       }

//       // course_level_form_selector key
//       // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
//       // if (!formSelList) {
//       //   console.log(funcName + 'Invalid formSelList');
//       //   throw (new Error('Invalid formSelList'));
//       // }
//       // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
//       // if (!formSelElementDict) {
//       //   console.log(funcName + 'Invalid formSelElementDict');
//       //   throw (new Error('Invalid formSelElementDict'));
//       // }

//       // for each course-category, scrape all courses
//       let totalCourseList = []; let count = 1;
//       for (const catDict of courseCatDictList) {
//         console.log(funcName + 'count = ' + count);
//         // click on href
//         console.log(funcName + 'catDict.href = ' + catDict.href);
//         await s.setupNewBrowserPage(catDict.href);
//         const selItem = coursLvlSelList[0];
//         console.log(funcName + 'selItem = ' + selItem);
//         const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
//         console.log(funcName + 'res = ' + JSON.stringify(res));
//         if (Array.isArray(res)) {
//           totalCourseList = totalCourseList.concat(res);
//         } else {
//           throw (new Error('res is not array, it must be...'));
//         }
//         // /////// TEMP /////////
//         // if (count >= 2) {
//         //   break;
//         // }
//         // /////// TEMP /////////
//         count += 1;
//       } // for (const catDict of courseDictList)
//       console.log(funcName + 'writing courseList to file....');
//       // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
//       await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
//       console.log(funcName + 'writing courseList to file completed successfully....');
//       // put file in S3 bucket
//       const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
//       console.log(funcName + 'object location = ' + res);
//       // destroy scrape resources
//       if (s) {
//         await s.close();
//       }
//       return null;
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       // destroy scrape resources
//       if (s) {
//         await s.close();
//       }
//       throw (error);
//     }
//   }
// } // class

// module.exports = { ScrapeCourseList };



const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'scrapeCourseCategoryList ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];
      var name;
      await s.setupNewBrowserPage("https://www.ashtoncollege.edu.au/");
      var mainCategory = [], redirecturl = [];

      const category_val = "(//*[@id='menu-item-2286']/a)[contains(text(),'Fees and Intake Dates')][1]/preceding::a[contains(text(),'Hospitality Courses') or contains(text(),'Business Courses') or contains(text(),'Automotive Courses')]";
      const levels = await s.page.$x(category_val)
      for (let country of levels) {
        name = await s.page.evaluate(el => el.textContent, country);
        redirecturl.push({ name: name });
        mainCategory.push(name);
      }
      console.log("subjectAreasArray-->", redirecturl)
      for (let catc of mainCategory) {
        const subjects = await s.page.$x("(//*[@id='menu-item-2286']/a)[contains(text(),'Fees and Intake Dates')][1]/preceding::a[contains(text(),'" + catc + "')]/../ul/li/a");
        for (let i = 0; i < subjects.length; i++) {
          var elementstring = await s.page.evaluate(el => el.textContent, subjects[i]);
          var elementlink = await s.page.evaluate(el => el.href, subjects[i])
          totalCourseList.push({ href: elementlink, innerText: elementstring, category: catc })
        }
      }
      fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(mainCategory))
      // "//*[@class='vc_column-inner ']/div/h2[(contains(text(),'" + catc + "'))]/following::div[3]//div[contains(@class,'stm-btn-container stm-btn-container_right')]/a")
      // fs.writeFileSync("./output/canberrainsituteoftechnology_courselist.json", JSON.stringify(datalist))
      fs.writeFileSync("./output/ashtoncollege_maincategorylist.json", JSON.stringify(mainCategory))
      console.log("totalCourseList -->", totalCourseList);
      await fs.writeFileSync("./output/ashtoncollege_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
      await fs.writeFileSync("./output/ashtoncollege_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/ashtoncollege_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  };
}
module.exports = { ScrapeCourseList };


