
const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');

const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {

  // refers to https://futurestudents.csu.edu.au/courses/all

  async scrapeOnlyInternationalCourseList() {
    const funcName = 'startScrappingFunc ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var datalist = [];
      await s.setupNewBrowserPage("https://www.griffith.edu.au/college");
      const categoryselectorUrl = "//div[@class='listing']/following::ul[@class='programs']/li/a";
      const categoryselectorUrl1 = "//div[@class='banner card gu8 a-left ']/h1";
      const targetLinksCardsUrls = await s.page.$x(categoryselectorUrl);

      var targetLinks = []
      for (let i = 0; i < targetLinksCardsUrls.length; i++) {


        var elementstring = await s.page.evaluate(el => el.innerText, targetLinksCardsUrls[i]);
        const categoryselectorUrl1 = "//div[@class='banner card gu8 a-left ']/h1";
        const targetLinksCardsUrls1 = await s.page.$x(categoryselectorUrl1);
        var elementhref = await s.page.evaluate(el => el.href, targetLinksCardsUrls[i]);

        //elementstring = elementstring.replace("\n", " ");

        targetLinks.push({ href: elementhref, innerText: elementstring });

      }
      console.log("MAinCategory-->", targetLinks)
      await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinks));

      //div[@data-title='Courses']//div[@class='columns is-multiline']//a
      var totalCourseList = [];
      for (let target of targetLinks) {
        await s.page.goto(target.href, { timeout: 0 });
        // let courseButtonClick = "//a[contains(@title,'Courses')]";
        let linkselector = "//div[@class='listing']/following::ul[@class='programs']/li/a";
        // let studylevelSel = "//div[@data-title='Courses']//div[@class='columns is-multiline']//a/../text()[1]";
        let textselector = "//div[@class='banner card gu8 a-left ']/h1"

        // var clickbtn = await s.page.$x(courseButtonClick);
        // await clickbtn[0].click();
        await s.page.waitFor(10);
        var courses = await s.page.$x(linkselector)
         var courses1 = await s.page.$x(textselector)
        //console.log("Length@@@",courses.length)
        // for (let i = 0; i < courses.length; i++) {
        //   const elementurl = await s.page.evaluate(el => el.href, courses[i])
        //   const elementstring = await s.page.evaluate(el => el.innerText, courses[i])
        //   totalCourseList.push({ href: elementurl, innerText: elementstring, category: target.innerText[i] })
        // }
        // var viewDegreeLink = "//div[@class='banner card gu8 a-left ']/h1";
        // var viewDegree = await s.page.$x(viewDegreeLink);
        for (let i = 0; i < courses.length; i++) {
          for (let j = 0; j < courses1.length; j++) {
          const elementurl = await s.page.evaluate(el => el.href, courses[i])
          // 
          const elementurl1 = await s.page.evaluate(el => el.innerText, courses1[j])

          // for (let j = 0; j < viewDegree.length; j++) {
          //   var dlink = await s.page.evaluate(el => el.innerText, viewDegree[j]);

            const elementstring = await s.page.evaluate(el => el.innerText, courses[i])
            //  const elementstring1 = await s.page.evaluate(el => el.innerText, courses[i])
            totalCourseList.push({ href: elementurl,  innerText: elementurl1, category: elementstring })
          }
        }
      }
     
      await fs.writeFileSync("./output/jamescookuniversity_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }


      await fs.writeFileSync("./output/jamescookuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));

      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }

          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/jamescookuniversity_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
      if (s) {
        await s.close();
      }



    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }



} // class

module.exports = { ScrapeCourseList };
