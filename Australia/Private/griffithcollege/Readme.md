**Australian National University: Total Courses Details As On 26 Feb 2019**
* Total Courses = 5
* Courses without CRICOS code = 0
* Year : 2019

On Campus - Master of Engineering (Mechanical)** - IELTS

An appropriate level of English Language Proficiency equivalent to an English pass level in an Australian Senior Certificate of Education, or an IELTS score of 6.0 (with no individual band less than 6.0), or equivalent as outlined in the EIT Admissions Policy.


On Campus - Master of Engineering (Electrical Systems)**

An appropriate level of English Language Proficiency equivalent to an English pass level in an Australian Senior Certificate of Education, or an IELTS score of 6.0 (with no individual band less than 6.0), or equivalent as outlined in the EIT Admissions Policy.