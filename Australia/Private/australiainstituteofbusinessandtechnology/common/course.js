// const fs = require('fs');
const Scrape = require('./scrape').Scrape;


class Course extends Scrape {
  // remove unwanted keys
  static async removeKeys(scrappedDataJson) {
    const funcName = 'removeKeys ';
    try {
      const resJson = scrappedDataJson;
      Scrape.validateParams([scrappedDataJson]);
      for (const key of Course.KEYS_TO_REMOVE) {
        console.log(funcName + 'removing key = ' + key);
        delete resJson[key];
      }
      return resJson;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
}
  
Course.KEYS_TO_REMOVE = ['select_study_year_and_citizenship_type', 'select_english_as_ibt', 'select_english_as_ielts', 'select_english_as_pbt', 
  'course_academic_requirement', 'course_duration_full_time', 'course_duration_part_time', 'course_tuition_fees_year', 'course_tuition_fees_international_student',
  'course_tuition_fee_duration_years', 'course_tuition_fees_currency', 'select_course_tuition_fees_international_student', 'course_toefl_ibt_indicator',
  'course_toefl_ielts_score', 'course_toefl_toefl_pbt_score', 'page_url'];

Course.COURSE_DURATION_UNIT_TYPES_LIST = ['year', 'years', 'month', 'months'];
Course.COURSE_MONTHS_LIST = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

module.exports = { Course };
