const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = course_category;
                            // As of not we do not have way to get this field value
                            break;
                        }

                        case 'course_title': {
                            resJsonData.course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("resJsonData.course_title3333", resJsonData.course_title);
                            const tt = String(resJsonData.course_title);
                            const ttt = String(tt).split('(')[0];
                            //  const p1 = String(p).split(')')[0];

                            console.log("tttdgsfdgf", ttt);
                            resJsonData.course_title = ttt;
                        }

                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':

                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                                console.log("##fullTimeText--->" + JSON.stringify(fullTimeText));
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const ibtDict = {}; const caeDict = {}; const toeflDict = {}; let ieltsNumber = null;
                            var ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);

                            console.log("ielts_req@@@", ielts_req);

                            //  console.log("pte_req@@@", pte_req);
                            // var ibt = ibt_req.split("TOEFL")[1];
                            // console.log("ibt@@@", ibt);
                            if (ielts_req) {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ielts_req;
                                ieltsDict.min = 0;
                                ieltsDict.require = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));;
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);
                            }

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }

                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);

                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];
                            courseAdminReq.english_requirements_url = "";
                            courseAdminReq.academic_requirements_url = "";
                            courseAdminReq.entry_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }

                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston
                            
                            const checkinternational=courseScrappedData.checkinternational;
                            let data=null
                            if (Array.isArray(checkinternational)) {
                                for (const rootEleDict of checkinternational) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                data = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            console.log("")
                            if(String(data).includes('NOT ACCEPTING ENROLMENTS\n(for International Students)') ||String(data).toLowerCase().includes('not accepting') ){
                                throw new Error('not available for international student ')
                            }
                            console.log("checkinternational====>",data)
                            const courseKeyVals = courseScrappedData.course_cricos_code;
                            const mycodes = [];
                            let replace_cri;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            console.log("course_cricos_code@@@@@@", course_cricos_code);
                           
                            if (String(course_cricos_code).includes("AIBT CRICOS CODE:")) {
                                console.log("course_cricos_code$$$", course_cricos_code[0])
                                for (let cri of course_cricos_code) {
                                    console.log("CRI-->", cri)

                                    if (String(cri).includes('AIBT CRICOS CODE:')) {
                                        replace_cri = cri.replace("AIBT CRICOS CODE:", '').trim();
                                        console.log("replace_cri@@@@", replace_cri);
                                    }
                                }
                            }
                            else if (String(course_cricos_code).includes("AIBT-I CRICOS CODE:")) {
                                console.log("course_cricos_code$$$11", course_cricos_code[1])
                                for (let cri of course_cricos_code) {
                                    console.log("CRI-->", cri)

                                    if (String(cri).includes('AIBT-I CRICOS CODE:')) {
                                        replace_cri = cri.replace("AIBT-I CRICOS CODE:", '').trim();
                                        console.log("replace_cri@@@@", replace_cri);
                                    }
                                }
                            } else{
                                replace_cri=course_cricos_code
                            }
                            if(!replace_cri) {
                                throw new Error("Cricos not found")
                            }   
                                                
                             console.log("replace_cri",replace_cri)                           
                                                        
                            var campLocationText;
                            var location = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            console.log("location@@@@@", location)
                            let one
                            var campusedata = [];
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("hshfgshdfghsfdg", resScholarshipJson)

                            if (String(resScholarshipJson).includes("(AIBT)")) {
                                let one;
                                let two = [];
                                console.log("inaibttt1", resScholarshipJson)
                                let aibt1 = resScholarshipJson[0]
                                console.log("aibt patt 1", aibt1);
                                if (aibt1.indexOf("|") > -1 || aibt1.indexOf("/")) {
                                    one = aibt1.split(/[|,/]+/)
                                    two.push(String(one).replace("Campuses (AIBT)", '').trim());
                                    console.log("Oneeeeeeee", two);
                                }
                                // let newcampus = [];
                                // for (let campus of two) {
                                //     if (!newcampus.includes(campus)) {
                                //         newcampus.push(campus);
                                //         console.log("##Campuscampus-->" + campus)
                                //     }
                                // }
                                var loca = String(two).split(',');

                                loca.forEach(element => {
                                    campusedata.push({
                                        "name": element.trim() + "(AIBT)",
                                       "code":String(replace_cri)
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                console.log("okkkkk_value1", resJsonData.course_campus_location);
                            }
                            if (String(resScholarshipJson).includes("(AIBT-I)")) {
                                let one1;
                                let two1 = [];
                                console.log("inaibttt22", resScholarshipJson)
                                let aibt2 = resScholarshipJson[1]
                                console.log("aib patr @ second if", aibt2);
                                if (String(aibt2).indexOf("|") > -1 || String(aibt2).indexOf("/")) {
                                    one1 = String(aibt2).split(/[|,/]+/)
                                    two1.push(String(one1).replace("Campuses (AIBT-I)", '').trim());
                                    console.log("Oneeeeeeedjghsdjge", one1);
                                }
                                // let newcampus = [];
                                // for (let campus of two) {
                                //     if (!newcampus.includes(campus)) {
                                //         newcampus.push(campus);
                                //         console.log("##Campuscampus-->" + campus)
                                //     }
                                // }
                                var loca1 = String(two1).split(',');

                                loca1.forEach(element => {
                                    campusedata.push({
                                        "name": element.trim() + "(AIBT-I)",
                                        "code":String(replace_cri)
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                console.log("okkkkk_value2", resJsonData.course_campus_location);
                            }

                            else {
                                if (resScholarshipJson.indexOf("|") > -1 || resScholarshipJson.indexOf("/")) {
                                    one = String(resScholarshipJson).split(/[|,/]+/)
                                    console.log("Oneeeeeeee", one);
                                }
                                let newcampus = [];
                                for (let campus of resScholarshipJson) {
                                    if (!newcampus.includes(campus)) {
                                        newcampus.push(campus);
                                        console.log("##Campuscampus-->" + campus)
                                    }
                                }
                                console.log("##Campus-->" + JSON.stringify(one))
                                //let valfainal = campLocationText.toString();
                                var loca = String(one).split(',');

                                if (loca && loca.length > 0) {
                                 //   var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                    resJsonData.course_study_mode = "On campus";
                                    //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                    // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    //console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                    var campusedata = [];

                                    let flocation;
                                   
                                    loca.forEach(element => {
                                        if(element.includes("NSW")){
                                            flocation="Blacktown"
                                        }else if(element.includes("QLD")){
                                            flocation="Mount Gravatt" 
                                        }else if(element.includes("TAS")){
                                            flocation="Hobart"
                                        }
                                        console.log("flocation==>",flocation)
                                        campusedata.push({
                                            "name": flocation,
                                            "code":String(replace_cri)
                                        })
                                    });
                                    resJsonData.course_campus_location = campusedata;
                                    // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                   
                                }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }

                        case 'course_study_mode':
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;
                            let feessplit;
                            const feesList = [];
                            const feesDict = {};

                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                           // let courseKeyVal_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_more);
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const arrval = String(feesWithDollorTrimmed).split('.');
                                const feesVal = String(arrval[0]);
                                if (feesVal.indexOf("OnShore") > -1) {
                                    feessplit = feesVal.split("OnShore")[0]
                                    console.log("feesSplit@@@@@@@@@", feessplit);
                                }
                                let duration=resJsonData.course_duration
                                console.log("location===->",duration)

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feessplit) {
                                    const regEx = /\d/g;
                                    let fduration=duration.match(regEx);
                                    console.log("fduration==>",fduration)
                                    let feesValNum = feessplit.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        if (Number(feesNumber)) {
                                            feesDict.international_student={
                                                amount: Number(feesNumber),
                                                duration:parseInt(String(fduration).replace(/,/g, ''), 10),
                                                unit: "Weeks",                                               
                                                description: feesVal
                                               
                                            }

                                        }
                                        if (feesNumber == "0") {
                                            console.log("FEesNumber = 0");
                                            feesDict.international_student={
                                                amount: 0,
                                                duration: 1,
                                                unit: "Week",                                               
                                                description: ""
                                                
                                            };
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                feesDict.international_student={
                                    amount: 0,
                                    duration: 1,
                                    unit: "Week",                                    
                                    description: "",
                                  
                                };
                            }
                            //var fee_desc_more = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            
                           // console.log("courseKeyVal_more@@1", courseKeyVal_more);

                           // fee_desc_more.push(courseKeyVal_more)
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    //feesDict.international_student_all_fees = fee_desc_more
                                    //console.log("morefees@@2", fee_desc_more);

                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }



                        case 'program_code': {
                            resJsonData.program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            resJsonData.program_code = resJsonData.program_code;

                            break;
                        }
                        case 'course_intake': {
                            var courseIntakeStr = [];
                            let intakeone;
                            let intakeStrList;
                            let intakeone1;
                            let arr = ['January','February','March','April','May','June','July','September','October','November'];
                            // existing intake value
                            let courseIntakeStr1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            console.log("courseIntakeStr1@@@@@", courseIntakeStr1)
                            if (courseIntakeStr1 && courseIntakeStr1.length > 0) {
                                if(courseIntakeStr1.includes('Monthly intakes'))
                                {
                                    courseIntakeStr1=arr
                                }else{
                                    throw new Error('Intake not found');
                                }
                            }
                               
                            if (arr && arr.length > 0) {
                                var campus = resJsonData.course_campus_location
                                console.log('Campuse location :' + campus);
                                var intakes = [];
                                console.log("Intake length-->" + arr.length);
                                console.log("Campus length-->" + campus.length);
                                console.log("Campus 2222222-->" + campus.name);
                                for (var camcount of campus) {
                                    var intakedetail = {};
                                    console.log("camcount@@@@@@", camcount)
                                    intakedetail.name = camcount.name;
                                    intakedetail.value = arr;
                                    intakes.push(intakedetail);
                                }
                                let formatedIntake = await format_functions.providemyintake(intakes, "mom");
                                console.log("FormatIntake", JSON.stringify(formatedIntake))
                                var intakedata = {};
                                intakedata.intake = formatedIntake;

                                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                intakedata.more_details = more_details;
                                resJsonData.course_intake = intakedata;
                            }
                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                             const courseKeyVal_major = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline);
                             const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                             let course_outlines = {};
                             if (courseKeyVal_minor != null) {
                                 course_outlines.minors = courseKeyVal_minor
                             } else {
                                 course_outlines.minors = []
                             }
                             if (courseKeyVal_major != null) {
                                 course_outlines.majors = courseKeyVal_major
                             } else {
                                 course_outlines.majors = []
                             }
                             if (courseKeyVal != null) {
                                 course_outlines.more_details = courseKeyVal
                             } else {
                                 course_outlines.more_details = ""
                             }
                             resJsonData.course_outline = course_outlines
                             break;
                         }
                         case 'application_fees':{
                             const courseKeyVal=await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                             if(courseKeyVal != null){
                                 resJsonData.application_fee=courseKeyVal
                             }else{
                                 resJsonData.application_fee=""
                             }
                             break;
                         }                 
                        


                        case 'course_country': {
                            resJsonData.course_country = await Course.extractValueFromScrappedElement(courseScrappedData.course_country)
                            break;
                        }
                        case 'course_overview': {
                            resJsonData.course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            let course_overview = resJsonData.course_overview;
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } //else {
                            //     resJsonData.course_overview = "";
                            // }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            for (let sel of selList) {
                                                sel = sel.replace(/[\r\n\t ]+/g, ' ').trim();
                                                if (sel.length > 0) {
                                                    console.log("sel----->", sel);
                                                    course_career_outcome.push(sel)
                                                    break;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = [course_career_outcome[0]]
                            } else {
                                resJsonData.course_career_outcome = [];
                            }


                            break;
                        }
                        // case 'course_career_outcome': {
                        //   //  const courseKeyVal = courseScrappedData.course_career_outcome;
                        //     resJsonData.course_career_outcome = await Course.extractValueFromScrappedElement(courseScrappedData.course_career_outcome);
                        //     let course_career_outcome = resJsonData.course_career_outcome;
                        //     console.log("course_career_outcome.length -->", course_career_outcome);
                        //     if (course_career_outcome.length > 0) {
                        //         resJsonData.course_career_outcome = [course_career_outcome]
                        //     } else {
                        //           resJsonData.course_career_outcome  = [];
                        //     }


                        //     break;
                        // }
                        case 'course_study_level': {
                            let splitCricos
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log("!@$$TGDGDG", cTitle)
                            if (cTitle.includes('AIBT CRICOS CODE:')) {
                                console.log("cTitle.cTitle------>>>>>>", cTitle);
                                splitCricos = String(cTitle).split('AIBT CRICOS CODE:')[1].trim();
                                console.log("resJsonData.sssss------>>>>>>", splitCricos);
                                const study_val = await format_functions.getMyStudyLevel(splitCricos.trim());
                                resJsonData.course_study_level = study_val;
                                console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                            }

                            else {
                                resJsonData.course_study_level = cTitle;

                            }
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;

            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }


                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                //resJsonData.basecourseid = location_wise_data.course_id;          
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
