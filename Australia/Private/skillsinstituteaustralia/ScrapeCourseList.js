const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const puppeteer = require('puppeteer');
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
class ScrapeCourseList extends Scrape {
  // refers to https://skills.qld.edu.au/study/
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      var datalist = [];
      let s;
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage("https://skills.qld.edu.au/study/", { timeout: 0 });
      console.log("yesssss")
      let maincategory = [];
      const maincategoryselector = "//*//h2[not((contains(text(),'Excellence in education')) or (contains(text(),'Short courses')))]";
      var category = await page.$x(maincategoryselector);
      console.log("Total categories-->" + category.length);
      for (let catc of category) {
        const category = await page.evaluate(el => el.textContent, catc)
        // var category1 = await page.evaluate(el => el.innerText, catc)
        console.log("categories-->" + category);
        maincategory.push(category)
      }
      await fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(maincategory))
      for (let catc of maincategory) {
        // const urlselector=await page.$x("//*[@class='menu-item-mega-container']/ul/li/a/span/span[(contains(text(),'" + catc + "'))]/following::ul[1]/li/a")
        // const textselector=await page.$x("//*[@class='menu-item-mega-container']/ul/li/a/span/span[(contains(text(),'" + catc + "'))]/following::ul[1]/li/a/span")
        const urlselector = await page.$x("(//*//h2[contains(text(),'" + catc + "')])/following-sibling::table[1]//a")
        const textselector = await page.$x("(//*//h2[contains(text(),'" + catc + "')])/following-sibling::table[1]//a")
        const programcode22 = await page.$x("(//*//h2[contains(text(),'" + catc + "')])/following-sibling::table[1]//a/../following-sibling::td[1]");
        const duration22 = await page.$x("(//*//h2[contains(text(),'" + catc + "')])/following-sibling::table[1]//a/../following-sibling::td[2]");
        for (let i = 0; i < urlselector.length; i++) {
          const href = await page.evaluate(el => el.href, urlselector[i])
          const innertext = await page.evaluate(el => el.textContent, textselector[i])
          const programcode11 = await page.evaluate(el => el.textContent, programcode22[i])
          const duration11 = await page.evaluate(el => el.textContent, duration22[i])
          console.log("Href->" + href + "innertext-->" + innertext, "programcode11-->" + programcode11, "duration11-->" + duration11)
          datalist.push({ href: href, innerText: innertext, category: catc, program: programcode11, duration: duration11 + ' ' + 'weeks' })
        }
      }
      console.log("maincategory", datalist)
      await fs.writeFileSync("./output/skillsinstituteaustralia_original_courselist.json", JSON.stringify(datalist));
      // let uniqueUrl = [];
      // //unique url from the courselist file
      // for (let i = 0; i < datalist.length; i++) {
      //   let cnt = 0;
      //   if (uniqueUrl.length > 0) {
      //     for (let j = 0; j < uniqueUrl.length; j++) {
      //       if (datalist[i].href == uniqueUrl[j].href) {
      //         cnt = 0;
      //         break;
      //       } else {
      //         cnt++;
      //       }
      //     }
      //     if (cnt > 0) {
      //       uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innertext, category: [], program_code: datalist[i].programcode11, duration_value: datalist[i].duration11 });
      //     }
      //   } else {
      //     uniqueUrl.push({ href: datalist[i].href, innerText: datalist[i].innertext, category: [], program_code: datalist[i].programcode11, duration_value: datalist[i].duration11 });
      //   }
      // }
      await fs.writeFileSync("./output/skillsinstituteaustralia_unique_courselist.json", JSON.stringify(datalist));
      // //based on unique urls mapping of categories
      // for (let i = 0; i < datalist.length; i++) {
      //   for (let j = 0; j < uniqueUrl.length; j++) {
      //     if (uniqueUrl[j].href == datalist[i].href) {
      //       uniqueUrl[j].category.push(datalist[i].category);
      //     }
      //   }
      // }
      console.log("totalCourseList -->", datalist);
      await fs.writeFileSync("./output/skillsinstituteaustralia_courselist.json", JSON.stringify(datalist));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }

  async scrapeintakesfess() {
    const URL = "https://skills.qld.edu.au/fee-structure/";
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    var subjectAreasArray = [];
    let Course_Name = "//*//table[@id='tablepress-45']//tr[td/b[(contains(text(),'Hospitality package 1'))]]/preceding-sibling::tr/td[1]/a";
    let Tuition_Fee = "//*//table[@id='tablepress-45']//tr[td/b[(contains(text(),'Hospitality package 1'))]]/preceding-sibling::tr/td[1]/a/../following-sibling::td[2]";
    const Course_Name11 = await page.$x(Course_Name);
    const Tuition_Fee11 = await page.$x(Tuition_Fee);
    for (let i = 0; i < Course_Name11.length; i++) {
      let Course_Name22 = await page.evaluate(el => el.textContent, Course_Name11[i]);
      let Tuition_Fee22 = await page.evaluate(el => el.textContent, Tuition_Fee11[i]);
      subjectAreasArray.push({
        Course_Name: Course_Name22,
        Tuition_Fee: Tuition_Fee22,
      });
    }
    await fs.writeFileSync("./output/skillsinstituteaustralia_fees.json", JSON.stringify(subjectAreasArray));
    console.log("name-->", JSON.stringify(subjectAreasArray));
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      await this.scrapeintakesfess();
      if (s) {
        await s.close();
      }
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class
module.exports = { ScrapeCourseList };