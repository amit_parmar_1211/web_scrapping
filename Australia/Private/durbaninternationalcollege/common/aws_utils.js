const fs = require('fs');
const path = require('path');
//const AWS = require('aws-sdk');
const appConfigs = require('./app-config');

let s3 = null;
// if (appConfigs.AWS_SAM_LOCAL) {
//   s3 = new AWS.S3({ apiVersion: '2006-03-01', accessKeyId: process.env.ACCESS_KEY, secretAccessKey: process.env.SECRET_KEY });
// } else {
//   s3 = new AWS.S3({ apiVersion: '2006-03-01' });
// }

async function putFileInBucket(filePath) {
  const funcName = 'putFileInBucket ';
  try {
    if (!(filePath && filePath.length > 0)) {
      console.log(funcName + 'Invalid filePath, filePath = ' + filePath);
      throw (new Error('Invalid filePath, filePath = ' + filePath));
    }
    let fileName = path.basename(filePath);
    console.log(funcName + 'fileName = ' + fileName);
    // from filePath, seperate univName
    const fileNameSplitList = String(fileName).split(appConfigs.univNameSeperater);
    if (!(fileNameSplitList && fileNameSplitList.length > 1)) {
      console.log(funcName + 'Failed to split fileName, fileName = ' + fileName);
      throw (new Error('Failed to split fileName, fileName = ' + fileName));
    }
    let univName = fileNameSplitList[0];
    console.log(funcName + 'univName = ' + univName);
    univName = univName.concat('/');
    fileName = univName.concat(fileName);
    console.log(funcName + 'fileName with folder path = ' + fileName);
    // const params = {
    //   Bucket: appConfigs.awsUniversitiesDataBucketName,
    //   Key: '',
    //   Body: '',
    // };
    // read file
    const readStream = await fs.createReadStream(filePath);
    params.Body = readStream;
    params.Key = fileName;
    // upload file
    const data = await s3.upload(params).promise();
    if (data === null) {
      console.log(funcName + 'Failed to put an object in bucket');
      throw (new Error('Failed to put an object in bucket'));
    }
    return data.Location;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

async function putFailedItemsListInBucket(univId, failedItemsList, apiOperationType) {
  const funcName = 'putFailedItemsListInBucket ';
  try {
    if (!(univId && univId.length > 0 && failedItemsList && failedItemsList.length > 0)) {
      console.log(funcName + 'Invalid univId or failedItemsList, univId = ' + univId + ' failedItemsList = ' + JSON.stringify(failedItemsList));
      throw (new Error('Invalid univId or failedItemsList, univId = ' + univId + ', failedItemsList = ' + JSON.stringify(failedItemsList)));
    }
    const dateTime = new Date();
    const date = dateTime.getUTCDate() + '_' + (dateTime.getMonth() + 1) + '_' + dateTime.getUTCFullYear();
    console.log(funcName + 'UTC date = ' + date);
    const time = dateTime.getUTCHours() + ':' + dateTime.getUTCMinutes() + ':' + dateTime.getUTCSeconds() + ':' + dateTime.getUTCMilliseconds();
    console.log(funcName + 'UTC time = ' + time);
    let fileDateTime = null;
    if (apiOperationType && apiOperationType.length > 0) {
      fileDateTime = String(apiOperationType).concat('_').concat(date).concat('_')
        .concat(time)
        .concat('.json');
    } else {
      fileDateTime = String(date).concat('_').concat(time).concat('.json');
    }
    console.log(funcName + 'fileDateTime = ' + fileDateTime);

    // add folder name failedItemsFolderName
    const univName = univId.concat('/');
    const failedItemsFolderPath = univName.concat(appConfigs.failedItemsFolderName).concat('/');
    const fileNameWithFullPath = failedItemsFolderPath.concat(fileDateTime);
    console.log(funcName + 'fileName with folder path, fileNameWithFullPath= ' + fileNameWithFullPath);

    const params = {
      Bucket: appConfigs.awsUniversitiesDataBucketName,
      Key: '',
      Body: '',
    };
    // read data into buffer
    const buf = Buffer.from(JSON.stringify(failedItemsList));
    params.Body = buf;
    params.Key = fileNameWithFullPath;
    // upload file
    const data = await s3.upload(params).promise();
    if (data === null) {
      console.log(funcName + 'Failed to put an object in bucket');
      throw (new Error('Failed to put an object in bucket'));
    }
    return data.Location;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

async function getObjectFromBucket(objKey) {
  const funcName = 'getObjectFromBucket ';
  try {
    if (!(objKey && objKey.length > 0)) {
      console.log(funcName + 'Invalid objKey, objKey = ' + objKey);
      throw (new Error('Invalid objKey, objKey = ' + objKey));
    }
    const params = {
      Bucket: appConfigs.awsUniversitiesDataBucketName,
      Key: objKey,
    };
    const data = await s3.getObject(params).promise();
    if (data === null) {
      console.log(funcName + 'Failed to get specified object from bucket, objKey = ' + objKey);
      throw (new Error('Failed to get specified object from bucket, objKey = ' + objKey));
    }
    return data.Body;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}
/*
async function getObjectFromBucket(objName) {
  const funcName = 'getObjectFromBucket ';
  try {
    if(!(objName && objName.length > 0)) {
      console.log(funcName + 'Invalid objName');
      throw (new Error('Invalid objName'));
    }
    const params = {
      Bucket: appConfigs.awsUniversitiesDataBucketName,
      Key: objName
    }
  const data = await s3.getObject(params).promise();
  if (data === null ) {
      console.log(funcName + 'Failed to get specified object from bucket, objName = ' + objName);
      throw (new Error('Failed to get specified object from bucket, objName = ' + objName));
    }
    return data.Body;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}
*/

module.exports = { putFileInBucket, getObjectFromBucket, putFailedItemsListInBucket };

/*
async function getAllBuckets() {
  const funcName = 'getAllBuckets ';
  try {
    const params = {};
    const data = await s3.listBuckets(params).promise();
    if (data === null) {
      throw (new Error('Failed to list bickets'));
    }
    console.log(funcName + 'data.Buckets = ' + JSON.stringify(data.Buckets));
    return data.Buckets;  
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}
*/
// If there is need to create S3 bucket programmatically instead of cloud formation template
/*
async function GetOrCreateDataBucket() {
  const funcName = 'createBucket ';
  try {
    const bucketName = appConfigs.awsUniversitiesDataBucketName;
    // #TODO is this necessary or can we delegate this to S3 AWS SDK itself?
    const bucketList = await getAllBuckets();
    for (const bucket of bucketList) {
      console.log(funcName + 'bucket = ' + JSON.stringify(bucket));
      if(bucket.Name === bucketName) {
        console.log(funcName + 'bucket ' + bucketName + ' already exist');
        // throw (new Error('Bucket with same name already exist'));
        return bucket.Name;
      }
    } // for
    // create bucket
    const params = {
      Bucket: bucketName,
      ACL: 'private',
    };
    const data = await s3.createBucket(params).promise();
    if (data === null) {
      console.log(funcName + 'Failed to create new bucket');
      throw (new Error('Failed to create new bucket'));
    }
    // enable versioning on the bucket
    const paramsVersioning = {
      Bucket: bucketName,
      VersioningConfiguration: {
        Status: 'Enabled'
      }
    };
    const dataVer = await s3.putBucketVersioning(paramsVersioning).promise();
    if (dataVer === null) {
      console.log(funcName + 'Bucket was created but failed to enable versioning on it');
      throw (new Error('Bucket was created but failed to enable versioning on it'));
    }
    // block all public access
    const paramsBlockPubAccess = {
      Bucket: bucketName,
      PublicAccessBlockConfiguration: {
        BlockPublicAcls: true,
        BlockPublicPolicy: true,
        IgnorePublicAcls: true,
        RestrictPublicBuckets: true
      }
    };
    const dataBlockPub = await s3.putPublicAccessBlock(paramsBlockPubAccess).promise();
    if (dataBlockPub === null) {
      console.log(funcName + 'Bucket was created, versioning enabled but failed to enable public access lock');
      throw (new Error('Bucket was created, versioning enabled but failed to enable public access lock'));
    }
    return data.Location;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}
*/
