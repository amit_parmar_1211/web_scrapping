const fs = require('fs');
const configs = require('./configs');
const appConfigs = require('./common/app-config');
const awsUtil = require('./common/aws_utils');
const utils = require('./common/utils');
const ScrapeCourseList = require('./ScrapeCourseList').ScrapeCourseList;
const ScrapeCourse = require('./ScrapeCourse').ScrapeCourse;
const ScrapeUniversity = require('./ScrapeUniversity').ScrapeUniversity;
request = require('request');

const START_COURSE_COUNT = 0;
let MAX_COURSE_COUNT = -1;// to scrape for all courses, set to -1
const SCRAPE_COURSE_LIST = false;
const SCRAPE_UNIVERSITY_DETAILS = true;
const SCRAPE_COURSE_DETAILS = false;


async function validateMandatoryConfigs() {
  const funcName = 'validateMandatoryConfigsForUniversity ';
  try {
    // 'univ_name' value is mandatory and this id will be used to put item into DDB
    if (!(configs.univ_name && configs.univ_name.length > 0)) {
      console.log(funcName + 'Invalid value set to \'univ_name\'');
      throw (new Error('Invalid value set to \'univ_name\'. To set this value, open \'config.js\' file of the specific university folder and edit its value. \
      \n\n ########## Warning ########## \n Make sure you set correct value to \'univ_name\'. Because this value will be used to create unique \'univ_id\' value for the university into databse. \
      \n Setting incorrect value may create unexpected result. \n'));
    }
    // 'univ_id' value is mandatory and this id will be used to put items into DDB
    if (!(configs.univ_id && configs.univ_id.length > 0)) {
      console.log(funcName + 'Invalid value set to \'univ_id\'');
      throw (new Error('Invalid value set to \'univ_id\'. To set this value, open \'config.js\' file of the specific university folder and edit its value. \
      \n\n ########## Warning ########## \n Make sure you set correct value to \'univ_id\'. Because this value will be used to make put operations into database. \
      \n Setting incorrect value may overwrite data into database. \n'));
    }
    // validate 'univ_id' rules
    const regEx = /[A-Z\s]/;
    if (String(configs.univ_id).search(regEx) !== -1) { // -1 = not found
      console.log(funcName + '\n \'univ_id\' must NOT contain any white space and must be in lowercase only \n');
      throw (new Error('\'univ_id\' must NOT contain any white space and must be in lowercase only'));
    }
    console.log(funcName + '****************  scrapping for \'univ_id\' = ' + configs.univ_id + ', and \'univ_name\' = ' + configs.univ_name + '  ****************');
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

// eslint-disable-next-line no-unused-vars
async function scrapeUnivDetailsAndPutAtS3() {
  const funcName = 'scrapeUnivDetailsAndPutAtS3 ';
  try {
    await validateMandatoryConfigs();
    const univOverviewJson = [];
    // get existing scrapped files from output folder and do not scrape from scratch
    const su = new ScrapeUniversity();
    if (appConfigs.shouldTakeFromOutputFolder) {
      console.log(funcName + 'reading scrapped data from output file...');
      // put file in S3 bucket
      //const res = await awsUtil.putFileInBucket(configs.opOverviewFilepath);
      //console.log(funcName + 'object location = ' + res);
      return;
    }
    const univScrappedData = await su.scrapeAndFormatUniversityDetails(appConfigs.selUniversityOverviewSelectorsJsonFilepath);
    univOverviewJson.push(univScrappedData);
    // write to output file
    await fs.writeFileSync(configs.opOverviewFilepath, JSON.stringify(univOverviewJson));
    request.post({
      "headers": { "content-type": "application/json" },
      "url": appConfigs.API_URL + "api/australia/university/v1/save",
      "body": JSON.stringify(univOverviewJson)
    }, (error, response, body) => {
      if (error) {
        console.log(error);
      }
      console.log("success save data! " + JSON.stringify(body));
    });
    // put file in S3 bucket
    //const res = await awsUtil.putFileInBucket(configs.opOverviewFilepath);
    //console.log(funcName + 'object location = ' + res);
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}
async function readFile() {
  return await fs.readdirSync(appConfigs.READ_FOLDER_PATH)
}
async function putCourseInSQL() {
  try {
    var myfiles = await readFile();

    console.log(JSON.stringify(myfiles))
    for (let filename of myfiles) {
      var resJsonData = JSON.parse(fs.readFileSync(appConfigs.READ_FOLDER_PATH + "/" + filename));

      if (appConfigs.ISINSERT) {
        console.log("start insert for" + filename)
        request.post({
          "headers": { "content-type": "application/json" },
          "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
          "body": JSON.stringify(resJsonData)
        }, (error, response, body) => {
          if (error) {
            console.log(error);
          }
          var myresponse = JSON.parse(body);
          if (!myresponse.flag) {
            var errorlog = [];
            if (fs.existsSync(configs.opFailedCourseListByAPI)) {
              errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
              errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
            }
            fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
          }
          console.log("success save data! " + JSON.stringify(body));
        });
      }
      else if (appConfigs.ISREPLACE) {
        request.post({
          "headers": { "content-type": "application/json" },
          "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
          "body": JSON.stringify(resJsonData)
        }, (error, response, body) => {
          if (error) {
            console.log(error);
          }
          var myresponse = JSON.parse(body);
          console.log("MyMessage-->" + JSON.stringify(myresponse))
          if (!myresponse.flag) {
            var errorlog = [];
            if (fs.existsSync(configs.opFailedCourseListByAPI)) {
              errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
              errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
            }
            fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
          }
          console.log("success save data! " + JSON.stringify(body));
        });
      }
      else if (appConfigs.ISUPDATE) {
        request.post({
          "headers": { "content-type": "application/json" },
          "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
          "body": JSON.stringify(resJsonData)
        }, (error, response, body) => {
          if (error) {
            console.log(error);
          }
          var myresponse = JSON.parse(body);
          if (!myresponse.flag) {
            var errorlog = [];
            if (fs.existsSync(configs.opFailedCourseListByAPI)) {
              errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
              errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
            }
            fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
          }
          console.log("success save data! " + JSON.stringify(body));
        });
      }
    }
  }
  catch (err) {
    console.log("#### try-catch error--->" + err);
  }
  return await true;
}
// Start scrapping
const startScrapping = async function startScrappingFunc() {
  const funcName = 'startScrappingFunc ';
  // let sc = null;
  try {
    // scrape all courses with its name and link
    let scrappedCourseList = null;
    if (SCRAPE_COURSE_LIST) {
      console.log(funcName + 'Starting to scrape course list.....');
      const scl = new ScrapeCourseList();
      scrappedCourseList = await scl.scrapeOnlyInternationalCourseList(appConfigs.selUniversityCourseListSelectorsJsonFilepath);
      // console.log(funcName + 'scrappedCourseList = ' + JSON.stringify(scrappedCourseList));
    }

    // scrape Univ Details
    if (SCRAPE_UNIVERSITY_DETAILS) {
      console.log(funcName + 'Starting to scrape univ details.....');
      await scrapeUnivDetailsAndPutAtS3();
    }

    // scrape course details for each course
    if (SCRAPE_COURSE_DETAILS) {
      console.log(funcName + 'Starting to scrape course details.....');
      if (appConfigs.SHOULD_TAKE_FROM_FOLDER) {
        await putCourseInSQL();
      }
      else {
        if (!scrappedCourseList) { // read it from local disk
          const fileData = fs.readFileSync(configs.opCourseListFilepath);
          scrappedCourseList = JSON.parse(fileData);
        }
        // failed course items list
        const failedCourseDictList = [];
        // for (const courseDict of scrappedCourseList) {
        if (MAX_COURSE_COUNT < 0) {
          MAX_COURSE_COUNT = scrappedCourseList.length;
        }
        console.log(funcName + 'START_COURSE_COUNT = ' + START_COURSE_COUNT + ', MAX_COURSE_COUNT = ' + MAX_COURSE_COUNT);
        for (let i = START_COURSE_COUNT; i < MAX_COURSE_COUNT; i += 1) {
          try {
            const courseDict = scrappedCourseList[i];
            console.log('\n\r----------------------------------------------------------------------');
            console.log('----------------------------------------------------------------------\n\r');
            console.log('   Start scraping for Course = ' + courseDict.innerText);
            console.log('   courseCount = ' + i);
            console.log('\n\r----------------------------------------------------------------------');
            console.log('----------------------------------------------------------------------\n\r');
            const finalScrappedDataList = [];
            global.course_name = courseDict.innerText;
            const finalScrappedData = await ScrapeCourse.scrapeAndFormatCourseDetails(courseDict);
            // get output file path basis on Update or normal PUT operation
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            finalScrappedDataList.push(finalScrappedData);
          } catch (error) {
            var failedCourseDict = scrappedCourseList[i];
            console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
            console.log(funcName + 'try-catch error = ' + error);
            console.log('Failed course href = ' + failedCourseDict.href);
            console.log('Failed course innerText = ' + failedCourseDict.innerText);
            failedCourseDict.reason = error.toString();
            failedCourseDict.index = i;
            failedCourseDictList.push(failedCourseDict);
            // write to local disk
            fs.writeFileSync(configs.opFailedCourseList, JSON.stringify(failedCourseDictList));
            console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
          }
        } // for
      }
      console.log('Exiting from program...');
    } // if (SCRAPE_COURSE_DETAILS)
    return true;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
};
startScrapping();
