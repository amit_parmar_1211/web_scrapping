const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = category_name;
                            break;
                        }
                        case 'course_title_category':
                        case 'course_title': {

                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                           
                            var res = title.slice(8);
                            
                           
                            var finalTitle = res.split("(")[0].trim();
                         
                            var ctitle = format_functions.titleCase(finalTitle).trim();

                            
                            resJsonData.course_title = ctitle



                            break;
                        }

                        case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                        const courseAdminReq = {};
                        const englishList = [];
                        const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; const cpeDict = {};
                        var myIELTS = "";
                        var myAcademic = [];
                     
                        var filedata = await utils.getValueFromHardCodedJsonFile('course_details');
                       
                        var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                       

                        

                            
                            
                            let tmpvar = filedata.filter(val => {
                                return val.name.toLowerCase() == title.toLowerCase();
                            });
                            
                            let oneielts = tmpvar[0].ielts;
                            
                             var IeltsIntStudent = oneielts;
                        
                        
                        
                 

                        if (oneielts) {
                            ieltsDict.name = 'ielts academic';
                            ieltsDict.description = IeltsIntStudent;
                            ieltsDict.min = 0;
                            ieltsDict.require = await utils.giveMeNumber(IeltsIntStudent.replace(/ /g, ' '));;
                            ieltsDict.max = 9;
                            ieltsDict.R = 0;
                            ieltsDict.W = 0;
                            ieltsDict.S = 0;
                            ieltsDict.L = 0;
                            ieltsDict.O = 0;
                            englishList.push(ieltsDict);
                        }

                        if (englishList && englishList.length > 0) {
                            courseAdminReq.english = englishList;
                        }

                            //var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var english_req = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            var academic_req = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirement_url);


                            var academicReq = null;
                            let IELTSStudent111 = await utils.getValueFromHardCodedJsonFile('course_details');
                           
                            var course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            
                            tmpvar = IELTSStudent111.filter(val => {
                                return val.name.toLowerCase() == course_title.toLowerCase()
                            });
                            console.log("oneeeeeeeeeeeeeeeeee----------->",tmpvar);
                            let one = tmpvar[0].academic
                            
                            courseAdminReq.academic = [one];
                            console.log(" courseAdminReq.academic", courseAdminReq.academic);
                            // courseAdminReq.academic.push(one.trim());

                            // courseAdminReq.academic = (academicReq) ? academicReq.toString() : [];
                            console.log("Academic requirement" + JSON.stringify(courseAdminReq.academic));

                            //  courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];

                            courseAdminReq.english_requirements_url = "https://www.bit.edu.au/images/pdf2019/BIT_SHB_OCT_2019.pdf";
                            courseAdminReq.entry_requirements_url = "https://www.bit.edu.au/images/pdf2019/BIT_SHB_OCT_2019.pdf";
                            courseAdminReq.academic_requirements_url = "https://www.bit.edu.au/images/pdf2019/BIT_SHB_OCT_2019.pdf";



                            //courseAdminReq.academic_more_details = academic_more;
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        // case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                        //     const courseAdminReq = {};
                        //     const englishList = [];
                        //     let ieltsNumber = null, pteNumber = null, ibtNumber = null, caeNumber = null;
                        //     let myscore = '';
                        //     const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {};
                        //     console.log("resJsonData.course_toefl_ielts_score -->", JSON.stringify(courseScrappedData.course_toefl_ielts_score));
                        //     let ieltsString = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                        //     let ieltsSplitString = '';
                           

                        //     {



                        //         if (ieltsSplitString && ieltsSplitString.length > 0) {
                        //             // extract exact number from string
                        //             const regEx = /[+-]?\d+(\.\d+)?/g;
                        //             const matchedStrList = String(ieltsSplitString).match(regEx);
                        //             console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                        //             if (matchedStrList && matchedStrList.length > 0) {
                        //                 ieltsNumber = Number(matchedStrList[0]);
                        //                 console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                        //             }
                        //         }
                        //         if (ieltsNumber) {
                        //             ieltsDict.name = 'ielts academic';
                        //             ieltsDict.description = ieltsSplitString;
                        //             ieltsDict.min = 0;
                        //             ieltsDict.require = ieltsNumber
                        //             ieltsDict.max = 9;
                        //             ieltsDict.R = 0;
                        //             ieltsDict.W = 0;
                        //             ieltsDict.S = 0;
                        //             ieltsDict.L = 0;
                        //             ieltsDict.O = 0;
                        //             englishList.push(ieltsDict);
                        //         }
                        //         ///progress bar start
                        //         const pieltsDict = {}; const pibtDict = {}; const ppteDict = {}; const pcaeDict = {};
                        //         var penglishList = [];
                        //         var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";

                        //         if (ieltsNumber != []) {
                        //             pieltsDict.name = 'ielts academic';
                        //             var value = {};
                        //             value.min = 0;
                        //             value.require = ieltsNumber;
                        //             value.max = 9;
                        //             pieltsDict.value = value;
                        //             penglishList.push(pieltsDict);
                        //         }
                        //         if (englishList && englishList.length > 0) {
                        //             courseAdminReq.english = englishList;
                        //         }

                        //         var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                        //         var academicReq = "";
                        //         var arr = [];
                        //         const courseKeyVal = courseScrappedData.course_academic_requirement;
                        //         console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                        //         if (Array.isArray(courseKeyVal)) {
                        //             for (const rootEleDict of courseKeyVal) {
                        //                 console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //                 const elementsList = rootEleDict.elements;
                        //                 for (const eleDict of elementsList) {
                        //                     console.log(funcName + '\n\r eleDict academic= ' + JSON.stringify(eleDict));
                        //                     const selectorsList = eleDict.selectors;
                        //                     for (const selList of selectorsList) {
                        //                         // console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                         // if (Array.isArray(selList) && selList.length > 0) {
                        //                         console.log("selList------>", selList);
                        //                         academicReq = selList;
                        //                         //}
                        //                     }
                        //                 }
                        //             }
                        //         }
                               
                        //         let text_val = [];
                        //         for (let acade_val of academicReq) {
                        //             text_val.push(acade_val);
                        //         }
                                
                        //         if (text_val) {
                        //             courseAdminReq.academic = text_val;
                        //         } else {
                        //             courseAdminReq.academic = [];
                        //         }
                                
                        //     }

                        //     resJsonData.course_admission_requirement = courseAdminReq;
                        //     let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                        //     let academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                        //     let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                        //     resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                        //     resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                        //     resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;
                        //     break;
                        // }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var academicReq = null;
                            var fullTimeText = "";
                            let IELTSStudent5644 = await utils.getValueFromHardCodedJsonFile('course_details');
                           
                            var course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                          
                            let tmpvar = IELTSStudent5644.filter(val => {
                                return val.name.toLowerCase() == course_title.toLowerCase()
                            });
                            
                            let one = tmpvar[0].duration
                            let one1 = one + " week";
                           
                            let course_cricos_code = one1;
                           

                            fullTimeText = course_cricos_code
                           

                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = courseDurationList;
                                    }
                                }
                            }
                        }
                            if (courseDurationDisplayList && courseDurationDisplayList.length > 0) {
                                resJsonData.course_duration_display = courseDurationDisplayList;
                              
                                let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                var isfulltime = false, isparttime = false;
                               
                                filtered_duration_formated.forEach(element => {
                                    if (element.display == "Full-Time") {
                                        isfulltime = true;
                                    }
                                    if (element.display == "Part-Time") {
                                        isparttime = true;
                                    }
                                });
                                resJsonData.isfulltime = isfulltime;
                                resJsonData.isparttime = isparttime;
                                break;
                            }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                       
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': {
                            var campLocationText = "";
                            var courseKeyVal = courseScrappedData.course_campus_location;

                            var courseKeyVal2 = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)

                            if (courseKeyVal2.includes('BSB51918') > -1 || courseKeyVal2.includes('BSB42015')) {
                                var courseKeyVal3 = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location1)

                                campLocationText = courseKeyVal3


                                if (campLocationText && campLocationText.length > 0) {
                                    var campLocationValTrimmed = String(campLocationText).trim();


                                    if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                        var myavailcampuses = [
                                            "Docklands",
                                            "Melbourne",
                                            "BITE",
                                        ];
                                        var mycamp = [];
                                       
                                        myavailcampuses.forEach(element => {
                                            if (campLocationValTrimmed.indexOf(element) != -1) {
                                                // mycamp.push(element);
                                                mycamp.push({
                                                    "name": this.titleCase(element),
                                                    "city": "",
                                                    "state": "",
                                                    "country": "",
                                                    "iscurrent": false
                                                })
                                            }
                                        });
                                       
                                        resJsonData.course_campus_location = mycamp;
                                    }
                                }
                            }
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal3));
                            
                          

                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                campLocationText = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log(funcName + 'campLocationText = ' + campLocationText);

                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = String(campLocationText).trim();
                                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                if (campLocationValTrimmed.toString().includes("/")) {
                                    var location_val = campLocationValTrimmed.split('/');
                                   
                                    var arr = [];
                                    location_val.forEach(element => {
                                       
                                        arr.push(element)
                                    })
                                    campLocationValTrimmed == arr;
                                }
                                if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    var myavailcampuses = [
                                        "Docklands",
                                        "Melbourne",
                                        "BITE",
                                    ];
                                    var mycamp = [];
                                    
                                    myavailcampuses.forEach(element => {
                                        if (campLocationValTrimmed.indexOf(element) != -1) {
                                            
                                            mycamp.push({
                                                "name": this.titleCase(element),
                                                "city": "",
                                                "state": "",
                                                "country": "",
                                                "iscurrent": false
                                            })
                                        }
                                    });
                                   console.log("fhsjfejfscampus-->");
                                    resJsonData.course_campus_location = mycamp;
                                    console.log("bdjcampus-->");
                                     resJsonData.course_study_mode = 'On campus';
                                }
                            } else {
                                throw new Error("No campus location found.");
                            }
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                          

                            const feesList = [];
                            const feesDict = {
                                international_student: [],


                            };

                            let feesIntStudent111 = await utils.getValueFromHardCodedJsonFile('course_details');
                          
                            var course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            let tmpvar = feesIntStudent111.filter(val => {
                                return val.name.toLowerCase() == course_title.toLowerCase()
                            });
                          
                            let one = tmpvar[0].fee
                          

                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (one && one.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(one).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const arrval = String(feesWithDollorTrimmed).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum556 = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum444 = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        if (Number(feesNumber)) {
                                            feesDict.international_student.push({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                isfulltime: true,
                                                description: "",
                                                type: ""
                                            });

                                        }
                                        if (feesNumber == "0") {
                                            console.log("FEesNumber = 0");
                                            feesDict.international_student.push({
                                                amount: 0,
                                                duration: 1,
                                                unit: "Year",
                                                isfulltime: true,
                                                description: "",
                                                type: ""
                                            });
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                feesDict.international_student.push({
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",
                                    isfulltime: true,
                                    description: "",
                                    type: ""
                                });
                            }
                            var fee_desc_more = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal_more = courseScrappedData.course_tuition_fees_international_student_more;
                            if (Array.isArray(courseKeyVal_more)) {
                                for (const rootEleDict of courseKeyVal_more) {
                                    console.log(funcName + '\n\r rootEleDict767676767 = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict464454 = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList6464646 = ' + JSON.stringify(selList));
                                            for (let sel of selList) {
                                                sel = sel.replace(/[\r\n\t ]+/g, ' ')
                                                fee_desc_more.push(sel);

                                            }
                                        }
                                    }
                                }
                            }

                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    feesDict.international_student_all_fees = fee_desc_more
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.course_tuition_fee);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }
                        case 'program_code': {
                            let courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);

                            var program_code = "";
                            var newProgCode = courseKeyVal.split(" ")[0];
                           
                            // if (program_code.length > 0) {
                            if (courseKeyVal && courseKeyVal.length > 0) {
                                resJsonData.program_code = newProgCode;
                            } else {
                                program_code = "";
                            }

                            //}
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            let courseIntakeDisplay = [];
                            var campus = resJsonData.course_campus_location;
                            if (courseIntakeDisplay) {
                                console.log("In Main If", courseIntakeDisplay);
                                var myintake = [];
                                var intakedata = {}
                                var arr = [];
                                console.log("arr--->", arr)
                                for (i = 0; i < campus.length; i++) {
                                    var intakedetail = {};
                                    console.log("campus--->", campus[i].name)
                                    intakedetail.name = campus[i].name;
                                    intakedetail.value = arr;
                                    myintake.push(intakedetail)
                                    console.log("CourseDetails-->", JSON.stringify(myintake))
                                }
                                intakedata.intake = myintake
                                intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                resJsonData.course_intake = intakedata;
                            }
                            break;
                        }

                        case 'course_cricos_code': {
                            //const courseKeyVal = courseScrappedData.course_cricos_code;
                            var mycodes = [];
                            var academicReq = null;
                            let CricosStudent = await utils.getValueFromHardCodedJsonFile('course_details');
                            
                            var course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            
                            let tmpvar = CricosStudent.filter(val => {
                                return val.name.toLowerCase() == course_title.toLowerCase()
                            });
                           
                            let one = tmpvar[0].cricos

                            let course_cricos_code = one;
                            

                            if (course_cricos_code) {
                                const locations = resJsonData.course_campus_location;
                                
                                
                                for (let location of locations) {
                                    mycodes.push({
                                        location: location.name, code: course_cricos_code, iscurrent: false
                                    });
                                }
                                resJsonData.course_cricos_code = mycodes;
                                
                            } else {
                                throw new Error("CRICOS not found");
                            }
                            //resJsonData.course_cricos_code = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            break;
                        }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }

                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r careerselList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                // course_career_outcome = selList;
                                                for (let selItem of selList) {
                                                    
                                                    selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ');
                                                    course_career_outcome.push(selItem.trim())
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }
                        // case 'course_study_level':
                        case 'course_study_level':
                            {
                                let CricosStudent11153 = await utils.getValueFromHardCodedJsonFile('course_details');
                               
                                var course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                                
                                let tmpvar = CricosStudent11153.filter(val => {
                                    return val.name.toLowerCase() == course_title.toLowerCase()
                                });
                                
                                let one = tmpvar[0].cricos

                                let course_cricos_code = one;
                               
                                let cStudyLevel = await format_functions.getMyStudyLevel(course_cricos_code);

                                resJsonData.course_study_level = cStudyLevel
                            }
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                console.log("lastlocation---->", resJsonData.course_title);

                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                console.log("prevlocation---->");
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                 var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                resJsonData.basecourseid = location_wise_data.course_id;

                for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                    if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                        resJsonData.course_cricos_code[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_cricos_code[i].iscurrent = false;
                    }
                }

                for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                    if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                        resJsonData.course_intake.intake[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_intake.intake[i].iscurrent = false;
                    }
                }

                for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                    if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                        resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                    }
                }

                for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                    if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                        resJsonData.course_campus_location[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_campus_location[i].iscurrent = false;
                    }
                }
                //resJsonData.course_cricos_code = location_wise_data.cricos_code;
                resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;

                //resJsonData.current_course_intake = location_wise_data.course_intake;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
                // const res = await awsUtil.putFileInBucket(filelocation);
                // console.log(funcName + 'S3 object location = ' + res);
            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
    // class
    static titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    }
}
module.exports = { ScrapeCourse };
