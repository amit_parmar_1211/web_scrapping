const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');

request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            const courseKeyVal = courseScrappedData.course_discipline;
                            let course_discipline = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_discipline = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            resJsonData.course_discipline = (course_discipline) ? [course_discipline] : [];
                            break;

                        }
                        case 'course_title': {
                            const courseKeyVal = courseScrappedData.course_title;
                            let course_title = null;
                            let splitdata = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_title = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("CourseTitle@@@@@@", course_title)
                            let arr = []
                            if (course_title) {
                                course_title.forEach(element => {
                                    if (element.includes("|")) {
                                        splitdata = element.split("|")[0]
                                        arr.push(splitdata)
                                        console.log("New arr-->", arr);
                                        resJsonData.course_title = String(arr)
                                        var ctitle = format_functions.titleCase(resJsonData.course_title);
                                        console.log(" resJsonData.course_title2", resJsonData.course_title);
                                        resJsonData.course_title = ctitle.trim();
                                        console.log("New Title-->2", resJsonData.course_title);
                                    }
                                    else {
                                        resJsonData.course_title = String(course_title);
                                        var ctitle = format_functions.titleCase(resJsonData.course_title);
                                        console.log(" resJsonData.course_title2", resJsonData.course_title);
                                        resJsonData.course_title = ctitle.trim();
                                        console.log("New Title2-->", resJsonData.course_title);
                                    }
                                })
                            }
                            break;
                        }
                        case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; let ieltsNumber = null;
                            var ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_req);
                            var pte_req = await Course.extractValueFromScrappedElement(courseScrappedData.pte_req);
                            var ibt_req = await Course.extractValueFromScrappedElement(courseScrappedData.ibt_req);

                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppteDict = {};
                            var penglishList = [];
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                            if (ielts_req) {
                                ieltsScore = await utils.giveMeNumber(ielts_req);
                            }
                            if (ibt_req) {
                                ibtScore = await utils.giveMeNumber(ibt_req);
                            }
                            if (pte_req) {
                                pteScore = await utils.giveMeNumber(pte_req);
                            }
                            if (ieltsScore != "NA") {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = ielts_req
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsScore
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                englishList.push(pieltsDict);

                            }
                            if (ibtScore != "NA") {
                                pibtDict.name = 'toefl ibt';
                                pibtDict.description = ibt_req;
                                pibtDict.min = 0;
                                pibtDict.require = ibtScore;
                                pibtDict.max = 120;
                                pibtDict.R = 0;
                                pibtDict.W = 0;
                                pibtDict.S = 0;
                                pibtDict.L = 0;
                                pibtDict.O = 0;
                                englishList.push(pibtDict);
                            }
                            if (pteScore != "NA") {
                                ppteDict.name = 'pte academic';
                                ppteDict.description = pte_req
                                ppteDict.min = 0;
                                ppteDict.require = pteScore;
                                ppteDict.max = 90;
                                ppteDict.R = 0;
                                ppteDict.W = 0;
                                ppteDict.S = 0;
                                ppteDict.L = 0;
                                ppteDict.O = 0;
                                englishList.push(ppteDict);
                            }

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            } else {
                                courseAdminReq.english = []
                            }

                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var entry_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_entry_more_details);
                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            const courseKeyVal = courseScrappedData.course_academic_requirement;

                            if (courseScrappedData.course_academic_requirement) {
                                //  let academicReq = null;
                                var academicReq = [];
                                academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + [academicReq]);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                let arr = []
                                arr.push(academicReq)

                                if (academicReq && academicReq.length > 0) {

                                    courseAdminReq.academic = arr;
                                    console.log('courseAdminReq = ', courseAdminReq.academic);
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }
                            //courseAdminReq.academic = (academicReq) ? academicReq : "";
                            courseAdminReq.english_requirements_url = english_more;
                            courseAdminReq.entry_requirements_url = "";
                            courseAdminReq.academic_requirements_url = academic_more;
                            //courseAdminReq.academic_more_details = academic_more;
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("DATA@@@", fullTimeText)
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("filtered_duration_formated", filtered_duration_formated);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;

                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston
                            const courseKeyVals = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            let array11 = []
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList
                                                console.log("criccoos-->", course_cricos_code);
                                                if (course_cricos_code.length > 0) {
                                                    course_cricos_code.forEach(element => {
                                                        if (element.includes("CRICOS:")) {
                                                            splitdata = element.split("CRICOS:")[1]
                                                            array11.push(splitdata)
                                                            console.log("progrcode-->", array11);
                                                            console.log("progrcode1--->", splitdata);
                                                        }
                                                    })
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                            
                            var campLocationText = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            console.log("##Campus-->" + campLocationText)
                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                var campusedata = [];
                                campLocationText.forEach(element => {
                                    campusedata.push({
                                        "name": element,
                                        "code":String(array11)
                                       
                                    })
                                });
                               
                                resJsonData.course_campus_location = campusedata;//await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                               
                                   let study_mode = "On campus";                                
                                resJsonData.course_study_mode = study_mode;//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                               
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {
                                
                            };

                            var fee_desc = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal_fee = courseScrappedData.course_tuition_fees_international_student;
                            if (Array.isArray(courseKeyVal_fee)) {
                                for (const rootEleDict of courseKeyVal_fee) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            fee_desc = selList;
                                            ///}
                                        }
                                    }
                                }
                            }

                            var fee_desc_more = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal_more = courseScrappedData.course_tuition_fees_international_student_more;
                            if (Array.isArray(courseKeyVal_more)) {
                                for (const rootEleDict of courseKeyVal_more) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            fee_desc_more = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            // if (feeYear && feeYear.length > 0) {
                            //     courseTuitionFee.year = feeYear;
                            // }

                            const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                            const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                            if (feesDuration && feesDuration.length > 0) {
                                feesDict.fee_duration_years = feesDuration;
                            }
                            if (feesCurrency && feesCurrency.length > 0) {
                                feesDict.currency = feesCurrency;
                            }
                            var campus = resJsonData.course_campus_location;
                            for (let loc of campus) {
                                //for (let element of fee_desc) {
                                //if (element.toLowerCase().trim().includes(loc.toLowerCase())) {
                                console.log("FEEE_DEiC-->" + JSON.stringify(fee_desc[0]));
                                if (fee_desc[0]) {
                                    var fee = fee_desc[0].split("$");
                                    feesDict.international_student={

                                        amount: await utils.giveMeNumber(fee[1].toString().replace(/[\r\n\t ]+/g, ' ')),
                                        duration: 1,
                                        unit: "Semester",                                       
                                        description: String(fee_desc_more)
                                       
                                    };

                                    //feesDict.international_student = await utils.giveMeNumber(fee[1].toString().replace(/[\r\n\t ]+/g, ' '));
                                }
                                else {
                                    feesDict.international_student={
                                        // amount: fee.fees,
                                        amount: 0,
                                        duration: 1,
                                        unit: "Year",                                      
                                        description: ""
                                    
                                    };
                                    //feesDict.international_student = [];
                                }
                                //feesDict.international_student_all_fees = fee_desc_more;
                                feesList.push({ name: loc.name, value: feesDict });
                                //}
                                //}
                            }
                            if (feesList && feesList.length > 0) {
                                courseTuitionFee.fees = feesList;
                            }
                            console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                            if (courseTuitionFee) {
                                resJsonData.course_tuition_fee = courseTuitionFee;
                            }
                            break;
                        }

                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = "";
                            var splitdata;
                            var arr = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }

                            if (program_code.length > 0) {
                                program_code.forEach(element => {
                                    if (element.includes("|")) {
                                        splitdata = element.split("|")[1]
                                        arr.push(splitdata)
                                        console.log("progrcode-->", arr);
                                        console.log("progrcode1--->", splitdata);
                                    }
                                })

                                //*/div[contains(strong/text(),'Course/s')]/following-sibling::div//p/strong
                                resJsonData.program_code = String(arr);
                            }
                            else {
                                resJsonData.program_code = "";
                            }
                            console.log("Program_code---->", resJsonData.program_code)
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            // Semester 1 (February) and Semester 2 (August)
                            // prepare emty JSON struct
                            const courseIntakeDisplay = {};
                            // existing intake value
                            var courseIntakeStr = null;//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                courseIntakeStr = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            // if (String(courseIntakeStr).includes(',')) {
                            //     var splitdata = String(courseIntakeStr).split(",")
                            //     console.log("SplitDAta@@@@@@", splitdata)
                            //     courseIntakeStr = splitdata
                            // }
                            if (!courseIntakeStr) {
                                courseIntakeStr = [""];
                            }
                            // for (let split_data of courseIntakeStr) {
                            //     let split_val = String(split_data).split(',');
                            //     console.log("split_val--->", split_val);
                            //     courseIntakeStr.push(split_val)
                            // }
                            let split_val = String(courseIntakeStr).split(',');
                            courseIntakeStr = split_val
                            console.log("IntakeDATA@@@@@@@@", JSON.stringify(courseIntakeStr))
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var intakes = [];
                                var campus = resJsonData.course_campus_location;

                                for (let loc of campus) {
                                    var intakedetail = {};
                                    var myintake = [];
                                    var myintake1 = [];
                                    courseIntakeStr[0]
                                    myintake = await utils.giveMeArray(String(courseIntakeStr).replace(/[\r\n\t()]+/g, '').replace('2020', '').replace('Semester 1', '').replace('  ', '').replace(' ', '').replace(':', '').replace('Semester 2', '').replace(' ', '').replace(' ', ''), "and")
                                    console.log("myintakes--->", myintake);


                                    intakedetail.name = loc.name;
                                    intakedetail.value = myintake;
                                    intakes.push(intakedetail);
                                }

                                var intakedata = {};
                                //var intakeurl= await Course.extractValueFrom
                                let formatIntake = await format_functions.providemyintake(intakes, "mom", "");
                                console.log("FormatIntake", JSON.stringify(formatIntake))
                                intakedata.intake = formatIntake;
                                intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                resJsonData.course_intake = intakedata;
                            }
                            break;
                        }
                        
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        
                       
                        // case 'course_cricos_code': {
                        //     const courseKeyVal = courseScrappedData.course_cricos_code;
                        //     let course_cricos_code = null;
                        //     let array11 = []
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                     if (Array.isArray(selList) && selList.length > 0) {
                        //                         course_cricos_code = selList
                        //                         console.log("criccoos-->", course_cricos_code);
                        //                         if (course_cricos_code.length > 0) {
                        //                             course_cricos_code.forEach(element => {
                        //                                 if (element.includes("CRICOS:")) {
                        //                                     splitdata = element.split("CRICOS:")[1]
                        //                                     array11.push(splitdata)
                        //                                     console.log("progrcode-->", array11);
                        //                                     console.log("progrcode1--->", splitdata);
                        //                                 }
                        //                             })
                        //                         }
                        //                     }

                        //                 }
                        //             }
                        //         }
                        //     }
                        //     if (array11.length > 0) {
                        //         //  global.cricos_code = course_cricos_code[0].split(":")[1];
                        //         var locations = resJsonData.course_campus_location;
                        //         var mycodes = [];
                        //         for (let location of locations) {
                        //             mycodes.push({ location: location.name, code: array11.toString(), iscurrent: false });
                        //         }
                        //         resJsonData.course_cricos_code = mycodes;
                        //     } else {
                        //         throw new Errorr("cricose code not found");
                        //     }
                        //     break;
                        // }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            resJsonData.course_overview = (course_overview) ? course_overview : "";
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }

                        case 'course_study_level': {
                            // const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            const courseKeyVal = courseScrappedData.course_cricos_code;
                            let course_study_level
                            let course_cricos_code = null;
                            let splitdata
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            let cStudyLevel = null;
                            console.log(funcName + 'course_title course_study_level = ' + course_cricos_code);
                            if (course_cricos_code.length > 1) {
                                course_study_level = "Package Course"

                            } else {
                                if (String(course_cricos_code).includes(":")) {
                                    splitdata = String(course_cricos_code).split(":")[1]

                                }
                                console.log("SpitData@@@@@@@@", splitdata)
                                const studylevel = await format_functions.getMyStudyLevel(splitdata.trim())
                                console.log("Study_level", studylevel)
                                course_study_level = studylevel

                            }

                            resJsonData.course_study_level = course_study_level
                            

                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                            const courseKeyVal_major = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline);
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                            let course_outlines = {};
                            if (courseKeyVal_minor != null) {
                                course_outlines.minors = courseKeyVal_minor
                            } else {
                                course_outlines.minors = []
                            }
                            if (courseKeyVal_major != null) {
                                course_outlines.majors = courseKeyVal_major
                            } else {
                                course_outlines.majors = []
                            }
                            if (courseKeyVal != null) {
                                course_outlines.more_details = courseKeyVal
                            } else {
                                course_outlines.more_details = ""
                            }
                            resJsonData.course_outline = course_outlines
                            break;
                        }
                        case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            if (courseKeyVal != null) {
                                resJsonData.application_fees = courseKeyVal
                            } else {
                                resJsonData.application_fees = ""
                            }
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            // console.log('\n\r' + funcName + 'course resJsonData =' + JSON.stringify(resJsonData));
            // var resJsonData = JSON.parse(fs.readFileSync("australiancatholicuniversity_graduatediplomainrehabilitation_coursedetails.json"))[0];
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_title))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                console.log("DATTATT@@@@", console.log(NEWJSONSTRUCT.course_id))
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;

                
                var filelocation = "./output/" + resJsonData.univ_id + "_" +basecourseid.replace(/[\n :""]+/g, '').replace(/[\/]+/g, '_') + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

                // try {
                //     if (appConfigs.ISINSERT) {
                //         request.post({
                //             "headers": { "content-type": "application/json" },
                //             "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                //             "body": JSON.stringify(resJsonData)
                //         }, (error, response, body) => {
                //             if (error) {
                //                 console.log(error);
                //             }
                //             var myresponse = JSON.parse(body);
                //             if (!myresponse.flag) {
                //                 var errorlog = null;
                //                 if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                //                     errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                //                     errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                //                 }
                //                 fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                //             }
                //             console.log("success save data! " + JSON.stringify(body));
                //         });
                //     }
                //     else if (appConfigs.ISREPLACE) {
                //         request.post({
                //             "headers": { "content-type": "application/json" },
                //             "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                //             "body": JSON.stringify(resJsonData)
                //         }, (error, response, body) => {
                //             if (error) {
                //                 console.log(error);
                //             }
                //             var myresponse = JSON.parse(body);
                //             console.log("MyMessage-->" + JSON.stringify(myresponse))
                //             if (!myresponse.flag) {
                //                 var errorlog = null;
                //                 if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                //                     errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                //                     errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                //                 }
                //                 fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                //             }
                //             console.log("success save data! " + JSON.stringify(body));
                //         });
                //     }
                //     else if (appConfigs.ISUPDATE) {
                //         request.post({
                //             "headers": { "content-type": "application/json" },
                //             "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                //             "body": JSON.stringify(resJsonData)
                //         }, (error, response, body) => {
                //             if (error) {
                //                 console.log(error);
                //             }
                //             var myresponse = JSON.parse(body);
                //             if (!myresponse.flag) {
                //                 var errorlog = null;
                //                 if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                //                     errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                //                     errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                //                 }
                //                 fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                //             }
                //             console.log("success save data! " + JSON.stringify(body));
                //         });
                //     }
                // }
                // catch (err) {
                //     console.log("#### try-catch error--->" + err);
                // }
                // const res = await awsUtil.putFileInBucket(filelocation);
                //console.log(funcName + 'S3 object location = ' + res);
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };