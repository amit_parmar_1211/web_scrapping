const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const appConfigs = require('./common/app-config');
class ScrapeCourseList extends Scrape {
  // refers to http://waifs.wa.edu.au/courses/
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'startScrappingFunc ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var datalist = [];
      await s.setupNewBrowserPage("https://www.barklycollege.vic.edu.au/");
      const categoryselectorUrl = "//li[@id='nav-menu-item-5147']/div[@class='second']/div[@class='inner']/ul/li/a"
      const targetLinksCardsUrls = await s.page.$x(categoryselectorUrl);
      const categoryselectorUrl1 = "//li[@id='nav-menu-item-5147']/div[@class='second']/div[@class='inner']/ul/li/a/span/span/text()";
      const targetLinksCardsUrls1 = await s.page.$x(categoryselectorUrl1);
      console.log("targetLinksCardsUrls1--->",targetLinksCardsUrls1);
      var targetLinks = []
      for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        var elementstring = await s.page.evaluate(el => el.href, targetLinksCardsUrls[i]);
        var elementhref = await s.page.evaluate(el => el.textContent, targetLinksCardsUrls1[i]);
        console.log("elementhref--->",elementhref);
        elementstring = elementstring.replace("\n", " ");
        targetLinks.push({ href: elementstring, innerText: elementhref });
      }
      console.log("MAinCategory-->", targetLinks)
      await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinks));
      var totalCourseList = [];
      for (let target of targetLinks) {
        await s.page.goto(target.href, { timeout: 0 });
        console.log("In_for------>")
        let linkselector = await s.page.$x("//li[@id='nav-menu-item-5147']/div[@class='second']/div[@class='inner']/ul/li/a/span/span[contains(text(),'" + target.innerText + "')]/following::ul[1]/li/a")
        let textselector = await s.page.$x("//li[@id='nav-menu-item-5147']/div[@class='second']/div[@class='inner']/ul/li/a/span/span[contains(text(),'" + target.innerText + "')]/following::ul[1]/li/a/span")
        console.log("Length@@@", linkselector.length)
        for (let i = 0; i < linkselector.length; i++) {
          const elementurl = await s.page.evaluate(el => el.href, linkselector[i])
          console.log("elementurl-->", elementurl)
          const elementstring = await s.page.evaluate(el => el.textContent, textselector[i])
          console.log("elementstring-->", elementstring)
         
            totalCourseList.push({ href: elementurl, innerText: elementstring, category: target.innerText });
          
        }
      }
      await fs.writeFileSync("./output/barklyinstitute_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
      await fs.writeFileSync("./output/barklyinstitute_unique_courselist.json", JSON.stringify(uniqueUrl));

      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }

          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/barklyinstitute_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
      if (s) {
        await s.close();
      }
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };