const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            let d;
                            let course_discipline = await Course.extractValueFromScrappedElement(courseScrappedData.course_discipline);
                            console.log("resJsonData.course_title3333gdfbgdbh", course_discipline);
                            if (course_discipline.includes("/")) {
                                d = course_discipline.split("/")[0]
                                console.log("course_title3333gdfbgdbh", d);
                                resJsonData.course_discipline = [d];
                            }
                            else {
                                console.log("ISelse--->")
                                resJsonData.course_discipline = [course_discipline];

                            }

                            break;
                        }

                        case 'course_title': {
                            resJsonData.course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("resJsonData.course_title3333", resJsonData.course_title);
                            const title = String(resJsonData.course_title);
                            console.log("title ", title);
                            let program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            const pro = program_code;
                            let pr = pro.split(" ")[0];
                            console.log("programwithtitle ", pr);
                            if (title.indexOf(pr) > -1) {
                                var splitCricos = title.split(pr)[1];
                                console.log("gdzdhghf", splitCricos);
                                resJsonData.course_title = splitCricos.trim();
                            } else {
                                resJsonData.course_title = title.trim();;
                            }
                            if (title == "General English") {
                                console.log("General English_General English", title)
                                resJsonData.course_title = title.trim();;

                            }
                        }


                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':

                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            let f1, f2;
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                                console.log("##fullTimeText--->" + JSON.stringify(fullTimeText));
                                                if (fullTimeText.indexOf("(") > -1) {
                                                    f1 = fullTimeText.replace("(", '');
                                                    console.log("fullTimeText@@@@", f1);
                                                    if (f1.indexOf(")") > -1) {
                                                        f2 = f1.replace(")", '');
                                                        console.log("fullTimeText@@@@", f2);
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            if (f2 && f2.length > 0) {
                                const resFulltime = f2;
                                if (resFulltime) {
                                    durationFullTime = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = durationFullTime;

                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {};; let ieltsNumber = null;
                            let ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_req);
                            let pbt = await Course.extractValueFromScrappedElement(courseScrappedData.toefl_pbt);
                            let ibt = await Course.extractValueFromScrappedElement(courseScrappedData.toefl_ibt);
                            let pte = await Course.extractValueFromScrappedElement(courseScrappedData.pte);
                            let ibt1, pbt1, pte1;
                            console.log("pbt#######", pbt);
                            if (pbt.includes("PBT")) {
                                pbt1 = pbt.split("PBT")[1];
                                console.log(" pbt1@@@@@@", ibt1);
                            }

                            console.log("ibt#######", ibt);
                            if (ibt.includes("iBt")) {
                                ibt1 = ibt.split("iBt")[1];
                                console.log("ibt1@@@@@@", ibt1);
                            }

                            console.log("pte######", pte);
                            if (pte.includes("PTE")) {
                                pte1 = pte.split("PTE")[1];
                                console.log("pte1@@@@@@", pte1);
                            }
                            console.log("##ielts_req-->" + ielts_req);
                            if (ielts_req.length == 0) {
                                //  ielts_req = "Academic IELTS of 5.5 with 5.0 in all bands, or equivalent.";
                            }

                            ///progress bar start
                            const pieltsDict = {}; const pbtDict = {}; const ppteDict = {}; const ibtDict = {}; const caeDict = {};
                            var penglishList = [];
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "", pbtScore = "";
                            if (ielts_req) {
                                ieltsScore = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                            }
                            if (pbt1) {
                                pbtScore = await utils.giveMeNumber(pbt1.replace(/ /g, ' '));
                            }
                            if (ibt1) {
                                ibtScore = await utils.giveMeNumber(ibt1.replace(/ /g, ' '));
                            }
                            if (pte1) {
                                pteScore = await utils.giveMeNumber(pte1.replace(/ /g, ' '));
                            }



                            if (ieltsScore) {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = ielts_req;
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsScore;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }
                            if (pteScore) {
                                ppteDict.name = 'pte academic';
                                ppteDict.description = "PTE" + pte1.replace("IELTS 5.5  with a minimum 5.0 in each band, or equivalent TOEFL iBt 46, TOEFL PBT 527, or", '');
                                ppteDict.min = 0;
                                ppteDict.require = pteScore;
                                ppteDict.max = 90;
                                ppteDict.R = 0;
                                ppteDict.W = 0;
                                ppteDict.S = 0;
                                ppteDict.L = 0;
                                ppteDict.O = 0;
                                penglishList.push(ppteDict);
                            }
                            if (ibtScore) {
                                ibtDict.name = 'toefl ibt';
                                ibtDict.description = "TOEFL iBt" + ibt1.replace(", TOEFL PBT 527, or PTE 42", '');
                                ibtDict.min = 0;
                                ibtDict.require = ibtScore;
                                ibtDict.max = 120;
                                ibtDict.R = 0;
                                ibtDict.W = 0;
                                ibtDict.S = 0;
                                ibtDict.L = 0;
                                ibtDict.O = 0;
                                penglishList.push(ibtDict);
                            }
                            if (pbtScore) {
                                pbtDict.name = 'toefl pbt';
                                pbtDict.description = "TOEFL PBT" + pbt1.replace(", or PTE 42", '');
                                pbtDict.min = 310;
                                pbtDict.require = pbtScore;
                                pbtDict.max = 677;
                                pbtDict.R = 0;
                                pbtDict.W = 0;
                                pbtDict.S = 0;
                                pbtDict.L = 0;
                                pbtDict.O = 0;
                                penglishList.push(pbtDict);
                            }

                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            } else {
                                throw new Error("english not found");
                            }

                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var academicReq = "";
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList;
                                        }
                                    }
                                }
                            }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            courseAdminReq.entry_requirements_url = entry_requirements_url;
                            courseAdminReq.english_requirements_url = english_more;
                            courseAdminReq.academic_requirements_url = academic_more;
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_cricos_code':


                        case 'course_campus_location': { // Location Launceston
                            var campLocationText; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            const courseKeyVal1 = courseScrappedData.course_cricos_code;
                            let cricosIntStudent1 = JSON.parse(fs.readFileSync(appConfigs.Locations));
                            console.log("cricosIntStudentdfsfd----->>>>>", cricosIntStudent1);
                            var title1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            // .replace('\n', '')
                            //   let title11 = title.split('|')[0];
                            //   title = title11.split('–')[1];
                            console.log("splitStr@@@2Sfsdf" + title1);
                            let tmpvar1 = cricosIntStudent1.filter(element => {
                                return element.Course_Name.toLowerCase().trim() == title1.toLowerCase().trim()
                            });
                            console.log("tmpvarcsds----->>>>>", tmpvar1);
                            let CRICOS = tmpvar1[0].Course_code;
                            console.log("one----->>>>>", CRICOS);

                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            let newcampus = [];
                            for (let campus of campLocationText) {
                                if (!newcampus.includes(campus)) {
                                    newcampus.push(campus);
                                    console.log("##Campuscampus-->" + campus)
                                }
                            }
                            // if (campus.indexOf(String(Larr)) > - 1) {
                            //     console.log("campus-------->", campus);
                            // }

                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            //   let valfainal =campLocationText.toString();
                            var finallocation = [];
                            var loca = String(campLocationText).split(',');
                            loca.forEach(element => {
                                if (element.includes('Campus')) {
                                    finallocation.push(element.replace('Campus', ''))
                                }
                            })
                            var campusss = [];
                            if (finallocation.length > 0) {
                                console.log("loca------>", finallocation);
                                let campuses = ['Melbourne Campus', 'Hobart Campus']
                                console.log("@@campuses------->", campuses);

                                for (let i = 0; i < finallocation.length; i++) {
                                    campuses.forEach(element => {
                                        console.log(element.includes(finallocation[i].trim()))
                                        if (element.includes(finallocation[i].trim())) {

                                            campusss.push(element)

                                        }
                                    })
                                }
                                console.log("@@campuses------->", campusss)
                                // if (campuses.indexOf(loca) > -1) {
                                //     console.log("it_is_in_if", loca);
                                // }


                                if (campusss && campusss.length > 0) {
                                    var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                    resJsonData.course_study_mode = "On campus";
                                    //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                    // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                    var campusedata = [];
                                    campusss.forEach(element => {
                                        campusedata.push({
                                            "name": element,
                                            "code": CRICOS

                                        })
                                    });
                                    resJsonData.course_campus_location = campusedata;
                                    // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                    var study_mode = [];
                                    // campLocationValTrimmed = campLocationValTrimmed.toLowerCase();

                                    // campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                                    if (campLocationValTrimmed.trim().length > 0) {
                                        study_mode.push('On campus');
                                    }
                                    resJsonData.course_study_mode = study_mode.toString();//.join(',');
                                    console.log("## FInal string-->" + campLocationValTrimmed);
                                    // }
                                } // if (campLocationText && campLocationText.length > 0)

                            }
                            break;
                        }


                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };

                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

                            let resScholarshipJson = null;


                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }




                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }

                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)

                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;


                            break;
                        }
                        case 'application_fee':
                            {
                                //let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);

                                let applicationfee = JSON.parse(fs.readFileSync(appConfigs.Locations));

                                resJsonData.application_fee = ""
                                break;
                            }
                        case 'course_study_mode':
                        case 'course_tuition_fees_international_student_more'://http://www.altec.edu.au/wp-content/uploads/2019/11/ALTEC-Fee_Schedule_2020.pdf
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};

                            //international_student: [],
                            let cricosIntStudent = JSON.parse(fs.readFileSync(appConfigs.Locations));
                            console.log("cricosIntStudent----->>>>>", cricosIntStudent);
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            console.log("splitStr@@@2" + title);
                            let tmpvar = cricosIntStudent.filter(element => {
                                return element.Course_Name.toLowerCase().trim() == title.toLowerCase().trim()
                            });
                            console.log("tmpvar----->>>>>", tmpvar);
                            let CRICOS = tmpvar[0].Tuition_Fee;
                            console.log("one----->>>>>", CRICOS);
                            if (CRICOS && CRICOS.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(CRICOS).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const arrval = String(feesWithDollorTrimmed).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        if (Number(feesNumber)) {
                                            var feesint = {};
                                            feesint.amount = Number(feesNumber);
                                            feesint.duration = 1;
                                            feesint.unit = "year";
                                            feesint.description = CRICOS;
                                            feesDict.international_student = feesint;
                                        }

                                        if (feesNumber == "0") {
                                            var feesint = {};
                                            feesint.amount = 0;
                                            feesint.duration = 1;
                                            feesint.unit = "year";
                                            feesint.description = "";
                                            feesDict.international_student = feesint;

                                        }
                                    }
                                }
                            }
                            else {

                                var feesint = {};
                                feesint.amount = 0;
                                feesint.duration = 1;
                                feesint.unit = "year";
                                feesint.description = "";
                                feesDict.international_student = feesint;

                            }
                            var fee_desc_more = [];
                            let tmpvar1 = cricosIntStudent.filter(element => {
                                return element.Course_Name.toLowerCase().trim() == title.toLowerCase().trim()
                            });
                            console.log("tmpvar----->>>>>", tmpvar);
                            let CRICOS1 = tmpvar1[0].Material_Fee;
                            console.log("one----->>>>>", CRICOS1);
                            fee_desc_more.push(CRICOS1)
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    // feesDict.international_student_all_fees = fee_desc_more
                                    console.log("morefees@@2", fee_desc_more);
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                        console.log("feesList------->", feesList);
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    console.log("courseTuitionFee------->", courseTuitionFee);
                                    resJsonData.course_tuition_fee = courseTuitionFee;

                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }


                        case 'program_code': {
                            let program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            console.log("program_code@@@@@", program_code)
                            let pr = program_code.split(" ")[0];
                            console.log("pr@@@@@@@@@@", pr);
                            resJsonData.program_code = pr.trim();
                            if (program_code == "General English") {
                                console.log("General English_General English_General", program_code)
                                resJsonData.program_code = "";

                            }
                            //General English



                            break;
                        }

                        case 'course_intake': {
                            var courseIntake = [];
                            const courseIntakeStr = await utils.getValueFromHardCodedJsonFile('course_intake');
                            console.log("courseIntakeStr@@@@@", courseIntakeStr)
                            for (let temp of courseIntakeStr) {
                                temp = temp.trim();
                                courseIntake.push(temp);
                            }
                            var locationArray = resJsonData.course_campus_location;
                            console.log("locationArray", locationArray);

                            if (courseIntake && courseIntake.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let location of locationArray) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location.name,
                                        "value": courseIntake

                                    });
                                    console.log("resJsonData.course_intake.intake", resJsonData.course_intake.intake);

                                }
                                let formatIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");
                                var intakeUrl = [];
                                console.log("formatIntake@@@@@", formatIntake);
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                resJsonData.course_intake.more_details = intakeUrl + "";
                                resJsonData.course_intake.intake = formatIntake;
                                let valmonths11 = "";
                            }
                            break;
                        }
                        case 'course_scholarship': {
                            const courseKeyVal = courseScrappedData.course_scholarship;
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                resJsonData.course_scholarship = resScholarshipJson;
                            }
                            break;
                        }
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                        case 'course_accomodation_cost': {
                            const courseKeyVal = courseScrappedData.course_accomodation_cost;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_accomodation_cost = resAccomodationCostJson;
                            }
                            break;
                        }
                        case 'course_cricos_code': {
                            const courseKeyVal = courseScrappedData.course_cricos_code;
                            const mycodes = [];
                            let course_cricos_code = null;
                            let cricosIntStudent = JSON.parse(fs.readFileSync(appConfigs.Locations));
                            console.log("cricosIntStudent----->>>>>", cricosIntStudent);
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)

                            console.log("splitStr@@@2" + title);
                            let tmpvar = cricosIntStudent.filter(element => {
                                return element.Course_Name.toLowerCase().trim() == title.toLowerCase().trim()
                            });
                            console.log("tmpvar----->>>>>", tmpvar);
                            let CRICOS = tmpvar[0].Course_code;
                            console.log("one----->>>>>", CRICOS);
                            if (CRICOS) {
                                const locations = resJsonData.course_campus_location;
                                console.log("locations----->", locations);
                                console.log("course_cricos_code----->", CRICOS);
                                for (let location of locations) {
                                    mycodes.push({
                                        location: location.name, code: CRICOS, iscurrent: false
                                    });
                                }
                                resJsonData.course_cricos_code = mycodes;
                            } else {
                                throw new Error("course_cricos_code not found");
                            }
                            break;
                        }


                        case 'course_country': {
                            resJsonData.course_country = await Course.extractValueFromScrappedElement(courseScrappedData.course_country)
                            break;
                        }

                        case 'course_overview': {
                            resJsonData.course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            let course_overview = resJsonData.course_overview;
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            for (let sel of selList) {
                                                sel = sel.replace(/[\r\n\t ]+/g, ' ').trim();
                                                if (sel.length > 0) {
                                                    console.log("sel----->", sel);
                                                    course_career_outcome.push(sel)
                                                    break;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = [course_career_outcome[0]]
                            } else {
                                resJsonData.course_career_outcome = [];
                            }


                            break;
                        }

                        case 'course_study_level': {
                            let cricosIntStudent = JSON.parse(fs.readFileSync(appConfigs.Locations));
                            console.log("cricosIntStudent----->>>>>", cricosIntStudent);
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)

                            console.log("splitStr@@@2" + title);
                            let tmpvar = cricosIntStudent.filter(element => {
                                return element.Course_Name.toLowerCase().trim() == title.toLowerCase().trim()
                            });
                            console.log("tmpvar----->>>>>", tmpvar);
                            let CRICOS = tmpvar[0].Course_code;
                            console.log("one----->>>>>", CRICOS);

                            const study_val = await format_functions.getMyStudyLevel(CRICOS);
                            resJsonData.course_study_level = study_val;
                            console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/  .json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;

            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }


                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);

            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
