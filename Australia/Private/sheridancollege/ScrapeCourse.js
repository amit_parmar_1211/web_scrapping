const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_outline_majors': {
                            var course_career_outline = {};
                            let majors = []
                            let minors = []
                            var courseKeyVal1 = courseScrappedData.course_outline_majors;
                          //  var courseKeyVal2 = courseScrappedData.course_outline_minors;
                            console.log("majors------>", JSON.stringify(courseKeyVal1));
                           // console.log("minors--->", JSON.stringify(courseKeyVal2));
              
                            if (courseKeyVal1 && courseKeyVal1.length > 0) {
                              for (const rootEleDict of courseKeyVal1) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                for (const eleDict of elementsList) {
                                  console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                  const selectorsList = eleDict.selectors;
                                  for (const selList of selectorsList) {
                                    console.log(funcName + '\n\r selListoutcome1 = ' + JSON.stringify(selList).replace(/[\r\n\t]+/g, ' '));
              
                                    if (Array.isArray(selList) && selList.length > 0) {
                                      //  let out_val = String(selList).replace(/[\r\n\t ]+/g, ' ');
                                      //var selList1 = String(selList).replace(/[\r\n\t ]+/g, '')
                                      // majors.push(selList);
                                      majors = 
                                       
                                         selList,
                                      
                                      course_career_outline = majors;
              
                                      console.log("course_career_outle--?", course_career_outline);
              
                                    }
                                    else {
                                      majors = 
                                       [],
                                       
                                      
                                      course_career_outline = majors;
                                    }
                                  }
                                }
                              }
                            }
              
                            // if (courseKeyVal2 && courseKeyVal2.length > 0) {
                            //   console.log("entered-->")
                            //   for (const rootEleDict of courseKeyVal2) {
                            //     console.log(funcName + '\n\r rootEleDictf = ' + JSON.stringify(rootEleDict));
                            //     const elementsList = rootEleDict.elements;
                            //     for (const eleDict of elementsList) {
                            //       console.log(funcName + '\n\r eleDictf = ' + JSON.stringify(eleDict));
                            //       const selectorsList = eleDict.selectors;
                            //       for (const selList of selectorsList) {
                            //         console.log(funcName + '\n\r selListoutcome2 = ' + JSON.stringify(selList).replace(/[\r\n\t]+/g, ' '));
              
                            //         if (Array.isArray(selList) && selList.length > 0) {
                            //           //  let out_val = String(selList).replace(/[\r\n\t ]+/g, ' ');
                            //           //var selList1 = String(selList).replace(/[\r\n\t ]+/g, '')
                            //           // majors.push(selList);
                            //           minors = 
                                       
                            //            selList
                                      
                            //           course_career_outline = minors;
              
                            //           console.log("course_career_outline--?", course_career_outline);
              
                            //         }
                            //         else {
                            //           minors = 
                                       
                            //              [],
                                      
                            //           course_career_outline = minors;
                            //         }
                            //       }
                            //     }
                            //   }
                            // }
                           
                            
              
                            if (course_career_outline) {
                              resJsonData.course_outline = ({
                                "majors": majors,
                                "minors": [],
                                "more_details": courseUrl
                              })
                              console.log("resJsonData.course_care--?", resJsonData.course_outline);
                            }
                            else {
                              resJsonData.course_outline = ({
                                "majors": [],
                                "minors": [],
                                "more_details": courseUrl
                              }) 
              
                            }
                            break;
                          }
                        case 'application_fee': {
                            resJsonData.application_fee = "";
                        }
                        case 'course_discipline': {


                            var cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);

                            var Tittle = cTitle.split("(CRICOS Code:")[1].trim();
                            var Tittle1 = Tittle.split(")")[0].trim();

                            if (Tittle1.includes('093569D')) {
                                resJsonData.course_discipline = ["Management and Commerce"];
                                break;

                            }
                            let DEESCIPLINE = await format_functions.getDISCIPLINE(Tittle1);

                            var DEESCIPLINE1 = DEESCIPLINE.split("-")[1].trim();
                            resJsonData.course_discipline = [DEESCIPLINE1]



                        }
                        case 'course_title_category':
                        case 'course_title': {

                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)

                            if (title.includes('MASTER OF EDUCATION (COURSEWORK)')) {
                                title = "Master Of Education"
                            }

                            var ctitle = format_functions.titleCase(title).trim();

                            //   
                            resJsonData.course_title = ctitle



                            break;
                        }

                        case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            var courseAdminReq = {};
                            const englishList = [];


                            const myscrore = await utils.getMappingScore(course_name);


                            var ibtDict = {}, ieltsDict = {}, pbtDict = {};
                            pbtDict.name = 'toefl pbt';
                            pbtDict.description = myscrore.TOEFL;
                            englishList.push(pbtDict);
                            ibtDict.name = 'toefl ibt';
                            ibtDict.description = myscrore.IBT;
                            englishList.push(ibtDict);

                            ieltsDict.name = 'ielts academic';
                            ieltsDict.description = myscrore.IELTS;
                            englishList.push(ieltsDict);



                            let ieltsNumber = null; let pbtNumber = null; let ibtNumber = null;
                            const penglishList = [];
                            var ieltsfinaloutput = null;
                            var tofelfinaloutput = null;
                            var tofelfinaloutput1 = null;
                            var ptefinaloutput = null;
                            var ibtfinaloutput = null;
                            const regEx = /[+-]?\d+(\.\d+)?/g;
                            const matchedStrList = String(myscrore.IELTS).match(regEx);
                            console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                            if (matchedStrList && matchedStrList.length > 0) {
                                ieltsNumber = Number(matchedStrList[0]);
                                console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                            }
                            if (ieltsNumber && ieltsNumber > 0) {
                                //tofel score

                                let tempTOFEL = [
                                    {
                                        "elements":
                                            [
                                                {
                                                    "selectors":
                                                        [
                                                            [myscrore.TOEFL]
                                                        ]
                                                }
                                            ]
                                    }
                                ]
                                const tofelScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempTOFEL);

                                if (tofelScore && tofelScore.length > 0) {
                                    tofelfinaloutput = tofelScore.toString();
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(tofelScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pbtNumber = Number(matchedStrList[0]);

                                    }
                                }
                            }
                            // if (ieltsNumber && ieltsNumber > 0) {
                            //cae score

                            let tempIBT = [
                                {
                                    "elements":
                                        [
                                            {
                                                "selectors":
                                                    [
                                                        [myscrore.IBT]
                                                    ]
                                            }
                                        ]
                                }
                            ]

                            const ibtAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempIBT);

                            if (ibtAScore && ibtAScore.length > 0) {
                                ibtfinaloutput = ibtAScore.toString();
                                // extract exact number from string
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                const matchedStrList = String(ibtAScore).match(regEx);
                                console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    ibtNumber = Number(matchedStrList[0]);

                                }
                            }
                            // }

                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};

                            if (ieltsNumber && ieltsNumber > 0) {
                                var IELTSJSON = {
                                    "name": "ielts academic",
                                    "description": ieltsDict.description,
                                    "min": 0,
                                    "require": ieltsNumber,
                                    "max": 9,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (ieltsNumber) {
                                    // pieltsDict.name = 'ielts academic';
                                    //  pieltsDict.value = IELTSJSON;
                                    penglishList.push(IELTSJSON);
                                }
                            }

                            if (pbtNumber && pbtNumber > 0) {
                                var PBTJSON = {
                                    "name": "toefl pbt",
                                    "description": pbtDict.description,
                                    "min": 310,
                                    "require": pbtNumber,
                                    "max": 677,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (pbtNumber) {
                                    //  pibtDict.name = 'toefl ibt';
                                    // pibtDict.value = IBTJSON;
                                    penglishList.push(PBTJSON);
                                }
                            }



                            if (ibtNumber && ibtNumber > 0) {
                                var IBTJSON = {
                                    "name": "toefl ibt",
                                    "description": ibtDict.description,
                                    "min": 0,
                                    "require": ibtNumber,
                                    "max": 120,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (ibtNumber) {
                                    // pcaeDict.name = 'cae';
                                    // pcaeDict.value = CAEJSON;
                                    penglishList.push(IBTJSON);
                                }
                            }

                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }

                            var academicReq = "";

                            let IELTSStudent111 = await utils.getValueFromHardCodedJsonFile('fees');

                            var course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            if (course_title.includes('MASTER OF EDUCATION (COURSEWORK)')) {
                                course_title = "MASTER OF EDUCATION"
                            }
                            let tmpvar = IELTSStudent111.filter(val => {
                                return val.name.toLowerCase() == course_title.toLowerCase()
                            });

                            let one = tmpvar[0].academic


                            courseAdminReq.academic = one;

                            resJsonData.course_admission_requirement = courseAdminReq;
                        }

                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }
                            ///progrssbar End
                            let resEnglishReqMoreDetailsJson = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.academic_requirements_url = "";
                                resJsonData.course_admission_requirement.english_requirements_url = "";
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                            }
                            break;


                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }

                        case 'course_duration_full_time': {
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = "";
                            var fullTimeText = "";
                            var durration = await utils.getValueFromHardCodedJsonFile('fees');

                            var course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            if (course_title.includes('MASTER OF EDUCATION (COURSEWORK)')) {
                                course_title = "MASTER OF EDUCATION"
                            }

                            let tmpvar = durration.filter(val => {
                                return val.name.toLowerCase() == course_title.toLowerCase()
                            });

                            fullTimeText = tmpvar[0].duration;
                            var DurationIntStudent = fullTimeText;
                            fullTimeText = DurationIntStudent
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime = resFulltime;
                                    courseDurationList = durationFullTime;
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = courseDurationList;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;

                                        var isfulltime = false, isparttime = false;

                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                        break;
                                    }
                                }
                            }

                        }

                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': {
                            var campLocationText = null;
                            const courseLocation = await utils.getValueFromHardCodedJsonFile('campuse');
                            const cricc = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            var criccos = ""
                            var cricccc = cricc.split("(CRICOS Code:")[1].trim();

                            var cricccc1 = cricccc.split(")")[0].trim();

                            criccos = cricccc1;

                            var newlocation = String(courseLocation);

                            var campusedata = [];

                            campusedata.push({
                                "name": newlocation,
                                "code": criccos
                            })


                            resJsonData.course_campus_location = campusedata;
                            //await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');

                            resJsonData.course_study_mode = 'On campus';//.join(',');

                            break;
                        }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            var courseTuitionFee = {};
                            var feesList = [];
                            var feesDict = {};
                            var filedata = await utils.getValueFromHardCodedJsonFile('fees');;

                            var FeesNew = "";
                            var title = course_name;
                            console.log("demoarray123 -->", demoarray);
                            let tmpvar = filedata.filter(val => {
                                return val.name.toLowerCase() == title.toLowerCase();
                            });

                            FeesNew = tmpvar[0].fee;

                            var feesIntStudent = FeesNew;

                        }
                            // const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);

                            if (feesIntStudent) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    if (feesVal != "NA" && feesVal != "N A") {
                                        const regEx = /\d/g;
                                        let feesValNum = feesVal.match(regEx);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            let feesNumber = null;
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }
                                            console.log(funcName + 'feesNumber = ' + feesNumber);
                                            if (Number(feesNumber)) {
                                                var feesint = {};
                                                feesint.amount = Number(feesNumber);
                                                feesint.duration = Number(demoarray.duration),
                                                    feesint.unit = demoarray.unit,

                                                    feesint.description = "$" + FeesNew;

                                                feesDict.international_student = feesint;
                                            }
                                        }
                                    }
                                    else {
                                        var feesint = {};
                                        feesint.amount = 0;
                                        feesint.duration = Number(demoarray.duration),
                                            feesint.unit = demoarray.unit,

                                            feesint.description = "$" + FeesNew;

                                    }
                                }
                            }
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                    var more_fee = [];

                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;
                        case 'program_code': {
                            let courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);

                            var program_code = "";

                            // if (program_code.length > 0) {
                            if (courseKeyVal && courseKeyVal.length > 0) {
                                var program_code = "";
                                resJsonData.program_code = program_code;
                            } else {
                                program_code = "";
                            }

                            //}
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            // Semester 1 (February) and Semester 2 (August)
                            // prepare emty JSON struct
                            const courseIntakeDisplay = {};
                            // existing intake value
                            var courseIntakeStr = [];
                            let intake_date = [
                                {
                                    key: "17 February, 2020",
                                    value: "2020-02-17"
                                },
                                {
                                    key: "1 June, 2020",
                                    value: "2020-06-01"
                                },
                                {
                                    key: "14 September, 2020",
                                    value: "2020-09-14"
                                }
                            ]
                            let course_intake = await utils.getValueFromHardCodedJsonFile('course_intake');
                            courseIntakeStr = course_intake;

                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var intakes = [];
                                var campus = resJsonData.course_campus_location;
                                for (let loc of campus) {
                                    var intakedetail = {};
                                    intakedetail.name = loc.name;
                                    intakedetail.value = courseIntakeStr;
                                    intakes.push(intakedetail);
                                }

                                let formatIntake = await format_functions.providemyintake(intakes, "MOM", "");

                                var intakedata = {};
                                var intakeUrl = [];
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                intakedata.intake = formatIntake;
                                intakedata.more_details = intakeUrl;


                                resJsonData.course_intake = intakedata;

                            }
                            break;
                        }


                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }

                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r careerselList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                // course_career_outcome = selList;
                                                for (let selItem of selList) {

                                                    selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ');
                                                    course_career_outcome.push(selItem.trim())
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }
                        // case 'course_study_level':
                        case 'course_study_level':
                            {
                                const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code)

                                if (cTitle.includes('Graduate Certificate of Business Administration')) {

                                    resJsonData.course_study_level = "Graduate Certificate"
                                    break;
                                }
                                var finalTitle = cTitle.split("(CRICOS Code:")[1].trim();
                                var finalTitle2 = finalTitle.split(")")[0].trim();

                                let cStudyLevel = await format_functions.getMyStudyLevel(finalTitle2);

                                resJsonData.course_study_level = cStudyLevel
                            }
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {


                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();

                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        // NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                //resJsonData.current_course_intake = location_wise_data.course_intake;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
                // const res = await awsUtil.putFileInBucket(filelocation);
                // console.log(funcName + 'S3 object location = ' + res);
            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
