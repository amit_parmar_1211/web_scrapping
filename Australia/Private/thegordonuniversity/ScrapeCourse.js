const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_discipline) {
        console.log("URL@@@", JSON.stringify(courseUrl))
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = course_discipline
                            break;
                        }

                        case 'course_title': {
                            //const title=courseScrappedData.course_title
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            var splitdata = courseKeyVal.split(" ")
                            var code = splitdata[0];
                            console.log("Programcode---->", JSON.stringify(courseKeyVal.split(code)))
                            var data = courseKeyVal.split(code)
                            var title = data[1].trim()
                            console.log("Title@@@@@", JSON.stringify(title))
                            resJsonData.course_title = title


                            break;
                        }
                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}, ibtDisc = {}, pteDict = {}, caeDict = {};
                            let ieltsNumber = null;
                            const penglishList = []; const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            let cTitle = resJsonData.course_title
                            if (cTitle == "Diploma of Hospitality Management") {
                                const ieltsScore = "IELTS Academic overall score of 5.5 with no band below 5.0 (or equivalent); or completion of Australian Year 12"
                                let myscore = await utils.getMappingScore(await utils.giveMeNumber(ieltsScore.replace(/ /g, ' ')));
                                let ibt_req, pte_req, cae_req
                                const pteDict = {}; const tofelDict = {}; const caeDict = {};
                                if (myscore.ibt && myscore.ibt.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.ibt).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ibt_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'ibtNumber = ' + ibt_req);
                                    }
                                }
                                if (myscore.pte && myscore.pte.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.pte).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pte_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'pteNumber = ' + ibt_req);
                                    }
                                }
                                if (myscore.cae && myscore.cae.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.cae).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        cae_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'caeNumber = ' + ibt_req);
                                    }
                                }
                                if (ieltsScore) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsScore;
                                    ieltsDict.min = 0;
                                    ieltsDict.require = await utils.giveMeNumber(ieltsScore.replace(/ /g, ' '));;
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);
                                }
                                if (ibt_req) {
                                    tofelDict.name = 'toefl ibt';
                                    tofelDict.description = myscore.ibt;
                                    tofelDict.min = 0;
                                    tofelDict.require = ibt_req;
                                    tofelDict.max = 120;
                                    tofelDict.R = 0;
                                    tofelDict.W = 0;
                                    tofelDict.S = 0;
                                    tofelDict.L = 0;
                                    tofelDict.O = 0;
                                    englishList.push(tofelDict);
                                }

                                if (pte_req) {
                                    pteDict.name = 'pte academic';
                                    pteDict.description = myscore.pte;
                                    pteDict.min = 0;
                                    pteDict.require = pte_req;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);
                                }
                                if (cae_req) {
                                    caeDict.name = 'cae';
                                    caeDict.description = myscore.cae;
                                    caeDict.min = 80;
                                    caeDict.require = cae_req;
                                    caeDict.max = 230;
                                    caeDict.R = 0;
                                    caeDict.W = 0;
                                    caeDict.S = 0;
                                    caeDict.L = 0;
                                    caeDict.O = 0;
                                    englishList.push(caeDict);
                                }
                              
                            } else if (cTitle == "Diploma of Nursing") {
                                const ieltsScore = "IELTS Academic overall score of 7  with no band below 6.5 (or equivalent)"

                                let myscore = await utils.getMappingScore(await utils.giveMeNumber(ieltsScore.replace(/ /g, ' ')));
                                let ibt_req, pte_req, cae_req
                                const pteDict = {}; const tofelDict = {}; const caeDict = {};
                                if (myscore.ibt && myscore.ibt.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.ibt).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ibt_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'ibtNumber = ' + ibt_req);
                                    }
                                }
                                if (myscore.pte && myscore.pte.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.pte).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pte_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'pteNumber = ' + ibt_req);
                                    }
                                }
                                if (myscore.cae && myscore.cae.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.cae).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        cae_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'caeNumber = ' + ibt_req);
                                    }
                                }
                                if (ieltsScore) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsScore;
                                    ieltsDict.min = 0;
                                    ieltsDict.require = await utils.giveMeNumber(ieltsScore.replace(/ /g, ' '));;
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);
                                }
                                if (ibt_req) {
                                    tofelDict.name = 'toefl ibt';
                                    tofelDict.description = myscore.ibt;
                                    tofelDict.min = 0;
                                    tofelDict.require = ibt_req;
                                    tofelDict.max = 120;
                                    tofelDict.R = 0;
                                    tofelDict.W = 0;
                                    tofelDict.S = 0;
                                    tofelDict.L = 0;
                                    tofelDict.O = 0;
                                    englishList.push(tofelDict);
                                }

                                if (pte_req) {
                                    pteDict.name = 'pte academic';
                                    pteDict.description = myscore.pte;
                                    pteDict.min = 0;
                                    pteDict.require = pte_req;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);
                                }
                                if (cae_req) {
                                    caeDict.name = 'cae';
                                    caeDict.description = myscore.cae;
                                    caeDict.min = 80;
                                    caeDict.require = cae_req;
                                    caeDict.max = 230;
                                    caeDict.R = 0;
                                    caeDict.W = 0;
                                    caeDict.S = 0;
                                    caeDict.L = 0;
                                    caeDict.O = 0;
                                    englishList.push(caeDict);
                                }
                            }
                            else {
                                if (courseScrappedData.course_toefl_ielts_score) {
                                    console.log("toefl score--->" + JSON.stringify(courseScrappedData.course_toefl_ielts_score))
                                    const ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                                    let myscore = await utils.getMappingScore(await utils.giveMeNumber(ieltsScore.replace(/ /g, ' ')));
                                    console.log("myscore : ", myscore);
                                    let ibt_req, pte_req, cae_req
                                    const pteDict = {}; const tofelDict = {}; const caeDict = {};
                                    if (myscore.ibt && myscore.ibt.length > 0) {
                                        // extract exact number from string
                                        const regEx = /[+]?\d+(\.\d+)?/g;
                                        const matchedStrList = String(myscore.ibt).match(regEx);
                                        console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                        if (matchedStrList && matchedStrList.length > 0) {
                                            ibt_req = Number(matchedStrList[0]);
                                            console.log(funcName + 'ibtNumber = ' + ibt_req);
                                        }
                                    }
                                    if (myscore.pte && myscore.pte.length > 0) {
                                        // extract exact number from string
                                        const regEx = /[+]?\d+(\.\d+)?/g;
                                        const matchedStrList = String(myscore.pte).match(regEx);
                                        console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                        if (matchedStrList && matchedStrList.length > 0) {
                                            pte_req = Number(matchedStrList[0]);
                                            console.log(funcName + 'pteNumber = ' + ibt_req);
                                        }
                                    }
                                    if (myscore.cae && myscore.cae.length > 0) {
                                        // extract exact number from string
                                        const regEx = /[+]?\d+(\.\d+)?/g;
                                        const matchedStrList = String(myscore.cae).match(regEx);
                                        console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                        if (matchedStrList && matchedStrList.length > 0) {
                                            cae_req = Number(matchedStrList[0]);
                                            console.log(funcName + 'caeNumber = ' + ibt_req);
                                        }
                                    }
                                    if (ieltsScore && ieltsScore.length > 0) {
                                        const ieltsDict = {};

                                        // var ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_req);
                                        if (ieltsScore) {
                                            ieltsDict.name = 'ielts academic';
                                            ieltsDict.description = ieltsScore;
                                            ieltsDict.min = 0;
                                            ieltsDict.require = await utils.giveMeNumber(ieltsScore.replace(/ /g, ' '));;
                                            ieltsDict.max = 9;
                                            ieltsDict.R = 0;
                                            ieltsDict.W = 0;
                                            ieltsDict.S = 0;
                                            ieltsDict.L = 0;
                                            ieltsDict.O = 0;
                                            englishList.push(ieltsDict);
                                        }
                                    }

                                    if (ibt_req) {
                                        tofelDict.name = 'toefl ibt';
                                        tofelDict.description = myscore.ibt;
                                        tofelDict.min = 0;
                                        tofelDict.require = ibt_req;
                                        tofelDict.max = 120;
                                        tofelDict.R = 0;
                                        tofelDict.W = 0;
                                        tofelDict.S = 0;
                                        tofelDict.L = 0;
                                        tofelDict.O = 0;
                                        englishList.push(tofelDict);
                                    }

                                    if (pte_req) {
                                        pteDict.name = 'pte academic';
                                        pteDict.description = myscore.pte;
                                        pteDict.min = 0;
                                        pteDict.require = pte_req;
                                        pteDict.max = 90;
                                        pteDict.R = 0;
                                        pteDict.W = 0;
                                        pteDict.S = 0;
                                        pteDict.L = 0;
                                        pteDict.O = 0;
                                        englishList.push(pteDict);
                                    }
                                    if (cae_req) {
                                        caeDict.name = 'cae';
                                        caeDict.description = myscore.cae;
                                        caeDict.min = 80;
                                        caeDict.require = cae_req;
                                        caeDict.max = 230;
                                        caeDict.R = 0;
                                        caeDict.W = 0;
                                        caeDict.S = 0;
                                        caeDict.L = 0;
                                        caeDict.O = 0;
                                        englishList.push(caeDict);
                                    }
                                }
                            }


                            ///progrssbar End
                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            } else {
                                courseAdminReq.english = []
                            }

                            //var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var english_req = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            var academic_req = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirement_url);

                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            // console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            academicReq = selList;
                                            //}
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];
                            console.log("Academic requirement" + JSON.stringify(courseAdminReq.academic));
                            courseAdminReq.entry_requirements_url = english_more;
                            courseAdminReq.english_requirements_url = english_req;
                            courseAdminReq.academic_requirements_url = academic_req;
                            //courseAdminReq.academic_more_details = academic_more;
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                                console.log("##fullTimeText--->" + JSON.stringify(fullTimeText));
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        // case 'course_duration_full_time': {
                        //     // display full time
                        //     var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                        //     var fullTimeText = "";
                        //     const courseKeyVal = courseScrappedData.course_duration_full_time;
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                     if (Array.isArray(selList) && selList.length > 0) {
                        //                         fullTimeText = selList[0];
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     if (fullTimeText && fullTimeText.length > 0) {
                        //         const resFulltime = fullTimeText;
                        //         if (resFulltime) {
                        //             durationFullTime.duration_full_time = resFulltime;
                        //             courseDurationList.push(durationFullTime);
                        //             courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                        //             console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                        //             let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                        //             console.log("filtered_duration_formated", filtered_duration_formated);
                        //             if (courseDurationList && courseDurationList.length > 0) {
                        //                 resJsonData.course_duration = courseDurationList;
                        //             }
                        //             if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                        //                 resJsonData.course_duration_display = filtered_duration_formated;
                        //                 var isfulltime = false, isparttime = false;
                        //                 filtered_duration_formated.forEach(element => {
                        //                     if (element.display == "Full-Time") {
                        //                         isfulltime = true;
                        //                     }
                        //                     if (element.display == "Part-Time") {
                        //                         isparttime = true;
                        //                     }
                        //                 });
                        //                 resJsonData.isfulltime = isfulltime;
                        //                 resJsonData.isparttime = isparttime;

                        //         }


                        //         }
                        //     }
                        //     break;
                        // }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston

                            const courseKeyVals = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;
                                            }
                                        }
                                    }
                                }
                            }


                            var campLocationText = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            let flocations = []
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            console.log("##Campus-->" + campLocationText)
                            if (campLocationText && campLocationText.length > 0) {
                                console.log("&&&location" + String(campLocationText).includes('&'));

                                if (String(campLocationText).includes('|')) {
                                    var nlocation = [];
                                    var finallocation = [];
                                    // let flocations=[]
                                    var mlocation = await utils.giveMeArray(String(campLocationText).replace(/[\r\n\t]+/g, '|'), '|');
                                    console.log("##Campus-->" + JSON.stringify(mlocation));
                                    for (var i = 0; i < mlocation.length; i++) {
                                        var newlocation = String(mlocation[i]).split(" Campus:");
                                        nlocation.push(newlocation);
                                        console.log("##Campuss-->" + JSON.stringify(newlocation));
                                    }
                                    for (var j = 0; j < nlocation.length; j++) {
                                        finallocation.push(nlocation[j][0].replace(/[\r\n\t\s ]+/g, ''));
                                        console.log("#Campus##-->" + JSON.stringify(finallocation));
                                    }

                                    var locations = ["Geelong", "Werribee"];
                                    for (let i = 0; i < locations.length; i++) {
                                        finallocation.forEach(element => {
                                            if (element.includes(locations[i])) {
                                                flocations.push(locations[i])
                                            }
                                        })
                                    }

                                    var campusedata = [];
                                    flocations.forEach(element => {
                                        campusedata.push({
                                            "name": element.trim(),
                                            "code": String(course_cricos_code)
                                        })
                                    });

                                    var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                    //resJsonData.course_campus_location = await utils.giveMeArray(String( finallocation).replace(/[\r\n\t ]+/g, ' '));
                                    resJsonData.course_campus_location = campusedata;

                                }
                                else if (String(campLocationText).includes('&')) {
                                    var nlocation = [];
                                    var finallocation = [];
                                    var mlocation = await utils.giveMeArray(String(campLocationText).replace(/[\r\n\t]+/g, '&'), '&');
                                    console.log("##Campus-->" + JSON.stringify(mlocation));
                                    for (var i = 0; i < mlocation.length; i++) {
                                        var newlocation = String(mlocation[i]).split(" Campus:");
                                        nlocation.push(newlocation);
                                        console.log("##Campuss-->" + JSON.stringify(newlocation));
                                    }
                                    for (var j = 0; j < nlocation.length; j++) {
                                        finallocation.push(nlocation[j][0].replace(/[\r\n\t\s ]+/g, ''));
                                        console.log("#Campus##-->" + JSON.stringify(finallocation));
                                    }
                                    var locations = ["Geelong", " Werribee"];
                                    for (let i = 0; i < locations.length; i++) {
                                        finallocation.forEach(element => {
                                            if (element.includes(locations[i])) {
                                                flocations.push(locations[i])
                                            }
                                        })
                                    }
                                    var campusedata = [];
                                    flocations.forEach(element => {
                                        campusedata.push({
                                            "name": element.trim(),
                                            "code": String(course_cricos_code)
                                        })
                                    });
                                    var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                    //resJsonData.course_campus_location = await utils.giveMeArray(String( finallocation).replace(/[\r\n\t ]+/g, ' '));
                                    resJsonData.course_campus_location = campusedata;
                                }
                                else {
                                    var location = String(campLocationText).split(" Campus:");
                                    var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                    //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                    // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                    var loc = await utils.giveMeArray(String(location[0]).replace(/[\r\n\t ]+/g, ' '));
                                    console.log("LOcation===>", JSON.stringify(loc))
                                    var locations = ["Geelong", " Werribee"];
                                    for (let i = 0; i < locations.length; i++) {
                                        loc.forEach(element => {
                                            if (element.includes(locations[i])) {
                                                flocations.push(locations[i])
                                            }
                                        })
                                    }
                                    var campusedata = [];
                                    flocations.forEach(element => {
                                        campusedata.push({
                                            "name": element.trim(),
                                            "code": String(course_cricos_code)
                                        })
                                    });
                                    //resJsonData.course_campus_location = 
                                    resJsonData.course_campus_location = campusedata
                                    console.log("Location!!!" + resJsonData.course_campus_location);
                                }
                                resJsonData.course_study_mode = "On campus";//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);

                            }
                            break;
                        }

                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};

                            var fee_desc = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal_fee = courseScrappedData.course_tuition_fees_international_student;
                            console.log("courseKeyVal_fee!!!" + JSON.stringify(courseKeyVal_fee));
                            if (!courseKeyVal_fee) {
                                fee_desc = "NA";
                            }
                            if (Array.isArray(courseKeyVal_fee)) {
                                for (const rootEleDict of courseKeyVal_fee) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            fee_desc = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            console.log("Fees_desc!!!" + JSON.stringify(fee_desc));
                            var fee_desc_more = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal_more = courseScrappedData.course_tuition_fees_international_student_more;
                            if (!courseKeyVal_more) {
                                feesDict.international_student_all_fees = []
                            }
                            console.log("courseKeyVal_more::" + JSON.stringify(fee_desc_more));
                            if (Array.isArray(courseKeyVal_more)) {
                                for (const rootEleDict of courseKeyVal_more) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            fee_desc_more = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            console.log("courseKeyVal+++++::" + JSON.stringify(fee_desc_more));

                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            // if (feeYear && feeYear.length > 0) {
                            //     courseTuitionFee.year = feeYear;
                            // }

                            const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                            const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                            if (feesDuration && feesDuration.length > 0) {
                                feesDict.fee_duration_years = feesDuration;
                            }
                            if (feesCurrency && feesCurrency.length > 0) {
                                feesDict.currency = feesCurrency;
                            }
                            var campus = resJsonData.course_campus_location;
                            var extracost = "Materials Fee:"
                            if (fee_desc_more == "" || fee_desc_more == null || fee_desc_more != []) {

                                console.log("INif 1")
                                for (let loc of campus) {
                                    // feesDict.international_student_all_fees = []
                                    var fee = fee_desc[0].split("$");

                                    var feess = await utils.giveMeNumber(String(fee).replace(/[\r\n\t ]+/g, ' '));
                                    console.log("feesssss", JSON.stringify(feess))
                                    feesDict.international_student = {

                                        amount: feess,
                                        duration: 1,
                                        unit: "Year",
                                        description: fee_desc[0],

                                    };
                                    feesList.push({ name: loc.name, value: feesDict });
                                }
                            } else {
                                var ecost = extracost.concat(fee_desc_more);
                                if (fee_desc && fee_desc.length > 0) {
                                    console.log("INif 2")
                                    var fee = fee_desc[0].split("$");

                                    var feess = await utils.giveMeNumber(String(fee).replace(/[\r\n\t ]+/g, ' '));
                                    console.log("feesssss", JSON.stringify(feess))
                                    feesDict.international_student = {
                                        // amount: fee.fees,
                                        amount: feess,
                                        duration: 1,
                                        unit: "Year",
                                        description: fee_desc[0] + '' + extracost + ecost.replace(/[\r\n\t ]+/g, ' '),

                                    };

                                } else {
                                    feesDict.international_student = {
                                        // amount: fee.fees,
                                        amount: 0,
                                        duration: 1,
                                        unit: "Year",

                                        description: "",

                                    };

                                }
                                for (let loc of campus) {
                                    feesList.push({ name: loc.name, value: feesDict });
                                }
                            }

                            if (feesList && feesList.length > 0) {
                                courseTuitionFee.fees = feesList;
                            }
                            console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                            if (courseTuitionFee) {
                                resJsonData.course_tuition_fee = courseTuitionFee;
                            }
                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            var program_code = []
                            console.log("PROGramCode--->", JSON.stringify(courseKeyVal))
                            var splitdata = courseKeyVal.split(" ")
                            var code = splitdata[0];
                            program_code.push(code)
                            console.log("PROGramCode--->", JSON.stringify(program_code))

                            resJsonData.program_code = String(program_code);
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            // Semester 1 (February) and Semester 2 (August)
                            // prepare emty JSON struct
                            const courseIntakeDisplay = {};
                            let formatIntake
                            // existing intake value
                            var courseIntakeStr = null;//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;

                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                courseIntakeStr = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (!courseIntakeStr) {
                                courseIntakeStr = [];
                            }
                            console.log("CourseIntakeStr" + JSON.stringify(courseIntakeStr));
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                console.log("##intakeDate@@@" + String(courseIntakeStr).includes("|"));
                                if (String(courseIntakeStr).includes("|")) {
                                    var nintakedata = [];
                                    var finalintake = [];
                                    var mlocation = await utils.giveMeArray(String(courseIntakeStr).replace(/[\r\n\t]+/g, '|'), '|');
                                    console.log("##courseIntakeStr-->" + JSON.stringify(mlocation));
                                    for (var i = 0; i < mlocation.length; i++) {
                                        var newintakedata = String(mlocation[i]).split(" Campus:");
                                        nintakedata.push(newintakedata);
                                        console.log("##Campuss-->" + JSON.stringify(nintakedata));
                                    }
                                    console.log("##intakeDate@@@-->" + JSON.stringify(nintakedata));
                                    console.log("###intakeDate@@@-->" + (nintakedata.length));
                                    for (var j = 0; j < nintakedata.length; j++) {
                                        finalintake.push(nlocation[j][1].replace(/[\r\n\t\s ]+/g, ''));
                                        console.log("#Campus##-->" + JSON.stringify(finalintake[0]));
                                    }
                                    var campus = resJsonData.course_campus_location;
                                    var intakes = [];
                                    console.log("Location@@@" + campus.length);
                                    var myintake_data = JSON.parse(fs.readFileSync("./selectors/university_hard_coded_data.json"))[0];
                                    var intakedetail = {};
                                    var myintake = [];
                                    //var newintakes=await utils.giveMeArray(String(finalintake[0]).replace(/[\r\n\t]+/g, '&'), '&');
                                    var newintakes = finalintake[0].replace(/[\r\n\t\s ]+/g, '');
                                    console.log("NewIntakes[0]" + newintakes);
                                    console.log("NewIntakes[0].length" + newintakes.length);

                                    if (newintakes.indexOf("February") > -1) {
                                        console.log("Match--> s1")
                                        myintake.push(myintake_data["term1"]);
                                    }
                                    if (newintakes.indexOf("July") > -1) {
                                        console.log("Match--> s1")
                                        myintake.push(myintake_data["term2"]);
                                    }
                                    for (var camcount = 0; camcount < campus.length; camcount++) {

                                        // console.log("intake -->" + count)
                                        var intakedetail = {};
                                        //intakedetail.campus = campus[count];
                                        intakedetail.name = campus[camcount].name;
                                        intakedetail.value = myintake;

                                        intakes.push(intakedetail);


                                    }
                                } else if (String(courseIntakeStr).includes("&")) {
                                    var nintakedata = [];
                                    var finalintake = [];
                                    var mlocation = await utils.giveMeArray(String(courseIntakeStr).replace(/[\r\n\t]+/g, '&'), '&');
                                    console.log("##courseIntakeStr-->" + JSON.stringify(mlocation));
                                    for (var i = 0; i < mlocation.length; i++) {
                                        var newintakedata = String(mlocation[i]).split(":");
                                        nintakedata.push(newintakedata);
                                        console.log("##Campuss-->" + JSON.stringify(nintakedata));
                                    }
                                    console.log("##intakeDate@@@-->" + JSON.stringify(nintakedata));
                                    console.log("###intakeDate@@@-->" + (nintakedata.length));
                                    for (var j = 0; j < nintakedata.length; j++) {
                                        finalintake.push(String(nlocation[j][1]).replace(/[\r\n\t\s ]+/g, ''));
                                        console.log("#Campus##-->" + JSON.stringify(finalintake[1]));
                                    }
                                    var campus = resJsonData.course_campus_location;
                                    var intakes = [];
                                    console.log("Location@@@" + campus.length);
                                    var myintake_data = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                                    var intakedetail = {};
                                    var myintake = [];
                                    //var newintakes=await utils.giveMeArray(String(finalintake[0]).replace(/[\r\n\t]+/g, '&'), '&');
                                    var newintakes = finalintake[1].replace(/[\r\n\t\s ]+/g, '');
                                    console.log("NewIntakes[0]" + newintakes);
                                    console.log("NewIntakes[0].length" + newintakes.length);

                                    if (newintakes.indexOf("February") > -1) {
                                        console.log("Match--> s1")
                                        myintake.push(myintake_data["term1"]);
                                    }
                                    if (newintakes.indexOf("July") > -1) {
                                        console.log("Match--> s1")
                                        myintake.push(myintake_data["term2"]);
                                    }
                                    for (var camcount = 0; camcount < campus.length; camcount++) {

                                        // console.log("intake -->" + count)
                                        var intakedetail = {};
                                        //intakedetail.campus = campus[count];
                                        intakedetail.name = campus[camcount].name;
                                        intakedetail.value = myintake;

                                        intakes.push(intakedetail);
                                        console.log("&intakeData", JSON.stringify(intakes))


                                    }
                                }


                                else {
                                    var newintake = String(courseIntakeStr).split(": ");

                                    var newintakes = await utils.giveMeArray(String(newintake[1]).replace(/[\r\n\t()]+/g, ''), " and ");
                                    console.log("intakeDate@@@" + newintakes);
                                    var intakes = [];
                                    var campus = resJsonData.course_campus_location;

                                    console.log("Location@@@" + JSON.stringify(campus));
                                    var myintake_data = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
                                    var intakedetail = {};
                                    var myintake = [];
                                    if (newintakes[0].length > 1) {
                                        var data;
                                        console.log("2ndIntake", JSON.stringify(newintakes[0]))
                                        data = newintakes[0].split(",")
                                        console.log("DATA@@@", JSON.stringify(data))
                                        var finaldata = data.concat(newintakes[1])
                                        let arr = []
                                        finaldata.forEach(element => {
                                            arr.push(element)
                                        })
                                        console.log("FINALDATA@@@@", JSON.stringify(String(arr).includes("July")))
                                        if (arr.includes("February")) {
                                            console.log("Match--> s1")
                                            myintake.push(myintake_data["term1"]);

                                        }
                                        if (arr.includes("July")) {
                                            console.log("Match--> s1")
                                            myintake.push(myintake_data["term2"]);
                                        }



                                    } else {
                                        if (newintakes.indexOf("February") > -1) {
                                            console.log("Match--> s1")
                                            myintake.push(myintake_data["term1"]);
                                        }
                                        if (newintakes.indexOf("July") > -1) {
                                            console.log("Match--> s1")
                                            myintake.push(myintake_data["term2"]);
                                        }
                                    }

                                    console.log("myintake@@@" + JSON.stringify(myintake));
                                    console.log("Location=====>", JSON.stringify(campus))
                                    //myintake = newintake[1];
                                    intakedetail.name = campus[0].name;
                                    intakedetail.value = myintake;
                                    intakes.push(intakedetail);
                                }

                                console.log("INtake===>", JSON.stringify(intakes))
                                formatIntake = await format_functions.providemyintake(intakes, "mom", "");
                                console.log("FormatIntake", JSON.stringify(formatIntake))
                                var intakedata = {};
                                intakedata.intake = formatIntake;
                                intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
                                console.log("Location@@@" + JSON.stringify(intakedetail));

                            } else {
                                var intakedetail = {};
                                var intakes = [];
                                intakedetail.name = "";
                                intakedetail.value = [];
                                intakes.push(intakedetail);
                                var intakedata = {};
                                intakedata.intake = intakes;
                                intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                resJsonData.course_intake = intakedata;
                            }
                            break;
                        }

                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                            const courseKeyVal_major = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline);
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                            let course_outlines = {};
                            if (courseKeyVal_minor != null) {
                                course_outlines.minors = courseKeyVal_minor
                            } else {
                                course_outlines.minors = []
                            }
                            if (courseKeyVal_major != null) {
                                course_outlines.majors = courseKeyVal_major
                            } else {
                                course_outlines.majors = []
                            }
                            if (courseKeyVal != null) {
                                course_outlines.more_details = courseKeyVal
                            } else {
                                course_outlines.more_details = ""
                            }
                            resJsonData.course_outline = course_outlines
                            break;
                        }
                        case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            if (courseKeyVal != null) {
                                resJsonData.application_fees = courseKeyVal
                            } else {
                                resJsonData.application_fees = ""
                            }
                        }



                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            //const courseKeyVal = courseScrappedData.course_overview;
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            console.log("overview---" + JSON.stringify(courseKeyVal));
                            let arr = [];
                            let resAccomodationCostJson = null;
                            if (courseKeyVal == null) {
                                console.log("NOT@@");
                                arr.push("NA");
                                resAccomodationCostJson = [];

                            }
                            if (courseKeyVal) {
                                console.log("@@@");
                                arr.push(courseKeyVal);
                                resAccomodationCostJson = courseKeyVal;

                            }

                            //     if (Array.isArray(courseKeyVal)) {
                            //         
                            //         console.log("@@@@ass"+JSON.stringify(courseKeyVal));
                            //       for (const rootEleDict of courseKeyVal) {
                            //         console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                            //         const elementsList = rootEleDict.elements;
                            //         for (const eleDict of elementsList) {
                            //           console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                            //           const selectorsList = eleDict.selectors;
                            //           for (const selList of selectorsList) {
                            //             console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                            //             if (Array.isArray(selList) && selList.length > 0) {
                            //               resAccomodationCostJson = selList;
                            //               resJsonData.course_overview = resAccomodationCostJson;
                            //             }
                            //           }
                            //         }
                            //       }
                            //     }
                            // 
                            if (resAccomodationCostJson) {
                                resJsonData.course_overview = resAccomodationCostJson;
                            }
                            console.log("overview---" + resJsonData.course_overview)
                            break;
                        }

                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }

                        case 'course_study_level': {
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log("CRICOSE@@@@@@", cTitle)
                            const studylevel = await format_functions.getMyStudyLevel(cTitle)
                            console.log("Study_level", studylevel)
                            resJsonData.course_study_level = studylevel
                            //const cricosecode =resJsonData.course_cricos_code.code


                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            // console.log('\n\r' + funcName + 'course resJsonData =' + JSON.stringify(resJsonData));
            // var resJsonData = JSON.parse(fs.readFileSync("australiancatholicuniversity_graduatediplomainrehabilitation_coursedetails.json"))[0];
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                var basecourseid = location_wise_data.course_id;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid.replace(/[\n :""]+/g, '').replace(/[\/]+/g, '_') + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };