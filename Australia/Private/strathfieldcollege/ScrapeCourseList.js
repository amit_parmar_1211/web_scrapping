const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
const puppeteer = require('puppeteer');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course

      var datalist = [];

      var mainCategory = [], redirecturl = [];
      //scrape main category
      const maincategoryselectorurl = "//h6/parent::div/following-sibling::div[2]/a";
      const maincategoryselector = "//h6/span/span";
      var category = await page.$x(maincategoryselector);
      var categoryurl = await page.$x(maincategoryselectorurl);
      console.log("Total categories-->" + category.length);
      for (let i = 0; i < category.length; i++) {
        var categorystringmain = await page.evaluate(el => el.innerText, category[i]);
        var categorystringmainurl = await page.evaluate(el => el.href, categoryurl[i]);
        mainCategory.push(categorystringmain.trim());
        redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
      }
      for (let catc of redirecturl) {
        await page.goto(catc.href);
        await page.waitFor(7000);

        let iframe_val = "//*//div[(contains(@class,'style-k54v0pb7overlay')) or (contains(@class,'style-k57olrr6overlay')) or (contains(@class,'style-k5bxsq3joverlay'))or (contains(@class,'style-k5bvq3dboverlay'))or (contains(@class,'style-k9t62nixoverlay'))or (contains(@class,'style-k72rvivsoverlay'))]/../iframe[@src]"
        var iframe_val11 = await page.$x(iframe_val);
        var iframe_val22 = await page.evaluate(el => el.src, iframe_val11[0]);
        await page.goto(iframe_val22);
        await page.waitFor(7000);
        console.log("categoryurl------->>>", iframe_val22);

        var courese11 = await page.$x("(//*//th[contains(text(),'Location')])/../../following-sibling::tbody[1]/tr/td[1]");
        var program_code11 = await page.$x("(//*//th[contains(text(),'Location')])/../../following-sibling::tbody[1]/tr/td[2]");
        var duration11 = await page.$x("(//*//th[contains(text(),'Location')])/../../following-sibling::tbody[1]/tr/td[3]");
        var ielts11 = await page.$x("(//*//th[contains(text(),'Location')])/../../following-sibling::tbody[1]/tr/td[4]");
        var Location11 = await page.$x("(//*//th[contains(text(),'Location')])/../../following-sibling::tbody[1]/tr/td[5]");

        for (let t = 0; t < courese11.length; t++) {

          var courese22 = await page.evaluate(el => el.textContent, courese11[t]);
          var program_code22 = await page.evaluate(el => el.textContent, program_code11[t]);
          var duration22 = await page.evaluate(el => el.textContent, duration11[t]);
          var ielts22 = await page.evaluate(el => el.textContent, ielts11[t]);
          var Location22 = await page.evaluate(el => el.textContent, Location11[t]);
          if (program_code22.includes('|')) {
            let program_code33 = program_code22.split('|')
            let pro_code = program_code33[0]
            let cricose11 = program_code33[1]
            datalist.push({
              href: catc.href,
              innerText: courese22,
              program_code: pro_code.trim(),
              cricose: cricose11.trim(),
              duration: duration22,
              ielts: ielts22,
              Location: Location22,
              category: catc.innerText
            });
          }


        }
      }

      fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(mainCategory))

      await fs.writeFileSync("./output/strathfieldcollege_courselist.json", JSON.stringify(datalist));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async scrapeintakesfess() {
    const URL = "https://www.strathfieldcollege.edu.au/programs-fees";
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    var subjectAreasArray = [];

    let iframe_valqq = "//*//div[(contains(@id,'comp-k47pj63tinlineContent-gridContainer'))]/..//wix-iframe[1]/iframe[@src]"
    var iframe_val111 = await page.$x(iframe_valqq);
    var iframe_val222 = await page.evaluate(el => el.src, iframe_val111[0]);
    await page.goto(iframe_val222);
    await page.waitFor(7000);
    console.log("fesssss------->>>", iframe_val222);

    let Course_Name = "(//*//table[@class='footable table outerBorder footable-loaded']//th[contains(.,'	Tuition Fee (AUD)')])/../../following-sibling::tbody//td[1]";
    let Tuition_Fee = "(//*//table[@class='footable table outerBorder footable-loaded']//th[contains(.,'	Tuition Fee (AUD)')])/../../following-sibling::tbody//td[4]";
    const Course_Name11 = await page.$x(Course_Name);
    const Tuition_Fee11 = await page.$x(Tuition_Fee);
    for (let i = 0; i < Course_Name11.length; i++) {
      let Course_Name22 = await page.evaluate(el => el.textContent, Course_Name11[i]);
      let Tuition_Fee22 = await page.evaluate(el => el.textContent, Tuition_Fee11[i]);
      subjectAreasArray.push({
        Course_Name: Course_Name22,
        Tuition_Fee: Tuition_Fee22,
      });
    }
    await fs.writeFileSync("./output/strathfieldcollege_fees.json", JSON.stringify(subjectAreasArray));
    console.log("name-->", JSON.stringify(subjectAreasArray));
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      await this.scrapeintakesfess();
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }


  // refers to https://programsandcourses.anu.edu.au/catalogue
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }


      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
