const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = course_category;
                            // As of not we do not have way to get this field value
                            break;
                        }
                        case 'course_title_category':
                        case 'course_title': {
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            // .replace('\n', '')
                            let title11 = title.split(' ');
                            console.log("splitStr@@@2" + title11);
                            var ctitle = format_functions.titleCase(title).trim();
                            // var ctitle2 = ctitle.replace(' ( ', '(');
                            console.log("ctitle@@@", ctitle.trim());
                            resJsonData.course_title = ctitle
                            break;
                        }
                        case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            let ieltsNumber = null, pteNumber = null, ibtNumber = null, caeNumber = null;
                            let myscore = '';
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {};
                            console.log("resJsonData.course_toefl_ielts_score -->", JSON.stringify(courseScrappedData.course_toefl_ielts_score));
                            let ieltsString = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);

                            if (ieltsString && ieltsString.length > 0) {
                                // extract exact number from string
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                const matchedStrList = String(ieltsString).match(regEx);
                                console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    ieltsNumber = Number(matchedStrList[0]);
                                    console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                }
                            }


                            if (ieltsNumber) {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ieltsString;
                                englishList.push(ieltsDict);
                            }

                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            var penglishList = [];
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";

                            if (ieltsNumber) {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = ieltsString;
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsNumber;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }

                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            } else {
                                throw new Error("english not found");
                            }

                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            var academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            // console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            academicReq = selList;
                                            //}
                                        }
                                    }
                                }
                            }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            // courseAdminReq.academic = (academicReq) ? academicReq.toString() : "";
                            courseAdminReq.entry_requirements_url = "";
                            courseAdminReq.academic_requirements_url = academic_requirements_url;
                            courseAdminReq.english_requirements_url = english_more;
                            //courseAdminReq.academic_more_details = academic_more;
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));

                                            rescourse_url = selList;

                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;

                        }

                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selListDuration = ' + JSON.stringify(selList));
                                            for (let sel of selList) {
                                                sel = sel.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s\s)/g, '');
                                                // console.log("Sel Before -->>", sel);
                                                // console.log("Sel 1111 -->>", sel.trim());
                                                fullTimeText = sel.trim();
                                            }
                                            // fullTimeText = selList[0];

                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    // durationFullTime.duration_full_time = resFulltime;
                                    // courseDurationList.push(durationFullTime);
                                    // demoarray = tempvar[0];
                                    // courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));

                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));


                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    // if (courseDurationList && courseDurationList.length > 0) {
                                    resJsonData.course_duration = resFulltime;
                                    //  }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston
                            var campLocationText = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            // campLocationText = selList;
                                            ///}

                                            for (let selItem of selList) {
                                                selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s)/g, ',').split(",");
                                                console.log("selItemCampus -->", selItem);
                                                for (let sel of selItem) {
                                                    sel = sel.trim();
                                                    campLocationText.push(sel);
                                                }

                                            } // selList
                                        }
                                    }
                                }
                            }
                            let newcampus = [];
                            for (let campus of campLocationText) {
                                if (!newcampus.includes(campus)) {
                                    newcampus.push(campus);
                                    console.log("##Campuscampus-->" + campus)
                                }
                            }
                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            console.log("##Campus-->" + campLocationText)



                            const courseKeyVal111 = courseScrappedData.course_cricos_code;
                            let course_cricos_code = [];
                            const mycodes = [];
                            if (Array.isArray(courseKeyVal111)) {
                                for (const rootEleDict of courseKeyVal111) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            //     course_cricos_code = selList;
                                            // }
                                            for (let selItem of selList) {
                                                let splitCricosCode = selItem.split('|');
                                                for (let sel of splitCricosCode) {
                                                    if (sel.toLowerCase().includes('cricos code')) {
                                                        let crCode = sel.split(':');
                                                        console.log("CourseCode -->", crCode);
                                                        course_cricos_code.push(crCode[1].trim());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }



                            if (newcampus && newcampus.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();


                                var campusedata = [];
                                newcampus.forEach(element => {
                                    campusedata.push({
                                        "name": format_functions.titleCase(element),
                                        "code": course_cricos_code.toString()
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;//await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                                resJsonData.course_study_mode = "On campus";//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }

                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            let feesList_final = [];

                            //let feesDict = {};
                            console.log("demoarray123 -->", demoarray);
                            var fee_desc = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal_fee = courseScrappedData.course_tuition_fees_international_student;
                            if (Array.isArray(courseKeyVal_fee)) {
                                for (const rootEleDict of courseKeyVal_fee) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selListFees = ' + JSON.stringify(selList));
                                            for (let sel of selList) {
                                                sel = sel.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s\s)/g, '');
                                                // console.log("Sel Before -->>", sel);
                                                sel = sel.trim();
                                                if (sel.includes('per semester')) {
                                                    sel = sel.replace(/per semester/g, '');
                                                }
                                                // fullTimeText = sel.trim();
                                                fee_desc.push(sel.trim());
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("fee_desc -->", fee_desc);
                            let replace_fees;
                            let fees_dicription;
                            if (fee_desc.includes('),')) {
                                fees_dicription = fee_desc.toString().replace('(', ' = ');
                                replace_fees = fees_dicription.split('),')[0];
                            } else {
                                replace_fees = fee_desc.toString();
                            }

                            console.log("fees_dicription---->>>>", fees_dicription)
                            console.log("replace_fees---->>>>", replace_fees)
                            var fee_desc_material_fees = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal_material_fees = courseScrappedData.course_tuition_fees_international_student_material_fees;
                            if (Array.isArray(courseKeyVal_material_fees)) {
                                for (const rootEleDict of courseKeyVal_material_fees) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            for (let sel of selList) {
                                                sel = sel.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s|\s\s)/g, '')
                                                fee_desc_material_fees.push("Materials fee: " + sel.trim());
                                            }
                                        }
                                    }
                                }
                            }
                            var fee_desc_resource_fees = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal_resource_fees = courseScrappedData.course_tuition_fees_international_student_resource_fees;
                            if (Array.isArray(courseKeyVal_resource_fees)) {
                                for (const rootEleDict of courseKeyVal_resource_fees) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            for (let sel of selList) {
                                                sel = sel.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s|\s\s)/g, '')
                                                fee_desc_resource_fees.push("Resource fee: " + sel.trim());
                                            }
                                        }
                                    }
                                }
                            }
                            let fee_desc_more = [];

                            for (let i = 0; i < fee_desc_material_fees.length; i++) {
                                fee_desc_more.push(fee_desc_material_fees[i] + " " + fee_desc_resource_fees[i])
                            }
                            console.log("fee_desc_more -->", fee_desc_more);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feeYear && feeYear.length > 0) {
                                courseTuitionFee.year = feeYear;
                            }

                            const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                            const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                            var campus = resJsonData.course_campus_location;

                            for (let i = 0; i < campus.length; i++) {
                                console.log("campus.length -> ", campus.length);
                                let feesDict = {
                                    international_student: []
                                };

                                feesDict.fee_duration_years = feesDuration;
                                feesDict.currency = feesCurrency;
                                var fee = fee_desc[i].split("$");
                                feesList_final = await utils.giveMeNumber(fee[1].toString().replace(/[\r\n\t ]+/g, ' '));
                                console.log("feesList_final------>", feesList_final);

                                if (feesList_final) {
                                    if (Number(feesList_final)) {
                                        feesDict.international_student = ({
                                            amount: Number(feesList_final),
                                            duration: 1,
                                            unit: "semester",
                                            description: replace_fees,
                                        });
                                    } else {
                                        feesDict.international_student = ({
                                            amount: 0,
                                            duration: 1,
                                            unit: "semester",
                                            description: "not available fee",
                                        });
                                    }
                                } else {
                                    feesDict.international_student = ({
                                        amount: 0,
                                        duration: 1,
                                        unit: "semester",
                                        description: "not available fee",
                                    });
                                }
                                console.log("feesDictinternational_student-->", feesDict.international_student);
                                // feesDict.international_student_all_fees = [fee_desc_more[i]];
                                console.log("loc----->", campus.name);
                                feesList.push({ name: campus[i].name, value: feesDict });
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                    console.log("feesList--------->", feesList);
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }

                            break;
                        }
                        case 'course_intake_url': {
                            const courseKeyVal = courseScrappedData.course_intake_url;
                            var url = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                url = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (url.length > 0) {
                                resJsonData.course_intake_url = url;
                            }
                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = [];
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            //     program_code = selList;
                                            // }
                                            for (let selItem of selList) {
                                                let splitProgramCode = selItem.split('|');
                                                for (let sel of splitProgramCode) {
                                                    if (sel.toLowerCase().includes('course code')) {
                                                        let courseCode = sel.split(':');
                                                        console.log("CourseCode -->", courseCode);
                                                        program_code.push(courseCode[1].trim());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            for (let program_val of program_code) {
                                resJsonData.program_code = program_val;
                            }
                            break;
                        }
                        case 'course_intake': {
                            const courseKeyVal = courseScrappedData.course_intake;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            for (let selItem of selList) {
                                                selItem = selItem.replace(/intakes/g, '');
                                                selItem = selItem.replace(/intake/g, '');
                                                var courseIntakeStr = [];
                                                if (selItem.includes('(')) {
                                                    // selItem = selItem.replace(/\(|\)/, '');
                                                    // selItem = selItem.replace('\[(.*?)\]', "")//extracts values from the baracket 
                                                    selItem = selItem.split(')')[0].split('(')[1];
                                                    console.log("selItem -->", selItem);
                                                    if (selItem.includes('and')) {
                                                        selItem = selItem.replace(/and/g, ',');
                                                    }
                                                    selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s|)/g, '').split(",");
                                                    console.log("selItemIntake -->", selItem);
                                                    for (let sel of selItem) {
                                                        sel = sel.trim();
                                                        courseIntakeStr.push(sel);
                                                        console.log("courseIntakeStr -->", courseIntakeStr);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            var campus11 = resJsonData.course_campus_location;


                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];

                                for (let location11 of campus11) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location11.name,
                                        "value": courseIntakeStr
                                    });
                                }
                            }
                            console.log("finalIntakes -->", courseIntakeStr);
                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            let formatedIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");
                            console.log("Intakes --> ", JSON.stringify(formatedIntake));
                            var intakedata = {};
                            intakedata.intake = formatedIntake;
                            intakedata.more_details = more_details;
                            //intakedata.more_details = resJsonData.course_intake_url;
                            resJsonData.course_intake = intakedata;
                            break;
                        }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }

                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome
                            } else {
                                resJsonData.course_career_outcome = []
                            }
                            break;
                        }

                        case 'course_study_level': {
                            const courseKeyVal_study = courseScrappedData.course_cricos_code;
                            let course_cricos_code = [];
                            if (Array.isArray(courseKeyVal_study)) {
                                for (const rootEleDict of courseKeyVal_study) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            for (let selItem of selList) {
                                                let splitCricosCode = selItem.split('|');
                                                for (let sel of splitCricosCode) {
                                                    if (sel.toLowerCase().includes('cricos code')) {
                                                        let crCode = sel.split(':');
                                                        console.log("CourseCode -->", crCode);
                                                        course_cricos_code.push(crCode[1].trim());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("cTitle------>>>>>>", course_cricos_code);
                            const study_val = await format_functions.getMyStudyLevel(course_cricos_code);
                            resJsonData.course_study_level = study_val;
                            console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                            break;
                        }
                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };
                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal111 = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal111------->", JSON.stringify(courseKeyVal111))
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal111)) {
                                for (const rootEleDict of courseKeyVal111) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }
                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }
                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)
                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;
                            break;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                // if (application_fee.length > 0) {
                                //     resJsonData.application_fee = application_fee;
                                // } else {
                                resJsonData.application_fee = '';
                                // }

                                break;
                            }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        }
                    }
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                }
            }
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;


                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
