const fs = require('fs');
const appConfigs = require('./app-config');
// eslint-disable-next-line import/no-unresolved
const configs = require('./../configs');

// validate function params
async function validateParams(paramsList) {
  const funcName = 'validateParams ';
  try {
    if (!paramsList) {
      console.log(funcName + 'Invalid value of paramsList');
      throw (new Error('Invalid value of paramsList'));
    }
    const invalidParamsList = [];
    paramsList.forEach((param) => {
      if (!param) {
        invalidParamsList.push(param);
      } // if
    });
    if (invalidParamsList.length > 0) {
      console.log(funcName + 'Invalid value of parameter, invalidParamsList = ' + JSON.stringify(invalidParamsList));
      throw (new Error('Invalid value of parameter, invalidParamsList = ' + JSON.stringify(invalidParamsList)));
    }
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

// check if string has valid cricos code
async function hasValidCricosode(scrappedCricosCodeStr) {
  const funcName = 'hasValidCricosode ';
  try {
    if (scrappedCricosCodeStr && scrappedCricosCodeStr.length > 0) {
      //&& String(scrappedCricosCodeStr).includes('N/A') === false) {
      return true;
    }
    return false;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

async function getSelectorsListForKey(rootEleKeyName, selectorJson) {
  const funcName = 'getSelectorsListForKey ';
  try {
    // get first root elementDict
    validateParams([rootEleKeyName, selectorJson]);
    const rootElementDictList = selectorJson[rootEleKeyName];
    console.log(funcName + 'rootElementDictList = ' + JSON.stringify(rootElementDictList));
    if (!(rootElementDictList && rootElementDictList.length > 0)) {
      console.log(funcName + 'Invalid or null course_list_selector key');
      throw (new Error('Invalid or null course_list_selector key'));
    }
    const rootElementDict = rootElementDictList[0];
    console.log(funcName + 'rootElementDict = ' + JSON.stringify(rootElementDict));
    // get url and elements list of this element dict
    const url = rootElementDict.url;
    console.log(funcName + 'url = ' + url);
    const elementsList = rootElementDict.elements;
    if (!(elementsList && elementsList.length > 0)) {
      console.log(funcName + 'Invalid or null elements key');
      throw (new Error('Invalid or null elements key'));
    }
    // get first elementDict
    const elementDict = elementsList[0];
    console.log(funcName + 'elementDict = ' + JSON.stringify(elementDict));
    const selectorsList = elementDict.selectors;
    if (!(selectorsList && selectorsList.length > 0)) {
      console.log(funcName + 'Invalid or null selectors key');
      return null;
    }
    return selectorsList;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

// get elementDict
async function getElementDictForKey(rootEleKeyName, selectorJson) {
  const funcName = 'getElementDictForKey ';
  try {
    // get first root elementDict
    validateParams([rootEleKeyName, selectorJson]);
    const rootElementDictList = selectorJson[rootEleKeyName];
    console.log(funcName + 'rootElementDictList = ' + JSON.stringify(rootElementDictList));
    if (!(rootElementDictList && rootElementDictList.length > 0)) {
      console.log(funcName + 'Invalid or null course_list_selector key');
      throw (new Error('Invalid or null course_list_selector key'));
    }
    const rootElementDict = rootElementDictList[0];
    console.log(funcName + 'rootElementDict = ' + JSON.stringify(rootElementDict));
    // get url and elements list of this element dict
    const url = rootElementDict.url;
    console.log(funcName + 'url = ' + url);
    const elementsList = rootElementDict.elements;
    if (!(elementsList && elementsList.length > 0)) {
      console.log(funcName + 'Invalid or null elements key');
      throw (new Error('Invalid or null elements key'));
    }
    // get first elementDict
    const elementDict = elementsList[0];
    if (elementDict) {
      return elementDict;
    }
    return null;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

// get root element 
async function getRooElementDictUrl(rootEleKeyName, selectorJson) {
  const funcName = 'getRooElementDictUrl ';
  try {
    // get first root elementDict
    validateParams([rootEleKeyName, selectorJson]);
    const rootElementDictList = selectorJson[rootEleKeyName];
    console.log(funcName + 'rootElementDictList = ' + JSON.stringify(rootElementDictList));
    if (!(rootElementDictList && rootElementDictList.length > 0)) {
      console.log(funcName + 'Invalid or null course_list_selector key');
      throw (new Error('Invalid or null course_list_selector key'));
    }
    const rootElementDict = rootElementDictList[0];
    console.log(funcName + 'rootElementDict = ' + JSON.stringify(rootElementDict));
    // get url and elements list of this element dict
    const url = rootElementDict.url;
    console.log(funcName + 'url = ' + url);
    if (url) {
      return url;
    }
    return null;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}

// create course id from course title
// Trim and remove all white spcaes and conver to lower case
async function removeAllWhiteSpcaesAndLowerCase(val) {
  const funcName = 'removeAllWhiteSpcaesAndLowerCase ';
  try {
    if (!(val && val.length > 0)) {
      console.log(funcName + 'Invalid prama, val = ' + val);
      throw (new Error('Invalid prama, val = ' + val));
    }
    // console.log(funcName + 'val  before operation = ' + val);
    const trimmedVal = String(val).trim();
    // console.log(funcName + 'trimmedVal = ' + trimmedVal);
    const regex = /[,\/ ]/g;
    const wsRemovedVal = String(trimmedVal).replace(regex, '');
    const ucVal = String(wsRemovedVal).toLowerCase();
    // console.log(funcName + 'val  after operation = ' + ucVal);
    return ucVal;
  } catch (error) {
    console.log(funcName + 'try-catch error = ', error);
    throw (error);
  }
}

// generate opCourseFilePath
async function generateOutputCourseFilePath(courseDict) {
  const funcName = 'generateOutputCourseFilePath ';
  try {
    let opCourseFilePath = null;
    if (!courseDict) {
      console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
      throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
    }
    // create courseId from course title
    const courseId = await removeAllWhiteSpcaesAndLowerCase(courseDict.innerText);
    // validate generated courseId
    console.log(funcName + 'courseId = ' + courseId);
    if (!(courseId && courseId.length > 0)) {
      console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
      throw (new Error('Invalid courseId, courseId = ' + courseId));
    }
    if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
      opCourseFilePath = configs.outputDirPath + configs.univ_id + '_' + courseId + '_update_coursedetails.json';
    } else {
      opCourseFilePath = configs.outputDirPath + configs.univ_id + '_' + courseId + '_coursedetails.json';
    }
    console.log(funcName + 'opCourseFilePath = ' + opCourseFilePath);
    return opCourseFilePath;
  } catch (error) {
    console.log(funcName + 'try-catch error = ', error);
    throw (error);
  }
}

// get values from hard-coded json file
async function getValueFromHardCodedJsonFile(jsonKeyName) {
  const funcName = 'getValueFromHardCodedJsonFile ';
  try {
    validateParams([jsonKeyName]);
    const fileData = fs.readFileSync(appConfigs.hardCodedJsonDataFilepath);
    if (!fileData) {
      throw (new Error('Invalid file data, fileData = ' + fileData));
    }
    const dataJSon = JSON.parse(fileData)[0];
    if (!dataJSon) {
      throw (new Error('Invalid json data, dataJSon = ' + JSON.stringify(dataJSon)));
    }
    const keyValData = dataJSon[jsonKeyName];
    return keyValData;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}
async function giveMeArray(rootstring, seperator) {
  console.log("Called array-->" + rootstring)
  var array_data = rootstring.replace(/\s/g, ' ').split(seperator);
  var retDict = [];
  array_data.forEach(element => {
    if (element) {
      retDict.push(element);
    }
  });
  return await retDict;
}
async function hasNumber(myString) {
  return await /\d/.test(myString);
}
async function giveMeNumber(myString) {
  var myarray = myString.split(" ");
  console.log("### split data-->" + JSON.stringify(myarray));
  for (let element of myarray) {
    if (/\d/.test(element)) {
      console.log("##match" + element)
      return await parseFloat(element.replace(/[\r\n\t:,]+/g, ''))
    }
  }
  return await "NA";
}
async function getMappingScore(coursename) {
  try {
    const scrore_file = "./Mapping/course_mapping.json";

    const fileData = JSON.parse(fs.readFileSync(scrore_file));
    if (!fileData) {
      throw (new Error('Invalid file data, fileData = ' + fileData));
    }
    var returnDict = {};
    
    for (let data of fileData) {
      console.log("Data : ", data);
      if (data.key.includes(coursename)) {
        console.log("####let data of fileData[1] Match data for--->" + coursename);
        returnDict.ielts = data.ielts;        
        returnDict.ibt = data.ibt;
        returnDict.pte = data.pte;
        returnDict.cae = data.cae;
      }

    }
    return await returnDict;
  } catch (error) {
    console.log('try-catch error = ' + error);
    throw (error);
  }
}
module.exports = {
  getSelectorsListForKey,
  getElementDictForKey,
  getRooElementDictUrl,
  getMappingScore,
  removeAllWhiteSpcaesAndLowerCase,
  generateOutputCourseFilePath,
  getValueFromHardCodedJsonFile,
  hasValidCricosode,
  giveMeArray,
  hasNumber,
  giveMeNumber,
};
