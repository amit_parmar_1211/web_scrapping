var pdfreader = require('pdfreader');
const fs = require('fs');
readline = require('readline');

async function readandgeneratefile(key, filepath, pfdpath) {
    if (fs.existsSync(filepath)) {
        fs.unlinkSync(filepath);
    }
    const nbCols = 2;
    const cellPadding = 70; // each cell is padded to fit 40 characters
    const columnQuantitizer = (item) => parseFloat(item.x) >= 10;
    const padColumns = (array, nb) =>
        Array.apply(null, { length: nb }).map((val, i) => array[i] || []);
    // .. because map() skips undefined elements
    const mergeCells = (cells) => (cells || [])
        .map((cell) => cell.text).join(' | ') // merge cells
    //.substr(0, cellPadding).padEnd(cellPadding, ' '); // padding

    const renderMatrix = (matrix) => (matrix || [])
        .map((row, y) => padColumns(row, nbCols)
            .map(mergeCells)
            .join(' | ')
        ).join('\n');
    var table = new pdfreader.TableParser();
    new pdfreader.PdfReader().parseFileItems(pfdpath, function (err, item) {
        if (!item || item.page) {
            // end of file, or page
            var filedata = JSON.stringify(renderMatrix(table.getMatrix()));
            filedata = filedata.split('\\n');
            for (var d of filedata) {
                var JSONDATA = [];
                if (fs.existsSync(filepath)) {
                    console.log("Entered if");
                    JSONDATA = JSON.parse(fs.readFileSync(filepath));
                }
                var mycode = d//.split("|");
                console.log("R mycode", mycode);
                // if (mycode[cricodeposition]) {
                //     if (mycode[cricodeposition].trim().length == cricodelength) {
                         var myjsondata = {};
                        for (var count = 0; count < key.length; count++) {
                            if (mycode[count]) {
                                myjsondata[key[count]] = mycode[count].trim();
                            }
                        }
                        JSONDATA.push(mycode);
                        fs.writeFileSync(filepath, JSON.stringify(JSONDATA))
                //     }
                // }
            }
            table = new pdfreader.TableParser(); // new/clear table for next page
        } else if (item.text) {
            // accumulate text items into rows object, per line
            table.processItem(item, columnQuantitizer(item));
        }
        if (err) {
            console.log("###Error--->" + err);
        }
    });
    return await true;
}
const key = ["Courses"];
//const cricodeposition = 1;
//const cricodelength = 7;
const jsonfilepath = "./danfordIntake.json";
const pdffilepath = "./Danford_2020-Intakes.pdf";
readandgeneratefile(key, jsonfilepath, pdffilepath);        