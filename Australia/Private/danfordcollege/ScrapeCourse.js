const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    static async formatOutput(courseScrappedData, course_url, course_name, course_category, program_code, s) {
        const funcName = 'formatOutput ';
        try {

            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];

            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {


                        case 'course_title': {
                            resJsonData.course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("resJsonData.course_title3333", resJsonData.course_title);
                            const tt = String(resJsonData.course_title);
                            let ttt1 = format_functions.titleCase(tt).trim();
                            console.log("ctitle@@@", ttt1.trim());
                            const ttt = String(ttt1).split('(')[0].trim();
                            console.log("tttdgsfdgf", ttt);
                            resJsonData.course_title = ttt;
                        }

                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = course_category;
                            // As of not we do not have way to get this field value
                            break;
                        }



                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':

                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;

                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList[0];
                                                console.log("##fullTimeText--->" + JSON.stringify(fullTimeText));
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }

                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {};; let ieltsNumber = null;
                            let courseKeyVal_ielts = courseScrappedData.ielts_req;
                            let ielts_req = ""
                            // let courseKeyVal_pte = await Course.extractValueFromScrappedElement(courseScrappedData.pte);
                            // let courseKeyVal_ibt = await Course.extractValueFromScrappedElement(courseScrappedData.ibt);
                            // let courseKeyVal_toefl = await Course.extractValueFromScrappedElement(courseScrappedData.toefl);
                            // let courseKeyVal_cea = await Course.extractValueFromScrappedElement(courseScrappedData.cea);
                            console.log("courseKeyVal_ielts$$$$$$", JSON.stringify(courseKeyVal_ielts));


                            if (Array.isArray(courseKeyVal_ielts)) {
                                for (const rootEleDict of courseKeyVal_ielts) {
                                    console.log(funcName + '\n\r rootEleDict ieltsDict= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict ieltsDict= ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList ieltsDict= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList)) {
                                                for (let sel of selList) {
                                                    if (ielts_req != sel) {
                                                        ielts_req = sel;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            console.log("##ielts_req-->" + ielts_req);
                            if (ielts_req.length == 0) {
                                //  ielts_req = "Academic IELTS of 5.5 with 5.0 in all bands, or equivalent.";
                            }

                            ///progress bar start
                            const pieltsDict = {}; const pbtDict = {}; const ppteDict = {}; const ibtDict = {}; const caeDict = {};
                            var penglishList = [];
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "", pbtScore = "";
                            if (ielts_req) {
                                ieltsScore = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));
                            }
                            // if (courseKeyVal_pte) {
                            //     pteScore = await utils.giveMeNumber(courseKeyVal_pte.replace(/ /g, ' '));
                            // }
                            // if (courseKeyVal_ibt) {
                            //     ibtScore = await utils.giveMeNumber(courseKeyVal_ibt.replace(/ /g, ' '));
                            // }
                            // if (courseKeyVal_toefl) {
                            //     pbtScore = await utils.giveMeNumber(courseKeyVal_toefl.replace(/ /g, ' '));
                            // }
                            // if (courseKeyVal_cea) {
                            //     caeScore = await utils.giveMeNumber(courseKeyVal_cea.replace(/ /g, ' '));
                            // }


                            if (ieltsScore) {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = ielts_req;
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsScore;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }


                            // if (pteScore) {
                            //     ppteDict.name = 'pte academic';
                            //     ppteDict.description = courseKeyVal_pte;
                            //     ppteDict.min = 0;
                            //     ppteDict.require = pteScore;
                            //     ppteDict.max = 90;
                            //     ppteDict.R = 0;
                            //     ppteDict.W = 0;
                            //     ppteDict.S = 0;
                            //     ppteDict.L = 0;
                            //     ppteDict.O = 0;
                            //     penglishList.push(ppteDict);
                            // }
                            // if (ibtScore) {
                            //     ibtDict.name = 'toefl ibt';
                            //     ibtDict.description = courseKeyVal_ibt;
                            //     ibtDict.min = 0;
                            //     ibtDict.require = ibtScore;
                            //     ibtDict.max = 120;
                            //     ibtDict.R = 0;
                            //     ibtDict.W = 0;
                            //     ibtDict.S = 0;
                            //     ibtDict.L = 0;
                            //     ibtDict.O = 0;
                            //     penglishList.push(ibtDict);
                            // }
                            // if (pbtScore) {
                            //     pbtDict.name = 'toefl pbt';
                            //     pbtDict.description = courseKeyVal_toefl;
                            //     pbtDict.min = 310;
                            //     pbtDict.require = pbtScore;
                            //     pbtDict.max = 677;
                            //     pbtDict.R = 0;
                            //     pbtDict.W = 0;
                            //     pbtDict.S = 0;
                            //     pbtDict.L = 0;
                            //     pbtDict.O = 0;
                            //     penglishList.push(pbtDict);
                            // }
                            // if (caeScore) {
                            //     caeDict.name = 'cae';
                            //     caeDict.description = courseKeyVal_cea;
                            //     caeDict.min = 80;
                            //     caeDict.require = caeScore;
                            //     caeDict.max = 230;
                            //     caeDict.R = 0;
                            //     caeDict.W = 0;
                            //     caeDict.S = 0;
                            //     caeDict.L = 0;
                            //     caeDict.O = 0;
                            //     penglishList.push(caeDict);
                            // }

                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }
                            else {
                                //  throw new Error("english not found");
                            }

                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var academicReq = "";


                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            courseAdminReq.entry_requirements_url = entry_requirements_url;
                            courseAdminReq.english_requirements_url = english_more;
                            courseAdminReq.academic_requirements_url = academic_more;
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':

                        case 'course_campus_location': { // Location Launceston
                            let campLocationText = await utils.getValueFromHardCodedJsonFile('location');
                            console.log("fgdugdbgkugyre", campLocationText);
                            let newcampus = [];
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            let splitCricos = cTitle.split('CRICOS CODE')[1];



                            // loca.forEach(element => {
                            //     if (element.includes('Campus')) {
                            //         finallocation.push(element.replace('Campus', ''))
                            //     }
                            // })
                            // var campusss = [];
                            // if (finallocation.length > 0) {
                            //     console.log("loca------>", finallocation);
                            //     let campuses = ['Melbourne Campus', 'Hobart Campus']
                            //     console.log("@@campuses------->", campuses);

                            //     for (let i = 0; i < finallocation.length; i++) {
                            //         campuses.forEach(element => {
                            //             console.log(element.includes(finallocation[i].trim()))
                            //             if (element.includes(finallocation[i].trim())) {

                            //                 campusss.push(element)

                            //             }
                            //         })
                            //     }
                            //     console.log("@@campuses------->", campusss)





                            console.log("##Campus-->" + JSON.stringify(campLocationText))
                            if (campLocationText && campLocationText.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                resJsonData.course_study_mode = "On campus";
                                console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                var campusedata = [];
                                [campLocationText].forEach(element => {
                                    campusedata.push({
                                        "name": element,
                                        "code": splitCricos.trim()
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                // ************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE ************** //
                                var study_mode = [];

                                if (campLocationValTrimmed.trim().length > 0) {
                                    study_mode.push('On campus');
                                }
                                resJsonData.course_study_mode = study_mode.toString();//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                                // }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }


                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""


                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                            break;

                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);

                                // let applicationfee = JSON.parse(fs.readFileSync(appConfigs.Locations));
                                // console.log("cricosIntStudent----->>>>>", applicationfee);
                                // var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                                // console.log("splitStr@@@2" + title);
                                // let tmpvar = applicationfee.filter(element => {
                                //     return element.Course_Name.toLowerCase().trim() == title.toLowerCase().trim()
                                // });
                                // console.log("tmpvar----->>>>>", tmpvar);
                                // let application = tmpvar[0].Application_Fee;
                                // console.log("one----->>>>>", application);
                                resJsonData.application_fee = ""
                                break;
                            }
                        case 'course_study_mode':
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {
                                international_student: {},


                            };

                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);

                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const arrval = String(feesWithDollorTrimmed).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,

                                                description: feesIntStudent,

                                            });

                                        }
                                        if (feesNumber == "0") {
                                            console.log("FEesNumber = 0");
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,
                                                description: "",
                                            });
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: Number(demoarray.duration),
                                    unit: demoarray.unit,
                                    description: "",
                                });
                            }
                            var fee_desc_more = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            let courseKeyVal_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student_more);
                            console.log("courseKeyVal_more@@1", courseKeyVal_more);

                            fee_desc_more.push(courseKeyVal_more)
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                  //  feesDict.international_student_all_fees = fee_desc_more
                                    console.log("morefees@@2", fee_desc_more);

                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }
                        case 'program_code': {
                            resJsonData.program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            const pro = String(resJsonData.program_code);
                            console.log("prooohhfdoooooo", pro);
                            // if (pro == null) {
                            //     console.log("prooooooooo", pro);
                            //     pro = "BSB80615"
                            //     resJsonData.program_code = pro;
                            // }

                            const p = String(pro).split('(')[1];
                            const p1 = String(p).split(')')[0];
                            console.log("p@@@@@@", p1);
                            resJsonData.program_code = p1;


                            break;
                        }

                        case 'course_intake': {
                            var courseIntake = [];
                            //var courseIntakeDisplay = [];
                            let datas = [];
                            let courseIntakeStr = await utils.getValueFromHardCodedJsonFile('intake_mapping');
                            let dat = courseIntakeStr;
                            let part
                            let one;
                            //  var course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            let tmpvar = dat.filter(val => {
                                return val.name.toLowerCase() == course_name.toLowerCase()

                            });
                            console.log("tmpvar$$$$$$$$$$$$", tmpvar)
                            let data_text = tmpvar;
                            console.log("data_text", data_text)
                            if (data_text.length != []) {
                                one = data_text[0].date;
                                console.log("One########", one);
                                // one = tmpvar[0].date
                                //     console.log("One########", one);
                                // if (one.length == 0) {
                                //     console.log("DAte######--->", one)
                                // }

                                if (one.indexOf(',') > -1) {
                                    part = one.split(",");
                                }
                                else {
                                    let arr = []
                                    arr.push(one)
                                    part = arr
                                }
                                console.log("One--->", part)
                                var campus = resJsonData.course_campus_location;
                                for (let location11 of campus) {
                                    var intakedetail = {};
                                    intakedetail.name = location11.name;
                                    intakedetail.value = part;
                                    datas.push(intakedetail);
                                }
                                console.log("DAte--->", datas)
                                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                let formatedIntake = await format_functions.providemyintake(datas, "ddmmyyyy", "-");
                                var intakedata = {};
                                intakedata.intake = formatedIntake;
                                let valmonths11 = "";
                                intakedata.more_details = more_details;
                                resJsonData.course_intake = intakedata;


                            } else if (data_text.length == 0) {
                                one = data_text
                                console.log("One@@@@@@@", one);
                                let courseIntakeDisplay = [];
                                var campus = resJsonData.course_campus_location;
                                if (courseIntakeDisplay) {
                                    console.log("In Main If", courseIntakeDisplay);
                                    var myintake = [];
                                    var intakedata = {}
                                    let arr = [];
                                    console.log("arr--->", arr)
                                    for (i = 0; i < campus.length; i++) {
                                        var intakedetail = {};
                                        console.log("campus--->", campus[i].name)
                                        intakedetail.name = campus[i].name;
                                        intakedetail.value = arr;
                                        myintake.push(intakedetail)
                                        console.log("CourseDetails-->", JSON.stringify(myintake))
                                    }
                                    intakedata.intake = myintake
                                    intakedata.more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                    resJsonData.course_intake = intakedata;

                                }
                            }



                        
                            break;
                        }
                        case 'course_scholarship': {
                            const courseKeyVal = courseScrappedData.course_scholarship;
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                resJsonData.course_scholarship = resScholarshipJson;
                            }
                            break;
                        }
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                        case 'course_accomodation_cost': {
                            const courseKeyVal = courseScrappedData.course_accomodation_cost;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_accomodation_cost = resAccomodationCostJson;
                            }
                            break;
                        }
                      

                        case 'course_country': {
                            resJsonData.course_country = await Course.extractValueFromScrappedElement(courseScrappedData.course_country)
                            break;
                        }
                        case 'course_overview': {
                            resJsonData.course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            let course_overview = resJsonData.course_overview;
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            for (let sel of selList) {
                                                sel = sel.replace(/[\r\n\t ]+/g, ' ').trim();
                                                if (sel.length > 0) {
                                                    console.log("sel----->", sel);
                                                    course_career_outcome.push(sel)
                                                    break;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = [course_career_outcome[0]]
                            } else {
                                resJsonData.course_career_outcome = [];
                            }


                            break;
                        }
                        case 'course_career_outcome': {
                            //  const courseKeyVal = courseScrappedData.course_career_outcome;
                            resJsonData.course_career_outcome = await Course.extractValueFromScrappedElement(courseScrappedData.course_career_outcome);
                            let course_career_outcome = resJsonData.course_career_outcome;
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = [course_career_outcome]
                            } else {
                                resJsonData.course_career_outcome = [];
                            }


                            break;
                        }
                        case 'course_study_level': {
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            let splitCricos = cTitle.split('CRICOS CODE')[1];
                            console.log("resJsonData.sssss------>>>>>>", splitCricos);

                            const study_val = await format_functions.getMyStudyLevel(splitCricos.trim());
                            resJsonData.course_study_level = study_val;
                            console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;

            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                // NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                // console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                //   NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }


                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
               // resJsonData.basecourseid = location_wise_data.course_id;

                // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                //     if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                //         resJsonData.course_cricos_code[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_cricos_code[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                //     if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_intake.intake[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_intake.intake[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                //     if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                //     if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_campus_location[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_campus_location[i].iscurrent = false;
                //     }
                // }



                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        const resJsonData = {};
        try {
            s = new Scrape();
            await s.init({ headless: false });
            await s.setupNewBrowserPage(courseDict.href);



            // let links2 = await s.page.$x("//*/div[2]/a[contains(text(),'SEE LESS')]");
            // await links2[0].click();
            // console.log("ClickedButton")
            // await s.page.waitFor(2000)
            //console.log("program_codeprogram_code@@@@@", courseDict.program_code);

            // let course_title = resJsonData.course_title;
            // console.log("course_title......>", course_title)
            // if (course_name.toLowerCase() == course_title.toLowerCase()) {
            //     console.log("in if@@@@");
            //     let links2 = await s.page.$x("//*/div[2]/a[contains(text(),'SEE MORE')]");
            //     await links2[0].click();
            //     console.log("ClickedButton")

            // }

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }






            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            const program_code = courseScrappedData.program_code
            //  console.log("data---------------->", JSON.stringify(program_code));

            // resJsonData.program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);

            // const pro = String(program_code);
            // const p = String(pro).split('(')[1];
            // const p1 = String(p).split(')')[0];
            // console.log("p1---------------->", p1);
            // console.log("program_code@@@@@@@@@",program_code)            
            // if (program_code == p1) {
            //     console.log("in if@@@@");
            //     let links2 = await s.page.$x("//*/div[2]/a[contains(text(),'SEE MORE')]");
            //     await links2[0].click();
            //     console.log("ClickedButton")

            // }

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url, courseDict.innerText, courseDict.category, courseDict.program_code, s);

            const finalScrappedData = await Course.removeKeys(formattedScrappedData);





            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
