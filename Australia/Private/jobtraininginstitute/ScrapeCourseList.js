const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'scrapeCourseCategoryList ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: false });
      var totalCourseList = [];
      var name;
      var href;
      await s.setupNewBrowserPage("https://jti.edu.au/");
      var mainCategory = [], redirecturl = [];
      // await page.waitforNavigation();
      var buttonInternational = "//*[@id='menu-item-6871']/a[contains(text(),'Courses')]";
      var buttonInternationalelem = await s.page.$x(buttonInternational);
      await buttonInternationalelem[0].click();
      
      console.log("clickedd-->");
      const category_val = "//*[@id='menu-item-3028']/a/following::ul[1]/li/a[not(contains(text(),'Short Courses'))]";
      const levels = await s.page.$x(category_val)
      for (let country of levels) {
        name = await s.page.evaluate(el => el.innerText, country);
        href = await s.page.evaluate(el => el.href, country);
        redirecturl.push({ name: name, href: href });
        mainCategory.push(name);
      }
      console.log("subjectAreasArray-->", redirecturl)
      for (let catc of redirecturl) {
        await s.page.goto(catc.href);

        var subjects = await s.page.$x("//*//strong[contains(text(),'')]/following::div[@class='vce-row-container'][2]/div[@class='vce-row vce-element--has-background vce-row--col-gap-30 vce-row-columns--top vce-row-content--top']//a");
        for (let i = 0; i < subjects.length; i++) {
          var elementstring = await s.page.evaluate(el => el.innerText, subjects[i]);
          var elementlink = await s.page.evaluate(el => el.href, subjects[i])
          totalCourseList.push({ href: elementlink, innerText: elementstring, category: catc.name })
        }
        console.log("subjects2--->", subjects);

      }

      fs.writeFileSync("./output/jobtraininginstitute_maincategorylist.json", JSON.stringify(mainCategory))
      console.log("totalCourseList -->", totalCourseList);
      await fs.writeFileSync("./output/jobtraininginstitute_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
      await fs.writeFileSync("./output/jobtraininginstitute_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/jobtraininginstitute_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  };
}
module.exports = { ScrapeCourseList };


