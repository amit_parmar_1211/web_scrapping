// const fs = require('fs');
// const Scrape = require('./common/scrape').Scrape;
// const utils = require('./common/utils');
// const configs = require('./configs');
// const appConfigs = require('./common/app-config');
// class ScrapeCourseList extends Scrape {
//   // refers to http://waifs.wa.edu.au/courses/
//   async scrapeOnlyInternationalCourseList() {
//     const funcName = 'startScrappingFunc ';
//     var s = null;
//     try {
//       s = new Scrape();
//       await s.init({ headless: true });
//       var datalist = [];
//       await s.setupNewBrowserPage("https://www.melbournecitycollege.edu.au/");
//       const categoryselectorUrl = "//*[@id='nav']/li[3]/ul[@class='sub_nav']/div[@class='width']/..//li"
//       const targetLinksCardsUrls = await s.page.$x(categoryselectorUrl);
//       const categoryselectorUrl1 = "//*[@id='nav']/li[3]/ul[@class='sub_nav']/div[@class='width']/..//li/a";
//       const targetLinksCardsUrls1 = await s.page.$x(categoryselectorUrl1);
//       var targetLinks = []
//       for (let i = 0; i < targetLinksCardsUrls.length; i++) {
//         var elementstring = await s.page.evaluate(el => el.href, targetLinksCardsUrls[i]);
//         var elementhref = await s.page.evaluate(el => el.innerText, targetLinksCardsUrls1[i]);
//         elementstring = elementstring.replace("\n", " ");
//         targetLinks.push({ href: elementstring, innerText: elementhref });
//       }
//       console.log("MAinCategory-->", targetLinks)
//       await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinks));
//       var totalCourseList = [];
//       for (let target of targetLinks) {
//         await s.page.goto(target.href, { timeout: 0 });
//         console.log("In_for------>")
//         let linkselector = await s.page.$x("//*//h1[contains(text(),'" + target.innerText + "')]//following-sibling::ul/li/a")
//         let textselector = await s.page.$x("//*//h1[contains(text(),'" + target.innerText + "')]//following-sibling::ul/li/a")
//         console.log("Length@@@", linkselector.length)
//         for (let i = 0; i < linkselector.length; i++) {
//           const elementurl = await s.page.evaluate(el => el.href, linkselector[i])
//           console.log("elementurl-->", elementurl)
//           const elementstring = await s.page.evaluate(el => el.innerText, textselector[i])
//           console.log("elementstring-->", elementstring)
//           if (!elementstring.includes('Local Student')) {
//             console.log("In_for------>")
//             totalCourseList.push({ href: elementurl, innerText: elementstring, category: target.innerText });
//           }
//         }
//       }
//       await fs.writeFileSync("./output/westernaustralianinstitutesoffurtherstudies_original_courselist.json", JSON.stringify(totalCourseList));
//       let uniqueUrl = [];
//       //unique url from the courselist file
//       for (let i = 0; i < totalCourseList.length; i++) {
//         let cnt = 0;
//         if (uniqueUrl.length > 0) {
//           for (let j = 0; j < uniqueUrl.length; j++) {
//             if (totalCourseList[i].href == uniqueUrl[j].href) {
//               cnt = 0;
//               break;
//             } else {
//               cnt++;
//             }
//           }
//           if (cnt > 0) {
//             uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
//           }
//         } else {
//           uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
//         }
//       }
//       await fs.writeFileSync("./output/westernaustralianinstitutesoffurtherstudies_unique_courselist.json", JSON.stringify(uniqueUrl));

//       for (let i = 0; i < totalCourseList.length; i++) {
//         for (let j = 0; j < uniqueUrl.length; j++) {
//           if (uniqueUrl[j].href == totalCourseList[i].href) {
//             if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

//             } else {
//               uniqueUrl[j].category.push(totalCourseList[i].category);
//             }

//           }
//         }
//       }
//       console.log("totalCourseList -->", uniqueUrl);
//       await fs.writeFileSync("./output/westernaustralianinstitutesoffurtherstudies_courselist.json", JSON.stringify(uniqueUrl));
//       console.log(funcName + 'writing courseList to file completed successfully....');
//       if (s) {
//         await s.close();
//       }
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       throw (error);
//     }
//   }
// } // class

// module.exports = { ScrapeCourseList };

const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
      // await s.setupNewBrowserPage("https://www.tafesa.edu.au/international/international-courses/");
      var mainCategory = [], redirecturl = [];
      //scrape main category
      const maincategoryselectorurl = "//*[@id='nav']/li[3]/ul[@class='sub_nav']/div[@class='width']/..//li/a";
      const maincategoryselector = "//*[@id='nav']/li[3]/ul[@class='sub_nav']/div[@class='width']/..//li";
      var category = await page.$x(maincategoryselector);
      var categoryurl = await page.$x(maincategoryselectorurl);
      console.log("Total categories-->" + category.length);
      for (let i = 0; i < category.length; i++) {
        var categorystringmain = await page.evaluate(el => el.textContent, category[i]);
        var categorystringmainurl = await page.evaluate(el => el.href, categoryurl[i]);
        mainCategory.push(categorystringmain.trim());
        redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl });
      }
     
      await fs.writeFileSync("./output/melbournecitycollege_original_courselist.json", JSON.stringify(redirecturl));
     
     
      await fs.writeFileSync("./output/melbournecitycollege_unique_courselist.json", JSON.stringify(redirecturl));
     
      await fs.writeFileSync("./output/melbournecitycollege_courselist.json", JSON.stringify(redirecturl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  // refers to https://programsandcourses.anu.edu.au/catalogue
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class
module.exports = { ScrapeCourseList };