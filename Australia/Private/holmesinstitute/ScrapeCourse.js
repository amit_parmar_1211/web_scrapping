const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {

        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            console.log("ok----->", course_name)
            const resJsonData = {};
            var demoarray = [];
            const intakefees = JSON.parse(fs.readFileSync(appConfigs.fees_intakes_caregary));
            console.log("intakefees-------->>>>", intakefees);
            let tmpvar = intakefees.filter(val => {
                return val.Course_Name == course_name
            });
            console.log("tmpvar------->>>>", tmpvar);
            console.log("programcode------->>>>", tmpvar[0].Course_Name);
            const Course_Name_title = tmpvar[0].Course_Name
            console.log("Course_Name_title------->>>>", Course_Name_title);
            resJsonData.course_title = format_functions.titleCase(Course_Name_title);
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            const cTitle = tmpvar[0].Course_code;
                            const cTitle111 = cTitle
                            console.log("cTitle------>>>>>>", cTitle111);
                            const category_val = await format_functions.getMyCategory(cTitle111.replace('CRICOS Course Code:', '').trim());
                            console.log("category_val------>>>>>>", category_val);
                            resJsonData.course_discipline = [category_val.split('-')[1].trim()];
                            console.log("resJsonData.course_discipline------>>>>>>", resJsonData.course_discipline);
                            break;
                        }

                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {};
                            var ielts_req = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_req);
                            console.log("ielts_req", ielts_req);

                            if (ielts_req) {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ielts_req;
                                ieltsDict.min = 0;
                                ieltsDict.require = await utils.giveMeNumber(ielts_req.replace(/ /g, ' '));;
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);
                            }
                            else {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = "";
                                ieltsDict.min = 0;
                                ieltsDict.require = ""
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);
                            }

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }
                            console.log("English--->", englishList)

                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];
                            courseAdminReq.english_requirements_url = "";
                            courseAdminReq.academic_requirements_url = "";
                            courseAdminReq.entry_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            for (let sel of selList) {
                                                sel = sel.replace(/[\r\n\t ]+/g, ' ').trim();
                                                if (sel.length > 0) {
                                                    console.log("sel----->", sel);
                                                    course_career_outcome.push(sel)
                                                    break;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (!String(course_career_outcome).includes('[]')) {
                                resJsonData.course_career_outcome = course_career_outcome
                            } else {
                                course_career_outcome = [];
                            }


                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            console.log("programcode------->>>>", tmpvar[0].Duration);
                            const duration_val = tmpvar[0].Duration
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            if (duration_val && duration_val.length > 0) {
                                const resFulltime = duration_val;
                                if (resFulltime) {
                                    // durationFullTime.duration_full_time = resFulltime;
                                    // courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    // if (courseDurationList && courseDurationList.length > 0) {
                                    resJsonData.course_duration = resFulltime;
                                    //  }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':

                        case 'course_campus_location': { // Location Launceston
                            let newCampusses = [];
                            let campLocationText;
                            console.log("Location------->>>>", tmpvar[0].Location);
                            const courseKeyVal = tmpvar[0].Location;
                            let course_campus_location = [];
                            console.log("Course_code------->>>>", tmpvar[0].Course_code);

                            const cTitle = tmpvar[0].Course_code;
                            let CRICOS = cTitle;
                            console.log("resJsonData.sssss------>>>>>>", CRICOS);
                            course_campus_location.push(courseKeyVal);
                            console.log("course_campus_location----->>>>", course_campus_location);


                            if (course_campus_location && course_campus_location.length > 0) {
                                var campLocationValTrimmed = String(course_campus_location).trim();
                                console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                const intakeSplita = String(campLocationValTrimmed).split(',');
                                console.log("intakeSplita------>>>>>>", intakeSplita);
                                if (intakeSplita && intakeSplita.length > 0) {
                                    // resJsonData.course_campus_location = await utils.giveMeArray(campLocationValTrimmed, ',');
                                    //   var campuss = await utils.giveMeArray(campLocationValTrimmed, ',');

                                    //let campuses= ['West Melbourne','North Melbourne','Melbourne']

                                    let campuses = ["Melbourne", "Sydney", "Brisbane", "Gold Coast", "Hong Kong"]
                                    campuses.forEach(element => {
                                        if (campLocationValTrimmed.includes(element)) {
                                            newCampusses.push(element);
                                            console.log("YESSSS location is there")

                                        }

                                    });
                                    console.log("newCampusses --> ", newCampusses);
                                    var campusedata = [];
                                    newCampusses.forEach(element => {
                                        campusedata.push({
                                            "name": element.trim(),
                                            "code": CRICOS.trim()

                                        })
                                    });

                                    resJsonData.course_campus_location = campusedata;
                                    // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                    var study_mode = [];
                                    if (campLocationValTrimmed.trim().length > 0) {
                                        study_mode.push('On campus');
                                    }
                                    resJsonData.course_study_mode = 'On campus';//.join(',');
                                    console.log("## FInal string-->" + campLocationValTrimmed);
                                }
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_study_mode': { // Location Launceston
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':


                        case 'course_tuition_fees_international_student': {
                            const courseTuitionFee = {};

                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };

                            console.log("demoarray123 -->", demoarray);
                            console.log("programcode------->>>>", tmpvar[0].Tuition_Fee);
                            const courseKeyVal_text = tmpvar[0].Tuition_Fee;


                            let first_val, second_val;
                            if (courseKeyVal_text.includes('\n')) {
                                console.log("courseKeyVal_text -->", courseKeyVal_text);
                                let split_val = courseKeyVal_text.split('\n');
                                first_val = split_val[0];
                                second_val = split_val[1];
                                let replace_first, replace_second, one, two;
                                let final_first, final_second = null;
                                if (first_val.includes('Full Fee')) {
                                    replace_first = first_val.replace('(Full Fee)', '').replace('$', '').trim();
                                    one = parseInt(replace_first.replace(/,/g, ''), 10)
                                    console.log("replace_first -->", replace_first);
                                }
                                if (first_val.includes('Full Course')) {
                                    replace_first = first_val.replace('(Full Course)', '').replace('$', '').trim();
                                    one = parseInt(replace_first.replace(/,/g, ''), 10)
                                    console.log("replace_first -->", replace_first);
                                }
                                if (second_val.includes('Weeks')) {
                                    replace_second = second_val.split('(')[0].replace('$', '').trim();
                                    two = parseInt(replace_second.replace(/,/g, ''), 10)
                                    console.log("replace_second -->", replace_second);
                                }
                                final_first = one;
                                final_second = two;
                                console.log("final_first -->", final_first);
                                console.log("final_second -->", final_second);
                                console.log("first_val -->", first_val);
                                console.log("second_val -->", second_val);
                                if (first_val) {
                                    feesDict.international_student = ({
                                        amount: final_first,
                                        duration: 1,
                                        unit: "trimester",
                                        description: first_val,
                                    });
                                }
                                if (second_val) {
                                    feesDict.international_student = ({
                                        amount: Number(final_second),
                                        duration: 1,
                                        unit: "trimester",
                                        description: second_val,
                                    });
                                }
                            } else if (courseKeyVal_text && courseKeyVal_text.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(courseKeyVal_text).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "trimester",
                                                description: courseKeyVal_text,
                                            });
                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: 1,
                                                unit: "trimester",
                                                description: "not available fee",
                                            });
                                        }
                                        console.log("feesDictinternational_student-->", feesDict.international_student);
                                    }
                                }
                            } else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: 1,
                                    unit: "trimester",
                                    description: "not available fee",
                                });
                            }
                            console.log("international_student -->", feesDict.international_student);
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            if (feeYear && feeYear.length > 0) {
                                courseTuitionFee.year = feeYear;
                            }

                            // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = Number(inetStudentFees);
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if


                            //   let international_student_all_fees_array = []
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                // feesDict.international_student_all_fees = [];
                                // if (international_student_all_fees_array && international_student_all_fees_array.length > 0) {

                                //     for (let student_all_fees of international_student_all_fees_array) {
                                //         feesDict.international_student_all_fees.push(student_all_fees.replace(/[\r\n\t ]+/g, ' ').replace('-', '').trim());
                                //     }
                                // }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;

                                }
                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }
                            break;
                        }

                        case 'program_code': {
                            const program = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            console.log("program------->>>>", program);
                            // let split_pro = program.split(' ')[0].trim();
                            //console.log("split_pro------->>>>", split_pro);
                            // if (split_pro) {
                            //     resJsonData.program_code = split_pro;
                            // } else {
                            resJsonData.program_code = "";
                            //  }
                            break;
                        }

                        case 'course_intake': { // January, April, June, September,
                            const courseIntakeDisplay = [];
                            // let intake_months = [
                            //     {
                            //         key: "January",
                            //         value: "2019-01-31"
                            //     },
                            //     {
                            //         key: "April",
                            //         value: "2019-04-30"
                            //     },
                            //     {
                            //         key: "June",
                            //         value: "2019-06-30"
                            //     },
                            //     {
                            //         key: "September",
                            //         value: "2019-09-30"
                            //     }
                            // ]
                            let courseKeyVal_intake = tmpvar[0].Intake_Dates;
                            console.log("courseKeyVal_intake", courseKeyVal_intake);
                            console.log("R locationArray", campus);
                            console.log('************************Start course_intake******************************');
                            console.log('courseIntakeStr : ' + courseKeyVal_intake);

                            if (courseKeyVal_intake && courseKeyVal_intake.length > 0) {
                                resJsonData.course_intake = courseKeyVal_intake;
                                const intakeStrList = String(courseKeyVal_intake).split('\n');
                                const intakeSplit = String(intakeStrList).split(',');
                                console.log('course_intake intakeStrList = ' + JSON.stringify(intakeSplit));
                                if (intakeSplit && intakeSplit.length > 0) {
                                    for (var part of intakeSplit) {
                                        console.log("R part", part);
                                        part = part.trim();
                                        courseIntakeDisplay.push(part);
                                    }
                                }
                                console.log('course_intake courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));
                            }
                            var campus = resJsonData.course_campus_location;
                            if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let location11 of campus) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location11.name,
                                        "value": courseIntakeDisplay
                                    });
                                }
                            }
                            console.log("intakes123 -->", resJsonData.course_intake.intake);
                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            let formatedIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");
                            console.log("Intakes --> ", JSON.stringify(formatedIntake));
                            var intakedata = {};
                            intakedata.intake = formatedIntake;
                            // let valmonths11 = "";
                            // for (let intakeval of formatedIntake) {
                            //     console.log("intakeval--->", intakeval);
                            //     for (let month of intakeval.value) {
                            //         console.log("month--->", month);
                            //         for (valmonths11 of intake_months) {
                            //             console.log("valmonths11--->", valmonths11);
                            //             if (valmonths11.key == month.month) {
                            //                 console.log("month.key--->", valmonths11.key);
                            //                 month.filterdate = valmonths11.value;
                            //                 console.log("intake_months--->", month.filterdate);
                            //                 break;
                            //             }
                            //         }
                            //     }
                            // }
                            intakedata.more_details = more_details;
                            resJsonData.course_intake = intakedata;
                            break;
                        }
                        case 'course_scholarship': {
                            const courseKeyVal = courseScrappedData.course_scholarship;
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                resJsonData.course_scholarship = resScholarshipJson;
                            }
                            break;
                        }
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                        case 'course_accomodation_cost': {
                            const courseKeyVal = courseScrappedData.course_accomodation_cost;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_accomodation_cost = resAccomodationCostJson;
                            }
                            break;
                        }

                        case 'course_country': {

                            resJsonData.course_country = await utils.getValueFromHardCodedJsonFile('univ_country');
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal111 = courseScrappedData.course_overview;
                            // const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview)
                            console.log("overview---->", JSON.stringify(courseScrappedData.course_overview))
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal111)) {
                                for (const rootEleDict of courseKeyVal111) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList overview= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                                console.log("overview===>>>>", course_overview)
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }

                        case 'course_study_level': {
                            //  const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log("programcode------->>>>", tmpvar[0].Course_code);
                            let rep_val = tmpvar[0].Course_code;
                            console.log("replace_cri ------>>>>>>", rep_val);
                            const study_val = await format_functions.getMyStudyLevel(rep_val);
                            resJsonData.course_study_level = study_val;
                            console.log("resJsonData.course_study_level------>>>>>>", resJsonData.course_study_level);
                            break;
                        }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: resJsonData.course_url
                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);

                                resJsonData.application_fee = "";
                                break;
                            }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;

            for (let location of locations) {
                NEWJSONSTRUCT.course_id = Course_Name_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = Course_Name_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                // NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;

                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });

            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            }
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }

            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
