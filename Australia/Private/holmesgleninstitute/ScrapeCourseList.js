const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      const categoryselectorUrl = "//*[@class='courseNavigation-courseInner']/a";
      var elementstring = "", elementhref = "", allcategory = [];
      const targetLinksCardsUrls = await page.$x(categoryselectorUrl);

      var targetLinksCardsTotal = [];
      var totalCourseList = [];
      for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        elementstring = await page.evaluate(el => el.innerText, targetLinksCardsUrls[i]);
        elementhref = await page.evaluate(el => el.href, targetLinksCardsUrls[i]);
        console.log("elementhref -->", elementhref);
        console.log("elementhref -->", elementstring);
        targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
      }

      for (let link of targetLinksCardsTotal) {
        await page.goto(link.href, { timeout: 0 })
        console.log("InnerText", link.href)
        const course_list = "//*[@id='mainContent']//div/section/a";
        const course_list_url = await page.$x(course_list);
        console.log("course_list_url -->", course_list_url.length);
        for (let j = 0; j < course_list_url.length; j++) {
          var course_string = await page.evaluate(el => el.innerText, course_list_url[j]);
          var course_href = await page.evaluate(el => el.href, course_list_url[j]);
          console.log("elementhref -->", course_string);
          console.log("elementhref -->", course_href);
          totalCourseList.push({ href: course_href, innerText: course_string, category: link.innerText })
        }

      }


      await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinksCardsTotal));
      console.log("#### main courses---" + JSON.stringify(targetLinksCardsTotal));
      console.log("totalCourseList -->", totalCourseList);
      await fs.writeFileSync("./output/holmesgleninstitute_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
      await fs.writeFileSync("./output/holmesgleninstitute_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/holmesgleninstitute_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  

} // class

module.exports = { ScrapeCourseList };
