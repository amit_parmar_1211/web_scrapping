const puppeteer = require('puppeteer');
const fs = require("fs");
async function start() {
    console.log(await getMyStudyLevel("098196F"));
    console.log(await getMyStudyLevel("099337A"));
}
start()
async function getMyStudyLevel(code) {
    let studylevelval = '', browser = null;
    try {
        const URL = "http://cricos.education.gov.au/Course/CourseDetails.aspx?CourseCode=";
        browser = await puppeteer.launch({ headless: false }); //headless:false so we can debug
        let page = await browser.newPage();
        await page.goto(URL + code, { timeout: 0 });
        const codeselector = "//*/label[contains(text(),'Course Level:')]/following-sibling::div/span";
        const studylevelelm = await page.$x(codeselector);
        studylevelval = await page.evaluate(el => el.innerText, studylevelelm[0]);
    } catch (error) {
        console.log('try-catch error = ' + error);
    }
    await browser.close();
    return studylevelval;
}