**Holmesglen Institute: Total Courses Details As On 29 April 2019**
* Total Courses = 80
* Courses without CRICOS code = 0
* DDB = 0
* Web portal available courses = 0
* for ielts score mapping is done based on the link given in english language requirement
* for some courses ielts is not given so added in hard_coded file
