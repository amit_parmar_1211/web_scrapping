const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  // async scrapeOnlyInternationalCourseList(selFilepath) {
  //   const funcName = 'scrapeCoursePageList ';
  //   let s = null;
  //   try {
  //     // validate params
  //     Scrape.validateParams([selFilepath]);
  //     // if should read from local file
  //     if (appConfigs.shouldTakeCourseListFromOutputFolder) {
  //       const fileData = fs.readFileSync(configs.opCourseListFilepath);
  //       if (!fileData) {
  //         throw (new Error('Invalif file data, fileData = ' + fileData));
  //       }
  //       const dataJson = JSON.parse(fileData);
  //       console.log(funcName + 'Success in getting local data so returning local data....');
  //       // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
  //       return dataJson;
  //     }
  //     // load file
  //     const fileData = fs.readFileSync(selFilepath);
  //     this.selectorJson = JSON.parse(fileData);
  //     // create Scrape object
  //     s = new Scrape();
  //     await s.init({ headless: true });
  //     // course_level_selector key

  //     const courseListSel = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
  //     if (!courseListSel) {
  //       console.log(funcName + 'Invalid courseListSel');
  //       throw (new Error('Invalid courseListSel'));
  //     }
  //     const elementDictForCourseListSel = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
  //     if (!elementDictForCourseListSel) {
  //       console.log(funcName + 'Invalid elementDictForCourseListSel');
  //       throw (new Error('Invalid elementDictForCourseListSel'));
  //     }
  //     const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
  //     console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
  //     await s.setupNewBrowserPage("https://international.tafeqld.edu.au");
  //     // set page to url
  //     var totalCourseList = [];
  //     var URL = rootEleDictUrl;
  //     for (let pageURL of URL) {
  //       console.log("URL proceed --> " + pageURL);
  //       console.log('---------------------------call selector-------------------------------')
  //       var courses = await s.scrapeElement(elementDictForCourseListSel.elementType, courseListSel, pageURL, null);
  //       var mycourses = [];
  //       courses.forEach(element => {
  //         if (!element.innerText.toLowerCase().includes("english")) {
  //           mycourses.push(element)
  //         }
  //       });
  //       totalCourseList = totalCourseList.concat(mycourses);
  //     }

  //     console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));

  //     console.log(funcName + 'writing courseList to file....');
  //     // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
  //     await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
  //     console.log(funcName + 'writing courseList to file completed successfully....');
  //     // put file in S3 bucket
  //     //const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
  //     //console.log(funcName + 'object location = ' + res);
  //     // destroy scrape resources
  //     if (s) {
  //       await s.close();
  //     }
  //     return totalCourseList;
  //   } catch (error) {
  //     console.log(funcName + 'try-catch error = ' + error);
  //     // destroy scrape resources
  //     if (s) {
  //       await s.close();
  //     }
  //     throw (error);
  //   }
  // }

   async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      // course_level_selector key

      const courseListSel = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!courseListSel) {
        console.log(funcName + 'Invalid courseListSel');
        throw (new Error('Invalid courseListSel'));
      }
      const elementDictForCourseListSel = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCourseListSel) {
        console.log(funcName + 'Invalid elementDictForCourseListSel');
        throw (new Error('Invalid elementDictForCourseListSel'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);

      console.log('---------------------------call selector-------------------------------')
      
      // anchor list selector of course list
      const totalCourseList = await s.scrapeElement(elementDictForCourseListSel.elementType, courseListSel, rootEleDictUrl, null);
      console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));

      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      //const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      //console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return totalCourseList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }

  // refers to https://programsandcourses.anu.edu.au/catalogue
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // course_level_form_selector key
      // const formSelList = await utils.getSelectorsListForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelList) {
      //   console.log(funcName + 'Invalid formSelList');
      //   throw (new Error('Invalid formSelList'));
      // }
      // const formSelElementDict = await utils.getElementDictForKey('course_level_form_selector', this.selectorJson);
      // if (!formSelElementDict) {
      //   console.log(funcName + 'Invalid formSelElementDict');
      //   throw (new Error('Invalid formSelElementDict'));
      // }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
