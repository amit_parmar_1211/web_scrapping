


const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      var datalist = [];
      var location1;
      var location2;
      var location3;
      var cricosurl2;
      var cricosurl3 
      var cricosurl4;
     
      var mainCategory = [], redirecturl = [];
      //scrape main category
      const maincategoryselectorurl = "//*[@class='col-lg-3 col-lg-offset-1 col-md-3 fltr_crs_dtls']//..//preceding::h3[not(contains(text(),'Bachelor of Health Science (Chinese Medicine)'))]/following::a[1]";
      const maincategoryselector = "//*[@class='fltr_rslt_itms mix health nursing sydney brisbane melbourne sydney adelaide']//following::h3[not(contains(text(),'Bachelor of Health Science (Chinese Medicine)'))]";
      const locationselector = "//*//h4[contains(text(),'CAMPUS')]/../preceding::h3[not(contains(text(),'Bachelor of Health Science (Chinese Medicine)'))]/following::div[@class='col-md-2 col-sm-3'][1]"
      const cricosselector = "//*[@class='fltr-course-code']/..//h3[not(contains(text(),'Bachelor of Health Science (Chinese Medicine)'))]/following::span[1]"
      const durationselector = "//*//h4[contains(text(),'DURATION')]/..//preceding::h3[not(contains(text(),'Bachelor of Health Science (Chinese Medicine)'))]/following::span[1]/..//h4[contains(text(),'DURATION')]/../p[1]"
      const overviewselector = "//*[@class='col-lg-8 col-md-9 fltr-description']/../preceding::h3[not(contains(text(),'Bachelor of Health Science (Chinese Medicine)'))]/following::span[1]/..//div[@class='col-lg-8 col-md-9 fltr-description']"
      var category = await page.$x(maincategoryselector);
      var categoryurl = await page.$x(maincategoryselectorurl);
      var location = await page.$x(locationselector);
      var cricos = await page.$x(cricosselector);
      var duration = await page.$x(durationselector);
      var overview = await page.$x(overviewselector);
      console.log("Total categories-->" + category.length);
      for (let i = 0; i < category.length; i++) {
        var categorystringmain = await page.evaluate(el => el.textContent, category[i]);
        var categorystringmainurl = await page.evaluate(el => el.href, categoryurl[i]);
        var locationurl = await page.evaluate(el => el.innerText, location[i]);
        var cricosurl = await page.evaluate(el => el.innerText, cricos[i]);
        var durationurl = await page.evaluate(el => el.innerText, duration[i]);
        var overviewurl = await page.evaluate(el => el.innerText, overview[i]);
        console.log("overviewurl------->",overviewurl);
        var duration1 = durationurl.replace("DURATION\n\n",'').replace('\n\n',' ');
        console.log("locat-->", duration1);
        if (cricosurl.includes("Course Code: HLT54115; CRICOS Code:")) {
          cricosurl3 = cricosurl.replace('Course Code: HLT54115; CRICOS Code:', '').trim();
          cricosurl4 = cricosurl3.replace('(','').replace(')','').trim();
          console.log("cricoslocation", cricosurl4);

        }
        if (cricosurl.includes("CRICOS CODE:")) {
          cricosurl3 = cricosurl.replace("CRICOS CODE:", '').trim();
          cricosurl4 = cricosurl3.replace('(','').replace(')','').trim();
          console.log("cricossssssssssss-->", cricosurl4);
        }

        if (locationurl.includes("\n\n")) {
          location1 = locationurl.toString().split('\n\n');
          location2 = String(location1).replace('CAMPUS,', '').replace(',Online', '').trim();
        
        }

  
        mainCategory.push(categorystringmain.trim());
        redirecturl.push({ innerText: categorystringmain.trim(), href: categorystringmainurl, location: location2, CRICOS: cricosurl4, DURATION: duration1, OVERVIEW: overviewurl});
      }
     

      await fs.writeFileSync("./output/thinkeducation_original_courselist.json", JSON.stringify(redirecturl));
    
      await fs.writeFileSync("./output/thinkeducation_unique_courselist.json", JSON.stringify(redirecturl));
      
      console.log("totalCourseList -->", redirecturl);
      await fs.writeFileSync("./output/thinkeducation_courselist.json", JSON.stringify(redirecturl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  // refers to https://programsandcourses.anu.edu.au/catalogue
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));
      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
     
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class
module.exports = { ScrapeCourseList };