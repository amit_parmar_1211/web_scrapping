const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name, location, DURATION, OVERVIEW, CRICOS) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {


                            const cTitle = CRICOS;

                            let DEESCIPLINE = await format_functions.getDISCIPLINE(cTitle);
                            console.log("course_descipline-------", DEESCIPLINE);
                            var DEESCIPLINE1 = DEESCIPLINE.split("-")[1].trim();
                            resJsonData.course_discipline = [DEESCIPLINE1]



                        }
                        case 'course_title_category':
                        case 'course_title': {
                            var title = course_name;

                            var ctitle = format_functions.titleCase(title).trim();
                            console.log("title<<<<<<<<<<<", ctitle);
                            resJsonData.course_title = ctitle
                            break;
                        }
                        // case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            let englishList = [];
                            let ieltsDict = {};
                            let pteDict = {};
                            let tofelDict = {};
                            let caeDict = {};
                            let ieltsNumber = null;
                            // english requirement
                            const regEx = /[+-]?\d+(\.\d+)?/g;
                            var titleielts = course_name;
                            console.log("titleielts-00-00-9-9->", titleielts);
                            if (titleielts.includes('Diploma of Applied Social Science') || (titleielts.includes('Bachelor of Applied Social Science (Counselling)'))) {
                                let courseKeyValIelts1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score2);
                                console.log("courseKeyValIelts1-->", courseKeyValIelts1);
                                ieltsNumber = courseKeyValIelts1;
                                if (courseKeyValIelts1 && courseKeyValIelts1.length > 0) {
                                    if (ieltsNumber.length > 0) {
                                        ieltsDict.name = 'ielts academic';
                                        ieltsDict.description = ieltsNumber;
                                        ieltsDict.min = 0;
                                        ieltsDict.require = await utils.giveMeNumber(ieltsNumber);
                                        ieltsDict.max = 9;
                                        ieltsDict.R = 0;
                                        ieltsDict.W = 0;
                                        ieltsDict.S = 0;
                                        ieltsDict.L = 0;
                                        ieltsDict.O = 0;

                                        englishList.push(ieltsDict);

                                        console.log("HGHFHDC--->", englishList);

                                    }
                                    console.log("ighrghggkj--->", englishList);
                                    //ieltsDic = ieltsDict;

                                }
                                var academicReq = null;
                                const courseKeyVal = courseScrappedData.course_academic_requirement2;
                                console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                // console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                // if (Array.isArray(selList) && selList.length > 0) {
                                                academicReq = selList;
                                                //}
                                            }
                                        }
                                    }
                                }
                                console.log("academicReq--------->???", academicReq)
                                courseAdminReq.academic = [];
                                for (let academicvalue of academicReq) {
                                    courseAdminReq.academic = academicvalue.replace(/[\r\n\t ]+/g, ' ').trim();
                                }
                                // courseAdminReq.academic = (academicReq) ? academicReq.toString() : [];
                                console.log("Academic requirement" + JSON.stringify(courseAdminReq.academic));

                                //  courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];

                                courseAdminReq.english_requirements_url = "http://www.jni.edu.au/apply-online/admissions-criteria";
                                courseAdminReq.entry_requirements_url = "http://www.jni.edu.au/apply-online/admissions-criteria";
                                courseAdminReq.academic_requirements_url = "http://www.jni.edu.au/apply-online/admissions-criteria";



                                //courseAdminReq.academic_more_details = academic_more;
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }
                            if (titleielts.includes('DIPLOMA OF NURSING')) {
                                let courseKeyValIelts1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score1);
                                console.log("courseKeyValIelts14646-->", courseKeyValIelts1);
                                ieltsNumber = courseKeyValIelts1;
                                if (courseKeyValIelts1 && courseKeyValIelts1.length > 0) {
                                    if (ieltsNumber.length > 0) {
                                        ieltsDict.name = 'ielts academic';
                                        ieltsDict.description = ieltsNumber;
                                        ieltsDict.min = 0;
                                        ieltsDict.require = await utils.giveMeNumber(ieltsNumber);
                                        ieltsDict.max = 9;
                                        ieltsDict.R = 0;
                                        ieltsDict.W = 0;
                                        ieltsDict.S = 0;
                                        ieltsDict.L = 0;
                                        ieltsDict.O = 0;

                                        englishList.push(ieltsDict);

                                        console.log("ighrghggkj--->", englishList);

                                    }
                                    console.log("ighrghggkj--->", englishList);
                                    //ieltsDic = ieltsDict;

                                }
                                var academicReq = null;
                                const courseKeyVal = courseScrappedData.course_academic_requirement1;
                                console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                // console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                // if (Array.isArray(selList) && selList.length > 0) {
                                                academicReq = selList;
                                                //}
                                            }
                                        }
                                    }
                                }
                                console.log("academicReq--------->???", academicReq)
                                courseAdminReq.academic = [];
                                for (let academicvalue of academicReq) {
                                    courseAdminReq.academic = academicvalue.replace(/[\r\n\t ]+/g, ' ').trim();
                                }
                                // courseAdminReq.academic = (academicReq) ? academicReq.toString() : [];
                                console.log("Academic requirement" + JSON.stringify(courseAdminReq.academic));

                                //  courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];

                                courseAdminReq.english_requirements_url = "";
                                courseAdminReq.entry_requirements_url = "";
                                courseAdminReq.academic_requirements_url = "";



                                //courseAdminReq.academic_more_details = academic_more;
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }
                            if (courseScrappedData.course_toefl_ielts_score) {
                                let ieltsScore = null;
                                let courseKeyValIelts = courseScrappedData.course_toefl_ielts_score;
                                //const ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                                if (Array.isArray(courseKeyValIelts)) {
                                    for (const rootEleDict of courseKeyValIelts) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    ieltsScore = selList;
                                                }
                                            }
                                        }
                                    }
                                } // if (Array.isArray(courseKeyVal))

                                console.log("ieltsScore ->", ieltsScore);
                                if (ieltsScore && ieltsScore.length > 0) {
                                    if (ieltsScore.length == 2) {
                                        ieltsDict.name = 'ielts academic (PhD)';
                                        ieltsDict.description = ieltsScore[0];
                                        ieltsDict.min = 0;
                                        ieltsDict.require = await utils.giveMeNumber(ieltsScore[0]);
                                        ieltsDict.max = 9;
                                        ieltsDict.R = 0;
                                        ieltsDict.W = 0;
                                        ieltsDict.S = 0;
                                        ieltsDict.L = 0;
                                        ieltsDict.O = 0;

                                        englishList.push(ieltsDict);

                                        ieltsDict = {};
                                        ieltsDict.name = 'ielts academic (M.Phil)';
                                        ieltsDict.description = ieltsScore[1];
                                        ieltsDict.min = 0;
                                        ieltsDict.require = await utils.giveMeNumber(ieltsScore[1]);
                                        ieltsDict.max = 9;
                                        ieltsDict.R = 0;
                                        ieltsDict.W = 0;
                                        ieltsDict.S = 0;
                                        ieltsDict.L = 0;
                                        ieltsDict.O = 0;

                                        englishList.push(ieltsDict);

                                    } else {
                                        console.log("else part ielts-->");
                                        ieltsDict.name = 'ielts academic';
                                        ieltsDict.description = ieltsScore[0];
                                        ieltsDict.min = 0;
                                        ieltsDict.require = await utils.giveMeNumber(ieltsScore[0]);
                                        ieltsDict.max = 9;
                                        ieltsDict.R = 0;
                                        ieltsDict.W = 0;
                                        ieltsDict.S = 0;
                                        ieltsDict.L = 0;
                                        ieltsDict.O = 0;
                                        englishList.push(ieltsDict);

                                    }

                                    //ieltsDic = ieltsDict;

                                }
                            }


                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }

                            // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = academicReq.toString().replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, ' ');
                                }
                            }

                            // add english requirement 'english_more_details' link
                            const courseKeyVal = courseScrappedData.course_admission_english_more_details;
                            let resEnglishReqMoreDetailsJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resEnglishReqMoreDetailsJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            let penglishList = [];
                            let pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            // extract exact number from string              
                            let matchedStrList_ielts, matchedStrList_pte, matchedStrList_tofel, matchedStrList_cae;
                            let ieltsNo, pteNo, ibtNo, caeNo;
                            if (ieltsDict) {
                                matchedStrList_ielts = String(ieltsDict.description).match(regEx);
                            }


                            if (matchedStrList_ielts && matchedStrList_ielts.length > 0) {
                                ieltsNo = Number(matchedStrList_ielts[0]);
                            }

                            if (ieltsNumber && ieltsNumber > 0) {
                                if (ieltsNo) {
                                    pieltsDict.name = 'ielts academic';
                                    pieltsDict.value = {};
                                    pieltsDict.value.min = 0;
                                    pieltsDict.value.require = ieltsNo;
                                    pieltsDict.value.max = 9;
                                    pieltsDict.value.R = 0;
                                    pieltsDict.value.W = 0;
                                    pieltsDict.value.S = 0;
                                    pieltsDict.value.L = 0;
                                    pieltsDict.value.O = 0;
                                    penglishList.push(pieltsDict);
                                }



                            }
                            // if courseAdminReq has any valid value then only assign it
                            resJsonData.course_admission_requirement = {};
                            if (courseAdminReq.english && courseAdminReq.english.length > 0) {
                                resJsonData.course_admission_requirement.english = courseAdminReq.english;
                            } else {
                                throw new Error("IELTS not found");
                                resJsonData.course_admission_requirement.english = [];
                            }
                            if (courseAdminReq.academic && courseAdminReq.academic.length > 0) {
                                resJsonData.course_admission_requirement.academic = [courseAdminReq.academic];
                            } else {
                                resJsonData.course_admission_requirement.academic = [];
                            }
                            // add english requirement 'english_more_details' link
                            const entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            const academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            const english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            if (entry_requirements_url && entry_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            }
                            if (academic_requirements_url && academic_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;
                            }
                            if (english_requirements_url && english_requirements_url.length > 0) {
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                            }

                            break;
                        }

                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }

                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            fullTimeText = DURATION;
                            var durTitle = course_name;
                            console.log("1111111--?", durTitle);
                            if (durTitle.includes('Bachelor of Health Science (Clinical Myotherapy)')) {
                                console.log("22222--->");
                                var durationtitle = fullTimeText.split("Full Time:")[1].trim();
                                var durationtitle3 = durationtitle + " full-time"
                                //  var durationtitle1 = durationtitle.split("includes")[0].trim();
                                console.log("durationtitle1durationtitle1durationtitle1-->", durationtitle3);
                                if (durationtitle3 && durationtitle3.length > 0) {
                                    var resFulltime = durationtitle;
                                    if (resFulltime) {
                                        durationFullTime.duration_full_time = resFulltime;

                                        courseDurationList.push(durationFullTime);
                                        courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                        let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);

                                        if (courseDurationList && courseDurationList.length > 0) {

                                            resJsonData.course_duration = resFulltime;

                                        }
                                        if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                            resJsonData.course_duration_display = filtered_duration_formated;

                                            var isfulltime = false, isparttime = false;
                                            filtered_duration_formated.forEach(element => {
                                                if (element.display == "Full-Time") {
                                                    isfulltime = true;
                                                }
                                                if (element.display == "Part-Time") {
                                                    isparttime = true;
                                                }
                                            });
                                            resJsonData.isfulltime = isfulltime;
                                            resJsonData.isparttime = isparttime;

                                        }
                                    }
                                }
                                break;
                            }
                            if (durTitle.includes('DIPLOMA OF NURSING')) {
                                var durationtitle = fullTimeText.split("Full Time:")[1].trim();
                                var durationtitle1 = durationtitle.split("includes")[0].trim();
                                console.log("durationtitle1durationtitle1durationtitle1-->", durationtitle1);
                                if (durationtitle1 && durationtitle1.length > 0) {
                                    var resFulltime = durationtitle1;
                                    if (resFulltime) {
                                        durationFullTime.duration_full_time = resFulltime;

                                        courseDurationList.push(durationFullTime);
                                        courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                        let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);

                                        if (courseDurationList && courseDurationList.length > 0) {

                                            resJsonData.course_duration = resFulltime;

                                        }
                                        if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                            resJsonData.course_duration_display = filtered_duration_formated;

                                            var isfulltime = false, isparttime = false;
                                            filtered_duration_formated.forEach(element => {
                                                if (element.display == "Full-Time") {
                                                    isfulltime = true;
                                                }
                                                if (element.display == "Part-Time") {
                                                    isparttime = true;
                                                }
                                            });
                                            resJsonData.isfulltime = isfulltime;
                                            resJsonData.isparttime = isparttime;

                                        }
                                    }
                                }
                                break;
                            }
                            console.log("fullTimeTextfullTimeTextfullTimeTextfullTimeText-->", fullTimeText);
                            var fullTimeText4 = fullTimeText.split("Full Time:")[1].trim();
                            var fullTimeText5 = fullTimeText4 + " full-time";
                            var fullTimeText2 = fullTimeText5 + " or equivalent part-time";
                            if (fullTimeText2 && fullTimeText2.length > 0) {
                                var resFulltime = fullTimeText2;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;

                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);

                                    if (courseDurationList && courseDurationList.length > 0) {

                                        resJsonData.course_duration = resFulltime;

                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;

                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;

                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':

                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':


                        case 'course_campus_location': { // Location Launceston
                            var campLocationText = [];
                            var campus1, Cri;
                            var aray, final = [] //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            // const courseKeyVal = courseScrappedData.course_campus_location;
                            let location_text = location;

                            let cricosnew = CRICOS;
                            console.log("cricodsfdfssnew--->", cricosnew);

                            //  console.log("Cricos----->", Cricos)
                            if (cricosnew.includes("CRICOS CODE:")) {
                                Cri = cricosnew.split("CRICOS CODE:")[1]

                            }
                            else {
                                Cri = cricosnew;
                            }
                            console.log("caddfddffmpus--->", location_text);
                            if (String(location_text.includes(','))) {
                                campus1 = String(location_text).split(",");
                                console.log("campus1-------->>.>", campus1);
                                final = campus1;
                            }

                            let newcampus = [];
                            let replace_val;
                            for (let campus of final) {
                                if (!newcampus.includes(campus)) {
                                    replace_val = String(campus).replace(/Vic/g, '').trim();
                                    if (!replace_val == '') {
                                        console.log("##123456-->" + replace_val)
                                        const location_hardcode = await utils.getValueFromHardCodedJsonFile('campuses');
                                        console.log("location_hardcode-------->>>>", location_hardcode);
                                        let tmpvar123 = location_hardcode.filter(val => {
                                            return val.name.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase() == replace_val.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase()
                                        });
                                        console.log("##tmpvar123-->" + JSON.stringify(tmpvar123[0].Location))
                                        let loc_val = tmpvar123[0].Location;
                                        console.log("##Campuscampus-->" + loc_val)
                                        newcampus.push(loc_val);
                                    }
                                }
                            }
                            if (newcampus && newcampus.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                resJsonData.course_study_mode = "On campus";
                                //console.log(funcName + 'campLocationValTrimmed = ' + campLocationValTrimmed);
                                // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                                console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                var campusedata = [];
                                newcampus.forEach(element => {
                                    campusedata.push({
                                        "name": element,
                                        "code": Cri.trim()

                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                var study_mode = [];
                                // campLocationValTrimmed = campLocationValTrimmed.toLowerCase();

                                // campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                                if (campLocationValTrimmed.trim().length > 0) {
                                    study_mode.push('On campus');
                                }
                                resJsonData.course_study_mode = study_mode.toString();//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                                // }
                            } // if (campLocationText && campLocationText.length > 0)
                            // }
                            // console.log("final-------->>.>", final);
                            // for (let loca of final) {

                            //     console.log("yhirtghirfvgikvnhvfnbhoivnfbhcoikbhnvgjfbj-->", loca);
                            //     campLocationText.push({
                            //         "name": String(loca).trim(),
                            //         "code": Cri

                            //     });
                            // }

                            // console.log("campLocationText-------->>.>", campLocationText);



                            // if (campLocationText && campLocationText.length > 0) {
                            //     var campLocationValTrimmed = campLocationText;
                            //     console.log("campLocationValTrimmed--->", campLocationValTrimmed);
                            //     // if (campLocationValTrimmed && campLocationValTrimmed.length > 0) {
                            //     resJsonData.course_campus_location = location;
                            //     console.log("locationnn-->", resJsonData.course_campus_location);
                            //     //await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                            //     // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                            //     var study_mode = "";
                            //     //campLocationValTrimmed = campLocationValTrimmed.toLowerCase();

                            //     //campLocationValTrimmed = campLocationValTrimmed.replace(/,/g, '');
                            //     if (campLocationValTrimmed.length > 0) {
                            //         study_mode = "On campus";
                            //     }
                            //     ;//.join(',');
                            //     console.log("## FInal string-->" + JSON.stringify(campLocationValTrimmed));
                            //     resJsonData.course_campus_location = campLocationValTrimmed;
                            //     resJsonData.course_study_mode = 'On campus';
                            //     // }
                            // } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        // case 'course_cricos_code': {
                        //     let cricosnew = CRICOS;
                        //     console.log("cricosnew--->", cricosnew);


                        //     if (cricosnew) {
                        //         //  global.cricos_code = course_cricos_code[0].split(":")[1];
                        //         var locations = resJsonData.course_campus_location;
                        //         console.log("newlocated !!", locations);
                        //         var mycodes = [];
                        //         for (let location of locations) {
                        //             mycodes.push({
                        //                 location: location.name, code: cricosnew, iscurrent: false
                        //             });
                        //         }
                        //         console.log("mycodes--->", mycodes);
                        //         resJsonData.course_cricos_code = mycodes;
                        //         console.log("globalll-->", resJsonData.course_cricos_code);
                        //     }
                        //     //resJsonData.course_cricos_code = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                        //     break;
                        // }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            var courseTuitionFee = {};
                            var feesList = [];
                            var feesDict = {};
                            var filedata = await utils.getValueFromHardCodedJsonFile('fees');;
                            var FeesNew = "";
                            var title = course_name;





                            let tmpvar = filedata.filter(val => {
                                return val.name.toLowerCase() == title.toLowerCase();
                            });
                            console.log("kshdshkjdfshhshdskjdddddddd--->", tmpvar);
                            FeesNew = tmpvar[0].fee;

                            var feesIntStudent = FeesNew;

                        }
                            console.log("feesIntStudent--->>>>>>>>>>", feesIntStudent);


                            // const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);

                            if (feesIntStudent) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    if (feesVal != "NA" && feesVal != "N A") {
                                        const regEx = /\d/g;
                                        let feesValNum = feesVal.match(regEx);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            let feesNumber = null;
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }
                                            console.log(funcName + 'feesNumber = ' + feesNumber);
                                            if (Number(feesNumber)) {
                                                var feesint = {};
                                                feesint.amount = Number(feesNumber);
                                                feesint.duration = "1";
                                                feesint.unit = "year";

                                                feesint.description = "$" + FeesNew;

                                                feesDict.international_student = feesint;
                                            }
                                        }
                                    }
                                    else {
                                        var feesint = {};
                                        feesint.amount = 0;
                                        feesint.duration = "1";
                                        feesint.unit = "year";

                                        feesint.description = "$" + FeesNew;

                                        feesDict.international_student = feesint;
                                    }
                                }
                            }
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                    var more_fee = [];
                                    // feesDict.international_student_all_fees = more_fee;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;


                        case 'program_code': {
                            let code = "";
                            resJsonData.program_code = code;
                            break;
                        }
                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };

                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

                            let resScholarshipJson = null;


                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }




                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }

                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)

                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;


                            break;
                        }

                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                resJsonData.application_fee = ""
                                break;
                            }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },

                            const courseIntakeDisplay = {};

                            var courseIntakeStr = [];
                            let Intake = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);


                            console.log("hsfkhffhdfhkshsk--->>", Intake);

                            if (Intake == '') {
                                console.log("its_null---->")
                                let course_intake = await utils.getValueFromHardCodedJsonFile('course_intake');
                                courseIntakeStr = course_intake;
                                if (courseIntakeStr && courseIntakeStr.length > 0) {
                                    var intakes = [];
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        var intakedetail = {};
                                        intakedetail.name = loc.name;
                                        intakedetail.value = courseIntakeStr;
                                        intakes.push(intakedetail);
                                    }
                                    console.log("form------>", intakes);
                                    let formatIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                    console.log("formattintake------>", formatIntake);
                                    var intakedata = {};
                                    var intakeUrl = [];
                                    intakeUrl = await utils.getValueFromHardCodedJsonFile('intake_url');
                                    intakedata.intake = formatIntake;
                                    intakedata.more_details = intakeUrl;

                                    // intakedata.more_details = resJsonData.course_intake_url;
                                    resJsonData.course_intake = intakedata;

                                }
                            }
                            else {
                                console.log("its_else---->")
                                const courseIntakeDisplay = [], intake_array_sub = [];
                                // existing intake value

                                let courseKeyVal_intake = courseScrappedData.course_intake;
                                // let courseKeyVal_intake = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                                let courseintake = [];
                                if (Array.isArray(courseKeyVal_intake)) {
                                    for (const rootEleDict of courseKeyVal_intake) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    for (let intake_text of selList) {
                                                        courseintake = intake_text;
                                                        console.log(funcName + '\n\r courseintake = ' + JSON.stringify(courseintake));
                                                        if (String(courseintake.includes('Next intake is on'))) {
                                                            let replace_inteake = String(courseintake).replace('Next intake is on', '').trim();
                                                            intake_array_sub.push(replace_inteake);
                                                            console.log('intake_array_subintake_array_sub : ', intake_array_sub);
                                                        } else {
                                                            intake_array_sub.push(courseintake);
                                                            console.log('intake_array_sub : ', intake_array_sub);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                console.log("R locationArray", campus);
                                console.log('************************Start course_intake******************************');
                                console.log('courseIntakeStr : ' + intake_array_sub[0]);
                                // let intake_array = [], intake_array_new = [];
                                // for (intake_array of intake_array_sub[0]) {
                                //     intake_array_new = intake_array;
                                //     console.log('intake_array_new--->>>', intake_array_new);
                                // }
                                if (intake_array_sub && intake_array_sub.length > 0) {
                                    resJsonData.course_intake = intake_array_sub;
                                    const intakeStrList = String(intake_array_sub).split('\n');
                                    const intakeSplit = String(intakeStrList).split(',');
                                    console.log('course_intake intakeStrList = ' + JSON.stringify(intakeSplit));
                                    if (intakeSplit && intakeSplit.length > 0) {
                                        for (var part of intakeSplit) {

                                            console.log("R part", part);
                                            part = part.trim();
                                            courseIntakeDisplay.push(part);
                                        } // for
                                    }
                                    console.log('course_intake courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));
                                }
                                var campus = resJsonData.course_campus_location;
                                if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
                                    resJsonData.course_intake = {};
                                    resJsonData.course_intake.intake = [];
                                    for (let location11 of campus) {
                                        resJsonData.course_intake.intake.push({
                                            "name": location11.name,
                                            "value": courseIntakeDisplay
                                        });
                                    }
                                }
                                console.log("intakes123 -->", resJsonData.course_intake.intake);
                                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                let formatedIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");

                                console.log("Intakes --> ", JSON.stringify(formatedIntake));
                                var intakedata = {};
                                intakedata.intake = formatedIntake;
                                intakedata.more_details = more_details;
                                resJsonData.course_intake = intakedata;
                            }
                            break;
                        }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }

                        case 'course_overview': {
                            resJsonData.course_overview = OVERVIEW;
                            console.log("resJsonData.course_overview-->", resJsonData.course_overview);

                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r careerselList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                // course_career_outcome = selList;
                                                for (let selItem of selList) {
                                                    console.log("selItemCareer -->", selItem);
                                                    selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ');
                                                    course_career_outcome.push(selItem.trim())
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }
                        case 'course_study_level':
                            {
                                const cTitle = CRICOS;
                                console.log("studylevelllll----->", cTitle)
                                // var cTitle1 = cTitle.split("CRICOS CODE:")[1].trim();
                                // console.log("cTitle1------>>>>>>>", cTitle1);
                                let cStudyLevel = await format_functions.getMyStudyLevel(cTitle);
                                console.log("course_study_level--------", cStudyLevel);
                                resJsonData.course_study_level = cStudyLevel
                            }
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                // var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                // for (let dintake of intakes) {
                //     if (location == dintake.name) {
                //         matchrec = dintake.value;
                //     }
                // }
                // if (matchrec.length > 0) {
                //     NEWJSONSTRUCT.course_intake = matchrec[0];
                // }
                // else {
                //     NEWJSONSTRUCT.course_intake = [];
                // }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        //  NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;

                //resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;

                //resJsonData.current_course_intake = location_wise_data.course_intake;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
                // const res = await awsUtil.putFileInBucket(filelocation);
                // console.log(funcName + 'S3 object location = ' + res);
            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category, courseDict.location, courseDict.DURATION, courseDict.OVERVIEW, courseDict.CRICOS);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
