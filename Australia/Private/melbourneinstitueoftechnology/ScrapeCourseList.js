const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
//const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'scrapeCourseCategoryList ';
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage("http://www.mit.edu.au/");

      const categoryselectorUrl = "//a[contains(text(),'Courses')]/following-sibling::div//a[contains(text(),'Undergraduate') or contains(text(),'Postgraduate')]/following-sibling::div//a[not(@class='dropdown-toggle')]";
      const categoryselectorText = "//a[contains(text(),'Courses')]/following-sibling::div//a[contains(text(),'Undergraduate') or contains(text(),'Postgraduate')]/following-sibling::div//a[not(@class='dropdown-toggle')]"
      var elementstring = "", elementhref = "", allcategory = [];
      const targetLinksCardsUrls = await s.page.$x(categoryselectorUrl);
      const targetLinksCardsText = await s.page.$x(categoryselectorText);
      var targetLinksCardsTotal = [];
      for (let i = 0; i < targetLinksCardsUrls.length; i++) {
        elementstring = await s.page.evaluate(el => el.innerText, targetLinksCardsText[i]);
        elementhref = await s.page.evaluate(el => el.href, targetLinksCardsUrls[i]);
        targetLinksCardsTotal.push({ href: elementhref, innerText: elementstring });
      }
      await fs.writeFileSync("./output/melbourneinstituteoftechnology_courseList.json", JSON.stringify(targetLinksCardsTotal));

      console.log(funcName + 'writing courseList to file completed successfully....');
      return targetLinksCardsTotal
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
    if (s) {
      await s.close();
    }
    throw (error);
  }




} // class

module.exports = { ScrapeCourseList };
