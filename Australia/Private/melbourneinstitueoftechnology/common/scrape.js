const fs = require('fs');
const puptr = require('puppeteer');
const appConfigs = require('./app-config');
const utils = require('./utils');
const format_functions = require('./format_functions');

class Scrape {
  // init
  async init(puptrOptions) {
    const funcName = 'init ';
    try {
      console.log(funcName + 'puputrOptions = ' + JSON.stringify(puptrOptions));
      if (this.browser) {
        console.log(funcName + 'one this.browser instance already exist so returning...');
        return;
      }
      // set puptr default options
      this.puptrOptions = {};
      //this.puptrOptions.executablePath = '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome';
      //this.puptrOptions.executablePath = 'C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe';
      this.puptrOptions.defaultViewport = { width: 1600, height: 900 };
      this.puptrOptions.headless = true;
      if (puptrOptions != null) {
        console.log(funcName + 'overwriting default puptrOptions...');
        if (puptrOptions.hasOwnProperty('executablePath')) {
          console.log(funcName + 'overwriting puptrOptions.executablePath...');
          this.puptrOptions.executablePath = puptrOptions.executablePath;
        }
        if (puptrOptions.hasOwnProperty('headless')) {
          console.log(funcName + 'overwriting puptrOptions.headless and puptrOptions.slowMo...');
          this.puptrOptions.headless = puptrOptions.headless;
          if (this.puptrOptions.headless === false) { // if running with head mode than only set slo mo
            this.puptrOptions.slowMo = 250;
          }
        }
        if (puptrOptions.slowMo) {
          if (this.puptrOptions.headless === false) { // if running with head mode than only set slo mo
            this.puptrOptions.slowMo = puptrOptions.slowMo;
          }
        }
        if (puptrOptions.timeout) {
          this.puptrOptions.timeout = puptrOptions.timeout;
        }
      }
      console.log(funcName + 'final puptrOptions = ' + JSON.stringify(this.puptrOptions));
      this.browser = await puptr.launch(this.puptrOptions);
      // console.log(funcName + 'this.browser = ' + this.browser);
      return;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // set page url and navigate to it
  async setupNewBrowserPage(pageUrl) {
    const funcName = 'setupBrowserPage ';
    try {
      if (!this.browser) {
        console.log(funcName + 'browser is set to null or invalid value');
        throw (new Error('browser is set to null or invalid value'));
      }
      Scrape.validateParams(Array([pageUrl]));
      if (!this.page) { // if page already exist then do not create new page, just goto url
        console.log(funcName + '---------- this.page is null so CREATING NEW PAGE.... ----------');
        this.page = await this.browser.newPage();
      }
      // if current page url is different than url-to-navigate then only got to url
      // else remain on this current page
      if (this.page.url() === pageUrl) {
        console.log(funcName + '---------- current page and page-to-navigate-to are SAME, so NOT NAVIGATING to new page... ----------');
        console.log(funcName + 'this.page.url() = ' + this.page.url() + ', pageUrl = ' + pageUrl);
      } else {
        console.log(funcName + '---------- current page and page-to-navigate-to are NOT SAME, navigating to NEW PAGE... ----------');
        console.log(funcName + 'this.page.url() = ' + this.page.url() + ', pageUrl = ' + pageUrl);
        // await this.page.goto(pageUrl, configs.puptrPageGotoOptions);
        await this.page.goto(pageUrl,{timeout:0});
      }
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (this.browser) {
        await this.close();
      }
      throw (error);
    }
  }


  // close browser and clean up
  async close() {
    const funcName = 'close ';
    try {
      if (this.browser) {
        await this.browser.close();
        console.log(funcName + 'browser closed successfully.....');
        return;
      }
      return;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // methods
  async scrapeElement(eleType, selector, pageUrl, options) {
    const funcName = 'scrapeElement ';
    try {
      Scrape.validateParams([eleType, selector]);
      switch (eleType) {
        case Scrape.ELEMENT_TYPE.DIV: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.DIV);
          return await this.scrapeDIVElement(selector, pageUrl);
        }
        case Scrape.ELEMENT_TYPE.LI: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.LI);
          return await this.scrapeLIElement(selector, pageUrl);
        }
        case Scrape.ELEMENT_TYPE.ANCHOR_CLICK: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.ANCHOR_CLICK);
          return await this.clickAnchorElement(selector, pageUrl);
        }
        case Scrape.ELEMENT_TYPE.SELECT_OPTION: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.SELECT_OPTION);
          Scrape.validateParams([selector, options]);
          return await this.selectOption(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.INPUT_VALUE: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.INPUT_VALUE);
          Scrape.validateParams([selector, options]);
          return await this.inputValue(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.INPUT_SPAN_INNERTEXT: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.INPUT_SPAN_INNERTEXT);
          Scrape.validateParams([selector, options]);
          return await this.setSpanInnerTextValue(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.FORM_SUBMIT: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.FORM_SUBMIT);
          Scrape.validateParams([selector]);
          return await this.submitForm(selector, pageUrl);
        }
        case Scrape.ELEMENT_TYPE.VALUE: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.VALUE);
          Scrape.validateParams([selector]);
          return await Scrape.scrapeValueElement(selector);
        }
        case Scrape.ELEMENT_TYPE.IMAGE_SRC: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.IMAGE_SRC);
          Scrape.validateParams([selector]);
          return await this.scrapeImageElement(selector, pageUrl);
        }
        case Scrape.ELEMENT_TYPE.CLICK: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.CLICK);
          Scrape.validateParams([selector]);
          return await this.clickElement(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.READ_FROM_FILE: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.READ_FROM_FILE);
          Scrape.validateParams([selector, options]);
          return await Scrape.scrapeReadFromFileElement(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.ANCHOR: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.ANCHOR);
          Scrape.validateParams([selector]);
          return await this.scrapeAnchorElement(selector, pageUrl);
        }
        case Scrape.ELEMENT_TYPE.ANCHORLIST_CLICK_SCRAPE: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.ANCHORLIST_CLICK_SCRAPE);
          Scrape.validateParams([selector, options]);
          return await this.anchorListClickAndScrape(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.PAGE_GO_BACK: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.PAGE_GO_BACK);
          return await this.pageGoBack(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.ANCHOR_CLICK_WHILE_VISIBLE: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.ANCHOR_CLICK_WHILE_VISIBLE);
          return await this.clickAnchorElementWhileVisible(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.SELECT_EACH_OPTION_AND_SCRAPE: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.SELECT_EACH_OPTION_AND_SCRAPE);
          return await this.selectEachOptionAndScrapeValue(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.INPUT_CHECKBOX: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.INPUT_CHECKBOX);
          return await this.inputCheckbox(selector, pageUrl);
        }
        case Scrape.ELEMENT_TYPE.ANCHOR_WITHIN_LI: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.ANCHOR_WITHIN_LI);
          return await this.scrapeAnchorWithinLIElement(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.DIV_PAGE_URL: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.DIV_PAGE_URL);
          return await this.scrapeDIVElementAndReturnWithPageURL(selector, pageUrl);
        }
        case Scrape.ELEMENT_TYPE.DIV_AT_INDEX: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.DIV_AT_INDEX);
          return await this.scrapeDIVElementAtIndex(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.SCRAPE_BY_XPATH: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.SCRAPE_BY_XPATH);
          return await this.scrapeElementByXpath(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.SCRAPE_OUTLINE_DIFF: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.SCRAPE_OUTLINE_DIFF);
          return await this.course_outline_scrape(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.SCRAPE_LIST_DIFF: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.SCRAPE_LIST_DIFF);
          return await this.course_list_scrape(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.REMOVE_ATRR: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.REMOVE_ATRR);
          return await this.removeAttribute(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.SET_ATRR: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.SET_ATRR);
          return await this.setAttribute(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.DOWNLOAD_PDF: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.DOWNLOAD_PDF);
          return await this.downloadPDF(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.SCRAPE_PDF: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.SCRAPE_PDF);
          return await this.scrapePDF(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.READ_FROM_JSON: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.READ_FROM_JSON);
          return await this.readFromFile(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.WAIT: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.WAIT);
          return await this.wait(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.SCRAPE_BY_ACTION: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.SCRAPE_BY_ACTION);
          return await this.scrape_by_action(selector, pageUrl, options);
        }
        case Scrape.ELEMENT_TYPE.SCRAPE_MULTI_ELEMENT: {
          console.log(funcName + 'case = ' + Scrape.ELEMENT_TYPE.SCRAPE_MULTI_ELEMENT);
          return await this.scrape_multi_elements(selector, pageUrl, options);
        }
        default:
          {
            console.log(funcName + 'case did not match so executing default case....');
            return;
          }
      } // switch  
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // throw (error);
    }
  }

  // validate function params
  static validateParams(paramsList) {
    const funcName = 'validateParams ';
    try {
      if (!paramsList) {
        console.log(funcName + 'Invalid value of paramsList');
        throw (new Error('Invalid value of paramsList'));
      }
      const invalidParamsList = [];
      paramsList.forEach((param) => {
        if (!param) {
          invalidParamsList.push(param);
        } // if
      });
      if (invalidParamsList.length > 0) {
        console.log(funcName + 'Invalid value of parameter, invalidParamsList = ' + JSON.stringify(invalidParamsList));
        throw (new Error('Invalid value of parameter, invalidParamsList = ' + JSON.stringify(invalidParamsList)));
      }
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape DIV type element
  async scrapeDIVElementAndReturnWithPageURL(sel, pageUrl) {
    const funcName = 'scrapeDIVElement ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      console.log(funcName + 'elements.length = ' + elements.length);
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      const resList = []; const pageUrlDict = {}; const resDict = {};
      for (const ele of elements) {
        const propHandle = await ele.getProperty('innerText');
        const hrefHandle = await ele.getProperty('href');
        const innerTextVal = await propHandle.jsonValue();
        const href = await hrefHandle.jsonValue();
        const innerTextValTrimmed = String(innerTextVal).trim();
        if (innerTextValTrimmed && innerTextValTrimmed.length > 0) {
          resList.push({ innerText: innerTextValTrimmed, href: href });
        }
      }
      pageUrlDict.url = this.page.url();

      resDict.list = resList;
      resDict.dict = pageUrlDict;

      console.log(funcName + 'resDict = ' + JSON.stringify(resDict));
      return resDict;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape DIV at index element
  async scrapeDIVElementAtIndex(sel, pageUrl, indexStr) {
    const funcName = 'scrapeDIVElementAtIndex ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, indexStr]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, indexStr]));
      }
      const index = Number.parseInt(indexStr, 10);
      if (index < 0) {
        console.log(funcName + 'Invalid index = ' + index);
        throw (new Error('Invalid index = ' + index));
      }

      console.log(funcName + 'waiting for selector = ' + sel);
      await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      console.log(funcName + 'elements.length = ' + elements.length);
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      const resList = [];
      if (!(elements.length >= (index + 1))) {
        console.log(funcName + 'Elements length is less than index, elements.length = ' + elements.length + ' and index = ' + index);
        throw (new Error('Elements length is less than index, elements.length = ' + elements.length + ' and index = ' + index));
      }
      const ele = elements[index];
      console.log(funcName + 'ele ata index = ' + ele + ' @ ' + index);
      if (!ele) {
        console.log(funcName + 'Invalid element at index = ' + index);
        throw (new Error('Invalid element at index = ' + index));
      }
      const propHandle = await ele.getProperty('innerText');
      const innerTextVal = await propHandle.jsonValue();
      const innerTextValTrimmed = String(innerTextVal).trim();
      if (innerTextValTrimmed && innerTextValTrimmed.length > 0) {
        resList.push(innerTextValTrimmed);
      }
      console.log(funcName + 'resList = ' + JSON.stringify(resList));
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // search and scrape element by xpath
  async scrapeElementByXpath(sel, pageUrl, indexStr) {
    const funcName = 'scrapeElementByXpath ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, indexStr]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, indexStr]));
      }
      const index = Number.parseInt(indexStr, 10);
      if (index < 0) {
        console.log(funcName + 'Invalid index = ' + index);
        throw (new Error('Invalid index = ' + index));
      }
      const resList = await format_functions.scrape_by_xpath(this.page, sel);
      console.log(funcName + 'resList for xpath element --->' + JSON.stringify(resList));
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // search and scrape element by diff outline
  async course_outline_scrape(sel, pageUrl, indexStr) {
    const funcName = 'scrapeElementfordiffurl';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, indexStr]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, indexStr]));
      }
      const index = Number.parseInt(indexStr, 10);
      if (index < 0) {
        console.log(funcName + 'Invalid index = ' + index);
        throw (new Error('Invalid index = ' + index));
      }
      const resList = await format_functions.course_outline_scrape(this.page, sel);
      console.log(funcName + 'resList for xpath element --->' + JSON.stringify(resList));
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // search and scrape element by diff list
  async course_list_scrape(sel, pageUrl, indexStr) {
    const funcName = 'scrapeElementfordiffurl';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, indexStr]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, indexStr]));
      }
      const index = Number.parseInt(indexStr, 10);
      if (index < 0) {
        console.log(funcName + 'Invalid index = ' + index);
        throw (new Error('Invalid index = ' + index));
      }
      const resList = await format_functions.course_list_scrape(this.page, sel);
      console.log(funcName + 'resList for xpath element --->' + JSON.stringify(resList));
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  // search and scrape element remove attr
  async removeAttribute(sel, pageUrl, indexStr) {
    const funcName = 'removeAttr';
    console.log(funcName + " ## called-----");
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, indexStr]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, indexStr]));
      }
      const index = Number.parseInt(indexStr, 10);
      if (index < 0) {
        console.log(funcName + 'Invalid index = ' + index);
        throw (new Error('Invalid index = ' + index));
      }
      await this.page.evaluate(sel => {
        console.log("### selector-->" + sel.selector);
        console.log("### selectorRM-->" + sel.removeattr);
        return document.querySelector(sel.selector).removeAttribute(sel.removeattr);
      }, sel);
      console.log("####Attribure removed----->");
      console.log(funcName + 'attribute removed --->');
      return "";
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // search and scrape element remove attr
  async setAttribute(sel, pageUrl, indexStr) {
    const funcName = 'removeAttr';
    console.log(funcName + " ## called-----");
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, indexStr]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, indexStr]));
      }
      const index = Number.parseInt(indexStr, 10);
      if (index < 0) {
        console.log(funcName + 'Invalid index = ' + index);
        throw (new Error('Invalid index = ' + index));
      }
      await this.page.evaluate(sel => {
        console.log("### selector-->" + sel.selector);
        console.log("### selectorSET-->" + sel.removeattr);
        return document.querySelector(sel.selector).setAttribute(sel.removeattr);
      }, sel);
      console.log("####Attribure set----->");
      return "";
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // search and scrape element remove attr
  async downloadPDF(sel, pageUrl, indexStr) {
    const funcName = 'downloadfile';
    console.log(funcName + "## called-----");
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, indexStr]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, indexStr]));
      }
      const index = Number.parseInt(indexStr, 10);
      if (index < 0) {
        console.log(funcName + 'Invalid index = ' + index);
        throw (new Error('Invalid index = ' + index));
      }
      await format_functions.downloadPDF(sel);
      return "";
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // search and scrape element remove attr
  async scrapePDF(sel, pageUrl, indexStr) {
    const funcName = 'Scrape PDF';
    console.log(funcName + "## called-----");
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, indexStr]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, indexStr]));
      }
      const index = Number.parseInt(indexStr, 10);
      if (index < 0) {
        console.log(funcName + 'Invalid index = ' + index);
        throw (new Error('Invalid index = ' + index));
      }
      await format_functions.scrapePDF(sel);
      return "";
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }


  // search and scrape element remove attr
  async scrape_by_action(sel, pageUrl, indexStr) {
    const funcName = 'scrape_by_actionF';
    console.log(funcName + "## called-----");
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, indexStr]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, indexStr]));
      }
      const index = Number.parseInt(indexStr, 10);
      if (index < 0) {
        console.log(funcName + 'Invalid index = ' + index);
        throw (new Error('Invalid index = ' + index));
      }
      const myresponse = await format_functions.action_scrape(this.page, sel);
      if (myresponse) {
        return myresponse;
      }
      return "";
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // search and scrape element remove attr
  async scrape_multi_elements(sel, pageUrl, indexStr) {
    const funcName = 'scrape_multi_elements';
    console.log(funcName + "## called-----");
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, indexStr]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, indexStr]));
      }
      const index = Number.parseInt(indexStr, 10);
      if (index < 0) {
        console.log(funcName + 'Invalid index = ' + index);
        throw (new Error('Invalid index = ' + index));
      }
      const myresponse = await format_functions.multi_selector_scrape(this.page, sel);
      if (myresponse) {
        return myresponse;
      }
      return "";
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // search and scrape element remove attr
  async readFromFile(sel, pageUrl, indexStr) {
    const funcName = 'Scrape PDF';
    console.log(funcName + "## called-----");
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, indexStr]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, indexStr]));
      }
      const index = Number.parseInt(indexStr, 10);
      if (index < 0) {
        console.log(funcName + 'Invalid index = ' + index);
        throw (new Error('Invalid index = ' + index));
      }
      const result = await format_functions.readanyfile(sel);
      return result;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // search and scrape element remove attr
  async wait(sel, pageUrl, indexStr) {
    const funcName = 'Wait';
    console.log(funcName + "## called-----");
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, indexStr]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, indexStr]));
      }
      const index = Number.parseInt(indexStr, 10);
      if (index < 0) {
        console.log(funcName + 'Invalid index = ' + index);
        throw (new Error('Invalid index = ' + index));
      }
      const result = await this.page.waitFor(sel.time);
      return result;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape DIV type element
  async scrapeDIVElement(sel, pageUrl) {
    const funcName = 'scrapeDIVElement ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      console.log(funcName + 'elements.length = ' + elements.length);
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      const resList = [];
      for (const ele of elements) {
        const propHandle = await ele.getProperty('innerText');
        const innerTextVal = await propHandle.jsonValue();
        const innerTextValTrimmed = String(innerTextVal).trim();
        if (innerTextValTrimmed && innerTextValTrimmed.length > 0) {
          resList.push(innerTextValTrimmed);
        }
      }
      console.log(funcName + 'resList = ' + JSON.stringify(resList));
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape anchor within LI
  async scrapeAnchorWithinLIElement(sel, pageUrl, anchorSel) {
    const funcName = 'scrapeAnchorWithinLIElement ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, anchorSel]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, anchorSel]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      // await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      await this.page.waitForSelector(sel, { timeout: 10000, visible: true });
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      const resList = [];
      for (const ele of elements) {
        console.log(funcName + 'ele = ' + ele);
        const aDict = await this.scrapeAnchorElement(anchorSel, pageUrl);
        resList.push(aDict);
      }
      console.log(funcName + 'resList = ' + JSON.stringify(resList));
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape LI type element
  async scrapeLIElement(sel, pageUrl) {
    const funcName = 'scrapeLIElement ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      // await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      await this.page.waitForSelector(sel, { timeout: 10000, visible: true });
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      const resList = [];
      for (const ele of elements) {
        const propHandle = await ele.getProperty('innerText');
        const innerTextVal = await propHandle.jsonValue();
        const innerTextValTrimmed = String(innerTextVal).trim();
        if (innerTextValTrimmed && innerTextValTrimmed.length > 0) {
          resList.push(innerTextValTrimmed);
        }
      }
      console.log(funcName + 'resList = ' + JSON.stringify(resList));
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  async pageGoBack(sel, pageUrl, option) {
    const funcName = 'pageGoBack ';
    console.log(funcName + 'sel = ' + sel + ' pageUrl = ' + pageUrl + ' option = ' + option);
    try {
      Scrape.validateParams(Array([this.page]));
      await this.page.goBack();
      return;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // iterate through anchor list, click each anchor then scrape li
  async anchorListClickAndScrape(sel, pageUrl, option) {
    const funcName = 'anchorListClickAndScrape ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, option]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, option]));
      }
      const anchorListSel = sel; const scrapeSel = option;
      console.log(funcName + 'waiting for selector = ' + sel);
      await this.page.waitForSelector(anchorListSel, Scrape.WAIT_FOR_SEL_OPTIONS);
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      const anchorsList = await this.scrapeAnchorElement(anchorListSel, pageUrl);
      const resList = [];
      if (anchorsList && Array.isArray(anchorsList)) {
        for (const aDict of anchorsList) {
          try {
            const honoursSubDict = {};
            console.log(funcName + 'aDict = ' + JSON.stringify(aDict));
            await this.page.goto(aDict.href,{timeout:0});
            console.log(funcName + 'await this.page.click(aDict.href); executed...');
            console.log(funcName + 'waiting for selector = ' + scrapeSel);
            await this.page.waitForSelector(scrapeSel, Scrape.WAIT_FOR_SEL_OPTIONS);
            const coreSubList = await this.scrapeLIElement(scrapeSel, pageUrl);
            console.log(funcName + 'coreSubList = ' + JSON.stringify(coreSubList));
            honoursSubDict[aDict.innerText] = coreSubList;
            resList.push(honoursSubDict);
            await this.page.goBack();
          } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
          }
        }
      } // if
      console.log(funcName + 'resList = ' + JSON.stringify(resList));
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape Href From Anchor element
  async scrapeAnchorElement(sel, pageUrl, propName) {
    const funcName = 'scrapeAnchorElement ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      // console.log(funcName + 'found matching selector elements = ' + elements);
      // for each found anchor element, scrape hRef & innerText value
      const resList = [];
      for (const ele of elements) {
        const anchorScrapeDataDict = {}; let propHandle = null;
        if (propName && propName.length > 0) {
          propHandle = await ele.getProperty(propName);
          const hrefValue = await propHandle.jsonValue();
          const hRefValTrimmed = String(hrefValue).trim();
          anchorScrapeDataDict[propName] = hRefValTrimmed;
        } else {
          propHandle = await ele.getProperty('href');
          const hrefValue = await propHandle.jsonValue();
          const hRefValTrimmed = String(hrefValue).trim();
          anchorScrapeDataDict.href = hRefValTrimmed;
        }
        propHandle = await ele.getProperty('innerText');
        const innerTextVal = await propHandle.jsonValue();
        const innerTextValTrimmed = String(innerTextVal).trim();
        if (innerTextValTrimmed && innerTextValTrimmed.length > 0) {
          anchorScrapeDataDict.innerText = innerTextValTrimmed;
        }
        resList.push(anchorScrapeDataDict);
      }
      console.log(funcName + 'resList = ' + JSON.stringify(resList));
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape Href From Anchor element
  async scrapeImageElement(sel, pageUrl) {
    const funcName = 'scrapeImageElement ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      const resList = [];
      for (const ele of elements) {
        const propHandle = await ele.getProperty('src'); // for debug purpose
        const srcValue = await propHandle.jsonValue();
        const srcValueTrimmed = String(srcValue).trim();
        resList.push(srcValueTrimmed);
      }
      console.log(funcName + 'resList = ' + JSON.stringify(resList));
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // click on element
  async clickElement(sel, pageUrl, option) {
    const funcName = 'clickElement ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      console.log(funcName + 'option = ' + option);
      for (const ele of elements) {
        try {
          const propHandle = await ele.getProperty('innerText'); // for debug purpose
          const innerTextVal = await propHandle.jsonValue();
          const innerTextValTrimmed = String(innerTextVal).trim();
          if (option && option.length > 0) {
            if (innerTextValTrimmed === option) {
              console.log(funcName + 'clicking an element... innerTextValTrimmed = ' + innerTextValTrimmed);
              await ele.click();
            }
          } else {
            console.log(funcName + 'clicking an element...');
            await ele.click();
          }
        } catch (err) {
          console.log(funcName + 'await e.click(); err = ' + err);
        }
      } // for
      return;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // click on Anchor element
  async clickAnchorElementWhileVisible(sel, pageUrl, options) {
    const funcName = 'clickAnchorElementWhileVisible ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, options]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, options]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      const visibleSel = options;
      while (await this.page.waitForSelector(visibleSel, { timeout: 10000, visible: true })) {
        console.log(funcName + visibleSel + ' selector is visible so clicking on Anchor element .....');
        const elements = await this.page.$$(sel); // if no match value found then returns empty []
        if (!(elements && elements.length > 0)) {
          console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
          throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
        }
        for (const ele of elements) {
          try {
            await ele.click();
            console.log(funcName + 'await ele.click(); executed...');
            await this.page.waitFor(3000);
          } catch (err) {
            console.log(funcName + 'await e.click(); err = ' + err);
          }
        }
      } // while
      return;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // click on Anchor element
  async clickAnchorElement(sel, pageUrl) {
    const funcName = 'clickAnchorElement ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      // await this.page.waitForSelector(sel, { timeout: 10000, visible: true });
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      for (const ele of elements) {
        try {
          await ele.click();
          console.log(funcName + 'await ele.click(); executed...waitinh for waitForNavigation...');
          // await this.page.waitForNavigation();
          console.log(funcName + 'this.page.waitForNavigation(); done...');
        } catch (err) {
          console.log(funcName + 'await e.click(); err = ' + err);
        }
      }
      return;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // select options
  async selectOption(sel, pageUrl, options) {
    const funcName = 'selectOptions ';
    try {
      console.log('**********************Start select option**********************************');
      console.log('Option :' + JSON.stringify(options));

      if (Array.isArray(options)) {
        console.log(funcName + 'optionas is an array, options = ' + JSON.stringify(options));
      } else {
        console.log(funcName + 'optionas is NOT an array, options = ' + options);
      }
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, options]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, options]));
      }
      console.log(funcName + 'waiting for selector selectOption = ' + sel);
      await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      let resOptions = null;
      console.log('Option value set :' + options);
      if (options) {
        // select option
        if (this.page) {
          console.log(funcName + 'options = ' + options);
          var arropt = options.split(',');
          for (var i = 0; i < arropt.length; i++) {
            console.log('Arra option :' + arropt[i]);
            console.log(funcName + 'start select result options value for = ' + resOptions);
            if (resOptions == null || resOptions == "") {
              resOptions = await this.page.select(sel, arropt[i]);
            }
            console.log(funcName + 'END select result options value for = ' + resOptions);
          }

          console.log(funcName + 'select result options value out for = ' + resOptions);

        } else {
          console.log(funcName + 'this.page is null, this.page = ' + this.page);
          throw (new Error('this.page is null, this.page = ' + this.page));
        }
      } else {
        console.log(funcName + 'invalid options, options = ' + JSON.stringify(options));
        throw (new Error('invalid options, options = ' + JSON.stringify(options)));
      }
      console.log(funcName + 'select result options = ' + resOptions);
      console.log('**********************END select option**********************************');
      return;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // input value
  async inputValue(sel, pageUrl, value) {
    const funcName = 'inputValue ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, value]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, value]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      let res = null;
      if (this.page) {
        res = this.page.$$eval(sel, (inputs, val) => {
          // eslint-disable-next-line no-param-reassign
          inputs[0].value = val;
        }, value);
      } else {
        console.log(funcName + 'this.page is null, this.page = ' + this.page);
        throw (new Error('this.page is null, this.page = ' + this.page));
      }
      console.log(funcName + 'res = ' + res);
      return;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // input checkbox
  async inputCheckbox(sel, pageUrl) {
    const funcName = 'inputCheckbox ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      // await this.page.waitForSelector(sel, { timeout: 10000, visible: true });
      await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      // const checkbox = await page.$('input[ng-model="user.managed.timezone"]');
      // console.log(await (await checkbox.getProperty('checked')).jsonValue());
      // await checkbox.click();
      // console.log(await (await checkbox.getProperty('checked')).jsonValue());
      // now check mark it
      let checkedPropVal = null;
      if (this.page) {
        // await this.page.waitFor(3000);
        const checkboxElement = elements[0];
        console.log(funcName + 'checkboxElement = ' + checkboxElement);
        let checkedProp = await checkboxElement.getProperty('checked');
        checkedPropVal = checkedProp.jsonValue();
        console.log(funcName + 'checkedPropVal = ' + checkedPropVal);

        await checkboxElement.click();

        checkedProp = await checkboxElement.getProperty('checked');
        checkedPropVal = checkedProp.jsonValue();
        console.log(funcName + 'checkedPropVal afttred checked clicked = ' + checkedPropVal);
      } else {
        console.log(funcName + 'this.page is null, this.page = ' + this.page);
        throw (new Error('this.page is null, this.page = ' + this.page));
      }
      console.log(funcName + 'checkedPropVal = ' + checkedPropVal);
      return;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // set span innerText value
  static async scrapeReadFromFileElement(sel, pageUrl, filePath) {
    const funcName = 'scrapeReadFromFileElement ';
    try {
      console.log(funcName + 'sel = ' + sel + ', filePath = ' + filePath);
      // ensure we have required params
      Scrape.validateParams([filePath, sel]);
      if (!sel.length > 0) {
        console.log(funcName + 'Invalid selector value, sel = ' + sel);
        throw (new Error('Invalid selector value, sel = ' + sel));
      }
      const fileData = fs.readFileSync(filePath);
      const jsonList = JSON.parse(fileData);
      // console.log(funcName + 'jsonList = ' + JSON.stringify(jsonList));
      const jsonData = jsonList[0];
      const jsonVal = jsonData[sel];
      // console.log(funcName + 'jsonVal = ' + JSON.stringify(jsonVal));
      // console.log(funcName + 'Object.keys(jsonVal) = ' + JSON.stringify(Object.keys(jsonVal)));
      console.log(funcName + 'Object.keys(jsonVal).length = ' + Object.keys(jsonVal).length);
      /*
      const jsonResList = [];
      if (Object.keys(jsonVal).length > 0) {
        jsonResList.push(jsonVal);
        return jsonResList;
      }
      */
      const resList = [];
      resList.push(jsonVal);
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // set span innerText value
  async setSpanInnerTextValue(sel, pageUrl, value) {
    const funcName = 'inpusetSpanInnerTextValuetValue ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel, pageUrl, value]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel, this.page, value]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      let res = null;
      if (this.page) {
        res = this.page.$$eval(sel, (spans, val) => {
          // eslint-disable-next-line no-param-reassign
          spans[0].innerText = val;
        }, value);
      } else {
        console.log(funcName + 'this.page is null, this.page = ' + this.page);
        throw (new Error('this.page is null, this.page = ' + this.page));
      }
      console.log(funcName + 'res = ' + res);
      return;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // select each option, submit for for each option and then scrape value
  async selectEachOptionAndScrapeValue(selNoUse, pageUrl, options) {
    const funcName = 'selectEachOptionAndScrapeValue ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([options]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([options]));
      }
      // extract optionDict values
      const optionDict = JSON.parse(options);
      const selectSel = optionDict.select_selector;
      const formSel = optionDict.form_selector;
      const valueSel = optionDict.value_selector;
      Scrape.validateParams([selectSel, formSel, valueSel]);

      await this.page.waitForSelector(selectSel, Scrape.WAIT_FOR_SEL_OPTIONS);
      console.log(funcName + 'waiting for selector = ' + selectSel);
      // fetch options
      let elements = await this.page.$$(selectSel);
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. selectSel = ' + selectSel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. selectSel = ' + selectSel + ', elements = ' + elements));
      }
      const selectSelElement = elements[0];
      console.log(funcName + 'selectSelElement = ' + selectSelElement);
      const propHandle = await selectSelElement.getProperty('options');
      const optionsCol = await propHandle.jsonValue();
      console.log(funcName + 'propHandle = ' + propHandle);
      console.log(funcName + 'optionsCol = ' + JSON.stringify(optionsCol));
      const selectOptions = [];
      for (const key in optionsCol) {
        if (optionsCol.hasOwnProperty(key)) {
          console.log(funcName + 'key = ' + key);
          const keyVal = optionsCol[key];
          console.log(funcName + 'keyVal = ' + JSON.stringify(keyVal));
          const opValPropHandle = await keyVal.getProperty('value');
          const opValue = await opValPropHandle.jsonValue();
          const opValueTrimmed = String(opValue).trim();
          console.log(funcName + 'opValueTrimmed = ' + opValueTrimmed);
          selectOptions.push(opValueTrimmed);
        }
      }
      // submit form for each option and scrape val
      await this.page.waitForSelector(formSel, Scrape.WAIT_FOR_SEL_OPTIONS);
      elements = await this.page.$$(formSel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. formSel = ' + formSel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. formSel = ' + formSel + ', elements = ' + elements));
      }
      let res = null; const resList = [];
      for (const option of selectOptions) {
        this.page.waitFor(3000);
        const selOptionValDict = {};
        await this.selectOption(selectSel, pageUrl, option);
        if (this.page) {
          res = this.page.$$eval(formSel, (form) => {
            // eslint-disable-next-line no-param-reassign
            form.submit();
          });
          console.log(funcName + 'res = ' + res);
          // extract value of selector
          const valResList = await this.scrapeDIVElement(valueSel, pageUrl);
          console.log(funcName + 'valueSel valResList = ' + JSON.stringify(valResList));
          if (valResList && valResList.length > 0) {
            selOptionValDict[option] = valResList[0];
            resList.push(selOptionValDict);
          }
        } else {
          console.log(funcName + 'this.page is null, this.page = ' + this.page);
          throw (new Error('this.page is null, this.page = ' + this.page));
        }
      } // for
      console.log(funcName + 'resList = ' + JSON.stringify(resList));
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // form submit
  async submitForm(sel, pageUrl) {
    const funcName = 'inputValue ';
    try {
      if (pageUrl && pageUrl.length > 0) {
        Scrape.validateParams(Array([sel]));
        await this.setupNewBrowserPage(pageUrl);
      } else {
        Scrape.validateParams(Array([sel]));
      }
      console.log(funcName + 'waiting for selector = ' + sel);
      await this.page.waitForSelector(sel, Scrape.WAIT_FOR_SEL_OPTIONS);
      const elements = await this.page.$$(sel); // if no match value found then returns empty []
      if (!(elements && elements.length > 0)) {
        console.log(funcName + 'failed to get matching selector. sel = ' + sel + ', elements = ' + elements);
        throw (new Error('failed to get matching selector. sel = ' + sel + ', elements = ' + elements));
      }
      let res = null;
      if (this.page) {
        res = this.page.$$eval(sel, (forms) => {
          // eslint-disable-next-line no-param-reassign
          forms[0].submit();
        });
      } else {
        console.log(funcName + 'this.page is null, this.page = ' + this.page);
        throw (new Error('this.page is null, this.page = ' + this.page));
      }
      console.log(funcName + 'res = ' + res);
      return res;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  // scrape value type element
  static async scrapeValueElement(sel) {
    const funcName = 'scrapeValueElement ';
    try {
      const resList = [];
      // in case of value type element, just simply return value of selectors
      resList.push(sel);
      return resList;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }

  async scrapeSelectorFile(selectorFilepath) {
    const funcName = 'scrapeSelectorFile ';
    // let s = null;
    try {
      Scrape.validateParams([selectorFilepath]);
      console.log(funcName + 'selectorFilepath = ' + selectorFilepath);
      const fileData = fs.readFileSync(selectorFilepath);
      const selectorJson = JSON.parse(fileData);
      this.selectorJson = selectorJson;
      // s = new Scrape();
      // await this.init({ headless: true });
      // set course URL value to selector json page-url
      // this.selectorJson.page_url = courseURL;
      // set new browser page with course URL
      // await this.setupNewBrowserPage(this.selectorJson.page_url);
      const courseScrappedData = {};
      // for each json key in slector file
      for (const jsonKey in this.selectorJson) {
        // console.log(funcName + 'jsonKey = ' + jsonKey);
        if (this.selectorJson.hasOwnProperty(jsonKey)) {
          console.log(funcName + '\n\r ###########################      Scraping for key = ' + jsonKey + '     ###########################');
          const jsonVal = this.selectorJson[jsonKey];
          // for each element in json-array, will be always 1 item in array
          if (Array.isArray(jsonVal)) {
            const allRootElemetDictScrappedResList = [];
            for (const elementDict of jsonVal) {
              const outRootElementDict = {};
              const elementUrl = elementDict.url;
              // console.log(funcName + '    elementDict url = ' + elementUrl);
              // for each elementDict, set url or page-url
              let url = null;
              if (elementUrl && elementUrl.length > 0) {
                console.log(funcName + 'overwriting page-url with elementDict url...');
                url = elementUrl;
              }
              const elements = elementDict.elements;
              console.log(funcName + '    elementDict elements = ' + JSON.stringify(elements));
              // for each element of the json-key-dict
              if (Array.isArray(elements)) {
                const allElementsScrappedResList = [];
                let hasAnyElementCricosCode = false;
                for (const eleDict of elements) {
                  const outElementDict = {};
                  const eleType = eleDict.elementType;
                  // console.log(funcName + '      elementType = ' + eleType);
                  let eleOptions = null;
                  if (eleDict.option) {
                    eleOptions = eleDict.option;
                  }
                  if (eleDict.value) {
                    eleOptions = eleDict.value;
                  }
                  const eleSelctors = eleDict.selectors;
                  // for each selector of the element
                  if (Array.isArray(eleSelctors)) {
                    const allSelectorsScrappedResList = [];
                    for (const sel of eleSelctors) {
                      const scrapeElementRes = await this.scrapeElement(eleType, sel, url, eleOptions);
                      // check if we have valid cricos code and change flag value accordingly
                      if (jsonKey === 'course_cricos_code') {
                        if ((appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION === false)
                          && (await utils.hasValidCricosode(scrapeElementRes) === true)) {
                          hasAnyElementCricosCode = true;
                        }
                      } // if (jsonKey === 'course_cricos_code')
                      console.log(funcName + 'scrapeElementRes = ' + scrapeElementRes);
                      if (scrapeElementRes) {
                        allSelectorsScrappedResList.push(scrapeElementRes);
                      }
                    } // for each sel in selectors[]
                    console.log('**');
                    console.log(funcName + 'All selectors scrapped for the element, allSelectorsJson length = ' + allSelectorsScrappedResList.length);
                    console.log(funcName + 'allSelectorsJson = ' + JSON.stringify(allSelectorsScrappedResList));
                    console.log('**');
                    // allElementsScrappedResList.push(allSelectorsScrappedResList);
                    outElementDict.selectors = allSelectorsScrappedResList;
                    allElementsScrappedResList.push(outElementDict);
                  } // if (Array.isArray(eleSelctors)) {
                } // for each element in elements[]
                // check if we have valid cricos code for any of element of cricos key?
                try {
                  if (jsonKey === 'course_cricos_code') {
                    // check if valid code else do not continue
                    if ((appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION === false)
                      && hasAnyElementCricosCode === false) {
                      console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                      console.log(funcName + 'Invalid cricos code so break scrapping function and continue with next course....');
                      console.log(funcName + 'hasAnyElementCricosCode = ' + hasAnyElementCricosCode);
                      console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                      return null;
                    }
                  } // if (jsonKey === 'course_cricos_code')
                } catch (error) {
                  console.log(error);
                  throw (error);
                }
                console.log('***');
                console.log(funcName + 'All elements scrapped for the key, allElementsJson length = ' + allElementsScrappedResList.length);
                console.log(funcName + 'allElementsJson = ' + JSON.stringify(allElementsScrappedResList));
                console.log('***');
                // allRootElemetDictScrappedResList.push(allElementsScrappedResList);
                outRootElementDict.elements = allElementsScrappedResList;
                allRootElemetDictScrappedResList.push(outRootElementDict);
              } // if (Array.isArray(elements)) {
            } // for (const elementDict of jsonVal)
            courseScrappedData[jsonKey] = allRootElemetDictScrappedResList;
          } else { // if (Array.isArray(jsonVal)) {
            courseScrappedData[jsonKey] = [];
          }
        } // if (this.selectorJson.hasOwnProperty(jsonKey)
        console.log('\n\r**********');
        console.log(funcName + 'courseScrappedData[' + jsonKey + '] = ' + JSON.stringify(courseScrappedData[jsonKey]));
        console.log('**********\n\r');
      } // for (const jsonKey in this.selectorJson)
      console.log('\n\r**********');
      console.log(funcName + 'All scrapped data,  courseScrappedData = ' + JSON.stringify(courseScrappedData));
      console.log('**********\n\r');
      // if (s) {
      //   await s.close();
      // }
      return courseScrappedData;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // if (s) {
      //   await s.close();
      // }
      throw (error);
    }
  }

  // extract element value from scrapped element
  static async extractValueFromScrappedElement(rootElementDictList) {
    const funcName = 'extractValueFromScrappedElement ';
    try {
      console.log(funcName + 'rootElementDictList =' + JSON.stringify(rootElementDictList));
      console.log(funcName + 'rootElementDictList = ' + rootElementDictList);
      let extractedVal = null;
      let concatnatedRootElementsStr = null;
      if (Array.isArray(rootElementDictList)) {
        for (const rootEleDict of rootElementDictList) {
          console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
          const elementsList = rootEleDict.elements;
          let concatnatedElementsStr = null;
          for (const eleDict of elementsList) {
            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
            const selectorsList = eleDict.selectors;
            let concatnatedSelectorsStr = null;
            for (const selList of selectorsList) {
              console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
              console.log(funcName + '\n\r selList = ' + selList);
              let concatnatedSelStr = null;
              for (const selItem of selList) {
                // if (!concatnatedSelStr) {
                //   concatnatedSelStr = selItem;
                // } else {
                //   concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                // }
                if (selItem && concatnatedSelStr === null) {
                  concatnatedSelStr = selItem;
                } else {
                  concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                }
              } // selList
              console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
              if (concatnatedSelStr) {
                if (!concatnatedSelectorsStr) {
                  concatnatedSelectorsStr = concatnatedSelStr;
                } else {
                  concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                }
              }
            } // selectorsList
            console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
            // concat elements
            if (concatnatedSelectorsStr) {
              if (!concatnatedElementsStr) {
                concatnatedElementsStr = concatnatedSelectorsStr;
              } else {
                concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
              }
            }
          } // elementsList
          console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
          if (concatnatedElementsStr) {
            if (!concatnatedRootElementsStr) {
              concatnatedRootElementsStr = concatnatedElementsStr;
            } else {
              concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
            }
            if (concatnatedRootElementsStr) {
              extractedVal = String(concatnatedRootElementsStr).trim();
            }
          }
        } // rootElementDictList
      }
      // extractedVal = String(concatnatedRootElementsStr).trim();
      console.log(funcName + 'extractedVal = ' + extractedVal);
      return extractedVal;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  }
  //page> page object of calling page > required 
  //pageurl >string > URL of page, whenever need to redirect or else pass ""
  //rootSelector > string > element group selector
  //elementselector > string> for select particular element
  //remover > string[] > list of removers for remove extra data 
  //seperator > string > an element or text using we seperate text for each entry
  //actionname > string[] > number of action to perform
  //actionelemselector > string[] > number of action selectors
  //limit > integer > number of element to br proceed
  static async findScrapeRemoveSeperatePerform_fun(pageurl, rootSelector, remover, seperator, actionname, actionelemselector, limit) {
    let actionelem = null, rootDict = [];
    console.log("##### call scrape div #####");
    try {
      if (page.url() != pageurl && pageurl) {
        await page.goto(pageurl,{timeout:0});
      }
      if (actionname.length > 0) {
        if (actionname.length != actionelemselector.length) {
          console.log("####Error in action make sure to have action name and it's selectors equal in the list######");
        }
        for (var countaction = 0; countaction < actionname.length; countaction++) {
          switch (actionname[countaction]) {
            case "CLICK":
              await page.waitForSelector(actionelemselector[countaction]);
              actionelem = await page.$(actionelemselector[countaction]);
              await actionelem.click();
          }
        }
      }
      var targetLinks = await page.$$(rootSelector);
      var dq = [], study_units = [];
      if (seperator != null) {
        for (var count = 0; count < targetLinks.length; count++) {
          elementstring = await page.evaluate(el => el.innerHTML, targetLinks[count]);
          dq = elementstring.split(seperator);
          dq.forEach(element => {
            study_units.push(element.replace(/<(.|\n)*?>/g, ''));
          });
        }
      }
      else {
        for (var count = 0; count < targetLinks.count; count++) {
          elementstring = await page.evaluate(el => el.innerText, targetLinks[count]);
          study_units.push(elementstring);
        }
      }
      if (remover.length > 0) {
        study_units.forEach(unit => {
          var remove = false;
          remover.forEach(unitremove => {
            remove = unit.includes(unitremove);
          })
          if (remove) {
            study_units.splice(study_units.indexOf(unit), 1);
          }
        })
      }
      console.log("Unit length ### " + study_units.length);
      console.log("limit ### " + limit);
      (study_units.length > limit) ? study_units.length = limit : "";
      rootDict = {
        course_outline: {
          study_units: study_units,
          more_details: page.url()
        }
      }
    }
    catch (error) {
      console.log("Exception during execution:" + error);
    }
    return rootDict;
  }
}

// Scrape Class Static Properties
// Element type to scrape
Scrape.WAIT_FOR_SEL_OPTIONS = {
  timeout: 5000, // ms
};
Scrape.ELEMENT_TYPE = {
  DIV: 'div',
  DIV_AT_INDEX: 'div_at_index',
  DIV_PAGE_URL: 'div_page_url',
  LI: 'li',
  CLICK: 'click',
  VALUE: 'value',
  ANCHOR_CLICK: 'anchorclick',
  ANCHOR_CLICK_WHILE_VISIBLE: 'anchorclick_while_visible',
  ANCHOR: 'anchor',
  SELECT_OPTION: 'select_option',
  ANCHORLIST_CLICK_SCRAPE: 'anchorlist_click_scrape',
  ANCHOR_WITHIN_LI: 'anchor_within_li',
  INPUT_SPAN_INNERTEXT: 'input_span_innertext',
  INPUT_VALUE: 'input_value',
  INPUT_CHECKBOX: 'input_checkbox',
  FORM_SUBMIT: 'form_submit',
  IMAGE_SRC: 'image_src',
  PAGE_GO_BACK: 'page_go_back',
  READ_FROM_FILE: 'readfromfile',
  COURSE_LIST: 'COURSE_LIST',
  SELECT_EACH_OPTION_AND_SCRAPE: 'select_each_option_and_scrape',
  SCRAPE_BY_XPATH: "scrape_by_xpath",
  SCRAPE_LIST_DIFF: "scrape_list_diff",
  SCRAPE_OUTLINE_DIFF: "scrape_outline_diff",
  REMOVE_ATRR: "remove_attr",
  SET_ATRR: "set_attr",
  DOWNLOAD_PDF: "download_pdf",
  SCRAPE_PDF: "scrape_pdf",
  READ_FROM_JSON: "read_from_json",
  WAIT: "wait",
  SCRAPE_BY_ACTION: "scrape_by_action",
  SCRAPE_MULTI_ELEMENT: "scrape_multi_selector",
};

module.exports = { Scrape };
