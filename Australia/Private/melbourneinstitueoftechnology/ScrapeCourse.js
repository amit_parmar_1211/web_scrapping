const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = course_category;
                            console.log("#course_discipline--->" + JSON.stringify(resJsonData.course_discipline));


                            break;
                        }
                        case 'course_title_category':
                        case 'course_title': {
                            let t;
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            if (title.includes(" - ")) {
                                t = title.replace(" - ", "_").trim()
                            }
                            else {
                                t = title
                            }
                            var ctitle = format_functions.titleCase(t).trim();
                            resJsonData.course_title = ctitle
                            break;
                        }
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const ibtDict = {}; const pteDict = {}; let ieltsNumber = null;
                            if (courseScrappedData.course_toefl_ielts_score) {
                                const ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                                if (ieltsScore && ieltsScore.length > 0) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ieltsScore;
                                    englishList.push(ieltsDict);
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(ieltsScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ieltsNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                    }
                                }
                            }
                            if (courseScrappedData.course_academic_requirement) {
                                const courseKeyVal = courseScrappedData.course_academic_requirement;
                                var academicReq = "";
                                let assigned = false;
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log('academicReq selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    if (assigned == false) {
                                                        for (let i = 0; i < selList.length; i++) {
                                                            if (selList[i] != "Results Required") {
                                                                academicReq = selList[i];
                                                                assigned = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    if (academicReq == "null") {
                                        courseAdminReq.academic = [];
                                    } else {
                                        courseAdminReq.academic = [academicReq];
                                    }
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }
                            const academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            const english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            const entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.course_entry_requirements_more_details);
                            resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                            resJsonData.course_admission_requirement.entry_requirements_url = "";
                            resJsonData.course_admission_requirement.academic_requirements_url = "";
                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};
                            var penglishList = [];
                            let ieltsNo, pteNo, pbtNo, ibtNo, caeNo;
                            ieltsNo = ieltsNumber;
                            if (ieltsNo) {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = ieltsDict.description;
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsNumber;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }
                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            var fullTime = "";
                            let assigned = false;
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (var selList of selectorsList) {
                                            console.log('course_duration_full_time selList = ' + JSON.stringify(selList));
                                            if (assigned == false) {
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    for (let i = 0; i < selList.length; i++) {
                                                        if (selList[i] && selList[i].length > 1) {
                                                            fullTimeText = selList[i];
                                                            if (selList[i].indexOf("Domestic Students only") > -1) {
                                                                fullTime = selList[i].split("or")[0].replace(/ *\([^)]*\) */g, " ");
                                                                assigned = true;
                                                            }
                                                            else {
                                                                fullTime = selList[i].replace(/ *\([^)]*\) */, " ");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTime && fullTimeText.length > 0 && fullTime.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(fullTime));

                                    // let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    // courseDurationDisplayList.push(tempvar);
                                    // demoarray = tempvar[0];
                                    // console.log("demoarray--->", demoarray);
                                    // let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);

                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (courseDurationDisplayList && courseDurationDisplayList.length > 0) {
                                        resJsonData.course_duration_display = courseDurationDisplayList;
                                    }
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode': {
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':


                        // case 'course_campus_location': { // Location Launceston
                        //     var campLocation = [];
                        //     let replace = [];
                        //     const courseKeyVal = courseScrappedData.course_campus_location;

                        //     var cricos = resJsonData.course_cricos_code;
                        //     console.log("locationsjgfhlength -->", cricos);


                        //     let assigned = false;
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (var selList of selectorsList) {
                        //                     if (assigned == false) {
                        //                         console.log('course_campus_location selList = ' + JSON.stringify(selList));
                        //                         if (selList && selList.length > 0) {
                        //                             selList = selList[0].split(")");
                        //                             if (selList.length > 2) {
                        //                                 for (let i = 0; i < selList.length; i++) {
                        //                                     if (selList[i].length > 1) {
                        //                                         campLocation.push(selList[i].split("(")[0].trim());
                        //                                         console.log("123234324", campLocation);
                        //                                     }
                        //                                 }
                        //                                 assigned = true;
                        //                             }
                        //                             else {
                        //                                 replace.push(String(selList[0]).replace('International students:', '').trim())
                        //                                 console.log("123234324", replace);
                        //                                 campLocation.push(replace[0].split("(")[0].trim());
                        //                                 assigned = true;
                        //                             }
                        //                         }
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     let loc = []
                        //     if (campLocation.includes(",") > -1) {
                        //         campLocation = String(campLocation).split(",");
                        //         for (let i of campLocation) {
                        //             loc.push(i);
                        //             console.log("loc@@@@@@", loc);
                        //         }
                        //     }
                        //     let newcampus = [];
                        //     for (let campus of loc) {
                        //         if (!newcampus.includes(campus)) {
                        //             newcampus.push(campus);
                        //             console.log("##Campuscampus-->" + campus)
                        //         }
                        //     }
                        //     if (newcampus && newcampus.length > 0) {
                        //         var campusedata = [];
                        //         newcampus.forEach(element => {
                        //             campusedata.push({
                        //                 "name": element,
                        //                 "code": cricos

                        //             })
                        //         });
                        //         resJsonData.course_campus_location = campusedata;
                        //         console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);
                        //     }
                        //     else {
                        //         throw new Error("No campus location found.");
                        //     }
                        //     break;
                        // }
                        case 'course_cricos_code': {

                            var campLocation = [];
                            let replace = [];
                            const courseKeyVals = courseScrappedData.course_campus_location;
                            let assigned = false;
                            if (Array.isArray(courseKeyVals)) {
                                for (const rootEleDict of courseKeyVals) {
                                    console.log('course_campus_location rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log('course_campus_location eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (var selList of selectorsList) {
                                            if (assigned == false) {
                                                console.log('course_campus_location selList = ' + JSON.stringify(selList));
                                                if (selList && selList.length > 0) {
                                                    selList = selList[0].split(")");
                                                    if (selList.length > 2) {
                                                        for (let i = 0; i < selList.length; i++) {
                                                            if (selList[i].length > 1) {
                                                                campLocation.push(selList[i].split("(")[0].trim());
                                                                console.log("123234324@@@", campLocation);
                                                            }
                                                        }
                                                        assigned = true;
                                                    }
                                                    else {
                                                        replace.push(String(selList[0]).replace('International students:', '').trim())
                                                        console.log("12323432#####", replace);
                                                        campLocation.push(replace[0].split("(")[0].trim());
                                                        assigned = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            let loc = []
                            if (campLocation.includes(",") > -1) {
                                campLocation = String(campLocation).split(",");
                                for (let i of campLocation) {
                                    loc.push(i);
                                    console.log("loc@@@@@@", loc);
                                }
                            }
                            let newcampus = [];
                            for (let campus of loc) {
                                if (!newcampus.includes(campus)) {
                                    newcampus.push(campus);
                                    console.log("##Campuscampus-->" + campus)
                                }
                            }


                            const courseKeyVal = courseScrappedData.course_cricos_code;
                            let course_cricos_code = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log('course_cricos_code rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log('course_cricos_code eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (var selList of selectorsList) {
                                            console.log('course_cricos_code selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                //course_cricos_code = selList;
                                                selList = selList[0].split(")");
                                                if (selList.length > 2) {
                                                    for (let i = 0; i < selList.length; i++) {
                                                        if (selList[i].length > 2) {
                                                            if (selList[i].indexOf("VIC") > -1) {
                                                                course_cricos_code.push(selList[i].split("(")[0].trim());
                                                                console.log("course_cricos_code1", course_cricos_code);
                                                            }
                                                            else if (selList[i].indexOf("NSW") > -1) {
                                                                course_cricos_code.push(selList[i].split("(")[0].trim());
                                                            }
                                                        }
                                                    }
                                                }
                                                else {
                                                    if (selList[0].indexOf("VIC") > -1) {
                                                        course_cricos_code.push(selList[0].split("(")[0].trim());
                                                        console.log("course_cricos_code1", course_cricos_code);
                                                        if (course_cricos_code.includes(",") > -1) {
                                                            course_cricos_code = String(course_cricos_code).split(",")[0];
                                                            var cricos1 = course_cricos_code.replace("VIC:", '').trim();
                                                            console.log("course_cricos_codemmmmmm", cricos1);
                                                            course_cricos_code = [cricos1];
                                                            console.log("course_cricos_codefsfdg", course_cricos_code);
                                                        }
                                                        if (selList[0].indexOf("NSW:") > -1) {

                                                            course_cricos_code.push(selList[0].split("NSW:")[1].trim());
                                                            console.log("course_cricos_codefsfdg------->", course_cricos_code);
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (newcampus && newcampus.length > 0) {
                                var campusedata = [];
                                console.log("newcampus", newcampus)

                                if (newcampus.length == course_cricos_code.length) {
                                    for (let i = 0; i < newcampus.length; i++) {
                                        console.log("newcampus------->", newcampus)
                                        campusedata.push({
                                            name: newcampus[i], code: course_cricos_code[i].toString()
                                        })
                                    }
                                    resJsonData.course_campus_location = campusedata;
                                    console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);
                                }
                                else {
                                    throw new Error("No campus location found.");
                                }
                            }


                            break;
                        }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            const fixedFeesDict = JSON.parse(fs.readFileSync(appConfigs.fees));
                            console.log("fixedFeesDict----->", fixedFeesDict);
                            var titlte = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            var feeIndex = fixedFeesDict.findIndex(value => { return value.key.trim() == titlte.trim() });
                            console.log("feeIndex", feeIndex);
                            let prevYearText = fixedFeesDict[feeIndex].Fees2018;
                            console.log("fixedFeesDict[feeIndex].Fees2018", fixedFeesDict[feeIndex].Fees2018);
                            let currentYearText = fixedFeesDict[feeIndex].Fees2019;
                            let feesIntStudent = currentYearText;
                            if (feesIntStudent.length > 0) {
                                console.log("international_student" + feesIntStudent)
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal = String(feesWithDollorTrimmed).replace('$', '');
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const myval = feesVal.split(".");
                                    const regEx = /\d/g;
                                    let feesValNum = myval[0].match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Trimester",

                                                description: feesWithDollorTrimmed

                                            });
                                        }
                                    }
                                }
                            }
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                            const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                            if (feesDuration && feesDuration.length > 0) {
                                feesDict.fee_duration_years = feesDuration;
                            }
                            if (feesCurrency && feesCurrency.length > 0) {
                                feesDict.currency = feesCurrency;
                            }
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes                               
                                if (feesIntStudent.length > 0) { // extract only digits
                                    // feesDict.international_student_all_fees = [];
                                    if (prevYearText && prevYearText != "NA") {
                                        // feesDict.international_student_all_fees.push("2019 fees: " + prevYearText.toString());
                                    }
                                    if (currentYearText) {
                                        // feesDict.international_student_all_fees.push("2020 fees: " + currentYearText.toString());
                                    }
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;
                        }
                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = []
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            for (let program_val of program_code) {
                                resJsonData.program_code = program_val;
                            }
                            break;
                        }
                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };

                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

                            let resScholarshipJson = null;


                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }




                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }

                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)

                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;


                            break;
                        }

                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                resJsonData.application_fee = ""
                                break;
                            }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            let courseIntakeDisplay = [];
                            let courseKeyVal_intake = courseScrappedData.course_intake;
                            let courseintake;
                            let courseIntakeDisplay1;
                            if (Array.isArray(courseKeyVal_intake)) {
                                for (const rootEleDict of courseKeyVal_intake) {
                                    console.log(funcName + '\n\r rootEleDict ieltsDict= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict ieltsDict= ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList ieltsDict= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList)) {
                                                for (let sel of selList) {
                                                    if (courseintake != sel) {
                                                        courseintake = sel;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }


                            console.log("R locationArray", campus);
                            console.log('************************Start course_intake******************************');
                            console.log('courseIntakeStr : ' + courseintake);

                            if (courseintake && courseintake.length > 0) {
                                resJsonData.course_intake = courseintake;

                                const intakeSplit = String(courseintake).split(',');
                                console.log('course_intake intakeStrList = ' + JSON.stringify(intakeSplit));

                                if (intakeSplit && intakeSplit.length > 0) {
                                    for (var part of intakeSplit) {
                                        console.log("R part", part);
                                        part = part.trim();
                                        courseIntakeDisplay.push(part);
                                    }
                                }
                                console.log('course_intake courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));

                            }

                            var campus = resJsonData.course_campus_location;

                            if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];

                                for (let location11 of campus) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location11.name,
                                        "value": courseIntakeDisplay
                                    });
                                }
                            }
                            console.log("intakes123 -->", resJsonData.course_intake.intake);
                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            let formatedIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");
                            // console.log(JSON.stringify(providemyintake(intake, "MOM", "")));
                            console.log("Intakes --> ", JSON.stringify(formatedIntake));
                            var intakedata = {};
                            intakedata.intake = formatedIntake;

                            intakedata.more_details = more_details;
                            resJsonData.course_intake = intakedata;
                            break;

                        }



                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            course_country = selList[0];
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = "";
                            let assigned = false;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log('course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                if (assigned == false) {
                                                    for (let i = 0; i < selList.length; i++) {
                                                        course_overview += selList[i];
                                                    }
                                                    assigned = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_overview.length > 0) {
                                resJsonData.course_overview = course_overview;
                            }
                            else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            let assigned = false;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (var selList of selectorsList) {
                                            console.log('course_career_outcome selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                if (assigned == false) {
                                                    if (selList[0].indexOf(":") > -1) {
                                                        selList = selList[0].split(":")[1].split(",");
                                                    }
                                                    else if (selList[0].indexOf("Typical roles include") > -1) {
                                                        selList = selList[0].split("Typical roles include")[1].split(",");
                                                    }
                                                    for (let i = 0; i < selList.length; i++) {
                                                        course_career_outcome.push(selList[i].trim().indexOf("and") > -1 ? selList[i].replace("and", "").replace(",", "").trim() : selList[i].replace(",", "").trim());
                                                    }
                                                    assigned = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            }
                            else {
                                resJsonData.course_career_outcome = []
                            }
                            break;
                        }
                        case 'course_study_level': {
                            const cStudyLevel = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
                            if (cStudyLevel) {
                                resJsonData.course_study_level = cStudyLevel;
                            }
                            else {
                                resJsonData.course_study_level = "Non AQF Award";
                            }
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        }
                    }
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                }
            }
            var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "NA";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;

                resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            Scrape.validateParams([courseDict]);
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            let courseScrappedData = null;
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            }
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    }
}
module.exports = { ScrapeCourse };
