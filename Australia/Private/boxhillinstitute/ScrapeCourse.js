const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, category, course_url) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_title': {
                            // resJsonData.course_title = 
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);

                            if (title.includes('\n') > -1) {
                                title.trim();
                                console.log("splitStr" + title);
                                let t = title.replace(/[\r\n\t ]+/g, ' ');
                                resJsonData.course_title = t
                            }
                            resJsonData.course_discipline = category;
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            var splitdata, finaldata, data;

                            if (cTitle.includes(":")) {
                                finaldata = cTitle.split(":")[1]
                            }

                            //let cStudyLevel = null;
                            console.log(funcName + 'course_title course_study_level = ' + finaldata);
                            const cStudyLevel = await format_functions.getMyStudyLevel(finaldata.trim())
                            console.log("Study_level", cStudyLevel)
                            if (cStudyLevel) {
                                resJsonData.course_study_level = cStudyLevel;
                            }
                            break;
                        }


                        case 'course_toefl_ielts_score': {
                            const courseAdminReq = {};
                            const englishList = [];
                            let ieltsNumber = null, pteNumber = null, ibtNumber = null, caeNumber = null;
                            let myscore = '';
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {};
                            console.log("resJsonData.course_toefl_ielts_score -->", JSON.stringify(courseScrappedData.course_toefl_ielts_score));
                            let ieltsString = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                            let title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            let studylevel = resJsonData.course_study_level
                            let ieltsSplitString = '';
                            console.log("ieltsString-->", studylevel)
                            if (ieltsString == null) {
                                myscore = await utils.getMappingScore(studylevel);
                                console.log("Myscore@@@", myscore.ielts)
                                if (myscore.ielts) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = myscore.ielts;
                                    ieltsDict.min = 0;
                                    ieltsDict.require = await utils.giveMeNumber(myscore.ielts);
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);
                                }
                                if (myscore.ibt) {
                                    tofelDict.name = 'toefl ibt';
                                    tofelDict.description = myscore.ibt;
                                    tofelDict.min = 0;
                                    tofelDict.require = await utils.giveMeNumber(myscore.ibt);
                                    tofelDict.max = 120;
                                    tofelDict.R = 0;
                                    tofelDict.W = 0;
                                    tofelDict.S = 0;
                                    tofelDict.L = 0;
                                    tofelDict.O = 0;
                                    englishList.push(tofelDict);
                                }
                                if (myscore.pte) {
                                    pteDict.name = 'pte academic';
                                    pteDict.description = myscore.pte;
                                    pteDict.min = 0;
                                    pteDict.require = await utils.giveMeNumber(myscore.pte);
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);
                                }
                                if (myscore.cae) {
                                    caeDict.name = 'cae';
                                    caeDict.description = myscore.cae;
                                    caeDict.min = 80;
                                    caeDict.require = await utils.giveMeNumber(myscore.cae);;
                                    caeDict.max = 230;
                                    caeDict.R = 0;
                                    caeDict.W = 0;
                                    caeDict.S = 0;
                                    caeDict.L = 0;
                                    caeDict.O = 0;
                                    englishList.push(caeDict);
                                }

                            } else {

                                if (ieltsString.indexOf('(') > -1) {
                                    ieltsSplitString = ieltsString.split('(');
                                } else {
                                    ieltsSplitString = ieltsString;
                                }
                                console.log("ieltsSplitString -->", ieltsSplitString);
                                if (ieltsSplitString && ieltsSplitString.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(ieltsSplitString).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ieltsNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                    }
                                }

                                myscore = await utils.getMappingScore(ieltsNumber);
                                console.log("myscore : ", myscore);                               
                                var ibt_req = '';
                                var pte_req = '';
                                var cae_req = '';
                                if (myscore.ibt && myscore.ibt.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.ibt).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        ibt_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'ibtNumber = ' + ibt_req);
                                    }
                                }
                                if (myscore.pbt && myscore.pbt.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.pbt).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pbt_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'pbtNumber = ' + pbt_req);
                                    }
                                }
                                if (myscore.pte && myscore.pte.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.pte).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pte_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'pteNumber = ' + pte_req);
                                    }
                                }
                                if (myscore.cae && myscore.cae.length > 0) {
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(myscore.cae).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        cae_req = Number(matchedStrList[0]);
                                        console.log(funcName + 'caeNumber = ' + cae_req);
                                    }
                                }

                                if (ieltsNumber) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = myscore.ielts;
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsNumber;
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);
                                    //englishList.push(ieltsDict);
                                }
                                if (ibt_req) {
                                    tofelDict.name = 'toefl ibt';
                                    tofelDict.description = myscore.ibt;
                                    tofelDict.min = 0;
                                    tofelDict.require = ibt_req;
                                    tofelDict.max = 120;
                                    tofelDict.R = 0;
                                    tofelDict.W = 0;
                                    tofelDict.S = 0;
                                    tofelDict.L = 0;
                                    tofelDict.O = 0;
                                    englishList.push(tofelDict);
                                }
                                if (pte_req) {
                                    pteDict.name = 'pte academic';
                                    pteDict.description = myscore.pte;
                                    pteDict.min = 0;
                                    pteDict.require = pte_req;
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);
                                }
                                if (cae_req) {
                                    caeDict.name = 'cae';
                                    caeDict.description = myscore.cae;
                                    caeDict.min = 80;
                                    caeDict.require = cae_req;
                                    caeDict.max = 230;
                                    caeDict.R = 0;
                                    caeDict.W = 0;
                                    caeDict.S = 0;
                                    caeDict.L = 0;
                                    caeDict.O = 0;
                                    englishList.push(caeDict);
                                }
                            }
                            ///progress bar start
                            console.log("title.toLowerCase() -->", title.toLowerCase().replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n|\s)/g, ' '))
                            if (title.toLowerCase().replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n|\s)/g, ' ').trim() == "diploma of nursing") {
                                console.log("YESSSSSS")
                                var ielts = "you achieve a minimum score of 7 in each component across the two sittings, and no score in any component of the test is below 6.5"
                                var pte = "you achieve a minimum score of 65 in each of the communicative skills across the 2 sittings, and no score in any of the communicative skills is below 58"
                                var ibt = "a minimum total score of 94 is achieved in each sitting and you achieve a minimum score of 24 for listening, 24 for reading, 27 for writing and 23 for speaking across the 2 sittings"
                                if (ielts) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ielts;
                                    ieltsDict.min = 0;
                                    ieltsDict.require = await utils.giveMeNumber(ielts);
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);
                                }
                                if (ibt) {
                                    tofelDict.name = 'toefl ibt';
                                    tofelDict.description = ibt;
                                    tofelDict.min = 0;
                                    tofelDict.require = await utils.giveMeNumber(ibt);
                                    tofelDict.max = 120;
                                    tofelDict.R = 0;
                                    tofelDict.W = 0;
                                    tofelDict.S = 0;
                                    tofelDict.L = 0;
                                    tofelDict.O = 0;
                                    englishList.push(tofelDict);
                                }
                                if (pte) {
                                    pteDict.name = 'pte academic';
                                    pteDict.description = pte;
                                    pteDict.min = 0;
                                    pteDict.require = await utils.giveMeNumber(pte);
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);
                                }

                            }
                            if(title.toLowerCase().replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n|\s)/g, ' ').trim() == "bachelor of early childhood education") {
                                var ielts = "IELTS Academic 7.0 (Academic) with no band score less than 7.0"
                                var pte = "Pearson Test of Academic English 69 (with no communicative skill score less than 65) or"
                                var ibt = "TOEFL Internet based 100" 
                                if (ielts) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = ielts;
                                    ieltsDict.min = 0;
                                    ieltsDict.require = await utils.giveMeNumber(ielts);
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    englishList.push(ieltsDict);
                                }
                                if (ibt) {
                                    tofelDict.name = 'toefl ibt';
                                    tofelDict.description = ibt;
                                    tofelDict.min = 0;
                                    tofelDict.require = await utils.giveMeNumber(ibt);
                                    tofelDict.max = 120;
                                    tofelDict.R = 0;
                                    tofelDict.W = 0;
                                    tofelDict.S = 0;
                                    tofelDict.L = 0;
                                    tofelDict.O = 0;
                                    englishList.push(tofelDict);
                                }
                                if (pte) {
                                    pteDict.name = 'pte academic';
                                    pteDict.description = pte;
                                    pteDict.min = 0;
                                    pteDict.require = await utils.giveMeNumber(pte);
                                    pteDict.max = 90;
                                    pteDict.R = 0;
                                    pteDict.W = 0;
                                    pteDict.S = 0;
                                    pteDict.L = 0;
                                    pteDict.O = 0;
                                    englishList.push(pteDict);
                                }
                            }


                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            } else {
                                // courseAdminReq.english = []
                                throw new Error("IELTS not found");
                            }
                            //var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var academicReq = null;
                            // const courseKeyVal = courseScrappedData.course_academic_requirement;
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                          
                            courseAdminReq.academic = (courseKeyVal) ? [courseKeyVal] : [""];
                            courseAdminReq.english_requirements_url = english_more;
                            courseAdminReq.entry_requirements_url = "";
                            courseAdminReq.academic_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            const courseKeyVal = courseScrappedData.course_url;
                            var url = "";
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            url = selList;
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_url = course_url;
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            for (let sel of selList) {
                                                sel = sel.replace(/years./g, 'Years');
                                                sel = sel.replace(/year /g, 'Years');
                                                sel = sel.replace(/\(|\)/g, " ");
                                                sel = sel.replace(/time:/g, 'time ');
                                                sel = sel.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ');
                                                console.log("Sel 1111 -->>", sel);
                                                fullTimeText = sel;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("Fulltime@@@@", fullTimeText)
                            if (fullTimeText.includes("yearsor")) {
                                //console.log("YESSSSSS",fullTimeText.replace("yearsor","years"))
                                fullTimeText = fullTimeText.replace("yearsor", "years")
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    console.log("filtered_duration_formated", filtered_duration_formated);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_campus_location': { // Location Launceston
                            const courseKeyVals = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code)
                            let course_cricos_code = [];
                            let splitdata;
                            let data

                            if (courseKeyVals.includes(":")) {
                                course_cricos_code.push(courseKeyVals.split(":")[1])

                            }else{
                                course_cricos_code.push(courseKeyVals);
                            }                     
                                                   
                                                     
                           
                            var campLocationText = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (selList.includes('and')) {
                                                selList = selList.replace('/and/', ',');
                                            }
                                            for (let selItem of selList) {
                                                selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s)/g, '').split(",");
                                                console.log("selItem -->", selItem);
                                                for (let sel of selItem) {
                                                    sel = sel.trim();
                                                    campLocationText.push(sel);
                                                }
                                            } // selList
                                        }
                                    }
                                }
                            }
                            console.log("##Campus-->" + campLocationText)
                            if (campLocationText && campLocationText.length > 0) {
                                let flocation=[];
                                var location=["Elgar","Nelson","city","Lilydale"]
                                for(let i=0;i<location.length;i++){
                                    campLocationText.forEach(element=>{
                                        if(element.includes(location[i])){
                                            flocation.push(location[i])

                                        }
                                    })
                                }
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                var campusedata = [];

                                flocation.forEach(element => {
                                    campusedata.push({
                                        "name": element,
                                        "code":String(course_cricos_code)
                                       
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;//await utils.giveMeArray(campLocationValTrimmed.replace(/[\r\n\t]+/g, ';'), ';');
                                // *************** STUDY/DELIEVRY MODE IS ALSO CAPTURED HERE *************** //
                                var study_mode = [];

                                resJsonData.course_study_mode = 'On Campus';//.join(',');
                               
                            } // if (campLocationText && campLocationText.length > 0)
                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {                             
                            };
                            // feesDict.international_student = configs.propValueNotAvaialble;
                            // feesDict.fee_duration_years = configs.propValueNotAvaialble;
                            // feesDict.currency = configs.propValueNotAvaialble;
                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);

                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                                const arrval = String(feesVal1).split('.');

                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student={
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",                                              
                                                description: feesIntStudent,
                                               
                                            };

                                        }
                                    }
                                }
                            } else {
                                throw new Error("Fees not Found");
                            } // if (feesIntStudent && feesIntStudent.length > 0)



                           
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                   
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            } else {
                                throw new Error("Fees not Found");
                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (feesDict.international_student.length == 0) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }

                      
                        case 'program_code': {
                            //const courseKeyVal = courseScrappedData.program_code;
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.program_code)
                            var program_code = null;

                            if (courseKeyVal.includes(":")) {
                                program_code = courseKeyVal.split(":")[1]
                            }
                            resJsonData.program_code = program_code.trim();
                            break;
                        }
                        case 'course_intake': {
                            const courseIntakeDisplay = {};
                            // existing intake value
                            var courseIntakeStr = [];//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_intake;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (selList.includes('and')) {
                                                selList = selList.replace(/and/g, ',');
                                            }
                                            for (let selItem of selList) {
                                                if (selItem.includes('and')) {
                                                    selItem = selItem.replace(/and/g, ',');
                                                }
                                                selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\n\s|)/g, '').split(",");
                                                console.log("selItem -->", selItem);
                                                for (let sel of selItem) {
                                                    sel = sel.trim();
                                                    courseIntakeStr.push(sel +",2020");
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            if (!courseIntakeStr) {
                                throw new Error("Intake not found");
                            }
                            console.log("CourseIntakeStr@@@")
                            if (courseIntakeStr && courseIntakeStr.length > 0) {
                                var intakes = [];
                                var campus = resJsonData.course_campus_location;
                                for (let loc of campus) {
                                    var intakedetail = {};
                                    var myintake = [];
                                    myintake = courseIntakeStr
                                    //myintake = await utils.giveMeArray(courseIntakeStr[0].replace(/[\r\n\t()]+/g, '').replace('Semester 1', '').replace('Semester 2', ''), "and");
                                    intakedetail.name = loc.name;
                                    intakedetail.value = myintake;
                                    intakes.push(intakedetail);
                                }
                                let formatedIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                var intakedata = {};
                                let arr = [];
                                arr.push(await utils.getValueFromHardCodedJsonFile('intake_url'))
                                intakedata.intake = formatedIntake;
                                intakedata.more_details = arr[0];
                                resJsonData.course_intake = intakedata;//courseIntakeStr.replace(/[\r\n\t ]+/g, ' ');
                            }
                            break;
                        }
                      
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            course_country = selList[0];
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }

                        case 'course_overview': {
                            var course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);
                            if (course_overview) {
                                resJsonData.course_overview = course_overview
                            } else {
                                resJsonData.course_overview = ""
                            }
                            //resJsonData.course_overview = await Course.extractValueFromScrappedElement(courseScrappedData.course_overview);

                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                            const courseKeyVal_major = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline);
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                            let course_outlines = {};
                            if (courseKeyVal_minor != null) {
                                course_outlines.minors = courseKeyVal_minor
                            } else {
                                course_outlines.minors = []
                            }
                            if (courseKeyVal_major != null) {
                                course_outlines.majors = courseKeyVal_major
                            } else {
                                course_outlines.majors = []
                            }
                            if (courseKeyVal != null) {
                                course_outlines.more_details = courseKeyVal
                            } else {
                                course_outlines.more_details = ""
                            }
                            resJsonData.course_outline = course_outlines
                            break;
                        }
                        case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            if (courseKeyVal != null) {
                                resJsonData.application_fee = courseKeyVal
                            } else {
                                resJsonData.application_fee = ""
                            }
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                }
            }

            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id.toLowerCase();
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                let basecourseid = location_wise_data.course_id;

               
                var filelocation = "./output/" + resJsonData.univ_id + "_" + basecourseid + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.category, courseDict.href);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };



















