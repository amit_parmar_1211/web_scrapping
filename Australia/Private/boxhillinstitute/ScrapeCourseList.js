const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const puppeteer = require('puppeteer');
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'scrapeCoursePageList ';
    const URL = "https://www.boxhill.edu.au/international/courses-for-international-students/";
    let browser = await puppeteer.launch({ headless: false }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.setViewport({ width: 1366, height: 900 });
    await page.goto(URL, { timeout: 0 });
    // let selector = "#modalTitle > button";

    //click international dropdown
    // let internationalStudSel = "#degree-student-type > option:nth-child(2)";
    // await page.select("#degree-student-type", "all|international");
    const categoryselector = "//div[@class='ais-InstantSearch__root']//div[@class='tile__body']/p[@class='body-3']/span[1]";
    // //div[@class='ais-InstantSearch__root']//div[@class='tile__body']/p[@class='body-3']/span[1]
    const courseurlselector = "//div[@class='ais-InstantSearch__root']/div[1]/div[2]/div/div/a";
    const coursetitleselector = "//div[@class='ais-InstantSearch__root']//h5";
    var ispage = true;
    var totalCourseList = [];
    while (ispage) {
      const categoryval = await page.$x(categoryselector);
      const courseurlval = await page.$x(courseurlselector);
      const coursetitleval = await page.$x(coursetitleselector);
      console.log("yess")
      for (let k = 0; k < categoryval.length; k++) {
        var title = await page.evaluate(el => el.innerText, coursetitleval[k]);
        var url = await page.evaluate(el => el.href, courseurlval[k]);
        var category = await page.evaluate(el => el.innerText, categoryval[k]);
        totalCourseList.push({ href: url, innerText: title, category: category });
      }
      var nextpageselector = "//ul[@class='ais-Pagination-list']/li[@class='ais-Pagination-item ais-Pagination-item--nextPage']";
      const nextpageelm = await page.$x(nextpageselector);
      if (nextpageelm[0]) {
        await nextpageelm[0].click();
        await page.waitFor(2000);
        console.log("next page!");
      }
      else {
        ispage = false;
      }
    }
    console.log("Total courses >>" + totalCourseList.length);
    var uniquecases = [], uniquedata = [], finalDict = [];
    totalCourseList.forEach(element => {
      if (!uniquecases.includes(element.href)) {
        uniquecases.push(element.href);
        uniquedata.push(element);
      }
    });
    uniquecases.forEach(element => {
      var dataobj = {};
      var data = totalCourseList.filter(e => e.href == element);
      var category = [];
      data.forEach(element => {
        if (!category.includes(element.category))
          category.push(element.category);
      });
      dataobj.href = element;
      dataobj.innerText = data[0].innerText;
      dataobj.category = (Array.isArray(category)) ? category : category;
      finalDict.push(dataobj);
    });

    fs.writeFileSync("./output/boxhillinstitute-all_courselist.json", JSON.stringify(totalCourseList));
    fs.writeFileSync("./output/boxhillinstitute_courselist.json", JSON.stringify(finalDict));
    await browser.close();
    console.log("Close browser successfully.");
  }
} // class

module.exports = { ScrapeCourseList };
