const fs = require("fs");
const Scrape = require("./common/scrape").Scrape;
const utils = require("./common/utils");
const configs = require("./configs");
const awsUtil = require("./common/aws_utils");
const appConfigs = require("./common/app-config");
//const puppeteer = require('puppeteer');

class ScrapeCourseList extends Scrape {
  // refers to https://humber.ca/search/full-time.html
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = "scrapeCourseCategoryList ";
    try {
      // category of course
      // await s.setupNewBrowserPage("https://humber.ca/search/full-time.html");
      let selectinternational = await page.$x(
        "/html/body/div[5]/div/div/div/div[1]/div/div[2]/a[2]"
      );
      selectinternational[0].click();
      await page.waitFor(5000);

      let totalcourselist = [];
      let courseurl = await page.$x(
        "//*[@id='programs-table']/tbody/tr/td[1]/a"
      );
      let degree = await page.$x(
        "//*[@id='programs-table']/tbody/tr/td[contains(@class,'degree')]"
      );
      let category = await page.$x(
        "//*[@id='programs-table']/tbody/tr/td[contains(@class,'faculty')]"
      );
      let campus = await page.$x(
        "//*[@id='programs-table']/tbody/tr/td[contains(@class,'location')]/a"
      );

      for (let i = 0; i < courseurl.length; i++) {
        //console.log("yesss");
        let href = await page.evaluate(el => el.href, courseurl[i]);
        let innertexts = await page.evaluate(el => el.innerText, courseurl[i]);
        let degrees = await page.evaluate(el => el.innerText, degree[i]);
        let categorys = await page.evaluate(el => el.innerText, category[i]);
        let campuses = await page.evaluate(el => el.innerText, campus[i]);
        console.log(degrees);
        totalcourselist.push({
          href: href,
          innertext: innertexts + "_" + degrees,
          category: categorys,
          campus: campuses
        });
      }

      console.log(funcName + "writing courseList to file....");
      // fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(allcategory))
      await fs.writeFileSync(
        "./output/universityofalberta_original_courselist.json",
        JSON.stringify(totalcourselist)
      );
      console.log("totalcourselist", totalcourselist);
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalcourselist.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalcourselist[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({
              href: totalcourselist[i].href,
              innertext: totalcourselist[i].innertext,
              campus: totalcourselist[i].campus,
              category: []
            });
          }
        } else {
          uniqueUrl.push({
            href: totalcourselist[i].href,
            innertext: totalcourselist[i].innertext,
            campus: totalcourselist[i].campus,
            category: []
          });
        }
      }
      await fs.writeFileSync(
        "./output/universityofalberta_unique_courselist.json",
        JSON.stringify(uniqueUrl)
      );
      for (let i = 0; i < totalcourselist.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalcourselist[i].href) {
            if (uniqueUrl[j].category.includes(totalcourselist[i].category)) {
            } else {
              uniqueUrl[j].category.push(totalcourselist[i].category);
            }
          }
        }
      }
      console.log("totalcourselist -->", uniqueUrl);

      await fs.writeFileSync(
        "./output/universityofalberta_courselist.json",
        JSON.stringify(uniqueUrl)
      );
    } catch (error) {
      console.log(funcName + "try-catch error = " + error);
    }
  }

  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = "scrapeCoursePageList ";
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw new Error("Invalif file data, fileData = " + fileData);
        }
        const dataJson = JSON.parse(fileData);
        console.log(
          funcName + "Success in getting local data so returning local data...."
        );
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl(
        "course_list_selector",
        this.selectorJson
      );
      console.log(
        funcName + "rootEleDictUrl = " + JSON.stringify(rootEleDictUrl)
      );
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);

      if (s) {
        await s.close();
      }
    } catch (error) {
      console.log(funcName + "try-catch error = " + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw error;
    }
  }

  // refers to https://programsandcourses.anu.edu.au/catalogue
  async scrapeCourseList(selFilepath) {
    const funcName = "scrapeCoursePageList ";
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey(
        "course_category_selector",
        this.selectorJson
      );
      if (!selectorsList) {
        console.log(funcName + "Invalid selectorsList");
        throw new Error("Invalid selectorsList");
      }
      const sel = selectorsList[0];
      console.log(funcName + "sel = " + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey(
        "course_category_selector",
        this.selectorJson
      );
      if (!elementDict) {
        console.log(funcName + "Invalid elementDict");
        throw new Error("Invalid elementDict");
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl(
        "course_category_selector",
        this.selectorJson
      );
      if (!rootEleDictUrl) {
        console.log(funcName + "Invalid rootEleDictUrl");
        throw new Error("Invalid rootEleDictUrl");
      }
      const courseCatDictList = await s.scrapeElement(
        elementDict.elementType,
        sel,
        rootEleDictUrl,
        null
      );
      console.log(
        funcName + "courseCatDictList = " + JSON.stringify(courseCatDictList)
      );

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey(
        "course_list_selector",
        this.selectorJson
      );
      if (!coursLvlSelList) {
        console.log(funcName + "Invalid coursLvlSelList");
        throw new Error("Invalid coursLvlSelList");
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey(
        "course_list_selector",
        this.selectorJson
      );
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + "Invalid elementDict");
        throw new Error("Invalid elementDict");
      }

      // for each course-category, scrape all courses
      let totalcourselist = [];
      let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + "count = " + count);
        // click on href
        console.log(funcName + "catDict.href = " + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + "selItem = " + selItem);
        const res = await s.scrapeElement(
          elementDictForCorsLvlKey.elementType,
          selItem,
          catDict.href,
          null
        );
        console.log(funcName + "res = " + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalcourselist = totalcourselist.concat(res);
        } else {
          throw new Error("res is not array, it must be...");
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + "writing courseList to file....");
      // console.log(funcName + 'totalcourselist = ' + JSON.stringify(totalcourselist));
      await fs.writeFileSync(
        configs.opCourseListFilepath,
        JSON.stringify(totalcourselist)
      );
      console.log(
        funcName + "writing courseList to file completed successfully...."
      );
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + "object location = " + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + "try-catch error = " + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw error;
    }
  }
} // class

module.exports = { ScrapeCourseList };
