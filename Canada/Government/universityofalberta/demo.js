const puppeteer = require("puppeteer");
async function calculatemyfees(studylevel, course) {
  // var myfee;
  try {
    const URL = "https://costcalculator.registrar.ualberta.ca/costcalculator/";
    let browser = await puppeteer.launch({ headless: false }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });

    let checkinter = await page.$x("//*[@id='FORGN']");
    await checkinter[0].click();
    await page.waitFor(5000);
    let clicknext = await page.$x("//*[@id='citizenship']/div[2]/button");
    await clicknext[0].click();
    await page.waitFor(5000);
    let choose = await page.$x("//*[@id='answerNo']");
    await choose[0].click();
    await page.waitFor(2000);
    let choosestudylevel = await page.$x(
      "//*[@id='facultySelected']/option[contains(text(),'" + studylevel + "')]"
    );
    //console.log("choosestudylevel", choosestudylevel.length);
    var elementstring = await page.evaluate(
      el => el.value,
      choosestudylevel[0]
    );
    console.log("value" + elementstring);
    await page.select("#facultySelected", elementstring);
    await page.waitFor(5000);
    let chosecourse = await page.$x(
      "//*[@id='programSelected']/option[./text()='" + course + "']"
    );
    var values = await page.evaluate(el => el.value, chosecourse[0]);

    //console.log("//*[@id='programSelected']/option[./text()='" + course + "']");
    await page.select("#programSelected", values);
    let nextclick = await page.$x('//*[@id="step1"]/fieldset/div[2]/button[2]');
    await nextclick[0].click();
    await page.waitFor(2000);
    let choosecampus = await page.$x("//*[@id='off-campus']");
    await choosecampus[0].click();
    let nextclicks = await page.$x("//*[@id='housing']/div[2]/button[2]");
    await nextclicks[0].click();
    await page.waitFor(2000);
    let checkreview = await page.$x("//*[@id='ReviewFees']");
    await checkreview[0].click();
    await page.waitFor(2000);
    let durations = await page.$x(
      "//*[@id='answer4']/h4[1]/strong[contains(text(),'Degree Length:')]/following::span[1]"
    );
    let duration = await page.evaluate(el => el.innerText, durations[0]);
    let fees = await page.$x(
      "//*[@id='instructionalFee']/table/tbody/tr/td[contains(text(),'Tuition Fee')]/following::td[1]/span"
    );
    let finalfee = await page.evaluate(el => el.innerText, fees[0]);
    console.log(duration + "_" + finalfee);

    await browser.close();
  } catch (e) {
    console.log("Error--->" + e);
  }
  // myfee.replace(/[$,]/g, "").trim();
}

async function start() {
  console.log(await calculatemyfees("Business", "Bachelor of Commerce"));
}
start();
