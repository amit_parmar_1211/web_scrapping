const fs = require("fs");
const Scrape = require("./common/scrape").Scrape;
const Course = require("./common/course").Course;
const utils = require("./common/utils");
const appConfigs = require("./common/app-config");
const configs = require("./configs");
const format_functions = require("./common/format_functions");
request = require("request");

class ScrapeCourse extends Course {
  // format courseScrappedData, format output json
  static async formatOutput(
    courseScrappedData,
    course_name,
    course_category,
    campus_location
  ) {
    console.log("data@@@@@@@@");
    const funcName = "formatOutput ";
    try {
      Scrape.validateParams([courseScrappedData]);
      const resJsonData = {};
      var demoarray = [];
      var finalfees;
      for (const key in courseScrappedData) {
        console.log("\n\r" + funcName + "key = " + key);
        if (courseScrappedData.hasOwnProperty(key)) {
          switch (key) {
            case "univ_id": {
              console.log(funcName + "configs.univ_id = " + configs.univ_id);
              if (!configs.univ_id && configs.univ_id.length > 0) {
                console.log(funcName + "configs.univ_id must have valid value");
                throw new Error("configs.univ_id must have valid value");
              }
              resJsonData.univ_id = configs.univ_id;
              break;
            }
            case "course_discipline": {
              //  resJsonData.course_discipline = [course_category];
              if (course_category) {
                resJsonData.course_discipline = course_category;
              } else {
                resJsonData.course_discipline = [];
              }
              break;
            }
            case "course_title_category":
            case "course_title": {
              let course_title = await Course.extractValueFromScrappedElement(
                courseScrappedData.course_title
              );
              let sub_title = await Course.extractValueFromScrappedElement(
                courseScrappedData.course_study_level
              );
              let titles;
              console.log("Course_title==>", course_title);
              if (sub_title.includes("-")) {
                sub_title = sub_title.replace("-", "_");
              }
              titles = sub_title + "_" + course_title;

              let course_title_text = format_functions.titleCase(
                String(titles).trim()
              );
              console.log("course_title_text==>", course_title_text);
              resJsonData.course_title = course_title_text;
              break;
            }
            case "ielts_req":
            case "pte_req":
            case "ibt_req":
            case "course_admission_academic_more_details":
            case "course_academic_requirement":
            case "course_toefl_ielts_score": {
              // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
              const courseAdminReq = {};
              const englishList = [];
              const ieltsDict = {};
              const pibtDict = {};
              const ppbtDict = {};
              const ppteDict = {};
              const pcaeDict = {};
              let othercourses = [];
              let othercourse;
              let ielts, ibt, pte, pbt, cae;
              const cStudyLevels = resJsonData.course_study_level;
              if (cStudyLevels.includes("_Co-op")) {
                cStudyLevels = cStudyLevels.replace("_Co-op", "").trim();
              }
              console.log("StudyLevel!!!" + JSON.stringify(cStudyLevels));
              if (cStudyLevels) {
                othercourse = await utils.getValueFromHardCodedJsonFile(
                  "ielts_mapping"
                );
                othercourse.forEach(element => {
                  console.log(element);
                  if (
                    element.KEY.toLowerCase().includes(
                      cStudyLevels.toLowerCase()
                    )
                  ) {
                    console.log("Element->", element);
                    othercourses = element;
                  }
                });
                ielts = othercourses["IELTS"];
                ibt = othercourses["IBT"];
                pte = othercourses["PTE"];

                console.log("ielts@@@@", ielts);
                console.log("ibt@@@@", ibt);
              }

              //   if (potherLngDict) {
              var ieltsScore = "",
                ibtScore = "",
                pteScore = "",
                caeScore = "",
                pbtScore = "";
              if (ielts) {
                ieltsScore = await utils.giveMeNumber(ielts);
                console.log("### IELTS data-->" + ieltsScore);
              }
              if (ibt) {
                ibtScore = await utils.giveMeNumber(ibt);
              }
              if (pte) {
                pteScore = await utils.giveMeNumber(pte);
              }

              if (ieltsScore) {
                ieltsDict.name = "ielts academic";
                ieltsDict.description = ielts;
                ieltsDict.min = 0;
                ieltsDict.require = ieltsScore;
                ieltsDict.max = 9;
                ieltsDict.R = 0;
                ieltsDict.W = 0;
                ieltsDict.S = 0;
                ieltsDict.L = 0;
                ieltsDict.O = 0;
                englishList.push(ieltsDict);
              }
              if (ibtScore) {
                pibtDict.name = "toefl ibt";
                pibtDict.description = ibt;
                pibtDict.min = 0;
                pibtDict.require = ibtScore;
                pibtDict.max = 120;
                pibtDict.W = 0;
                pibtDict.S = 0;
                pibtDict.L = 0;
                pibtDict.O = 0;
                englishList.push(pibtDict);
              }

              if (pteScore) {
                ppteDict.name = "pte academic";
                ppteDict.description = pte;
                ppteDict.min = 0;
                ppteDict.require = pteScore;
                ppteDict.max = 90;
                ppteDict.W = 0;
                ppteDict.S = 0;
                ppteDict.L = 0;
                ppteDict.O = 0;
                englishList.push(ppteDict);
              }
              // if (caeScore != "NA") {
              //     pcaeDict.name = 'cae academic';
              //     pcaeDict.description = cae
              //     pcaeDict.min = 80;
              //     pcaeDict.require = caeScore;
              //     pcaeDict.max = 230;
              //     pcaeDict.W = 0;
              //     pcaeDict.S = 0;
              //     pcaeDict.L = 0;
              //     pcaeDict.O = 0;
              //     englishList.push(pcaeDict);
              // }

              if (englishList && englishList.length > 0) {
                courseAdminReq.english = englishList;
              } else {
                courseAdminReq.english = [];
              }

              var academic_more = await utils.getValueFromHardCodedJsonFile(
                "academic_requirements_url"
              );
              var english_more = await utils.getValueFromHardCodedJsonFile(
                "english_requirements_url"
              );
              var entry_requirements_url = await utils.getValueFromHardCodedJsonFile(
                "entry_requirements_url"
              );
              var academicReq = null;
              const courseKeyVal =
                courseScrappedData.course_academic_requirement;
              console.log("Academic_req-->" + JSON.stringify(courseKeyVal));
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(
                    funcName +
                      "\n\r rootEleDict = " +
                      JSON.stringify(rootEleDict)
                  );
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(
                      funcName + "\n\r eleDict = " + JSON.stringify(eleDict)
                    );
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      academicReq = selList;
                    }
                  }
                }
              }
              courseAdminReq.academic = academicReq
                ? [academicReq.toString()]
                : [];
              courseAdminReq.english_requirements_url = english_more;
              courseAdminReq.academic_requirements_url = academic_more;
              courseAdminReq.entry_requirements_url = "";
              resJsonData.course_admission_requirement = courseAdminReq;
              break;
            }

            case "course_url": {
              let newUrl = courseScrappedData.course_url;
              let rescourse_url = null;
              if (Array.isArray(newUrl)) {
                for (const rootEleDict of newUrl) {
                  console.log(
                    funcName +
                      "\n\r rootEleDict courseurl= " +
                      JSON.stringify(rootEleDict)
                  );
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(
                      funcName + "\n\r eleDict = " + JSON.stringify(eleDict)
                    );
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(
                        funcName +
                          "\n\r selList courseurl= " +
                          JSON.stringify(selList)
                      );
                      rescourse_url = selList;
                    }
                  }
                }
              }
              if (rescourse_url) {
                resJsonData.course_url = rescourse_url;
              }
              break;
            }
            case "course_duration_full_time": {
              let duration = [];
              let studylevel = await Course.extractValueFromScrappedElement(
                courseScrappedData.course_study_level
              );
              duration = await format_functions.getfeesandduration(
                course_category,
                studylevel
              );
              console.log("Duration----->" + duration[0]);
              finalfees = duration[1];
              // display full time
              var courseDurationDisplayList = [],
                durationFullTime = {},
                courseDurationList = [];
              //resFulltime = [];
              let fullTimeText;
              let spl;
              // const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time);
              const courseKeyVal = duration[0] + " years";
              console.log(
                "SplcourseKeyVal----->>>>>>",
                JSON.stringify(courseKeyVal)
              );
              let resFulltime = courseKeyVal;
              if (resFulltime) {
                durationFullTime.duration_full_time = resFulltime;
                //courseDurationList.push(durationFullTime);
                courseDurationDisplayList.push(
                  await format_functions.validate_course_duration_full_time(
                    resFulltime
                  )
                );
                let filtered_duration_formated = await format_functions.getfilterduration(
                  courseDurationDisplayList[0]
                );

                if (resFulltime && resFulltime.length > 0) {
                  resJsonData.course_duration = resFulltime;
                }
                if (
                  filtered_duration_formated &&
                  filtered_duration_formated.length > 0
                ) {
                  resJsonData.course_duration_display = filtered_duration_formated;
                  var isfulltime = false,
                    isparttime = false;
                  filtered_duration_formated.forEach(element => {
                    if (element.display == "Full-Time") {
                      isfulltime = true;
                    }
                    if (element.display == "Part-Time") {
                      isparttime = true;
                    }
                  });
                  resJsonData.isfulltime = isfulltime;
                  resJsonData.isparttime = isparttime;
                }
              }
              console.log(
                "resJsonData.course_duration_display",
                resJsonData.course_duration_display
              );
              break;
            }
            case "course_tuition_fee":
            case "course_study_mode":
            case "page_url":
            case "select_english_as_ielts":
            case "select_english_as_ibt":
            case "course_toefl_ibt_indicator":
            case "select_english_as_pbt":
            case "course_toefl_toefl_pbt_score":
            case "course_duration":
            case "course_tuition_fee_duration_years":
            case "course_tuition_fees_currency":
            case "course_tuition_fees_year":
            case "course_admission_requirement":
            case "course_campus_location": {
              let campusedata = [];
              console.log("Campus==>", campus);
              campusedata.push({
                name: campus_location,
                code: ""
              });

              resJsonData.course_campus_location = campusedata;

              break;
            }
            case "course_study_mode": {
              // Location Launceston
              //resJsonData.course_study_mode = coursestudymodeText;
              resJsonData.course_study_mode = "On campus";
              break;
            }
            case "course_tuition_fees_international_student_more":
            case "course_tuition_fees_international_student": {
              // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
              const courseTuitionFee = {};
              // courseTuitionFee.year = configs.propValueNotAvaialble;
              let campusedata111 = [];
              const feesList = [];
              const feesDict = {};
              let finalduration;
              const Tuition_Fee = finalfees;
              if (Tuition_Fee && Tuition_Fee.length > 0) {
                // extract only digits
                const feesWithDollorTrimmed = Tuition_Fee.trim();
                console.log(
                  funcName + "feesWithDollorTrimmed = " + feesWithDollorTrimmed
                );
                // const feesVal1 = String(feesWithDollorTrimmed).replace("$", "");
                const arrval = Tuition_Fee.split(".");
                const feesVal = String(arrval[0]);
                console.log(funcName + "feesVal = " + feesVal);
                if (feesVal) {
                  const regEx = /\d/g;
                  let feesValNum = feesVal.match(regEx);
                  if (feesValNum) {
                    console.log(funcName + "feesValNum = " + feesValNum);
                    feesValNum = feesValNum.join("");
                    console.log(funcName + "feesValNum = " + feesValNum);
                    let feesNumber = null;
                    if (feesValNum.includes(",")) {
                      feesNumber = parseInt(feesValNum.replace(/,/g, ""), 10);
                    } else {
                      feesNumber = feesValNum;
                    }

                    console.log(funcName + "feesNumber = " + feesNumber);
                    if (Number(feesNumber)) {
                      feesDict.international_student = {
                        amount: Number(feesNumber),
                        duration: 1,
                        unit: "Year",
                        description: finalfees
                      };
                    } else {
                      feesDict.international_student = {
                        amount: 0,
                        duration: 1,
                        unit: "Year",
                        description: "not available fee"
                      };
                    }
                    console.log(
                      "feesDictinternational_student-->",
                      feesDict.international_student
                    );
                  }
                }
              } else {
                throw new Error("not international course");
              } // if (feesIntStudent && feesIntStudent.length > 0)
              // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf

              if (feesDict.international_student) {
                // if we have fees value then add following supporting attributes
                const feesDuration = await Course.extractValueFromScrappedElement(
                  courseScrappedData.course_tuition_fee_duration_years
                );
                const feesCurrency = await Course.extractValueFromScrappedElement(
                  courseScrappedData.course_tuition_fees_currency
                );
                if (feesDuration && feesDuration.length > 0) {
                  feesDict.fee_duration_years = feesDuration;
                }

                if (feesCurrency && feesCurrency.length > 0) {
                  feesDict.currency = feesCurrency;
                }
                if (feesDict) {
                  feesList.push({ name: campus_location, value: feesDict });
                }
                if (feesList && feesList.length > 0) {
                  courseTuitionFee.fees = feesList;
                }
                console.log(
                  funcName +
                    "course_tuition_fee = " +
                    JSON.stringify(courseTuitionFee)
                );
                if (courseTuitionFee) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                }
                // take tuition fee value at json top level so will help in DDB for indexing
                if (courseTuitionFee && feesDict.international_student) {
                  resJsonData.course_tuition_fee = courseTuitionFee;
                  // resJsonData.course_tuition_fee_amount = feesDict.international_student;
                }
              }
              console.log(
                funcName +
                  "feesDict.international_student = " +
                  feesDict.international_student
              );
              // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
              if (!feesDict.international_student) {
                console.log("\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r");
                console.log(
                  funcName +
                    "Invalid TUITION FEE AMOUNT so break formatting function and continue with next course...."
                );
                console.log(
                  funcName +
                    "feesDict.international_student = " +
                    feesDict.international_student
                );
                console.log("\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r");
                return null; // this will add this item into FailedItemList and writes file to local disk
              }
              break;
            }

            case "program_code": {
              const courseKeyVal = courseScrappedData.program_code;
              var program_code = "";
              console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(
                    funcName +
                      "\n\r rootEleDict = " +
                      JSON.stringify(rootEleDict)
                  );
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(
                      funcName + "\n\r eleDict = " + JSON.stringify(eleDict)
                    );
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(
                        funcName +
                          "\n\r programcode programcode selList = " +
                          JSON.stringify(selList)
                      );
                      if (Array.isArray(selList) && selList.length > 0) {
                        program_code = selList[0];
                      }
                    }
                  }
                }
              }
              if (program_code != "" && program_code.length > 0) {
                const selList = [];
                console.log("program_code3 -->", program_code);
                resJsonData.program_code = String(program_code);
              } else {
                // program_code = [];
                // program_code.push("NA");
                resJsonData.program_code = "";
              }
              break;
            }
            case "course_intake": {
              // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
              const courseIntakeDisplay = [];
              // existing intake value
              // const courseIntakeStrElem = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);

              let courseintake;
              if (campus == "Augustana Campus (Camrose)") {
                courseintake = await utils.getValueFromHardCodedJsonFile(
                  "intake_augustana"
                );
              } else if (course_category == "intake_law") {
                courseintake = await utils.getValueFromHardCodedJsonFile(
                  "intake_augustana"
                );
              } else {
                courseintake = await utils.getValueFromHardCodedJsonFile(
                  "intakes"
                );
              }

              console.log(
                "************************Start course_intake******************************"
              );
              console.log("courseIntakeStr : " + courseintake);
              console.log("==courseintake===>", courseintake);
              let arr = [];
              if (courseintake && courseintake.length > 0) {
                resJsonData.course_intake = courseintake;

                if (courseintake && courseintake.length > 0) {
                  for (var part of courseintake) {
                    console.log("R part", part);
                    courseIntakeDisplay.push(part.trim());
                  } // for
                }
                console.log(
                  "course_intake courseIntakeDisplay = " +
                    JSON.stringify(courseIntakeDisplay)
                );
              }
              var campus = resJsonData.course_campus_location;
              if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
                resJsonData.course_intake = {};
                resJsonData.course_intake.intake = [];
                for (let location11 of campus) {
                  resJsonData.course_intake.intake.push({
                    name: location11.name,
                    value: courseIntakeDisplay
                  });
                }
              }
              console.log("intakes123 -->", resJsonData.course_intake.intake);
              let more_details = await utils.getValueFromHardCodedJsonFile(
                "intake_url"
              );
              let formatedIntake = await format_functions.providemyintake(
                resJsonData.course_intake.intake,
                "mom",
                ""
              );
              // console.log(JSON.stringify(providemyintake(intake, "MOM", "")));
              console.log("Intakes --> ", JSON.stringify(formatedIntake));
              var intakedata = {};
              intakedata.intake = formatedIntake;
              intakedata.more_details = more_details;
              resJsonData.course_intake = intakedata;
              break;
            }
            case "course_country": {
              const courseKeyVal = courseScrappedData.course_country;
              let course_country = null;
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(
                    funcName +
                      "\n\r rootEleDict = " +
                      JSON.stringify(rootEleDict)
                  );
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(
                      funcName + "\n\r eleDict = " + JSON.stringify(eleDict)
                    );
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(
                        funcName + "\n\r selList = " + JSON.stringify(selList)
                      );
                      //if (Array.isArray(selList) && selList.length > 0) {
                      course_country = selList[0];
                      //}
                    }
                  }
                }
              }
              resJsonData.course_country = course_country;
              break;
            }

            case "course_overview": {
              console.log(funcName + "matched case: " + key);
              const rootElementDictList = courseScrappedData[key];
              console.log(
                funcName + "key-val is " + JSON.stringify(rootElementDictList)
              );
              let concatnatedRootElementsStr = null;
              for (const rootEleDict of rootElementDictList) {
                console.log(
                  funcName + "\n\r rootEleDict = " + JSON.stringify(rootEleDict)
                );
                const elementsList = rootEleDict.elements;
                let concatnatedElementsStr = null;
                for (const eleDict of elementsList) {
                  console.log(
                    funcName + "\n\r eleDict = " + JSON.stringify(eleDict)
                  );
                  const selectorsList = eleDict.selectors;
                  let concatnatedSelectorsStr = null;
                  for (const selList of selectorsList) {
                    console.log(
                      funcName + "\n\r selList = " + JSON.stringify(selList)
                    );
                    console.log(funcName + "\n\r selList = " + selList);
                    let concatnatedSelStr = null;
                    for (const selItem of selList) {
                      if (!concatnatedSelStr) {
                        concatnatedSelStr = selItem;
                      } else {
                        concatnatedSelStr = String(concatnatedSelStr)
                          .concat(" ")
                          .concat(selItem)
                          .replace(/[\r\n\t ]+/g, " ")
                          .trim();
                      }
                    } // selList
                    console.log(
                      funcName + "concatnatedSelStr = " + concatnatedSelStr
                    );
                    if (concatnatedSelStr) {
                      if (!concatnatedSelectorsStr) {
                        concatnatedSelectorsStr = concatnatedSelStr;
                      } else {
                        concatnatedSelectorsStr = String(
                          concatnatedSelectorsStr
                        )
                          .concat(" ")
                          .concat(concatnatedSelStr)
                          .replace(/[\r\n\t ]+/g, " ")
                          .trim();
                      }
                    }
                  } // selectorsList
                  console.log(
                    funcName +
                      "concatnatedSelectorsStr = " +
                      concatnatedSelectorsStr
                  );
                  // concat elements
                  if (concatnatedSelectorsStr) {
                    if (!concatnatedElementsStr) {
                      concatnatedElementsStr = concatnatedSelectorsStr;
                    } else {
                      concatnatedElementsStr = String(concatnatedElementsStr)
                        .concat(" ")
                        .concat(concatnatedSelectorsStr)
                        .replace(/[\r\n\t ]+/g, " ")
                        .trim();
                    }
                  }
                } // elementsList
                console.log(
                  funcName +
                    "concatnatedElementsStr = " +
                    concatnatedElementsStr
                );
                if (concatnatedElementsStr) {
                  if (!concatnatedRootElementsStr) {
                    concatnatedRootElementsStr = concatnatedElementsStr;
                  } else {
                    concatnatedRootElementsStr = String(
                      concatnatedRootElementsStr
                    )
                      .concat(" ")
                      .concat(concatnatedElementsStr)
                      .replace(/[\r\n\t ]+/g, " ")
                      .trim();
                  }
                }
              } // rootElementDictList
              console.log(
                funcName +
                  "concatnatedRootElementsStr =11 " +
                  concatnatedRootElementsStr
              );
              // add only if it has valid value
              // let concatnatedRootElementsStrArray = [];
              if (
                concatnatedRootElementsStr &&
                concatnatedRootElementsStr.length > 0
              ) {
                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                resJsonData[key] = String(concatnatedRootElementsStr)
                  .replace(/[\r\n\t ]+/g, " ")
                  .trim();
                // console.log("resJsonData[key] -->11",resJsonData[key]);
              }
              break;
            }
            case "course_career_outcome": {
              const courseKeyVal = courseScrappedData.course_career_outcome;
              let course_career_outcome = [];
              console.log("courseKeyVal_outcome", courseKeyVal);
              if (Array.isArray(courseKeyVal)) {
                for (const rootEleDict of courseKeyVal) {
                  console.log(
                    funcName +
                      "\n\r course_career_outcome rootEleDict = " +
                      JSON.stringify(rootEleDict)
                  );
                  const elementsList = rootEleDict.elements;
                  for (const eleDict of elementsList) {
                    console.log(
                      funcName +
                        "\n\r course_career_outcome eleDict = " +
                        JSON.stringify(eleDict)
                    );
                    const selectorsList = eleDict.selectors;
                    for (const selList of selectorsList) {
                      console.log(
                        funcName +
                          "\n\r course_career_outcome selList = " +
                          JSON.stringify(selList)
                      );
                      //if (Array.isArray(selList) && selList.length > 0) {
                      course_career_outcome = selList;
                      //}
                    }
                  }
                }
              }
              if (course_career_outcome.length > 0) {
                resJsonData.course_career_outcome = course_career_outcome;
              } else {
                resJsonData.course_career_outcome = [];
              }
              break;
            }

            case "course_study_level": {
              let attach;
              const cTitle = await Course.extractValueFromScrappedElement(
                courseScrappedData.course_study_level
              );
              if (
                cTitle.includes("Bachelor") ||
                cTitle.includes("Certificate")
              ) {
                resJsonData.course_study_level = "Undergraduate";
              }
              if (cTitle.includes("Co-operative")) {
                attach = resJsonData.course_study_level + "_Co-op";
              } else {
                attach = resJsonData.course_study_level;
              }
              resJsonData.course_study_level = attach;

              break;
            }
            case "course_outline": {
              const courseKeyVal_minor = await Course.extractValueFromScrappedElement(
                courseScrappedData.course_outline_minor
              );
              const courseKeyVal_major = await Course.extractValueFromScrappedElement(
                courseScrappedData.course_outline
              );
              const courseKeyVal = await Course.extractValueFromScrappedElement(
                courseScrappedData.course_outline_moredetails
              );
              let course_outlines = {};
              if (courseKeyVal_minor != null) {
                course_outlines.minors = courseKeyVal_minor;
              } else {
                course_outlines.minors = [];
              }
              if (courseKeyVal_major != null) {
                course_outlines.majors = courseKeyVal_major;
              } else {
                course_outlines.majors = [];
              }
              if (courseKeyVal != null) {
                course_outlines.more_details = courseKeyVal;
              } else {
                course_outlines.more_details = "";
              }
              resJsonData.course_outline = course_outlines;
              break;
            }
            case "application_fees": {
              const courseKeyVal = await Course.extractValueFromScrappedElement(
                courseScrappedData.application_fees
              );
              if (courseKeyVal != null) {
                resJsonData.application_fees = courseKeyVal;
              } else {
                resJsonData.application_fees = "";
              }
            }
            default: {
              console.log(
                funcName +
                  "none of mach found with the case, so executing default and not processing anything..."
              );
              break;
            } // default case
          } //
          console.log(
            funcName +
              resJsonData[key] +
              " = " +
              JSON.stringify(resJsonData[key])
          );
        } // if (courseScrappedData.hasOwnProperty(key))
      } // for (const key in courseScrappedData)
      var NEWJSONSTRUCT = {},
        structDict = [];
      console.log(
        "#location-->" + JSON.stringify(resJsonData.course_campus_location)
      );
      var locations = resJsonData.course_campus_location;
      for (let location of locations) {
        NEWJSONSTRUCT.course_id = resJsonData.course_title
          .replace(/:/g, "")
          .replace(/\s/g, "")
          .replace(/[\/]+/g, "_")
          .toLowerCase();
        console.log("##Course Title -->", NEWJSONSTRUCT.course_id);
        NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
        NEWJSONSTRUCT.course_title = resJsonData.course_title;
        NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
        NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
        NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
        NEWJSONSTRUCT.course_url = resJsonData.course_url;
        //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
        console.log(
          "##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code)
        );
        NEWJSONSTRUCT.course_campus_location = location.name;
        NEWJSONSTRUCT.course_tuition_fee =
          resJsonData.course_tuition_fee.fees[0].international_student;
        NEWJSONSTRUCT.currency =
          resJsonData.course_tuition_fee.fees[0].currency;
        NEWJSONSTRUCT.fee_duration_years =
          resJsonData.course_tuition_fee.fees[0].fee_duration_years;
        NEWJSONSTRUCT.course_duration_display =
          resJsonData.course_duration_display;
        NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
        var intakes = resJsonData.course_intake.intake;
        var matchrec = [];
        for (let dintake of intakes) {
          if (location == dintake.name) {
            matchrec = dintake.value;
          }
        }
        if (matchrec.length > 0) {
          NEWJSONSTRUCT.course_intake = matchrec[0];
        } else {
          NEWJSONSTRUCT.course_intake = "";
        }
        for (let myfees of resJsonData.course_tuition_fee.fees) {
          if (myfees.name == location) {
            NEWJSONSTRUCT.international_student_all_fees = myfees;
          }
        }
        structDict.push(NEWJSONSTRUCT);
        NEWJSONSTRUCT = {};
      }
      for (let location_wise_data of structDict) {
        console.log("location::::" + location_wise_data.course_campus_location);
        resJsonData.course_id = location_wise_data.course_location_id;
        //resJsonData.basecourseid = location_wise_data.course_id;
        // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
        //     if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
        //         resJsonData.course_cricos_code[i].iscurrent = true;
        //     }
        //     else {
        //         resJsonData.course_cricos_code[i].iscurrent = false;
        //     }
        // }

        var filelocation =
          "./output/" +
          resJsonData.univ_id +
          "_" +
          location_wise_data.course_location_id +
          "_details.json";
        console.log("Write file--->" + filelocation);
        fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
      }
      return resJsonData;
    } catch (error) {
      console.log(funcName + "try-catch error = " + error);
      throw error;
    }
  }

  // scrape each course details
  static async scrapeAndFormatCourseDetails(courseDict) {
    const funcName = "scrapeAndFormatCourseDetails ";
    let s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      await s.setupNewBrowserPage(courseDict.href);

      Scrape.validateParams([courseDict]);
      console.log(
        "courseDict.innertext",
        courseDict.innertext,
        courseDict.href
      );
      // get course href and innerText from courseDict and ensure it is valid
      if (
        !(
          courseDict &&
          courseDict.href &&
          courseDict.href.length > 0 &&
          courseDict.innertext &&
          courseDict.innertext.length > 0
        )
      ) {
        console.log(
          funcName +
            "Invalid courseDict, courseDict = " +
            JSON.stringify(courseDict)
        );
        throw new Error(
          "Invalid courseDict, courseDict = " + JSON.stringify(courseDict)
        );
      }
      const courseUrl = courseDict.href;
      const courseTitle = courseDict.innertext;
      console.log(funcName + "courseTitle = " + courseTitle);
      console.log(funcName + "courseUrl = " + courseUrl);
      // output course data and file path
      let courseScrappedData = null;
      // create courseId from course title
      const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(
        courseTitle
      );
      // validate generated courseId
      console.log(funcName + "courseId = " + courseId);
      if (!(courseId && courseId.length > 0)) {
        console.log(funcName + "Invalid courseId, courseId = " + courseId);
        throw new Error("Invalid courseId, courseId = " + courseId);
      }
      // configure output file path as per UpdateItem configuration
      const opCourseFilePath = await utils.generateOutputCourseFilePath(
        courseDict
      );
      // decide if need to scrape or read data from local outpurfile
      if (appConfigs.shouldTakeFromOutputFolder) {
        // read data from local file
        console.log(
          funcName + "reading scrapped data from local output file..."
        );
        const fileData = fs.readFileSync(opCourseFilePath);
        if (!fileData) {
          console.log(funcName + "Invalid fileData, fileData = " + fileData);
          throw new Error("Invalid fileData, fileData = " + fileData);
        }
        courseScrappedData = JSON.parse(fileData);
        return courseScrappedData;
      } // if (appConfigs.shouldTakeFromOutputFolder)
      // Scrape course data as per selector file

      //custom call

      if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
        console.log(
          funcName +
            "reading selUpdateCourseDetailsSelectorsJsonFilepath = " +
            appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath
        );
        courseScrappedData = await s.scrapeSelectorFile(
          appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath
        );
      } else {
        console.log(
          funcName +
            "reading selCourseDetailsSelectorsJsonFilepath = " +
            appConfigs.selCourseDetailsSelectorsJsonFilepath
        );
        courseScrappedData = await s.scrapeSelectorFile(
          appConfigs.selCourseDetailsSelectorsJsonFilepath
        );
      }
      if (!courseScrappedData) {
        throw new Error(
          "Invalid courseScrappedData, courseScrappedData = " +
            courseScrappedData
        );
      }

      const formattedScrappedData = await ScrapeCourse.formatOutput(
        courseScrappedData,
        courseDict.innertext,
        courseDict.category,
        courseDict.campus,
        s.page.url()
      );
      const finalScrappedData = await Course.removeKeys(formattedScrappedData);
      if (s) {
        await s.close();
      }
      return finalScrappedData;
    } catch (error) {
      console.log(funcName + "try-catch error = " + error);
      if (s) {
        await s.close();
      }
      throw error;
    }
  } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
