const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
//const puppeteer = require('puppeteer');

class ScrapeCourseList extends Scrape {
  // refers to https://humber.ca/search/full-time.html
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // category of course
      // await s.setupNewBrowserPage("https://humber.ca/search/full-time.html");
      let urlarr=[
        "https://www.utoronto.ca/academics/programs-directory?field_degrees_value=1&keys=",
        "https://www.utoronto.ca/academics/programs-directory?field_degrees_value=2&keys="
      ]
      var mainCategory = [], redirecturl = [];
      //scrape main category
      let urlarr=["https://www.utoronto.ca/academics/programs-directory?field_degrees_value=1&keys=",
     "https://www.utoronto.ca/academics/programs-directory?field_degrees_value=2&keys="]
     
     let totalcourselist=[]

     for(let url of urlarr){
       await page.goto(url,{timeout:0})
      
     
       let ispage=true;
       while(ispage){
        const urlselecotor=await page.$x("//*[@class='view-content clearfix']/div/div/div/a");
        const titleselector=await page.$x("//*[@class='view-content clearfix']/div/div/div/a/h4");
        const campusselector=await page.$x("//*[@class='view-content clearfix']/div/div/div/a/p[contains(@class,'campus')]");
        const degreeselector=await page.$x("//*[@class='view-content clearfix']/div/div/div/a/p[contains(@class,'degrees')]")
         for(let i=0;i<urlselecotor.length;i++){
           let href=await page.evaluate(el=>el.href,urlselecotor[i])
           let innertext=await page.evaluate(el=>el.innerText,titleselector[i])
           let campus=await page.evaluate(el=>el.innerText,campusselector[i])
           let degree=await page.evaluate(el=>el.innerText,degreeselector[i])
           totalcourselist.push({href:href,innertext:innertext,campus:campus,degree:degree})
         }
         const activateselector=await page.$x("//*[@id='block-system-main']/div/div/ul/li[contains(@class,'pager-next')]/a")
         if(activateselector[0]){
           activateselector[0].click();
           console.log("next page!")
           await page.waitFor(10000)
         }else{
          ispage = false;
         }
       }
       await fs.writeFileSync("./output/universityoftoronto_original_courselist.json", JSON.stringify(totalcourselist));
       console.log(funcName + 'writing courseList to file....');
      // fs.writeFileSync("./output/maincategorylist.json", JSON.stringify(allcategory))
     

     }
     let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalcourselist.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalcourselist[i].innertext == uniqueUrl[j].innertext) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalcourselist[i].href, innerText: totalcourselist[i].innertext, campus: [],degree:totalcourselist[i].degree });
          }
        } else {
          uniqueUrl.push({ href: totalcourselist[i].href, innerText: totalcourselist[i].innertext, campus: [],degree:totalcourselist[i].degree });
        }
      }
      await fs.writeFileSync("./output/univetsityoftoronto_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalcourselist.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].innertext == totalcourselist[i].innertext) {
            uniqueUrl[j].campus.push(totalcourselist[i].campus);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);

      await fs.writeFileSync("./output/universityuoftoronto_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }

  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);

      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }


  // refers to https://programsandcourses.anu.edu.au/catalogue
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }


      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        // /////// TEMP /////////
        // if (count >= 2) {
        //   break;
        // }
        // /////// TEMP /////////
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };
