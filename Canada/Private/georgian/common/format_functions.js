var pdfreader = require('pdfreader');
const puppeteer = require('puppeteer');
const fs = require('fs');
readline = require('readline');
request = require('request');
const http = require("https");


const WAIT_FOR_SEL_OPTIONS = {
    timeout: 5000, // ms
};
function getfilterduration(duration) {
    var newduration = [];
    duration.forEach(element => {
        switch (element.unit) {
            case 'semesters': case 'semester': {
                element.filterduration = element.duration / 2;
                console.log("Semester match::" + element.filterduration);
                break;
            }
            case 'trimister': case 'trimisters': {
                element.filterduration = element.duration / 3;
                console.log("Trimister match::" + element.filterduration);
                break;
            }
            case 'month': case 'months': {
                element.filterduration = element.duration / 12;
                console.log("Months match::" + element.filterduration);
                break;
            }
            case 'day': case 'days': {
                element.filterduration = element.duration / 365;
                console.log("Days match::" + element.filterduration);
                break;
            }
            case 'week': case 'weeks': {
                element.filterduration = element.duration / 52.1429;
                console.log("Weeks match::" + element.filterduration);
                break;
            }
            default: {
                element.filterduration = element.duration / 1;
                console.log("Nothing match::" + element.filterduration);
                break;
            }
        }
        newduration.push(element);
    });
    return newduration;
}
function providemyintake(oldintake, format, separator) {
    var newintake = [];

    const fullmonths = [{ fullkey: "January", shortkey: "Jan", intake: "Summer" }, { fullkey: "February", shortkey: "Feb", intake: "Summer" },
    { fullkey: "March", shortkey: "Mar", intake: "Autumn" }, { fullkey: "April", shortkey: "Apr", intake: "Autumn" }, { fullkey: "May", shortkey: "May", intake: "Autumn" }, { fullkey: "June", shortkey: "June", intake: "Winter" },
    { fullkey: "July", shortkey: "Jul", intake: "Winter" }, { fullkey: "August", shortkey: "Aug", intake: "Winter" }, { fullkey: "September", shortkey: "Sept", intake: "Spring" },
    { fullkey: "October", shortkey: "Oct", intake: "Spring" }, { fullkey: "November", shortkey: "Nov", intake: "Spring" }, { fullkey: "December", shortkey: "Dec", intake: "Summer" },
    { fullkey: "September", shortkey: "Sept", intake: "Fall" },
    { fullkey: "June", shortkey: "June", intake: "Winter" }];

    oldintake.forEach(element => {
        var newdataafterreplace = {}, replacedata = [];
        element.value.forEach(elementdate => {
            var date = {};
            switch (format) {
                case "MOM": {
                    fullmonths.forEach(elementmon => {
                        if (elementdate.toLowerCase().indexOf(elementmon.fullkey.toLowerCase()) != -1) {
                            date.actualdate = elementdate;
                            date.month = elementmon.fullkey;
                            //    date.intake = elementmon.intake;
                        }
                        else if (elementdate.toLowerCase().indexOf(elementmon.intake.toLowerCase()) != -1) {
                            console.log("elementdate", elementdate);
                            date.actualdate = elementdate;
                            date.month = elementmon.fullkey;
                            // date.intake = elementmon.intake;
                        }
                    });
                    break;
                }
                case "mom": {
                    fullmonths.forEach(elementmon => {
                        if (elementdate.toLowerCase().indexOf(elementmon.shortkey.toLowerCase()) != -1) {
                            date.actualdate = elementdate;
                            date.month = elementmon.fullkey;
                            // date.intake = elementmon.intake;
                        }
                    });
                    break;
                }
                case "ddmmyyyy": {
                    var elementmon = fullmonths[elementdate.split(separator)[1] - 1];
                    date.actualdate = elementdate;
                    date.month = elementmon.fullkey;
                    //date.intake = elementmon.intake;
                    break;
                }
                case "mmddyyyy": {
                    var elementmon = fullmonths[elementdate.split(separator)[0] - 1];
                    date.actualdate = elementdate;
                    date.month = elementmon.fullkey;
                    //                    date.intake = elementmon.intake;
                    break;
                }
            }
            replacedata.push(date);
        });
        newdataafterreplace.name = element.name;
        newdataafterreplace.value = replacedata;
        newintake.push(newdataafterreplace);
    });
    return newintake;
}
async function getMyStudyLevel(code) {
    let studylevelval = '', browser = null;
    try {
        const URL = "http://cricos.education.gov.au/Course/CourseDetails.aspx?CourseCode=";
        browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
        let page = await browser.newPage();
        await page.goto(URL + code, { timeout: 0 });
        const codeselector = "//*/label[contains(text(),'Course Level:')]/following-sibling::div/span";
        const studylevelelm = await page.$x(codeselector);
        studylevelval = await page.evaluate(el => el.innerText, studylevelelm[0]);
    } catch (error) {
        console.log('try-catch error = ' + error);
    }
    await browser.close();
    return studylevelval;
}
async function scrape_by_xpath(page, selector) {
    var retDict = [];
    try {
        //await page.waitForSelector(selector, WAIT_FOR_SEL_OPTIONS);
        const targetLinks = await page.$x(selector);
        for (let link of targetLinks) {
            var elementstring = await page.evaluate(el => el.innerText, link);
            retDict.push(elementstring)
        }
    }
    catch (e) {
        console.log("Error--->" + e);
    }
    return await retDict;
}

async function not_formated_course_duration(yeartext) {
    var retValueDict = [];
    try {
        yeartext = yeartext[0].elements[0].selectors[0][0];
        var yeardata = yeartext.toLowerCase();
        console.log("### text to formate for full time year--->" + yeartext);
        console.log("### data after split--->" + yeardata);
        retValueDict = yeardata;
        console.log("#### formated year--->" + JSON.stringify(retValueDict));
    }
    catch (err) {
        console.log("#### try-catch error--->" + err);
    }
    return await retValueDict;
}
async function course_list_scrape(page, selector) {
    var courselist = [];
    try {
        for (let sel of selector) {
            switch (sel.action) {
                case "WAIT": {
                    if (sel.istime) {
                        await page.waitFor(sel.time);
                    }
                    else {
                        await page.waitForNavigation();
                    }
                    break;
                }
                case "CLICK": {
                    if (sel.isxpath) {
                        await page.$x(sel.selector, el => el.click());
                    }
                    else {
                        await page.$eval(sel.selector, el => el.click());
                    }
                    break;
                }
                case "SCRAPE": {
                    if (!sel.isxpath) {
                        for (let link of sel.selector) {
                            const scrape_element_url = await page.$$(link.urlselector);
                            const scrape_element_text = await page.$$(link.textselector);
                            for (let count = 0; count < scrape_element_url.length; count++) {
                                var elementstring = await page.evaluate(el => el.innerText, scrape_element_text[count]);
                                var elementurl = await page.evaluate(el => el.href, scrape_element_url[count]);
                                if (sel.iscomb) {
                                    const combtext = await page.evaluate(el => el.innerText, scrape_element_url[count]);
                                    elementstring = combtext.toString().trim() + " " + elementstring.toString().trim();
                                }
                                courselist.push({ href: elementurl, innerText: elementstring });
                            }
                        }
                    }
                    else {
                        for (let link of sel.selector) {
                            const scrape_element_url = await page.$x(link.urlselector);
                            const scrape_element_text = await page.$x(link.textselector);
                            for (let count = 0; count < scrape_element_url.length; count++) {
                                var elementstring = await page.evaluate(el => el.innerText, scrape_element_text[count]);
                                var elementurl = await page.evaluate(el => el.href, scrape_element_url[count]);
                                if (sel.iscomb) {
                                    const combtext = await page.evaluate(el => el.innerText, scrape_element_url[count]);
                                    elementstring = combtext.toString().trim() + " " + elementstring.toString().trim();
                                }
                                courselist.push({ href: elementurl, innerText: elementstring });
                            }
                        }
                    }
                    break;
                }
            }
        }
    }
    catch (e) {
        console.log("Error--->" + e);
    }
    console.log(JSON.stringify(courselist));
    return await courselist;
}
async function course_outline_scrape(page, selector) {
    var courselist = [], course_outline = {};
    try {
        console.log("####Action--->" + selector.action);
        // for (let sel of selector) {
        let sel = selector;
        switch (sel.action) {
            case "WAIT": {
                if (sel.istime) {
                    await page.waitFor(sel.time);
                }
                else {
                    await page.waitForNavigation();
                }
                break;
            }
            case "ANCHOR_CLICK": {
                var links = null;
                if (sel.isxpath) {
                    links = await page.$x(sel.selector);
                }
                else {
                    links = await page.$$(sel.selector);
                }
                for (let link of links) {
                    var propelem = await link.getProperty("target");
                    var proptext = await propelem.jsonValue()
                    console.log("##ANCHOR_CLICK--target->" + proptext)
                    if (!proptext || proptext == "self" || proptext == "_self") {
                        await link.click();
                    }
                    else if (proptext && (proptext == "blank" || proptext == "_blank")) {
                        var propelem = await link.getProperty("href");
                        var proptext = await propelem.jsonValue()
                        console.log("##ANCHOR_CLICK--href->" + proptext)
                        page.goto(proptext, { timeout: 0 });
                        //page.waitForNavigation();
                    }
                }
                break;
            }
            case "REMOVE_ATTR": {
                await page.evaluate(sel => {
                    console.log("### selector-->" + sel.selector);
                    console.log("### selectorRM-->" + sel.removeattr);
                    return document.querySelector(sel.selector).removeAttribute(sel.removeattr);
                }, sel);
                break;
            }
            case "CLICK": {
                if (sel.isxpath) {
                    const links = await page.$x(sel.selector);
                    for (let link of links) {
                        await link.click();
                    }
                }
                else {
                    await page.$eval(sel.selector, el => el.click());
                }
                break;
            }
            case "SCRAPE": {
                if (!sel.isxpath) {
                    for (let link of sel.selector) {
                        const scrape_element_url = await page.$$(link.urlselector);
                        const scrape_element_text = await page.$$(link.textselector);
                        const selectorLength = scrape_element_url.length;
                        var counterlength = (selectorLength > sel.limit) ? sel.limit : selectorLength;
                        console.log("counterlength : " + counterlength);
                        for (let count = 0; count < counterlength; count++) {
                            if (sel.isurl) {
                                var elementstring = await page.evaluate(el => el.innerText, scrape_element_text[count]);
                                var elementurl = await page.evaluate(el => el.href, scrape_element_url[count]);
                                if (sel.iscomb) {
                                    const combtext = await page.evaluate(el => el.innerText, scrape_element_url[count]);
                                    elementstring = combtext.toString().trim() + " " + elementstring.toString().trim();
                                }
                                if (elementurl && elementstring) {
                                    courselist.push({ href: elementurl, innerText: elementstring.replace(/[\r\n\t]+/g, '').trim() });
                                }
                            }
                            else {
                                var elementstring = await page.evaluate(el => el.innerText, scrape_element_text[count]);
                                if (sel.iscomb) {
                                    const combtext = await page.evaluate(el => el.innerText, scrape_element_url[count]);
                                    elementstring = combtext.toString().trim() + " " + elementstring.toString().trim();
                                }
                                if (elementstring) {
                                    courselist.push(elementstring.replace(/[\r\n\t]+/g, '').trim());
                                }
                            }
                        }
                    }
                }
                else {
                    for (let link of sel.selector) {
                        const scrape_element_url = await page.$x(link.urlselector);
                        const scrape_element_text = await page.$x(link.textselector);
                        const selectorLength = scrape_element_url.length;
                        var counterlength = (selectorLength > sel.limit) ? sel.limit : selectorLength;
                        console.log("counterlength : " + counterlength);
                        for (let count = 0; count < counterlength; count++) {
                            if (sel.isurl) {
                                var elementstring = await page.evaluate(el => el.innerText, scrape_element_text[count]);
                                var elementurl = await page.evaluate(el => el.href, scrape_element_url[count]);
                                if (sel.iscomb) {
                                    const combtext = await page.evaluate(el => el.innerText, scrape_element_url[count]);
                                    elementstring = combtext.toString().trim() + " " + elementstring.toString().trim();
                                }
                                if (elementurl && elementstring) {
                                    courselist.push({ href: elementurl, innerText: elementstring.replace(/[\r\n\t]+/g, '').trim() });
                                }
                            }
                            else {
                                var elementstring = await page.evaluate(el => el.innerText, scrape_element_text[count]);
                                if (sel.iscomb) {
                                    const combtext = await page.evaluate(el => el.innerText, scrape_element_url[count]);
                                    elementstring = combtext.toString().trim() + " " + elementstring.toString().trim();
                                }
                                if (elementstring && elementstring.length > 0 && !elementstring.toLowerCase().includes("units") && !elementstring.toLowerCase().includes("----------")) {
                                    courselist.push(elementstring.replace(/[\r\n\t]+/g, '').trim());
                                }
                            }
                        }
                    }
                }
                break;
            }
        }
        //}
    }
    catch (e) {
        console.log("Error--->" + e);
    }
    if (selector.action == "SCRAPE") {
        if (selector.isurl) {
            course_outline.subject_areas = courselist;
        }
        else {
            course_outline.study_units = courselist;
        }

        course_outline.more_details = page.url();
        return await course_outline;
    }
}

async function action_scrape(page, selector) {
    var courselist = [], course_outline = {};
    try {
        console.log("####Action--->" + selector.action);
        // for (let sel of selector) {
        let sel = selector;
        switch (sel.action) {
            case "WAIT": {
                if (sel.istime) {
                    await page.waitFor(sel.time);
                }
                else {
                    await page.waitForNavigation();
                }
                break;
            }
            case "ANCHOR_CLICK": {
                var links = null;
                if (sel.isxpath) {
                    links = await page.$x(sel.selector);
                }
                else {
                    links = await page.$$(sel.selector);
                }
                for (let link of links) {
                    var propelem = await link.getProperty("target");
                    var proptext = await propelem.jsonValue()
                    console.log("##ANCHOR_CLICK--target->" + proptext)
                    if (!proptext || proptext == "self" || proptext == "_self") {
                        await link.click();
                    }
                    else if (proptext && (proptext == "blank" || proptext == "_blank")) {
                        var propelem = await link.getProperty("href");
                        var proptext = await propelem.jsonValue()
                        console.log("##ANCHOR_CLICK--href->" + proptext)
                        page.goto(proptext, { timeout: 0 });
                        //page.waitForNavigation();
                    }
                }
                break;
            }
            case "REMOVE_ATTR": {
                await page.evaluate(sel => {
                    console.log("### selector-->" + sel.selector);
                    console.log("### selectorRM-->" + sel.removeattr);
                    return document.querySelector(sel.selector).removeAttribute(sel.removeattr);
                }, sel);
                break;
            }
            case "CLICK": {
                if (sel.isxpath) {
                    const links = await page.$x(sel.selector);
                    for (let link of links) {
                        await link.click();
                    }
                }
                else {
                    await page.$eval(sel.selector, el => el.click());
                }
                break;
            }
            case "SCRAPE": {
                if (!sel.isxpath) {
                    for (let link of sel.selector) {
                        const scrape_element_url = await page.$$(link);
                        for (let elementsel of scrape_element_url) {
                            var elementstring = await page.evaluate(el => el.innerText, elementsel);
                            courselist.push(elementstring);
                        }
                    }
                }
                else {
                    for (let link of sel.selector) {
                        const scrape_element_url = await page.$x(link);
                        for (let elementsel of scrape_element_url) {
                            var elementstring = await page.evaluate(el => el.innerText, elementsel);
                            courselist.push(elementstring);
                        }
                    }
                }
                break;
            }
        }
        //}
    }
    catch (e) {
        console.log("Error--->" + e);
    }
    if (selector.action == "SCRAPE") {
        return await courselist;
    }
}

async function downloadPDF(pdfDetail) {
    try {
        const file = fs.createWriteStream(pdfDetail.filename);
        http.get(pdfDetail.URL, response => {
            response.pipe(file);
        });
        console.log("Writing downloaded PDF file to " + pdfDetail.filename + "...");
    }
    catch (err) {
        console.log("#### try-catch error--->" + err);
    }
    return await "";
}

async function scrapePDF(pdfDetail) {
    try {
        console.log("pdfDetail.filepath", pdfDetail)
        if (fs.existsSync(pdfDetail.filepath)) {
            fs.unlinkSync(pdfDetail.filepath);
        }
        const nbCols = 2;
        const cellPadding = 40; // each cell is padded to fit 40 characters
        const columnQuantitizer = (item) => parseFloat(item.x) >= 20;
        const padColumns = (array, nb) =>
            Array.apply(null, { length: nb }).map((val, i) => array[i] || []);
        // .. because map() skips undefined elements
        const mergeCells = (cells) => (cells || [])
            .map((cell) => cell.text).join(' | ') // merge cells
        //.substr(0, cellPadding).padEnd(cellPadding, ' '); // padding

        const renderMatrix = (matrix) => (matrix || [])
            .map((row, y) => padColumns(row, nbCols)
                .map(mergeCells)
                .join(' | ')
            ).join('\n');
        var table = new pdfreader.TableParser();
        new pdfreader.PdfReader().parseFileItems(pdfDetail.pfdpath, function (err, item) {
            if (!item || item.page) {
                // end of file, or page
                var filedata = JSON.stringify(renderMatrix(table.getMatrix()));
                filedata = filedata.split('\\n');
                console.log("##File Data--->" + JSON.stringify(filedata))
                for (var d of filedata) {
                    var JSONDATA = [];
                    if (fs.existsSync(pdfDetail.filepath)) {
                        JSONDATA = JSON.parse(fs.readFileSync(pdfDetail.filepath));
                    }
                    var mycode = d.split("|");
                    const key = pdfDetail.key;
                    console.log("mycode[pdfDetail.cricodeposition]", mycode.length)
                    console.log("pdfDetail.cricodelength", key.length)

                    if (mycode.length == key.length) {
                        if (mycode[pdfDetail.cricodeposition]) {

                            if (mycode[pdfDetail.cricodeposition].trim().length == pdfDetail.cricodelength) {
                                console.log("YESSSS")
                                var myjsondata = {};
                                for (var count = 0; count < key.length; count++) {
                                    myjsondata[key[count]] = mycode[count].trim();
                                }
                                JSONDATA.push(myjsondata);
                                fs.writeFileSync(pdfDetail.filepath, JSON.stringify(JSONDATA))
                            }
                        }
                    }
                }
                table = new pdfreader.TableParser(); // new/clear table for next page
            } else if (item.text) {
                // accumulate text items into rows object, per line
                table.processItem(item, columnQuantitizer(item));
            }
            if (err) {
                console.log("###Error--->" + err);
            }
        });
    }
    catch (err) {
        console.log("#### try-catch error--->" + err);
    }
    return await "";
}
async function readanyfile(fileDetail) {
    console.log("####Readfromfile-->" + global.course_name);
    var returnKeydata = "";
    try {
        const filedata = JSON.parse(fs.readFileSync(fileDetail.filepath));
        if (fileDetail.value == "global.course_name") {
            fileDetail.value = global.course_name;
        }
        if (fileDetail.value == "global.cricos_code") {
            fileDetail.value = global.cricos_code;
        }
        for (let data of filedata) {
            if (data[fileDetail.comparekeycolumn] == fileDetail.value) {
                console.log("###match" + data[fileDetail.returnkey]);
                returnKeydata = data[fileDetail.returnkey];
            }
        }
    }
    catch (err) {
        console.log("#### try-catch error--->" + err);
    }
    return await (returnKeydata) ? returnKeydata : "NA";
}
//for duration
async function validate_course_duration_full_time(searchstring) {
    searchstring = searchstring.toString().toLowerCase();
    searchstring = searchstring.replace(/[\r\n\t ]+/g, ' ')
    searchstring = searchstring.replace(/(?:,|,)/g, '');
    const _units = ["semester", "semesters", "year", "years", "year.", "years.", "month", "months", "day", "days", "week", "weeks", "quater", "quaters"];
    const _duration_type_fulltime = ["full-time", "fulltime", "full time", "full - time"];
    const _duration_type_parttime = ["part-time", "parttime", "part time", "part - time"];
    const _unitpoints = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"];

    const _duration_type_fulltime_replace = [
        { "key": "full-time.", "value": "fulltime" },
        { "key": "fulltime.", "value": "fulltime" },
        { "key": "full time.", "value": "fulltime" },
        { "key": "full - time.", "value": "fulltime" },
        { "key": "year.", "value": "year" },
        { "key": "years.", "value": "years" }
    ];
    const _duration_type_parttime_replace = [
        { "key": "part-time.", "value": "parttime" },
        { "key": "parttime.", "value": "parttime" },
        { "key": "part time.", "value": "parttime" },
        { "key": "part - time.", "value": "parttime" }
    ];
    _duration_type_fulltime_replace.forEach(element => {
        if (searchstring.includes(element.key)) {
            searchstring = searchstring.replace(element.key, element.value)
        }
    });
    _duration_type_parttime_replace.forEach(element => {
        if (searchstring.includes(element.key)) {
            console.log("Includes--->");
            searchstring = searchstring.replace(element.key, element.value)
        }
    });

    var _isfulltime = false, _isparttime = false, _fulltimematch = "", _parttimematch = "";
    var _duration = [];
    console.log("After replace process-->" + searchstring)
    _duration_type_fulltime.forEach(element => {
        if (searchstring.includes(element)) {
            _isfulltime = true;
            //fulltimematch = "full";
            _fulltimematch = element;
        }
    });
    _duration_type_parttime.forEach(element => {
        if (searchstring.includes(element)) {
            _isparttime = true;
            //_parttimematch = "part";
            _parttimematch = element;
        }
    });
    if (_isfulltime) {
        var _fulltimeduration = {};
        console.log("### match fulltime-->");
        var matchdata = {};
        matchdata.unit = _fulltimematch;
        var retuit = await matchstring(searchstring, matchdata, _units);
        _fulltimeduration.unit = retuit.unit;
        retuit.unit = _fulltimematch;
        var retdur = await matchstring(searchstring, retuit, _unitpoints);
        //_fulltimeduration.isdegit = retdur.isdegit;
        if (retdur.isdegit) {
            _fulltimeduration.duration = retdur.unit;
        }
        else {
            _fulltimeduration.duration = (_unitpoints.indexOf(retdur.unit) + 1).toString();
        }
        _fulltimeduration.display = "Full-Time";
        _duration.push(_fulltimeduration);
    }
    if (_isparttime) {
        var _parttimeduration = {};
        console.log("### match parttime-->");
        if (searchstring.includes("equivalent")) {
            const durationval = _duration[0];
            console.log("## durationdata-->" + JSON.stringify(durationval));
            _parttimeduration.unit = durationval.unit;
            //_parttimeduration.duration = (durationval.duration * 2)
            // if (durationval.isdegit) {
            var durationdata = durationval.duration.split("-");
            console.log("## durationdata-->" + durationdata);
            if (durationdata.length > 1) {
                _parttimeduration.duration = ((durationdata[0] * 2) + "-" + (durationdata[1] * 2)).toString();
            }
            else {
                _parttimeduration.duration = (durationdata[0] * 2).toString();
            }
            // }
        }
        else {
            var matchdata = {};
            console.log("##Not equivalent")
            matchdata.unit = _parttimematch;
            var retuit = await matchstring(searchstring, matchdata, _units);
            _parttimeduration.unit = retuit.unit;
            retuit.unit = _parttimematch;
            var retdur = await matchstring(searchstring, retuit, _unitpoints);
            //_parttimeduration.unit = retuit.unit;
            if (retdur.isdegit) {
                _parttimeduration.duration = retdur.unit;
            }
            else {
                _parttimeduration.duration = (_unitpoints.indexOf(retdur.unit) + 1).toString();
            }
        }
        _parttimeduration.display = "Part-Time";
        _duration.push(_parttimeduration);
    }
    if (!_isfulltime && !_isparttime) {
        var _fulltimeduration = {};
        console.log("### match nothing proceed default-->");
        var matchdata = {};
        _units.forEach(element => {
            if (searchstring.includes(element)) {
                matchdata.unit = element;
                var data = searchstring.split(" ");
                matchdata.index = data.indexOf(element);
                matchdata.isdegit = false;
            }
        });

        //var retuit = await matchstring(searchstring, matchdata, _units);
        //console.log("mainUNIT-->" + JSON.stringify(retuit));
        var retdur = await matchstring(searchstring, matchdata, _unitpoints);
        console.log("subUNIT-->" + JSON.stringify(retdur));
        _fulltimeduration.unit = matchdata.unit;
        //_fulltimeduration.isdegit = retdur.isdegit;
        if (retdur.isdegit) {
            _fulltimeduration.duration = retdur.unit;
        }
        else {
            _fulltimeduration.duration = (_unitpoints.indexOf(retdur.unit) + 1);
        }
        _fulltimeduration.display = "Full-Time";
        _duration.push(_fulltimeduration);
    }
    console.log("## scraped data for duration-->" + JSON.stringify(_duration));
    return await _duration;
}
async function matchstring(searchstring, closeto, searchelments) {
    var duration = {}
    try {
        console.log("## matchstring called" + JSON.stringify(closeto));
        const seperated_searchstring = searchstring.replace(/\u00a0/g, " ").split(" ");
        console.log("## seperated elements-->" + JSON.stringify(seperated_searchstring))
        var index = 0;
        // if (closeto.index > 0) {
        //   index = closeto.index;
        //  } else {
        index = seperated_searchstring.indexOf(closeto.unit);
        //}

        console.log("## Index of close-->" + closeto.unit + "" + index)
        var preindex = 0, nextindex = 0, loop = true;
        while (loop) {
            if ((index - preindex) >= 0) {
                if (hasNumber((seperated_searchstring[(index - preindex)]))) {
                    console.log("##Has number");
                    duration.unit = (seperated_searchstring[(index - preindex)]);
                    duration.index = (index - preindex);
                    duration.isdegit = true;
                } else {
                    searchelments.forEach(element => {
                        if (!duration.unit) {
                            if ((seperated_searchstring[(index - preindex)] === element)) {
                                duration.unit = element;
                                duration.index = (index - preindex);
                                duration.isdegit = false;
                            }
                        }
                    });
                }

                preindex++;
            }
            if (!duration.unit) {
                if ((index + nextindex) <= seperated_searchstring.length) {
                    if (hasNumber((seperated_searchstring[(index - preindex)]))) {
                        console.log("##Has number");
                        duration.unit = (seperated_searchstring[(index - preindex)]);
                        duration.index = (index - preindex);
                        duration.isdegit = true;
                    } else {
                        searchelments.forEach(element => {
                            if (!duration.unit) {
                                if ((seperated_searchstring[(index + nextindex)] === element)) {
                                    duration.unit = element;
                                    duration.index = (index + nextindex);
                                    duration.isdegit = false;
                                }
                            }
                        });
                    }
                    nextindex++;
                }
            }
            if (duration.unit)
                loop = false;
        }
    }
    catch (e) {
        console.log("## Error-->" + e);
    }

    console.log("## match data-->" + JSON.stringify(duration));
    return await duration;
}
function hasNumber(myString) {
    return /\d/.test(myString);
}
async function multi_selector_scrape(page, selector) {
    console.log("multi_selector_scrape called");
    var outerArray = [];
    try {
        var allLinks = [];
        let link = selector;
        //  for (let link of selector) {
        if (link.isxpath) {
            for (let innerlink of link.selectorelement) {
                const links = await page.$x(innerlink)
                allLinks.push(links);
            }
        }
        else {
            for (let innerlink of link.selectorelement) {
                const links = await page.$$(innerlink)
                allLinks.push(links);
            }
        }

        //}

        for (let link of allLinks) {
            var innerArray = [];
            for (let innerlink of link) {
                const myvalue = await page.evaluate(el => el.innerText, innerlink);
                innerArray.push(myvalue);
            }
            outerArray.push(innerArray);
        }
    }
    catch (e) {
        console.log("Error--->" + e);
    }
    console.log("## retDict-->" + JSON.stringify(outerArray));
    return await outerArray;
}
function titleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}
module.exports = {
    validate_course_duration_full_time,
    not_formated_course_duration,
    scrape_by_xpath,
    course_outline_scrape,
    course_list_scrape,
    downloadPDF,
    scrapePDF,
    readanyfile,
    action_scrape,
    multi_selector_scrape,
    getfilterduration,
    providemyintake,
    getMyStudyLevel,
    titleCase
};
