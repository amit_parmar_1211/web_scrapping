
const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'scrapeCourseCategoryList ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: false });
      var totalCourseList = [];

      await s.setupNewBrowserPage("https://www.fanshawec.ca/programs-and-courses");
      var mainCategory = [], redirecturl = [];

      var clickbtn = await s.page.$x('//*//div[@class="search-tabset"]/span[2]');
      await clickbtn[0].click();
      const category_val = "//*[@id='edit-field-area-wrapper']/div/div/div/div/div/label";
      const levels = await s.page.$x(category_val)
      const multipleId = await s.page.$x("//*[@id='edit-field-area-wrapper']/div/div/div/div/div/input");
      for (let i = 0; i < multipleId.length; i++) {
        var title = await s.page.evaluate(el => el.innerText, levels[i]);
        var id = await s.page.evaluate(el => el.value, multipleId[i])
        redirecturl.push({ id: id, title: title });
        mainCategory.push(title);
      }

      for (let j = 0; j < redirecturl.length; j++) {
        console.log("redirecturl[i].id  -->", redirecturl[j].id);
        await s.page.goto("https://www.fanshawec.ca/programs-and-courses?search_api_views_fulltext=&field_area%5B%5D=" + redirecturl[j].id + "&availability_time=any")
        const subjects = await s.page.$x("//*[@id='block-system-main']/div/div/div[2]/div/div/span/a");
        for (let i = 0; i < subjects.length; i++) {
          var elementstring = await s.page.evaluate(el => el.innerText, subjects[i]);
          var elementlink = await s.page.evaluate(el => el.href, subjects[i])
          totalCourseList.push({ href: elementlink, innerText: elementstring, category: redirecturl[j].title })
        }

      }
      console.log("subjectAreasArray-->", mainCategory)

      //  fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(mainCategory))
      // "//*[@class='vc_column-inner ']/div/h2[(contains(text(),'" + catc + "'))]/following::div[3]//div[contains(@class,'stm-btn-container stm-btn-container_right')]/a")
      // fs.writeFileSync("./output/canberrainsituteoftechnology_courselist.json", JSON.stringify(datalist))
      fs.writeFileSync("./output/fanshawecollege_maincategorylist.json", JSON.stringify(mainCategory))
      console.log("totalCourseList -->", totalCourseList);
      await fs.writeFileSync("./output/fanshawecollege_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
      await fs.writeFileSync("./output/fanshawecollege_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/fanshawecollege_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  };
}
module.exports = { ScrapeCourseList };


