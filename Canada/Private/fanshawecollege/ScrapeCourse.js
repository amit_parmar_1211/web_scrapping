const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = category_name;
                            break;
                        }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: courseUrl


                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        case 'application_fee': {
                            resJsonData.application_fee = "100";
                        }
                        case 'course_title_category':
                        case 'course_title': {

                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            resJsonData.course_title = title



                            break;
                        }

                        case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':
                      


                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            var penglishList = [];
                            var potherLngDict = null, ieltsdic = null;
                            // english requirement
                            let scrappedIELTS = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                            //let scrappedPBT = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_pbt);
                            let scrappedIBT = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_ibt);
                            let scrappedCAE = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_cae);
                            let scrappedPTE = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_pte);
                            console.log("scrappedIELTS ->", scrappedIELTS)
                            // console.log("scrappedPBT ->", scrappedPBT)
                            console.log("scrappedIBT ->", scrappedIBT)
                            console.log("scrappedCAE ->", scrappedCAE)
                            console.log("scrappedPTE ->", scrappedPTE)
                            if (scrappedIELTS != null && scrappedIELTS.length > 0) {
                                let ieltsDict = {};

                                // englishList.push(ieltsDict);

                                var ieltsScore = await utils.giveMeNumber(scrappedIELTS);
                                console.log("ieltsssscore---->", ieltsScore);
                                if (ieltsScore != []) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = scrappedIELTS.replace(/OR/, '').trim();
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsScore;
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = ieltsDict;
                                    englishList.push(ieltsDict);
                                }
                            }
                            // var matches = str.match(/(\d+)/);

                            if (scrappedIBT != null && scrappedIBT.length > 0) {
                                let ieltsDict = {};

                                //   console.log("ieltsDict.description----->", ieltsDict.description);
                                // englishList.push(ieltsDict);

                                let ibtScore = scrappedIBT.match(/(\d+)/)[0];
                                // var ibtScore = ibtScore1.replace(/"/g,"");
                                console.log("ibtsvcore---->", ibtScore);
                                if (ibtScore != []) {
                                    ieltsDict.name = 'toefl ibt';
                                    ieltsDict.description = scrappedIBT.replace(/OR/, '').trim();
                                    ieltsDict.min = 0;
                                    ieltsDict.require = Number(ibtScore);
                                    console.log("idshdsdshsfhfsh-->", ieltsDict.require);
                                    ieltsDict.max = 120;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = value;
                                    englishList.push(ieltsDict);
                                }
                            }
                            if (scrappedCAE != null && scrappedCAE.length > 0) {
                                let ieltsDict = {};

                                //englishList.push(ieltsDict);

                                var caeScore = await utils.giveMeNumber(scrappedCAE);
                                if (caeScore != []) {
                                    ieltsDict.name = 'cae';
                                    ieltsDict.description = scrappedCAE.replace(/OR/, '').trim();
                                    ieltsDict.min = 80;
                                    ieltsDict.require = caeScore;
                                    ieltsDict.max = 230;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = value;
                                    englishList.push(ieltsDict);
                                }
                            }
                            if (scrappedPTE != null && scrappedPTE.length > 0) {
                                let ieltsDict = {};

                                // englishList.push(ieltsDict);

                                var pteScore = await utils.giveMeNumber(scrappedPTE);
                                if (pteScore != []) {
                                    ieltsDict.name = 'pte academic';
                                    ieltsDict.description = scrappedPTE.replace(/OR/, '').trim();
                                    ieltsDict.min = 0;
                                    ieltsDict.require = pteScore;
                                    ieltsDict.max = 90;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = value;
                                    englishList.push(ieltsDict);
                                }
                            }

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }
                            // else {
                            //     throw new Error('IELTS not found');
                            // }

                            //courseAdminReq.academic = "Academic requirements are provided by country please visit the university to know more.";

                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var english_req = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            var academic_req = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirement_url);


                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            // console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            academicReq = selList;
                                            //}
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }
                            // courseAdminReq.academic = (academicReq) ? academicReq.toString() : [];
                            console.log("Academic requirement" + JSON.stringify(courseAdminReq.academic));

                            //  courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];

                            courseAdminReq.english_requirements_url = "https://www.fanshawec.ca/admissions/applying/admission-requirements/english-language-requirements";
                            courseAdminReq.entry_requirements_url = "https://www.fanshawec.ca/admissions/applying/admission-requirements";
                            courseAdminReq.academic_requirements_url = "https://www.fanshawec.ca/admissions/applying/admission-requirements";



                            //courseAdminReq.academic_more_details = academic_more;
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_duration_full_time': {
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = "";
                            var fullTimeText = "";
                            var cDuratiion = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time)
                            var TitleD = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            if (TitleD.includes('Pre-Media') || (TitleD.includes('Power Engineering Techniques - 4th Class') || (TitleD.includes('Remotely Piloted Aerial Systems Commercial Operations'))))
                
                            {
                                fullTimeText = cDuratiion + " weeks";
                              //  fullTimeText = cDuratiion + " weeks";
                                console.log("fullTimeTextfullTimeTextfullTimeText0-0-0-0->>>>>>",fullTimeText);
                                if (fullTimeText && fullTimeText.length > 0) {
                                    const resFulltime = fullTimeText;
                                    if (resFulltime) {
                                        durationFullTime = resFulltime;
                                        courseDurationList = durationFullTime;
                                        courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
    
                                        let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                        if (courseDurationList && courseDurationList.length > 0) {
                                            resJsonData.course_duration = courseDurationList;
                                        }
                                        if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                            resJsonData.course_duration_display = filtered_duration_formated;
                                            var isfulltime = false, isparttime = false;
                                            filtered_duration_formated.forEach(element => {
                                                if (element.display == "Full-Time") {
                                                    isfulltime = true;
                                                }
                                                if (element.display == "Part-Time") {
                                                    isparttime = true;
                                                }
                                            });
                                            resJsonData.isfulltime = isfulltime;
                                            resJsonData.isparttime = isparttime;
                                        }
                                    }
                                }
                                break;
                            }
                            // if (TitleD.includes('Power Engineering Techniques - 4th Class'))
                
                            // {
                            //     fullTimeText = cDuratiion + " weeks";
                            //   //  fullTimeText = cDuratiion + " weeks";
                            //     console.log("fullTimeTextfullTimeTextfullTimeText0-0-0-0->>>>>>",fullTimeText);
                            //     if (fullTimeText && fullTimeText.length > 0) {
                            //         const resFulltime = fullTimeText;
                            //         if (resFulltime) {
                            //             durationFullTime.duration_full_time = resFulltime;
                            //             courseDurationList.push(durationFullTime);
                            //             courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
    
                            //             let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                            //             if (courseDurationList && courseDurationList.length > 0) {
                            //                 resJsonData.course_duration = courseDurationList;
                            //             }
                            //             if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                            //                 resJsonData.course_duration_display = filtered_duration_formated;
                            //                 var isfulltime = false, isparttime = false;
                            //                 filtered_duration_formated.forEach(element => {
                            //                     if (element.display == "Full-Time") {
                            //                         isfulltime = true;
                            //                     }
                            //                     if (element.display == "Part-Time") {
                            //                         isparttime = true;
                            //                     }
                            //                 });
                            //                 resJsonData.isfulltime = isfulltime;
                            //                 resJsonData.isparttime = isparttime;
                            //             }
                            //         }
                            //     }
                            //     break;
                           // }
                            // if (TitleD.includes('Remotely Piloted Aerial Systems Commercial Operations'))
                
                            // {
                            //     fullTimeText = cDuratiion + " weeks";
                            //   //  fullTimeText = cDuratiion + " weeks";
                            //     console.log("fullTimeTextfullTimeTextfullTimeText0-0-0-0->>>>>>",fullTimeText);
                            //     if (fullTimeText && fullTimeText.length > 0) {
                            //         const resFulltime = fullTimeText;
                            //         if (resFulltime) {
                            //             durationFullTime.duration_full_time = resFulltime;
                            //             courseDurationList.push(durationFullTime);
                            //             courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
    
                            //             let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                            //             if (courseDurationList && courseDurationList.length > 0) {
                            //                 resJsonData.course_duration = courseDurationList;
                            //             }
                            //             if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                            //                 resJsonData.course_duration_display = filtered_duration_formated;
                            //                 var isfulltime = false, isparttime = false;
                            //                 filtered_duration_formated.forEach(element => {
                            //                     if (element.display == "Full-Time") {
                            //                         isfulltime = true;
                            //                     }
                            //                     if (element.display == "Part-Time") {
                            //                         isparttime = true;
                            //                     }
                            //                 });
                            //                 resJsonData.isfulltime = isfulltime;
                            //                 resJsonData.isparttime = isparttime;
                            //             }
                            //         }
                            //     }
                            //     break;
                            // }
                            }
                            
                                fullTimeText = cDuratiion ;
                                console.log("fullTimeText700-->", fullTimeText);
                                if (fullTimeText && fullTimeText.length > 0) {
                                    const resFulltime = fullTimeText;
                                    if (resFulltime) {
                                        durationFullTime = resFulltime;
                                        courseDurationList = durationFullTime;
                                        courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));

                                        let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                        if (courseDurationList && courseDurationList.length > 0) {
                                            resJsonData.course_duration = courseDurationList;
                                        }
                                        if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                            resJsonData.course_duration_display = filtered_duration_formated;
                                            var isfulltime = false, isparttime = false;
                                            filtered_duration_formated.forEach(element => {
                                                if (element.display == "Full-Time") {
                                                    isfulltime = true;
                                                }
                                                if (element.display == "Part-Time") {
                                                    isparttime = true;
                                                }
                                            });
                                            resJsonData.isfulltime = isfulltime;
                                            resJsonData.isparttime = isparttime;
                                        }
                                    }
                                }
                             break;
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston
                            var campLocationText = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            console.log("campusssssssssssssssss--->", courseKeyVal);
                            var campusss = [];
                            var campuss = [];

                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selLists12222 = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {

                                                campLocationText = selList;
                                                console.log("camplocationtext-->", campLocationText);
                                                var uniqueArray = Array.from(new Set(campLocationText));
                                                console.log("uniqueArr--->", uniqueArray);
                                            }
                                        }
                                    }
                                }
                            }

                            if (uniqueArray && uniqueArray.length > 0) {
                                var campusedata = [];
                                uniqueArray.forEach(element => {
                                    campusedata.push({
                                        "name": element,
                                        "code": "",
                                        
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

                            }
                            else {
                                throw new Error("No campus location found.");
                            }
                            resJsonData.course_study_mode = "On campus";

                            //resJsonData.course_study_mode = await utils.giveMeArray(studymodeType.replace(/[\r\n\t]+/g, ''), ',');;
                            // console.log("## FInal string-->" + campLocationValTrimmed);
                        }
                        // if (campLocationText && campLocationText.length > 0)


                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;
                            var FeesTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("feestitle------>",FeesTitle);
                            
                            const feesList = [];
                            const feesDict = {
                                international_student: {},


                            };

                            const feesIntStudent = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student);
                            const feesIntStudents2 = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_level2);
                            const feesIntStudents3 = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_level3);
                            const feesIntStudents4 = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_level4);
                            const feesIntStudents5 = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_level5);
                            const feesIntStudents6 = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_level6);

                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed7 = ' + feesWithDollorTrimmed);



                                const arrval = String(feesWithDollorTrimmed).split('.');

                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                               
                                                description: "Level-1 fee",
                                                
                                            });

                                        }
                                    }
                                }
                            } else {
                                feesDict.international_student.push({
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",
                                    isfulltime: true,
                                    description: feesIntStudent,
                                    type: ""
                                });
                            }
                            if (feesIntStudents2 && feesIntStudents2.length > 0) {
                                const feesWithDollorTrimmed = String(feesIntStudents2).trim();

                                console.log(funcName + 'feesWithDollorTrimmed9 = ' + feesWithDollorTrimmed);



                                const arrvals = String(feesWithDollorTrimmed).split('.');


                                const feesVals = String(arrvals[1]);
                                const feesVals1 = String(feesVals).split('$')[1];
                                console.log("feesVals1feesVals1feesVals1feesVals1-->", feesVals1);
                                console.log(funcName + 'feesVal 677= ' + feesVals1);
                                if (feesVals1) {
                                    const regEx = /\d/g;
                                    let feesValNums = feesVals1.match(regEx);
                                    if (feesValNums) {
                                        console.log(funcName + 'feesValNum577567 = ' + feesValNums);
                                        feesValNums = feesValNums.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNums);
                                        let feesNumbers = null;
                                        if (feesValNums.includes(',')) {
                                            feesNumbers = parseInt(feesValNums.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumbers = feesValNums;
                                        }
                                        console.log(funcName + 'feesNumber76575 = ' + feesNumbers);
                                        if (Number(feesNumbers)) {
                                            const regEx = /\d/g;
                                            //     var durations = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time)
                                            //    console.log("ghjghdjghdhgjdg-->");
                                            //     console.log("DUrations-->", JSON.stringify(durations.match(regEx)))
                                            //     console.log("Data@@@", JSON.stringify(durations))
                                            feesDict.international_student = ({
                                                amount: Number(feesNumbers),
                                                duration: 1,
                                                unit: "Year",
                                                
                                                description: "Level-2 fee",
                                               
                                            });

                                        }
                                    }
                                }
                            }
                            if (feesIntStudents3 && feesIntStudents3.length > 0) {
                                const feesWithDollorTrimmed = String(feesIntStudents3).trim();

                                console.log(funcName + 'feesWithDollorTrimmed 11= ' + feesWithDollorTrimmed);



                                const arrvals = String(feesWithDollorTrimmed).split('.');

                                const feesVals = String(arrvals[2]);
                                const feesVals2 = String(feesVals).split('$')[1];
                                console.log(funcName + 'feesVal565657566 = ' + feesVals2);
                                if (feesVals2) {
                                    const regEx = /\d/g;
                                    let feesValNums = feesVals2.match(regEx);
                                    if (feesValNums) {
                                        console.log(funcName + 'feesValNum654644646464646 = ' + feesValNums);
                                        feesValNums = feesValNums.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNums);
                                        let feesNumbers = null;
                                        if (feesValNums.includes(',')) {
                                            feesNumbers = parseInt(feesValNums.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumbers = feesValNums;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumbers);
                                        if (Number(feesNumbers)) {
                                            const regEx = /\d/g;
                                            // var durations = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time)
                                            // console.log("DUrations-->", JSON.stringify(durations.match(regEx)))
                                            // console.log("Data@@@", JSON.stringify(durations))
                                            feesDict.international_student = ({
                                                amount: Number(feesNumbers),
                                                duration: 1,
                                                unit: "Year",
                                               
                                                description: "Level-3 fee",
                                             
                                            });

                                        }
                                    }
                                }
                            }
                            if (feesIntStudents4 && feesIntStudents4.length > 0) {
                                const feesWithDollorTrimmed = String(feesIntStudents4).trim();

                                console.log(funcName + 'feesWithDollorTrimmed 11= ' + feesWithDollorTrimmed);



                                const arrvals = String(feesWithDollorTrimmed).split('.');

                                const feesVals = String(arrvals[3]);
                                const feesVals3 = String(feesVals).split('$')[1];
                                console.log(funcName + 'feesVal565657566 ssss= ' + feesVals3);
                                if (feesVals3) {
                                    const regEx = /\d/g;
                                    let feesValNums = feesVals3.match(regEx);
                                    if (feesValNums) {
                                        console.log(funcName + 'feesValNum654644646464646 = ' + feesValNums);
                                        feesValNums = feesValNums.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNums);
                                        let feesNumbers = null;
                                        if (feesValNums.includes(',')) {
                                            feesNumbers = parseInt(feesValNums.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumbers = feesValNums;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumbers);
                                        if (Number(feesNumbers)) {
                                            const regEx = /\d/g;
                                            // var durations = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time)
                                            // console.log("DUrations-->", JSON.stringify(durations.match(regEx)))
                                            // console.log("Data@@@", JSON.stringify(durations))
                                            feesDict.international_student = ({
                                                amount: Number(feesNumbers),
                                                duration: 1,
                                                unit: "Year",
                                              
                                                description: "Level-4 fee",
                                               
                                            });

                                        }
                                    }
                                }
                            }
                            if (feesIntStudents5 && feesIntStudents5.length > 0) {
                                const feesWithDollorTrimmed = String(feesIntStudents5).trim();

                                console.log(funcName + 'feesWithDollorTrimmed 11= ' + feesWithDollorTrimmed);



                                const arrvals = String(feesWithDollorTrimmed).split('.');

                                const feesVals = String(arrvals[4]);
                                const feesVals4 = String(feesVals).split('$')[1];
                                console.log(funcName + 'feesVal565657566 = ' + feesVals4);
                                if (feesVals4) {
                                    const regEx = /\d/g;
                                    let feesValNums = feesVals4.match(regEx);
                                    if (feesValNums) {
                                        console.log(funcName + 'feesValNum654644646464646 = ' + feesValNums);
                                        feesValNums = feesValNums.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNums);
                                        let feesNumbers = null;
                                        if (feesValNums.includes(',')) {
                                            feesNumbers = parseInt(feesValNums.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumbers = feesValNums;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumbers);
                                        if (Number(feesNumbers)) {
                                            const regEx = /\d/g;
                                            // var durations = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time)
                                            // console.log("DUrations-->", JSON.stringify(durations.match(regEx)))
                                            // console.log("Data@@@", JSON.stringify(durations))
                                            feesDict.international_student = ({
                                                amount: Number(feesNumbers),
                                                duration: 1,
                                                unit: "Year",
                                               
                                                description: "Level-5 fee",
                                               
                                            });

                                        }
                                    }
                                }
                            }
                            if (feesIntStudents6 && feesIntStudents6.length > 0) {
                                const feesWithDollorTrimmed = String(feesIntStudents6).trim();

                                console.log(funcName + 'feesWithDollorTrimmed 11= ' + feesWithDollorTrimmed);



                                const arrvals = String(feesWithDollorTrimmed).split('.');

                                const feesVals = String(arrvals[5]);
                                const feesVals5 = String(feesVals).split('$')[1];
                                console.log(funcName + 'feesVal565657566 = ' + feesVals5);
                                if (feesVals5) {
                                    const regEx = /\d/g;
                                    let feesValNums = feesVals5.match(regEx);
                                    if (feesValNums) {
                                        console.log(funcName + 'feesValNum654644646464646 = ' + feesValNums);
                                        feesValNums = feesValNums.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNums);
                                        let feesNumbers = null;
                                        if (feesValNums.includes(',')) {
                                            feesNumbers = parseInt(feesValNums.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumbers = feesValNums;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumbers);
                                        if (Number(feesNumbers)) {
                                            const regEx = /\d/g;
                                            // var durations = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time)
                                            // console.log("DUrations-->", JSON.stringify(durations.match(regEx)))
                                            // console.log("Data@@@", JSON.stringify(durations))
                                            feesDict.international_student = ({
                                                amount: Number(feesNumbers),
                                                duration: 1,
                                                unit: "Year",
                                                
                                                description: "Level-6 fee",
                                               
                                            });

                                        }
                                    }
                                }
                            }

                          //  let international_student_all_fees_array = [];
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                  //  feesDict.international_student_all_fees = international_student_all_fees_array
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }
                       
                        case 'program_code': {
                            let courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            console.log("program-====code-->>>>", program_code);
                            var program_code = "";
                            var newProgCode = courseKeyVal;

                            // if (program_code.length > 0) {
                            if (courseKeyVal && courseKeyVal.length > 0) {
                                resJsonData.program_code = newProgCode;
                            } else {
                                program_code = "";
                            }

                            //}
                            break;
                        }

                        case 'course_intake':
                            {
                                let courseIntakeStr = [];
                                let tempIntakeArray = [];
                                const courseKeyVal = courseScrappedData.course_intake;
                                console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                                if (Array.isArray(courseKeyVal)) {
                                    for (const rootEleDict of courseKeyVal) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList Intake= ' + JSON.stringify(selList));
                                                // if (Array.isArray(selList) && selList.length > 0) {


                                                for (let selItem of selList) {
                                                    selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, '')
                                                    console.log("sellitem -->", selItem);;
                                                    tempIntakeArray.push(selItem);
                                                }

                                                console.log("tempIntakeArray -->", tempIntakeArray);
                                                if (tempIntakeArray && tempIntakeArray.length > 0) {
                                                    for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                                                        courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": tempIntakeArray });
                                                    }
                                                } else {
                                                    throw new Error('Intake not found');
                                                    for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                                                        courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": ["February", "July"] });
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                                var intakeUrl = [];
                                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                let formatIntake = await format_functions.providemyintake(courseIntakeStr, "MOM", "");
                                if (formatIntake && formatIntake.length > 0) {
                                    var intakedata = {};
                                    intakedata.intake = formatIntake;
                                    intakedata.more_details = more_details;
                                    resJsonData.course_intake = intakedata;
                                    console.log("resJsonData.course_intake -->> ", resJsonData.course_intake);
                                }
                                break;
                            }
                        
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                        }

                            break;
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r careerselList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                // course_career_outcome = selList;
                                                for (let selItem of selList) {
                                                    console.log("selItemCareer -->", selItem);
                                                    selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ');
                                                    course_career_outcome.push(selItem.trim())
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }
                        case 'course_study_level':
                        var studylevel = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level)
                        var titleforstudyL = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                        console.log("titleforstudyL=--=-=-=-=-=>",titleforstudyL);
                        console.log("studylevelstudylevelstudylevel-=-=->",studylevel);
                        if(titleforstudyL.includes('(Co-Op)'))
                        { 
                          
                           // var studylevel1 = studylevel.split('Ontario College')[1].trim();
                            var studylevel2 = studylevel + " ,Co-Op"
                            console.log("studylevallll-->",studylevel2);
                            resJsonData.course_study_level = studylevel2;
                        }
                        else if(studylevel.includes('Ontario College'))
                        {
                            var studylevel1 = studylevel.split('Ontario College')[1].trim();
                            var studylevel2 = studylevel1 + " ,Co-Op"
                        console.log("studylevallll-->",studylevel2);
                        resJsonData.course_study_level = studylevel2;
                        }
                        else{
                            resJsonData.course_study_level = studylevel;
                        }
                    


                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                console.log("lastlocation---->", resJsonData.course_title);

                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                console.log("prevlocation---->");
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                console.log("bdsfhklssssssss------>", JSON.stringify(resJsonData.course_intake));
                var intakes = resJsonData.course_intake.intake;

                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                //        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
               // resJsonData.basecourseid = location_wise_data.course_id;



                // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                //     if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_intake.intake[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_intake.intake[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                //     if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                //     if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_campus_location[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_campus_location[i].iscurrent = false;
                //     }
                // }

              //  resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;

                //resJsonData.current_course_intake = location_wise_data.course_intake;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
                // const res = await awsUtil.putFileInBucket(filelocation);
                // console.log(funcName + 'S3 object location = ' + res);
            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
