const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
const puppeteer = require('puppeteer');
class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {


      var datalist = [];
      var mainCategory = [], redirecturl = [];

      const maincategoryselectorurl = "//*//h3[contains(text(),'Undergraduate')]/following::div[2]//li/a | //*//h3[contains(text(),'Graduate')]/following::div[2]//li/a";
      var categoryurl = await page.$x(maincategoryselectorurl);
      console.log("Total categories-->" + categoryurl.length);
      for (let i = 0; i < categoryurl.length; i++) {
        var categorystringmainurl = await page.evaluate(el => el.href, categoryurl[i]);
        mainCategory.push({ href: categorystringmainurl });
      }
      for (let catc of mainCategory) {
        await page.goto(catc.href);
        const course_url = "//*//ul[@class='filtered-programs']/li[@class='']/a"
        const course_txt = "//*//ul[@class='filtered-programs']/li[@class='']/a[@title]"
        const intake_selcter = "//*//ul[@class='filtered-programs']/li[@class='']/a[@data-start-date]"
        const campus_selecter = "//*//ul[@class='filtered-programs']/li[@class='']/a[@data-campus]"
        const study_level = "//*//ul[@class='filtered-programs']/li[@class='']/a[@data-credential]"
        let course_category = "//*//ul[@class='filtered-programs']/li[@class='']/a[@data-interest]"
        var course_url11 = await page.$x(course_url);
        var course_txt11 = await page.$x(course_txt);
        var intake_selcter11 = await page.$x(intake_selcter);
        var campus_selecter11 = await page.$x(campus_selecter);
        var study_level11 = await page.$x(study_level);
        var course_category11 = await page.$x(course_category);
        console.log("Total categories-->" + course_url11.length);
        for (let i = 0; i < course_url11.length; i++) {
          var course_txt22 = await page.evaluate(el => el.title, course_txt11[i]);
          var course_url22 = await page.evaluate(el => el.href, course_url11[i]);
          var intake_selcter22 = await page.evaluate(el => el.getAttribute('data-start-date'), intake_selcter11[i]);
          var campus_selecter22 = await page.evaluate(el => el.getAttribute('data-campus'), campus_selecter11[i]);
          var study_level22 = await page.evaluate(el => el.getAttribute('data-credential'), study_level11[i]);
          var course_category22 = await page.evaluate(el => el.getAttribute('data-interest'), course_category11[i]);
          if (!course_category22.includes("beer-wine-and-viticulture")) {
            redirecturl.push({
              innerText: course_txt22.replace('Permanent Link to', '').trim(),
              href: course_url22.trim(),
              intake: intake_selcter22.trim(),
              location_name: campus_selecter22.trim(),
              study_leval: study_level22.trim(),
              category: course_category22.trim()
            });
          }
        }
      }
      console.log("redirecturl------->>>", redirecturl);
      await fs.writeFileSync("./output/niagaracollege_original_courselist.json", JSON.stringify(redirecturl));
      await fs.writeFileSync("./output/niagaracollege_unique_courselist.json", JSON.stringify(redirecturl));
      await fs.writeFileSync("./output/niagaracollege_courselist.json", JSON.stringify(redirecturl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);

      // await this.scrapeintakesfess();
      if (s) {
        await s.close();
      }
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  // refers to https://bond.edu.au/future-students/study-bond/search-program
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }

        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class
module.exports = { ScrapeCourseList };