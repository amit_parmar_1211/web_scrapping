const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = [course_category];
                            // As of not we do not have way to get this field value
                            break;
                        }
                        case 'course_title_category':
                        case 'course_title': {
                            let course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            if(course_title.includes('-')){
                                course_title=course_title.replace('-','_');
                            }
                            let course_title_text = format_functions.titleCase(String(course_title).trim());;
                            resJsonData.course_title = course_title_text;
                            break;
                        }
                        case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admi ssion_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            // const flag = await Course.extractValueFromScrappedElement(courseScrappedData.internationalStudentsFlag);
                            // if (flag && flag.length > 0 && flag.indexOf("not currently available for international students.") > -1) {
                            //     throw new Error("This program is not currently available for international students.");
                            // }
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {};
                            const pbtDict = {};
                            const ibtDict = {};
                            const caelDict = {};
                            const pteDict = {};
                            let ieltsNumber = null;
                            let ibtNumber = null;
                            let pbtNumber = null;
                            let caelNumber = null;
                            let pteNumber = null;

                            const regEx = /[+-]?\d+(\.\d+)?/g;

                            var ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                            var toeflScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_toefl);

                            var caelScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_cael);
                            var pteScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_pte);
                            console.log("caelScore -->", caelScore);
                            console.log("pteScore -->", pteScore);

                            let tofelResult = "";
                            let ibtString = "";
                            let pbtString = "";
                            if (toeflScore && toeflScore.indexOf('(') > -1) {
                                tofelResult = toeflScore.split('(');
                                ibtString = tofelResult[1];
                                pbtString = tofelResult[0];
                            } else {
                                tofelResult = toeflScore;
                                ibtString = toeflScore;
                            }
                            if (ieltsScore) {
                                ieltsScore = ieltsScore.replace(/[\r\n\t ]+/g, ' ').trim();
                            }
                            if (ibtString) {
                                ibtString = ibtString.replace(')', ' ').trim();
                                ibtString = ibtString.replace(/[\r\n\t ]+/g, ' ').trim();
                            }
                            console.log("ieltsScore -->", ieltsScore);
                            console.log("ieltsScore -->", pbtString);
                            var academicReq = "";
                            if (ieltsScore && ieltsScore.length > 0) {
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                const matchedStrList = String(ieltsScore).match(regEx);
                                academicReq += ieltsScore.replace(/[\r\n\t ]+/g, ' ').trim();
                                console.log("ieltsScore -->", academicReq);
                                // matchedStrList = matchedStrList.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ').trim();
                                console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    ieltsNumber = Number(matchedStrList[0]);
                                    console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                                }
                            }
                            if (pbtString && pbtString.length > 0) {
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                pbtString = pbtString.trim();
                                const matchedStrList = String(pbtString).match(regEx);
                                academicReq += pbtString.replace(/[\r\n\t ]+/g, ' ').trim();
                                console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {

                                    pbtNumber = Number(matchedStrList[0]);
                                    console.log(funcName + 'pbtNumber = ' + pbtNumber);
                                }
                            }
                            if (ibtString && ibtString.length > 0) {
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                const matchedStrList = String(ibtString).match(regEx);
                                academicReq += ibtString.replace(/[\r\n\t ]+/g, ' ').trim();
                                console.log("ieltsScore22 -->", academicReq);
                                console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    ibtNumber = Number(matchedStrList[0]);
                                    console.log(funcName + 'ibtNumber = ' + ibtNumber);
                                }
                            }

                            if (caelScore && caelScore.length > 0) {
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                const matchedStrList = String(caelScore).match(regEx);
                                academicReq += ibtString.replace(/[\r\n\t ]+/g, ' ').trim();
                                console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    caelNumber = Number(matchedStrList[0]);
                                    console.log(funcName + 'caelNumber = ' + caelNumber);
                                }
                            }
                            if (pteScore && pteScore.length > 0) {
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                const matchedStrList = String(pteScore).match(regEx);
                                academicReq += pteScore.replace(/[\r\n\t ]+/g, ' ').trim();
                                console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    pteNumber = Number(matchedStrList[0]);
                                    console.log(funcName + 'pteNumber = ' + pteNumber);
                                }
                            }
                            if (ieltsNumber) {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ieltsScore;
                                englishList.push(ieltsDict);
                            }

                            if (pbtNumber) {
                                pbtDict.name = 'toefl pbt';
                                pbtDict.description = pbtString;
                                englishList.push(pbtDict);
                            }
                            if (ibtNumber) {
                                ibtDict.name = 'toefl ibt';
                                ibtDict.description = ibtString;
                                englishList.push(ibtDict);
                            }

                            if (caelNumber) {
                                caelDict.name = 'cael';
                                caelDict.description = caelScore;
                                englishList.push(caelDict);
                            }
                            if (pteNumber) {
                                pteDict.name = 'pte academic';
                                pteDict.description = pteScore;
                                englishList.push(pteDict);
                            }

                            // if (englishList && englishList.length > 0) {
                            //     courseAdminReq.english = englishList;
                            // }

                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {};
                            const pcaelDict = {};
                            const ppteDict = {};
                            var penglishList = [];
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                            if (ieltsNumber != "NA" && ieltsNumber != null) {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = ieltsDict.description;
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsNumber;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }
                            if (pbtNumber != "NA" && pbtNumber != null) {
                                ppbtDict.name = 'toefl pbt';
                                ppbtDict.description = pbtDict.description;
                                ppbtDict.min = 310;
                                ppbtDict.require = pbtNumber;
                                ppbtDict.max = 677;
                                ppbtDict.R = 0;
                                ppbtDict.W = 0;
                                ppbtDict.S = 0;
                                ppbtDict.L = 0;
                                ppbtDict.O = 0;
                                penglishList.push(ppbtDict);
                            }
                            if (ibtNumber != "NA" && ibtNumber != null) {
                                pibtDict.name = 'toefl ibt';
                                pibtDict.description = ibtDict.description;
                                pibtDict.min = 0;
                                pibtDict.require = ibtNumber;
                                pibtDict.max = 120;
                                pibtDict.R = 0;
                                pibtDict.W = 0;
                                pibtDict.S = 0;
                                pibtDict.L = 0;
                                pibtDict.O = 0;
                                penglishList.push(pibtDict);
                            }
                            // if (caelNumber != "NA" && caelNumber != null) {
                            //     pcaelDict.name = 'cael';
                            //     pcaelDict.description = caelDict.description;
                            //     pcaelDict.min = 80;
                            //     pcaelDict.require = caelNumber;
                            //     pcaelDict.max = 230;
                            //     pcaelDict.R = 0;
                            //     pcaelDict.W = 0;
                            //     pcaelDict.S = 0;
                            //     pcaelDict.L = 0;
                            //     pcaelDict.O = 0;
                            //     penglishList.push(pcaelDict);
                            // }
                            if (pteNumber != "NA" && pteNumber != null) {
                                ppteDict.name = 'pte academic';
                                ppteDict.description = pteDict.description;
                                ppteDict.min = 0;
                                ppteDict.require = pteNumber;
                                ppteDict.max = 90;
                                ppteDict.R = 0;
                                ppteDict.W = 0;
                                ppteDict.S = 0;
                                ppteDict.L = 0;
                                ppteDict.O = 0;
                                penglishList.push(ppteDict);
                            }
                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }
                            resJsonData.course_admission_requirement = courseAdminReq;
                            // // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                var academicReq = "";
                                if (Array.isArray(courseScrappedData.course_academic_requirement)) {
                                    for (const rootEleDict of courseScrappedData.course_academic_requirement) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    for (let i = 0; i < selList.length; i++) {
                                                        academicReq += selList[i].replace(/[\r\n\t ]+/g, ' ').trim();
                                                        console.log("selList -->", academicReq);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq];
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }
                            let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            console.log("english_requirements_url -->", english_requirements_url);
                            resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                            let academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            resJsonData.course_admission_requirement.academic_requirements_url = academic_requirements_url;
                            resJsonData.course_admission_requirement.entry_requirements_url = "";
                            break;
                        }

                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [], resFulltime = [];
                            var fullTimeText = "";
                            let spl;
                            // const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time);
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            console.log("SplcourseKeyVal----->>>>>>", JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fullTimeText = selList;
                                                console.log("##fullTimeText--->" + JSON.stringify(fullTimeText));
                                                // for (let duration of fullTimeText) {
                                                //     if ((duration.includes("(")) | (duration.includes(","))) {
                                                //         spl = duration.split("(")[0];
                                                //         // spl = spl[spl.length - 1]
                                                //         spl = spl.replace("year", "years").replace("yearss","years").trim();
                                                //         console.log("Spl@@@@@@@@@@@@@@", spl);
                                                //         if(spl=="Fast Track - allows students to complete a two-years diploma program"){
                                                //             spl="two years"
                                                //         }
                                                //     }
                                                //     resFulltime.push(spl);
                                                // }
                                            }
                                        }
                                    }
                                }
                            }
                            resFulltime=fullTimeText
                            if (resFulltime && resFulltime.length > 0) {
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = String(resFulltime).trim();
                                    courseDurationList.push(durationFullTime);
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = String(resFulltime).trim();
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': {
                            var campusedata = [];
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            console.log("courseKeyVal------>>>>", courseKeyVal)
                            // const locationsss = await utils.getValueFromHardCodedJsonFile('locations');
                            // console.log("locationsss####", locationsss);
                            console.log("courseKeyValcourseKeyVal------>>>>", courseKeyVal)
                            let loca = courseKeyVal.split('(')[0];
                            let newcampus = [];
                            for (let campus of [loca]) {
                                if (!newcampus.includes(campus)) {
                                   if(campus.includes('Campus')){
                                      let newlocation= campus.split('Campus')[0]
                                      newcampus.push(newlocation.trim());
                                   }else{
                                    newcampus.push(campus);
                                   }
                                   
                                    console.log("##Campuscampus-->" + campus)
                                }
                            }
                            if (newcampus && newcampus.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                resJsonData.course_study_mode = "On campus"
                                console.log("##campLocationValTrimmed-->", campLocationValTrimmed);
                                var campusedata = [];
                                newcampus.forEach(element => {
                                    campusedata.push({
                                        "name": format_functions.titleCase(String(element).trim()),
                                       "code":""
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;

                            }
                            break;
                        }
                        case 'course_study_mode': { // Location Launceston
                            //resJsonData.course_study_mode = coursestudymodeText;
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {};
                               
                            // feesDict.international_student = [];
                            console.log("demoarray123 -->", demoarray);
                            // console.log("programcode------->>>>", tmpvar[0].Tuition_Fee);
                            const Tuition_Fee = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_international_student)
                            console.log("Course_Name_title------->>>>", Tuition_Fee);
                            var feesIntStudent111 = null;//await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);
                            const courseKeyVal = courseScrappedData.course_tuition_fees_international_student_more_tution;
                            console.log("courseKeyVal-->  ", courseKeyVal);
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                feesIntStudent111 = selList;
                                            }
                                        }
                                    }
                                }
                            }


                            // const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            // if (feeYear && feeYear.length > 0) {
                            //     courseTuitionFee.year = feeYear;
                            // }
                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (Tuition_Fee && Tuition_Fee.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(Tuition_Fee).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('Fees')[1];
                                const arrval11 = String(arrval).split('.')[0];
                                const feesVal = String(arrval11);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }

                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student={
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                               description: Tuition_Fee +''+feesIntStudent111
                                               
                                            };
                                        } else {
                                            feesDict.international_student={
                                                amount: 0,
                                                duration: 1,
                                                unit: "Year",                                                
                                                description: "not available fee"
                                                
                                            };
                                        }
                                        console.log("feesDictinternational_student-->", feesDict.international_student);
                                    }
                                }
                            } else {
                                feesDict.international_student={
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",                                                
                                    description: "not available fee"
                                    
                                };
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                                                      
                           
                            console.log("feesIntStudent111-->  ", feesIntStudent111);
                            //  const feeStudent111 = feesIntStudent111.toString().replace(/[\r\n\t ]+/g, ' ').trim();
                            //let international_student_all_fees_array = newfeesmore;
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                   const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }
                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }
                            break;
                        }

                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = "";
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r programcode selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }

                            if (program_code != '' && program_code.length > 0) {
                                const selList = [];
                                console.log("program_code3 -->", program_code);
                                if (program_code[0].indexOf('-') > -1) {
                                    var aftersplit = program_code[0].split('-');
                                    for (let element of aftersplit) {
                                        console.log("aftersplit -->", element);
                                        var isnumber = await utils.hasNumber(element.trim());
                                        if (isnumber) {
                                            selList.push(element.trim());
                                            console.log("program_code1 -->", element.trim());
                                        }
                                    };
                                }
                                console.log("program_code -->", selList);
                                resJsonData.program_code = String(selList);
                            } else {
                                // program_code = [];
                                // program_code.push("NA");
                                resJsonData.program_code = String(program_code);
                            }
                            break;
                        }
                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            const courseIntakeDisplay = [];
                            // existing intake value
                            // const courseIntakeStrElem = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);

                            let courseKeyVal_intake = courseScrappedData.course_intake;
                            let courseintake;
                            if (Array.isArray(courseKeyVal_intake)) {
                                for (const rootEleDict of courseKeyVal_intake) {
                                    console.log(funcName + '\n\r rootEleDict ieltsDict= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict ieltsDict= ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList ieltsDict= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList)) {
                                                for (let sel of selList) {
                                                    if (courseintake != sel) {
                                                        courseintake = sel;
                                                        // if (courseintake.includes('+')) {
                                                        //     courseintake = courseintake.split('+');
                                                        // }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("R locationArray", campus);
                            console.log('************************Start course_intake******************************');
                            console.log('courseIntakeStr : ' + courseintake);
                            //console.log("R courseIntakeStrFile", courseIntakeStrFile);
                            // const intakeSplit =[];
                            if (courseintake && courseintake.length > 0) {
                                resJsonData.course_intake = courseintake;
                                //resJsonData.course_intake_display = courseIntakeStr;
                                courseintake = String(courseintake).split('\n');
                                courseintake = String(courseintake).split(',');
                                //   courseintake = String(courseintake).split('or');
                                console.log('course_intake intakeStrList = ' + JSON.stringify(courseintake));
                                //const regEx = /[ ]/g; var semList = [];
                                if (courseintake && courseintake.length > 0) {
                                    for (var part of courseintake) {
                                        // part = part.split("(");
                                        // part = part[1].split(")")[0];
                                        console.log("R part", part);
                                        part = part.replace('.', '').replace('Enrol in', '').trim();
                                        courseIntakeDisplay.push(part);
                                    } // for
                                }
                                console.log('course_intake courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));
                            }
                            var campus = resJsonData.course_campus_location;
                            if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let location11 of campus) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location11.name,
                                        "value": courseIntakeDisplay
                                    });
                                }
                            }
                            console.log("intakes123 -->", resJsonData.course_intake.intake);
                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            let formatedIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");
                            // console.log(JSON.stringify(providemyintake(intake, "MOM", "")));
                            console.log("Intakes --> ", JSON.stringify(formatedIntake));
                            var intakedata = {};
                            intakedata.intake = formatedIntake;
                            intakedata.more_details = more_details;
                            resJsonData.course_intake = intakedata;
                            break;
                        }                        
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }

                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).replace(/[\r\n\t ]+/g, ' ').trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).replace(/[\r\n\t ]+/g, ' ').trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).replace(/[\r\n\t ]+/g, ' ').trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).replace(/[\r\n\t ]+/g, ' ').trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).replace(/[\r\n\t ]+/g, ' ').trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r careerselList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                // course_career_outcome = selList;
                                                for (let selItem of selList) {
                                                    console.log("selItemCareer -->", selItem);
                                                    selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ').trim();
                                                    course_career_outcome.push(selItem.trim())
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            } else {
                                resJsonData.course_career_outcome = []
                            }
                            break;
                        }

                        case 'course_study_level': {
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
                            console.log("cTitlecTitle---->>>>>", cTitle);
                            if (cTitle) {
                                resJsonData.course_study_level = cTitle.replace("Ontario College", '').trim();
                            }
                            break;
                        }
                        case 'course_outline': {
                            const courseKeyVal_minor = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_minor);
                            const courseKeyVal_major = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline);
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_outline_moredetails);
                            let course_outlines = {};
                            if (courseKeyVal_minor != null) {
                                course_outlines.minors = courseKeyVal_minor
                            } else {
                                course_outlines.minors = []
                            }
                            if (courseKeyVal_major != null) {
                                course_outlines.majors = courseKeyVal_major
                            } else {
                                course_outlines.majors = []
                            }
                            if (courseKeyVal != null) {
                                course_outlines.more_details = courseKeyVal
                            } else {
                                course_outlines.more_details = ""
                            }
                            resJsonData.course_outline = course_outlines
                            break;
                        }
                        case 'application_fees': {
                            const courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.application_fees)
                            if (courseKeyVal != null) {
                                resJsonData.application_fees = courseKeyVal
                            } else {
                                resJsonData.application_fees = ""
                            }
                            break;
                        }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/:/g, '').replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                console.log("##Course Title -->", NEWJSONSTRUCT.course_id);
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                //resJsonData.basecourseid = location_wise_data.course_id;
                // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                //     if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                //         resJsonData.course_cricos_code[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_cricos_code[i].iscurrent = false;
                //     }
                // }
                
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
