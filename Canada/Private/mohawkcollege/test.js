const puppeteer = require('puppeteer');

async function start() {
    let browser = await puppeteer.launch({ headless: false }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto("https://www.mohawkcollege.ca/programs/business/advertising-and-marketing-communications-management-652", { timeout: 0 });

    const clickselector = "//*[@id='block-de-theme-content']/div/article/div[2]/div[1]/div/ul/li[6]/a";
    const clickselectorelm = await page.$x(clickselector);
    await clickselectorelm[0].click();


    const clickselector1 = "//*[@id='edit-group-pos']/div/div/div/p[2]/a";
    const clickselectorelm1 = await page.$x(clickselector1);
    var url = await page.evaluate(el => el.href, clickselectorelm1[0]);
    await page.goto(url, { timeout: 0 });
    //*[@id="edit-group-pos"]/div/div/div/p[2]/a

    await page.waitFor(5000);

    const scrape_element_url = await page.$x("//*[@id='anyid']/tbody/tr/td[2]/a");
    console.log("selectors to scrape length-->" + scrape_element_url.length)

    await browser.close();
}
start();
// const selectorLength = scrape_element_url.length;
// var counterlength = (selectorLength > sel.limit) ? sel.limit : selectorLength;
// console.log("counterlength : " + counterlength);
// for (let count = 0; count < counterlength; count++) {
//     if (sel.isurl) {
//         var elementstring = await page.evaluate(el => el.innerText, scrape_element_text[count]);
//         var elementurl = await page.evaluate(el => el.href, scrape_element_url[count]);
//         if (sel.iscomb) {
//             const combtext = await page.evaluate(el => el.innerText, scrape_element_url[count]);
//             elementstring = combtext.toString().trim() + " " + elementstring.toString().trim();
//         }
//         if (elementurl && elementstring) {
//             courselist.push({ href: elementurl, innerText: elementstring });
//         }
//     }