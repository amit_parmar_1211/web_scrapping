const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            const intakefees = JSON.parse(fs.readFileSync("./Mapping/international_details.json"));
            let tmpvar = intakefees.filter(val => {
                return val.PROGRAM_NAME == course_name

            });

            let arraydata11 = [];
            let arraydata22 = [];
            let arraydata33 = [];
            let arraydata44 = [];
            var ProgramCode;
            for (let arraydata of tmpvar) {
                arraydata11.push(arraydata);
            }
            for (let tmpv of arraydata11) {

                arraydata22.push(tmpv.fees);
                arraydata33.push(tmpv.Term);
                arraydata44.push(tmpv.sem);
                ProgramCode = tmpv.Course_code;

            }
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = [];
                            break;
                        }
                        case 'course_title_category':
                        case 'course_title': {

                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            console.log("title--->", title);
                            if (title == null) {
                                throw new Error("Page Not Found!!!");
                            }
                            else if (title == 'Apprenticeship') {
                                throw new Error("Not available now !!")
                            }
                            var ctitle = format_functions.titleCase(title).trim();
                            resJsonData.course_title = ctitle
                            break;
                        }

                        case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            var penglishList = [];
                            var potherLngDict = null, ieltsdic = null;
                            // english requirement
                            let scrappedIELTS = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                            var Ieltsnew = scrappedIELTS.split('transcript.')[1];


                            let scrappedPTE = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score_pte);
                            var Ptenew = scrappedPTE.split('or')[1]

                            if (scrappedIELTS != null && scrappedIELTS.length > 0) {
                                let ieltsDict = {};

                                // englishList.push(ieltsDict);

                                var ieltsScore = await utils.giveMeNumber(Ieltsnew);

                                if (ieltsScore != []) {
                                    ieltsDict.name = 'ielts academic';
                                    ieltsDict.description = Ieltsnew.replace(/OR/, '').trim();
                                    ieltsDict.min = 0;
                                    ieltsDict.require = ieltsScore;
                                    ieltsDict.max = 9;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = ieltsDict;
                                    englishList.push(ieltsDict);
                                }
                            }

                            if (scrappedPTE != null && scrappedPTE.length > 0) {
                                let ieltsDict = {};

                                // englishList.push(ieltsDict);

                                var pteScore = await utils.giveMeNumber(Ptenew);
                                if (pteScore != []) {
                                    ieltsDict.name = 'pte academic';
                                    ieltsDict.description = Ptenew.replace(/OR/, '').trim();
                                    ieltsDict.min = 0;
                                    ieltsDict.require = pteScore;
                                    ieltsDict.max = 90;
                                    ieltsDict.R = 0;
                                    ieltsDict.W = 0;
                                    ieltsDict.S = 0;
                                    ieltsDict.L = 0;
                                    ieltsDict.O = 0;
                                    // dict.value = value;
                                    englishList.push(ieltsDict);
                                }
                            }

                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }
                            else {
                                throw new Error('IELTS not found');
                            }

                            //courseAdminReq.academic = "Academic requirements are provided by country please visit the university to know more.";

                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var english_req = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            var academic_req = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirement_url);


                            var academicReq = "";
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            // console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            // if (Array.isArray(selList) && selList.length > 0) {
                                            academicReq = selList;
                                            console.log("academicReq-=-=-==----=-=->", academicReq);
                                            //}
                                        }
                                    }
                                }
                            }

                            courseAdminReq.academic = academicReq;

                            console.log("Academic requirement" + JSON.stringify(courseAdminReq.academic));



                            courseAdminReq.english_requirements_url = "https://www.stclaircollege.ca/programs/admission-procedures";
                            courseAdminReq.entry_requirements_url = "https://www.stclaircollege.ca/programs/admission-procedures";
                            courseAdminReq.academic_requirements_url = "https://www.stclaircollege.ca/programs/admission-procedures";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            const courseKeyVal = courseScrappedData.course_duration_full_time;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict787554 = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict64633 = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList6565 = ' + JSON.stringify(selList));

                                            for (let sel of selList) {

                                                sel = sel.replace(/[\r\n\t ]+/g, ' ');

                                                fullTimeText = sel.trim();
                                            }
                                        }
                                    }
                                }
                            }
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = courseDurationList;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                        }

                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': {
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            var campLocationText = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {

                                                var course_campus_location = selList;
                                                console.log("course_campus_locationcourse_campus_location-->", course_campus_location);
                                            }

                                        }
                                        if (course_campus_location && course_campus_location.length > 0) {
                                            var campusedata = [];
                                            course_campus_location.forEach(element => {
                                                campusedata.push({
                                                    "name": element,
                                                    "city": "",
                                                    "state": "",
                                                    "country": "",
                                                    "iscurrent": false
                                                })
                                            });
                                            resJsonData.course_campus_location = campusedata;


                                        }
                                    }
                                }
                            }


                            resJsonData.course_study_mode = 'On campus';//.join(',');

                            break;
                        }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};

                            const feesList = [];
                            let arraydata11 = [];
                            let arraydata22 = [];
                            const feesDict = {};
                            var FeesInt;

                            var feesIntStudent = arraydata22;

                            for (let arraydata of tmpvar) {
                                arraydata11.push(arraydata);


                            }
                            for (let tmpv of arraydata11) {

                                arraydata22.push(tmpv.fees);
                            }
                            for (let tmpv of arraydata11) {



                                FeesInt = tmpv.fees;

                            }

                            feesIntStudent = FeesInt;
                            console.log("Fee in student--->" + feesIntStudent);




                            if (feesIntStudent) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    if (feesVal != "NA" && feesVal != "N A") {
                                        const regEx = /\d/g;
                                        let feesValNum = feesVal.match(regEx);
                                        console.log("feesValNum=-0=0=0=0=>", feesValNum);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            let feesNumber = null;
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }
                                            console.log(funcName + 'feesNumber = ' + feesNumber);
                                            if (Number(feesNumber)) {
                                                var feesint = {};
                                                feesint.amount = Number(feesNumber);
                                                feesint.duration = 1,
                                                    feesint.unit = "Year",
                                                    feesint.isfulltime = false;
                                                feesint.description = FeesInt;
                                                feesint.type = "";
                                                feesDict.international_student = [feesint];
                                            }
                                        }
                                    }

                                }
                            }
                            else {

                                var feesint = {};
                                feesint.amount = 0;
                                feesint.duration = 1,
                                    feesint.unit = "Year",
                                    feesint.isfulltime = false;
                                feesint.description = FeesInt;
                                feesint.type = "";
                                feesDict.international_student = [feesint];
                            }
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                    var more_fee = [];
                                    feesDict.international_student_all_fees = more_fee;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;
                        }

                        case 'program_code': {
                            var program_code = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);

                            resJsonData.program_code = program_code.trim();

                        }
                        case 'course_intake': {
                            //CUSTOMIZED CODE DON'T COPY
                            var courseIntake = [];
                            // courseScrappedData.course_campus_location
                            var courseIntakeStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);

                            if (courseIntakeStr) {
                                if (courseIntakeStr.indexOf("-") > -1) {
                                    courseIntakeStr = courseIntakeStr.split("-")[1].trim();
                                }
                                // console.log("kfhsfkdfdfkskf",courseIntakeStr);
                                if (courseIntakeStr.indexOf(",") > -1) {
                                    courseIntakeStr = courseIntakeStr.split(",");
                                }
                                if (courseIntakeStr.indexOf("CEP") > -1) {
                                    courseIntakeStr = [courseIntakeStr].split("CEP")[0].trim;
                                    var courseIntakeStr1 = [courseIntakeStr]

                                }

                                console.log("courseIntakeStr--->", courseIntakeStr);
                                if (Array.isArray(courseIntakeStr)) {

                                    for (let intake of courseIntakeStr) {

                                        
                                        intake = String(intake).trim();
                                        courseIntake.push(intake);
console.log("courseIntakecourseIntake-=-=-=-=>",courseIntake);
                                    }
                                }
                            }
                            var locationArray = resJsonData.course_campus_location;

                            if (courseIntake && courseIntake.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let location of locationArray) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location.name,
                                        "value": courseIntake
                                    });
                                }
                                let formatIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "MOM", "");
                                resJsonData.course_intake.intake = formatIntake;

                                var intakeUrl = [];
                                intakeUrl.push(await utils.getValueFromHardCodedJsonFile('intake_url'));
                                resJsonData.course_intake.more_details = intakeUrl[0];
                            }
                            break;
                        }

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }

                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict111111 = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict2222222 = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList3333333 = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList 4444444= ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr556777 = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr78886 = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr798097 = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r careerselList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                // course_career_outcome = selList;
                                                for (let selItem of selList) {

                                                    selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ');
                                                    course_career_outcome.push(selItem.trim())
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }


                        case 'course_study_level':
                            var course_study_level = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
                            console.log("kjvckbjvlkbnchkvkvcbnckvjcobjxovpzxbvibik-->", course_study_level);
                            if (course_study_level == null) {
                                resJsonData.course_study_level = "";
                            }

                            else if (course_study_level.indexOf('Bachelor of Science in Nursing')) {

                                resJsonData.course_study_level = "Bachelor";
                            }
                            else if (course_study_level == null) {
                                resJsonData.course_study_level = "";
                            }
                            else if (course_study_level.indexOf('- Ontario College')) {
                                course_study_level = course_study_level.split('- Ontario College')[1].trim();
                                resJsonData.course_study_level = course_study_level;
                            }

                            else {
                                // course_study_level = course_study_level.split('- Ontario College')[1].trim();
                                resJsonData.course_study_level = course_study_level;
                            }


                        //     }
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                console.log("lastlocation---->", resJsonData.course_title);

                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                console.log("prevlocation---->");
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                // NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                //NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                // NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                console.log("grrtt")
                console.log("resJsonData.course_intake=-==--==>", resJsonData.course_intake);
                var intakes = resJsonData.course_intake.intake;
                console.log("gfgfgfgfgfg")
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = [];
                }
                // for (let FeesNew of resJsonData.course_tuition_fee.fees) {
                //     if (FeesNew.name == location) {
                //         NEWJSONSTRUCT.international_student_all_fees = FeesNew;
                //     }
                // }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                resJsonData.basecourseid = location_wise_data.course_id;

                // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                //     if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                //         resJsonData.course_cricos_code[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_cricos_code[i].iscurrent = false;
                //     }
                // }

                for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                    if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                        resJsonData.course_intake.intake[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_intake.intake[i].iscurrent = false;
                    }
                }

                // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                //     if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                //     }
                // }

                for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                    if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                        resJsonData.course_campus_location[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_campus_location[i].iscurrent = false;
                    }
                }
                //resJsonData.course_cricos_code = location_wise_data.cricos_code;
                // resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;

                //resJsonData.current_course_intake = location_wise_data.course_intake;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
                // const res = await awsUtil.putFileInBucket(filelocation);
                // console.log(funcName + 'S3 object location = ' + res);
            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
