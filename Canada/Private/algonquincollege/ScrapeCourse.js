const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category, location_name, study_leval, duration) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            resJsonData.course_discipline = [course_category];
                            // As of not we do not have way to get this field value
                            break;
                        }
                        case 'course_title_category':
                        case 'course_title': {
                            let course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            let course_title_text = format_functions.titleCase(String(course_title).trim());;
                            resJsonData.course_title = course_title_text;
                            break;
                        }
                        case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': {
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {};
                            const pbtDict = {};
                            const ibtDict = {};
                            const caelDict = {};
                            const pteDict = {};
                            let ieltsNumber = null;
                            let ibtNumber = null;
                            let pbtNumber = null;
                            let caelNumber = null;
                            let pteNumber = null;

                            const regEx = /[+-]?\d+(\.\d+)?/g;

                            //  var ieltsScore = await Course.extractValueFromScrappedElement(courseScrappedData.course_toefl_ielts_score);
                            const courseKeyVal = courseScrappedData.course_toefl_ielts_score;
                            var course_toefl_ielts_score;
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r programcode selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_toefl_ielts_score = selList[0];
                                            }
                                        }
                                    }
                                }
                            }


                            console.log("course_toefl_ielts_score -->", course_toefl_ielts_score);
                            console.log("pteScore -->", pteScore);

                            let tofelResult = "";
                            let ibtString = "";
                            let pbtString = "";
                            let split_ielts, Ielts_var;
                            let ibt_var;
                            // International applicants must provide proof of the subject-specific requirements noted above along with proof of either: 
                            // (IELTS / TOEFL) IELTS-International English Language Testing Service (Academic) Overall band of 6.5 with a minimum of 6.0 in each band;
                            // OR TOEFL-Internet-based (iBT)-overall 88, with a minimum of 22 in each component: Reading 22; Listening 22; Speaking 22; Writing 22.

                            if (course_toefl_ielts_score.includes("IELTS-")) {
                                split_ielts = course_toefl_ielts_score.split("IELTS-")[1];
                                Ielts_var = split_ielts.split("OR")[0]
                                console.log("split_ielts -->", Ielts_var);
                                if (split_ielts.includes("TOEFL-")) {
                                    ibt_var = split_ielts.split("TOEFL-")[1]
                                    console.log("split_ibt_var -->", ibt_var);

                                }
                            }

                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {};
                            const pcaelDict = {};
                            const ppteDict = {};
                            var penglishList = [];
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                            if (Ielts_var) {
                                ieltsScore = await utils.giveMeNumber(Ielts_var);
                                console.log("### IELTS data-->" + ieltsScore);
                            }
                            if (ibt_var) {
                                ibtScore = await utils.giveMeNumber(ibt_var);
                            }
                            if (ieltsScore) {
                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = Ielts_var.trim();
                                pieltsDict.min = 0;
                                pieltsDict.require = ieltsScore;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }

                            if (ibtScore) {
                                pibtDict.name = 'toefl ibt';
                                pibtDict.description = ibt_var.trim();
                                pibtDict.min = 0;
                                pibtDict.require = ibtScore;
                                pibtDict.max = 120;
                                pibtDict.R = 0;
                                pibtDict.W = 0;
                                pibtDict.S = 0;
                                pibtDict.L = 0;
                                pibtDict.O = 0;
                                penglishList.push(pibtDict);
                            }

                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.englishprogress = penglishList;
                            }
                            resJsonData.course_admission_requirement = courseAdminReq;
                            // // academic requirement
                            if (courseScrappedData.course_academic_requirement) {
                                var academicReq = "";
                                if (Array.isArray(courseScrappedData.course_academic_requirement)) {
                                    for (const rootEleDict of courseScrappedData.course_academic_requirement) {
                                        console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                        const elementsList = rootEleDict.elements;
                                        for (const eleDict of elementsList) {
                                            console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                            const selectorsList = eleDict.selectors;
                                            for (const selList of selectorsList) {
                                                console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                                if (Array.isArray(selList) && selList.length > 0) {
                                                    for (let i = 0; i < selList.length; i++) {
                                                        academicReq += selList[i].replace(/[\r\n\t ]+/g, ' ').trim();
                                                        console.log("selList -->", academicReq);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq];
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }
                            let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            console.log("english_requirements_url -->", english_requirements_url);
                            resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                            let academic_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            resJsonData.course_admission_requirement.academic_requirements_url = "";
                            resJsonData.course_admission_requirement.entry_requirements_url = "";
                            break;
                        }

                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            // display full time
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [], resFulltime = [];
                            const courseKeyVal = duration
                            console.log("SplcourseKeyVal----->>>>>>", JSON.stringify(courseKeyVal));

                            if ((courseKeyVal.includes('Part-time\nOn Campus')) || (courseKeyVal.includes('Variable'))) {
                                throw (new Error('Part-time\nOn Campus duration not found'));
                            }
                            else {
                                resFulltime.push(courseKeyVal.replace('Full-time\nOn Campus\n', '').trim());
                                if (resFulltime && resFulltime.length > 0) {
                                    if (resFulltime) {
                                        durationFullTime.duration_full_time = String(resFulltime).trim();
                                        courseDurationList.push(durationFullTime);
                                        let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                        courseDurationDisplayList.push(tempvar);
                                        demoarray = tempvar[0];
                                        console.log("demoarray--->", demoarray);
                                        console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                        let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                        if (courseDurationList && courseDurationList.length > 0) {
                                            resJsonData.course_duration = String(resFulltime).trim();
                                        }
                                        if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                            resJsonData.course_duration_display = filtered_duration_formated;
                                            var isfulltime = false, isparttime = false;
                                            filtered_duration_formated.forEach(element => {
                                                if (element.display == "Full-Time") {
                                                    isfulltime = true;
                                                }
                                                if (element.display == "Part-Time") {
                                                    isparttime = true;
                                                }
                                            });
                                            resJsonData.isfulltime = isfulltime;
                                            resJsonData.isparttime = isparttime;
                                        }
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fees_currency':
                        case 'course_admission_requirement':
                        case 'course_campus_location': {

                            let campusedata = []
                            campusedata.push({
                                "name": location_name,
                                "code": ""
                            })
                            //});
                            console.log("campushhhh@@", campusedata)
                            resJsonData.course_campus_location = campusedata;
                            resJsonData.course_study_mode = "On campus";
                            //}
                            // }
                            break;
                        }
                        // case 'course_study_mode': { // Location Launceston
                        //     //resJsonData.course_study_mode = coursestudymodeText;
                        //     resJsonData.course_study_mode = "On campus";
                        //     break;
                        // }
                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            console.log("location_name -->", location_name);
                            const intakefees = JSON.parse(fs.readFileSync(appConfigs.coures_fees));
                            let pro_code = resJsonData.program_code;
                            let tmpvar = intakefees.filter(val => {
                                return val.program_code.toLowerCase() == pro_code.toLowerCase() && location_name.toLowerCase() == val.cumpus_name.toLowerCase();
                            });
                            console.log("tmpvar------->>>>", tmpvar);
                            let fees_val
                            for (let array_data of tmpvar) {
                                fees_val = array_data.fees;
                                console.log("fees_val------->>>>", fees_val);
                            }


                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;
                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            // feesDict.international_student = [];
                            console.log("demoarray123 -->", demoarray);
                            // console.log("programcode------->>>>", tmpvar[0].Tuition_Fee);
                            const Tuition_Fee = fees_val
                            console.log("Course_Name_title------->>>>", Tuition_Fee);

                            if (Tuition_Fee && Tuition_Fee.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(Tuition_Fee).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                // const arrval = String(feesVal1).split('Fees')[1];
                                const arrval11 = String(feesVal1).split('.')[0];
                                const feesVal = String(arrval11);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }

                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: Tuition_Fee
                                            });
                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: 1,
                                                unit: "Year",
                                                description: "not available fee"
                                            });
                                        }
                                        console.log("feesDictinternational_student-->", feesDict.international_student);
                                    }
                                }
                            } else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",
                                    description: "not available fee"
                                });
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            // For few courses where 'Fees – *Fee discounts apply' is mentioned, we have hard coded fees number takes from UTAS pdf
                            const cricoseStr = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            console.log(funcName + 'cricoseStr = ' + cricoseStr);
                            console.log(funcName + 'feesDict = ' + JSON.stringify(feesDict));
                            if ((!feesDict.international_student) && cricoseStr && cricoseStr.length > 0) {
                                const fixedFeesDict = await utils.getValueFromHardCodedJsonFile('tuition_fees_fixed');
                                if (fixedFeesDict) {
                                    for (const codeKey in fixedFeesDict) {
                                        console.log(funcName + 'codeKey = ' + codeKey);
                                        if (fixedFeesDict.hasOwnProperty(codeKey)) {
                                            const keyVal = fixedFeesDict[codeKey];
                                            console.log(funcName + 'keyVal = ' + keyVal);
                                            if (cricoseStr.includes(codeKey)) {
                                                const feesDictVal = fixedFeesDict[cricoseStr];
                                                console.log(funcName + 'feesDictVal = ' + feesDictVal);
                                                if (feesDictVal && feesDictVal.length > 0) {
                                                    const inetStudentFees = Number(feesDictVal);
                                                    console.log(funcName + 'inetStudentFees = ' + inetStudentFees);
                                                    if (Number(inetStudentFees)) {
                                                        feesDict.international_student = Number(inetStudentFees);
                                                    }
                                                }
                                            }
                                        }
                                    } // for
                                } // if
                            } // if


                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                //   feesDict.international_student_all_fees = [];
                                // if (international_student_all_fees_array && international_student_all_fees_array.length > 0) {

                                //     for (let student_all_fees of international_student_all_fees_array) {
                                //         feesDict.international_student_all_fees.push(student_all_fees.replace(/[\r\n\t ]+/g, ' ').replace('-', '').trim());
                                //     }
                                // } else {
                                //     feesDict.international_student_all_fee = []
                                // }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campuss = campus
                                    console.log("campuss", campuss)
                                    for (let loc of campuss) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }
                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }
                            break;
                        }

                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = "";
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r programcode selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }

                            if (program_code != '' && program_code.length > 0) {
                                console.log("program_code -->", program_code);
                                resJsonData.program_code = String(program_code);
                            } else {

                                resJsonData.program_code = "";
                            }
                            break;
                        }



                        case 'course_intake': {
                            const courseIntakeDisplay = {};
                            let int
                            let courseIntakeStr1, courseIntakeStr2;
                            var courseIntakeStr = [];
                            let courseKeyVal_intake = courseScrappedData.course_intake;
                            let courseintake = [];
                            let valintake
                            if (Array.isArray(courseKeyVal_intake)) {
                                for (const rootEleDict of courseKeyVal_intake) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r programcode selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                courseintake = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (courseintake.includes("(")) {
                                courseIntakeStr1 = courseintake.split("(")[1]
                                int = courseIntakeStr1.replace(')', '').trim();
                                console.log("bdfcgjbfs", int);
                                courseIntakeStr.push(String(int));
                            }
                            else {
                                courseIntakeStr = courseintake
                                console.log("courseKeyVal#####", courseIntakeStr)
                                //                                throw new Error("Intake not found");
                            }
                            console.log("courseIntakeStr@@@", courseIntakeStr);

                            if (courseIntakeStr && courseIntakeStr.length > -1) {
                                console.log("courseIntakeStr#####", courseIntakeStr);
                                var intakes = [];
                                var campus = resJsonData.course_campus_location;
                                for (let loc of campus) {
                                    var intakedetail = {};
                                    intakedetail.name = loc.name;
                                    intakedetail.value = courseIntakeStr;
                                    intakes.push(intakedetail);
                                }
                                console.log("Intaakesssss", intakes)
                                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                let formatedIntake = await format_functions.providemyintake(intakes, "MOM", "");
                                var intakedata = {};
                                intakedata.intake = formatedIntake;
                                intakedata.more_details = more_details;
                                resJsonData.course_intake = intakedata;
                            }
                            break;
                        }
                        case 'course_cricos_code': {
                            const courseKeyVal = courseScrappedData.course_cricos_code;
                            let course_cricos_code = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_cricos_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_cricos_code) {
                                //  global.cricos_code = course_cricos_code[0].split(":")[1];
                                var locations = resJsonData.course_campus_location;
                                var mycodes = [];
                                for (let location of locations) {
                                    course_cricos_code.forEach(element => {
                                        mycodes.push(location + " - " + element);
                                    });
                                }
                                resJsonData.course_cricos_code = mycodes;
                            }
                            //resJsonData.course_cricos_code = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                            break;
                        }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).replace(/[\r\n\t ]+/g, ' ').trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).replace(/[\r\n\t ]+/g, ' ').trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).replace(/[\r\n\t ]+/g, ' ').trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).replace(/[\r\n\t ]+/g, ' ').trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).replace(/[\r\n\t ]+/g, ' ').trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r careerselList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                // course_career_outcome = selList;
                                                for (let selItem of selList) {
                                                    console.log("selItemCareer -->", selItem);
                                                    selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ').trim();
                                                    course_career_outcome.push(selItem.trim())
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome;
                            } else {
                                resJsonData.course_career_outcome = []
                            }
                            break;
                        }

                        case 'course_study_level': {
                            const cTitle = study_leval;
                            console.log("cTitlecTitle---->>>>>", cTitle);
                            if (cTitle) {
                                resJsonData.course_study_level = cTitle.replace("Ontario College", '').trim();
                            }
                            break;
                        }
                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };
                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal111 = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal111------->", JSON.stringify(courseKeyVal111))
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal111)) {
                                for (const rootEleDict of courseKeyVal111) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }
                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }
                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)
                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;
                            break;
                        }
                        case 'application_fee':
                            {
                                let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                if (application_fee.length > 0) {
                                    resJsonData.application_fee = application_fee;
                                } else {
                                    resJsonData.application_fee = '';
                                }

                                break;
                            }
                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/:/g, '').replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                console.log("##Course Title -->", NEWJSONSTRUCT.course_id);
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                //NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                // resJsonData.basecourseid = location_wise_data.course_id;
                // for (var i = 0; i < resJsonData.course_cricos_code.length; i++) {
                //     if (resJsonData.course_cricos_code[i].location === location_wise_data.course_campus_location) {
                //         resJsonData.course_cricos_code[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_cricos_code[i].iscurrent = false;
                //     }
                // }
                // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                //     if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_intake.intake[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_intake.intake[i].iscurrent = false;
                //     }
                // }
                // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                //     if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                //     }
                // }
                // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                //     if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_campus_location[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_campus_location[i].iscurrent = false;
                //     }
                // }
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call
            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category, courseDict.location_name, courseDict.study_leval, courseDict.duration);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
