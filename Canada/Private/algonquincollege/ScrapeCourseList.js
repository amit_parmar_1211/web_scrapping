const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
const puppeteer = require('puppeteer');
class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      var datalist = [];
      var mainCategory = [], redirecturl = [];
      const course_url = "//*//td[@class='sorting_1']//a"
      const course_txt = "//*//td[@class='sorting_1']//a/text()"
      const category_selcter = "//*//td[@class='sorting_1']/following-sibling::td[1]"
      const campus_selecter = "//*//td[@class='sorting_1']/following-sibling::td[2]"
      const study_level = "//*//td[@class='sorting_1']/following-sibling::td[3]"
      let course_duration = "//*//td[@class='sorting_1']/following-sibling::td[4]"
      var course_url11 = await page.$x(course_url);
      var course_txt11 = await page.$x(course_txt);
      var category_selcter11 = await page.$x(category_selcter);
      var campus_selecter11 = await page.$x(campus_selecter);
      var study_level11 = await page.$x(study_level);
      var course_duration11 = await page.$x(course_duration);
      console.log("Total categories-->" + course_url11.length);
      for (let i = 0; i < course_url11.length; i++) {
        var course_txt22 = await page.evaluate(el => el.textContent, course_txt11[i]);
        var course_url22 = await page.evaluate(el => el.href, course_url11[i]);
        var category_selcter22 = await page.evaluate(el => el.innerText, category_selcter11[i]);
        var campus_selecter22 = await page.evaluate(el => el.innerText, campus_selecter11[i]);
        var study_level22 = await page.evaluate(el => el.innerText, study_level11[i]);
        var course_duration22 = await page.evaluate(el => el.innerText, course_duration11[i]);
        if (!course_duration22.includes("Online")) {
          redirecturl.push({
            innerText: course_txt22.trim(),
            href: course_url22,
            category: category_selcter22,
            location_name: campus_selecter22,
            study_leval: study_level22,
            duration: course_duration22
          });
        }
      }
      console.log("redirecturl------->>>", redirecturl);
      await fs.writeFileSync("./output/algonquincollege_original_courselist.json", JSON.stringify(redirecturl));
      await fs.writeFileSync("./output/algonquincollege_unique_courselist.json", JSON.stringify(redirecturl));
      await fs.writeFileSync("./output/algonquincollege_courselist.json", JSON.stringify(redirecturl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async start_fees() {
    const URL = "https://www.algonquincollege.com/ro/pay/fee-estimator/";
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    var subjectAreasArray = [];
    var programsAreasArray = [];
    let intakeArray = [];
    let courseNameList = [];
    let cumpus_Name = "//*//select[@class='campus']/option[not(contains(text(),'All'))]";
    let campus_val = "//*//select[@class='campus']/option[not(@value='ALL')]";
    let intake_month = "//*//select[@class='startterm']/option";
    let intake_val = "//*//select[@class='startterm']/option[@value]";
    let programs_name = "//*//select[@class='programcode']/option[not(contains(text(),' -- select a program -- '))]";
    let program_val = "//*//select[@class='programcode']/option[not(@value='')]";
    let submit_btn = "//*//div[@class='buttons']/input[@value='submit']";
    let internationl_val = "//*//select[@class='residency']/option[@value='international']";
    const cumpus_Name11 = await page.$x(cumpus_Name);
    const campus_val11 = await page.$x(campus_val);
    const intake_month11 = await page.$x(intake_month);
    const intake_val11 = await page.$x(intake_val);
    const programs_name11 = await page.$x(programs_name);
    const program_val11 = await page.$x(program_val);
    for (let i = 0; i < cumpus_Name11.length; i++) {
      let cumpus_Name22 = await page.evaluate(el => el.innerText, cumpus_Name11[i]);
      let campus_val22 = await page.evaluate(el => el.value, campus_val11[i]);
      subjectAreasArray.push({
        cumpus_name: cumpus_Name22,
        cumpus_value: campus_val22,
      });
    }
    for (let j = 0; j < intake_month11.length; j++) {
      let intake_month22 = await page.evaluate(el => el.innerText, intake_month11[j]);
      let intake_val22 = await page.evaluate(el => el.value, intake_val11[j]);
      intakeArray.push({
        intake_Month: intake_month22,
        intake_value: intake_val22,
      });
    }
    for (let k = 0; k < programs_name11.length; k++) {
      let programs_name22 = await page.evaluate(el => el.innerText, programs_name11[k]);
      let program_val22 = await page.evaluate(el => el.value, program_val11[k]);
      courseNameList.push({
        programs_Name: programs_name22,
        programs_value: program_val22,
      });
    }
    for (let sub_campus of subjectAreasArray) {
      // await page.select("#searchFees > label:nth-child(1) > select", sub_campus.cumpus_value);
      // await page.waitFor(3000);
      for (let sub_intake of intakeArray) {
        // await page.select("#searchFees > label:nth-child(2) > select", sub_intake.intake_value);
        // await page.select("#searchFees > label:nth-child(3) > select", "international");
        for (let sub_programs of courseNameList) {
          let newUrl = URL + "?campus=" + sub_campus.cumpus_value + "&startterm=" + sub_intake.intake_value + "&residency=international&programFees=" + sub_programs.programs_value + "#programDetail"
          await page.goto(newUrl, { timeout: 0 });
          // await page.select("#searchFees > label:nth-child(4) > select", sub_programs.programs_value);
          // let submitBtn = await page.$x("//*//div[@class='buttons']/input[@value='submit']");
          // await submitBtn[0].click();
          await page.waitFor(2000);
          let fees_val = "//*//li[contains(text(),'Estimated Program Total:')]/span";
          let fees_val_text = await page.$x(fees_val);
          if (fees_val_text && fees_val_text.length > 0) {
            let feesValueFinal = await page.evaluate(el => el.innerText, fees_val_text[0]);
            console.log("feesValueFinal -->", feesValueFinal);
            programsAreasArray.push({
              cumpus_name: sub_campus.cumpus_name,
              intake_Month: sub_intake.intake_Month,
              programs_Name: sub_programs.programs_Name,
              fees: feesValueFinal
            });
          }
        }
      }
    }
    await fs.writeFileSync("./output/algonquincollege_fees_intakes_caregary.json", JSON.stringify(programsAreasArray));
    console.log("name-->", JSON.stringify(programsAreasArray));
    await browser.close();
  }

  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      await this.start_fees();
      // await this.scrapeintakesfess();
      if (s) {
        await s.close();
      }
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  // refers to https://bond.edu.au/future-students/study-bond/search-program
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }

        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class
module.exports = { ScrapeCourseList };