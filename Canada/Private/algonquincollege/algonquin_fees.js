const puppeteer = require('puppeteer');
const fs = require("fs");
async function start_fees() {
    const URL = "https://www.algonquincollege.com/ro/pay/fee-estimator/";
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    var subjectAreasArray = [];
    var programsAreasArray = [];
    let intakeArray = [];
    let courseNameList = [];
    let cumpus_Name = "//*//select[@class='campus']/option[not(contains(text(),'All'))]";
    let campus_val = "//*//select[@class='campus']/option[not(@value='ALL')]";

    let intake_month = "//*//select[@class='startterm']/option";
    let intake_val = "//*//select[@class='startterm']/option[@value]";

    let programs_name = "//*//select[@class='programcode']/option[not(contains(text(),' -- select a program -- '))]";
    let program_val = "//*//select[@class='programcode']/option[not(@value='')]";

    let submit_btn = "//*//div[@class='buttons']/input[@value='submit']";
    let internationl_val = "//*//select[@class='residency']/option[@value='international']";

    const cumpus_Name11 = await page.$x(cumpus_Name);
    const campus_val11 = await page.$x(campus_val);

    const intake_month11 = await page.$x(intake_month);
    const intake_val11 = await page.$x(intake_val);

    const programs_name11 = await page.$x(programs_name);
    const program_val11 = await page.$x(program_val);

    for (let i = 0; i < cumpus_Name11.length; i++) {
        let cumpus_Name22 = await page.evaluate(el => el.innerText, cumpus_Name11[i]);
        let campus_val22 = await page.evaluate(el => el.value, campus_val11[i]);
        subjectAreasArray.push({
            cumpus_name: cumpus_Name22,
            cumpus_value: campus_val22,
        });
    }
    for (let j = 0; j < intake_month11.length; j++) {
        let intake_month22 = await page.evaluate(el => el.innerText, intake_month11[j]);
        let intake_val22 = await page.evaluate(el => el.value, intake_val11[j]);
        intakeArray.push({
            intake_Month: intake_month22,
            intake_value: intake_val22,
        });
    }
    for (let k = 0; k < programs_name11.length; k++) {
        let programs_name22 = await page.evaluate(el => el.innerText, programs_name11[k]);
        let program_val22 = await page.evaluate(el => el.value, program_val11[k]);
        courseNameList.push({
            programs_Name: programs_name22,
            programs_value: program_val22,
        });
    }
    for (let sub_campus of subjectAreasArray) {
        // await page.select("#searchFees > label:nth-child(1) > select", sub_campus.cumpus_value);
        // await page.waitFor(3000);
        for (let sub_intake of intakeArray) {
            // await page.select("#searchFees > label:nth-child(2) > select", sub_intake.intake_value);
            // await page.select("#searchFees > label:nth-child(3) > select", "international");
            for (let sub_programs of courseNameList) {
                let newUrl = URL + "?campus=" + sub_campus.cumpus_value + "&startterm=" + sub_intake.intake_value + "&residency=international&programFees=" + sub_programs.programs_value + "#programDetail"
                await page.goto(newUrl, { timeout: 0 });
                // await page.select("#searchFees > label:nth-child(4) > select", sub_programs.programs_value);
                // let submitBtn = await page.$x("//*//div[@class='buttons']/input[@value='submit']");
                // await submitBtn[0].click();
                await page.waitFor(2000);
                let fees_val = "//*//li[contains(text(),'Estimated Program Total:')]/span";
                let fees_val_text = await page.$x(fees_val);
                if (fees_val_text && fees_val_text.length > 0) {
                    let feesValueFinal = await page.evaluate(el => el.innerText, fees_val_text[0]);
                    console.log("feesValueFinal -->", feesValueFinal);
                    programsAreasArray.push({
                        cumpus_name: sub_campus.cumpus_name,
                        intake_Month: sub_intake.intake_Month,
                        programs_Name: sub_programs.programs_Name,
                        program_code: sub_programs.programs_value,
                        fees: feesValueFinal
                    });
                }
            }
        }
    }
    await fs.writeFileSync("./output/algonquin_fees_intakes_caregary.json", JSON.stringify(programsAreasArray));
    console.log("name-->", JSON.stringify(programsAreasArray));
    await browser.close();
}
start_fees();