const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
const puppeteer = require('puppeteer');
class ScrapeCourseList extends Scrape {
  // refers to https://humber.ca/search/full-time.html
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];
      var name;
      var href;
      await s.setupNewBrowserPage("https://cambriancollege.ca/international/international-students-programs/");
      var mainCategory = [], redirecturl = [];

      const category_val = "//*//div[@class='grid grid--unpadded']//h3";

      const levels = await s.page.$x(category_val)

      for (let country of levels) {
        name = await s.page.evaluate(el => el.innerText, country);
        href = await s.page.evaluate(el => el.href, country);
        let fcategory = String(name).replace("\n", " ").replace("\n", " ").trim()
        redirecturl.push({ name: fcategory, href: href, tempcategory: String(name).split("\n")[0].trim() });

      }

      for (let catc of redirecturl) {
        console.log("//*/h3[contains(text(),'" + catc.tempcategory + "')]/..//table/thead//../tbody/tr/td/a");
        const subjects = await s.page.$x("//*/h3[contains(text(),'" + catc.tempcategory + "')]/..//table/thead//../tbody/tr/td/a");
        for (let i = 0; i < subjects.length; i++) {
          var elementstring = await s.page.evaluate(el => el.innerText, subjects[i]);
          var elementlink = await s.page.evaluate(el => el.href, subjects[i])
          totalCourseList.push({ href: elementlink, innerText: elementstring, category: catc.name })
        }

      }

      fs.writeFileSync("./output/cambriancollege_maincategorylist.json", JSON.stringify(mainCategory))
      await fs.writeFileSync("./output/cambriancollege_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
      await fs.writeFileSync("./output/cambriancollege_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }
          }
        }
      }
      await fs.writeFileSync("./output/cambriancollege_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);

    }
  };
  async  start() {
    const URL = "https://cambriancollege.ca/apply/how-to-apply/tuition-and-fees/international-students-tuition-2019-20/";
    let browser = await puppeteer.launch({ headless: true }); //headless:false so we can debug
    let page = await browser.newPage();
    await page.goto(URL, { timeout: 0 });
    var ispage = true
    await page.select("#tuition-fees-table_length > label > select", "100");
    var subjectAreasArray = [];
    while (ispage) {
      var activeselector = await page.$x("//*//a[@class='paginate_button next']");
      //   var hide_next = await page.$x("//*//a[@class='paginate_button next disabled']")
      //  console.log("Yes I m Here---------->", activeselector.length);

      let Course_Name = "//*//tbody[@class='row-hover']/tr/td[1]";
      let Course_code = "//*//tbody[@class='row-hover']/tr/td[2]";
      let term = "//*//tbody[@class='row-hover']/tr/td[3]";
      let sem = "//*//tbody[@class='row-hover']/tr/td[4]";
      let Tuition_Fee = "//*//tbody[@class='row-hover']/tr/td[5]";
      const Course_Name11 = await page.$x(Course_Name);
      const Course_code11 = await page.$x(Course_code);
      const term11 = await page.$x(term);
      const sem11 = await page.$x(sem);
      const Tuition_Fee11 = await page.$x(Tuition_Fee);
      for (let i = 0; i < Course_Name11.length; i++) {
        let Course_Name22 = await page.evaluate(el => el.textContent, Course_Name11[i]);
        let Course_code22 = await page.evaluate(el => el.textContent, Course_code11[i]);
        let term22 = await page.evaluate(el => el.textContent, term11[i]);
        let sem22 = await page.evaluate(el => el.textContent, sem11[i]);
        let Tuition_Fee22 = await page.evaluate(el => el.textContent, Tuition_Fee11[i]);
        subjectAreasArray.push({
          Course_Name: Course_Name22,
          Course_code: Course_code22,
          Term: term22,
          sem: sem22,
          Tuition_Fee: Tuition_Fee22,
        });
      }
      if (activeselector && activeselector.length > 0) {
        ispage = true
        await page.waitFor(5000);
        await activeselector[0].click();
      } else {
       
        ispage = false;
      }
      //console.log("ispage valueeee-----------=-=-==-==--=->",ispage);
    }
    await fs.writeFileSync("./output/cambrian_fees_intakes.json", JSON.stringify(subjectAreasArray));
    // console.log("name-->", JSON.stringify(subjectAreasArray));
    await browser.close();
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      await this.start();
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }


  // refers to https://programsandcourses.anu.edu.au/catalogue
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }


      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }

        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class

module.exports = { ScrapeCourseList };