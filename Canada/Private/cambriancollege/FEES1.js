const puppeteer = require('puppeteer');
const fs = require("fs");
async function start() {
  const URL = "https://cambriancollege.ca/apply/how-to-apply/tuition-and-fees/international-students-tuition-2019-20/";
  let browser = await puppeteer.launch({ headless: false }); //headless:false so we can debug
  let page = await browser.newPage();
  await page.goto(URL, { timeout: 0 });
  var ispage = true
  await page.select("#tuition-fees-table_length > label > select", "100");
  var subjectAreasArray = [];
  while (ispage) {
    var activeselector = await page.$x("//*//a[@class='paginate_button next']");
    //   var hide_next = await page.$x("//*//a[@class='paginate_button next disabled']")
    //  console.log("Yes I m Here---------->", activeselector.length);

    let Course_Name = "//*//tbody[@class='row-hover']/tr/td[1]";
    let Course_code = "//*//tbody[@class='row-hover']/tr/td[2]";
    let term = "//*//tbody[@class='row-hover']/tr/td[3]";
    let sem = "//*//tbody[@class='row-hover']/tr/td[4]";
    let Tuition_Fee = "//*//tbody[@class='row-hover']/tr/td[5]";
    const Course_Name11 = await page.$x(Course_Name);
    const Course_code11 = await page.$x(Course_code);
    const term11 = await page.$x(term);
    const sem11 = await page.$x(sem);
    const Tuition_Fee11 = await page.$x(Tuition_Fee);
    for (let i = 0; i < Course_Name11.length; i++) {
      let Course_Name22 = await page.evaluate(el => el.textContent, Course_Name11[i]);
      let Course_code22 = await page.evaluate(el => el.textContent, Course_code11[i]);
      let term22 = await page.evaluate(el => el.textContent, term11[i]);
      let sem22 = await page.evaluate(el => el.textContent, sem11[i]);
      let Tuition_Fee22 = await page.evaluate(el => el.textContent, Tuition_Fee11[i]);
      subjectAreasArray.push({
        Course_Name: Course_Name22,
        Course_code: Course_code22,
        Term: term22,
        sem: sem22,
        Tuition_Fee: Tuition_Fee22,
      });
    }
    if (activeselector && activeselector.length > 0) {
      ispage = true
      await page.waitFor(5000);
      await activeselector[0].click();
    } else {
      console.log("feeeeeeeesssssssssssssseeeeeeeee-->");
      ispage = false;
    }
    //console.log("ispage valueeee-----------=-=-==-==--=->",ispage);
  }
  await fs.writeFileSync("./output/cambrian_fees_intakes.json", JSON.stringify(subjectAreasArray));
  // console.log("name-->", JSON.stringify(subjectAreasArray));
  await browser.close();
}
start();
