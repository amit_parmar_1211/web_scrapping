const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'scrapeCourseCategoryList ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: false });
      var totalCourseList = [];
      var name;
      var href;
      await s.setupNewBrowserPage("https://cambriancollege.ca/international/international-students-programs/");
      var mainCategory = [], redirecturl = [], mainCategory1 = [];

      const category_val = "//*//div[@class='grid grid--unpadded']//h3";
      // var clickbtn = await s.page.$x('//*//div[@class="item item--align-center item--icon-large"]//h3/following::a[1]');
      // await clickbtn[0].click();
      // console.log("clicked---->",clickbtn);
      const levels = await s.page.$x(category_val)

      console.log("category_val--->", levels);
      for (let country of levels) {
        name = await s.page.evaluate(el => el.innerText, country);
        href = await s.page.evaluate(el => el.href, country);
        redirecturl.push({ name: name, href: href });
        console.log("mainCategory---->", String(mainCategory));
        // mainCategory.push(String(name).split('\n')[0]);
        var clickbtn = await s.page.$x('//*//div[@class="grid grid--unpadded"]//h3/following::a[1]');
        await clickbtn[0].click();
        mainCategory.push(String(name).replace('\n',' ').replace('\n').trim());
        console.log("maincategory1--->",mainCategory);
      }
      console.log("subjectAreasArray-->", redirecturl)

      for (let catc of mainCategory) {
        const subjects = await s.page.$x("//*/h3[contains(text(),'" + catc + "')]/..//table/thead//../tbody/tr/td/a");
        for (let i = 0; i < subjects.length; i++) {
          var elementstring = await s.page.evaluate(el => el.innerText, subjects[i]);
          var elementlink = await s.page.evaluate(el => el.href, subjects[i])
          totalCourseList.push({ href: elementlink, innerText: elementstring, category: catc })
        }
        console.log("subjects2--->", subjects);

      }

      fs.writeFileSync("./output/brighton_maincategorylist.json", JSON.stringify(mainCategory))
      console.log("totalCourseList -->", totalCourseList);
      await fs.writeFileSync("./output/brighton_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
      await fs.writeFileSync("./output/brighton_unique_courselist.json", JSON.stringify(uniqueUrl));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/brighton_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  };
}
module.exports = { ScrapeCourseList };



// const fs = require('fs');
// const Scrape = require('./common/scrape').Scrape;
// const utils = require('./common/utils');
// const configs = require('./configs');

// const appConfigs = require('./common/app-config');

// class ScrapeCourseList extends Scrape {

//   // refers to https://futurestudents.csu.edu.au/courses/all

//   async scrapeOnlyInternationalCourseList() {
//     const funcName = 'startScrappingFunc ';
//     var s = null;
//     try {
//       s = new Scrape();
//       await s.init({ headless: true });
//       var datalist = [];
//       await s.setupNewBrowserPage("https://cambriancollege.ca/international/international-students-programs/");
//       const categoryselectorUrl = "//*//div[@class='grid grid--unpadded']//h3";
//       const targetLinksCardsUrls = await s.page.$x(categoryselectorUrl);
//       var targetLinks=[]
//       for(let i=0;i<targetLinksCardsUrls.length;i++){
//         var elementstring = await s.page.evaluate(el => el.innerText, targetLinksCardsUrls[i]);
//        var elementhref = await s.page.evaluate(el => el.href, targetLinksCardsUrls[i]);
//         elementstring = elementstring;
//         targetLinks.push({ href: elementhref, innerText: elementstring });
//       }
//       console.log("MAinCategory-->",targetLinks)
//       await fs.writeFileSync("./output/main_category_courses.json", JSON.stringify(targetLinks));

//       let linkselector = "";
//       // let studylevelSel = "//div[@data-title='Courses']//div[@class='columns is-multiline']//a/../text()[1]";
//       let textselector = "//div[@data-title='Courses']//div[@class='columns is-multiline']//a"
//       //div[@data-title='Courses']//div[@class='columns is-multiline']//a
//       var totalCourseList = [];
//       for (let target of targetLinks) {
//         await s.page.goto(target.href, { timeout: 0 });
//         let courseButtonClick = "//a[contains(@title,'Courses')]";

//     var clickbtn = await s.page.$x(courseButtonClick);
//     await clickbtn[0].click();
//     await s.page.waitFor(5000);
//     const courses=await s.page.$x(linkselector)
//     //console.log("Length@@@",courses.length)
//     for(let i=0;i<courses.length;i++){
//     const elementurl=await s.page.evaluate(el=>el.href,courses[i])
//     const elementstring=await s.page.evaluate(el=>el.innerText,courses[i])
//     totalCourseList.push({href:elementurl,innerText:elementstring,category: target.innerText})
//     }

//       }

//       await fs.writeFileSync("./output/jamescookuniversity_original_courselist.json", JSON.stringify(totalCourseList));
//       let uniqueUrl = [];
//       //unique url from the courselist file
//       for (let i = 0; i < totalCourseList.length; i++) {
//         let cnt = 0;
//         if (uniqueUrl.length > 0) {
//           for (let j = 0; j < uniqueUrl.length; j++) {
//             if (totalCourseList[i].href == uniqueUrl[j].href) {
//               cnt = 0;
//               break;
//             } else {
//               cnt++;
//             }
//           }
//           if (cnt > 0) {
//             uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
//           }
//         } else {
//           uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
//         }
//       }


//       await fs.writeFileSync("./output/jamescookuniversity_unique_courselist.json", JSON.stringify(uniqueUrl));

//       for (let i = 0; i < totalCourseList.length; i++) {
//         for (let j = 0; j < uniqueUrl.length; j++) {
//           if (uniqueUrl[j].href == totalCourseList[i].href) {
//             if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

//             } else {
//               uniqueUrl[j].category.push(totalCourseList[i].category);
//             }

//           }
//         }
//       }
//       console.log("totalCourseList -->", uniqueUrl);
//       await fs.writeFileSync("./output/jamescookuniversity_courselist.json", JSON.stringify(uniqueUrl));
//       console.log(funcName + 'writing courseList to file completed successfully....');
//       if (s) {
//         await s.close();
//       }



//     }catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       throw (error);
//     }
//     }



//   } // class

// module.exports = { ScrapeCourseList };
