const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};

            const intakefees = JSON.parse(fs.readFileSync("./output/fees_mapping.json"));
            let tmpvar = intakefees.filter(val => {
                return val.Course_Name == course_name

            });

            let arraydata11 = [];
            let arraydata22 = [];
            let arraydata33 = [];
            let arraydata44 = [];
            var ProgramCode;
            for (let arraydata of tmpvar) {
                arraydata11.push(arraydata);
            }
            for (let tmpv of arraydata11) {

                arraydata22.push(tmpv.Tuition_Fee);
                arraydata33.push(tmpv.Term);
                arraydata44.push(tmpv.sem);
                ProgramCode = tmpv.Course_code;

            }
            for (let tmpv of arraydata11) {
                ProgramCode = tmpv.Course_code;
            }
            resJsonData.program_code = ProgramCode;

            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = category_name;
                            break;
                        }
                        case 'course_outline': {
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: courseUrl


                            };
                            console.log("outline------>", outline)
                            resJsonData.course_outline = outline;
                        }
                        case 'application_fee': {
                            resJsonData.application_fee = "";
                        }
                        case 'course_title_category':
                        case 'course_title': {

                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            if (title.includes('Network Technician')) {
                                title = "Computer Systems Technician"
                                resJsonData.course_title = title
                            }
                            else {
                                var ctitle = format_functions.titleCase(title).trim();

                                resJsonData.course_title = ctitle
                            }
                            break;
                        }

                        case 'ielts_req':
                        case 'pte_req':
                        case 'ibt_req':
                        case 'course_admission_academic_more_details':

                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];

                            const myscrore = await utils.getMappingScore(course_name);


                            var ibtDict = {}, catDict = {}, pteDict = {}, ieltsDict = {}, caeDict = {};
                            ibtDict.name = 'toefl ibt';
                            ibtDict.description = myscrore.TOEFL;
                            englishList.push(ibtDict);
                            pteDict.name = 'pte academic';
                            pteDict.description = myscrore.PTE;
                            englishList.push(pteDict);
                            ieltsDict.name = 'ielts academic';
                            ieltsDict.description = myscrore.IELTS;
                            englishList.push(ieltsDict);
                            catDict.name = 'cae';
                            catDict.description = myscrore.CAE;
                            englishList.push(catDict);

                            let ieltsNumber = null; let pbtNumber = null; let pteNumber = null; let caeNumber = null;
                            const penglishList = [];
                            var ieltsfinaloutput = null;
                            var tofelfinaloutput = null;
                            var ptefinaloutput = null;
                            var caefinaloutput = null;
                            const regEx = /[+-]?\d+(\.\d+)?/g;
                            const matchedStrList = String(myscrore.IELTS).match(regEx);
                            console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                            if (matchedStrList && matchedStrList.length > 0) {
                                ieltsNumber = Number(matchedStrList[0]);
                                console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                            }
                            if (ieltsNumber && ieltsNumber > 0) {
                                //tofel score
                                console.log("myscrore.TOEFL -->", myscrore.TOEFL);
                                let tempTOFEL = [
                                    {
                                        "elements":
                                            [
                                                {
                                                    "selectors":
                                                        [
                                                            [myscrore.TOEFL]
                                                        ]
                                                }
                                            ]
                                    }
                                ]
                                const tofelScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempTOFEL);
                                if (tofelScore && tofelScore.length > 0) {
                                    tofelfinaloutput = tofelScore.toString();
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(tofelScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pbtNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'pbtNumber = ' + pbtNumber);
                                    }
                                }

                            }

                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const ppbtDict = {}; const ppteDict = {}; const pcaeDict = {};

                            if (ieltsNumber && ieltsNumber > 0) {
                                var IELTSJSON = {
                                    "name": "ielts academic",
                                    "description": ieltsDict.description,
                                    "min": 0,
                                    "require": ieltsNumber,
                                    "max": 9,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (ieltsNumber) {

                                    penglishList.push(IELTSJSON);
                                }
                            }

                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }

                            var academicReq = "";
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList;
                                        }
                                    }
                                }
                            }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq];
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }
                            ///progrssbar End
                            let resEnglishReqMoreDetailsJson = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                            let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.academic_requirements_url = "";
                                resJsonData.course_admission_requirement.english_requirements_url = "";
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = "";
                            var fullTimeText = "";
                            var cDuratiion = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time)
                          
                            if (cDuratiion == null) {
                               
                                cDuratiion = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                                cDuratiion = cDuratiion.split('|')[1].trim();
                                console.log("sandip gohel----->",cDuratiion);
                            }

                            else if (cDuratiion.includes('-')) {
                                console.log("con1--------?")
                                cDuratiion = cDuratiion.split('-')[0].trim();
                            }
                            else if (cDuratiion.includes('2 years')) {
                                console.log("con12--------?")
                                cDuratiion = "2 years";
                            }
                            else if (cDuratiion.includes('–')) {
                                cDuratiion = cDuratiion.split('–')[0].trim();
                            }
                            else if (cDuratiion.includes('s,')) {
                               
                                cDuratiion = cDuratiion.split(',')[0].trim();
                                console.log("hello ji--->",cDuratiion)
                            }
                            else if (cDuratiion.includes('–')) {

                                cDuratiion = cDuratiion.split('–')[0].trim();
                            }
                            else  {
                                cDuratiion = 
                                console.log("fdgdgddgdb------->",cDuratiion);
                            }

                                
                            

                            fullTimeText = cDuratiion
                            if (fullTimeText && fullTimeText.length > 0) {
                                const resFulltime = fullTimeText;
                                if (resFulltime) {
                                    durationFullTime = resFulltime;
                                    courseDurationList = durationFullTime;
                                    courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));


                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = courseDurationList;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;

                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            else {

                                fullTimeText = cDuratiion;
                                if (fullTimeText && fullTimeText.length > 0) {
                                    const resFulltime = fullTimeText;
                                    if (resFulltime) {
                                        durationFullTime = resFulltime;
                                        courseDurationList = durationFullTime;
                                        courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));


                                        let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                        if (courseDurationList && courseDurationList.length > 0) {
                                            resJsonData.course_duration = courseDurationList;
                                        }
                                        if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                            resJsonData.course_duration_display = filtered_duration_formated;
                                            var isfulltime = false, isparttime = false;
                                            filtered_duration_formated.forEach(element => {
                                                if (element.display == "Full-Time") {
                                                    isfulltime = true;
                                                }
                                                if (element.display == "Part-Time") {
                                                    isparttime = true;
                                                }
                                            });
                                            resJsonData.isfulltime = isfulltime;
                                            resJsonData.isparttime = isparttime;
                                        }
                                    }
                                }

                            }
                            break;

                        }

                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        case 'course_campus_location': { // Location Launceston
                            var campLocationText = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;


                            var coursetitleforlocation = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            console.log("hiii--->",coursetitleforlocation)
                            if (coursetitleforlocation.includes('Network Technician')) {
                                var courselocation = await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location1);


                            }
                            var campusss = [];
                            console.log("keyvalll--->",JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selLists12222 = ' + JSON.stringify(selList));
                                            console.log("selList--->", selList);
                                            if (selList.includes("Important Notice")) {
                                                console.log("hii---->");

                                            }
                                            console.log("HHEEE-->", selList);
                                            if (Array.isArray(selList) && selList.length > 0) {

                                                var campLocationText1 = String(selList).split('|')[2].trim();
                                                var campLocationText = [campLocationText1]
                                                console.log("camplocationtext-->", campLocationText);

                                            }
                                            else {
                                                throw new Error("CAMPUS LOCATION NOT FOUND !!!");
                                            }
                                        }
                                    }
                                }
                            }

                            if (campLocationText && campLocationText.length > 0) {
                                var campusedata = [];
                                campLocationText.forEach(element => {
                                    campusedata.push({
                                        "name": element,
                                        "code": "",

                                    })
                                });
                                resJsonData.course_campus_location = campusedata;


                            }
                            else {
                                throw new Error("No campus location found.");
                            }
                            resJsonData.course_study_mode = "On campus";


                        }

                        case 'course_tuition_fees_international_student_more':
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {
                                international_student: [],


                            };

                            const feesIntStudent = arraydata22;

                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed7 = ' + feesWithDollorTrimmed);



                                const arrval = String(feesWithDollorTrimmed).split('.');

                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;

                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Semester",

                                                description: arraydata33[0] + "(" + arraydata44[0] + ")"

                                            });

                                        }
                                    }
                                }
                            }
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed8 = ' + feesWithDollorTrimmed);



                                const arrval = String(feesWithDollorTrimmed).split(',$');

                                const feesVal = String(arrval[1]);
                                const feesval1 = feesVal.split('.')[0];
                                console.log(funcName + 'feesVal8 = ' + feesval1);
                                if (feesval1) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesval1.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum88 = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum8888 = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber888888 = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Semester",

                                                description: arraydata33[1] + "(" + arraydata44[1] + ")"

                                            });

                                        }
                                    }
                                }
                            }
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed222 = ' + feesWithDollorTrimmed);



                                const arrval = String(feesWithDollorTrimmed).split(',$');

                                const feesVal = String(arrval[2]);
                                const feesVal2 = feesVal.split('.')[0];
                                console.log(funcName + 'feesVal2222 = ' + feesVal2);
                                if (feesVal2) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal2.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum222222 = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum2212 = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber1123 = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Semester",

                                                description: arraydata33[2] + "(" + arraydata44[2] + ")"

                                            });

                                        }
                                    }
                                }
                            }
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed7 = ' + feesWithDollorTrimmed);



                                const arrval = String(feesWithDollorTrimmed).split(',$');

                                const feesVal = String(arrval[3]);
                                var feesVal77 = feesVal.split('.')[0].trim();
                                console.log(funcName + 'feesVal = ' + feesVal77);
                                if (feesVal77) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal77.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Semester",
                                                
                                                description: arraydata33[3] + "(" + arraydata44[3] + ")",
                                        
                                            });

                                        }
                                    }
                                }
                            }
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed7 = ' + feesWithDollorTrimmed);



                                const arrval = String(feesWithDollorTrimmed).split(',$');

                                const feesVal = String(arrval[4]);
                                var feesval9 = feesVal.split('.')[0].trim()
                                console.log(funcName + 'feesVal1112 = ' + feesval9);
                                if (feesval9) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesval9.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum4562 = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum7979 = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber58888 = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Semester",

                                                description: arraydata33[4] + "(" + arraydata44[4] + ")"

                                            });

                                        }
                                    }
                                }
                            } else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: 1,
                                    unit: "Semester",

                                    description: feesIntStudent

                                });
                            }
                            // 
                            //  let international_student_all_fees_array = [];
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    //      feesDict.international_student_all_fees = international_student_all_fees_array
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }

                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {
                                international_student: {},


                            };

                            const feesIntStudent = Fees;


                            var fee_desc_more = []; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal_more = courseScrappedData.course_tuition_fees_international_student_more;
                            if (Array.isArray(courseKeyVal_more)) {
                                for (const rootEleDict of courseKeyVal_more) {
                                    console.log(funcName + '\n\r rootEleDict767676767 = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict464454 = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList6464646 = ' + JSON.stringify(selList));
                                            for (let sel of selList) {
                                                sel = sel.replace(/[\r\n\t ]+/g, ' ')
                                                fee_desc_more.push(sel);

                                            }
                                        }
                                    }
                                }
                            }

                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                    //  feesDict.international_student_all_fees = fee_desc_more
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }

                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }

                                // take tuition fee value at json top level so will help in DDB for indexing
                                if (courseTuitionFee && feesDict.international_student) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                    // resJsonData.course_tuition_fee_amount = feesDict.international_student;

                                }

                            }
                            console.log(funcName + 'feesDict.international_student = ' + feesDict.course_tuition_fee);
                            // fail and return null if we do not have valid tuitin_fee value available because it is mandatory field
                            if (!feesDict.international_student) {
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                console.log(funcName + 'Invalid TUITION FEE AMOUNT so break formatting function and continue with next course....');
                                console.log(funcName + 'feesDict.international_student = ' + feesDict.international_student);
                                console.log('\n\r\n\r!!!!!!!!!!!!!!!!!!!!!\n\r');
                                return null; // this will add this item into FailedItemList and writes file to local disk
                            }

                            break;
                        }
                        case 'program_code': {
                            let courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.program_code);
                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            if (title.includes('Computer Systems')) {
                            console.log("program title-->",title)

                                resJsonData.program_code = "CETN"
                                break;
                            }

                            console.log("jiijjijijjjij--->>")
                            let courseKeyVal1 = courseKeyVal.split("(")[1].trim();
                            let courseKeyVal2 = courseKeyVal1.split(")")[0].trim();
                            if (courseKeyVal2 && courseKeyVal2.length > 0) {
                                resJsonData.program_code = courseKeyVal2;
                            }
                            else {
                                resJsonData.program_code = "";
                            }


                            //}
                            break;
                        }

                        // case 'course_intake':
                        //     {
                        //         let courseIntakeStr = [];
                        //         var tempIntakeArray = [];
                        //         var courseKeyVal = courseScrappedData.course_intake;
                        //         var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                        //         if (title.includes('Computer Systems Technician')) {
                        //             var intek = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake1)
                        //             intek = intek.split('SEMESTER 1').trim();
                        //             console.log("intekk-->",intek)
                        //             courseKeyVal = intek;
                        //         }
                        //         if (Array.isArray(courseKeyVal)) {
                        //             for (const rootEleDict of courseKeyVal) {
                        //                 console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //                 const elementsList = rootEleDict.elements;
                        //                 for (const eleDict of elementsList) {
                        //                     console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                     const selectorsList = eleDict.selectors;
                        //                     for (const selList of selectorsList) {
                        //                         console.log(funcName + '\n\r selList Intake= ' + JSON.stringify(selList));
                        //                         // if (Array.isArray(selList) && selList.length > 0) {


                        //                         for (let selItem of selList) {
                        //                             selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r)/g, '')
                        //                             console.log("sellitem -->", selItem);;
                        //                             if (selItem.includes('Fall 2019')) {
                        //                                 console.log("intakeeee-==-==-=->");
                        //                                 selItem = "September 2019"
                        //                             }
                        //                             if (selItem.includes('Spring 2020')) {
                        //                                 console.log("intakeeee-==-==-=->");
                        //                                 selItem = "May 2020"
                        //                             }
                        //                             if (selItem.includes('Fall 2020')) {
                        //                                 console.log("intakeeee-==-==-=->");
                        //                                 selItem = "September 2020"
                        //                             }
                        //                             if (selItem.includes('Winter 2020')) {
                        //                                 console.log("intakeeee-==-==-=->");
                        //                                 selItem = "January 2020"
                        //                             }
                        //                             if (selItem.includes('Winter 2021')) {
                        //                                 console.log("intakeeee-==-==-=->");
                        //                                 selItem = "January 2021"
                        //                             }
                        //                             if (selItem.includes('Spring 2021')) {
                        //                                 console.log("intakeeee-==-==-=->");
                        //                                 selItem = "May 2021"
                        //                             }
                        //                             tempIntakeArray.push(selItem);
                        //                         }


                        //                         if (tempIntakeArray && tempIntakeArray.length > 0) {

                        //                             for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                        //                                 courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": tempIntakeArray });
                        //                             }

                        //                         } else {
                        //                             throw new Error('Intake not found');
                        //                             for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                        //                                 courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": ["February", "July"] });
                        //                             }
                        //                         }

                        //                     }
                        //                 }
                        //             }
                        //         }
                        //         var intakeUrl = [];
                        //         let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                        //         let formatIntake = await format_functions.providemyintake(courseIntakeStr, "MOM", "");
                        //         if (formatIntake && formatIntake.length > 0) {
                        //             var intakedata = {};
                        //             intakedata.intake = formatIntake;
                        //             intakedata.more_details = more_details;
                        //             resJsonData.course_intake = intakedata;

                        //         }
                        //         break;
                        //     }


                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                        }

                            break;
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r careerselList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                // course_career_outcome = selList;
                                                for (let selItem of selList) {
                                                    console.log("selItemCareer -->", selItem);
                                                    selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ');
                                                    course_career_outcome.push(selItem.trim())
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }
                        case 'course_study_level':
                            var studylevel = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level)


                            var studylevel1 = studylevel.split('|')[0].trim();
                            if (studylevel1.includes('Ontario College')) {
                                var studylevel2 = studylevel1.split('Ontario College')[1].trim();
                                resJsonData.course_study_level = studylevel2;
                            }
                            else {
                                resJsonData.course_study_level = studylevel1;
                            }
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            var NEWJSONSTRUCT = {}, structDict = [];
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                console.log("lastlocation---->", resJsonData.course_title);

                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/\s/g, '').replace(/[\/]+/g, '_').toLowerCase();
                console.log("prevlocation---->");
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                console.log("bdsfhklssssssss------>", JSON.stringify(resJsonData.course_intake));
                // var intakes = resJsonData.course_intake.intake;

                var matchrec = [];
                // for (let dintake of intakes) {
                //     if (location == dintake.name) {
                //         matchrec = dintake.value;
                //     }
                // }
                // if (matchrec.length > 0) {
                //     NEWJSONSTRUCT.course_intake = matchrec[0];
                // }
                // else {
                //     NEWJSONSTRUCT.course_intake = [];
                // }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        //     NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                // resJsonData.basecourseid = location_wise_data.course_id;



                // for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                //     if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_intake.intake[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_intake.intake[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                //     if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                //     }
                // }

                // for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                //     if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                //         resJsonData.course_campus_location[i].iscurrent = true;
                //     }
                //     else {
                //         resJsonData.course_campus_location[i].iscurrent = false;
                //     }
                // }

                // resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;

                //resJsonData.current_course_intake = location_wise_data.course_intake;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));
                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = [];
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body), date: new Date() })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
                // const res = await awsUtil.putFileInBucket(filelocation);
                // console.log(funcName + 'S3 object location = ' + res);
            }
            // fs.writeFileSync("./output/new_structure_data.json", JSON.stringify(structDict));
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
