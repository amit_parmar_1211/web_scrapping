/* eslint-disable camelcase */
const outputDirPath = './output/';
const errorDirPath = './error/';
// univ_id must NOT have any white space or empty space and it MUST be in lowercase
const univ_id = 'conestogacollege';
const univ_name = 'Conestoga College';

module.exports = {
  opOverviewFilepath: outputDirPath + univ_id + '_overview.json',
  opCoursePageListFilepath: outputDirPath + univ_id + '_pagelist.json',
  opCourseListFilepath: outputDirPath + univ_id + '_courselist.json',
  opCourseDetailsFilepath: outputDirPath + univ_id + '_coursedetails.json',
  opUpdateCourseDetailsFilepath: outputDirPath + univ_id + '_update_coursedetails.json',
  opFailedCourseList: errorDirPath + univ_id + '_failed_courselist.json',
  opFailedCourseListByAPI: errorDirPath + univ_id + '_api_failed_courselist.json',

  univ_id,
  univ_name,
  pageListJsonRootKey: 'course_page_list', 
  propValueNotAvaialble: 'N/A',
  outputDirPath,
  puptrPageGotoOptions: { waitUntil: 'networkidle0' },
};
