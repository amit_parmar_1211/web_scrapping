const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, category_name) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            console.log("course_name-->", course_name);
            const intakefees = JSON.parse(fs.readFileSync("./Mapping/international_details.json"));
            //var myintake_data = JSON.parse(fs.readFileSync(appConfigs.hardCodedJsonDataFilepath))[0];
            console.log("intakessfees-->", intakefees);
            let tmpvar = intakefees.filter(val => {
                return val.PROGRAM_NAME.toLowerCase == course_name.toLowerCase

            });
            let arraydata11 = [];
            let arraydata22 = [];
            let arraydata33 = [];
            let arraydata44 = [];
            let arraydata55 = [];
            let arraydata66 = [];
            let arraydata77 = [];
            let arraydata88 = [];
            var ProgramCode;
            var Intakee;
            var Credential;
            var CourseDuration;
            var FeesType;
            var Academic;
            var FeesInt;
            for (let arraydata of tmpvar) {
                arraydata11.push(arraydata);
                console.log("tarraydata11mtam-=-=>", arraydata11);

            }
            for (let tmpv of arraydata11) {

                arraydata22.push(tmpv.TUITION);
                arraydata33.push(tmpv.CODE);
                arraydata44.push(tmpv.START);
                arraydata55.push(tmpv.CREDENTIAL);
                arraydata66.push(tmpv.LENGTH);
                arraydata77.push(tmpv.REQUIREMENTS);
                arraydata88.push(tmpv.APPLIED_LEARNING);
                ProgramCode = tmpv.Course_code;

            }
            for (let tmpv of arraydata11) {

                //  arraydata33.push(tmpv.Term);


                //  console.log("p4444444444444444444", arraydata33);
                ProgramCode = tmpv.CODE;
                Intakee = tmpv.START;
                Credential = tmpv.CREDENTIAL;
                CourseDuration = tmpv.LENGTH;
                FeesType = tmpv.APPLIED_LEARNING;
                Academic = tmpv.REQUIREMENTS;
                FeesInt = tmpv.TUITION;
                resJsonData.program_code = ProgramCode;
            }


            console.log("resJsonData.program_code--->", resJsonData.program_code);
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            // As of not we do not have way to get this field value
                            resJsonData.course_discipline = [category_name];
                            break;
                        }
                        case 'course_title': {

                            var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            var ctitle = format_functions.titleCase(title).trim();
                            console.log("ctitlectitlectitle---->", ctitle);
                            resJsonData.course_title = ctitle + "(" + category_name + ")"

                            break;
                        }
                        case 'international_apply': {
                            var applicants = await Course.extractValueFromScrappedElement(courseScrappedData.international_apply)
                            if (applicants == null) {
                                throw new Error("Not for International Students-->");
                            }
                            console.log("applicants-->", applicants);
                        }

                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                          
                            const myscrore = await utils.getMappingScore(title);
                            console.log("titlefortofel--->",title);
                            console.log("## Score result--->" + JSON.stringify(myscrore));
                        
                            var ibtDict = {}, pbtDict = {}, catDict = {}, pteDict = {}, ieltsDict = {}, caeDict = {};
                            ibtDict.name = 'toefl ibt';
                            ibtDict.description = myscrore.IBT;
                            englishList.push(ibtDict);

                            pbtDict.name = 'toefl pbt';
                            pbtDict.description = myscrore.PBT;
                            englishList.push(pbtDict);
                            pteDict.name = 'pte academic';
                            pteDict.description = myscrore.PTE;
                            englishList.push(pteDict);
                            ieltsDict.name = 'ielts academic';
                            ieltsDict.description = myscrore.IELTS;
                            englishList.push(ieltsDict);




                            let ieltsNumber = null; let pbtNumber = null; let pteNumber = null; let caeNumber = null; let ibtNumber = null;
                            const penglishList = [];
                            const penglishList1 = [];
                            var ieltsfinaloutput = null;
                            var tofelfinaloutput = null;
                            var ptefinaloutput = null;

                            var ibtfinaloutput = null;
                            var pbtfinaloutput = null;
                            const regEx = /[+-]?\d+(\.\d+)?/g;
                            var matchedStrList = String(myscrore.IELTS).match(regEx);
                            console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                            
                            if (matchedStrList && matchedStrList.length > 0) {
                                ieltsNumber = Number(matchedStrList[0]);
                                console.log(funcName + 'ieltsNumber = ' + ieltsNumber);
                            }
                            if(matchedStrList==null)
                            {
                                console.log("hgffkgvkhhfkjhfk--->");
                                matchedStrList = 0;
                            }
                            if (ieltsNumber && ieltsNumber > 0) {
                                //tofel score
                                console.log("myscrore.IELTS -->", myscrore.IELTS);
                                let tempTOFEL = [
                                    {
                                        "elements":
                                            [
                                                {
                                                    "selectors":
                                                        [
                                                            [myscrore.IELTS]
                                                        ]
                                                }
                                            ]
                                    }
                                ]
                                const tofelScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempTOFEL);
                                if (tofelScore && tofelScore.length > 0) {
                                    tofelfinaloutput = tofelScore.toString();
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(tofelScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pbtNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'pbtNumber = ' + pbtNumber);
                                    }
                                }
                                //pte score
                                console.log("myscrore.PTE -->", myscrore.PTE);
                                let tempPTE = [
                                    {
                                        "elements":
                                            [
                                                {
                                                    "selectors":
                                                        [
                                                            [myscrore.PTE]
                                                        ]
                                                }
                                            ]
                                    }
                                ]
                                const pteAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempPTE);
                                if (pteAScore && pteAScore.length > 0) {
                                    ptefinaloutput = pteAScore.toString();
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(pteAScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pteNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'pteNumber = ' + pteNumber);
                                    }
                                }
                                //cae score
                                let tempPBT = [
                                    {
                                        "elements":
                                            [
                                                {
                                                    "selectors":
                                                        [
                                                            [myscrore.PBT]
                                                        ]
                                                }
                                            ]
                                    }
                                ]
                                const pbtAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempPBT);
                                if (pbtAScore && pbtAScore.length > 0) {
                                    pbtfinaloutput = pbtAScore.toString();
                                    // extract exact number from string
                                    const regEx = /[+-]?\d+(\.\d+)?/g;
                                    const matchedStrList = String(pbtAScore).match(regEx);
                                    console.log(funcName + 'matchedStrList = ' + JSON.stringify(matchedStrList));
                                    if (matchedStrList && matchedStrList.length > 0) {
                                        pbtNumber = Number(matchedStrList[0]);
                                        console.log(funcName + 'pteNumber = ' + pbtNumber);
                                    }
                                }
                                console.log("myscrore.CAE -->", myscrore.CAE);
                            }

                            let tempIBT = [
                                {
                                    "elements":
                                        [
                                            {
                                                "selectors":
                                                    [
                                                        [myscrore.IBT]
                                                    ]
                                            }
                                        ]
                                }
                            ]
                            const ibtAScore = await Course.extractValueEnglishlanguareFromScrappedElement(tempIBT);
                            if (ibtAScore && ibtAScore.length > 0) {
                                ibtfinaloutput = ibtAScore.toString();
                                // extract exact number from string
                                const regEx = /[+-]?\d+(\.\d+)?/g;
                                const matchedStrList = String(ibtAScore).match(regEx);
                                console.log(funcName + 'matchedStrList333 = ' + JSON.stringify(matchedStrList));
                                if (matchedStrList && matchedStrList.length > 0) {
                                    ibtNumber = Number(matchedStrList[1]);
                                    console.log("ibtNumber---->", ibtNumber);
                                    var ibtNumber1 = String(ibtNumber).split('-')[1];

                                    console.log(funcName + 'ibtNumber222 = ' + ibtNumber1);
                                }
                            }

                            ///progress bar start
                            const pieltsDict = {}; const pibtDict = {}; const cibtDict = {}; const ppbtDict = {}; const ppteDict = {};

                            if (ieltsNumber && ieltsNumber > 0) {
                                var IELTSJSON = {
                                    "name": "ielts academic",
                                    "description": ieltsDict.description,
                                    "min": 0,
                                    "require": ieltsNumber,
                                    "max": 9,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };

                                if (ieltsNumber) {
                                    // pieltsDict.name = 'ielts academic';
                                    //  pieltsDict.value = IELTSJSON;
                                    penglishList.push(IELTSJSON);
                                }
                            }
                            else{
                                var IELTSJSON1 = {
                                "name": "ielts academic",
                                "description": "",
                                "min": 0,
                                "require": "",
                                "max": 9,
                                "R": 0,
                                "W": 0,
                                "S": 0,
                                "L": 0,
                                "O": 0
                            }
                            penglishList.push(IELTSJSON1);
                        };
                         
console.log("kkjkkjkkkkjkk--->")
                            if (ibtNumber1 && ibtNumber1 > 0) {
                                console.log("ibtnumber121---->", ibtNumber1);
                                var IBTJSON = {
                                    "name": "toefl ibt",
                                    "description": ibtDict.description,
                                    "min": 0,
                                    "require": Number(ibtNumber1),
                                    "max": 120,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (ibtNumber1) {
                                    //  pibtDict.name = 'toefl ibt';
                                    // pibtDict.value = IBTJSON;
                                    penglishList1.push(IBTJSON);
                                }
                            }
                            else
                            {
                                var IBTJSON1 = {
                                    "name": "toefl ibt",
                                    "description": "",
                                    "min": 0,
                                    "require": "",
                                    "max": 120,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                }; 
                                penglishList.push(IBTJSON1);
                            }
                          
                            if (pbtNumber && pbtNumber > 0) {
                                var PBTJSON = {
                                    "name": "toefl pbt",
                                    "description": pbtDict.description,
                                    "min": 310,
                                    "require": pbtNumber,
                                    "max": 677,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (pbtNumber) {
                                    //  pibtDict.name = 'toefl ibt';
                                    // pibtDict.value = IBTJSON;
                                    penglishList.push(PBTJSON);
                                }
                            }
                            
                            else{
                                var PBTJSON1 = {
                                    "name": "toefl pbt",
                                    "description": "",
                                    "min": 310,
                                    "require": "",
                                    "max": 677,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                penglishList.push(PBTJSON1);
                            }
                         
                            if (pteNumber && pteNumber > 0) {
                                var PTEJSON = {
                                    "name": "pte academic",
                                    "description": pteDict.description,
                                    "min": 0,
                                    "require": pteNumber,
                                    "max": 90,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                if (pteNumber) {
                                    //  ppteDict.name = 'pte academic';
                                    //  ppteDict.value = PTEJSON;
                                    penglishList.push(PTEJSON);
                                }
                            }
                            else
                            {
                                var PTEJSON1 = {
                                    "name": "pte academic",
                                    "description": "",
                                    "min": 0,
                                    "require": "",
                                    "max": 90,
                                    "R": 0,
                                    "W": 0,
                                    "S": 0,
                                    "L": 0,
                                    "O": 0
                                };
                                penglishList.push(PTEJSON1);
                            }
                          
                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }

                            var academicReq = "";
                            const courseKeyVal = Academic;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            // if (Array.isArray(courseKeyVal)) {
                            //     for (const rootEleDict of courseKeyVal) {
                            //         console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                            //         const elementsList = rootEleDict.elements;
                            //         for (const eleDict of elementsList) {
                            //             console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                            //             const selectorsList = eleDict.selectors;
                            //             for (const selList of selectorsList) {
                                            academicReq = [courseKeyVal];
                            //             }
                            //         }
                            //     }
                            // }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            if (courseScrappedData.course_academic_requirement) {
                                const academicReq = await Course.extractValueFromScrappedElement(courseScrappedData.course_academic_requirement);
                                console.log('\n\r');
                                console.log(funcName + 'academicReq = ' + academicReq);
                                console.log(funcName + 'academicReq.length = ' + String(academicReq).length);
                                console.log('\n\r');
                                if (academicReq && String(academicReq).length > 0) {
                                    courseAdminReq.academic = [academicReq];
                                } else {
                                    courseAdminReq.academic = [];
                                }
                            }
                            // if courseAdminReq has any valid value then only assign it
                            if (courseAdminReq.english || courseAdminReq.academic) {
                                resJsonData.course_admission_requirement = courseAdminReq;
                            }
                            ///progrssbar End
                            let resEnglishReqMoreDetailsJson = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            let english_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.english_requirements_url);
                             let entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            if (resEnglishReqMoreDetailsJson && resJsonData.course_admission_requirement) {
                                resJsonData.course_admission_requirement.academic_requirements_url = resEnglishReqMoreDetailsJson;
                                resJsonData.course_admission_requirement.english_requirements_url = english_requirements_url;
                                resJsonData.course_admission_requirement.entry_requirements_url = entry_requirements_url;
                            } else if (resEnglishReqMoreDetailsJson) {
                                resJsonData.course_admission_requirement = {};
                                resJsonData.course_admission_requirement.academic_requirements_url = "";
                                resJsonData.course_admission_requirement.english_requirements_url = "";
                                resJsonData.course_admission_requirement.entry_requirements_url = "";
                            }
                            break;
                        }
                       
                        case 'course_url': {
                            if (courseUrl && courseUrl.length > 0) {
                                resJsonData.course_url = courseUrl;
                            }
                            break;
                        }
                        case 'course_duration_full_time': {
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            var cDuratiion = CourseDuration;
                            console.log("cDuratiion-0-0-90--->",cDuratiion);
                              
                                //cDuratiion = cDuratiion.split('')[0].trim();

                               
                                fullTimeText = cDuratiion
                                if (fullTimeText && fullTimeText.length > 0) {
                                    const resFulltime = fullTimeText;
                                    if (resFulltime) {
                                        durationFullTime.duration_full_time = resFulltime;
                                        courseDurationList.push(durationFullTime);
                                        courseDurationDisplayList.push(await format_functions.validate_course_duration_full_time(resFulltime));

                                        let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                        if (courseDurationList && courseDurationList.length > 0) {
                                            resJsonData.course_duration = courseDurationList;
                                        }
                                        if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                            resJsonData.course_duration_display = filtered_duration_formated;
                                            var isfulltime = false, isparttime = false;
                                            filtered_duration_formated.forEach(element => {
                                                if (element.display == "Full-Time") {
                                                    isfulltime = true;
                                                }
                                                if (element.display == "Part-Time") {
                                                    isparttime = true;
                                                }
                                            });
                                            resJsonData.isfulltime = isfulltime;
                                            resJsonData.isparttime = isparttime;
                                        }
                                    }
                                }
                            
                            break;

                        }
                     
                        case 'course_tuition_fee':
                        case 'course_study_mode':
                        case 'page_url':

                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':

                        case 'course_campus_location': { // Location Launceston
                            var campLocationText = null; //await Course.extractValueFromScrappedElement(courseScrappedData.course_campus_location);
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            campLocationText = String(selList).split(",")[0].trim();
                                            var campLocationText1 = [campLocationText];
                                            console.log("campLocationText--------00000-0-00>", campLocationText);
                                            var uniqueArray = Array.from(new Set(campLocationText1));
                                            console.log("uniqueArr--->", uniqueArray);
                                            //campLocationText = selList;
                                            ///}
                                        }
                                    }
                                }
                            }
                            console.log("##Campus-->" + campLocationText)
                            if (uniqueArray && uniqueArray.length > 0) {
                                var campusedata = [];
                                uniqueArray.forEach(element => {
                                    campusedata.push({
                                        "name": element,
                                        "city": "",
                                        "state": "",
                                        "country": "",
                                        "iscurrent": false
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                console.log("resJsonData.course_campus_location", resJsonData.course_campus_location);

                            }
                            else {
                                throw new Error("No campus location found.");
                            }
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'course_intake':
                            {
                                let courseIntakeStr = [];
                                var tempIntakeArray = [];
                                const courseKeyVal = Intakee;
                                console.log("courseKeyVal-->", courseKeyVal);
                                if (courseKeyVal.includes(',')) {
                                    var courseKeyVal1 = String(courseKeyVal).split(',');
                                    var courseKeyVal2 = courseKeyVal1
                                    console.log("hi i am course key val-->", courseKeyVal2);
                                }
                                var selItem = courseKeyVal2;
                               console.log("sellllllllllll-->",selItem);

                               // tempIntakeArray.push(selItem);
                                


                                console.log("tempIntakeArray -->1", tempIntakeArray.length);
                                console.log("tempIntakeArray-->", tempIntakeArray);
                                if (selItem && selItem.length > 0) {
                                    console.log("resJsonData.course_campus_location -->1", resJsonData.course_campus_location);
                                    for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                                        courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": selItem });
                                    }
                                    console.log("courseIntakeStr -->", JSON.stringify(courseIntakeStr));
                                } else {
                                    throw new Error('Intake not found');
                                    for (let j = 0; j < resJsonData.course_campus_location.length; j++) {
                                        courseIntakeStr.push({ "name": resJsonData.course_campus_location[j].name, "value": ["February", "July"] });
                                    }
                                }





                                var intakeUrl = [];
                                let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                                console.log("courseKeyVal1-->", courseIntakeStr);
                                let formatIntake = await format_functions.providemyintake(courseIntakeStr, "MOM", "");
                                if (formatIntake && formatIntake.length > 0) {
                                    var intakedata = {};
                                    intakedata.intake = formatIntake;
                                    intakedata.more_details = more_details;
                                    resJsonData.course_intake = intakedata;
                                    console.log("resJsonData.course_intake -->> ", resJsonData.course_intake);
                                }
                                break;
                            }
                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            const feesList = [];
                            const feesDict = {};
                            // console.log("demoarrray--->",demoarray);
                            // let duration_yesr = demoarray[0].duration;
                            // console.log("duration_yesr -->", duration_yesr);

                            // let duration_unit = demoarray[0].unit;
                            // console.log("duration_yesr -->", duration_unit);
                            

                          //  var myfee = "";
                           // var title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);
                            // filedata["fees"].forEach(element => {
                            //     if (element.name == title) {
                            //         myfee = element.fee;
                            //     }
                            // });
                            const feesIntStudent = FeesInt;
                            console.log("Fee in student--->" + feesIntStudent);

                            // if we can extract value as Int successfully then replace Or keep as it is

                            
                            if (feesIntStudent) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();
                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);
                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');
                                const arrval = String(feesVal1).split('.');
                                const feesVal = String(arrval[0]);
                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    if (feesVal != "NA" && feesVal != "N A") {
                                        const regEx = /\d/g;
                                        let feesValNum = feesVal.match(regEx);
                                        console.log("feesValNum=-0=0=0=0=>", feesValNum);
                                        if (feesValNum) {
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            feesValNum = feesValNum.join('');
                                            console.log(funcName + 'feesValNum = ' + feesValNum);
                                            let feesNumber = null;
                                            if (feesValNum.includes(',')) {
                                                feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                            } else {
                                                feesNumber = feesValNum;
                                            }
                                            console.log(funcName + 'feesNumber = ' + feesNumber);
                                            if (Number(feesNumber)) {
                                                var feesint = {};
                                                feesint.amount = Number(feesNumber);
                                                feesint.duration = 1,
                                                    feesint.unit = "Year",
                                                    feesint.isfulltime = false;
                                                feesint.description = FeesInt;
                                                feesint.type = FeesType;
                                                feesDict.international_student = [feesint];
                                            }
                                        }
                                    }

                                }
                            }
                            else {
                                console.log("else part !!!!");
                                var feesint = {};
                                feesint.amount = 0;
                                feesint.duration = 1,
                                    feesint.unit = "Year",
                                    feesint.isfulltime = false;
                                feesint.description = FeesInt;
                                feesint.type = FeesType;
                                feesDict.international_student = [feesint];
                            }
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                    var more_fee = [];
                                    feesDict.international_student_all_fees = more_fee;
                                }
                                if (feesDict) {
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':


                       

                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            console.log(funcName + 'matched case: ' + key);
                            const rootElementDictList = courseScrappedData[key];
                            console.log(funcName + 'key-val is ' + JSON.stringify(rootElementDictList));
                            let concatnatedRootElementsStr = null;
                            for (const rootEleDict of rootElementDictList) {
                                console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                const elementsList = rootEleDict.elements;
                                let concatnatedElementsStr = null;
                                for (const eleDict of elementsList) {
                                    console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                    const selectorsList = eleDict.selectors;
                                    let concatnatedSelectorsStr = null;
                                    for (const selList of selectorsList) {
                                        console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                        console.log(funcName + '\n\r selList = ' + selList);
                                        let concatnatedSelStr = null;
                                        for (const selItem of selList) {
                                            if (!concatnatedSelStr) {
                                                concatnatedSelStr = selItem;
                                            } else {
                                                concatnatedSelStr = String(concatnatedSelStr).concat(' ').concat(selItem).trim();
                                            }
                                        } // selList
                                        console.log(funcName + 'concatnatedSelStr = ' + concatnatedSelStr);
                                        if (concatnatedSelStr) {
                                            if (!concatnatedSelectorsStr) {
                                                concatnatedSelectorsStr = concatnatedSelStr;
                                            } else {
                                                concatnatedSelectorsStr = String(concatnatedSelectorsStr).concat(' ').concat(concatnatedSelStr).trim();
                                            }
                                        }
                                    } // selectorsList
                                    console.log(funcName + 'concatnatedSelectorsStr = ' + concatnatedSelectorsStr);
                                    // concat elements
                                    if (concatnatedSelectorsStr) {
                                        if (!concatnatedElementsStr) {
                                            concatnatedElementsStr = concatnatedSelectorsStr;
                                        } else {
                                            concatnatedElementsStr = String(concatnatedElementsStr).concat(' ').concat(concatnatedSelectorsStr).trim();
                                        }
                                    }
                                } // elementsList
                                console.log(funcName + 'concatnatedElementsStr = ' + concatnatedElementsStr);
                                if (concatnatedElementsStr) {
                                    if (!concatnatedRootElementsStr) {
                                        concatnatedRootElementsStr = concatnatedElementsStr;
                                    } else {
                                        concatnatedRootElementsStr = String(concatnatedRootElementsStr).concat(' ').concat(concatnatedElementsStr).trim();
                                    }
                                }
                            } // rootElementDictList
                            console.log(funcName + 'concatnatedRootElementsStr =11 ' + concatnatedRootElementsStr);
                            // add only if it has valid value
                            // let concatnatedRootElementsStrArray = [];
                            if (concatnatedRootElementsStr && concatnatedRootElementsStr.length > 0) {
                                // concatnatedRootElementsStrArray.push(String(concatnatedRootElementsStr).trim());
                                resJsonData[key] = String(concatnatedRootElementsStr).trim();
                                // console.log("resJsonData[key] -->11",resJsonData[key]);
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r careerselList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                // course_career_outcome = selList;
                                                for (let selItem of selList) {
                                                    console.log("selItemCareer -->", selItem);
                                                    selItem = selItem.replace(/(?:\r\n\t|\r|\n|\t|\n\n|\n\t|\n\r|\t\r|\r\r|\t\t|\n\r|\s)/g, ' ');
                                                    course_career_outcome.push(selItem.trim())
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // if (course_career_outcome.length > 0) {
                            resJsonData.course_career_outcome = (course_career_outcome) ? course_career_outcome : [];
                            // }
                            break;
                        }

                        case 'course_study_level': {
                            var cStudyL = Credential;
                            // var cStudyL1 = cStudyL.split(")")[1].trim();
                             console.log("cStudyL", cStudyL);
                            // // let cStudyLevel = "";
                            resJsonData.course_study_level = cStudyL;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } // for (const key in courseScrappedData)
            // console.log('\n\r' + funcName + 'course resJsonData =' + JSON.stringify(resJsonData));
            // var resJsonData = JSON.parse(fs.readFileSync("australiancatholicuniversity_graduatediplomainrehabilitation_coursedetails.json"))[0];
            var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            //console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;
            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                // console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                // console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                // NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                // NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                // NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;
                 var intakes = resJsonData.course_intake.intake;
                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "NA";
                }
                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                resJsonData.course_id = location_wise_data.course_location_id;
                resJsonData.basecourseid = location_wise_data.course_id;
                for (var i = 0; i < resJsonData.course_intake.intake.length; i++) {
                    if (resJsonData.course_intake.intake[i].name === location_wise_data.course_campus_location) {
                        resJsonData.course_intake.intake[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_intake.intake[i].iscurrent = false;
                    }
                }

                for (var i = 0; i < resJsonData.course_tuition_fee.fees.length; i++) {
                    if (resJsonData.course_tuition_fee.fees[i].name === location_wise_data.course_campus_location) {
                        resJsonData.course_tuition_fee.fees[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_tuition_fee.fees[i].iscurrent = false;
                    }
                }

                for (var i = 0; i < resJsonData.course_campus_location.length; i++) {
                    if (resJsonData.course_campus_location[i].name === location_wise_data.course_campus_location) {
                        resJsonData.course_campus_location[i].iscurrent = true;
                    }
                    else {
                        resJsonData.course_campus_location[i].iscurrent = false;
                    }
                }
                //resJsonData.course_cricos_code = location_wise_data.cricos_code;
                // resJsonData.current_course_campus_location = location_wise_data.course_campus_location;
                //resJsonData.current_course_intake = location_wise_data.course_intake;
                //resJsonData.course_intake_display = location_wise_data.course_intake;
                //  resJsonData.international_student_all_fees = location_wise_data.international_student_all_fees;
                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));

                try {
                    if (appConfigs.ISINSERT) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/savecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISREPLACE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/replacecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            console.log("MyMessage-->" + JSON.stringify(myresponse))
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                    else if (appConfigs.ISUPDATE) {
                        request.post({
                            "headers": { "content-type": "application/json" },
                            "url": appConfigs.API_URL + "api/australia/course/v1/updatecourse",
                            "body": JSON.stringify(resJsonData)
                        }, (error, response, body) => {
                            if (error) {
                                console.log(error);
                            }
                            var myresponse = JSON.parse(body);
                            if (!myresponse.flag) {
                                var errorlog = null;
                                if (fs.existsSync(configs.opFailedCourseListByAPI)) {
                                    errorlog = JSON.parse(fs.readFileSync(configs.opFailedCourseListByAPI));
                                    errorlog.push({ name: resJsonData.course_title, reason: JSON.stringify(body) })
                                }
                                fs.writeFileSync(configs.opFailedCourseListByAPI, JSON.stringify(errorlog));
                            }
                            console.log("success save data! " + JSON.stringify(body));
                        });
                    }
                }
                catch (err) {
                    console.log("#### try-catch error--->" + err);
                }
                // const res = await awsUtil.putFileInBucket(filelocation);
                //console.log(funcName + 'S3 object location = ' + res);
            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
