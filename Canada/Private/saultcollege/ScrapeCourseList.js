
const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');
class ScrapeCourseList extends Scrape {
  async scrapeOnlyInternationalCourseList() {
    const funcName = 'scrapeCourseCategoryList ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: true });
      var totalCourseList = [];
      var name;
      var std;
      var href;
      await s.setupNewBrowserPage("https://www.saultcollege.ca/Programs/AZindex.asp");
      var mainCategory = [], redirecturl = [];

      // const category_val = "//*[@id='leftCol4M']/h2";
      // const studyl = "//*[@id='leftCol4M']/ul/li/text()"
      // const levels = await s.page.$x(category_val)


      const category_val = "//*[@id='leftCol4M']/h2";
      const levels = await s.page.$x(category_val)
      for (let country of levels) {
        name = await s.page.evaluate(el => el.textContent, country);
        ///  href = await s.page.evaluate(el => el.href, country);
        // redirecturl.push({ name: name, href: href });
        mainCategory.push(name);
      }
        console.log("subjectAreasArray-->", mainCategory)

      for (let catc of mainCategory) {
        const subjects = await s.page.$x("//*[@id='leftCol4M']//h2[contains(text(),'" + catc + "')]/following::ul[1]/li/a");
       console.log("subjects--->",subjects);
        // const studyl = "//*[@id='leftCol4M']/ul/li/text()"
       // const studyl_11 = await s.page.$x(studyl)
        //console.log("studyl_11=-=--=-=-=-=-", studyl_11);
        for (let i = 0; i < subjects.length; i++) {
          var elementstring = await s.page.evaluate(el => el.innerText, subjects[i]);
          var elementlink = await s.page.evaluate(el => el.href, subjects[i])
         // var studyl_22 = await s.page.evaluate(el => el.textContent, studyl_11[i]);
          // if (studyl_22.includes('(')) {
          //   var studyL = String(studyl_22).split('(')[0].trim();
          //   console.log("studyLstudyLstudyLstudyL--->",studyL);
          //   if(studyL.includes('Ontario College'))
          //   {
          //     var studyval = studyL.split('Ontario College')[1].trim();
          //     console.log("studyvalnew->>>>",studyval);
              totalCourseList.push({ href: elementlink, innerText: elementstring + "(" + catc + ")", category: catc })
           // }
        }  
            
         // }
         
            //  totalCourseList.push({ href: elementlink, innerText: elementstring, category: catc,  studyLevel: studyl_22 })
            
      }
      fs.writeFileSync("./output/saultcollege_maincategorylist.json", JSON.stringify(mainCategory))
      console.log("totalCourseList -->", totalCourseList);
      await fs.writeFileSync("./output/saultcollege_original_courselist.json", JSON.stringify(totalCourseList));
      let uniqueUrl = [];
      //unique url from the courselist file
      for (let i = 0; i < totalCourseList.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (totalCourseList[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: totalCourseList[i].href, innerText: totalCourseList[i].innerText, category: [] });
        }
      }
      await fs.writeFileSync("./output/saultcollege_unique_courselist.json", JSON.stringify(totalCourseList));
      //based on unique urls mapping of categories
      for (let i = 0; i < totalCourseList.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == totalCourseList[i].href) {
            if (uniqueUrl[j].category.includes(totalCourseList[i].category)) {

            } else {
              uniqueUrl[j].category.push(totalCourseList[i].category);
            }
          }
        }
      }
      // console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/saultcollege_courselist.json", JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      await s.browser.close();
      console.log(funcName + 'browser closed successfully.....');
      return true;
    
   } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      throw (error);
    }
  };
}
module.exports = { ScrapeCourseList };


