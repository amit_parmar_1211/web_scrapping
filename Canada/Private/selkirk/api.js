const request = require('request-promise');
const fs = require('fs');

start();
async function start() {
   var inputDir = "./Inputfiles";
   var myfiles = await readFile();
   for (let filename of myfiles) {
      console.log("## start file-->" + filename)
      var mynewdata = []
      var filedata = JSON.parse(fs.readFileSync(inputDir + "/" + filename));
      var outputdir = "./LocationConvertor/" + filedata[0].univ_id;
      if (!fs.existsSync(outputdir)) {
         fs.mkdirSync(outputdir);
      }
      console.log(filedata[0].contactInformation.university.place_id)
      var body = await getdata(filedata[0].contactInformation.university.place_id);
      var address_components = JSON.parse(body).result.address_components;
      var result = JSON.parse(body).result;
      for (let address of address_components) {
         if (address.types.includes("administrative_area_level_2")) {
            filedata[0].contactInformation.university.city = address.short_name;
         }
         if (address.types.includes("administrative_area_level_1")) {
            filedata[0].contactInformation.university.state = address.long_name;
         }
         if (address.types.includes("country")) {
            filedata[0].contactInformation.university.country = address.long_name;
         }
      }
      filedata[0].contactInformation.university.overview = "";
      filedata[0].contactInformation.university.name = filedata[0].univ_name;
      filedata[0].contactInformation.university.type = "University";
      filedata[0].contactInformation.university.latlong = result.geometry.location;
      mynewdata.push(filedata[0].contactInformation.university);
      fs.writeFileSync(outputdir + "/university_mapdata.json", JSON.stringify(JSON.parse(body)))
      filedata[0].googlerating = (result.rating) ? result.rating : "NA";
      for (let i = 0; i < filedata[0].contactInformation.campus.length; i++) {
         var body = await getdata(filedata[0].contactInformation.campus[i].place_id);
         var address_components = JSON.parse(body).result.address_components;
         var result = JSON.parse(body).result;
         for (let address of address_components) {
            if (address.types.includes("administrative_area_level_2")) {
               filedata[0].contactInformation.campus[i].city = address.short_name;
            }
            if (address.types.includes("administrative_area_level_1")) {
               filedata[0].contactInformation.campus[i].state = address.long_name;
            }
            if (address.types.includes("country")) {
               filedata[0].contactInformation.campus[i].country = address.long_name;
            }

         }
         filedata[0].contactInformation.campus[i].type = "Campus";
         filedata[0].contactInformation.campus[i].latlong = result.geometry.location;
         mynewdata.push(filedata[0].contactInformation.campus[i]);

         var dataofcampus = [{ "placeid": filedata[0].contactInformation.campus[i].place_id, "data": JSON.parse(body) }];
         fs.writeFileSync(outputdir + "/" + filedata[0].contactInformation.campus[i].place_id + ".json", JSON.stringify(dataofcampus))
      }
      delete filedata[0].contactInformation;
      filedata[0].contactInformation = mynewdata;
      fs.writeFileSync(outputdir + "/" + filename, JSON.stringify(filedata))
   }
}

async function readFile() {
   return await fs.readdirSync("./Inputfiles")
}
async function getdata(placeid) {
   var result = await request('https://maps.googleapis.com/maps/api/place/details/json?placeid=' + placeid + '&key=AIzaSyCkQ0pVMQyDUzOw0G5OUS71FpqrpbseX6Q', function (error, response, body) {
      return body;
   });
   return result;
}
