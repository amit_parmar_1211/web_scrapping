**James Cook University: Total Courses Details As On 28 Mar 2019**
* Total Courses = 138
* Courses without CRICOS code = 0
* Total available courses = 138
* Repeted = 0
**course_career_outcome**
* for some cases career opportunities tab is not given so it will not get displayed in some cases
* eg : https://www.jcu.edu.au/courses-and-study/international-courses/graduate-certificate-of-accounting

**IELTS**
* for some courses ielts is not given properly so add in hardcode file with its cricoscode
* ielts_mapping

* BACHELOR OF NURSING SCIENCE