const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, courseUrl, course_name, course_category) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            let category = await Course.extractValueFromScrappedElement(courseScrappedData.course_discipline)
                            //resJsonData.course_discipline = [category];
                            resJsonData.course_discipline = [];
                            // As of not we do not have way to get this field value
                            break;
                        }
                        case 'course_title': {
                            let course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            let course_title_text = format_functions.titleCase(String(course_title.split('-')[0]).trim());;
                            resJsonData.course_title = course_title_text;
                            break;
                        }
                        case 'course_study_level': {
                            let study_l;
                            const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);
                            //  let cStudyLevel = null;
                            if (cTitle.includes("Ontario College")) {
                                study_l = cTitle.split("Ontario College")[1].trim();
                            }
                            resJsonData.course_study_level = study_l
                        }

                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':


                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {}; const pteDict = {}; const tofelDict = {}; const caeDict = {}; let ieltsNumber = null;
                            const pieltsDict = {}; const pibtDict = {}; const ppteDict = {};
                            var penglishList = [];
                            let ielts = await Course.extractValueFromScrappedElement(courseScrappedData.ielts_req);
                            console.log("dhgfshfhfas", ielts);
                            //IELTS of 6.5 TOEFL iBT 79 Passed Lambton Institute of English placement test

                            if (ielts != null) {
                                console.log("iffffffffffff", ielts);
                                let ielts_req = ""
                                let splitielts;
                                let ibt;
                                if (ielts.includes("TOEFL")) {
                                    splitielts = ielts.split("TOEFL")[0].trim();
                                    //    console.log("GXFGDFGDGDG", splitielts)
                                    if (ielts.includes("iBT")) {
                                        ibt = ielts.split("iBT")[1].trim();
                                        console.log("GXFGDFGDGDG", ibt)
                                    }

                                }


                                ///progress bar start

                                var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "";
                                if (splitielts) {
                                    ieltsScore = await utils.giveMeNumber(splitielts.replace(/ /g, ' '));
                                }
                                if (ibt) {
                                    ibtScore = await utils.giveMeNumber(ibt.replace(/ /g, ' '));
                                }


                                if (ieltsScore) {
                                    pieltsDict.name = 'ielts academic';
                                    pieltsDict.description = splitielts;
                                    pieltsDict.min = 0;
                                    pieltsDict.require = ieltsScore;
                                    pieltsDict.max = 9;
                                    pieltsDict.R = 0;
                                    pieltsDict.W = 0;
                                    pieltsDict.S = 0;
                                    pieltsDict.L = 0;
                                    pieltsDict.O = 0;
                                    penglishList.push(pieltsDict);
                                }
                                if (ibtScore) {
                                    pibtDict.name = 'toefl ibt';
                                    pibtDict.description = "TOEFL iBT" + ibt
                                    pibtDict.min = 0;
                                    pibtDict.require = ibtScore;
                                    pibtDict.max = 120;
                                    pibtDict.W = 0;
                                    pibtDict.S = 0;
                                    pibtDict.L = 0;
                                    pibtDict.O = 0;
                                    penglishList.push(pibtDict);
                                }


                                // else {
                                //     console.log("ieltsScore@@@", ieltsScore);
                                //     if (ieltsScore.length == 0) {
                                //         console.log("ieltsScore@@@@@@@", ieltsScore);


                                //         pieltsDict.name = 'ielts academic';
                                //         pieltsDict.description = ielts_req;
                                //         pieltsDict.min = 0;
                                //         pieltsDict.require = 0;
                                //         pieltsDict.max = 9;
                                //         pieltsDict.R = 0;
                                //         pieltsDict.W = 0;
                                //         pieltsDict.S = 0;
                                //         pieltsDict.L = 0;
                                //         pieltsDict.O = 0;
                                //         penglishList.push(pieltsDict);
                                //     }
                                // }
                            } else {

                                pieltsDict.name = 'ielts academic';
                                pieltsDict.description = "";
                                pieltsDict.min = 0;
                                pieltsDict.require = 0;
                                pieltsDict.max = 9;
                                pieltsDict.R = 0;
                                pieltsDict.W = 0;
                                pieltsDict.S = 0;
                                pieltsDict.L = 0;
                                pieltsDict.O = 0;
                                penglishList.push(pieltsDict);
                            }

                            if (penglishList && penglishList.length > 0) {
                                courseAdminReq.english = penglishList;
                            }
                            // else {
                            //     throw new Error("english not found");
                            // }

                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_academic_more_details);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.course_admission_english_more_details);
                            var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var academicReq = "";
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList;
                                        }
                                    }
                                }
                            }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            courseAdminReq.academic = [];
                            for (let academicvalue of academicReq) {
                                courseAdminReq.academic.push(academicvalue.replace(/[\r\n\t ]+/g, ' ').trim());
                            }

                            courseAdminReq.entry_requirements_url = "";
                            courseAdminReq.english_requirements_url = "";
                            courseAdminReq.academic_requirements_url = "";
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }

                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };

                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal------->", JSON.stringify(courseKeyVal))

                            let resScholarshipJson = null;


                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }




                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;

                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }

                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)

                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;


                            break;
                        }
                        case 'application_fee':
                            {
                                let applicationfee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);

                                resJsonData.application_fee = ""
                                break;
                            }
                        case 'course_duration_full_time': {
                            // display full time
                            let f1, f2;
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            let spl;
                            var courseKeyVal = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_full_time);
                            console.log("courseKeyVal@@@@", courseKeyVal);

                            if (!courseKeyVal.includes("null")) {
                                console.log("FSDFSDFFSDF", courseKeyVal)
                                if (courseKeyVal.indexOf("-") > -1) {
                                    f1 = courseKeyVal.replace("-", " ");
                                    console.log("fullTimeText@@@@", f1);
                                }

                                else {
                                    f1 = courseKeyVal;
                                    console.log("in Else@@@@", f1);
                                }
                                if (f1 && f1.length > 0) {
                                    const resFulltime = f1;
                                    if (resFulltime) {
                                        durationFullTime.duration_full_time = resFulltime;
                                        courseDurationList.push(durationFullTime);
                                        console.log("DAta--->", JSON.stringify(resFulltime))
                                        let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                        courseDurationDisplayList.push(tempvar);
                                        demoarray = tempvar[0];
                                        console.log("demoarray--->", demoarray);
                                        console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                        let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                        if (courseDurationList && courseDurationList.length > 0) {
                                            resJsonData.course_duration = resFulltime;
                                        }
                                        if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                            resJsonData.course_duration_display = filtered_duration_formated;
                                            var isfulltime = false, isparttime = false;
                                            filtered_duration_formated.forEach(element => {
                                                if (element.display == "Full-Time") {
                                                    isfulltime = true;
                                                }
                                                if (element.display == "Part-Time") {
                                                    isparttime = true;
                                                }
                                            });
                                            resJsonData.isfulltime = isfulltime;
                                            resJsonData.isparttime = isparttime;
                                        }
                                    }
                                }
                            }
                            else {
                                console.log("PPPPPPPPPPPPP", courseKeyVal)
                                throw new Error("Duration not found");
                            }


                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':
                        // case 'course_campus_location': { // Location Launceston
                        //     let locattt = [];
                        //     var loca;
                        //     //"//*[@class='items-wrapper']/p[4]"
                        //     let newcampus = [];

                        //     var campusedata = [];
                        //     var campusedata1 = [];
                        //     const courseKeyVal = courseScrappedData.course_campus_location;
                        //     console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     locattt = selList;
                        //                     console.log("gsjdgfdbdnbg", JSON.stringify(locattt));
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     //loca// [[{"name":"Sarnia","value":"Sarnia"},{"name":"Toronto","value":"Toronto"}],
                        //     //intake//[{"location":"Sarnia","intake":["SEP"]},{"location":"Toronto","intake":["SEP","JAN","MAY"]}],
                        //     //fees//[{"location":"Sarnia","fees":["$26,254.62"]},{"location":"Toronto","fees":["$28,492.00"]}]]
                        //     let loc2 = []
                        //     let int2 = []
                        //     let fee2 = [];

                        //     locattt[0].forEach(element => {
                        //         loc2.push({
                        //             "name": element.value,
                        //             "code": ""

                        //         })

                        //     })
                        //     console.log("elements", loc2)
                        //     // for (let loc11 of locattt[0]) {
                        //     //     loc2.push(loc11.name);
                        //     //     console.log("llllllllll", loc2)
                        //     // }
                        //     // for (let intake11 of locattt[1]) {
                        //     //     int2.push(intake11.intake)
                        //     //     console.log("iiiiiiiiiii", int2)
                        //     // }
                        //     // for (let fess11 of locattt[2]) {
                        //     //     fee2.push(fess11.fees);
                        //     //     console.log("fffffffffff", fee2)
                        //     // }

                        //     //for (let alldata of loc2) {
                        //     // if (loc2 && loc2.length > -1) {
                        //     //     if (int2 && int2.length > -1) {
                        //     //         if (fee2 && fee2.length > -1) {
                        //     //             // for (let alldata of campusedata1) {
                        //     //             console.log("locaaaaaa", loc2);
                        //     //             console.log("inittttttt", int2);
                        //     //             console.log("feesssssss", fee2);

                        //     //             campusedata1.push({ location: loc2, intake: JSON.stringify(int2), fees: JSON.stringify(fee2) });
                        //     //             console.log("alldata$$$$", campusedata1);

                        //     //         }
                        //     //     }
                        //     // }
                        //     // loc2.forEach(element => {
                        //     //     campusedata1.push({
                        //     //         "name": loc12.name,
                        //     //         "city": "",
                        //     //         "state": "",
                        //     //         "country": "",
                        //     //         "iscurrent": false
                        //     //     })
                        //     //     console.log("bgdgfsdhjfshfg", newcampus)
                        //     // });




                        //     // for (let campus of locattt) {
                        //     //     if (!newcampus.includes(campus)) {
                        //     //         newcampus.push(campus);
                        //     //         console.log("##Campuscampus-->" +JSON.stringify( campus));
                        //     //     }
                        //     // }
                        //     // console.log("##Campus-->" + JSON.stringify(campLocationText))

                        //     // var loca = String(campLocationText).split(',');


                        //     resJsonData.course_study_mode = "On campus";

                        //     resJsonData.course_campus_location = loc2;

                        //     // if (campLocationText && campLocationText.length > 0)
                        //     break;
                        // }



                        case 'course_campus_location': { // Location Launceston
                            let locattt = [];
                            var loca;
                            //"//*[@class='items-wrapper']/p[4]"
                            let newcampus = [];

                            var campusedata = [];
                            var campusedata1 = [];
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            locattt = selList;
                                            console.log("gsjdgfdbdnbg", JSON.stringify(locattt));
                                        }
                                    }
                                }
                            }
                            //loca// [[{"name":"Sarnia","value":"Sarnia"},{"name":"Toronto","value":"Toronto"}],
                            //intake//[{"location":"Sarnia","intake":["SEP"]},{"location":"Toronto","intake":["SEP","JAN","MAY"]}],
                            //fees//[{"location":"Sarnia","fees":["$26,254.62"]},{"location":"Toronto","fees":["$28,492.00"]}]]
                            let loc2 = []
                            let int2 = []
                            let fee2 = [];

                            locattt[0].forEach(element => {
                                loc2.push({
                                    "name": element.value,
                                    "code": ""

                                })

                            })
                            console.log("elements", loc2)
                            // for (let loc11 of locattt[0]) {
                            //     loc2.push(loc11.name);
                            //     console.log("llllllllll", loc2)
                            // }
                            // for (let intake11 of locattt[1]) {
                            //     int2.push(intake11.intake)
                            //     console.log("iiiiiiiiiii", int2)
                            // }
                            // for (let fess11 of locattt[2]) {
                            //     fee2.push(fess11.fees);
                            //     console.log("fffffffffff", fee2)
                            // }

                            //for (let alldata of loc2) {
                            // if (loc2 && loc2.length > -1) {
                            //     if (int2 && int2.length > -1) {
                            //         if (fee2 && fee2.length > -1) {
                            //             // for (let alldata of campusedata1) {
                            //             console.log("locaaaaaa", loc2);
                            //             console.log("inittttttt", int2);
                            //             console.log("feesssssss", fee2);

                            //             campusedata1.push({ location: loc2, intake: JSON.stringify(int2), fees: JSON.stringify(fee2) });
                            //             console.log("alldata$$$$", campusedata1);

                            //         }
                            //     }
                            // }
                            // loc2.forEach(element => {
                            //     campusedata1.push({
                            //         "name": loc12.name,
                            //         "city": "",
                            //         "state": "",
                            //         "country": "",
                            //         "iscurrent": false
                            //     })
                            //     console.log("bgdgfsdhjfshfg", newcampus)
                            // });




                            // for (let campus of locattt) {
                            //     if (!newcampus.includes(campus)) {
                            //         newcampus.push(campus);
                            //         console.log("##Campuscampus-->" +JSON.stringify( campus));
                            //     }
                            // }
                            // console.log("##Campus-->" + JSON.stringify(campLocationText))

                            // var loca = String(campLocationText).split(',');


                            resJsonData.course_study_mode = "On campus";

                            resJsonData.course_campus_location = loc2;

                            // if (campLocationText && campLocationText.length > 0)
                            break;
                        }


                        case 'course_intake': {
                            const courseKeyVal = courseScrappedData.course_intake;
                            var intake_array = [];
                            var intakedata = {}

                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                intake_array = selList;
                                                console.log("intake_array#########", intake_array);

                                            }
                                        }
                                    }
                                }
                            }
                            const intakearrs = []
                            intake_array[1].forEach(element => {
                                var intakedetail = {};
                                intakedetail.name = element.location
                                intakedetail.value = element.value;
                                if (String(intakedetail.value) == []) {
                                    console.log("intakedetail.value==>>", intakedetail.value);
                                    throw new Error("intak value not found");
                                } //else {
                                intakearrs.push(intakedetail)
                                let formatedIntake = format_functions.providemyintake(intakearrs, "mom", "");
                                intakedata.intake = formatedIntake
                                // }

                            });
                            console.log("Campus length  intakedetail-->" + JSON.stringify(intakedata));
                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            //   var intakedata = {};
                            intakedata.more_details = more_details;
                            resJsonData.course_intake = intakedata
                            break;
                        }


                        case 'course_study_mode': { // Location Launceston
                            //resJsonData.course_study_mode = coursestudymodeText;
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':

                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;
                            var fees_array = [];

                            const feesList = [];

                            const courseKeyVal = courseScrappedData.course_tuition_fees_international_student;

                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                fees_array = selList;
                                                console.log("fees_array#########", fees_array);

                                            }
                                        }
                                    }
                                }
                            }
                            const feesarrs = []

                            for (let element of fees_array[2]) {
                                var feesVal = String(element.fees)
                                const feedetails = {};
                                const feesDict = {
                                    international_student: {},
                                };

                                //console.log(funcName + 'feesNumber = ' + feesNumber);
                                if (feesVal) {
                                    let fees_val;
                                    console.log(feesVal.includes('.'))
                                    if (feesVal.includes('.')) {

                                        fees_val = feesVal.split('.')[0]
                                    } else {
                                        fees_val = feesVal
                                    }
                                    const regEx = /\d/g;
                                    let feesValNum = String(fees_val).match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum[2] = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum[2] = ' + feesValNum);


                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);

                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log("feesNumber[2]", feesNumber)
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,

                                                description: feesNumber
                                            });
                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: Number(demoarray.duration),
                                                unit: demoarray.unit,

                                                description: "not available fee"
                                            });
                                        }

                                    }
                                    let international_student_all_fees_array = [];
                                    if (feesDict.international_student) {
                                        const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                        const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                        console.log("cureeeeeeeeee", feesCurrency)
                                        if (feesDuration && feesDuration.length > 0) {
                                            feesDict.fee_duration_years = feesDuration;
                                            //      feesDict.international_student_all_fees = international_student_all_fees_array
                                        }
                                        if (feesCurrency && feesCurrency.length > 0) {
                                            feesDict.currency = feesCurrency;
                                            console.log("Hello --> ", JSON
                                                .stringify(feesDict));
                                        }


                                        if (feesDict) {
                                            feedetails.name = element.location;
                                            feedetails.value = feesDict
                                            console.log("DDDDDDDDDDDD", feesDict)
                                            feesarrs.push(feedetails)
                                        }
                                        console.log("feesarrs----->>>>>>>>>>", feesarrs)
                                        if (feesarrs && feesarrs.length > 0) {
                                            courseTuitionFee.fees = feesarrs;
                                            console.log("courseTuitionFee.fees----->>>>>>>>>>", courseTuitionFee.fees)
                                        }
                                        if (courseTuitionFee) {
                                            resJsonData.course_tuition_fee = courseTuitionFee;
                                            console.log("feedetaidgdfgdgls==>", resJsonData.course_tuition_fee)
                                        }
                                        // take tuition fee value at json top level so will help in DDB for indexing
                                        if (courseTuitionFee && feesDict.international_student) {
                                            resJsonData.course_tuition_fee = courseTuitionFee;
                                            console.log("feedgdfgdfggdggreretails==>", resJsonData.course_tuition_fee)
                                            //resJsonData.course_tuition_fee_amount = feesDict.international_student;
                                        }
                                    }

                                    // feedetails.value=element.fees

                                }

                                //   resJsonData.course_tuition_fee = courseTuitionFee;

                                // console.log("feedetails==>",resJsonData.course_tuition_fee)
                            }







                            break;
                        }

                        case 'program_code': {
                            const courseKeyVal = courseScrappedData.program_code;
                            var program_code = ""
                            console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                program_code = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (program_code.length > 0) {
                                for (let arr of program_code) {

                                    resJsonData.program_code = arr;
                                }
                            }
                            else {
                                resJsonData.program_code = "";
                            }

                            break;

                        }



                        case 'course_scholarship': {
                            const courseKeyVal = courseScrappedData.course_scholarship;
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                resJsonData.course_scholarship = resScholarshipJson;
                            }
                            break;
                        }
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                        case 'course_accomodation_cost': {
                            const courseKeyVal = courseScrappedData.course_accomodation_cost;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_accomodation_cost = resAccomodationCostJson;
                            }
                            break;
                        }
                        // case 'course_cricos_code': {
                        //     const courseKeyVal = courseScrappedData.course_cricos_code;
                        //     const mycodes = [];
                        //     let course_cricos_code = null;
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                     if (Array.isArray(selList) && selList.length > 0) {
                        //                         course_cricos_code = selList;
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }

                        //     if (course_cricos_code) {
                        //         const locations = resJsonData.course_campus_location;
                        //         console.log("locations----->", locations);
                        //         console.log("course_cricos_code----->", course_cricos_code);
                        //         for (let location of locations) {
                        //             mycodes.push({
                        //                 location: location.name, code: course_cricos_code.toString(), iscurrent: false
                        //             });
                        //         }

                        //         resJsonData.course_cricos_code = mycodes;
                        //     } else {
                        //         throw new Error("Campus not found");
                        //     }
                        //     //resJsonData.course_cricos_code = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                        //     break;
                        // }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_overview) {
                                resJsonData.course_overview = String(course_overview);
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome
                            } else {
                                resJsonData.course_career_outcome = [];
                            }


                            break;
                        }

                            break;

                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;

            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                // NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                //      console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;

                var intakes = resJsonData.course_intake.intake;

                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }


                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;

                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);

            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            //custom call


            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }



            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, s.page.url(), courseDict.innerText, courseDict.category);
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
