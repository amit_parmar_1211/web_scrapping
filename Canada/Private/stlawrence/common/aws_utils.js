const fs = require('fs');
const path = require('path');
const AWS = require('aws-sdk');
const appConfigs = require('./app-config');

let s3 = null;
if (appConfigs.AWS_SAM_LOCAL) {
  s3 = new AWS.S3({ apiVersion: '2006-03-01', accessKeyId: process.env.ACCESS_KEY, secretAccessKey: process.env.SECRET_KEY });
} else {
  s3 = new AWS.S3({ apiVersion: '2006-03-01' });
}

async function putFileInBucket(filePath) {
  const funcName = 'putFileInBucket ';
  try {
    if (!(filePath && filePath.length > 0)) {
      console.log(funcName + 'Invalid filePath, filePath = ' + filePath);
      throw (new Error('Invalid filePath, filePath = ' + filePath));
    }
    let fileName = path.basename(filePath);
    console.log(funcName + 'fileName = ' + fileName);
    const fileNameSplitList = String(fileName).split(appConfigs.univNameSeperater);
    if (!(fileNameSplitList && fileNameSplitList.length > 1)) {
      console.log(funcName + 'Failed to split fileName, fileName = ' + fileName);
      throw (new Error('Failed to split fileName, fileName = ' + fileName));
    }
    let univName = fileNameSplitList[0];
    console.log(funcName + 'univName = ' + univName);
    univName = univName.concat('/');
    fileName = univName.concat(fileName);
    console.log(funcName + 'fileName with folder path = ' + fileName);
    const params = {
      Bucket: appConfigs.awsUniversitiesDataBucketName,
      Key: '',
      Body: '',
    };
    const readStream = await fs.createReadStream(filePath);
    params.Body = readStream;
    params.Key = fileName;
    const data = await s3.upload(params).promise();
    if (data === null) {
      console.log(funcName + 'Failed to put an object in bucket');
      throw (new Error('Failed to put an object in bucket'));
    }
    return data.Location;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}
async function putFailedItemsListInBucket(univId, failedItemsList, apiOperationType) {
  const funcName = 'putFailedItemsListInBucket ';
  try {
    if (!(univId && univId.length > 0 && failedItemsList && failedItemsList.length > 0)) {
      console.log(funcName + 'Invalid univId or failedItemsList, univId = ' + univId + ' failedItemsList = ' + JSON.stringify(failedItemsList));
      throw (new Error('Invalid univId or failedItemsList, univId = ' + univId + ', failedItemsList = ' + JSON.stringify(failedItemsList)));
    }
    const dateTime = new Date();
    const date = dateTime.getUTCDate() + '_' + (dateTime.getMonth() + 1) + '_' + dateTime.getUTCFullYear();
    console.log(funcName + 'UTC date = ' + date);
    const time = dateTime.getUTCHours() + ':' + dateTime.getUTCMinutes() + ':' + dateTime.getUTCSeconds() + ':' + dateTime.getUTCMilliseconds();
    console.log(funcName + 'UTC time = ' + time);
    let fileDateTime = null;
    if (apiOperationType && apiOperationType.length > 0) {
      fileDateTime = String(apiOperationType).concat('_').concat(date).concat('_')
        .concat(time)
        .concat('.json');
    } else {
      fileDateTime = String(date).concat('_').concat(time).concat('.json');
    }
    console.log(funcName + 'fileDateTime = ' + fileDateTime);
    const univName = univId.concat('/');
    const failedItemsFolderPath = univName.concat(appConfigs.failedItemsFolderName).concat('/');
    const fileNameWithFullPath = failedItemsFolderPath.concat(fileDateTime);
    console.log(funcName + 'fileName with folder path, fileNameWithFullPath= ' + fileNameWithFullPath);
    const params = {
      Bucket: appConfigs.awsUniversitiesDataBucketName,
      Key: '',
      Body: '',
    };
    const buf = Buffer.from(JSON.stringify(failedItemsList));
    params.Body = buf;
    params.Key = fileNameWithFullPath;
    const data = await s3.upload(params).promise();
    if (data === null) {
      console.log(funcName + 'Failed to put an object in bucket');
      throw (new Error('Failed to put an object in bucket'));
    }
    return data.Location;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}
async function getObjectFromBucket(objKey) {
  const funcName = 'getObjectFromBucket ';
  try {
    if (!(objKey && objKey.length > 0)) {
      console.log(funcName + 'Invalid objKey, objKey = ' + objKey);
      throw (new Error('Invalid objKey, objKey = ' + objKey));
    }
    const params = {
      Bucket: appConfigs.awsUniversitiesDataBucketName,
      Key: objKey,
    };
    const data = await s3.getObject(params).promise();
    if (data === null) {
      console.log(funcName + 'Failed to get specified object from bucket, objKey = ' + objKey);
      throw (new Error('Failed to get specified object from bucket, objKey = ' + objKey));
    }
    return data.Body;
  } catch (error) {
    console.log(funcName + 'try-catch error = ' + error);
    throw (error);
  }
}
module.exports = { putFileInBucket, getObjectFromBucket, putFailedItemsListInBucket };

