const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const configs = require('./configs');
class ScrapeCourseList extends Scrape {
  async scrapeCourseListAndPutAtS3() {
    const funcName = 'startScrappingFunc ';
    var s = null;
    try {
      s = new Scrape();
      await s.init({ headless: false });
      var datalist = [];
      await s.setupNewBrowserPage("https://www.stlawrencecollege.ca/");
      var URLS = ["https://www.stlawrencecollege.ca/programs-and-courses/full-time/program-list/"]
      var maincategory = [];
      let subjectAreasArray = [];
      for (let url of URLS) {
        await s.page.goto(url, { timeout: 0 });
        let catName = await s.page.$x("//*[@id='pagecontent_1_ddlCategory']/option[not(contains(text(),'ALL')) and not(contains(text(),'Category'))]");
        for (let i = 0; i < catName.length; i++) {
          let cumpus_Name22 = await s.page.evaluate(el => el.innerText, catName[i]);
          let campus_val22 = await s.page.evaluate(el => el.value, catName[i]);
          subjectAreasArray.push({
            name: cumpus_Name22,
            value: campus_val22,
          });
        }
        console.log("subjectAreasArray -->", subjectAreasArray);
        for (let subCat of subjectAreasArray) {
          console.log("subCat.value -->", subCat.value);
          try {
            await s.page.select("#pagecontent_1_ddlCategory", subCat.value);
          } catch (e) {
          }
          await s.page.waitFor(5000)
          const maincategoryselector = "//*[@id='pageContent']/div/a[not(contains(@href,'online'))]";
          var category = await s.page.$x(maincategoryselector);
          console.log("Total categories-->" + category.length);
          for (let catc of category) {
            var categorys = await s.page.evaluate(el => el.innerText, catc)
            var link = await s.page.evaluate(el => el.href, catc)
            maincategory.push({ href: link, innerText: categorys, category: subCat.name })
          }

        }
      }
      fs.writeFileSync("./output/stlawrence_original_courselist.json", JSON.stringify(maincategory))
      let uniqueUrl = [];
      for (let i = 0; i < maincategory.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (maincategory[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: maincategory[i].href, innerText: maincategory[i].innerText, category: [] });
          }
        } else {
          uniqueUrl.push({ href: maincategory[i].href, innerText: maincategory[i].innerText, category: [] });
        }
      }
      await fs.writeFileSync("./output/stlawrence_unique_courselist.json", JSON.stringify(uniqueUrl));
      for (let i = 0; i < maincategory.length; i++) {
        for (let j = 0; j < uniqueUrl.length; j++) {
          if (uniqueUrl[j].href == maincategory[i].href) {
            uniqueUrl[j].category.push(maincategory[i].category);
          }
        }
      }
      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/stlawrence_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      Scrape.validateParams([selFilepath]);
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        return dataJson;
      }
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      s = new Scrape();
      await s.init({ headless: true });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }

    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      Scrape.validateParams([selFilepath]);
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));
      s = new Scrape();
      await s.init({ headless: true });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        count += 1;
      }
      console.log(funcName + 'writing courseList to file....');
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
}
module.exports = { ScrapeCourseList };




//       let uniqueUrl = [];
//       //unique url from the courselist file
//       for (let i = 0; i < maincategory.length; i++) {
//         let cnt = 0;
//         if (uniqueUrl.length > 0) {
//           for (let j = 0; j < uniqueUrl.length; j++) {
//             if (maincategory[i].href == uniqueUrl[j].href) {
//               cnt = 0;
//               break;
//             } else {
//               cnt++;
//             }
//           }
//           if (cnt > 0) {
//             uniqueUrl.push({ href: maincategory[i].href, innerText: maincategory[i].innerText });
//           }
//         } else {
//           uniqueUrl.push({ href: maincategory[i].href, innerText: maincategory[i].innerText });
//         }
//       }
//       //   

//       await fs.writeFileSync("./output/stlawrence_unique_courselist.json", JSON.stringify(uniqueUrl));

//       //based on unique urls mapping of categories
//       for (let i = 0; i < maincategory.length; i++) {
//         for (let j = 0; j < uniqueUrl.length; j++) {
//           if (uniqueUrl[j].href == maincategory[i].href) {
//             if (uniqueUrl[j].category.includes(maincategory[i].category)) {

//             } else {
//               uniqueUrl[j].category.push(maincategory[i].category);
//             }

//           }
//         }
//       }
//       console.log("totalCourseList -->", uniqueUrl);
//       await fs.writeFileSync("./output/stlawrence_courselist.json", JSON.stringify(uniqueUrl));
//       console.log(funcName + 'writing courseList to file completed successfully....');
//       await s.browser.close();
//       console.log(funcName + 'browser closed successfully.....');
//       return true;
//     } catch (error) {
//       console.log(funcName + 'try-catch error = ' + error);
//       throw (error);
//     }
//   }
// }
// module.exports = { ScrapeCourseList };