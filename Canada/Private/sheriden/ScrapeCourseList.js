const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const puppeteer = require('puppeteer');
const utils = require('./common/utils');
const configs = require('./configs');
const awsUtil = require('./common/aws_utils');
const appConfigs = require('./common/app-config');

class ScrapeCourseList extends Scrape {
  // refers to http://www.utas.edu.au/international/studying/course-search-tools/list-all-courses
  async scrapeOnlyInternationalCourseList_Mapping(page) {
    const funcName = 'scrapeCourseCategoryList ';
    try {
      // await page.waitFor(5000);

      var datalist = [];
      let s;
      s = new Scrape();
      await s.init({ headless: false });
      await s.setupNewBrowserPage("https://academics.sheridancollege.ca/programs/alpha?s=", { timeout: 0 });

      var mainCategory = [], redirecturl = [];
      var postGradCourseList = [];
      const maincategoryselectorurl = "//*[@class='program-list']/div/div/ul/li/a";
      var category = await s.page.$x(maincategoryselectorurl);
      for (let i = 0; i < category.length; i++) {
        var categorystringmain = await s.page.evaluate(el => el.innerText, category[i]);
        console.log("categorystringmain -->", await s.page.evaluate(el => el.innerText, category[i]));
        var categorystringmainurl = await s.page.evaluate(el => el.href, category[i]);
        redirecturl.push({ innerText: categorystringmain, href: categorystringmainurl });

      }
      console.log("redirecturl", JSON.stringify(redirecturl));
      await fs.writeFileSync("./output/sheridencollege_original_courselist.json", JSON.stringify(redirecturl)); let uniqueUrl = [];
      for (let i = 0; i < redirecturl.length; i++) {
        let cnt = 0;
        if (uniqueUrl.length > 0) {
          for (let j = 0; j < uniqueUrl.length; j++) {
            if (redirecturl[i].href == uniqueUrl[j].href) {
              cnt = 0;
              break;
            } else {
              cnt++;
            }
          }
          if (cnt > 0) {
            uniqueUrl.push({ href: redirecturl[i].href, innerText: redirecturl[i].innerText });
          }
        } else {
          uniqueUrl.push({ href: redirecturl[i].href, innerText: redirecturl[i].innerText });
        }
      }
      await fs.writeFileSync("./output/sheridencollege_unique_courselist.json", JSON.stringify(uniqueUrl));

      console.log("totalCourseList -->", uniqueUrl);
      await fs.writeFileSync("./output/sheridencollege_courselist.json", JSON.stringify(uniqueUrl));
      console.log(funcName + 'writing courseList to file completed successfully....');
      let postgradurlselectors = []
      let postgradurlselector = {
        studylevel: "//*[@class='plan-item']/li/div[1]/div/h4",
        duration_time: "//*[@class='plan-item']/li/div[1]/div/ul/li[contains(text(),'Full-time')]",
        duration: "//*[@class='plan-item']/li/div[1]/div/ul/li[contains(text(),'Full-time')]/following::li[contains(text(),'yrs') or contains(text(),'yr') or contains(text(),'year')]",
        program_code: "//*[@class='plan-item']/li/div[1]/div/ul/li[contains(text(),'Full-time')]/following::li[contains(text(),'Program code')]",
        name: "//*[@id='main']/article/header[1]/div/div/div/div[1]/h1",
        check: "//*/table/tbody/tr/td[contains(.,'Available')]"

      }
      for (let course of uniqueUrl) {
        //await page.goto(course.href,{timeout:0})
        let s1 = null;
        s1 = new Scrape();
        await s1.init({ headless: false });
        await s1.setupNewBrowserPage(course.href);
        let postgradName_H, postgradDuration_time, postgradDuration, postgradIntake, postgradlocation, postgradstudylevel, postgradpcode, checked;
        postgradName_H = await s1.page.$x(postgradurlselector.name);
        postgradDuration = await s1.page.$x(postgradurlselector.duration);
        postgradDuration_time = await s1.page.$x(postgradurlselector.duration_time);
        postgradIntake = await s1.page.$x(postgradurlselector.intake);
        postgradstudylevel = await s1.page.$x(postgradurlselector.studylevel);
        postgradpcode = await s1.page.$x(postgradurlselector.program_code);
        checked = await s1.page.$x(postgradurlselector.check)
        console.log("yesssss_in", course.href)

        for (let i = 0; i < postgradDuration_time.length; i++) {
          let name, duration, duration_time, intake, programcode, study_levels, location, checks;
          let nintake = []
          let nlocation = []
          //console.log("duration==>",  postgradDuration[i])
          name = await s1.page.evaluate(el => el.innerText, postgradName_H[0]);
          if (checked[i]) {
            checks = await s1.page.evaluate(el => el.innerText, checked[i])
          }

          console.log("checks", checks)
          if (checks != undefined) {
            if (postgradDuration[i] && postgradDuration_time[i]) {
              duration = await s1.page.evaluate(el => el.innerText, postgradDuration[i]);
              duration_time = await s1.page.evaluate(el => el.innerText, postgradDuration_time[i]);
              console.log("duration_Q", duration)
            } else {
              duration_time = await s1.page.evaluate(el => el.innerText, postgradDuration_time[i]);
              console.log("yesssss", await s1.page.evaluate(el => el.innerText, postgradpcode[i]))
              console.log("yesssss", await s1.page.evaluate(el => el.innerText, postgradstudylevel[i]))
            }
            programcode = await s1.page.evaluate(el => el.innerText, postgradpcode[i]);
            study_levels = await s1.page.evaluate(el => el.innerText, postgradstudylevel[i]);
            console.log("duration_time", duration_time, "  programcode", programcode, "  study_levels", study_levels);
            let dynamicsel_intake = "//*[@class='plan-item']/li/div[1]/div/ul/li[contains(text(),'Full-time')]/following::li[contains(.,'" + programcode + "')]/.././following::div[1]/figure/table/tbody/tr/td[contains(.,'Available')]/preceding::td[2]";
            let dynamic_intakes = "//*[@class='plan-item']/li/div[1]/div/ul/li[contains(text(),'Full-time')]/following::li[contains(.,'" + duration + "')" + "and following::li[contains(.,'" + programcode + "')]]/.././following::div[1]/figure/table/tbody/tr/td[contains(.,'Available')]/preceding::td[2]"
            let dynamicsel_location = "//*[@class='plan-item']/li/div[1]/div/ul/li[contains(text(),'Full-time')]/following::li[contains(.,'" + programcode + "')]/.././following::div[1]/figure/table/tbody/tr/td[contains(.,'Available')]/preceding::td[1]"
            let dynamic_locations = "//*[@class='plan-item']/li/div[1]/div/ul/li[contains(text(),'Full-time')]/following::li[contains(.,'" + duration + "')" + "and following::li[contains(.,'" + programcode + "')]]/.././following::div[1]/figure/table/tbody/tr/td[contains(.,'Available')]/preceding::td[1]"
            postgradIntake = await s1.page.$x(dynamic_intakes);
            postgradlocation = await s1.page.$x(dynamic_locations);
            if (duration) {
              console.log("YEsssssss_inif")
              // if (dynamicsel_intake.length > 0 || dynamic_intakes.length > 0 || dynamicsel_location.length > 0 || dynamic_locations.length > 0) {
              if (postgradlocation && postgradlocation.length > 0) {
                console.log('postgradIntake.length -->', postgradIntake.length);
                for (let j = 0; j < postgradIntake.length; j++) {
                  intake = await s1.page.evaluate(el => el.innerText, postgradIntake[j])
                  nintake.push(intake)
                  location = await s1.page.evaluate(el => el.innerText, postgradlocation[j])

                  nlocation.push(location)
                }
                console.log("Location in If===>", nlocation);
              }

            }
            else {
              console.log("inelseeee")
              let cnt;
              let postgradIntakes = await s1.page.$x(dynamicsel_intake);
              let postgradlocations = await s1.page.$x(dynamicsel_location);
              if (postgradIntakes.length == 1) {
                cnt = 1
              } else {
                cnt = 2
              }
              for (let j = 0; j < cnt; j++) {
                intake = await s1.page.evaluate(el => el.innerText, postgradIntakes[j])
                nintake.push(intake);
                location = await s1.page.evaluate(el => el.innerText, postgradlocations[j])
                // if(location.includes('\n')){

                // }
                nlocation.push(location);
              }
            }
            console.log(duration)
            if (duration) {
              duration = duration.trim()
            } else {
              duration = "";
            }
            postgradurlselectors.push({
              key: course.href,
              name: course.innerText + "" + duration,
              duration_time: duration_time.trim(),
              duration: duration,
              programcode: programcode.trim(),
              intake: nintake,
              study_levels: study_levels.trim(),
              location: [String(nlocation).replace('\n', '').trim()],
            });
          } else {
            console.log(" not internation")
          }
        }
        if (s1) {
          await s1.close();
        }
        console.log('postGradCourseList = ' + JSON.stringify(postgradurlselectors));
        await fs.writeFileSync("./output/sheridencollege_postgraduate_courselist.json", JSON.stringify(postgradurlselectors));
      }
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
    }
  }
  async scrapeOnlyInternationalCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // if should read from local file
      if (appConfigs.shouldTakeCourseListFromOutputFolder) {
        const fileData = fs.readFileSync(configs.opCourseListFilepath);
        if (!fileData) {
          throw (new Error('Invalif file data, fileData = ' + fileData));
        }
        const dataJson = JSON.parse(fileData);
        console.log(funcName + 'Success in getting local data so returning local data....');
        // console.log(funcName + 'dataJson = ' + JSON.stringify(dataJson));
        return dataJson;
      }
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_list_selector', this.selectorJson);
      console.log(funcName + 'rootEleDictUrl = ' + JSON.stringify(rootEleDictUrl));
      // set page to url
      await s.setupNewBrowserPage(rootEleDictUrl);
      await this.scrapeOnlyInternationalCourseList_Mapping(s.page);
      if (s) {
        await s.close();
      }
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
  // refers to https://programsandcourses.anu.edu.au/catalogue
  async scrapeCourseList(selFilepath) {
    const funcName = 'scrapeCoursePageList ';
    let s = null;
    try {
      // validate params
      Scrape.validateParams([selFilepath]);
      // load file
      const fileData = fs.readFileSync(selFilepath);
      this.selectorJson = JSON.parse(fileData);
      // course_list selector
      const selectorsList = await utils.getSelectorsListForKey('course_category_selector', this.selectorJson);
      if (!selectorsList) {
        console.log(funcName + 'Invalid selectorsList');
        throw (new Error('Invalid selectorsList'));
      }
      const sel = selectorsList[0];
      console.log(funcName + 'sel = ' + JSON.stringify(sel));

      // create Scrape object
      s = new Scrape();
      await s.init({ headless: false });
      const elementDict = await utils.getElementDictForKey('course_category_selector', this.selectorJson);
      if (!elementDict) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }
      const rootEleDictUrl = await utils.getRooElementDictUrl('course_category_selector', this.selectorJson);
      if (!rootEleDictUrl) {
        console.log(funcName + 'Invalid rootEleDictUrl');
        throw (new Error('Invalid rootEleDictUrl'));
      }
      const courseCatDictList = await s.scrapeElement(elementDict.elementType, sel, rootEleDictUrl, null);
      console.log(funcName + 'courseCatDictList = ' + JSON.stringify(courseCatDictList));

      // course_level_selector key
      const coursLvlSelList = await utils.getSelectorsListForKey('course_list_selector', this.selectorJson);
      if (!coursLvlSelList) {
        console.log(funcName + 'Invalid coursLvlSelList');
        throw (new Error('Invalid coursLvlSelList'));
      }
      const elementDictForCorsLvlKey = await utils.getElementDictForKey('course_list_selector', this.selectorJson);
      if (!elementDictForCorsLvlKey) {
        console.log(funcName + 'Invalid elementDict');
        throw (new Error('Invalid elementDict'));
      }

      // for each course-category, scrape all courses
      let totalCourseList = []; let count = 1;
      for (const catDict of courseCatDictList) {
        console.log(funcName + 'count = ' + count);
        // click on href
        console.log(funcName + 'catDict.href = ' + catDict.href);
        await s.setupNewBrowserPage(catDict.href);
        const selItem = coursLvlSelList[0];
        console.log(funcName + 'selItem = ' + selItem);
        const res = await s.scrapeElement(elementDictForCorsLvlKey.elementType, selItem, catDict.href, null);
        console.log(funcName + 'res = ' + JSON.stringify(res));
        if (Array.isArray(res)) {
          totalCourseList = totalCourseList.concat(res);
        } else {
          throw (new Error('res is not array, it must be...'));
        }
        count += 1;
      } // for (const catDict of courseDictList)
      console.log(funcName + 'writing courseList to file....');
      // console.log(funcName + 'totalCourseList = ' + JSON.stringify(totalCourseList));
      await fs.writeFileSync(configs.opCourseListFilepath, JSON.stringify(totalCourseList));
      console.log(funcName + 'writing courseList to file completed successfully....');
      // put file in S3 bucket
      const res = await awsUtil.putFileInBucket(configs.opCourseListFilepath);
      console.log(funcName + 'object location = ' + res);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      return null;
    } catch (error) {
      console.log(funcName + 'try-catch error = ' + error);
      // destroy scrape resources
      if (s) {
        await s.close();
      }
      throw (error);
    }
  }
} // class
module.exports = { ScrapeCourseList };