const fs = require('fs');
const Scrape = require('./common/scrape').Scrape;
const Course = require('./common/course').Course;
const utils = require('./common/utils');
const appConfigs = require('./common/app-config');
const configs = require('./configs');
const format_functions = require('./common/format_functions');
request = require('request');
const postgradCourseList = JSON.parse(fs.readFileSync(appConfigs.sheriden_postgraduate_courselist));

class ScrapeCourse extends Course {
    // format courseScrappedData, format output json
    static async formatOutput(courseScrappedData, course_name, courseUrl) {
        const funcName = 'formatOutput ';
        try {
            Scrape.validateParams([courseScrappedData]);
            console.log("course_name", course_name)
            const postGradIndex = postgradCourseList.findIndex(value => value.name == course_name);
            console.log("postGradIndex===>>>", postGradIndex);
            const resJsonData = {};
            var demoarray = [];
            for (const key in courseScrappedData) {
                console.log('\n\r' + funcName + 'key = ' + key);
                if (courseScrappedData.hasOwnProperty(key)) {
                    switch (key) {
                        case 'univ_id': {
                            console.log(funcName + 'configs.univ_id = ' + configs.univ_id);
                            if (!configs.univ_id && configs.univ_id.length > 0) {
                                console.log(funcName + 'configs.univ_id must have valid value');
                                throw (new Error('configs.univ_id must have valid value'));
                            }
                            resJsonData.univ_id = configs.univ_id;
                            break;
                        }
                        case 'course_discipline': {
                            let category = await Course.extractValueFromScrappedElement(courseScrappedData.course_discipline)
                            resJsonData.course_discipline = [category];
                            // resJsonData.course_discipline = course_category;
                            // As of not we do not have way to get this field value
                            break;
                        }
                        // case 'course_title': {
                        //     let t1 = await Course.extractValueFromScrappedElement(courseScrappedData.course_title);

                        //     console.log("resJsonData.course_title3333", t1);
                        //     resJsonData.course_title = t1;

                        // }
                        case 'course_title': {
                            let course_title = await Course.extractValueFromScrappedElement(courseScrappedData.course_title)
                            let course_title_text = format_functions.titleCase(String(course_title.replace('-', ' ')).trim());;
                            resJsonData.course_title = course_title_text;
                            break;
                        }
                        case 'course_study_level': {
                            let study_l;
                            let arrrr = []
                            let academicReq = [];
                            //  const cTitle = await Course.extractValueFromScrappedElement(courseScrappedData.course_study_level);

                            let studylevels = postgradCourseList[postGradIndex].study_levels
                            let duration = postgradCourseList[postGradIndex].duration;
                            console.log("studylevels", studylevels)
                            console.log("==>split_value@@@", studylevels.split("Ontario College")[1])
                            if (studylevels.includes("Ontario College")) {
                                study_l = studylevels.split("Ontario College")[1];
                                if (duration.includes("Co-op")) {
                                    resJsonData.course_study_level = study_l.trim() + " Co-op"
                                } else {
                                    resJsonData.course_study_level = study_l.trim()
                                }
                            }
                            else {

                                console.log("asgdsdcaf", academicReq)
                                resJsonData.course_study_level = ""
                            }

                        }

                        case 'ielts_req':
                        case 'course_admission_academic_more_details':
                        case 'course_academic_requirement':
                        case 'course_toefl_ielts_score': { // "course_admission_requirement": { "english": [{ "ielts": ""},{ "ibt": "" },{ "pte": "" }],"academic": [""]}
                            const courseAdminReq = {};
                            const englishList = [];
                            const ieltsDict = {};
                            const ibtDict = {};
                            const ppbtDict = {};
                            const ppteDict = {};
                            const caeDict = {};
                            let othercourses = [];
                            let othercourse;
                            let ielts, toeflibt, pte, pbt, cae;

                            const cStudyLevels = resJsonData.course_study_level
                            console.log("StudyLevel!!!" + JSON.stringify(cStudyLevels));


                            if (cStudyLevels) {
                                othercourse = await utils.getValueFromHardCodedJsonFile('ielts_mapping');
                                //console.log("OtherCourses-->",JSON.stringify(othercourse))
                                othercourse.forEach(element => {
                                    console.log(element)
                                    if (element.key.includes(cStudyLevels)) {
                                        console.log("Element->", element)
                                        othercourses = element

                                    }
                                })
                                ielts = othercourses["ielts"]
                                pte = othercourses["pte"]
                                pbt = othercourses["pbt"]
                                toeflibt = othercourses["toeflibt"]
                                cae = othercourses["cae"]
                                console.log("ielts@@@@", ielts)
                                // console.log("ibt@@@@", ibt)

                            }

                            //   if (potherLngDict) {
                            var ieltsScore = "", ibtScore = "", pteScore = "", caeScore = "", toeflScore = "", pbtScore = "";
                            if (ielts) {
                                ieltsScore = await utils.giveMeNumber(ielts);
                                console.log("### IELTS data-->" + ieltsScore);
                            }
                            if (pte) {
                                pteScore = await utils.giveMeNumber(pte);
                            }
                            if (pbt) {
                                pbtScore = await utils.giveMeNumber(pbt);
                            }
                            if (toeflibt) {
                                toeflScore = await utils.giveMeNumber(toeflibt);
                            }
                            if (cae) {
                                caeScore = await utils.giveMeNumber(cae);
                            }
                            // if (pbt) {
                            //     pbtScore = await utils.giveMeNumber(pbt);
                            //     console.log("### IELTS datas-->" + pbtScore);
                            // }
                            if (ieltsScore != "NA") {
                                ieltsDict.name = 'ielts academic';
                                ieltsDict.description = ielts;
                                ieltsDict.min = 0;
                                ieltsDict.require = ieltsScore;
                                ieltsDict.max = 9;
                                ieltsDict.R = 0;
                                ieltsDict.W = 0;
                                ieltsDict.S = 0;
                                ieltsDict.L = 0;
                                ieltsDict.O = 0;
                                englishList.push(ieltsDict);
                            }
                            if (pteScore != "NA") {
                                ppteDict.name = 'pte academic';
                                ppteDict.description = pte
                                ppteDict.min = 0;
                                ppteDict.require = pteScore;
                                ppteDict.max = 90;
                                ppteDict.W = 0;
                                ppteDict.S = 0;
                                ppteDict.L = 0;
                                ppteDict.O = 0;
                                englishList.push(ppteDict);
                            }
                            if (pbtScore) {
                                ppbtDict.name = 'toefl pbt';
                                ppbtDict.description = pbt
                                ppbtDict.min = 310;
                                ppbtDict.require = pbtScore;
                                ppbtDict.max = 677;
                                ppbtDict.W = 0;
                                ppbtDict.S = 0;
                                ppbtDict.L = 0;
                                ppbtDict.O = 0;
                                englishList.push(ppbtDict);
                            }
                            if (toeflScore) {
                                ibtDict.name = 'toefl ibt';
                                ibtDict.description = toeflibt
                                ibtDict.min = 0;
                                ibtDict.require = toeflScore;
                                ibtDict.max = 120;
                                ibtDict.R = 0;
                                ibtDict.W = 0;
                                ibtDict.S = 0;
                                ibtDict.L = 0;
                                ibtDict.O = 0;
                                englishList.push(ibtDict);
                            }

                            if (caeScore) {
                                caeDict.name = 'cea';
                                caeDict.description = cae;
                                caeDict.min = 80;
                                caeDict.require = caeScore;
                                caeDict.max = 230;
                                caeDict.R = 0;
                                caeDict.W = 0;
                                caeDict.S = 0;
                                caeDict.L = 0;
                                caeDict.O = 0;
                                englishList.push(caeDict);
                            }
                            // if (pbtScore != "NA") {
                            //     ppbtDict.name = 'toefl pbt';
                            //     ppbtDict.description = pbt
                            //     ppbtDict.min = 310;
                            //     ppbtDict.require = pbtScore;
                            //     ppbtDict.max = 677;
                            //     ppbtDict.W = 0;
                            //     ppbtDict.S = 0;
                            //     ppbtDict.L = 0;
                            //     ppbtDict.O = 0;
                            //     englishList.push(ppbtDict);
                            // }


                            if (englishList && englishList.length > 0) {
                                courseAdminReq.english = englishList;
                            }

                            var academic_more = await Course.extractValueFromScrappedElement(courseScrappedData.academic_requirements_url);
                            var english_more = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var entry_requirements_url = await Course.extractValueFromScrappedElement(courseScrappedData.entry_requirements_url);
                            var academicReq = null;
                            const courseKeyVal = courseScrappedData.course_academic_requirement;
                            console.log("Academic_req-->" + JSON.stringify(courseKeyVal))
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            academicReq = selList
                                        }
                                    }
                                }
                            }
                            courseAdminReq.academic = (academicReq) ? [academicReq.toString()] : [];
                            courseAdminReq.english_requirements_url = english_more;
                            courseAdminReq.academic_requirements_url = academic_more;
                            courseAdminReq.entry_requirements_url = entry_requirements_url;
                            resJsonData.course_admission_requirement = courseAdminReq;
                            break;
                        }
                        case 'course_url': {
                            let newUrl = courseScrappedData.course_url;
                            let rescourse_url = null;
                            if (Array.isArray(newUrl)) {
                                for (const rootEleDict of newUrl) {
                                    console.log(funcName + '\n\r rootEleDict courseurl= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList courseurl= ' + JSON.stringify(selList));
                                            rescourse_url = selList;
                                        }
                                    }
                                }
                            }
                            if (rescourse_url) {
                                resJsonData.course_url = rescourse_url;
                            }
                            break;
                        }
                        case 'course_duration_part_time': {
                            // display full time
                            let f1, f2;
                            var courseDurationDisplayList = [], durationFullTime = {}, courseDurationList = [];
                            var fullTimeText = "";
                            let spl;
                            //let coursepartime = await Course.extractValueFromScrappedElement(courseScrappedData.course_duration_part_time);
                            // "//*[@class='plan-item']/li/div/div/ul/li[contains(text(),'Part-Time')]",


                            let coursepartime = courseScrappedData.course_duration_part_time;
                            let rescourse_url = null;
                            let replace
                            let all_suration_value = []

                            if (Array.isArray(coursepartime)) {
                                for (const rootEleDict of coursepartime) {
                                    console.log(funcName + '\n\r rootEleDict coursepartime = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict coursepartime = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList coursepartime = ' + JSON.stringify(selList));
                                            rescourse_url = selList;

                                            if (String(all_suration_value).includes("yrs")) {
                                                rescourse_url = String(all_suration_value).replace('yrs', 'years').replace(',', ' , ').trim();
                                                console.log("rescourse_url---->>", rescourse_url)
                                            }
                                            if (rescourse_url) {
                                                spl = String(rescourse_url) + " " + "and equivalent parttime"
                                                console.log("spl--->>", spl);
                                            }

                                            // if (String(spl).includes('Program code:')) {
                                            //     for (let split_duration of spl) {
                                            //         replace = String(split_duration).split(',Program code:')[0]
                                            //         all_suration_value.push(replace)
                                            //         console.log("rescourse_url==>", all_suration_value)
                                            //     }
                                            // }
                                        }
                                    }
                                }
                            }
                            console.log("coursepartime==>", replace)

                            if (postGradIndex > -1) {
                                let duration = postgradCourseList[postGradIndex].duration;
                                if (duration.includes("(")) {
                                    duration = duration.split("(")[0];
                                }
                                if (duration.includes("yrs")) {
                                    duration = duration.replace('yrs', 'years')
                                }
                                let duration_time = postgradCourseList[postGradIndex].duration_time
                                spl = duration + " " + duration_time;
                            }
                            if (coursepartime) {
                                spl = spl + " " + "and equivalent parttime"
                            }



                            console.log("location_spl==>", spl)
                            if (spl && spl.length > 0) {
                                const resFulltime = spl;
                                if (resFulltime) {
                                    durationFullTime.duration_full_time = resFulltime;
                                    courseDurationList.push(durationFullTime);
                                    console.log("DAta--->", JSON.stringify(resFulltime))
                                    let tempvar = await format_functions.validate_course_duration_full_time(resFulltime)
                                    courseDurationDisplayList.push(tempvar);
                                    demoarray = tempvar[0];
                                    console.log("demoarray--->", demoarray);
                                    console.log("##CourseDurationDisplay--->" + JSON.stringify(courseDurationDisplayList));
                                    let filtered_duration_formated = await format_functions.getfilterduration(courseDurationDisplayList[0]);
                                    if (courseDurationList && courseDurationList.length > 0) {
                                        resJsonData.course_duration = resFulltime;
                                    }
                                    if (filtered_duration_formated && filtered_duration_formated.length > 0) {
                                        resJsonData.course_duration_display = filtered_duration_formated;
                                        var isfulltime = false, isparttime = false;
                                        filtered_duration_formated.forEach(element => {
                                            if (element.display == "Full-Time") {
                                                isfulltime = true;
                                            }
                                            if (element.display == "Part-Time") {
                                                isparttime = true;
                                            }
                                        });
                                        resJsonData.isfulltime = isfulltime;
                                        resJsonData.isparttime = isparttime;
                                    }
                                }
                            }
                            break;
                        }
                        case 'course_tuition_fee':
                        case 'page_url':
                        case 'select_english_as_ielts':
                        case 'select_english_as_ibt':
                        case 'course_toefl_ibt_indicator':
                        case 'select_english_as_pbt':
                        case 'course_toefl_toefl_pbt_score':
                        case 'course_duration':
                        case 'course_tuition_fee_duration_years':
                        case 'course_tuition_fees_currency':
                        case 'course_tuition_fees_year':
                        case 'course_admission_requirement':

                        case 'course_campus_location': { // Location Launceston
                            var campLocationText;
                            var loc2;
                            const courseKeyVal = courseScrappedData.course_campus_location;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));

                                            if (String(selList).includes('\n')) {
                                                let loc_val = String(selList).split('\n')
                                                let locat = String(loc_val).split(',')
                                                campLocationText = locat;
                                                console.log("##cojdgjdg", campLocationText);
                                            }
                                        }
                                    }
                                }
                            }
                            if (postGradIndex > -1) {
                                let location_post = postgradCourseList[postGradIndex].location;
                                console.log("location_post----------->", location_post);
                                if (String(location_post).includes('\n')) {
                                    let loc_val = String(location_post).split('\n')
                                    let locat = String(loc_val).split(',')
                                    campLocationText = locat;
                                    console.log("##cojdgjdg", campLocationText);
                                }
                            }
                            let newcampus = [];
                            for (let campus of campLocationText) {
                                if (!newcampus.includes(campus)) {
                                    if (!String(campus) == '') {
                                        console.log("##Campus-->>" + campus)
                                        newcampus.push(campus);
                                    }

                                    console.log("##Campuscampus-->" + campus)
                                }
                            }

                            var loca = String(newcampus).split(',');
                            console.log("----------->", loca);


                            if (loca && loca.length > 0) {
                                var campLocationValTrimmed = String(courseScrappedData.course_campus_location).trim();
                                resJsonData.course_study_mode = "On campus";
                                console.log("##campLocationValTrimmed-->", JSON.stringify(campLocationValTrimmed));
                                var campusedata = [];
                                loca.forEach(element => {
                                    campusedata.push({
                                        "name": element.trim(),
                                        "code": ""
                                    })
                                });
                                resJsonData.course_campus_location = campusedata;
                                var study_mode = [];
                                if (campLocationValTrimmed.trim().length > 0) {
                                    study_mode.push('On campus');
                                }
                                resJsonData.course_study_mode = study_mode.toString();//.join(',');
                                console.log("## FInal string-->" + campLocationValTrimmed);
                            }
                            break;
                        }


                        case 'course_study_mode': { // Location Launceston
                            //resJsonData.course_study_mode = coursestudymodeText;
                            resJsonData.course_study_mode = "On campus";
                            break;
                        }
                        case 'course_tuition_fees_international_student_more':

                        case 'course_tuition_fees_international_student': { // {"year": "2019","fees": [{"international_student": "43500","fee_duration_years": "1","currency": "AUD"}]}
                            const courseTuitionFee = {};
                            // courseTuitionFee.year = configs.propValueNotAvaialble;

                            const feesList = [];
                            const feesDict = {
                                international_student: {}
                            };
                            const feeYear = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_year);
                            const courseKeyVal = courseScrappedData.course_tuition_fees_international_student;
                            const courseKeyValAdditional = courseScrappedData.course_tuition_fees_international_student_additional;
                            console.log("courseKeyValAdditional -->", JSON.stringify(courseKeyVal));
                            let feesIntStudent = [];
                            let feesIntStudentAdditional = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                feesIntStudent = selList[0];
                                            }
                                        }
                                    }
                                }
                            }

                            //External study mode fees are different
                            if (Array.isArray(courseKeyValAdditional)) {
                                for (const rootEleDict of courseKeyValAdditional) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                feesIntStudentAdditional = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (feesIntStudentAdditional && feesIntStudentAdditional.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudentAdditional).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                                const arrval = String(feesVal1).split('.');

                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesIntStudentAdditional = Number(feesNumber);
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)


                            console.log("feesIntStudentAdditional -->", feesIntStudentAdditional);
                            //
                            // if (feeYear && feeYear.length > 0) {
                            //   courseTuitionFee.year = feeYear;
                            // }

                            // if we can extract value as Int successfully then replace Or keep as it is
                            if (feesIntStudent && feesIntStudent.length > 0) { // extract only digits
                                const feesWithDollorTrimmed = String(feesIntStudent).trim();

                                console.log(funcName + 'feesWithDollorTrimmed = ' + feesWithDollorTrimmed);

                                const feesVal1 = String(feesWithDollorTrimmed).replace('$', '');


                                const arrval = String(feesVal1).split('.');

                                const feesVal = String(arrval[0]);

                                console.log(funcName + 'feesVal = ' + feesVal);
                                if (feesVal) {
                                    const regEx = /\d/g;
                                    let feesValNum = feesVal.match(regEx);
                                    if (feesValNum) {
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        feesValNum = feesValNum.join('');
                                        console.log(funcName + 'feesValNum = ' + feesValNum);
                                        let feesNumber = null;
                                        if (feesValNum.includes(',')) {
                                            feesNumber = parseInt(feesValNum.replace(/,/g, ''), 10);
                                        } else {
                                            feesNumber = feesValNum;
                                        }
                                        console.log(funcName + 'feesNumber = ' + feesNumber);
                                        if (Number(feesNumber)) {
                                            feesDict.international_student = ({
                                                amount: Number(feesNumber),
                                                duration: 1,
                                                unit: "Year",
                                                description: feesVal
                                            });
                                        } else {
                                            feesDict.international_student = ({
                                                amount: 0,
                                                duration: 1,
                                                unit: "Year",
                                                description: ""
                                            });
                                        }
                                    }
                                }
                            } // if (feesIntStudent && feesIntStudent.length > 0)
                            else {
                                feesDict.international_student = ({
                                    amount: 0,
                                    duration: 1,
                                    unit: "Year",
                                    description: ""
                                });
                            }
                            if (feesIntStudent.length > 0) { // extract only digits
                                let arr = [];
                                if (feesIntStudentAdditional == '' || feesIntStudentAdditional == undefined) {

                                } else {
                                    feesIntStudentAdditional = "External/Online - AUD " + feesIntStudentAdditional;
                                    arr.push(feesIntStudentAdditional);
                                    feesIntStudent = "On-campus - " + feesIntStudent;
                                    arr.push(feesIntStudent);
                                }

                                //feesDict.international_student_all_fees = arr;
                            } else {
                                let arr = [];

                                //  feesDict.international_student_all_fees = arr;
                            }
                            if (feesDict.international_student) { // if we have fees value then add following supporting attributes
                                const feesDuration = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fee_duration_years);
                                const feesCurrency = await Course.extractValueFromScrappedElement(courseScrappedData.course_tuition_fees_currency);
                                if (feesDuration && feesDuration.length > 0) {
                                    feesDict.fee_duration_years = feesDuration;
                                }
                                if (feesCurrency && feesCurrency.length > 0) {
                                    feesDict.currency = feesCurrency;
                                }
                                if (feesDict) {
                                    // feesList.push(feesDict);
                                    var campus = resJsonData.course_campus_location;
                                    for (let loc of campus) {
                                        feesList.push({ name: loc.name, value: feesDict });
                                    }
                                }
                                if (feesList && feesList.length > 0) {
                                    courseTuitionFee.fees = feesList;
                                }
                                console.log(funcName + 'course_tuition_fee = ' + JSON.stringify(courseTuitionFee));
                                if (courseTuitionFee) {
                                    resJsonData.course_tuition_fee = courseTuitionFee;
                                }
                                console.log("feesDict.international_student = ", feesDict.international_student);
                                console.log("resJsonData.course_tuition_fee_amount = ", resJsonData.course_tuition_fee_amount);
                            }

                            break;
                        }
                        case 'program_code': {
                            if (postGradIndex > -1) {
                                let programcode = postgradCourseList[postGradIndex].programcode;
                                if (programcode.includes(":")) {
                                    programcode = programcode.split(':')[1]

                                }
                                console.log("Category_programcode", programcode)
                                resJsonData.program_code = programcode
                            }
                            break;
                        }



                        // case 'program_code': {
                        //     const courseKeyVal = courseScrappedData.program_code;
                        //     var program_code = ""
                        //     console.log("courseIntakeStr-->" + JSON.stringify(courseKeyVal));
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                     if (Array.isArray(selList) && selList.length > 0) {
                        //                         program_code = selList;
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }

                        //     if (program_code.length > 1) {
                        //         //for (let arr of program_code) {
                        //         console.log("sfdhfdhfv", program_code);
                        //         if (program_code[0] != program_code[1]) {

                        //             console.log("sfdhfgdffgdhfv", program_code[0]);
                        //             throw new Error("Program not found");
                        //         }
                        //         //  if (program_code[0] == program_code[1]) {
                        //         else {
                        //             console.log("sfdhfgdffgdhfgddfdgdv", program_code[0]);
                        //             resJsonData.program_code = program_code[0];
                        //         }
                        //     }
                        //     else {
                        //         if (program_code.length > 0) {
                        //             for (let arr of program_code) {
                        //                 console.log("sfdhfdhfv", arr);
                        //                 resJsonData.program_code = arr;
                        //             }
                        //         }
                        //     }
                        //     break;

                        // }

                        case 'course_intake': { // "course_intake_display": { "semester1": "February 2019", "semester2": "August 2019" },
                            const courseIntakeDisplay = [];
                            // existing intake value
                            // const courseIntakeStrElem = await Course.extractValueFromScrappedElement(courseScrappedData.course_intake);

                            let courseKeyVal_intake = courseScrappedData.course_intake;
                            let courseintake = [];
                            if (Array.isArray(courseKeyVal_intake)) {
                                for (const rootEleDict of courseKeyVal_intake) {
                                    console.log(funcName + '\n\r rootEleDict ieltsDict= ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict ieltsDict= ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList ieltsDict= ' + JSON.stringify(selList));
                                            if (Array.isArray(selList)) {
                                                for (let sel of selList) {
                                                    if (courseintake != sel) {
                                                        courseintake.push(sel);
                                                        console.log("courseintakecourseintake", courseintake)
                                                        // if (courseintake.includes('Starts in ')) {
                                                        //     courseintake = courseintake.replace('Starts in', '').trim();
                                                        // }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("R locationArray", campus);
                            let intake_val = []
                            for (let int of courseintake) {
                                intake_val.push(int)
                                console.log("R intake_val", intake_val);
                            }
                            console.log('courseIntakeStr : ' + courseintake);
                            //console.log("R courseIntakeStrFile", courseIntakeStrFile);
                            // const intakeSplit =[];
                            if (courseintake && courseintake.length > 0) {
                                resJsonData.course_intake = courseintake;

                                courseintake = String(courseintake).split(',');

                                console.log('course_intake intakeStrList = ' + JSON.stringify(courseintake));

                                if (courseintake && courseintake.length > 0) {
                                    for (var part of courseintake) {
                                        // part = part.split("(");
                                        // part = part[1].split(")")[0];
                                        console.log("R part", part);
                                        //part = part.replace('.', '').replace('Enrol in', '').trim();
                                        courseIntakeDisplay.push(part.trim());
                                    } // for
                                }
                                console.log('course_intake courseIntakeDisplay = ' + JSON.stringify(courseIntakeDisplay));
                            }
                            var campus = resJsonData.course_campus_location;
                            if (courseIntakeDisplay && courseIntakeDisplay.length > 0) {
                                resJsonData.course_intake = {};
                                resJsonData.course_intake.intake = [];
                                for (let location11 of campus) {
                                    resJsonData.course_intake.intake.push({
                                        "name": location11.name,
                                        "value": courseIntakeDisplay
                                    });
                                }
                            }
                            console.log("intakes123 -->", resJsonData.course_intake.intake);
                            let more_details = await utils.getValueFromHardCodedJsonFile('intake_url');
                            let formatedIntake = await format_functions.providemyintake(resJsonData.course_intake.intake, "mom", "");
                            // console.log(JSON.stringify(providemyintake(intake, "MOM", "")));
                            console.log("Intakes --> ", JSON.stringify(formatedIntake));
                            var intakedata = {};
                            intakedata.intake = formatedIntake;

                            intakedata.more_details = more_details;
                            resJsonData.course_intake = intakedata;
                            break;
                        }


                        case 'course_scholarship': {
                            const courseKeyVal = courseScrappedData.course_scholarship;
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                resJsonData.course_scholarship = resScholarshipJson;
                            }
                            break;
                        }
                        case 'univ_name': {
                            const courseKeyVal = courseScrappedData.univ_name;
                            let resUnivName = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivName = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivName) {
                                resJsonData.univ_name = resUnivName;
                            }
                            break;
                        }
                        case 'univ_logo': {
                            const courseKeyVal = courseScrappedData.univ_logo;
                            let resUnivLogo = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resUnivLogo = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resUnivLogo) {
                                resJsonData.univ_logo = resUnivLogo;
                            }
                            break;
                        }
                        case 'course_accomodation_cost': {
                            const courseKeyVal = courseScrappedData.course_accomodation_cost;
                            let resAccomodationCostJson = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resAccomodationCostJson = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (resAccomodationCostJson) {
                                resJsonData.course_accomodation_cost = resAccomodationCostJson;
                            }
                            break;
                        }
                        // case 'course_cricos_code': {
                        //     const courseKeyVal = courseScrappedData.course_cricos_code;
                        //     const mycodes = [];
                        //     let course_cricos_code = null;
                        //     if (Array.isArray(courseKeyVal)) {
                        //         for (const rootEleDict of courseKeyVal) {
                        //             console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                        //             const elementsList = rootEleDict.elements;
                        //             for (const eleDict of elementsList) {
                        //                 console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                        //                 const selectorsList = eleDict.selectors;
                        //                 for (const selList of selectorsList) {
                        //                     console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                        //                     if (Array.isArray(selList) && selList.length > 0) {
                        //                         course_cricos_code = selList;
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }

                        //     if (course_cricos_code) {
                        //         const locations = resJsonData.course_campus_location;
                        //         console.log("locations----->", locations);
                        //         console.log("course_cricos_code----->", course_cricos_code);
                        //         for (let location of locations) {
                        //             mycodes.push({
                        //                 location: location.name, code: course_cricos_code.toString(), iscurrent: false
                        //             });
                        //         }

                        //         resJsonData.course_cricos_code = mycodes;
                        //     } else {
                        //         throw new Error("Campus not found");
                        //     }
                        //     //resJsonData.course_cricos_code = await Course.extractValueFromScrappedElement(courseScrappedData.course_cricos_code);
                        //     break;
                        // }
                        case 'course_country': {
                            const courseKeyVal = courseScrappedData.course_country;
                            let course_country = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            //if (Array.isArray(selList) && selList.length > 0) {
                                            course_country = selList[0];
                                            //}
                                        }
                                    }
                                }
                            }
                            resJsonData.course_country = course_country;
                            break;
                        }
                        case 'course_overview': {
                            const courseKeyVal = courseScrappedData.course_overview;
                            let course_overview = null;
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_overview = selList[0];
                                            }
                                        }
                                    }
                                }
                            }
                            if (course_overview) {
                                resJsonData.course_overview = course_overview;
                            } else {
                                resJsonData.course_overview = "";
                            }
                            break;
                        }
                        case 'course_career_outcome': {
                            const courseKeyVal = courseScrappedData.course_career_outcome;
                            let course_career_outcome = [];
                            if (Array.isArray(courseKeyVal)) {
                                for (const rootEleDict of courseKeyVal) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r course_overview selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                course_career_outcome = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            console.log("course_career_outcome.length -->", course_career_outcome);
                            if (course_career_outcome.length > 0) {
                                resJsonData.course_career_outcome = course_career_outcome
                            } else {
                                resJsonData.course_career_outcome = [];
                            }


                            break;
                        }

                        case 'course_outline': {
                            console.log("course_outline--->")
                            const outline = {
                                majors: [],
                                minors: [],
                                more_details: ""
                            };
                            var major, minor
                            var intakedata1 = {};
                            const courseKeyVal111 = courseScrappedData.course_outline_major;
                            console.log("courseKeyVal111------->", JSON.stringify(courseKeyVal111))
                            let resScholarshipJson = null;
                            if (Array.isArray(courseKeyVal111)) {
                                for (const rootEleDict of courseKeyVal111) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson = selList;
                                                console.log("resScholarshipJson", resScholarshipJson)
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson) {
                                major = resScholarshipJson
                            } else {
                                major = []
                            }
                            const courseKeyVal1 = courseScrappedData.course_outline_minors;
                            let resScholarshipJson1 = null;
                            if (Array.isArray(courseKeyVal1)) {
                                for (const rootEleDict of courseKeyVal1) {
                                    console.log(funcName + '\n\r rootEleDict = ' + JSON.stringify(rootEleDict));
                                    const elementsList = rootEleDict.elements;
                                    for (const eleDict of elementsList) {
                                        console.log(funcName + '\n\r eleDict = ' + JSON.stringify(eleDict));
                                        const selectorsList = eleDict.selectors;
                                        for (const selList of selectorsList) {
                                            console.log(funcName + '\n\r selList = ' + JSON.stringify(selList));
                                            if (Array.isArray(selList) && selList.length > 0) {
                                                resScholarshipJson1 = selList;
                                            }
                                        }
                                    }
                                }
                            }
                            if (resScholarshipJson1) {
                                minor = resScholarshipJson1
                            } else {
                                minor = []
                            }
                            let m_d = resJsonData.course_url;
                            console.log("md--------->", m_d)
                            var intakedata1 = {};
                            intakedata1.majors = major;
                            intakedata1.minors = minor;
                            intakedata1.more_details = m_d;
                            resJsonData.course_outline = intakedata1;
                            break;
                        }
                        case 'application_fee':
                            {
                                // let application_fee = await Course.extractValueFromScrappedElement(courseScrappedData.application_fee);
                                // if (application_fee.length > 0) {
                                //     resJsonData.application_fee = application_fee;
                                // } else {
                                resJsonData.application_fee = '';
                                // }

                                break;
                            }

                        default: {
                            console.log(funcName + 'none of mach found with the case, so executing default and not processing anything...');
                            break;
                        } // default case
                    } //
                    console.log(funcName + resJsonData[key] + ' = ' + JSON.stringify(resJsonData[key]));
                } // if (courseScrappedData.hasOwnProperty(key))
            } var NEWJSONSTRUCT = {}, structDict = [];
            if (fs.existsSync("./output/new_structure_data.json")) {
                structDict = JSON.parse(fs.readFileSync("./output/new_structure_data.json"));
            }
            console.log("#location-->" + JSON.stringify(resJsonData.course_campus_location))
            var locations = resJsonData.course_campus_location;

            for (let location of locations) {
                NEWJSONSTRUCT.course_id = resJsonData.course_title.replace(/[\/]+/g, '').replace(/\s/g, '').toLowerCase();
                NEWJSONSTRUCT.course_location_id = NEWJSONSTRUCT.course_id;
                NEWJSONSTRUCT.course_title = resJsonData.course_title;
                NEWJSONSTRUCT.univ_id = resJsonData.univ_id;
                NEWJSONSTRUCT.univ_name = resJsonData.univ_name;
                NEWJSONSTRUCT.univ_logo = resJsonData.univ_logo;
                NEWJSONSTRUCT.course_url = resJsonData.course_url;
                // NEWJSONSTRUCT.cricos_code = resJsonData.course_cricos_code;
                //      console.log("##cricos_code-->" + JSON.stringify(NEWJSONSTRUCT.cricos_code));
                console.log("##fees-->" + JSON.stringify(resJsonData.course_tuition_fee));
                NEWJSONSTRUCT.course_campus_location = location.name;
                NEWJSONSTRUCT.course_tuition_fee = resJsonData.course_tuition_fee.fees[0].international_student;
                NEWJSONSTRUCT.currency = resJsonData.course_tuition_fee.fees[0].currency;
                NEWJSONSTRUCT.fee_duration_years = resJsonData.course_tuition_fee.fees[0].fee_duration_years;
                NEWJSONSTRUCT.course_duration_display = resJsonData.course_duration_display;
                console.log("##course_duration_display-->" + JSON.stringify(resJsonData.course_duration_display));
                NEWJSONSTRUCT.course_study_mode = resJsonData.course_study_mode;

                var intakes = resJsonData.course_intake.intake;

                var matchrec = [];
                for (let dintake of intakes) {
                    if (location == dintake.name) {
                        matchrec = dintake.value;
                    }
                }
                if (matchrec.length > 0) {
                    NEWJSONSTRUCT.course_intake = matchrec[0];
                }
                else {
                    NEWJSONSTRUCT.course_intake = "";
                }


                for (let myfees of resJsonData.course_tuition_fee.fees) {
                    if (myfees.name == location) {
                        NEWJSONSTRUCT.international_student_all_fees = myfees;
                    }
                }
                structDict.push(NEWJSONSTRUCT);
                console.log("NEWJSONSTRUCT::::" + NEWJSONSTRUCT);
                NEWJSONSTRUCT = {};
            }
            for (let location_wise_data of structDict) {
                console.log("location::::" + location_wise_data.course_campus_location);
                resJsonData.course_id = location_wise_data.course_location_id;
                // resJsonData.basecourseid = location_wise_data.course_id;

                var filelocation = "./output/" + resJsonData.univ_id + "_" + location_wise_data.course_location_id + "-" + resJsonData.course_study_level.toLowerCase() + "_details.json";
                console.log("Write file--->" + filelocation)
                fs.writeFileSync(filelocation, JSON.stringify(resJsonData));


            }
            return resJsonData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            throw (error);
        }
    }

    // scrape each course details
    static async scrapeAndFormatCourseDetails(courseDict) {
        const funcName = 'scrapeAndFormatCourseDetails ';
        let s = null;
        try {
            s = new Scrape();
            await s.init({ headless: true });
            await s.setupNewBrowserPage(courseDict.href);
            Scrape.validateParams([courseDict]);
            // get course href and innerText from courseDict and ensure it is valid
            if (!(courseDict && courseDict.href && courseDict.href.length > 0 && courseDict.innerText && courseDict.innerText.length > 0)) {
                console.log(funcName + 'Invalid courseDict, courseDict = ' + JSON.stringify(courseDict));
                throw (new Error('Invalid courseDict, courseDict = ' + JSON.stringify(courseDict)));
            }
            const courseUrl = courseDict.href; const courseTitle = courseDict.innerText;
            console.log(funcName + 'courseTitle = ' + courseTitle);
            console.log(funcName + 'courseUrl = ' + courseUrl);
            // output course data and file path
            let courseScrappedData = null;
            // create courseId from course title
            const courseId = await utils.removeAllWhiteSpcaesAndLowerCase(courseTitle);
            // validate generated courseId
            console.log(funcName + 'courseId = ' + courseId);
            if (!(courseId && courseId.length > 0)) {
                console.log(funcName + 'Invalid courseId, courseId = ' + courseId);
                throw (new Error('Invalid courseId, courseId = ' + courseId));
            }
            // configure output file path as per UpdateItem configuration
            const opCourseFilePath = await utils.generateOutputCourseFilePath(courseDict);
            // decide if need to scrape or read data from local outpurfile
            if (appConfigs.shouldTakeFromOutputFolder) { // read data from local file
                console.log(funcName + 'reading scrapped data from local output file...');
                const fileData = fs.readFileSync(opCourseFilePath);
                if (!fileData) {
                    console.log(funcName + 'Invalid fileData, fileData = ' + fileData);
                    throw (new Error('Invalid fileData, fileData = ' + fileData));
                }
                courseScrappedData = JSON.parse(fileData);
                return courseScrappedData;
            } // if (appConfigs.shouldTakeFromOutputFolder)
            // Scrape course data as per selector file

            if (appConfigs.AWS_PERFORM_UPDATEITEM_OPERATION) {
                console.log(funcName + 'reading selUpdateCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selUpdateCourseDetailsSelectorsJsonFilepath);
            } else {
                console.log(funcName + 'reading selCourseDetailsSelectorsJsonFilepath = ' + appConfigs.selCourseDetailsSelectorsJsonFilepath);
                courseScrappedData = await s.scrapeSelectorFile(appConfigs.selCourseDetailsSelectorsJsonFilepath);
            }
            if (!courseScrappedData) {
                throw (new Error('Invalid courseScrappedData, courseScrappedData = ' + courseScrappedData));
            }
            // console.log("courseDict.innerText",courseDict.innerText)
            //const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.href, courseDict.innerText);
            const formattedScrappedData = await ScrapeCourse.formatOutput(courseScrappedData, courseDict.innerText, courseDict.href, s.page.url());
            const finalScrappedData = await Course.removeKeys(formattedScrappedData);
            if (s) {
                await s.close();
            }
            return finalScrappedData;
        } catch (error) {
            console.log(funcName + 'try-catch error = ' + error);
            if (s) {
                await s.close();
            }
            throw (error);
        }
    } // scrapeCourseDetails
} // class
module.exports = { ScrapeCourse };
